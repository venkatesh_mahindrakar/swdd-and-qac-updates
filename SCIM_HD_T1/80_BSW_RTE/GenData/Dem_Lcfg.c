/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Dem
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Dem_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:38
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/



/* configuration, interrupt handling implementations differ from the
 * source identification define used here. The naming
 * schemes for those files can be taken from this list:
 *
 * Dem.c:         DEM_SOURCE
 * Dem_Lcfg.c:    DEM_LCFG_SOURCE
 * Dem_PBcfg.c:   DEM_PBCFG_SOURCE */
#define DEM_LCFG_SOURCE


/**********************************************************************************************************************
  MISRA JUSTIFICATIONS
**********************************************************************************************************************/

/* PRQA S 0810 EOF */ /* MD_MSR_1.1_810 */                                      /* #include "..." causes nesting to exceed 8 levels - program is non-conforming. -- caused by #include'd files. */
/* PRQA S 0828 EOF */ /* MD_MSR_1.1_828 */                                      /* Maximum '#if...' nesting exceeds 8 levels - program is non-conforming -- caused by #include'd files. */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */                                      /* Number of macro definitions exceeds 1024 - program is non-conforming -- caused by #include'd files. */
/* PRQA S 0779 EOF */ /* MD_DEM_5.1 */                                          /* Identifier does not differ in 32 significant characters -- caused by Autosar algorithm for unique symbolic names. */
/* PRQA S 0612 EOF */ /* MD_DEM_1.1_612 */                                      /* The size of an object exceeds 32767 bytes - program is non-conforming -- caused by large user configration. */


/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/

#include "Dem.h"
#include "Os.h"
#if (DEM_CFG_USE_NVM == STD_ON)
# include "NvM.h"                                                               /* get: symbolic names for NvM block IDs */
#endif
#if (DEM_CFG_SUPPORT_J1939 == STD_ON)
# include "J1939Nm.h"                                                           /* get: symbolic names for J1939Nm node IDs */
#endif
#if (DEM_CFG_USE_RTE == STD_ON)
/* DEM used with RTE */
# include "Rte_DemMaster_0.h"
#endif
#include "Dem_AdditionalIncludeCfg.h"                                           /* additional, configuration defined files */

/**********************************************************************************************************************
  LOCAL CONSTANT MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL FUNCTION MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/



/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/



/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  Dem_Cfg_CallbackDtcStatusChanged
**********************************************************************************************************************/
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_DtcStatusChangedFPtrType, DEM_CONST) Dem_Cfg_CallbackDtcStatusChanged[1] = {
  /* Index     CallbackDtcStatusChanged                                           */
  /*     0 */ Rte_Call_CBStatusDTC_DemCallbackDTCStatusChanged_DTCStatusChanged 
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DataCollectionTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DataCollectionTable
  \details
  Element                                Description
  IdNumber                           
  CollectionSize                     
  DataElementTableCol2ElmtIndEndIdx      the end index of the 0:n relation pointing to Dem_Cfg_DataElementTableCol2ElmtInd
  DataElementTableCol2ElmtIndStartIdx    the start index of the 0:n relation pointing to Dem_Cfg_DataElementTableCol2ElmtInd
  MaskedBits                             contains bitcoded the boolean data of Dem_Cfg_DataElementTableCol2ElmtIndUsedOfDataCollectionTable, Dem_Cfg_UpdateOfDataCollectionTable
  StorageKind                        
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DataCollectionTableType, DEM_CONST) Dem_Cfg_DataCollectionTable[25] = {
    /* Index    IdNumber  CollectionSize  DataElementTableCol2ElmtIndEndIdx                                  DataElementTableCol2ElmtIndStartIdx                                  MaskedBits  StorageKind                       Referable Keys */
  { /*     0 */  0x0000u,             0u, DEM_CFG_NO_DATAELEMENTTABLECOL2ELMTINDENDIDXOFDATACOLLECTIONTABLE, DEM_CFG_NO_DATAELEMENTTABLECOL2ELMTINDSTARTIDXOFDATACOLLECTIONTABLE,      0x00u, DEM_CFG_EREC_TYPE_GLOBAL   },  /* [#NoDataCollectionConfigured] */
  { /*     1 */  0x0001u,             6u,                                                                1u,                                                                  0u,      0x03u, DEM_CFG_EREC_TYPE_USER     },  /* [#EdrDemExtendedDataRecordClass, Ext:DemExtendedDataClass] */
  { /*     2 */  0x0030u,             1u,                                                                2u,                                                                  1u,      0x03u, DEM_CFG_EREC_TYPE_GLOBAL   },  /* [#EdrFaultDetectionCounter, Ext:ExtendedDataClass_361fef39] */
  { /*     3 */  0x0031u,             1u,                                                                3u,                                                                  2u,      0x03u, DEM_CFG_EREC_TYPE_INTERNAL },  /* [#EdrHighestFaultDetectionCounterValueSinceLastCleared, Ext:ExtendedDataClass_361fef39] */
  { /*     4 */  0x0045u,             1u,                                                                4u,                                                                  3u,      0x03u, DEM_CFG_EREC_TYPE_INTERNAL },  /* [#EdrNumberofconsecutiveoperationcycleswithtestFailedtrue, Ext:ExtendedDataClass_361fef39] */
  { /*     5 */  0x0042u,             1u,                                                                5u,                                                                  4u,      0x03u, DEM_CFG_EREC_TYPE_INTERNAL },  /* [#EdrNumberofoperationcyclessincefirstvalidatedtestFailedtrue, Ext:ExtendedDataClass_361fef39] */
  { /*     6 */  0x0040u,             1u,                                                                6u,                                                                  5u,      0x03u, DEM_CFG_EREC_TYPE_INTERNAL },  /* [#EdrNumberofoperationcyclessincelastvalidatedtestFailedtrue, Ext:ExtendedDataClass_361fef39] */
  { /*     7 */  0x0041u,             1u,                                                                7u,                                                                  6u,      0x03u, DEM_CFG_EREC_TYPE_INTERNAL },  /* [#EdrNumberofvalidatedtestoperationcyclessincelastvalidatedtestFailedtrue, Ext:ExtendedDataClass_361fef39] */
  { /*     8 */  0x0043u,             1u,                                                                8u,                                                                  7u,      0x03u, DEM_CFG_EREC_TYPE_INTERNAL },  /* [#EdrNumberofvalidatedtestpassedoperationcyclessincefirstvalidatedtestFailedtrue, Ext:ExtendedDataClass_361fef39] */
  { /*     9 */  0x0044u,             1u,                                                                9u,                                                                  8u,      0x03u, DEM_CFG_EREC_TYPE_INTERNAL },  /* [#EdrNumberoperationcycleswithtestFailedtrue, Ext:ExtendedDataClass_361fef39] */
  { /*    10 */  0x0020u,             2u,                                                               10u,                                                                  9u,      0x03u, DEM_CFG_EREC_TYPE_INTERNAL },  /* [#EdrOccurrenceCounter, Ext:ExtendedDataClass_361fef39] */
  { /*    11 */  0x0010u,             6u,                                                               16u,                                                                 10u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#EdrUTCTimeStamp_First, Ext:ExtendedDataClass_361fef39] */
  { /*    12 */  0x0011u,             6u,                                                               22u,                                                                 16u,      0x03u, DEM_CFG_EREC_TYPE_USER     },  /* [#EdrUTCTimeStamp_Latest, Ext:ExtendedDataClass_361fef39] */
  { /*    13 */  0x0055u,             2u,                                                               23u,                                                                 22u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidDemDidClass_StartApplication, Ffm:DemFreezeFrameClass_StartApplication] */
  { /*    14 */  0x1102u,             2u,                                                               24u,                                                                 23u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1AFR, Ffm:FreezeFrameClass_04f05c8a, Ffm:FreezeFrameClass_345716cc, Ffm:FreezeFrameClass_505ba959, Ffm:FreezeFrameClass_9a94c929, Ffm:FreezeFrameClass_9cf73c71, Ffm:FreezeFrameClass_ebf00ce7, Ffm:FreezeFrameClass_ed93f9bf, Ffm:FreezeFrameClass_f947e5b7] */
  { /*    15 */  0x1104u,             4u,                                                               25u,                                                                 24u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1AFS, Ffm:FreezeFrameClass_04f05c8a, Ffm:FreezeFrameClass_345716cc, Ffm:FreezeFrameClass_505ba959, Ffm:FreezeFrameClass_9a94c929, Ffm:FreezeFrameClass_9cf73c71, Ffm:FreezeFrameClass_ebf00ce7, Ffm:FreezeFrameClass_ed93f9bf, Ffm:FreezeFrameClass_f947e5b7] */
  { /*    16 */  0x1100u,             1u,                                                               26u,                                                                 25u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1AFT, Ffm:FreezeFrameClass_04f05c8a, Ffm:FreezeFrameClass_345716cc, Ffm:FreezeFrameClass_505ba959, Ffm:FreezeFrameClass_9a94c929, Ffm:FreezeFrameClass_9cf73c71, Ffm:FreezeFrameClass_ebf00ce7, Ffm:FreezeFrameClass_ed93f9bf, Ffm:FreezeFrameClass_f947e5b7] */
  { /*    17 */  0x120Au,             1u,                                                               27u,                                                                 26u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1CXF, Ffm:FreezeFrameClass_f947e5b7] */
  { /*    18 */  0x1208u,             1u,                                                               28u,                                                                 27u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1DCT, Ffm:FreezeFrameClass_ebf00ce7] */
  { /*    19 */  0x1209u,             5u,                                                               29u,                                                                 28u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1DCU, Ffm:FreezeFrameClass_9cf73c71] */
  { /*    20 */  0x127Au,             1u,                                                               30u,                                                                 29u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1QXI, Ffm:FreezeFrameClass_345716cc] */
  { /*    21 */  0x127Bu,             4u,                                                               31u,                                                                 30u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1QXJ, Ffm:FreezeFrameClass_04f05c8a, Ffm:FreezeFrameClass_345716cc, Ffm:FreezeFrameClass_9a94c929, Ffm:FreezeFrameClass_ed93f9bf] */
  { /*    22 */  0x127Cu,             2u,                                                               32u,                                                                 31u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1QXM, Ffm:FreezeFrameClass_04f05c8a] */
  { /*    23 */  0x127Du,             2u,                                                               33u,                                                                 32u,      0x02u, DEM_CFG_EREC_TYPE_USER     },  /* [#DidP1QXP, Ffm:FreezeFrameClass_9a94c929] */
  { /*    24 */  0x127Eu,             2u,                                                               34u,                                                                 33u,      0x02u, DEM_CFG_EREC_TYPE_USER     }   /* [#DidP1QXR, Ffm:FreezeFrameClass_ed93f9bf] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DataCollectionTableEdr2CollInd
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DataCollectionTableEdr2CollInd
  \brief  the indexes of the 1:1 sorted relation pointing to Dem_Cfg_DataCollectionTable
*/ 
#define DEM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DataCollectionTableEdr2CollIndType, DEM_CONST) Dem_Cfg_DataCollectionTableEdr2CollInd[12] = {
  /* Index     DataCollectionTableEdr2CollInd      Referable Keys */
  /*     0 */                              1u,  /* [Ext:DemExtendedDataClass] */
  /*     1 */                             11u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*     2 */                             12u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*     3 */                             10u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*     4 */                              2u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*     5 */                              3u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*     6 */                              6u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*     7 */                              7u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*     8 */                              5u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*     9 */                              8u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*    10 */                              9u,  /* [Ext:ExtendedDataClass_361fef39] */
  /*    11 */                              4u   /* [Ext:ExtendedDataClass_361fef39] */
};
#define DEM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DataCollectionTableFfm2CollInd
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DataCollectionTableFfm2CollInd
  \brief  the indexes of the 1:1 sorted relation pointing to Dem_Cfg_DataCollectionTable
*/ 
#define DEM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DataCollectionTableFfm2CollIndType, DEM_CONST) Dem_Cfg_DataCollectionTableFfm2CollInd[36] = {
  /* Index     DataCollectionTableFfm2CollInd      Referable Keys */
  /*     0 */                             13u,  /* [Ffm:DemFreezeFrameClass_StartApplication] */
  /*     1 */                             16u,  /* [Ffm:FreezeFrameClass_04f05c8a] */
  /*     2 */                             14u,  /* [Ffm:FreezeFrameClass_04f05c8a] */
  /*     3 */                             15u,  /* [Ffm:FreezeFrameClass_04f05c8a] */
  /*     4 */                             21u,  /* [Ffm:FreezeFrameClass_04f05c8a] */
  /*     5 */                             22u,  /* [Ffm:FreezeFrameClass_04f05c8a] */
  /*     6 */                             16u,  /* [Ffm:FreezeFrameClass_345716cc] */
  /*     7 */                             14u,  /* [Ffm:FreezeFrameClass_345716cc] */
  /*     8 */                             15u,  /* [Ffm:FreezeFrameClass_345716cc] */
  /*     9 */                             20u,  /* [Ffm:FreezeFrameClass_345716cc] */
  /*    10 */                             21u,  /* [Ffm:FreezeFrameClass_345716cc] */
  /*    11 */                             16u,  /* [Ffm:FreezeFrameClass_505ba959] */
  /*    12 */                             14u,  /* [Ffm:FreezeFrameClass_505ba959] */
  /*    13 */                             15u,  /* [Ffm:FreezeFrameClass_505ba959] */
  /*    14 */                             16u,  /* [Ffm:FreezeFrameClass_9a94c929] */
  /*    15 */                             14u,  /* [Ffm:FreezeFrameClass_9a94c929] */
  /*    16 */                             15u,  /* [Ffm:FreezeFrameClass_9a94c929] */
  /*    17 */                             21u,  /* [Ffm:FreezeFrameClass_9a94c929] */
  /*    18 */                             23u,  /* [Ffm:FreezeFrameClass_9a94c929] */
  /*    19 */                             16u,  /* [Ffm:FreezeFrameClass_9cf73c71] */
  /*    20 */                             14u,  /* [Ffm:FreezeFrameClass_9cf73c71] */
  /*    21 */                             15u,  /* [Ffm:FreezeFrameClass_9cf73c71] */
  /*    22 */                             19u,  /* [Ffm:FreezeFrameClass_9cf73c71] */
  /*    23 */                             16u,  /* [Ffm:FreezeFrameClass_ebf00ce7] */
  /*    24 */                             14u,  /* [Ffm:FreezeFrameClass_ebf00ce7] */
  /*    25 */                             15u,  /* [Ffm:FreezeFrameClass_ebf00ce7] */
  /*    26 */                             18u,  /* [Ffm:FreezeFrameClass_ebf00ce7] */
  /*    27 */                             16u,  /* [Ffm:FreezeFrameClass_ed93f9bf] */
  /*    28 */                             14u,  /* [Ffm:FreezeFrameClass_ed93f9bf] */
  /*    29 */                             15u,  /* [Ffm:FreezeFrameClass_ed93f9bf] */
  /*    30 */                             21u,  /* [Ffm:FreezeFrameClass_ed93f9bf] */
  /*    31 */                             24u,  /* [Ffm:FreezeFrameClass_ed93f9bf] */
  /*    32 */                             16u,  /* [Ffm:FreezeFrameClass_f947e5b7] */
  /*    33 */                             14u,  /* [Ffm:FreezeFrameClass_f947e5b7] */
  /*    34 */                             15u,  /* [Ffm:FreezeFrameClass_f947e5b7] */
  /*    35 */                             17u   /* [Ffm:FreezeFrameClass_f947e5b7] */
};
#define DEM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DataElementTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DataElementTable
  \details
  Element         Description
  ElementKind     DataElement kind, returned by Dem_Cfg_DataCallbackType()
  ElementSize     Size of data element in Byte.
  ReadDataFunc    C-function for getting the data. Its signature depends on ElementKind: With value(s) DEM_CFG_DATA_FROM_CBK_WITH_EVENTID use: Std_ReturnType (*)(uint8* Buffer, uint16 EventId); - and use: Std_ReturnType (*)(uint8* Buffer); with the other values DEM_CFG_DATA_FROM_CBK, DEM_CFG_DATA_FROM_SR_PORT_BOOLEAN, DEM_CFG_DATA_FROM_SR_PORT_SINT16, DEM_CFG_DATA_FROM_SR_PORT_SINT16_INTEL, DEM_CFG_DATA_FROM_SR_PORT_SINT32, DEM_CFG_DATA_FROM_SR_PORT_SINT32_INTEL, DEM_CFG_DATA_FROM_SR_PORT_SINT8, DEM_CFG_DATA_FROM_SR_PORT_SINT8_N, DEM_CFG_DATA_FROM_SR_PORT_UINT16, DEM_CFG_DATA_FROM_SR_PORT_UINT16_INTEL, DEM_CFG_DATA_FROM_SR_PORT_UINT32, DEM_CFG_DATA_FROM_SR_PORT_UINT32_INTEL, DEM_CFG_DATA_FROM_SR_PORT_UINT8, DEM_CFG_DATA_FROM_SR_PORT_UINT8_N.
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DataElementTableType, DEM_CONST) Dem_Cfg_DataElementTable[35] = {
    /* Index    ElementKind                                         ElementSize  ReadDataFunc                                                                                                                         Referable Keys */
  { /*     0 */ DEM_CFG_DATAELEMENT_INVALID                       ,          0u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#NoDataElementConfigured] */
  { /*     1 */ DEM_CFG_DATA_FROM_CBK                             ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataElementClass_StartApplication_ReadData },  /* [#DemDataElementClass_StartApplication, DidDemDidClass_StartApplication] */
  { /*     2 */ DEM_CFG_DATA_FROM_CBK                             ,          6u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_DemDataClass_ReadData                         },  /* [#DemDataClass, EdrDemExtendedDataRecordClass] */
  { /*     3 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_Latest_Year_ReadData             },  /* [#UTCTimeStamp_Latest_Year, EdrUTCTimeStamp_Latest] */
  { /*     4 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_First_Day_ReadData               },  /* [#UTCTimeStamp_First_Day, EdrUTCTimeStamp_First] */
  { /*     5 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_First_Seconds_ReadData           },  /* [#UTCTimeStamp_First_Seconds, EdrUTCTimeStamp_First] */
  { /*     6 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1AFT_Data_P1AFT_ReadData                     },  /* [#P1AFT_Data_P1AFT, DidP1AFT] */
  { /*     7 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_First_Hour_ReadData              },  /* [#UTCTimeStamp_First_Hour, EdrUTCTimeStamp_First] */
  { /*     8 */ DEM_CFG_DATA_FROM_AGINGCTR                        ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#NumberofvalidatedtestoperationcyclessincelastvalidatedtestFailedtrue_Numberofvalidatedtestoperationcyclessincelastvalida2ed14afd, EdrNumberofvalidatedtestoperationcyclessincelastvalidatedtestFailedtrue] */
  { /*     9 */ DEM_CFG_DATA_FROM_MAX_FDC_SINCE_LAST_CLEAR        ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#HighestFaultDetectionCounterValueSinceLastCleared_HighestFaultDetectionCounterValueSinceLastCleared, EdrHighestFaultDetectionCounterValueSinceLastCleared] */
  { /*    10 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_First_Minutes_ReadData           },  /* [#UTCTimeStamp_First_Minutes, EdrUTCTimeStamp_First] */
  { /*    11 */ DEM_CFG_DATA_FROM_OCCCTR_2BYTE                    ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#OccurrenceCounter_OccurrenceCounter, EdrOccurrenceCounter] */
  { /*    12 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_First_Month_ReadData             },  /* [#UTCTimeStamp_First_Month, EdrUTCTimeStamp_First] */
  { /*    13 */ DEM_CFG_DATA_FROM_CONSECUTIVE_FAILED_CYCLES       ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#NumberofconsecutiveoperationcycleswithtestFailedtrue_NumberofconsecutiveoperationcycleswithtestFailed_true, EdrNumberofconsecutiveoperationcycleswithtestFailedtrue] */
  { /*    14 */ DEM_CFG_DATA_FROM_CYCLES_SINCE_FIRST_FAILED       ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#NumberofoperationcyclessincefirstvalidatedtestFailedtrue_NumberofoperationcyclessincefirstvalidatedtestFailed_true, EdrNumberofoperationcyclessincefirstvalidatedtestFailedtrue] */
  { /*    15 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_Latest_Hour_ReadData             },  /* [#UTCTimeStamp_Latest_Hour, EdrUTCTimeStamp_Latest] */
  { /*    16 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData          },  /* [#UTCTimeStamp_Latest_Seconds, EdrUTCTimeStamp_Latest] */
  { /*    17 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_Latest_Day_ReadData              },  /* [#UTCTimeStamp_Latest_Day, EdrUTCTimeStamp_Latest] */
  { /*    18 */ DEM_CFG_DATA_FROM_CBK                             ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1AFR_Data_P1AFR_ReadData                     },  /* [#P1AFR_Data_P1AFR, DidP1AFR] */
  { /*    19 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_First_Year_ReadData              },  /* [#UTCTimeStamp_First_Year, EdrUTCTimeStamp_First] */
  { /*    20 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_Latest_Month_ReadData            },  /* [#UTCTimeStamp_Latest_Month, EdrUTCTimeStamp_Latest] */
  { /*    21 */ DEM_CFG_DATA_FROM_CYCLES_TESTED_SINCE_FIRST_FAILED,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#NumberofvalidatedtestpassedoperationcyclessincefirstvalidatedtestFailedtrue_Numberofvalidatedtestpassedoperationcyclessi51bc6327, EdrNumberofvalidatedtestpassedoperationcyclessincefirstvalidatedtestFailedtrue] */
  { /*    22 */ DEM_CFG_DATA_FROM_CURRENT_FDC                     ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#FaultDetectionCounter_FaultDetectionCounter, EdrFaultDetectionCounter] */
  { /*    23 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1CXF_Data_P1CXF_ReadData                     },  /* [#P1CXF_Data_P1CXF, DidP1CXF] */
  { /*    24 */ DEM_CFG_DATA_FROM_CYCLES_SINCE_LAST_FAILED        ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#NumberofoperationcyclessincelastvalidatedtestFailedtrue_NumberofoperationcyclessincelastvalidatedtestFailed_true, EdrNumberofoperationcyclessincelastvalidatedtestFailedtrue] */
  { /*    25 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData          },  /* [#UTCTimeStamp_Latest_Minutes, EdrUTCTimeStamp_Latest] */
  { /*    26 */ DEM_CFG_DATA_FROM_CBK                             ,          4u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1AFS_Data_P1AFS_ReadData                     },  /* [#P1AFS_Data_P1AFS, DidP1AFS] */
  { /*    27 */ DEM_CFG_DATA_FROM_FAILED_CYCLES                   ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ NULL_PTR                                                          },  /* [#NumberoperationcycleswithtestFailedtrue_NumberoperationcycleswithtestFailed_true, EdrNumberoperationcycleswithtestFailedtrue] */
  { /*    28 */ DEM_CFG_DATA_FROM_CBK                             ,          5u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1DCU_Data_P1DCU_ReadData                     },  /* [#P1DCU_Data_P1DCU, DidP1DCU] */
  { /*    29 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1DCT_Data_P1DCT_ReadData                     },  /* [#P1DCT_Data_P1DCT, DidP1DCT] */
  { /*    30 */ DEM_CFG_DATA_FROM_CBK                             ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1QXR_Data_P1QXR_ReadData                     },  /* [#P1QXR_Data_P1QXR, DidP1QXR] */
  { /*    31 */ DEM_CFG_DATA_FROM_CBK                             ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1QXM_Data_P1QXM_ReadData                     },  /* [#P1QXM_Data_P1QXM, DidP1QXM] */
  { /*    32 */ DEM_CFG_DATA_FROM_CBK                             ,          2u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1QXP_Data_P1QXP_ReadData                     },  /* [#P1QXP_Data_P1QXP, DidP1QXP] */
  { /*    33 */ DEM_CFG_DATA_FROM_CBK                             ,          4u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1QXJ_Data_P1QXJ_ReadData                     },  /* [#P1QXJ_Data_P1QXJ, DidP1QXJ] */
  { /*    34 */ DEM_CFG_DATA_FROM_CBK                             ,          1u,  (Dem_ReadDataFPtrType) /* PRQA S 0313 */ /* MD_DEM_11.1 */ Rte_Call_CBReadData_P1QXI_Data_P1QXI_ReadData                     }   /* [#P1QXI_Data_P1QXI, DidP1QXI] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DataElementTableCol2ElmtInd
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DataElementTableCol2ElmtInd
  \brief  the indexes of the 1:1 sorted relation pointing to Dem_Cfg_DataElementTable
*/ 
#define DEM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DataElementTableCol2ElmtIndType, DEM_CONST) Dem_Cfg_DataElementTableCol2ElmtInd[34] = {
  /* Index     DataElementTableCol2ElmtInd      Referable Keys */
  /*     0 */                           2u,  /* [EdrDemExtendedDataRecordClass] */
  /*     1 */                          22u,  /* [EdrFaultDetectionCounter] */
  /*     2 */                           9u,  /* [EdrHighestFaultDetectionCounterValueSinceLastCleared] */
  /*     3 */                          13u,  /* [EdrNumberofconsecutiveoperationcycleswithtestFailedtrue] */
  /*     4 */                          14u,  /* [EdrNumberofoperationcyclessincefirstvalidatedtestFailedtrue] */
  /*     5 */                          24u,  /* [EdrNumberofoperationcyclessincelastvalidatedtestFailedtrue] */
  /*     6 */                           8u,  /* [EdrNumberofvalidatedtestoperationcyclessincelastvalidatedtestFailedtrue] */
  /*     7 */                          21u,  /* [EdrNumberofvalidatedtestpassedoperationcyclessincefirstvalidatedtestFailedtrue] */
  /*     8 */                          27u,  /* [EdrNumberoperationcycleswithtestFailedtrue] */
  /*     9 */                          11u,  /* [EdrOccurrenceCounter] */
  /*    10 */                          19u,  /* [EdrUTCTimeStamp_First] */
  /*    11 */                          12u,  /* [EdrUTCTimeStamp_First] */
  /*    12 */                           4u,  /* [EdrUTCTimeStamp_First] */
  /*    13 */                           7u,  /* [EdrUTCTimeStamp_First] */
  /*    14 */                          10u,  /* [EdrUTCTimeStamp_First] */
  /*    15 */                           5u,  /* [EdrUTCTimeStamp_First] */
  /*    16 */                           3u,  /* [EdrUTCTimeStamp_Latest] */
  /*    17 */                          20u,  /* [EdrUTCTimeStamp_Latest] */
  /*    18 */                          17u,  /* [EdrUTCTimeStamp_Latest] */
  /*    19 */                          15u,  /* [EdrUTCTimeStamp_Latest] */
  /*    20 */                          25u,  /* [EdrUTCTimeStamp_Latest] */
  /*    21 */                          16u,  /* [EdrUTCTimeStamp_Latest] */
  /*    22 */                           1u,  /* [DidDemDidClass_StartApplication] */
  /*    23 */                          18u,  /* [DidP1AFR] */
  /*    24 */                          26u,  /* [DidP1AFS] */
  /*    25 */                           6u,  /* [DidP1AFT] */
  /*    26 */                          23u,  /* [DidP1CXF] */
  /*    27 */                          29u,  /* [DidP1DCT] */
  /*    28 */                          28u,  /* [DidP1DCU] */
  /*    29 */                          34u,  /* [DidP1QXI] */
  /*    30 */                          33u,  /* [DidP1QXJ] */
  /*    31 */                          31u,  /* [DidP1QXM] */
  /*    32 */                          32u,  /* [DidP1QXP] */
  /*    33 */                          30u   /* [DidP1QXR] */
};
#define DEM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DebounceTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DebounceTable
  \details
  Element                   Description
  DecrementStepSize         (-1) * DemDebounceCounterDecrementStepSize of the DemEventParameter/DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterBased
  FailedThreshold           DemDebounceCounterFailedThreshold of the DemEventParameter/DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterBased
  IncrementStepSize         DemDebounceCounterIncrementStepSize of the DemEventParameter/DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterBased
  PassedThreshold           DemDebounceCounterPassedThreshold of the DemEventParameter/DemEventClass/DemDebounceAlgorithmClass/DemDebounceCounterBased
  EventDebounceAlgorithm    Used DemEventParameter/DemEventClass/DemDebounceAlgorithmClass
  MaskedBits                contains bitcoded the boolean data of Dem_Cfg_DebounceContinuousOfDebounceTable, Dem_Cfg_EventDebounceBehaviorOfDebounceTable, Dem_Cfg_JumpDownOfDebounceTable, Dem_Cfg_JumpUpOfDebounceTable, Dem_Cfg_StorageOfDebounceTable
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DebounceTableType, DEM_CONST) Dem_Cfg_DebounceTable[8] = {
    /* Index    DecrementStepSize  FailedThreshold  IncrementStepSize  PassedThreshold  EventDebounceAlgorithm                                                      MaskedBits        Referable Keys */
  { /*     0 */              -128,             127,               127,            -128, DEM_CFG_DEM_CFG_DEBOUNCETYPE_COUNTER_EVENTDEBOUNCEALGORITHMOFDEBOUNCETABLE,      0x06u },  /* [D1E1Q_19, D1E1R_19] */
  { /*     1 */              -127,             127,                32,            -128, DEM_CFG_DEM_CFG_DEBOUNCETYPE_COUNTER_EVENTDEBOUNCEALGORITHMOFDEBOUNCETABLE,      0x06u },  /* [D1E0K_16, D1E0K_17, D1E1Q_16, D1E1Q_17, D1E1R_16, D1E1R_17, D1E1S_16, D1E1S_17, D1E1T_16, D1E1T_17, D1E1U_16, D1E1U_17, D1E1V_16, D1E1V_17, D1E1W_16, D1E1W_17, D1E2G_16, D1E2G_17, D1E2H_16, D1E2H_17, D1E2I_16, D1E2I_17] */
  { /*     2 */               -64,             127,                64,            -128, DEM_CFG_DEM_CFG_DEBOUNCETYPE_COUNTER_EVENTDEBOUNCEALGORITHMOFDEBOUNCETABLE,      0x02u },  /* [D1BUP_12] */
  { /*     3 */              -127,             127,                16,            -128, DEM_CFG_DEM_CFG_DEBOUNCETYPE_COUNTER_EVENTDEBOUNCEALGORITHMOFDEBOUNCETABLE,      0x06u },  /* [D1AD0_1C] */
  { /*     4 */              -127,             127,               127,            -128, DEM_CFG_DEM_CFG_DEBOUNCETYPE_COUNTER_EVENTDEBOUNCEALGORITHMOFDEBOUNCETABLE,      0x06u },  /* [D1A7X_67, D1ACK_67, D1ACM_67, D1ACN_67, D1AD0_05, D1AD0_29, D1AD0_2F, D1AD0_41, D1AD0_44, D1AD0_45, D1AD0_46, D1AD0_47, D1AD0_48, D1AD0_49, D1AD0_4A, D1AD0_94, D1AD9_16, D1AD9_17, D1AD9_1C, D1BJO_88, D1BJP_88, D1BJQ_88, D1BJR_88, D1BJS_88, D1BK9_87, D1BKB_87, D1BKC_87, D1BKD_87, D1BKE_87, D1BKF_87, D1BKG_87, D1BKH_87, D1BN8_16, D1BN8_17, D1BN8_44, D1BN8_79, D1BOI_63, D1BOV_56, D1BOX_4A, D1BOY_4A, D1BR9_68, D1BUK_16, D1BUK_63, D1BUL_31, D1BUL_95, D1BUO_63, D1BZE_12, D1C18_67, D1C5I_68, D1CXA_63, D1CXB_63, D1CXC_63, D1D72_44, D1D72_45, D1D72_46, D1D72_49, D1D72_4A, D1D72_55, D1D72_92, D1D72_96, D1D72_9A, D1D73_01, D1D74_01, D1D75_01, D1D76_01, D1DOO_63, D1E0K_11, D1E0K_12, D1E0K_13, D1E0K_38, D1E10_11, D1E10_13, D1E11_11, D1E11_13, D1E12_11, D1E12_13, D1E13_11, D1E13_13, D1E14_11, D1E14_13, D1E15_11, D1E15_13, D1E16_11, D1E16_13, D1E17_11, D1E17_13, D1E18_11, D1E18_13, D1E19_11, D1E19_13, D1E1Q_11, D1E1Q_12, D1E1R_11, D1E1R_12, D1E1S_11, D1E1S_12, D1E1S_13, D1E1S_38, D1E1T_11, D1E1T_12, D1E1T_13, D1E1U_11, D1E1U_12, D1E1U_13, D1E1V_11, D1E1V_12, D1E1V_13, D1E1W_11, D1E1W_12, D1E1W_13, D1E1X_11, D1E1X_13, D1E1Y_11, D1E1Y_13, D1E1Z_11, D1E1Z_13, D1E2A_11, D1E2A_13, D1E2B_11, D1E2B_13, D1E2C_11, D1E2C_13, D1E2G_12, D1E2G_14, D1E2H_12, D1E2H_14, D1E2H_38, D1E2I_12, D1E2I_14, D1E2I_38, D1E2L_29, D1E2M_29, D1E4Q_88, D1E8B_01, D1F0B_16, D1F0B_17, D1F0B_44, D1F0B_45, D1F0B_46, D1F0B_49, D1F0B_94, D1F0C_87, D1F0O_1E, D1F1A_11, D1F1A_13, D1F1B_11, D1F1B_13, D1F1C_11, D1F1C_13, D1F1F_87, D1FM3_11, D1FM3_12, D1FM3_13, D1FM3_98, D1FM4_11, D1FM4_12, D1FM4_13, D1FM5_11, D1FM5_12, D1FM5_13, D1FM6_11, D1FM6_12, D1FM6_13, D1FM7_11, D1FM7_12, D1FM7_13, D1FZ9_1E] */
  { /*     5 */              -127,             127,               127,            -128, DEM_CFG_DEM_CFG_DEBOUNCETYPE_COUNTER_EVENTDEBOUNCEALGORITHMOFDEBOUNCETABLE,      0x02u },  /* [D1A6D_88, D1A6E_88, D1A6F_88, D1A6H_88, D1A6L_88, D1A8N_11, D1A8O_11, D1BN1_16, D1BN1_17, D1BN1_44, D1BN1_45, D1BN1_46, D1BN1_94, D1BN2_16, D1BN2_17, D1BN2_44, D1BN2_45, D1BN2_46, D1BN2_94, D1BN4_16, D1BN4_17, D1BN4_44, D1BN4_45, D1BN4_46, D1BN4_49, D1BN4_94, D1BN9_16, D1BN9_17, D1BN9_44, D1BN9_45, D1BN9_46, D1BN9_49, D1BN9_94, D1BOG_16, D1BOG_17, D1BOG_46, D1BOG_94, D1BOH_16, D1BOH_17, D1BOH_44, D1BOH_45, D1BOH_46, D1BOH_49, D1BOH_94, D1BOI_16, D1BOI_17, D1BOI_46, D1BOI_94, D1BOV_63] */
  { /*     6 */                -1,             127,                 1,            -128, DEM_CFG_DEM_CFG_DEBOUNCETYPE_COUNTER_EVENTDEBOUNCEALGORITHMOFDEBOUNCETABLE,      0x06u },  /* [AutoCreatedDemEvent_ALLOW_COM_CN_FMSNet_fce1aae5_BswMReportFailToDemRef, AutoCreatedDemEvent_AdcDemEventParameterRefs_ADC_E_TIMEOUT, AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFa_540060b1, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone1, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone2, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_CabSubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_FMSNet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_SecuritySubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone1, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone2, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_CabSubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_FMSNet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_SecuritySubnet, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef_1, 
            AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_2, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN01_5bde9749_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_PostRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ESH_WaitForNvm_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_EcuMClearValidatedWakeupEvents_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_EcuMGoToSelectedShutdownTarget_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_OnEnterPostRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_OnEnterShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterWaitForNvm_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SwitchPostRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_PostRunNested_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_PostRunToPrepNested_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_RunToPostRunNested_BswMReportFailToDemRef, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_ALL_RUN_REQUESTS_KILLED, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_CONFIGURATION_DATA_INCONSISTENT, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_IMPROPER_CALLER, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_RAM_CHECK_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_COMPARE_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_ERASE_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_READ_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_UNEXPECTED_FLASH_ID, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_WRITE_FAILED, AutoCreatedDemEvent_GptDemEventParameterRefs_GPT_E_TIMEOUT, AutoCreatedDemEvent_INIT_Action_CanIf_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_CanNm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_CanSM_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_CanTp_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Can_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_ComM_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Com_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Dcm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_EnableInterrupts_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Issm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_J1939Nm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_J1939Rm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_J1939Tp_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_LinIf_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_LinSM_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_LinTp_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Lin_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Nm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_NvMReadAll_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_PduR_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_RteStart_BswMReportFailToDemRef, AutoCreatedDemEvent_J1939NmDemEventParameterRefs_J1939NM_E_ADDRESS_LOST, AutoCreatedDemEvent_J1939TpConfiguration_J1939TP_E_COMMUNICATION, AutoCreatedDemEvent_LinDemEventParameterRefs_LIN_E_TIMEOUT, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_BUS, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_DESCRIPTOR, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_ECC, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_INCONSISTENCY, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_PRIORITY, 
            AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_UNRECOGNIZED, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_CLOCK_FAILURE, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_INVALIDMODE_CONFIG, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_SSCM_CER_FAILURE, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_TIMEOUT_FAILURE, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_INTEGRITY_FAILED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_LOSS_OF_REDUNDANCY, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_QUEUE_OVERFLOW, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_REQ_FAILED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_VERIFY_FAILED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRITE_PROTECTED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRONG_BLOCK_ID, AutoCreatedDemEvent_RamTstDemEventParameterRefs_RAMTST_E_RAM_FAILURE, AutoCreatedDemEvent_SpiDemEventParameterRefs_SPI_E_HARDWARE_ERROR, AutoCreatedDemEvent_SpiDriver_SPI_E_HARDWARE_ERROR, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_CORRUPT_CONFIG, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_DISABLE_REJECTED, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_FORBIDDEN_INVOCATION, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_CALL, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_PARAMETER, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_MODE_FAILED, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_UNLOCKED, DEM_EVENT_StartApplication, DemEventParameter] */
  { /*     7 */                 0,               0,                 0,               0, DEM_CFG_DEM_CFG_DEBOUNCETYPE_INVALID_EVENTDEBOUNCEALGORITHMOFDEBOUNCETABLE,      0x00u }   /* [#EVENT_INVALID] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DtcGroupMask
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DtcGroupMask
  \brief  Mapping of configured DemGroupOfDTC/DemGroupDTCs ('UdsGroupDtc') to the internal used mask value ('GroupMask'). This table is sorted by the GroupDTC number.
  \details
  Element        Description
  UdsGroupDtc    configuration value Dem/DemGeneral/DemGroupOfDTC/DemGroupDTCs
  GroupMask      internally used mask for the DTC group, see defines DEM_CFG_GROUPMASK_POWERTRAIN, ~_CHASSIS, ~_BODY, ~_NETWORK
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DtcGroupMaskType, DEM_CONST) Dem_Cfg_DtcGroupMask[4] = {
    /* Index    UdsGroupDtc   GroupMask                           Comment */
  { /*     0 */ 0x00000001uL, DEM_CFG_GROUPMASK_POWERTRAIN },  /* [GroupDTC 0x000001: DEM_DTC_GROUP_POWERTRAIN_DTCS] */
  { /*     1 */ 0x00400000uL, DEM_CFG_GROUPMASK_CHASSIS    },  /* [GroupDTC 0x400000: DEM_DTC_GROUP_CHASSIS_DTCS] */
  { /*     2 */ 0x00800000uL, DEM_CFG_GROUPMASK_BODY       },  /* [GroupDTC 0x800000: DEM_DTC_GROUP_BODY_DTCS] */
  { /*     3 */ 0x00C00000uL, DEM_CFG_GROUPMASK_NETWORK    }   /* [GroupDTC 0xC00000: DEM_DTC_GROUP_NETWORK_COM_DTCS] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DtcTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DtcTable
  \details
  Element           Description
  UdsDtc        
  FunctionalUnit
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_DtcTableType, DEM_CONST) Dem_Cfg_DtcTable[244] = {
    /* Index    UdsDtc        FunctionalUnit        Referable Keys */
  { /*     0 */ 0x00FFFFFFuL,           255u },  /* [#NoUdsDtcConfigured, #NoObdDtcConfigured, #NoJ1939DtcConfigured] */
  { /*     1 */ 0x00482BFFuL,             0u },  /* [DemDTCClass_StartApplication] */
  { /*     2 */ 0x00D14288uL,             0u },  /* [DTCClass_D1BJO_88] */
  { /*     3 */ 0x00D14388uL,             0u },  /* [DTCClass_D1BJP_88] */
  { /*     4 */ 0x00D14488uL,             0u },  /* [DTCClass_D1BJQ_88] */
  { /*     5 */ 0x00D14588uL,             0u },  /* [DTCClass_D1BJR_88] */
  { /*     6 */ 0x00D14688uL,             0u },  /* [DTCClass_D1BJS_88] */
  { /*     7 */ 0x00D14787uL,             0u },  /* [DTCClass_D1BKB_87] */
  { /*     8 */ 0x00D14887uL,             0u },  /* [DTCClass_D1BKC_87] */
  { /*     9 */ 0x00D14987uL,             0u },  /* [DTCClass_D1BKD_87] */
  { /*    10 */ 0x00D14A87uL,             0u },  /* [DTCClass_D1BKE_87] */
  { /*    11 */ 0x00D14B87uL,             0u },  /* [DTCClass_D1BKF_87] */
  { /*    12 */ 0x00D14D87uL,             0u },  /* [DTCClass_D1BKH_87] */
  { /*    13 */ 0x00950216uL,             0u },  /* [DTCClass_D1BN1_16] */
  { /*    14 */ 0x00950217uL,             0u },  /* [DTCClass_D1BN1_17] */
  { /*    15 */ 0x00950244uL,             0u },  /* [DTCClass_D1BN1_44] */
  { /*    16 */ 0x00950245uL,             0u },  /* [DTCClass_D1BN1_45] */
  { /*    17 */ 0x00950246uL,             0u },  /* [DTCClass_D1BN1_46] */
  { /*    18 */ 0x00950294uL,             0u },  /* [DTCClass_D1BN1_94] */
  { /*    19 */ 0x00950316uL,             0u },  /* [DTCClass_D1BN2_16] */
  { /*    20 */ 0x00950317uL,             0u },  /* [DTCClass_D1BN2_17] */
  { /*    21 */ 0x00950344uL,             0u },  /* [DTCClass_D1BN2_44] */
  { /*    22 */ 0x00950345uL,             0u },  /* [DTCClass_D1BN2_45] */
  { /*    23 */ 0x00950346uL,             0u },  /* [DTCClass_D1BN2_46] */
  { /*    24 */ 0x00950394uL,             0u },  /* [DTCClass_D1BN2_94] */
  { /*    25 */ 0x00950516uL,             0u },  /* [DTCClass_D1BN4_16] */
  { /*    26 */ 0x00950517uL,             0u },  /* [DTCClass_D1BN4_17] */
  { /*    27 */ 0x00950544uL,             0u },  /* [DTCClass_D1BN4_44] */
  { /*    28 */ 0x00950545uL,             0u },  /* [DTCClass_D1BN4_45] */
  { /*    29 */ 0x00950546uL,             0u },  /* [DTCClass_D1BN4_46] */
  { /*    30 */ 0x00950549uL,             0u },  /* [DTCClass_D1BN4_49] */
  { /*    31 */ 0x00950594uL,             0u },  /* [DTCClass_D1BN4_94] */
  { /*    32 */ 0x00950616uL,             0u },  /* [DTCClass_D1BN9_16] */
  { /*    33 */ 0x00950617uL,             0u },  /* [DTCClass_D1BN9_17] */
  { /*    34 */ 0x00950644uL,             0u },  /* [DTCClass_D1BN9_44] */
  { /*    35 */ 0x00950645uL,             0u },  /* [DTCClass_D1BN9_45] */
  { /*    36 */ 0x00950646uL,             0u },  /* [DTCClass_D1BN9_46] */
  { /*    37 */ 0x00950649uL,             0u },  /* [DTCClass_D1BN9_49] */
  { /*    38 */ 0x00950694uL,             0u },  /* [DTCClass_D1BN9_94] */
  { /*    39 */ 0x00950816uL,             0u },  /* [DTCClass_D1BOG_16] */
  { /*    40 */ 0x00950817uL,             0u },  /* [DTCClass_D1BOG_17] */
  { /*    41 */ 0x00950846uL,             0u },  /* [DTCClass_D1BOG_46] */
  { /*    42 */ 0x00950894uL,             0u },  /* [DTCClass_D1BOG_94] */
  { /*    43 */ 0x00950716uL,             0u },  /* [DTCClass_D1BOH_16] */
  { /*    44 */ 0x00950717uL,             0u },  /* [DTCClass_D1BOH_17] */
  { /*    45 */ 0x00950744uL,             0u },  /* [DTCClass_D1BOH_44] */
  { /*    46 */ 0x00950745uL,             0u },  /* [DTCClass_D1BOH_45] */
  { /*    47 */ 0x00950746uL,             0u },  /* [DTCClass_D1BOH_46] */
  { /*    48 */ 0x00950749uL,             0u },  /* [DTCClass_D1BOH_49] */
  { /*    49 */ 0x00950794uL,             0u },  /* [DTCClass_D1BOH_94] */
    /* Index    UdsDtc        FunctionalUnit        Referable Keys */
  { /*    50 */ 0x00950416uL,             0u },  /* [DTCClass_D1BOI_16] */
  { /*    51 */ 0x00950417uL,             0u },  /* [DTCClass_D1BOI_17] */
  { /*    52 */ 0x00950446uL,             0u },  /* [DTCClass_D1BOI_46] */
  { /*    53 */ 0x00950463uL,             0u },  /* [DTCClass_D1BOI_63] */
  { /*    54 */ 0x00950494uL,             0u },  /* [DTCClass_D1BOI_94] */
  { /*    55 */ 0x00950C63uL,             0u },  /* [DTCClass_D1BOV_63] */
  { /*    56 */ 0x00951463uL,             0u },  /* [DTCClass_D1BUO_63] */
  { /*    57 */ 0x00951763uL,             0u },  /* [DTCClass_D1CXA_63] */
  { /*    58 */ 0x00951863uL,             0u },  /* [DTCClass_D1CXB_63] */
  { /*    59 */ 0x00951963uL,             0u },  /* [DTCClass_D1CXC_63] */
  { /*    60 */ 0x00951A63uL,             0u },  /* [DTCClass_D1DOO_63] */
  { /*    61 */ 0x00D14E87uL,             0u },  /* [DTCClass_D1BK9_87] */
  { /*    62 */ 0x00D14C87uL,             0u },  /* [DTCClass_D1BKG_87] */
  { /*    63 */ 0x00950916uL,             0u },  /* [DTCClass_D1BN8_16] */
  { /*    64 */ 0x00950917uL,             0u },  /* [DTCClass_D1BN8_17] */
  { /*    65 */ 0x00950944uL,             0u },  /* [DTCClass_D1BN8_44] */
  { /*    66 */ 0x00950979uL,             0u },  /* [DTCClass_D1BN8_79] */
  { /*    67 */ 0x00950A4AuL,             0u },  /* [DTCClass_D1BOX_4A] */
  { /*    68 */ 0x00950B4AuL,             0u },  /* [DTCClass_D1BOY_4A] */
  { /*    69 */ 0x00950E16uL,             0u },  /* [DTCClass_D1BUK_16] */
  { /*    70 */ 0x00950E63uL,             0u },  /* [DTCClass_D1BUK_63] */
  { /*    71 */ 0x00951031uL,             0u },  /* [DTCClass_D1BUL_31] */
  { /*    72 */ 0x00951095uL,             0u },  /* [DTCClass_D1BUL_95] */
  { /*    73 */ 0x00901016uL,             0u },  /* [DTCClass_D1F0B_16] */
  { /*    74 */ 0x00901017uL,             0u },  /* [DTCClass_D1F0B_17] */
  { /*    75 */ 0x00901044uL,             0u },  /* [DTCClass_D1F0B_44] */
  { /*    76 */ 0x00901045uL,             0u },  /* [DTCClass_D1F0B_45] */
  { /*    77 */ 0x00901046uL,             0u },  /* [DTCClass_D1F0B_46] */
  { /*    78 */ 0x00901049uL,             0u },  /* [DTCClass_D1F0B_49] */
  { /*    79 */ 0x00901094uL,             0u },  /* [DTCClass_D1F0B_94] */
  { /*    80 */ 0x00D00587uL,             0u },  /* [DTCClass_D1F0C_87] */
  { /*    81 */ 0x00C00188uL,             0u },  /* [DTCClass_D1A6D_88] */
  { /*    82 */ 0x00C01088uL,             0u },  /* [DTCClass_D1A6E_88] */
  { /*    83 */ 0x00C03788uL,             0u },  /* [DTCClass_D1A6F_88] */
  { /*    84 */ 0x00C05588uL,             0u },  /* [DTCClass_D1A6H_88] */
  { /*    85 */ 0x00E00388uL,             0u },  /* [DTCClass_D1A6L_88] */
  { /*    86 */ 0x00C10167uL,             0u },  /* [DTCClass_D1A7X_67] */
  { /*    87 */ 0x00D00667uL,             0u },  /* [DTCClass_D1ACK_67] */
  { /*    88 */ 0x00D00767uL,             0u },  /* [DTCClass_D1ACM_67] */
  { /*    89 */ 0x00D00867uL,             0u },  /* [DTCClass_D1ACN_67] */
  { /*    90 */ 0x00F0002FuL,             0u },  /* [DTCClass_D1AD0_2F] */
  { /*    91 */ 0x00F0004AuL,             0u },  /* [DTCClass_D1AD0_4A] */
  { /*    92 */ 0x00F00005uL,             0u },  /* [DTCClass_D1AD0_05] */
  { /*    93 */ 0x00F00029uL,             0u },  /* [DTCClass_D1AD0_29] */
  { /*    94 */ 0x00F00041uL,             0u },  /* [DTCClass_D1AD0_41] */
  { /*    95 */ 0x00F00044uL,             0u },  /* [DTCClass_D1AD0_44] */
  { /*    96 */ 0x00F00045uL,             0u },  /* [DTCClass_D1AD0_45] */
  { /*    97 */ 0x00F00046uL,             0u },  /* [DTCClass_D1AD0_46] */
  { /*    98 */ 0x00F00047uL,             0u },  /* [DTCClass_D1AD0_47] */
  { /*    99 */ 0x00F00048uL,             0u },  /* [DTCClass_D1AD0_48] */
    /* Index    UdsDtc        FunctionalUnit        Referable Keys */
  { /*   100 */ 0x00F00049uL,             0u },  /* [DTCClass_D1AD0_49] */
  { /*   101 */ 0x00F00094uL,             0u },  /* [DTCClass_D1AD0_94] */
  { /*   102 */ 0x00F0031CuL,             0u },  /* [DTCClass_D1AD9_1C] */
  { /*   103 */ 0x00F00316uL,             0u },  /* [DTCClass_D1AD9_16] */
  { /*   104 */ 0x00F00317uL,             0u },  /* [DTCClass_D1AD9_17] */
  { /*   105 */ 0x00D14F68uL,             0u },  /* [DTCClass_D1BR9_68] */
  { /*   106 */ 0x00DFFF68uL,             0u },  /* [DTCClass_D1C5I_68] */
  { /*   107 */ 0x00D00967uL,             0u },  /* [DTCClass_D1C18_67] */
  { /*   108 */ 0x00901311uL,             0u },  /* [DTCClass_D1E0K_11] */
  { /*   109 */ 0x00901312uL,             0u },  /* [DTCClass_D1E0K_12] */
  { /*   110 */ 0x00901313uL,             0u },  /* [DTCClass_D1E0K_13] */
  { /*   111 */ 0x00901316uL,             0u },  /* [DTCClass_D1E0K_16] */
  { /*   112 */ 0x00901317uL,             0u },  /* [DTCClass_D1E0K_17] */
  { /*   113 */ 0x00901411uL,             0u },  /* [DTCClass_D1E1Q_11] */
  { /*   114 */ 0x00901412uL,             0u },  /* [DTCClass_D1E1Q_12] */
  { /*   115 */ 0x00901416uL,             0u },  /* [DTCClass_D1E1Q_16] */
  { /*   116 */ 0x00901417uL,             0u },  /* [DTCClass_D1E1Q_17] */
  { /*   117 */ 0x00901511uL,             0u },  /* [DTCClass_D1E1R_11] */
  { /*   118 */ 0x00901512uL,             0u },  /* [DTCClass_D1E1R_12] */
  { /*   119 */ 0x00901516uL,             0u },  /* [DTCClass_D1E1R_16] */
  { /*   120 */ 0x00901517uL,             0u },  /* [DTCClass_D1E1R_17] */
  { /*   121 */ 0x00901611uL,             0u },  /* [DTCClass_D1E1S_11] */
  { /*   122 */ 0x00901612uL,             0u },  /* [DTCClass_D1E1S_12] */
  { /*   123 */ 0x00901613uL,             0u },  /* [DTCClass_D1E1S_13] */
  { /*   124 */ 0x00901616uL,             0u },  /* [DTCClass_D1E1S_16] */
  { /*   125 */ 0x00901617uL,             0u },  /* [DTCClass_D1E1S_17] */
  { /*   126 */ 0x00901711uL,             0u },  /* [DTCClass_D1E1T_11] */
  { /*   127 */ 0x00901712uL,             0u },  /* [DTCClass_D1E1T_12] */
  { /*   128 */ 0x00901713uL,             0u },  /* [DTCClass_D1E1T_13] */
  { /*   129 */ 0x00901716uL,             0u },  /* [DTCClass_D1E1T_16] */
  { /*   130 */ 0x00901717uL,             0u },  /* [DTCClass_D1E1T_17] */
  { /*   131 */ 0x00901811uL,             0u },  /* [DTCClass_D1E1U_11] */
  { /*   132 */ 0x00901812uL,             0u },  /* [DTCClass_D1E1U_12] */
  { /*   133 */ 0x00901813uL,             0u },  /* [DTCClass_D1E1U_13] */
  { /*   134 */ 0x00901816uL,             0u },  /* [DTCClass_D1E1U_16] */
  { /*   135 */ 0x00901817uL,             0u },  /* [DTCClass_D1E1U_17] */
  { /*   136 */ 0x00901911uL,             0u },  /* [DTCClass_D1E1V_11] */
  { /*   137 */ 0x00901912uL,             0u },  /* [DTCClass_D1E1V_12] */
  { /*   138 */ 0x00901913uL,             0u },  /* [DTCClass_D1E1V_13] */
  { /*   139 */ 0x00901916uL,             0u },  /* [DTCClass_D1E1V_16] */
  { /*   140 */ 0x00901917uL,             0u },  /* [DTCClass_D1E1V_17] */
  { /*   141 */ 0x00901A11uL,             0u },  /* [DTCClass_D1E1W_11] */
  { /*   142 */ 0x00901A12uL,             0u },  /* [DTCClass_D1E1W_12] */
  { /*   143 */ 0x00901A13uL,             0u },  /* [DTCClass_D1E1W_13] */
  { /*   144 */ 0x00901A16uL,             0u },  /* [DTCClass_D1E1W_16] */
  { /*   145 */ 0x00901A17uL,             0u },  /* [DTCClass_D1E1W_17] */
  { /*   146 */ 0x00901B11uL,             0u },  /* [DTCClass_D1E1X_11] */
  { /*   147 */ 0x00901B13uL,             0u },  /* [DTCClass_D1E1X_13] */
  { /*   148 */ 0x00901C11uL,             0u },  /* [DTCClass_D1E1Y_11] */
  { /*   149 */ 0x00901C13uL,             0u },  /* [DTCClass_D1E1Y_13] */
    /* Index    UdsDtc        FunctionalUnit        Referable Keys */
  { /*   150 */ 0x00901D11uL,             0u },  /* [DTCClass_D1E1Z_11] */
  { /*   151 */ 0x00901D13uL,             0u },  /* [DTCClass_D1E1Z_13] */
  { /*   152 */ 0x00902811uL,             0u },  /* [DTCClass_D1E2A_11] */
  { /*   153 */ 0x00902813uL,             0u },  /* [DTCClass_D1E2A_13] */
  { /*   154 */ 0x00902911uL,             0u },  /* [DTCClass_D1E2B_11] */
  { /*   155 */ 0x00902913uL,             0u },  /* [DTCClass_D1E2B_13] */
  { /*   156 */ 0x00902A11uL,             0u },  /* [DTCClass_D1E2C_11] */
  { /*   157 */ 0x00902A13uL,             0u },  /* [DTCClass_D1E2C_13] */
  { /*   158 */ 0x00902B12uL,             0u },  /* [DTCClass_D1E2G_12] */
  { /*   159 */ 0x00902B16uL,             0u },  /* [DTCClass_D1E2G_16] */
  { /*   160 */ 0x00902B17uL,             0u },  /* [DTCClass_D1E2G_17] */
  { /*   161 */ 0x00902C12uL,             0u },  /* [DTCClass_D1E2H_12] */
  { /*   162 */ 0x00902C16uL,             0u },  /* [DTCClass_D1E2H_16] */
  { /*   163 */ 0x00902C17uL,             0u },  /* [DTCClass_D1E2H_17] */
  { /*   164 */ 0x00902D12uL,             0u },  /* [DTCClass_D1E2I_12] */
  { /*   165 */ 0x00902D16uL,             0u },  /* [DTCClass_D1E2I_16] */
  { /*   166 */ 0x00902D17uL,             0u },  /* [DTCClass_D1E2I_17] */
  { /*   167 */ 0x00902E29uL,             0u },  /* [DTCClass_D1E2L_29] */
  { /*   168 */ 0x00902F29uL,             0u },  /* [DTCClass_D1E2M_29] */
  { /*   169 */ 0x00D00A88uL,             0u },  /* [DTCClass_D1E4Q_88] */
  { /*   170 */ 0x00901E11uL,             0u },  /* [DTCClass_D1E10_11] */
  { /*   171 */ 0x00901E13uL,             0u },  /* [DTCClass_D1E10_13] */
  { /*   172 */ 0x00901F11uL,             0u },  /* [DTCClass_D1E11_11] */
  { /*   173 */ 0x00901F13uL,             0u },  /* [DTCClass_D1E11_13] */
  { /*   174 */ 0x00902011uL,             0u },  /* [DTCClass_D1E12_11] */
  { /*   175 */ 0x00902013uL,             0u },  /* [DTCClass_D1E12_13] */
  { /*   176 */ 0x00902111uL,             0u },  /* [DTCClass_D1E13_11] */
  { /*   177 */ 0x00902113uL,             0u },  /* [DTCClass_D1E13_13] */
  { /*   178 */ 0x00902211uL,             0u },  /* [DTCClass_D1E14_11] */
  { /*   179 */ 0x00902213uL,             0u },  /* [DTCClass_D1E14_13] */
  { /*   180 */ 0x00902311uL,             0u },  /* [DTCClass_D1E15_11] */
  { /*   181 */ 0x00902313uL,             0u },  /* [DTCClass_D1E15_13] */
  { /*   182 */ 0x00902411uL,             0u },  /* [DTCClass_D1E16_11] */
  { /*   183 */ 0x00902413uL,             0u },  /* [DTCClass_D1E16_13] */
  { /*   184 */ 0x00902511uL,             0u },  /* [DTCClass_D1E17_11] */
  { /*   185 */ 0x00902513uL,             0u },  /* [DTCClass_D1E17_13] */
  { /*   186 */ 0x00902611uL,             0u },  /* [DTCClass_D1E18_11] */
  { /*   187 */ 0x00902613uL,             0u },  /* [DTCClass_D1E18_13] */
  { /*   188 */ 0x00902711uL,             0u },  /* [DTCClass_D1E19_11] */
  { /*   189 */ 0x00902713uL,             0u },  /* [DTCClass_D1E19_13] */
  { /*   190 */ 0x00903511uL,             0u },  /* [DTCClass_D1F1A_11] */
  { /*   191 */ 0x00903513uL,             0u },  /* [DTCClass_D1F1A_13] */
  { /*   192 */ 0x00903611uL,             0u },  /* [DTCClass_D1F1B_11] */
  { /*   193 */ 0x00903613uL,             0u },  /* [DTCClass_D1F1B_13] */
  { /*   194 */ 0x00903711uL,             0u },  /* [DTCClass_D1F1C_11] */
  { /*   195 */ 0x00903713uL,             0u },  /* [DTCClass_D1F1C_13] */
  { /*   196 */ 0x00D00B87uL,             0u },  /* [DTCClass_D1F1F_87] */
  { /*   197 */ 0x00903011uL,             0u },  /* [DTCClass_D1FM3_11] */
  { /*   198 */ 0x00903012uL,             0u },  /* [DTCClass_D1FM3_12] */
  { /*   199 */ 0x00903013uL,             0u },  /* [DTCClass_D1FM3_13] */
    /* Index    UdsDtc        FunctionalUnit        Referable Keys */
  { /*   200 */ 0x00903098uL,             0u },  /* [DTCClass_D1FM3_98] */
  { /*   201 */ 0x00903111uL,             0u },  /* [DTCClass_D1FM4_11] */
  { /*   202 */ 0x00903112uL,             0u },  /* [DTCClass_D1FM4_12] */
  { /*   203 */ 0x00903113uL,             0u },  /* [DTCClass_D1FM4_13] */
  { /*   204 */ 0x00903211uL,             0u },  /* [DTCClass_D1FM5_11] */
  { /*   205 */ 0x00903212uL,             0u },  /* [DTCClass_D1FM5_12] */
  { /*   206 */ 0x00903213uL,             0u },  /* [DTCClass_D1FM5_13] */
  { /*   207 */ 0x00903311uL,             0u },  /* [DTCClass_D1FM6_11] */
  { /*   208 */ 0x00903312uL,             0u },  /* [DTCClass_D1FM6_12] */
  { /*   209 */ 0x00903313uL,             0u },  /* [DTCClass_D1FM6_13] */
  { /*   210 */ 0x00903411uL,             0u },  /* [DTCClass_D1FM7_11] */
  { /*   211 */ 0x00903412uL,             0u },  /* [DTCClass_D1FM7_12] */
  { /*   212 */ 0x00903413uL,             0u },  /* [DTCClass_D1FM7_13] */
  { /*   213 */ 0x00900111uL,             0u },  /* [DTCClass_D1A8N_11] */
  { /*   214 */ 0x00900211uL,             0u },  /* [DTCClass_D1A8O_11] */
  { /*   215 */ 0x0090121EuL,             0u },  /* [DTCClass_D1F0O_1E] */
  { /*   216 */ 0x0090111EuL,             0u },  /* [DTCClass_D1FZ9_1E] */
  { /*   217 */ 0x00951212uL,             0u },  /* [DTCClass_D1BUP_12] */
  { /*   218 */ 0x00900812uL,             0u },  /* [DTCClass_D1BZE_12] */
  { /*   219 */ 0x00F0001CuL,             0u },  /* [DTCClass_D1AD0_1C] */
  { /*   220 */ 0x00950C56uL,             0u },  /* [DTCClass_D1BOV_56] */
  { /*   221 */ 0x00901338uL,             0u },  /* [DTCClass_D1E0K_38] */
  { /*   222 */ 0x00901419uL,             0u },  /* [DTCClass_D1E1Q_19] */
  { /*   223 */ 0x00901519uL,             0u },  /* [DTCClass_D1E1R_19] */
  { /*   224 */ 0x00901638uL,             0u },  /* [DTCClass_D1E1S_38] */
  { /*   225 */ 0x00902C38uL,             0u },  /* [DTCClass_D1E2H_38] */
  { /*   226 */ 0x00902D38uL,             0u },  /* [DTCClass_D1E2I_38] */
  { /*   227 */ 0x00902B14uL,             0u },  /* [DTCClass_D1E2G_14] */
  { /*   228 */ 0x00902C14uL,             0u },  /* [DTCClass_D1E2H_14] */
  { /*   229 */ 0x00902D14uL,             0u },  /* [DTCClass_D1E2I_14] */
  { /*   230 */ 0x00A0004AuL,             0u },  /* [DTCClass_D1D72_4A] */
  { /*   231 */ 0x00A0009AuL,             0u },  /* [DTCClass_D1D72_9A] */
  { /*   232 */ 0x00A00044uL,             0u },  /* [DTCClass_D1D72_44] */
  { /*   233 */ 0x00A00045uL,             0u },  /* [DTCClass_D1D72_45] */
  { /*   234 */ 0x00A00046uL,             0u },  /* [DTCClass_D1D72_46] */
  { /*   235 */ 0x00A00049uL,             0u },  /* [DTCClass_D1D72_49] */
  { /*   236 */ 0x00A00055uL,             0u },  /* [DTCClass_D1D72_55] */
  { /*   237 */ 0x00A00092uL,             0u },  /* [DTCClass_D1D72_92] */
  { /*   238 */ 0x00A00096uL,             0u },  /* [DTCClass_D1D72_96] */
  { /*   239 */ 0x00A00101uL,             0u },  /* [DTCClass_D1D73_01] */
  { /*   240 */ 0x00A00201uL,             0u },  /* [DTCClass_D1D74_01] */
  { /*   241 */ 0x00A00301uL,             0u },  /* [DTCClass_D1D75_01] */
  { /*   242 */ 0x00A00401uL,             0u },  /* [DTCClass_D1D76_01] */
  { /*   243 */ 0x00A00801uL,             0u }   /* [DTCClass_D1E8B_01] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionGroupTableInd
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionGroupTableInd
  \brief  the indexes of the 1:1 sorted relation pointing to Dem_Cfg_EnableConditionGroupTable
*/ 
#define DEM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_EnableConditionGroupTableIndType, DEM_CONST) Dem_Cfg_EnableConditionGroupTableInd[3] = {
  /* Index     EnableConditionGroupTableInd      Referable Keys */
  /*     0 */                            0u,  /* [__Internal_ControlDtcSetting] */
  /*     1 */                            1u,  /* [__Internal_ControlDtcSetting] */
  /*     2 */                            1u   /* [IgnitionNotCranking] */
};
#define DEM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionTable
  \brief  Map each EnableCondition(Id) to the referring EnableConditionGroups - this is reverse direction of the AUTOSAR configuration model.
  \details
  Element                                 Description
  EnableConditionGroupTableIndStartIdx    the start index of the 0:n relation pointing to Dem_Cfg_EnableConditionGroupTableInd
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_EnableConditionTableType, DEM_CONST) Dem_Cfg_EnableConditionTable[2] = {
    /* Index    EnableConditionGroupTableIndStartIdx */
  { /*     0 */                                   0u },
  { /*     1 */                                   2u }
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EventTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EventTable
  \details
  Element                         Description
  AgingCycleCounterThreshold      DemAgingCycleCounterThreshold of the DemEventParameter/DemEventClass, if AgingAllowedOfEventTable==FALSE we use '255' here, too.
  AgingCycleId                    DemOperationCycle (ID) referenced by DemEventParameter/DemEventClass/DemAgingCycleRef
  DebounceTableIdx                the index of the 1:1 relation pointing to Dem_Cfg_DebounceTable
  DtcTableIdx                     the index of the 1:1 relation pointing to Dem_Cfg_DtcTable
  EnableConditionGroupTableIdx    the index of the 1:1 relation pointing to Dem_Cfg_EnableConditionGroupTable
  EventKind                       DemEventKind of the DemEventParameter
  ExtendedDataTableIdx            the index of the 1:1 relation pointing to Dem_Cfg_ExtendedDataTable
  FreezeFrameNumTableEndIdx       the end index of the 0:n relation pointing to Dem_Cfg_FreezeFrameNumTable
  FreezeFrameNumTableStartIdx     the start index of the 0:n relation pointing to Dem_Cfg_FreezeFrameNumTable
  FreezeFrameTableStdFFIdx        the index of the 1:1 relation pointing to Dem_Cfg_FreezeFrameTable
  HealingTarget                   DemIndicatorHealingCycleCounterThreshold of the DemEventParameter/DemEventClass/DemIndicatorAttribute (for each event all attribute's value must be identical). Events without DemIndicatorAttribute have: 0.
  InitMonitorForEventIdx          the index of the 0:1 relation pointing to Dem_Cfg_InitMonitorForEvent
  MaskedBits                      contains bitcoded the boolean data of Dem_Cfg_AgingAllowedOfEventTable, Dem_Cfg_EventLatchTFOfEventTable, Dem_Cfg_FreezeFrameNumTableUsedOfEventTable, Dem_Cfg_InitMonitorForEventUsedOfEventTable, Dem_Cfg_NormalIndicatorTableUsedOfEventTable
  NormalIndicatorTableEndIdx      the end index of the 0:n relation pointing to Dem_Cfg_NormalIndicatorTable
  NormalIndicatorTableStartIdx    the start index of the 0:n relation pointing to Dem_Cfg_NormalIndicatorTable
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_EventTableType, DEM_CONST) Dem_Cfg_EventTable[441] = {
    /* Index    AgingCycleCounterThreshold  AgingCycleId                          DebounceTableIdx  DtcTableIdx  EnableConditionGroupTableIdx  EventKind                                         ExtendedDataTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  HealingTarget  InitMonitorForEventIdx  MaskedBits  NormalIndicatorTableEndIdx                         NormalIndicatorTableStartIdx                               Referable Keys */
  { /*     0 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [#EVENT_INVALID, Satellite#0] */
  { /*     1 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [DemEventParameter, Satellite#0] */
  { /*     2 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_AdcDemEventParameterRefs_ADC_E_TIMEOUT, Satellite#0] */
  { /*     3 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ALLOW_COM_CN_FMSNet_fce1aae5_BswMReportFailToDemRef, Satellite#0] */
  { /*     4 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  { /*     5 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  { /*     6 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  { /*     7 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  { /*     8 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  { /*     9 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  { /*    10 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, Satellite#0] */
  { /*    11 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  { /*    12 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, Satellite#0] */
  { /*    13 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  { /*    14 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, Satellite#0] */
  { /*    15 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  { /*    16 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, Satellite#0] */
  { /*    17 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, Satellite#0] */
  { /*    18 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, Satellite#0] */
  { /*    19 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, Satellite#0] */
  { /*    20 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, Satellite#0] */
  { /*    21 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, Satellite#0] */
  { /*    22 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, Satellite#0] */
  { /*    23 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, Satellite#0] */
  { /*    24 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, Satellite#0] */
  { /*    25 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, Satellite#0] */
  { /*    26 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, Satellite#0] */
  { /*    27 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, Satellite#0] */
  { /*    28 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, Satellite#0] */
  { /*    29 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  { /*    30 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, Satellite#0] */
  { /*    31 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  { /*    32 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  { /*    33 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  { /*    34 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  { /*    35 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  { /*    36 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  { /*    37 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, Satellite#0] */
  { /*    38 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  { /*    39 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, Satellite#0] */
  { /*    40 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  { /*    41 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, Satellite#0] */
  { /*    42 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  { /*    43 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, Satellite#0] */
  { /*    44 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, Satellite#0] */
  { /*    45 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, Satellite#0] */
  { /*    46 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, Satellite#0] */
  { /*    47 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, Satellite#0] */
  { /*    48 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, Satellite#0] */
  { /*    49 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, Satellite#0] */
    /* Index    AgingCycleCounterThreshold  AgingCycleId                          DebounceTableIdx  DtcTableIdx  EnableConditionGroupTableIdx  EventKind                                         ExtendedDataTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  HealingTarget  InitMonitorForEventIdx  MaskedBits  NormalIndicatorTableEndIdx                         NormalIndicatorTableStartIdx                               Referable Keys */
  { /*    50 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, Satellite#0] */
  { /*    51 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, Satellite#0] */
  { /*    52 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, Satellite#0] */
  { /*    53 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, Satellite#0] */
  { /*    54 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, Satellite#0] */
  { /*    55 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, Satellite#0] */
  { /*    56 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  { /*    57 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, Satellite#0] */
  { /*    58 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFa_540060b1, Satellite#0] */
  { /*    59 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, Satellite#0] */
  { /*    60 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFailToDemRef, Satellite#0] */
  { /*    61 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, Satellite#0] */
  { /*    62 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone1, Satellite#0] */
  { /*    63 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_SecuritySubnet, Satellite#0] */
  { /*    64 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone2, Satellite#0] */
  { /*    65 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_CabSubnet, Satellite#0] */
  { /*    66 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_FMSNet, Satellite#0] */
  { /*    67 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone1, Satellite#0] */
  { /*    68 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_SecuritySubnet, Satellite#0] */
  { /*    69 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone2, Satellite#0] */
  { /*    70 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_CabSubnet, Satellite#0] */
  { /*    71 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_FMSNet, Satellite#0] */
  { /*    72 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, Satellite#0] */
  { /*    73 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    74 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, Satellite#0] */
  { /*    75 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    76 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, Satellite#0] */
  { /*    77 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    78 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, Satellite#0] */
  { /*    79 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    80 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef, Satellite#0] */
  { /*    81 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    82 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, Satellite#0] */
  { /*    83 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    84 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, Satellite#0] */
  { /*    85 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    86 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, Satellite#0] */
  { /*    87 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    88 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, Satellite#0] */
  { /*    89 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    90 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef, Satellite#0] */
  { /*    91 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_1, Satellite#0] */
  { /*    92 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_2, Satellite#0] */
  { /*    93 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, Satellite#0] */
  { /*    94 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, Satellite#0] */
  { /*    95 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, Satellite#0] */
  { /*    96 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, Satellite#0] */
  { /*    97 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN01_5bde9749_BswMReportFailToDemRef, Satellite#0] */
  { /*    98 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, Satellite#0] */
  { /*    99 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, Satellite#0] */
    /* Index    AgingCycleCounterThreshold  AgingCycleId                          DebounceTableIdx  DtcTableIdx  EnableConditionGroupTableIdx  EventKind                                         ExtendedDataTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  HealingTarget  InitMonitorForEventIdx  MaskedBits  NormalIndicatorTableEndIdx                         NormalIndicatorTableStartIdx                               Referable Keys */
  { /*   100 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, Satellite#0] */
  { /*   101 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, Satellite#0] */
  { /*   102 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ESH_PostRun_BswMReportFailToDemRef, Satellite#0] */
  { /*   103 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef, Satellite#0] */
  { /*   104 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   105 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef, Satellite#0] */
  { /*   106 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   107 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ESH_WaitForNvm_BswMReportFailToDemRef, Satellite#0] */
  { /*   108 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef, Satellite#0] */
  { /*   109 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   110 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_EcuMClearValidatedWakeupEvents_BswMReportFailToDemRef, Satellite#0] */
  { /*   111 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_EcuMGoToSelectedShutdownTarget_BswMReportFailToDemRef, Satellite#0] */
  { /*   112 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef, Satellite#0] */
  { /*   113 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   114 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef, Satellite#0] */
  { /*   115 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   116 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_OnEnterPostRun_BswMReportFailToDemRef, Satellite#0] */
  { /*   117 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef, Satellite#0] */
  { /*   118 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   119 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef, Satellite#0] */
  { /*   120 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   121 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_OnEnterShutdown_BswMReportFailToDemRef, Satellite#0] */
  { /*   122 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_OnEnterWaitForNvm_BswMReportFailToDemRef, Satellite#0] */
  { /*   123 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef, Satellite#0] */
  { /*   124 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   125 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef, Satellite#0] */
  { /*   126 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   127 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_SwitchPostRun_BswMReportFailToDemRef, Satellite#0] */
  { /*   128 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef, Satellite#0] */
  { /*   129 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   130 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef, Satellite#0] */
  { /*   131 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   132 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef, Satellite#0] */
  { /*   133 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef_1, Satellite#0] */
  { /*   134 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_PostRunNested_BswMReportFailToDemRef, Satellite#0] */
  { /*   135 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_PostRunToPrepNested_BswMReportFailToDemRef, Satellite#0] */
  { /*   136 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_ESH_RunToPostRunNested_BswMReportFailToDemRef, Satellite#0] */
  { /*   137 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_ALL_RUN_REQUESTS_KILLED, Satellite#0] */
  { /*   138 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_CONFIGURATION_DATA_INCONSISTENT, Satellite#0] */
  { /*   139 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_IMPROPER_CALLER, Satellite#0] */
  { /*   140 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_RAM_CHECK_FAILED, Satellite#0] */
  { /*   141 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_GptDemEventParameterRefs_GPT_E_TIMEOUT, Satellite#0] */
  { /*   142 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_CanIf_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   143 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_CanNm_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   144 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_CanSM_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   145 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_CanTp_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   146 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_Can_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   147 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_ComM_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   148 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_Com_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   149 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_Dcm_Init_BswMReportFailToDemRef, Satellite#0] */
    /* Index    AgingCycleCounterThreshold  AgingCycleId                          DebounceTableIdx  DtcTableIdx  EnableConditionGroupTableIdx  EventKind                                         ExtendedDataTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  HealingTarget  InitMonitorForEventIdx  MaskedBits  NormalIndicatorTableEndIdx                         NormalIndicatorTableStartIdx                               Referable Keys */
  { /*   150 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_EnableInterrupts_BswMReportFailToDemRef, Satellite#0] */
  { /*   151 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_Issm_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   152 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_J1939Nm_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   153 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_J1939Rm_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   154 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_J1939Tp_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   155 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_LinIf_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   156 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_LinSM_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   157 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_LinTp_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   158 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_Lin_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   159 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_Nm_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   160 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_NvMReadAll_BswMReportFailToDemRef, Satellite#0] */
  { /*   161 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_PduR_Init_BswMReportFailToDemRef, Satellite#0] */
  { /*   162 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_INIT_Action_RteStart_BswMReportFailToDemRef, Satellite#0] */
  { /*   163 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_J1939NmDemEventParameterRefs_J1939NM_E_ADDRESS_LOST, Satellite#0] */
  { /*   164 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_J1939TpConfiguration_J1939TP_E_COMMUNICATION, Satellite#0] */
  { /*   165 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_LinDemEventParameterRefs_LIN_E_TIMEOUT, Satellite#0] */
  { /*   166 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_BUS, Satellite#0] */
  { /*   167 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_DESCRIPTOR, Satellite#0] */
  { /*   168 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_ECC, Satellite#0] */
  { /*   169 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_INCONSISTENCY, Satellite#0] */
  { /*   170 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_PRIORITY, Satellite#0] */
  { /*   171 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_UNRECOGNIZED, Satellite#0] */
  { /*   172 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_CLOCK_FAILURE, Satellite#0] */
  { /*   173 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_INVALIDMODE_CONFIG, Satellite#0] */
  { /*   174 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_SSCM_CER_FAILURE, Satellite#0] */
  { /*   175 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_TIMEOUT_FAILURE, Satellite#0] */
  { /*   176 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_SpiDemEventParameterRefs_SPI_E_HARDWARE_ERROR, Satellite#0] */
  { /*   177 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_SpiDriver_SPI_E_HARDWARE_ERROR, Satellite#0] */
  { /*   178 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_CORRUPT_CONFIG, Satellite#0] */
  { /*   179 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_DISABLE_REJECTED, Satellite#0] */
  { /*   180 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_FORBIDDEN_INVOCATION, Satellite#0] */
  { /*   181 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_CALL, Satellite#0] */
  { /*   182 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_PARAMETER, Satellite#0] */
  { /*   183 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_MODE_FAILED, Satellite#0] */
  { /*   184 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_UNLOCKED, Satellite#0] */
  { /*   185 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_INTEGRITY_FAILED, Satellite#0] */
  { /*   186 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_LOSS_OF_REDUNDANCY, Satellite#0] */
  { /*   187 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_QUEUE_OVERFLOW, Satellite#0] */
  { /*   188 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_REQ_FAILED, Satellite#0] */
  { /*   189 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_VERIFY_FAILED, Satellite#0] */
  { /*   190 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRITE_PROTECTED, Satellite#0] */
  { /*   191 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRONG_BLOCK_ID, Satellite#0] */
  { /*   192 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_COMPARE_FAILED, Satellite#0] */
  { /*   193 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_ERASE_FAILED, Satellite#0] */
  { /*   194 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_READ_FAILED, Satellite#0] */
  { /*   195 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_UNEXPECTED_FLASH_ID, Satellite#0] */
  { /*   196 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_WRITE_FAILED, Satellite#0] */
  { /*   197 */                       255u, DemConf_DemOperationCycle_PowerCycle,               6u,          1u,                           0u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   0u,                                               1u,                                                 0u,                       1u,            0u,                     0u,      0x06u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [DEM_EVENT_StartApplication, Satellite#0] */
  { /*   198 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,          2u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BJO_88, Satellite#0] */
  { /*   199 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,          3u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BJP_88, Satellite#0] */
    /* Index    AgingCycleCounterThreshold  AgingCycleId                          DebounceTableIdx  DtcTableIdx  EnableConditionGroupTableIdx  EventKind                                         ExtendedDataTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  HealingTarget  InitMonitorForEventIdx  MaskedBits  NormalIndicatorTableEndIdx                         NormalIndicatorTableStartIdx                               Referable Keys */
  { /*   200 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,          4u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BJQ_88, Satellite#0] */
  { /*   201 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,          5u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BJR_88, Satellite#0] */
  { /*   202 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,          6u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BJS_88, Satellite#0] */
  { /*   203 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,          7u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BKB_87, Satellite#0] */
  { /*   204 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,          8u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BKC_87, Satellite#0] */
  { /*   205 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,          9u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BKD_87, Satellite#0] */
  { /*   206 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         10u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BKE_87, Satellite#0] */
  { /*   207 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         11u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BKF_87, Satellite#0] */
  { /*   208 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         12u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BKH_87, Satellite#0] */
  { /*   209 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         13u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN1_16, Satellite#0] */
  { /*   210 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         14u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN1_17, Satellite#0] */
  { /*   211 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         15u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN1_44, Satellite#0] */
  { /*   212 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         16u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN1_45, Satellite#0] */
  { /*   213 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         17u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN1_46, Satellite#0] */
  { /*   214 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         18u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       9u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN1_94, Satellite#0] */
  { /*   215 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         19u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN2_16, Satellite#0] */
  { /*   216 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         20u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN2_17, Satellite#0] */
  { /*   217 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         21u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN2_44, Satellite#0] */
  { /*   218 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         22u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN2_45, Satellite#0] */
  { /*   219 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         23u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN2_46, Satellite#0] */
  { /*   220 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         24u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       9u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN2_94, Satellite#0] */
  { /*   221 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         25u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN4_16, Satellite#0] */
  { /*   222 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         26u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN4_17, Satellite#0] */
  { /*   223 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         27u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN4_44, Satellite#0] */
  { /*   224 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         28u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN4_45, Satellite#0] */
  { /*   225 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         29u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN4_46, Satellite#0] */
  { /*   226 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         30u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN4_49, Satellite#0] */
  { /*   227 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         31u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       9u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN4_94, Satellite#0] */
  { /*   228 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         32u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN9_16, Satellite#0] */
  { /*   229 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         33u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN9_17, Satellite#0] */
  { /*   230 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         34u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN9_44, Satellite#0] */
  { /*   231 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         35u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN9_45, Satellite#0] */
  { /*   232 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         36u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN9_46, Satellite#0] */
  { /*   233 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         37u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN9_49, Satellite#0] */
  { /*   234 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         38u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       9u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN9_94, Satellite#0] */
  { /*   235 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         39u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOG_16, Satellite#0] */
  { /*   236 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         40u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOG_17, Satellite#0] */
  { /*   237 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         41u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOG_46, Satellite#0] */
  { /*   238 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         42u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOG_94, Satellite#0] */
  { /*   239 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         43u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOH_16, Satellite#0] */
  { /*   240 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         44u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOH_17, Satellite#0] */
  { /*   241 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         45u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOH_44, Satellite#0] */
  { /*   242 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         46u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOH_45, Satellite#0] */
  { /*   243 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         47u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOH_46, Satellite#0] */
  { /*   244 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         48u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOH_49, Satellite#0] */
  { /*   245 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         49u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOH_94, Satellite#0] */
  { /*   246 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         50u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOI_16, Satellite#0] */
  { /*   247 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         51u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOI_17, Satellite#0] */
  { /*   248 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         52u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOI_46, Satellite#0] */
  { /*   249 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         53u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOI_63, Satellite#0] */
    /* Index    AgingCycleCounterThreshold  AgingCycleId                          DebounceTableIdx  DtcTableIdx  EnableConditionGroupTableIdx  EventKind                                         ExtendedDataTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  HealingTarget  InitMonitorForEventIdx  MaskedBits  NormalIndicatorTableEndIdx                         NormalIndicatorTableStartIdx                               Referable Keys */
  { /*   250 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,         54u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOI_94, Satellite#0] */
  { /*   251 */                        80u, DemConf_DemOperationCycle_PowerCycle,               5u,         55u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOV_63, Satellite#0] */
  { /*   252 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         56u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BUO_63, Satellite#0] */
  { /*   253 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         57u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1CXA_63, Satellite#0] */
  { /*   254 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         58u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1CXB_63, Satellite#0] */
  { /*   255 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         59u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1CXC_63, Satellite#0] */
  { /*   256 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         60u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1DOO_63, Satellite#0] */
  { /*   257 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         61u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       6u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BK9_87, Satellite#0] */
  { /*   258 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         62u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BKG_87, Satellite#0] */
  { /*   259 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         63u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN8_16, Satellite#0] */
  { /*   260 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         64u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN8_17, Satellite#0] */
  { /*   261 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         65u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN8_44, Satellite#0] */
  { /*   262 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         66u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       7u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BN8_79, Satellite#0] */
  { /*   263 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         67u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       7u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOX_4A, Satellite#0] */
  { /*   264 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         68u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       7u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOY_4A, Satellite#0] */
  { /*   265 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         69u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BUK_16, Satellite#0] */
  { /*   266 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         70u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BUK_63, Satellite#0] */
  { /*   267 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         71u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     1u,      0x17u,                                                1u,                                                  0u },  /* [D1BUL_31, Satellite#0] */
  { /*   268 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         72u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     2u,      0x17u,                                                1u,                                                  0u },  /* [D1BUL_95, Satellite#0] */
  { /*   269 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         73u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F0B_16, Satellite#0] */
  { /*   270 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         74u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F0B_17, Satellite#0] */
  { /*   271 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         75u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F0B_44, Satellite#0] */
  { /*   272 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         76u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F0B_45, Satellite#0] */
  { /*   273 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         77u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F0B_46, Satellite#0] */
  { /*   274 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         78u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F0B_49, Satellite#0] */
  { /*   275 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,         79u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F0B_94, Satellite#0] */
  { /*   276 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         80u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F0C_87, Satellite#0] */
  { /*   277 */                        80u, DemConf_DemOperationCycle_PowerCycle,               5u,         81u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1A6D_88, Satellite#0] */
  { /*   278 */                        80u, DemConf_DemOperationCycle_PowerCycle,               5u,         82u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1A6E_88, Satellite#0] */
  { /*   279 */                        80u, DemConf_DemOperationCycle_PowerCycle,               5u,         83u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1A6F_88, Satellite#0] */
  { /*   280 */                        80u, DemConf_DemOperationCycle_PowerCycle,               5u,         84u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1A6H_88, Satellite#0] */
  { /*   281 */                        80u, DemConf_DemOperationCycle_PowerCycle,               5u,         85u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1A6L_88, Satellite#0] */
  { /*   282 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         86u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1A7X_67, Satellite#0] */
  { /*   283 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         87u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1ACK_67, Satellite#0] */
  { /*   284 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         88u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1ACM_67, Satellite#0] */
  { /*   285 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         89u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1ACN_67, Satellite#0] */
  { /*   286 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         90u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_2F, Satellite#0] */
  { /*   287 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         91u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_4A, Satellite#0] */
  { /*   288 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         92u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_05, Satellite#0] */
  { /*   289 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         93u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_29, Satellite#0] */
  { /*   290 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         94u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       2u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_41, Satellite#0] */
  { /*   291 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         95u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       2u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_44, Satellite#0] */
  { /*   292 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         96u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       2u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_45, Satellite#0] */
  { /*   293 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         97u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       2u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_46, Satellite#0] */
  { /*   294 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         98u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       5u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_47, Satellite#0] */
  { /*   295 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,         99u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_48, Satellite#0] */
  { /*   296 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        100u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       8u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_49, Satellite#0] */
  { /*   297 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        101u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_94, Satellite#0] */
  { /*   298 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        102u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD9_1C, Satellite#0] */
  { /*   299 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        103u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD9_16, Satellite#0] */
    /* Index    AgingCycleCounterThreshold  AgingCycleId                          DebounceTableIdx  DtcTableIdx  EnableConditionGroupTableIdx  EventKind                                         ExtendedDataTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  HealingTarget  InitMonitorForEventIdx  MaskedBits  NormalIndicatorTableEndIdx                         NormalIndicatorTableStartIdx                               Referable Keys */
  { /*   300 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        104u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD9_17, Satellite#0] */
  { /*   301 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        105u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BR9_68, Satellite#0] */
  { /*   302 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        106u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1C5I_68, Satellite#0] */
  { /*   303 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        107u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1C18_67, Satellite#0] */
  { /*   304 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        108u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E0K_11, Satellite#0] */
  { /*   305 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        109u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E0K_12, Satellite#0] */
  { /*   306 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        110u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E0K_13, Satellite#0] */
  { /*   307 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        111u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E0K_16, Satellite#0] */
  { /*   308 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        112u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E0K_17, Satellite#0] */
  { /*   309 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        113u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1Q_11, Satellite#0] */
  { /*   310 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        114u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1Q_12, Satellite#0] */
  { /*   311 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        115u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1Q_16, Satellite#0] */
  { /*   312 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        116u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1Q_17, Satellite#0] */
  { /*   313 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        117u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1R_11, Satellite#0] */
  { /*   314 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        118u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1R_12, Satellite#0] */
  { /*   315 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        119u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1R_16, Satellite#0] */
  { /*   316 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        120u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1R_17, Satellite#0] */
  { /*   317 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        121u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1S_11, Satellite#0] */
  { /*   318 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        122u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1S_12, Satellite#0] */
  { /*   319 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        123u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1S_13, Satellite#0] */
  { /*   320 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        124u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1S_16, Satellite#0] */
  { /*   321 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        125u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1S_17, Satellite#0] */
  { /*   322 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        126u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1T_11, Satellite#0] */
  { /*   323 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        127u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1T_12, Satellite#0] */
  { /*   324 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        128u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1T_13, Satellite#0] */
  { /*   325 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        129u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1T_16, Satellite#0] */
  { /*   326 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        130u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1T_17, Satellite#0] */
  { /*   327 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        131u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1U_11, Satellite#0] */
  { /*   328 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        132u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1U_12, Satellite#0] */
  { /*   329 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        133u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1U_13, Satellite#0] */
  { /*   330 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        134u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1U_16, Satellite#0] */
  { /*   331 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        135u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1U_17, Satellite#0] */
  { /*   332 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        136u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1V_11, Satellite#0] */
  { /*   333 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        137u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1V_12, Satellite#0] */
  { /*   334 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        138u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1V_13, Satellite#0] */
  { /*   335 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        139u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1V_16, Satellite#0] */
  { /*   336 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        140u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1V_17, Satellite#0] */
  { /*   337 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        141u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1W_11, Satellite#0] */
  { /*   338 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        142u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1W_12, Satellite#0] */
  { /*   339 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        143u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1W_13, Satellite#0] */
  { /*   340 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        144u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1W_16, Satellite#0] */
  { /*   341 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        145u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1W_17, Satellite#0] */
  { /*   342 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        146u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1X_11, Satellite#0] */
  { /*   343 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        147u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1X_13, Satellite#0] */
  { /*   344 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        148u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1Y_11, Satellite#0] */
  { /*   345 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        149u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1Y_13, Satellite#0] */
  { /*   346 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        150u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1Z_11, Satellite#0] */
  { /*   347 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        151u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1Z_13, Satellite#0] */
  { /*   348 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        152u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2A_11, Satellite#0] */
  { /*   349 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        153u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2A_13, Satellite#0] */
    /* Index    AgingCycleCounterThreshold  AgingCycleId                          DebounceTableIdx  DtcTableIdx  EnableConditionGroupTableIdx  EventKind                                         ExtendedDataTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  HealingTarget  InitMonitorForEventIdx  MaskedBits  NormalIndicatorTableEndIdx                         NormalIndicatorTableStartIdx                               Referable Keys */
  { /*   350 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        154u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2B_11, Satellite#0] */
  { /*   351 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        155u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2B_13, Satellite#0] */
  { /*   352 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        156u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2C_11, Satellite#0] */
  { /*   353 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        157u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2C_13, Satellite#0] */
  { /*   354 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        158u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2G_12, Satellite#0] */
  { /*   355 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        159u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2G_16, Satellite#0] */
  { /*   356 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        160u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2G_17, Satellite#0] */
  { /*   357 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        161u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2H_12, Satellite#0] */
  { /*   358 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        162u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2H_16, Satellite#0] */
  { /*   359 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        163u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2H_17, Satellite#0] */
  { /*   360 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        164u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2I_12, Satellite#0] */
  { /*   361 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        165u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2I_16, Satellite#0] */
  { /*   362 */                        80u, DemConf_DemOperationCycle_PowerCycle,               1u,        166u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2I_17, Satellite#0] */
  { /*   363 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        167u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2L_29, Satellite#0] */
  { /*   364 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        168u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2M_29, Satellite#0] */
  { /*   365 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        169u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E4Q_88, Satellite#0] */
  { /*   366 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        170u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E10_11, Satellite#0] */
  { /*   367 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        171u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E10_13, Satellite#0] */
  { /*   368 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        172u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E11_11, Satellite#0] */
  { /*   369 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        173u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E11_13, Satellite#0] */
  { /*   370 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        174u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E12_11, Satellite#0] */
  { /*   371 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        175u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E12_13, Satellite#0] */
  { /*   372 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        176u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E13_11, Satellite#0] */
  { /*   373 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        177u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E13_13, Satellite#0] */
  { /*   374 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        178u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E14_11, Satellite#0] */
  { /*   375 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        179u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E14_13, Satellite#0] */
  { /*   376 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        180u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E15_11, Satellite#0] */
  { /*   377 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        181u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E15_13, Satellite#0] */
  { /*   378 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        182u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E16_11, Satellite#0] */
  { /*   379 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        183u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E16_13, Satellite#0] */
  { /*   380 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        184u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E17_11, Satellite#0] */
  { /*   381 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        185u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E17_13, Satellite#0] */
  { /*   382 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        186u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E18_11, Satellite#0] */
  { /*   383 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        187u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E18_13, Satellite#0] */
  { /*   384 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        188u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E19_11, Satellite#0] */
  { /*   385 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        189u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E19_13, Satellite#0] */
  { /*   386 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        190u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F1A_11, Satellite#0] */
  { /*   387 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        191u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F1A_13, Satellite#0] */
  { /*   388 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        192u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F1B_11, Satellite#0] */
  { /*   389 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        193u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F1B_13, Satellite#0] */
  { /*   390 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        194u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F1C_11, Satellite#0] */
  { /*   391 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        195u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F1C_13, Satellite#0] */
  { /*   392 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        196u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F1F_87, Satellite#0] */
  { /*   393 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        197u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM3_11, Satellite#0] */
  { /*   394 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        198u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM3_12, Satellite#0] */
  { /*   395 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        199u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM3_13, Satellite#0] */
  { /*   396 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        200u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM3_98, Satellite#0] */
  { /*   397 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        201u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM4_11, Satellite#0] */
  { /*   398 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        202u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM4_12, Satellite#0] */
  { /*   399 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        203u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM4_13, Satellite#0] */
    /* Index    AgingCycleCounterThreshold  AgingCycleId                          DebounceTableIdx  DtcTableIdx  EnableConditionGroupTableIdx  EventKind                                         ExtendedDataTableIdx  FreezeFrameNumTableEndIdx                         FreezeFrameNumTableStartIdx                         FreezeFrameTableStdFFIdx  HealingTarget  InitMonitorForEventIdx  MaskedBits  NormalIndicatorTableEndIdx                         NormalIndicatorTableStartIdx                               Referable Keys */
  { /*   400 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        204u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM5_11, Satellite#0] */
  { /*   401 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        205u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM5_12, Satellite#0] */
  { /*   402 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        206u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM5_13, Satellite#0] */
  { /*   403 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        207u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM6_11, Satellite#0] */
  { /*   404 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        208u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM6_12, Satellite#0] */
  { /*   405 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        209u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM6_13, Satellite#0] */
  { /*   406 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        210u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM7_11, Satellite#0] */
  { /*   407 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        211u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM7_12, Satellite#0] */
  { /*   408 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        212u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FM7_13, Satellite#0] */
  { /*   409 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,        213u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1A8N_11, Satellite#0] */
  { /*   410 */                       255u, DemConf_DemOperationCycle_PowerCycle,               5u,        214u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1A8O_11, Satellite#0] */
  { /*   411 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        215u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1F0O_1E, Satellite#0] */
  { /*   412 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        216u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1FZ9_1E, Satellite#0] */
  { /*   413 */                        80u, DemConf_DemOperationCycle_PowerCycle,               2u,        217u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BUP_12, Satellite#0] */
  { /*   414 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        218u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BZE_12, Satellite#0] */
  { /*   415 */                       255u, /*no AgingCycle*/ 1U                ,               6u,          0u,                           0u, DEM_CFG_DEM_EVENT_KIND_BSW_EVENTKINDOFEVENTTABLE,                   0u, DEM_CFG_NO_FREEZEFRAMENUMTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_FREEZEFRAMENUMTABLESTARTIDXOFEVENTTABLE,                       0u,            0u,                     0u,      0x02u, DEM_CFG_NO_NORMALINDICATORTABLEENDIDXOFEVENTTABLE, DEM_CFG_NO_NORMALINDICATORTABLESTARTIDXOFEVENTTABLE },  /* [AutoCreatedDemEvent_RamTstDemEventParameterRefs_RAMTST_E_RAM_FAILURE, Satellite#0] */
  { /*   416 */                        80u, DemConf_DemOperationCycle_PowerCycle,               3u,        219u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1AD0_1C, Satellite#0] */
  { /*   417 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        220u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1BOV_56, Satellite#0] */
  { /*   418 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        221u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E0K_38, Satellite#0] */
  { /*   419 */                        80u, DemConf_DemOperationCycle_PowerCycle,               0u,        222u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1Q_19, Satellite#0] */
  { /*   420 */                        80u, DemConf_DemOperationCycle_PowerCycle,               0u,        223u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1R_19, Satellite#0] */
  { /*   421 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        224u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E1S_38, Satellite#0] */
  { /*   422 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        225u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2H_38, Satellite#0] */
  { /*   423 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        226u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2I_38, Satellite#0] */
  { /*   424 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        227u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2G_14, Satellite#0] */
  { /*   425 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        228u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2H_14, Satellite#0] */
  { /*   426 */                        80u, DemConf_DemOperationCycle_PowerCycle,               4u,        229u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       3u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1E2I_14, Satellite#0] */
  { /*   427 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        230u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D72_4A, Satellite#0] */
  { /*   428 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        231u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D72_9A, Satellite#0] */
  { /*   429 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        232u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D72_44, Satellite#0] */
  { /*   430 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        233u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D72_45, Satellite#0] */
  { /*   431 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        234u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D72_46, Satellite#0] */
  { /*   432 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        235u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D72_49, Satellite#0] */
  { /*   433 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        236u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D72_55, Satellite#0] */
  { /*   434 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        237u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D72_92, Satellite#0] */
  { /*   435 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        238u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D72_96, Satellite#0] */
  { /*   436 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        239u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D73_01, Satellite#0] */
  { /*   437 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        240u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D74_01, Satellite#0] */
  { /*   438 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        241u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D75_01, Satellite#0] */
  { /*   439 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        242u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u },  /* [D1D76_01, Satellite#0] */
  { /*   440 */                       255u, DemConf_DemOperationCycle_PowerCycle,               4u,        243u,                           1u, DEM_CFG_DEM_EVENT_KIND_SWC_EVENTKINDOFEVENTTABLE,                   2u,                                               2u,                                                 1u,                       4u,            3u,                     0u,      0x17u,                                                1u,                                                  0u }   /* [D1E8B_01, Satellite#0] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_ExtendedDataTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_ExtendedDataTable
  \details
  Element                                   Description
  DataCollectionTableEdr2CollIndEndIdx      the end index of the 0:n relation pointing to Dem_Cfg_DataCollectionTableEdr2CollInd
  DataCollectionTableEdr2CollIndStartIdx    the start index of the 0:n relation pointing to Dem_Cfg_DataCollectionTableEdr2CollInd
  MaxRecordSize                         
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_ExtendedDataTableType, DEM_CONST) Dem_Cfg_ExtendedDataTable[3] = {
    /* Index    DataCollectionTableEdr2CollIndEndIdx                                DataCollectionTableEdr2CollIndStartIdx                                MaxRecordSize        Referable Keys */
  { /*     0 */ DEM_CFG_NO_DATACOLLECTIONTABLEEDR2COLLINDENDIDXOFEXTENDEDDATATABLE, DEM_CFG_NO_DATACOLLECTIONTABLEEDR2COLLINDSTARTIDXOFEXTENDEDDATATABLE,            0u },  /* [#NoExtendedDataRecordConfigured, #EVENT_INVALID, DemEventParameter, AutoCreatedDemEvent_AdcDemEventParameterRefs_ADC_E_TIMEOUT, AutoCreatedDemEvent_ALLOW_COM_CN_FMSNet_fce1aae5_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFa_540060b1, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone1, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_SecuritySubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone2, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_CabSubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_FMSNet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone1, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_SecuritySubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone2, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_CabSubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_FMSNet, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef_1, 
            AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_2, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN01_5bde9749_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_PostRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ESH_WaitForNvm_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_EcuMClearValidatedWakeupEvents_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_EcuMGoToSelectedShutdownTarget_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_OnEnterPostRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_OnEnterShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterWaitForNvm_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SwitchPostRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_PostRunNested_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_PostRunToPrepNested_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_RunToPostRunNested_BswMReportFailToDemRef, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_ALL_RUN_REQUESTS_KILLED, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_CONFIGURATION_DATA_INCONSISTENT, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_IMPROPER_CALLER, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_RAM_CHECK_FAILED, AutoCreatedDemEvent_GptDemEventParameterRefs_GPT_E_TIMEOUT, AutoCreatedDemEvent_INIT_Action_CanIf_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_CanNm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_CanSM_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_CanTp_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Can_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_ComM_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Com_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Dcm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_EnableInterrupts_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Issm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_J1939Nm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_J1939Rm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_J1939Tp_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_LinIf_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_LinSM_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_LinTp_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Lin_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Nm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_NvMReadAll_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_PduR_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_RteStart_BswMReportFailToDemRef, AutoCreatedDemEvent_J1939NmDemEventParameterRefs_J1939NM_E_ADDRESS_LOST, AutoCreatedDemEvent_J1939TpConfiguration_J1939TP_E_COMMUNICATION, AutoCreatedDemEvent_LinDemEventParameterRefs_LIN_E_TIMEOUT, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_BUS, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_DESCRIPTOR, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_ECC, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_INCONSISTENCY, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_PRIORITY, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_UNRECOGNIZED, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_CLOCK_FAILURE, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_INVALIDMODE_CONFIG, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_SSCM_CER_FAILURE, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_TIMEOUT_FAILURE, 
            AutoCreatedDemEvent_SpiDemEventParameterRefs_SPI_E_HARDWARE_ERROR, AutoCreatedDemEvent_SpiDriver_SPI_E_HARDWARE_ERROR, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_CORRUPT_CONFIG, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_DISABLE_REJECTED, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_FORBIDDEN_INVOCATION, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_CALL, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_PARAMETER, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_MODE_FAILED, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_UNLOCKED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_INTEGRITY_FAILED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_LOSS_OF_REDUNDANCY, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_QUEUE_OVERFLOW, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_REQ_FAILED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_VERIFY_FAILED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRITE_PROTECTED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRONG_BLOCK_ID, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_COMPARE_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_ERASE_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_READ_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_UNEXPECTED_FLASH_ID, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_WRITE_FAILED, DEM_EVENT_StartApplication, AutoCreatedDemEvent_RamTstDemEventParameterRefs_RAMTST_E_RAM_FAILURE] */
  { /*     1 */                                                                 1u,                                                                   0u,            6u },  /* [#DemExtendedDataClass] */
  { /*     2 */                                                                12u,                                                                   1u,            6u }   /* [#ExtendedDataClass_361fef39, D1BJO_88, D1BJP_88, D1BJQ_88, D1BJR_88, D1BJS_88, D1BKB_87, D1BKC_87, D1BKD_87, D1BKE_87, D1BKF_87, D1BKH_87, D1BN1_16, D1BN1_17, D1BN1_44, D1BN1_45, D1BN1_46, D1BN1_94, D1BN2_16, D1BN2_17, D1BN2_44, D1BN2_45, D1BN2_46, D1BN2_94, D1BN4_16, D1BN4_17, D1BN4_44, D1BN4_45, D1BN4_46, D1BN4_49, D1BN4_94, D1BN9_16, D1BN9_17, D1BN9_44, D1BN9_45, D1BN9_46, D1BN9_49, D1BN9_94, D1BOG_16, D1BOG_17, D1BOG_46, D1BOG_94, D1BOH_16, D1BOH_17, D1BOH_44, D1BOH_45, D1BOH_46, D1BOH_49, D1BOH_94, D1BOI_16, D1BOI_17, D1BOI_46, D1BOI_63, D1BOI_94, D1BOV_63, D1BUO_63, D1CXA_63, D1CXB_63, D1CXC_63, D1DOO_63, D1BK9_87, D1BKG_87, D1BN8_16, D1BN8_17, D1BN8_44, D1BN8_79, D1BOX_4A, D1BOY_4A, D1BUK_16, D1BUK_63, D1BUL_31, D1BUL_95, D1F0B_16, D1F0B_17, D1F0B_44, D1F0B_45, D1F0B_46, D1F0B_49, D1F0B_94, D1F0C_87, D1A6D_88, D1A6E_88, D1A6F_88, D1A6H_88, D1A6L_88, D1A7X_67, D1ACK_67, D1ACM_67, D1ACN_67, D1AD0_2F, D1AD0_4A, D1AD0_05, D1AD0_29, D1AD0_41, D1AD0_44, D1AD0_45, D1AD0_46, D1AD0_47, D1AD0_48, D1AD0_49, D1AD0_94, D1AD9_1C, D1AD9_16, D1AD9_17, D1BR9_68, D1C5I_68, D1C18_67, D1E0K_11, D1E0K_12, D1E0K_13, D1E0K_16, D1E0K_17, D1E1Q_11, D1E1Q_12, D1E1Q_16, D1E1Q_17, D1E1R_11, D1E1R_12, D1E1R_16, D1E1R_17, D1E1S_11, D1E1S_12, D1E1S_13, D1E1S_16, D1E1S_17, D1E1T_11, D1E1T_12, D1E1T_13, D1E1T_16, D1E1T_17, D1E1U_11, D1E1U_12, D1E1U_13, D1E1U_16, D1E1U_17, D1E1V_11, D1E1V_12, D1E1V_13, D1E1V_16, D1E1V_17, D1E1W_11, D1E1W_12, D1E1W_13, D1E1W_16, D1E1W_17, D1E1X_11, D1E1X_13, D1E1Y_11, D1E1Y_13, D1E1Z_11, D1E1Z_13, D1E2A_11, D1E2A_13, D1E2B_11, D1E2B_13, D1E2C_11, D1E2C_13, D1E2G_12, D1E2G_16, D1E2G_17, D1E2H_12, D1E2H_16, D1E2H_17, D1E2I_12, D1E2I_16, D1E2I_17, D1E2L_29, D1E2M_29, D1E4Q_88, D1E10_11, D1E10_13, D1E11_11, D1E11_13, D1E12_11, D1E12_13, D1E13_11, D1E13_13, D1E14_11, D1E14_13, D1E15_11, D1E15_13, D1E16_11, D1E16_13, D1E17_11, D1E17_13, D1E18_11, D1E18_13, D1E19_11, D1E19_13, D1F1A_11, D1F1A_13, D1F1B_11, D1F1B_13, D1F1C_11, D1F1C_13, D1F1F_87, D1FM3_11, D1FM3_12, D1FM3_13, D1FM3_98, D1FM4_11, D1FM4_12, D1FM4_13, D1FM5_11, D1FM5_12, D1FM5_13, D1FM6_11, D1FM6_12, D1FM6_13, D1FM7_11, D1FM7_12, D1FM7_13, D1A8N_11, D1A8O_11, D1F0O_1E, D1FZ9_1E, D1BUP_12, D1BZE_12, D1AD0_1C, D1BOV_56, D1E0K_38, D1E1Q_19, D1E1R_19, D1E1S_38, D1E2H_38, D1E2I_38, D1E2G_14, D1E2H_14, D1E2I_14, D1D72_4A, D1D72_9A, D1D72_44, D1D72_45, D1D72_46, D1D72_49, D1D72_55, D1D72_92, D1D72_96, D1D73_01, D1D74_01, D1D75_01, D1D76_01, D1E8B_01] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_FreezeFrameNumTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_FreezeFrameNumTable
  \details
  Element     Description
  FFNumber
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_FreezeFrameNumTableType, DEM_CONST) Dem_Cfg_FreezeFrameNumTable[2] = {
    /* Index    FFNumber        Referable Keys */
  { /*     0 */       1u },  /* [DemFreezeFrameRecNumClass_StartApplication, DEM_EVENT_StartApplication] */
  { /*     1 */      16u }   /* [FreezeFrameRecNumClass_a15d25e1, D1BJO_88, D1BJP_88, D1BJQ_88, D1BJR_88, D1BJS_88, D1BKB_87, D1BKC_87, D1BKD_87, D1BKE_87, D1BKF_87, D1BKH_87, D1BN1_16, D1BN1_17, D1BN1_44, D1BN1_45, D1BN1_46, D1BN1_94, D1BN2_16, D1BN2_17, D1BN2_44, D1BN2_45, D1BN2_46, D1BN2_94, D1BN4_16, D1BN4_17, D1BN4_44, D1BN4_45, D1BN4_46, D1BN4_49, D1BN4_94, D1BN9_16, D1BN9_17, D1BN9_44, D1BN9_45, D1BN9_46, D1BN9_49, D1BN9_94, D1BOG_16, D1BOG_17, D1BOG_46, D1BOG_94, D1BOH_16, D1BOH_17, D1BOH_44, D1BOH_45, D1BOH_46, D1BOH_49, D1BOH_94, D1BOI_16, D1BOI_17, D1BOI_46, D1BOI_63, D1BOI_94, D1BOV_63, D1BUO_63, D1CXA_63, D1CXB_63, D1CXC_63, D1DOO_63, D1BK9_87, D1BKG_87, D1BN8_16, D1BN8_17, D1BN8_44, D1BN8_79, D1BOX_4A, D1BOY_4A, D1BUK_16, D1BUK_63, D1BUL_31, D1BUL_95, D1F0B_16, D1F0B_17, D1F0B_44, D1F0B_45, D1F0B_46, D1F0B_49, D1F0B_94, D1F0C_87, D1A6D_88, D1A6E_88, D1A6F_88, D1A6H_88, D1A6L_88, D1A7X_67, D1ACK_67, D1ACM_67, D1ACN_67, D1AD0_2F, D1AD0_4A, D1AD0_05, D1AD0_29, D1AD0_41, D1AD0_44, D1AD0_45, D1AD0_46, D1AD0_47, D1AD0_48, D1AD0_49, D1AD0_94, D1AD9_1C, D1AD9_16, D1AD9_17, D1BR9_68, D1C5I_68, D1C18_67, D1E0K_11, D1E0K_12, D1E0K_13, D1E0K_16, D1E0K_17, D1E1Q_11, D1E1Q_12, D1E1Q_16, D1E1Q_17, D1E1R_11, D1E1R_12, D1E1R_16, D1E1R_17, D1E1S_11, D1E1S_12, D1E1S_13, D1E1S_16, D1E1S_17, D1E1T_11, D1E1T_12, D1E1T_13, D1E1T_16, D1E1T_17, D1E1U_11, D1E1U_12, D1E1U_13, D1E1U_16, D1E1U_17, D1E1V_11, D1E1V_12, D1E1V_13, D1E1V_16, D1E1V_17, D1E1W_11, D1E1W_12, D1E1W_13, D1E1W_16, D1E1W_17, D1E1X_11, D1E1X_13, D1E1Y_11, D1E1Y_13, D1E1Z_11, D1E1Z_13, D1E2A_11, D1E2A_13, D1E2B_11, D1E2B_13, D1E2C_11, D1E2C_13, D1E2G_12, D1E2G_16, D1E2G_17, D1E2H_12, D1E2H_16, D1E2H_17, D1E2I_12, D1E2I_16, D1E2I_17, D1E2L_29, D1E2M_29, D1E4Q_88, D1E10_11, D1E10_13, D1E11_11, D1E11_13, D1E12_11, D1E12_13, D1E13_11, D1E13_13, D1E14_11, D1E14_13, D1E15_11, D1E15_13, D1E16_11, D1E16_13, D1E17_11, D1E17_13, D1E18_11, D1E18_13, D1E19_11, D1E19_13, D1F1A_11, D1F1A_13, D1F1B_11, D1F1B_13, D1F1C_11, D1F1C_13, D1F1F_87, D1FM3_11, D1FM3_12, D1FM3_13, D1FM3_98, D1FM4_11, D1FM4_12, D1FM4_13, D1FM5_11, D1FM5_12, D1FM5_13, D1FM6_11, D1FM6_12, D1FM6_13, D1FM7_11, D1FM7_12, D1FM7_13, D1A8N_11, D1A8O_11, D1F0O_1E, D1FZ9_1E, D1BUP_12, D1BZE_12, D1AD0_1C, D1BOV_56, D1E0K_38, D1E1Q_19, D1E1R_19, D1E1S_38, D1E2H_38, D1E2I_38, D1E2G_14, D1E2H_14, D1E2I_14, D1D72_4A, D1D72_9A, D1D72_44, D1D72_45, D1D72_46, D1D72_49, D1D72_55, D1D72_92, D1D72_96, D1D73_01, D1D74_01, D1D75_01, D1D76_01, D1E8B_01] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_FreezeFrameTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_FreezeFrameTable
  \details
  Element                                   Description
  DataCollectionTableFfm2CollIndEndIdx      the end index of the 0:n relation pointing to Dem_Cfg_DataCollectionTableFfm2CollInd
  DataCollectionTableFfm2CollIndStartIdx    the start index of the 0:n relation pointing to Dem_Cfg_DataCollectionTableFfm2CollInd
  RecordSize                                Summarized size of did data that is stored in Dem_Cfg_PrimaryEntryType.SnapshotData[][] (i.e. typically without size of dids containing internal data elements).
  RecordSizeUds                             Summarized size of did data, did numbers and snapshot header (i.e. dynamical payload size of the uds response message).
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_FreezeFrameTableType, DEM_CONST) Dem_Cfg_FreezeFrameTable[10] = {
    /* Index    DataCollectionTableFfm2CollIndEndIdx                               DataCollectionTableFfm2CollIndStartIdx                               RecordSize  RecordSizeUds        Referable Keys */
  { /*     0 */ DEM_CFG_NO_DATACOLLECTIONTABLEFFM2COLLINDENDIDXOFFREEZEFRAMETABLE, DEM_CFG_NO_DATACOLLECTIONTABLEFFM2COLLINDSTARTIDXOFFREEZEFRAMETABLE,         0u,            0u },  /* [#NoFreezeFrameConfigured, #EVENT_INVALID, DemEventParameter, AutoCreatedDemEvent_AdcDemEventParameterRefs_ADC_E_TIMEOUT, AutoCreatedDemEvent_ALLOW_COM_CN_FMSNet_fce1aae5_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFa_540060b1, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFailToDemRef, AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone1, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_SecuritySubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone2, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_CabSubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_FMSNet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone1, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_SecuritySubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone2, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_CabSubnet, AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_FMSNet, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef_1, 
            AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_2, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN01_5bde9749_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_PostRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ESH_WaitForNvm_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_EcuMClearValidatedWakeupEvents_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_EcuMGoToSelectedShutdownTarget_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_OnEnterPostRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_OnEnterShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterWaitForNvm_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SwitchPostRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef_1, AutoCreatedDemEvent_ESH_PostRunNested_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_PostRunToPrepNested_BswMReportFailToDemRef, AutoCreatedDemEvent_ESH_RunToPostRunNested_BswMReportFailToDemRef, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_ALL_RUN_REQUESTS_KILLED, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_CONFIGURATION_DATA_INCONSISTENT, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_IMPROPER_CALLER, AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_RAM_CHECK_FAILED, AutoCreatedDemEvent_GptDemEventParameterRefs_GPT_E_TIMEOUT, AutoCreatedDemEvent_INIT_Action_CanIf_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_CanNm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_CanSM_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_CanTp_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Can_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_ComM_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Com_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Dcm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_EnableInterrupts_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Issm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_J1939Nm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_J1939Rm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_J1939Tp_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_LinIf_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_LinSM_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_LinTp_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Lin_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_Nm_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_NvMReadAll_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_PduR_Init_BswMReportFailToDemRef, AutoCreatedDemEvent_INIT_Action_RteStart_BswMReportFailToDemRef, AutoCreatedDemEvent_J1939NmDemEventParameterRefs_J1939NM_E_ADDRESS_LOST, AutoCreatedDemEvent_J1939TpConfiguration_J1939TP_E_COMMUNICATION, AutoCreatedDemEvent_LinDemEventParameterRefs_LIN_E_TIMEOUT, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_BUS, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_DESCRIPTOR, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_ECC, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_INCONSISTENCY, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_PRIORITY, AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_UNRECOGNIZED, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_CLOCK_FAILURE, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_INVALIDMODE_CONFIG, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_SSCM_CER_FAILURE, AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_TIMEOUT_FAILURE, 
            AutoCreatedDemEvent_SpiDemEventParameterRefs_SPI_E_HARDWARE_ERROR, AutoCreatedDemEvent_SpiDriver_SPI_E_HARDWARE_ERROR, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_CORRUPT_CONFIG, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_DISABLE_REJECTED, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_FORBIDDEN_INVOCATION, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_CALL, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_PARAMETER, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_MODE_FAILED, AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_UNLOCKED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_INTEGRITY_FAILED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_LOSS_OF_REDUNDANCY, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_QUEUE_OVERFLOW, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_REQ_FAILED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_VERIFY_FAILED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRITE_PROTECTED, AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRONG_BLOCK_ID, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_COMPARE_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_ERASE_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_READ_FAILED, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_UNEXPECTED_FLASH_ID, AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_WRITE_FAILED, AutoCreatedDemEvent_RamTstDemEventParameterRefs_RAMTST_E_RAM_FAILURE] */
  { /*     1 */                                                                1u,                                                                  0u,         2u,            6u },  /* [#DemFreezeFrameClass_StartApplication, DEM_EVENT_StartApplication] */
  { /*     2 */                                                                6u,                                                                  1u,        13u,           25u },  /* [#FreezeFrameClass_04f05c8a, D1AD0_41, D1AD0_44, D1AD0_45, D1AD0_46] */
  { /*     3 */                                                               11u,                                                                  6u,        12u,           24u },  /* [#FreezeFrameClass_345716cc, D1BJO_88, D1BJP_88, D1BJQ_88, D1BJR_88, D1BJS_88, D1BKB_87, D1BKC_87, D1BKD_87, D1BKE_87, D1BKF_87, D1BKH_87, D1BKG_87, D1F0C_87, D1A6D_88, D1A6E_88, D1A6F_88, D1A6H_88, D1A6L_88, D1AD0_2F, D1AD0_29, D1AD0_48, D1AD0_94, D1AD9_1C, D1AD9_16, D1AD9_17, D1E0K_11, D1E0K_12, D1E0K_13, D1E0K_16, D1E0K_17, D1E1Q_11, D1E1Q_12, D1E1Q_16, D1E1Q_17, D1E1R_11, D1E1R_12, D1E1R_16, D1E1R_17, D1E1S_11, D1E1S_12, D1E1S_13, D1E1S_16, D1E1S_17, D1E1T_11, D1E1T_12, D1E1T_13, D1E1T_16, D1E1T_17, D1E1U_11, D1E1U_12, D1E1U_13, D1E1U_16, D1E1U_17, D1E1V_11, D1E1V_12, D1E1V_13, D1E1V_16, D1E1V_17, D1E1W_11, D1E1W_12, D1E1W_13, D1E1W_16, D1E1W_17, D1E1X_11, D1E1X_13, D1E1Y_11, D1E1Y_13, D1E1Z_11, D1E1Z_13, D1E2A_11, D1E2A_13, D1E2B_11, D1E2B_13, D1E2C_11, D1E2C_13, D1E2G_12, D1E2G_16, D1E2G_17, D1E2H_12, D1E2H_16, D1E2H_17, D1E2I_12, D1E2I_16, D1E2I_17, D1E2L_29, D1E2M_29, D1E4Q_88, D1E10_11, D1E10_13, D1E11_11, D1E11_13, D1E12_11, D1E12_13, D1E13_11, D1E13_13, D1E14_11, D1E14_13, D1E15_11, D1E15_13, D1E16_11, D1E16_13, D1E17_11, D1E17_13, D1E18_11, D1E18_13, D1E19_11, D1E19_13, D1F1A_11, D1F1A_13, D1F1B_11, D1F1B_13, D1F1C_11, D1F1C_13, D1FM3_11, D1FM3_12, D1FM3_13, D1FM3_98, D1FM4_11, D1FM4_12, D1FM4_13, D1FM5_11, D1FM5_12, D1FM5_13, D1FM6_11, D1FM6_12, D1FM6_13, D1FM7_11, D1FM7_12, D1FM7_13, D1AD0_1C, D1E0K_38, D1E1Q_19, D1E1R_19, D1E1S_38, D1E2H_38, D1E2I_38, D1E2G_14, D1E2H_14, D1E2I_14] */
  { /*     4 */                                                               14u,                                                                 11u,         7u,           15u },  /* [#FreezeFrameClass_505ba959, D1BN1_16, D1BN1_17, D1BN1_44, D1BN1_45, D1BN1_46, D1BN2_16, D1BN2_17, D1BN2_44, D1BN2_45, D1BN2_46, D1BN4_16, D1BN4_17, D1BN4_44, D1BN4_45, D1BN4_46, D1BN4_49, D1BN9_16, D1BN9_17, D1BN9_44, D1BN9_45, D1BN9_46, D1BN9_49, D1BOG_16, D1BOG_17, D1BOG_46, D1BOG_94, D1BOH_16, D1BOH_17, D1BOH_44, D1BOH_45, D1BOH_46, D1BOH_49, D1BOH_94, D1BOI_16, D1BOI_17, D1BOI_46, D1BOI_63, D1BOI_94, D1BOV_63, D1BUO_63, D1CXA_63, D1CXB_63, D1CXC_63, D1DOO_63, D1BN8_16, D1BN8_17, D1BN8_44, D1BUK_16, D1BUK_63, D1BUL_31, D1BUL_95, D1F0B_16, D1F0B_17, D1F0B_44, D1F0B_45, D1F0B_46, D1F0B_49, D1F0B_94, D1A7X_67, D1ACK_67, D1ACM_67, D1ACN_67, D1AD0_4A, D1AD0_05, D1BR9_68, D1C5I_68, D1C18_67, D1F1F_87, D1A8N_11, D1A8O_11, D1F0O_1E, D1FZ9_1E, D1BUP_12, D1BZE_12, D1BOV_56, D1D72_4A, D1D72_9A, D1D72_44, D1D72_45, D1D72_46, D1D72_49, D1D72_55, D1D72_92, D1D72_96, D1D73_01, D1D74_01, D1D75_01, D1D76_01, D1E8B_01] */
  { /*     5 */                                                               19u,                                                                 14u,        13u,           25u },  /* [#FreezeFrameClass_9a94c929, D1AD0_47] */
  { /*     6 */                                                               23u,                                                                 19u,        12u,           22u },  /* [#FreezeFrameClass_9cf73c71, D1BK9_87] */
  { /*     7 */                                                               27u,                                                                 23u,         8u,           18u },  /* [#FreezeFrameClass_ebf00ce7, D1BN8_79, D1BOX_4A, D1BOY_4A] */
  { /*     8 */                                                               32u,                                                                 27u,        13u,           25u },  /* [#FreezeFrameClass_ed93f9bf, D1AD0_49] */
  { /*     9 */                                                               36u,                                                                 32u,         8u,           18u }   /* [#FreezeFrameClass_f947e5b7, D1BN1_94, D1BN2_94, D1BN4_94, D1BN9_94] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_InitMonitorForEvent
**********************************************************************************************************************/
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_InitMonitorForEventFPtrType, DEM_CONST) Dem_Cfg_InitMonitorForEvent[3] = {
  /* Index     InitMonitorForEvent                                  Referable Keys */
  /*     0 */ NULL_PTR                                        ,  /* [#NoCallbackInitMonitorConfigured] */
  /*     1 */ Rte_Call_CBInitEvt_D1BUL_31_InitMonitorForEvent ,  /* [D1BUL_31] */
  /*     2 */ Rte_Call_CBInitEvt_D1BUL_95_InitMonitorForEvent    /* [D1BUL_95] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryBlockId
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryBlockId
  \brief  The array contains these items: Admin, Status, 10 * Primary
*/ 
#define DEM_START_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_MemoryBlockIdType, DEM_CONST) Dem_Cfg_MemoryBlockId[12] = {
  /* Index     MemoryBlockId                                                          Comment */
  /*     0 */ NvMConf_NvMBlockDescriptor_DemAdminDataBlock /*NvMBlockId=24*/    ,  /* [Dem_AdminData] */
  /*     1 */ NvMConf_NvMBlockDescriptor_DemStatusDataBlock /*NvMBlockId=25*/   ,  /* [Dem_StatusData] */
  /*     2 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock0 /*NvMBlockId=26*/ ,  /* [Dem_PrimaryEntry0] */
  /*     3 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock1 /*NvMBlockId=4*/  ,  /* [Dem_PrimaryEntry1] */
  /*     4 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock2 /*NvMBlockId=5*/  ,  /* [Dem_PrimaryEntry2] */
  /*     5 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock3 /*NvMBlockId=6*/  ,  /* [Dem_PrimaryEntry3] */
  /*     6 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock4 /*NvMBlockId=7*/  ,  /* [Dem_PrimaryEntry4] */
  /*     7 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock5 /*NvMBlockId=8*/  ,  /* [Dem_PrimaryEntry5] */
  /*     8 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock6 /*NvMBlockId=9*/  ,  /* [Dem_PrimaryEntry6] */
  /*     9 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock7 /*NvMBlockId=10*/ ,  /* [Dem_PrimaryEntry7] */
  /*    10 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock8 /*NvMBlockId=11*/ ,  /* [Dem_PrimaryEntry8] */
  /*    11 */ NvMConf_NvMBlockDescriptor_DemPrimaryDataBlock9 /*NvMBlockId=12*/    /* [Dem_PrimaryEntry9] */
};
#define DEM_STOP_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryBlockIdToMemoryEntryId
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryBlockIdToMemoryEntryId
  \brief  The array contains these items: Admin, Status, 10 * Primary
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_MemoryEntry_HandleType, DEM_CONST) Dem_Cfg_MemoryBlockIdToMemoryEntryId[12] = {
  /* Index     MemoryBlockIdToMemoryEntryId        Comment */
  /*     0 */ DEM_MEMORYENTRY_HANDLE_INVALID ,  /* [Dem_AdminData] */
  /*     1 */ DEM_MEMORYENTRY_HANDLE_INVALID ,  /* [Dem_StatusData] */
  /*     2 */ 0u                             ,  /* [Dem_PrimaryEntry0] */
  /*     3 */ 1u                             ,  /* [Dem_PrimaryEntry1] */
  /*     4 */ 2u                             ,  /* [Dem_PrimaryEntry2] */
  /*     5 */ 3u                             ,  /* [Dem_PrimaryEntry3] */
  /*     6 */ 4u                             ,  /* [Dem_PrimaryEntry4] */
  /*     7 */ 5u                             ,  /* [Dem_PrimaryEntry5] */
  /*     8 */ 6u                             ,  /* [Dem_PrimaryEntry6] */
  /*     9 */ 7u                             ,  /* [Dem_PrimaryEntry7] */
  /*    10 */ 8u                             ,  /* [Dem_PrimaryEntry8] */
  /*    11 */ 9u                                /* [Dem_PrimaryEntry9] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryDataPtr
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryDataPtr
  \brief  The array contains these items: Admin, Status, 10 * Primary
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_NvDataPtrType, DEM_CONST) Dem_Cfg_MemoryDataPtr[12] = {
  /* Index     MemoryDataPtr                                                                                  Comment */
  /*     0 */ (Dem_NvDataPtrType) &Dem_Cfg_GetAdminData()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_AdminData] */
  /*     1 */ (Dem_NvDataPtrType) &Dem_Cfg_GetStatusData()       /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_StatusData] */
  /*     2 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_0()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry0] */
  /*     3 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_1()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry1] */
  /*     4 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_2()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry2] */
  /*     5 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_3()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry3] */
  /*     6 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_4()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry4] */
  /*     7 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_5()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry5] */
  /*     8 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_6()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry6] */
  /*     9 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_7()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry7] */
  /*    10 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_8()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry8] */
  /*    11 */ (Dem_NvDataPtrType) &Dem_Cfg_GetPrimaryEntry_9()   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */   /* [Dem_PrimaryEntry9] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryDataSize
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryDataSize
  \brief  The array contains these items: Admin, Status, 10 * Primary
*/ 
#define DEM_START_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_MemoryDataSizeType, DEM_CONST) Dem_Cfg_MemoryDataSize[12] = {
  /* Index     MemoryDataSize                                                        Comment */
  /*     0 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetAdminData())      ,  /* [Dem_AdminData] */
  /*     1 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetStatusData())     ,  /* [Dem_StatusData] */
  /*     2 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_0()) ,  /* [Dem_PrimaryEntry0] */
  /*     3 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_1()) ,  /* [Dem_PrimaryEntry1] */
  /*     4 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_2()) ,  /* [Dem_PrimaryEntry2] */
  /*     5 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_3()) ,  /* [Dem_PrimaryEntry3] */
  /*     6 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_4()) ,  /* [Dem_PrimaryEntry4] */
  /*     7 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_5()) ,  /* [Dem_PrimaryEntry5] */
  /*     8 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_6()) ,  /* [Dem_PrimaryEntry6] */
  /*     9 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_7()) ,  /* [Dem_PrimaryEntry7] */
  /*    10 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_8()) ,  /* [Dem_PrimaryEntry8] */
  /*    11 */ (Dem_Cfg_MemoryDataSizeType) sizeof(Dem_Cfg_GetPrimaryEntry_9())    /* [Dem_PrimaryEntry9] */
};
#define DEM_STOP_SEC_CONST_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryEntry
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryEntry
  \brief  The array contains these items: 10 * Primary, ReadoutBuffer; size = DEM_CFG_GLOBAL_PRIMARY_SIZE + DEM_CFG_GLOBAL_SECONDARY_SIZE + DEM_CFG_NUMBER_OF_READOUTBUFFERS
*/ 
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_SharedMemoryEntryPtrType, DEM_CONST) Dem_Cfg_MemoryEntry[11] = {
  /* Index     MemoryEntry                                                                     Comment */
  /*     0 */ &Dem_Cfg_GetPrimaryEntry_0()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry0] */
  /*     1 */ &Dem_Cfg_GetPrimaryEntry_1()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry1] */
  /*     2 */ &Dem_Cfg_GetPrimaryEntry_2()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry2] */
  /*     3 */ &Dem_Cfg_GetPrimaryEntry_3()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry3] */
  /*     4 */ &Dem_Cfg_GetPrimaryEntry_4()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry4] */
  /*     5 */ &Dem_Cfg_GetPrimaryEntry_5()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry5] */
  /*     6 */ &Dem_Cfg_GetPrimaryEntry_6()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry6] */
  /*     7 */ &Dem_Cfg_GetPrimaryEntry_7()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry7] */
  /*     8 */ &Dem_Cfg_GetPrimaryEntry_8()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry8] */
  /*     9 */ &Dem_Cfg_GetPrimaryEntry_9()        /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */,  /* [Dem_PrimaryEntry9] */
  /*    10 */ &Dem_Cfg_GetReadoutBuffer(0).Data   /* PRQA S 0310 */ /* MD_DEM_11.4_nvm */   /* [Dem_Cfg_ReadoutBuffer[0].Data] */
};
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryEntryInit
**********************************************************************************************************************/
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_PrimaryEntryType, DEM_CONST) Dem_Cfg_MemoryEntryInit = { 0 };
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_TimeSeriesEntryInit
**********************************************************************************************************************/
#define DEM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(Dem_Cfg_TimeSeriesEntryType, DEM_CONST) Dem_Cfg_TimeSeriesEntryInit = { 0 };
#define DEM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_AdminData
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_AdminDataType, DEM_NVM_DATA_NOINIT) Dem_Cfg_AdminData;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_ClearDTCTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_ClearDTCTable
  \brief  size = DEM_CFG_NUMBER_OF_CLEARDTCS
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_ClearDTC_DataType, DEM_VAR_NOINIT) Dem_Cfg_ClearDTCTable[1];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_CommitBuffer
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_CommitBufferType, DEM_VAR_NOINIT) Dem_Cfg_CommitBuffer;  /* PRQA S 0759 */ /* MD_MSR_18.4 */
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_DTCSelectorTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_DTCSelectorTable
  \brief  size = DEM_CFG_NUMBER_OF_DTCSELECTORS
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_DTCSelector_DataType, DEM_VAR_NOINIT) Dem_Cfg_DTCSelectorTable[1];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionGroupCounter
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionGroupCounter
  \brief  (DEM_CFG_SUPPORT_ENABLE_CONDITIONS == STD_ON) or there are internal EnableConditions. Table index: Condition group number. Table value: count of conditions in state 'enable'.
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_EnableConditionGroupCounterType, DEM_VAR_NOINIT) Dem_Cfg_EnableConditionGroupCounter[2];
  /* Index        Referable Keys  */
  /*     0 */  /* [##NoEnableConditionGroupConfigured, __Internal_ControlDtcSetting] */
  /*     1 */  /* [#EnableConditionGroup_IgnitionNotCranking, __Internal_ControlDtcSetting, IgnitionNotCranking] */

#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionGroupState
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionGroupState
  \brief  (DEM_CFG_SUPPORT_ENABLE_CONDITIONS == STD_ON) or there are internal EnableConditions. Table index: Condition group number. Table value: count of conditions in state 'enable'.
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
volatile VAR(Dem_Cfg_EnableConditionGroupStateType, DEM_VAR_NOINIT) Dem_Cfg_EnableConditionGroupState[2];
  /* Index        Referable Keys  */
  /*     0 */  /* [##NoEnableConditionGroupConfigured, __Internal_ControlDtcSetting] */
  /*     1 */  /* [#EnableConditionGroup_IgnitionNotCranking, __Internal_ControlDtcSetting, IgnitionNotCranking] */

#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EnableConditionState
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EnableConditionState
  \brief  (DEM_CFG_SUPPORT_ENABLE_CONDITIONS == STD_ON) or there are internal EnableConditions. Table index: Condition ID. Table value: current condition state '0' disable, '1' enable.
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_EnableConditionStateType, DEM_VAR_NOINIT) Dem_Cfg_EnableConditionState[2];
#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EventDebounceValue
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EventDebounceValue
  \brief  size = DEM_G_NUMBER_OF_EVENTS
*/ 
#define DEM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
volatile VAR(Dem_Cfg_EventDebounceValueType, DEM_VAR_NOINIT) Dem_Cfg_EventDebounceValue[441];
  /* Index        Referable Keys  */
  /*     0 */  /* [#EVENT_INVALID, Satellite#0] */
  /*     1 */  /* [DemEventParameter, Satellite#0] */
  /*     2 */  /* [AutoCreatedDemEvent_AdcDemEventParameterRefs_ADC_E_TIMEOUT, Satellite#0] */
  /*     3 */  /* [AutoCreatedDemEvent_ALLOW_COM_CN_FMSNet_fce1aae5_BswMReportFailToDemRef, Satellite#0] */
  /*     4 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  /*     5 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  /*     6 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  /*     7 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  /*     8 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  /*     9 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  /*    10 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, Satellite#0] */
  /*    11 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  /*    12 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, Satellite#0] */
  /*    13 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  /*    14 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, Satellite#0] */
  /*    15 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  /*    16 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, Satellite#0] */
  /*    17 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, Satellite#0] */
  /*    18 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, Satellite#0] */
  /*    19 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, Satellite#0] */
  /*    20 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, Satellite#0] */
  /*    21 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, Satellite#0] */
  /*    22 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, Satellite#0] */
  /*    23 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, Satellite#0] */
  /*    24 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, Satellite#0] */
  /*    25 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, Satellite#0] */
  /*    26 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, Satellite#0] */
  /*    27 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, Satellite#0] */
  /*    28 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, Satellite#0] */
  /*    29 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  /*    30 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, Satellite#0] */
  /*    31 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  /*    32 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  /*    33 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  /*    34 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  /*    35 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  /*    36 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  /*    37 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, Satellite#0] */
  /*    38 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  /*    39 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, Satellite#0] */
  /*    40 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  /*    41 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, Satellite#0] */
  /*    42 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  /*    43 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, Satellite#0] */
  /*    44 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, Satellite#0] */
  /*    45 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, Satellite#0] */
  /*    46 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, Satellite#0] */
  /*    47 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, Satellite#0] */
  /*    48 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, Satellite#0] */
  /*    49 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, Satellite#0] */
  /* Index        Referable Keys  */
  /*    50 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, Satellite#0] */
  /*    51 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, Satellite#0] */
  /*    52 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, Satellite#0] */
  /*    53 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, Satellite#0] */
  /*    54 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, Satellite#0] */
  /*    55 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, Satellite#0] */
  /*    56 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  /*    57 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, Satellite#0] */
  /*    58 */  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFa_540060b1, Satellite#0] */
  /*    59 */  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, Satellite#0] */
  /*    60 */  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFailToDemRef, Satellite#0] */
  /*    61 */  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, Satellite#0] */
  /*    62 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone1, Satellite#0] */
  /*    63 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_SecuritySubnet, Satellite#0] */
  /*    64 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone2, Satellite#0] */
  /*    65 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_CabSubnet, Satellite#0] */
  /*    66 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_FMSNet, Satellite#0] */
  /*    67 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone1, Satellite#0] */
  /*    68 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_SecuritySubnet, Satellite#0] */
  /*    69 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone2, Satellite#0] */
  /*    70 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_CabSubnet, Satellite#0] */
  /*    71 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_FMSNet, Satellite#0] */
  /*    72 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, Satellite#0] */
  /*    73 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef_1, Satellite#0] */
  /*    74 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, Satellite#0] */
  /*    75 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef_1, Satellite#0] */
  /*    76 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, Satellite#0] */
  /*    77 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef_1, Satellite#0] */
  /*    78 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, Satellite#0] */
  /*    79 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef_1, Satellite#0] */
  /*    80 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef, Satellite#0] */
  /*    81 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef_1, Satellite#0] */
  /*    82 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, Satellite#0] */
  /*    83 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef_1, Satellite#0] */
  /*    84 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, Satellite#0] */
  /*    85 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef_1, Satellite#0] */
  /*    86 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, Satellite#0] */
  /*    87 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef_1, Satellite#0] */
  /*    88 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, Satellite#0] */
  /*    89 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef_1, Satellite#0] */
  /*    90 */  /* [AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef, Satellite#0] */
  /*    91 */  /* [AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_1, Satellite#0] */
  /*    92 */  /* [AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_2, Satellite#0] */
  /*    93 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, Satellite#0] */
  /*    94 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, Satellite#0] */
  /*    95 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, Satellite#0] */
  /*    96 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, Satellite#0] */
  /*    97 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN01_5bde9749_BswMReportFailToDemRef, Satellite#0] */
  /*    98 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, Satellite#0] */
  /*    99 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, Satellite#0] */
  /* Index        Referable Keys  */
  /*   100 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, Satellite#0] */
  /*   101 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, Satellite#0] */
  /*   102 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_PostRun_BswMReportFailToDemRef, Satellite#0] */
  /*   103 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef, Satellite#0] */
  /*   104 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef_1, Satellite#0] */
  /*   105 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef, Satellite#0] */
  /*   106 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef_1, Satellite#0] */
  /*   107 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_WaitForNvm_BswMReportFailToDemRef, Satellite#0] */
  /*   108 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef, Satellite#0] */
  /*   109 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef_1, Satellite#0] */
  /*   110 */  /* [AutoCreatedDemEvent_ESH_Action_EcuMClearValidatedWakeupEvents_BswMReportFailToDemRef, Satellite#0] */
  /*   111 */  /* [AutoCreatedDemEvent_ESH_Action_EcuMGoToSelectedShutdownTarget_BswMReportFailToDemRef, Satellite#0] */
  /*   112 */  /* [AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef, Satellite#0] */
  /*   113 */  /* [AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef_1, Satellite#0] */
  /*   114 */  /* [AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef, Satellite#0] */
  /*   115 */  /* [AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef_1, Satellite#0] */
  /*   116 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterPostRun_BswMReportFailToDemRef, Satellite#0] */
  /*   117 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef, Satellite#0] */
  /*   118 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef_1, Satellite#0] */
  /*   119 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef, Satellite#0] */
  /*   120 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef_1, Satellite#0] */
  /*   121 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterShutdown_BswMReportFailToDemRef, Satellite#0] */
  /*   122 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterWaitForNvm_BswMReportFailToDemRef, Satellite#0] */
  /*   123 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef, Satellite#0] */
  /*   124 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef_1, Satellite#0] */
  /*   125 */  /* [AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef, Satellite#0] */
  /*   126 */  /* [AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef_1, Satellite#0] */
  /*   127 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchPostRun_BswMReportFailToDemRef, Satellite#0] */
  /*   128 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef, Satellite#0] */
  /*   129 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef_1, Satellite#0] */
  /*   130 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef, Satellite#0] */
  /*   131 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef_1, Satellite#0] */
  /*   132 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef, Satellite#0] */
  /*   133 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef_1, Satellite#0] */
  /*   134 */  /* [AutoCreatedDemEvent_ESH_PostRunNested_BswMReportFailToDemRef, Satellite#0] */
  /*   135 */  /* [AutoCreatedDemEvent_ESH_PostRunToPrepNested_BswMReportFailToDemRef, Satellite#0] */
  /*   136 */  /* [AutoCreatedDemEvent_ESH_RunToPostRunNested_BswMReportFailToDemRef, Satellite#0] */
  /*   137 */  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_ALL_RUN_REQUESTS_KILLED, Satellite#0] */
  /*   138 */  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_CONFIGURATION_DATA_INCONSISTENT, Satellite#0] */
  /*   139 */  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_IMPROPER_CALLER, Satellite#0] */
  /*   140 */  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_RAM_CHECK_FAILED, Satellite#0] */
  /*   141 */  /* [AutoCreatedDemEvent_GptDemEventParameterRefs_GPT_E_TIMEOUT, Satellite#0] */
  /*   142 */  /* [AutoCreatedDemEvent_INIT_Action_CanIf_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   143 */  /* [AutoCreatedDemEvent_INIT_Action_CanNm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   144 */  /* [AutoCreatedDemEvent_INIT_Action_CanSM_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   145 */  /* [AutoCreatedDemEvent_INIT_Action_CanTp_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   146 */  /* [AutoCreatedDemEvent_INIT_Action_Can_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   147 */  /* [AutoCreatedDemEvent_INIT_Action_ComM_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   148 */  /* [AutoCreatedDemEvent_INIT_Action_Com_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   149 */  /* [AutoCreatedDemEvent_INIT_Action_Dcm_Init_BswMReportFailToDemRef, Satellite#0] */
  /* Index        Referable Keys  */
  /*   150 */  /* [AutoCreatedDemEvent_INIT_Action_EnableInterrupts_BswMReportFailToDemRef, Satellite#0] */
  /*   151 */  /* [AutoCreatedDemEvent_INIT_Action_Issm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   152 */  /* [AutoCreatedDemEvent_INIT_Action_J1939Nm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   153 */  /* [AutoCreatedDemEvent_INIT_Action_J1939Rm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   154 */  /* [AutoCreatedDemEvent_INIT_Action_J1939Tp_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   155 */  /* [AutoCreatedDemEvent_INIT_Action_LinIf_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   156 */  /* [AutoCreatedDemEvent_INIT_Action_LinSM_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   157 */  /* [AutoCreatedDemEvent_INIT_Action_LinTp_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   158 */  /* [AutoCreatedDemEvent_INIT_Action_Lin_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   159 */  /* [AutoCreatedDemEvent_INIT_Action_Nm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   160 */  /* [AutoCreatedDemEvent_INIT_Action_NvMReadAll_BswMReportFailToDemRef, Satellite#0] */
  /*   161 */  /* [AutoCreatedDemEvent_INIT_Action_PduR_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   162 */  /* [AutoCreatedDemEvent_INIT_Action_RteStart_BswMReportFailToDemRef, Satellite#0] */
  /*   163 */  /* [AutoCreatedDemEvent_J1939NmDemEventParameterRefs_J1939NM_E_ADDRESS_LOST, Satellite#0] */
  /*   164 */  /* [AutoCreatedDemEvent_J1939TpConfiguration_J1939TP_E_COMMUNICATION, Satellite#0] */
  /*   165 */  /* [AutoCreatedDemEvent_LinDemEventParameterRefs_LIN_E_TIMEOUT, Satellite#0] */
  /*   166 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_BUS, Satellite#0] */
  /*   167 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_DESCRIPTOR, Satellite#0] */
  /*   168 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_ECC, Satellite#0] */
  /*   169 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_INCONSISTENCY, Satellite#0] */
  /*   170 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_PRIORITY, Satellite#0] */
  /*   171 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_UNRECOGNIZED, Satellite#0] */
  /*   172 */  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_CLOCK_FAILURE, Satellite#0] */
  /*   173 */  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_INVALIDMODE_CONFIG, Satellite#0] */
  /*   174 */  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_SSCM_CER_FAILURE, Satellite#0] */
  /*   175 */  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_TIMEOUT_FAILURE, Satellite#0] */
  /*   176 */  /* [AutoCreatedDemEvent_SpiDemEventParameterRefs_SPI_E_HARDWARE_ERROR, Satellite#0] */
  /*   177 */  /* [AutoCreatedDemEvent_SpiDriver_SPI_E_HARDWARE_ERROR, Satellite#0] */
  /*   178 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_CORRUPT_CONFIG, Satellite#0] */
  /*   179 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_DISABLE_REJECTED, Satellite#0] */
  /*   180 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_FORBIDDEN_INVOCATION, Satellite#0] */
  /*   181 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_CALL, Satellite#0] */
  /*   182 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_PARAMETER, Satellite#0] */
  /*   183 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_MODE_FAILED, Satellite#0] */
  /*   184 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_UNLOCKED, Satellite#0] */
  /*   185 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_INTEGRITY_FAILED, Satellite#0] */
  /*   186 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_LOSS_OF_REDUNDANCY, Satellite#0] */
  /*   187 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_QUEUE_OVERFLOW, Satellite#0] */
  /*   188 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_REQ_FAILED, Satellite#0] */
  /*   189 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_VERIFY_FAILED, Satellite#0] */
  /*   190 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRITE_PROTECTED, Satellite#0] */
  /*   191 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRONG_BLOCK_ID, Satellite#0] */
  /*   192 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_COMPARE_FAILED, Satellite#0] */
  /*   193 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_ERASE_FAILED, Satellite#0] */
  /*   194 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_READ_FAILED, Satellite#0] */
  /*   195 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_UNEXPECTED_FLASH_ID, Satellite#0] */
  /*   196 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_WRITE_FAILED, Satellite#0] */
  /*   197 */  /* [DEM_EVENT_StartApplication, Satellite#0] */
  /*   198 */  /* [D1BJO_88, Satellite#0] */
  /*   199 */  /* [D1BJP_88, Satellite#0] */
  /* Index        Referable Keys  */
  /*   200 */  /* [D1BJQ_88, Satellite#0] */
  /*   201 */  /* [D1BJR_88, Satellite#0] */
  /*   202 */  /* [D1BJS_88, Satellite#0] */
  /*   203 */  /* [D1BKB_87, Satellite#0] */
  /*   204 */  /* [D1BKC_87, Satellite#0] */
  /*   205 */  /* [D1BKD_87, Satellite#0] */
  /*   206 */  /* [D1BKE_87, Satellite#0] */
  /*   207 */  /* [D1BKF_87, Satellite#0] */
  /*   208 */  /* [D1BKH_87, Satellite#0] */
  /*   209 */  /* [D1BN1_16, Satellite#0] */
  /*   210 */  /* [D1BN1_17, Satellite#0] */
  /*   211 */  /* [D1BN1_44, Satellite#0] */
  /*   212 */  /* [D1BN1_45, Satellite#0] */
  /*   213 */  /* [D1BN1_46, Satellite#0] */
  /*   214 */  /* [D1BN1_94, Satellite#0] */
  /*   215 */  /* [D1BN2_16, Satellite#0] */
  /*   216 */  /* [D1BN2_17, Satellite#0] */
  /*   217 */  /* [D1BN2_44, Satellite#0] */
  /*   218 */  /* [D1BN2_45, Satellite#0] */
  /*   219 */  /* [D1BN2_46, Satellite#0] */
  /*   220 */  /* [D1BN2_94, Satellite#0] */
  /*   221 */  /* [D1BN4_16, Satellite#0] */
  /*   222 */  /* [D1BN4_17, Satellite#0] */
  /*   223 */  /* [D1BN4_44, Satellite#0] */
  /*   224 */  /* [D1BN4_45, Satellite#0] */
  /*   225 */  /* [D1BN4_46, Satellite#0] */
  /*   226 */  /* [D1BN4_49, Satellite#0] */
  /*   227 */  /* [D1BN4_94, Satellite#0] */
  /*   228 */  /* [D1BN9_16, Satellite#0] */
  /*   229 */  /* [D1BN9_17, Satellite#0] */
  /*   230 */  /* [D1BN9_44, Satellite#0] */
  /*   231 */  /* [D1BN9_45, Satellite#0] */
  /*   232 */  /* [D1BN9_46, Satellite#0] */
  /*   233 */  /* [D1BN9_49, Satellite#0] */
  /*   234 */  /* [D1BN9_94, Satellite#0] */
  /*   235 */  /* [D1BOG_16, Satellite#0] */
  /*   236 */  /* [D1BOG_17, Satellite#0] */
  /*   237 */  /* [D1BOG_46, Satellite#0] */
  /*   238 */  /* [D1BOG_94, Satellite#0] */
  /*   239 */  /* [D1BOH_16, Satellite#0] */
  /*   240 */  /* [D1BOH_17, Satellite#0] */
  /*   241 */  /* [D1BOH_44, Satellite#0] */
  /*   242 */  /* [D1BOH_45, Satellite#0] */
  /*   243 */  /* [D1BOH_46, Satellite#0] */
  /*   244 */  /* [D1BOH_49, Satellite#0] */
  /*   245 */  /* [D1BOH_94, Satellite#0] */
  /*   246 */  /* [D1BOI_16, Satellite#0] */
  /*   247 */  /* [D1BOI_17, Satellite#0] */
  /*   248 */  /* [D1BOI_46, Satellite#0] */
  /*   249 */  /* [D1BOI_63, Satellite#0] */
  /* Index        Referable Keys  */
  /*   250 */  /* [D1BOI_94, Satellite#0] */
  /*   251 */  /* [D1BOV_63, Satellite#0] */
  /*   252 */  /* [D1BUO_63, Satellite#0] */
  /*   253 */  /* [D1CXA_63, Satellite#0] */
  /*   254 */  /* [D1CXB_63, Satellite#0] */
  /*   255 */  /* [D1CXC_63, Satellite#0] */
  /*   256 */  /* [D1DOO_63, Satellite#0] */
  /*   257 */  /* [D1BK9_87, Satellite#0] */
  /*   258 */  /* [D1BKG_87, Satellite#0] */
  /*   259 */  /* [D1BN8_16, Satellite#0] */
  /*   260 */  /* [D1BN8_17, Satellite#0] */
  /*   261 */  /* [D1BN8_44, Satellite#0] */
  /*   262 */  /* [D1BN8_79, Satellite#0] */
  /*   263 */  /* [D1BOX_4A, Satellite#0] */
  /*   264 */  /* [D1BOY_4A, Satellite#0] */
  /*   265 */  /* [D1BUK_16, Satellite#0] */
  /*   266 */  /* [D1BUK_63, Satellite#0] */
  /*   267 */  /* [D1BUL_31, Satellite#0] */
  /*   268 */  /* [D1BUL_95, Satellite#0] */
  /*   269 */  /* [D1F0B_16, Satellite#0] */
  /*   270 */  /* [D1F0B_17, Satellite#0] */
  /*   271 */  /* [D1F0B_44, Satellite#0] */
  /*   272 */  /* [D1F0B_45, Satellite#0] */
  /*   273 */  /* [D1F0B_46, Satellite#0] */
  /*   274 */  /* [D1F0B_49, Satellite#0] */
  /*   275 */  /* [D1F0B_94, Satellite#0] */
  /*   276 */  /* [D1F0C_87, Satellite#0] */
  /*   277 */  /* [D1A6D_88, Satellite#0] */
  /*   278 */  /* [D1A6E_88, Satellite#0] */
  /*   279 */  /* [D1A6F_88, Satellite#0] */
  /*   280 */  /* [D1A6H_88, Satellite#0] */
  /*   281 */  /* [D1A6L_88, Satellite#0] */
  /*   282 */  /* [D1A7X_67, Satellite#0] */
  /*   283 */  /* [D1ACK_67, Satellite#0] */
  /*   284 */  /* [D1ACM_67, Satellite#0] */
  /*   285 */  /* [D1ACN_67, Satellite#0] */
  /*   286 */  /* [D1AD0_2F, Satellite#0] */
  /*   287 */  /* [D1AD0_4A, Satellite#0] */
  /*   288 */  /* [D1AD0_05, Satellite#0] */
  /*   289 */  /* [D1AD0_29, Satellite#0] */
  /*   290 */  /* [D1AD0_41, Satellite#0] */
  /*   291 */  /* [D1AD0_44, Satellite#0] */
  /*   292 */  /* [D1AD0_45, Satellite#0] */
  /*   293 */  /* [D1AD0_46, Satellite#0] */
  /*   294 */  /* [D1AD0_47, Satellite#0] */
  /*   295 */  /* [D1AD0_48, Satellite#0] */
  /*   296 */  /* [D1AD0_49, Satellite#0] */
  /*   297 */  /* [D1AD0_94, Satellite#0] */
  /*   298 */  /* [D1AD9_1C, Satellite#0] */
  /*   299 */  /* [D1AD9_16, Satellite#0] */
  /* Index        Referable Keys  */
  /*   300 */  /* [D1AD9_17, Satellite#0] */
  /*   301 */  /* [D1BR9_68, Satellite#0] */
  /*   302 */  /* [D1C5I_68, Satellite#0] */
  /*   303 */  /* [D1C18_67, Satellite#0] */
  /*   304 */  /* [D1E0K_11, Satellite#0] */
  /*   305 */  /* [D1E0K_12, Satellite#0] */
  /*   306 */  /* [D1E0K_13, Satellite#0] */
  /*   307 */  /* [D1E0K_16, Satellite#0] */
  /*   308 */  /* [D1E0K_17, Satellite#0] */
  /*   309 */  /* [D1E1Q_11, Satellite#0] */
  /*   310 */  /* [D1E1Q_12, Satellite#0] */
  /*   311 */  /* [D1E1Q_16, Satellite#0] */
  /*   312 */  /* [D1E1Q_17, Satellite#0] */
  /*   313 */  /* [D1E1R_11, Satellite#0] */
  /*   314 */  /* [D1E1R_12, Satellite#0] */
  /*   315 */  /* [D1E1R_16, Satellite#0] */
  /*   316 */  /* [D1E1R_17, Satellite#0] */
  /*   317 */  /* [D1E1S_11, Satellite#0] */
  /*   318 */  /* [D1E1S_12, Satellite#0] */
  /*   319 */  /* [D1E1S_13, Satellite#0] */
  /*   320 */  /* [D1E1S_16, Satellite#0] */
  /*   321 */  /* [D1E1S_17, Satellite#0] */
  /*   322 */  /* [D1E1T_11, Satellite#0] */
  /*   323 */  /* [D1E1T_12, Satellite#0] */
  /*   324 */  /* [D1E1T_13, Satellite#0] */
  /*   325 */  /* [D1E1T_16, Satellite#0] */
  /*   326 */  /* [D1E1T_17, Satellite#0] */
  /*   327 */  /* [D1E1U_11, Satellite#0] */
  /*   328 */  /* [D1E1U_12, Satellite#0] */
  /*   329 */  /* [D1E1U_13, Satellite#0] */
  /*   330 */  /* [D1E1U_16, Satellite#0] */
  /*   331 */  /* [D1E1U_17, Satellite#0] */
  /*   332 */  /* [D1E1V_11, Satellite#0] */
  /*   333 */  /* [D1E1V_12, Satellite#0] */
  /*   334 */  /* [D1E1V_13, Satellite#0] */
  /*   335 */  /* [D1E1V_16, Satellite#0] */
  /*   336 */  /* [D1E1V_17, Satellite#0] */
  /*   337 */  /* [D1E1W_11, Satellite#0] */
  /*   338 */  /* [D1E1W_12, Satellite#0] */
  /*   339 */  /* [D1E1W_13, Satellite#0] */
  /*   340 */  /* [D1E1W_16, Satellite#0] */
  /*   341 */  /* [D1E1W_17, Satellite#0] */
  /*   342 */  /* [D1E1X_11, Satellite#0] */
  /*   343 */  /* [D1E1X_13, Satellite#0] */
  /*   344 */  /* [D1E1Y_11, Satellite#0] */
  /*   345 */  /* [D1E1Y_13, Satellite#0] */
  /*   346 */  /* [D1E1Z_11, Satellite#0] */
  /*   347 */  /* [D1E1Z_13, Satellite#0] */
  /*   348 */  /* [D1E2A_11, Satellite#0] */
  /*   349 */  /* [D1E2A_13, Satellite#0] */
  /* Index        Referable Keys  */
  /*   350 */  /* [D1E2B_11, Satellite#0] */
  /*   351 */  /* [D1E2B_13, Satellite#0] */
  /*   352 */  /* [D1E2C_11, Satellite#0] */
  /*   353 */  /* [D1E2C_13, Satellite#0] */
  /*   354 */  /* [D1E2G_12, Satellite#0] */
  /*   355 */  /* [D1E2G_16, Satellite#0] */
  /*   356 */  /* [D1E2G_17, Satellite#0] */
  /*   357 */  /* [D1E2H_12, Satellite#0] */
  /*   358 */  /* [D1E2H_16, Satellite#0] */
  /*   359 */  /* [D1E2H_17, Satellite#0] */
  /*   360 */  /* [D1E2I_12, Satellite#0] */
  /*   361 */  /* [D1E2I_16, Satellite#0] */
  /*   362 */  /* [D1E2I_17, Satellite#0] */
  /*   363 */  /* [D1E2L_29, Satellite#0] */
  /*   364 */  /* [D1E2M_29, Satellite#0] */
  /*   365 */  /* [D1E4Q_88, Satellite#0] */
  /*   366 */  /* [D1E10_11, Satellite#0] */
  /*   367 */  /* [D1E10_13, Satellite#0] */
  /*   368 */  /* [D1E11_11, Satellite#0] */
  /*   369 */  /* [D1E11_13, Satellite#0] */
  /*   370 */  /* [D1E12_11, Satellite#0] */
  /*   371 */  /* [D1E12_13, Satellite#0] */
  /*   372 */  /* [D1E13_11, Satellite#0] */
  /*   373 */  /* [D1E13_13, Satellite#0] */
  /*   374 */  /* [D1E14_11, Satellite#0] */
  /*   375 */  /* [D1E14_13, Satellite#0] */
  /*   376 */  /* [D1E15_11, Satellite#0] */
  /*   377 */  /* [D1E15_13, Satellite#0] */
  /*   378 */  /* [D1E16_11, Satellite#0] */
  /*   379 */  /* [D1E16_13, Satellite#0] */
  /*   380 */  /* [D1E17_11, Satellite#0] */
  /*   381 */  /* [D1E17_13, Satellite#0] */
  /*   382 */  /* [D1E18_11, Satellite#0] */
  /*   383 */  /* [D1E18_13, Satellite#0] */
  /*   384 */  /* [D1E19_11, Satellite#0] */
  /*   385 */  /* [D1E19_13, Satellite#0] */
  /*   386 */  /* [D1F1A_11, Satellite#0] */
  /*   387 */  /* [D1F1A_13, Satellite#0] */
  /*   388 */  /* [D1F1B_11, Satellite#0] */
  /*   389 */  /* [D1F1B_13, Satellite#0] */
  /*   390 */  /* [D1F1C_11, Satellite#0] */
  /*   391 */  /* [D1F1C_13, Satellite#0] */
  /*   392 */  /* [D1F1F_87, Satellite#0] */
  /*   393 */  /* [D1FM3_11, Satellite#0] */
  /*   394 */  /* [D1FM3_12, Satellite#0] */
  /*   395 */  /* [D1FM3_13, Satellite#0] */
  /*   396 */  /* [D1FM3_98, Satellite#0] */
  /*   397 */  /* [D1FM4_11, Satellite#0] */
  /*   398 */  /* [D1FM4_12, Satellite#0] */
  /*   399 */  /* [D1FM4_13, Satellite#0] */
  /* Index        Referable Keys  */
  /*   400 */  /* [D1FM5_11, Satellite#0] */
  /*   401 */  /* [D1FM5_12, Satellite#0] */
  /*   402 */  /* [D1FM5_13, Satellite#0] */
  /*   403 */  /* [D1FM6_11, Satellite#0] */
  /*   404 */  /* [D1FM6_12, Satellite#0] */
  /*   405 */  /* [D1FM6_13, Satellite#0] */
  /*   406 */  /* [D1FM7_11, Satellite#0] */
  /*   407 */  /* [D1FM7_12, Satellite#0] */
  /*   408 */  /* [D1FM7_13, Satellite#0] */
  /*   409 */  /* [D1A8N_11, Satellite#0] */
  /*   410 */  /* [D1A8O_11, Satellite#0] */
  /*   411 */  /* [D1F0O_1E, Satellite#0] */
  /*   412 */  /* [D1FZ9_1E, Satellite#0] */
  /*   413 */  /* [D1BUP_12, Satellite#0] */
  /*   414 */  /* [D1BZE_12, Satellite#0] */
  /*   415 */  /* [AutoCreatedDemEvent_RamTstDemEventParameterRefs_RAMTST_E_RAM_FAILURE, Satellite#0] */
  /*   416 */  /* [D1AD0_1C, Satellite#0] */
  /*   417 */  /* [D1BOV_56, Satellite#0] */
  /*   418 */  /* [D1E0K_38, Satellite#0] */
  /*   419 */  /* [D1E1Q_19, Satellite#0] */
  /*   420 */  /* [D1E1R_19, Satellite#0] */
  /*   421 */  /* [D1E1S_38, Satellite#0] */
  /*   422 */  /* [D1E2H_38, Satellite#0] */
  /*   423 */  /* [D1E2I_38, Satellite#0] */
  /*   424 */  /* [D1E2G_14, Satellite#0] */
  /*   425 */  /* [D1E2H_14, Satellite#0] */
  /*   426 */  /* [D1E2I_14, Satellite#0] */
  /*   427 */  /* [D1D72_4A, Satellite#0] */
  /*   428 */  /* [D1D72_9A, Satellite#0] */
  /*   429 */  /* [D1D72_44, Satellite#0] */
  /*   430 */  /* [D1D72_45, Satellite#0] */
  /*   431 */  /* [D1D72_46, Satellite#0] */
  /*   432 */  /* [D1D72_49, Satellite#0] */
  /*   433 */  /* [D1D72_55, Satellite#0] */
  /*   434 */  /* [D1D72_92, Satellite#0] */
  /*   435 */  /* [D1D72_96, Satellite#0] */
  /*   436 */  /* [D1D73_01, Satellite#0] */
  /*   437 */  /* [D1D74_01, Satellite#0] */
  /*   438 */  /* [D1D75_01, Satellite#0] */
  /*   439 */  /* [D1D76_01, Satellite#0] */
  /*   440 */  /* [D1E8B_01, Satellite#0] */

#define DEM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_EventInternalStatus
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_EventInternalStatus
  \brief  size = DEM_G_NUMBER_OF_EVENTS
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
volatile VAR(Dem_Event_InternalStatusType, DEM_VAR_NOINIT) Dem_Cfg_EventInternalStatus[441];
  /* Index        Referable Keys  */
  /*     0 */  /* [#EVENT_INVALID, Satellite#0] */
  /*     1 */  /* [DemEventParameter, Satellite#0] */
  /*     2 */  /* [AutoCreatedDemEvent_AdcDemEventParameterRefs_ADC_E_TIMEOUT, Satellite#0] */
  /*     3 */  /* [AutoCreatedDemEvent_ALLOW_COM_CN_FMSNet_fce1aae5_BswMReportFailToDemRef, Satellite#0] */
  /*     4 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  /*     5 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  /*     6 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  /*     7 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  /*     8 */  /* [AutoCreatedDemEvent_CC_DisableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  /*     9 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  /*    10 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, Satellite#0] */
  /*    11 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  /*    12 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, Satellite#0] */
  /*    13 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  /*    14 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, Satellite#0] */
  /*    15 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  /*    16 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, Satellite#0] */
  /*    17 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, Satellite#0] */
  /*    18 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, Satellite#0] */
  /*    19 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, Satellite#0] */
  /*    20 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, Satellite#0] */
  /*    21 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, Satellite#0] */
  /*    22 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, Satellite#0] */
  /*    23 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, Satellite#0] */
  /*    24 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, Satellite#0] */
  /*    25 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, Satellite#0] */
  /*    26 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, Satellite#0] */
  /*    27 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, Satellite#0] */
  /*    28 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, Satellite#0] */
  /*    29 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  /*    30 */  /* [AutoCreatedDemEvent_CC_DisablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, Satellite#0] */
  /*    31 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  /*    32 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  /*    33 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  /*    34 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  /*    35 */  /* [AutoCreatedDemEvent_CC_EnableDM_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  /*    36 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776_BswMReportFailToDemRef, Satellite#0] */
  /*    37 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone1J1939_Tx_0x31_BC_3782f500_BswMReportFailToDemRef, Satellite#0] */
  /*    38 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Rx_4e624434_BswMReportFailToDemRef, Satellite#0] */
  /*    39 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oBackbone2_Tx_1838e3b2_BswMReportFailToDemRef, Satellite#0] */
  /*    40 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Rx_063a5fbc_BswMReportFailToDemRef, Satellite#0] */
  /*    41 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oCabSubnet_Tx_5060f83a_BswMReportFailToDemRef, Satellite#0] */
  /*    42 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Rx_BC_dd2c1510_BswMReportFailToDemRef, Satellite#0] */
  /*    43 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88_BswMReportFailToDemRef, Satellite#0] */
  /*    44 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878_BswMReportFailToDemRef, Satellite#0] */
  /*    45 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d_BswMReportFailToDemRef, Satellite#0] */
  /*    46 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Rx_dd181faa_BswMReportFailToDemRef, Satellite#0] */
  /*    47 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN00_Tx_8b42b82c_BswMReportFailToDemRef, Satellite#0] */
  /*    48 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Rx_65a478cf_BswMReportFailToDemRef, Satellite#0] */
  /*    49 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN01_Tx_33fedf49_BswMReportFailToDemRef, Satellite#0] */
  /* Index        Referable Keys  */
  /*    50 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Rx_7711d721_BswMReportFailToDemRef, Satellite#0] */
  /*    51 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN02_Tx_214b70a7_BswMReportFailToDemRef, Satellite#0] */
  /*    52 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Rx_cfadb044_BswMReportFailToDemRef, Satellite#0] */
  /*    53 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN03_Tx_99f717c2_BswMReportFailToDemRef, Satellite#0] */
  /*    54 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Rx_527a88fd_BswMReportFailToDemRef, Satellite#0] */
  /*    55 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oLIN04_Tx_04202f7b_BswMReportFailToDemRef, Satellite#0] */
  /*    56 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc_BswMReportFailToDemRef, Satellite#0] */
  /*    57 */  /* [AutoCreatedDemEvent_CC_EnablePDUGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a_BswMReportFailToDemRef, Satellite#0] */
  /*    58 */  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFa_540060b1, Satellite#0] */
  /*    59 */  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, Satellite#0] */
  /*    60 */  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFailToDemRef, Satellite#0] */
  /*    61 */  /* [AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, Satellite#0] */
  /*    62 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone1, Satellite#0] */
  /*    63 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_SecuritySubnet, Satellite#0] */
  /*    64 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_Backbone2, Satellite#0] */
  /*    65 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_CabSubnet, Satellite#0] */
  /*    66 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_BUS_OFF_FMSNet, Satellite#0] */
  /*    67 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone1, Satellite#0] */
  /*    68 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_SecuritySubnet, Satellite#0] */
  /*    69 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_Backbone2, Satellite#0] */
  /*    70 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_CabSubnet, Satellite#0] */
  /*    71 */  /* [AutoCreatedDemEvent_CanSMDemEventParameterRefs_CANSM_E_MODE_REQUEST_TIMEOUT_FMSNet, Satellite#0] */
  /*    72 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, Satellite#0] */
  /*    73 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef_1, Satellite#0] */
  /*    74 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, Satellite#0] */
  /*    75 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_Backbone2_78967e2c_BswMReportFailToDemRef_1, Satellite#0] */
  /*    76 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, Satellite#0] */
  /*    77 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef_1, Satellite#0] */
  /*    78 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, Satellite#0] */
  /*    79 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef_1, Satellite#0] */
  /*    80 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef, Satellite#0] */
  /*    81 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN01_5bde9749_BswMReportFailToDemRef_1, Satellite#0] */
  /*    82 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, Satellite#0] */
  /*    83 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef_1, Satellite#0] */
  /*    84 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, Satellite#0] */
  /*    85 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN03_b5d0f665_BswMReportFailToDemRef_1, Satellite#0] */
  /*    86 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, Satellite#0] */
  /*    87 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_LIN04_2bb463c6_BswMReportFailToDemRef_1, Satellite#0] */
  /*    88 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, Satellite#0] */
  /*    89 */  /* [AutoCreatedDemEvent_ESH_Action_ComMAllow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef_1, Satellite#0] */
  /*    90 */  /* [AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef, Satellite#0] */
  /*    91 */  /* [AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_1, Satellite#0] */
  /*    92 */  /* [AutoCreatedDemEvent_ESH_Action_ComMCheckPendingRequests_BswMReportFailToDemRef_2, Satellite#0] */
  /*    93 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone1J1939_0b1f4bae_BswMReportFailToDemRef, Satellite#0] */
  /*    94 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_Backbone2_78967e2c_BswMReportFailToDemRef, Satellite#0] */
  /*    95 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_CabSubnet_9ea693f1_BswMReportFailToDemRef, Satellite#0] */
  /*    96 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN00_2cd9a7df_BswMReportFailToDemRef, Satellite#0] */
  /*    97 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN01_5bde9749_BswMReportFailToDemRef, Satellite#0] */
  /*    98 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN02_c2d7c6f3_BswMReportFailToDemRef, Satellite#0] */
  /*    99 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN03_b5d0f665_BswMReportFailToDemRef, Satellite#0] */
  /* Index        Referable Keys  */
  /*   100 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_LIN04_2bb463c6_BswMReportFailToDemRef, Satellite#0] */
  /*   101 */  /* [AutoCreatedDemEvent_ESH_Action_ComMDisallow_CN_SecuritySubnet_e7a0ee54_BswMReportFailToDemRef, Satellite#0] */
  /*   102 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_PostRun_BswMReportFailToDemRef, Satellite#0] */
  /*   103 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef, Satellite#0] */
  /*   104 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_PrepShutdown_BswMReportFailToDemRef_1, Satellite#0] */
  /*   105 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef, Satellite#0] */
  /*   106 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_Run_BswMReportFailToDemRef_1, Satellite#0] */
  /*   107 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_WaitForNvm_BswMReportFailToDemRef, Satellite#0] */
  /*   108 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef, Satellite#0] */
  /*   109 */  /* [AutoCreatedDemEvent_ESH_Action_ESH_Wakeup_BswMReportFailToDemRef_1, Satellite#0] */
  /*   110 */  /* [AutoCreatedDemEvent_ESH_Action_EcuMClearValidatedWakeupEvents_BswMReportFailToDemRef, Satellite#0] */
  /*   111 */  /* [AutoCreatedDemEvent_ESH_Action_EcuMGoToSelectedShutdownTarget_BswMReportFailToDemRef, Satellite#0] */
  /*   112 */  /* [AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef, Satellite#0] */
  /*   113 */  /* [AutoCreatedDemEvent_ESH_Action_EnterExclusiveArea_BswMReportFailToDemRef_1, Satellite#0] */
  /*   114 */  /* [AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef, Satellite#0] */
  /*   115 */  /* [AutoCreatedDemEvent_ESH_Action_ExitExclusiveArea_BswMReportFailToDemRef_1, Satellite#0] */
  /*   116 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterPostRun_BswMReportFailToDemRef, Satellite#0] */
  /*   117 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef, Satellite#0] */
  /*   118 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterPrepShutdown_BswMReportFailToDemRef_1, Satellite#0] */
  /*   119 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef, Satellite#0] */
  /*   120 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterRun_BswMReportFailToDemRef_1, Satellite#0] */
  /*   121 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterShutdown_BswMReportFailToDemRef, Satellite#0] */
  /*   122 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterWaitForNvm_BswMReportFailToDemRef, Satellite#0] */
  /*   123 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef, Satellite#0] */
  /*   124 */  /* [AutoCreatedDemEvent_ESH_Action_OnEnterWakeup_BswMReportFailToDemRef_1, Satellite#0] */
  /*   125 */  /* [AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef, Satellite#0] */
  /*   126 */  /* [AutoCreatedDemEvent_ESH_Action_SelfRunRequestTimer_Start_BswMReportFailToDemRef_1, Satellite#0] */
  /*   127 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchPostRun_BswMReportFailToDemRef, Satellite#0] */
  /*   128 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef, Satellite#0] */
  /*   129 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchRun_BswMReportFailToDemRef_1, Satellite#0] */
  /*   130 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef, Satellite#0] */
  /*   131 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchShutdown_BswMReportFailToDemRef_1, Satellite#0] */
  /*   132 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef, Satellite#0] */
  /*   133 */  /* [AutoCreatedDemEvent_ESH_Action_SwitchWakeup_BswMReportFailToDemRef_1, Satellite#0] */
  /*   134 */  /* [AutoCreatedDemEvent_ESH_PostRunNested_BswMReportFailToDemRef, Satellite#0] */
  /*   135 */  /* [AutoCreatedDemEvent_ESH_PostRunToPrepNested_BswMReportFailToDemRef, Satellite#0] */
  /*   136 */  /* [AutoCreatedDemEvent_ESH_RunToPostRunNested_BswMReportFailToDemRef, Satellite#0] */
  /*   137 */  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_ALL_RUN_REQUESTS_KILLED, Satellite#0] */
  /*   138 */  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_CONFIGURATION_DATA_INCONSISTENT, Satellite#0] */
  /*   139 */  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_IMPROPER_CALLER, Satellite#0] */
  /*   140 */  /* [AutoCreatedDemEvent_EcuMDemEventParameterRefs_ECUM_E_RAM_CHECK_FAILED, Satellite#0] */
  /*   141 */  /* [AutoCreatedDemEvent_GptDemEventParameterRefs_GPT_E_TIMEOUT, Satellite#0] */
  /*   142 */  /* [AutoCreatedDemEvent_INIT_Action_CanIf_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   143 */  /* [AutoCreatedDemEvent_INIT_Action_CanNm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   144 */  /* [AutoCreatedDemEvent_INIT_Action_CanSM_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   145 */  /* [AutoCreatedDemEvent_INIT_Action_CanTp_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   146 */  /* [AutoCreatedDemEvent_INIT_Action_Can_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   147 */  /* [AutoCreatedDemEvent_INIT_Action_ComM_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   148 */  /* [AutoCreatedDemEvent_INIT_Action_Com_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   149 */  /* [AutoCreatedDemEvent_INIT_Action_Dcm_Init_BswMReportFailToDemRef, Satellite#0] */
  /* Index        Referable Keys  */
  /*   150 */  /* [AutoCreatedDemEvent_INIT_Action_EnableInterrupts_BswMReportFailToDemRef, Satellite#0] */
  /*   151 */  /* [AutoCreatedDemEvent_INIT_Action_Issm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   152 */  /* [AutoCreatedDemEvent_INIT_Action_J1939Nm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   153 */  /* [AutoCreatedDemEvent_INIT_Action_J1939Rm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   154 */  /* [AutoCreatedDemEvent_INIT_Action_J1939Tp_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   155 */  /* [AutoCreatedDemEvent_INIT_Action_LinIf_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   156 */  /* [AutoCreatedDemEvent_INIT_Action_LinSM_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   157 */  /* [AutoCreatedDemEvent_INIT_Action_LinTp_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   158 */  /* [AutoCreatedDemEvent_INIT_Action_Lin_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   159 */  /* [AutoCreatedDemEvent_INIT_Action_Nm_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   160 */  /* [AutoCreatedDemEvent_INIT_Action_NvMReadAll_BswMReportFailToDemRef, Satellite#0] */
  /*   161 */  /* [AutoCreatedDemEvent_INIT_Action_PduR_Init_BswMReportFailToDemRef, Satellite#0] */
  /*   162 */  /* [AutoCreatedDemEvent_INIT_Action_RteStart_BswMReportFailToDemRef, Satellite#0] */
  /*   163 */  /* [AutoCreatedDemEvent_J1939NmDemEventParameterRefs_J1939NM_E_ADDRESS_LOST, Satellite#0] */
  /*   164 */  /* [AutoCreatedDemEvent_J1939TpConfiguration_J1939TP_E_COMMUNICATION, Satellite#0] */
  /*   165 */  /* [AutoCreatedDemEvent_LinDemEventParameterRefs_LIN_E_TIMEOUT, Satellite#0] */
  /*   166 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_BUS, Satellite#0] */
  /*   167 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_DESCRIPTOR, Satellite#0] */
  /*   168 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_ECC, Satellite#0] */
  /*   169 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_INCONSISTENCY, Satellite#0] */
  /*   170 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_PRIORITY, Satellite#0] */
  /*   171 */  /* [AutoCreatedDemEvent_MclDemEventParameterRefs_MCL_E_DMA_UNRECOGNIZED, Satellite#0] */
  /*   172 */  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_CLOCK_FAILURE, Satellite#0] */
  /*   173 */  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_INVALIDMODE_CONFIG, Satellite#0] */
  /*   174 */  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_SSCM_CER_FAILURE, Satellite#0] */
  /*   175 */  /* [AutoCreatedDemEvent_McuDemEventParameterRefs_MCU_E_TIMEOUT_FAILURE, Satellite#0] */
  /*   176 */  /* [AutoCreatedDemEvent_SpiDemEventParameterRefs_SPI_E_HARDWARE_ERROR, Satellite#0] */
  /*   177 */  /* [AutoCreatedDemEvent_SpiDriver_SPI_E_HARDWARE_ERROR, Satellite#0] */
  /*   178 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_CORRUPT_CONFIG, Satellite#0] */
  /*   179 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_DISABLE_REJECTED, Satellite#0] */
  /*   180 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_FORBIDDEN_INVOCATION, Satellite#0] */
  /*   181 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_CALL, Satellite#0] */
  /*   182 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_INVALID_PARAMETER, Satellite#0] */
  /*   183 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_MODE_FAILED, Satellite#0] */
  /*   184 */  /* [AutoCreatedDemEvent_WdgDemEventParameterRefs_WDG_E_UNLOCKED, Satellite#0] */
  /*   185 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_INTEGRITY_FAILED, Satellite#0] */
  /*   186 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_LOSS_OF_REDUNDANCY, Satellite#0] */
  /*   187 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_QUEUE_OVERFLOW, Satellite#0] */
  /*   188 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_REQ_FAILED, Satellite#0] */
  /*   189 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_VERIFY_FAILED, Satellite#0] */
  /*   190 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRITE_PROTECTED, Satellite#0] */
  /*   191 */  /* [AutoCreatedDemEvent_NvmDemEventParameterRefs_NVM_E_WRONG_BLOCK_ID, Satellite#0] */
  /*   192 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_COMPARE_FAILED, Satellite#0] */
  /*   193 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_ERASE_FAILED, Satellite#0] */
  /*   194 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_READ_FAILED, Satellite#0] */
  /*   195 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_UNEXPECTED_FLASH_ID, Satellite#0] */
  /*   196 */  /* [AutoCreatedDemEvent_FlsDemEventParameterRefs_FLS_E_WRITE_FAILED, Satellite#0] */
  /*   197 */  /* [DEM_EVENT_StartApplication, Satellite#0] */
  /*   198 */  /* [D1BJO_88, Satellite#0] */
  /*   199 */  /* [D1BJP_88, Satellite#0] */
  /* Index        Referable Keys  */
  /*   200 */  /* [D1BJQ_88, Satellite#0] */
  /*   201 */  /* [D1BJR_88, Satellite#0] */
  /*   202 */  /* [D1BJS_88, Satellite#0] */
  /*   203 */  /* [D1BKB_87, Satellite#0] */
  /*   204 */  /* [D1BKC_87, Satellite#0] */
  /*   205 */  /* [D1BKD_87, Satellite#0] */
  /*   206 */  /* [D1BKE_87, Satellite#0] */
  /*   207 */  /* [D1BKF_87, Satellite#0] */
  /*   208 */  /* [D1BKH_87, Satellite#0] */
  /*   209 */  /* [D1BN1_16, Satellite#0] */
  /*   210 */  /* [D1BN1_17, Satellite#0] */
  /*   211 */  /* [D1BN1_44, Satellite#0] */
  /*   212 */  /* [D1BN1_45, Satellite#0] */
  /*   213 */  /* [D1BN1_46, Satellite#0] */
  /*   214 */  /* [D1BN1_94, Satellite#0] */
  /*   215 */  /* [D1BN2_16, Satellite#0] */
  /*   216 */  /* [D1BN2_17, Satellite#0] */
  /*   217 */  /* [D1BN2_44, Satellite#0] */
  /*   218 */  /* [D1BN2_45, Satellite#0] */
  /*   219 */  /* [D1BN2_46, Satellite#0] */
  /*   220 */  /* [D1BN2_94, Satellite#0] */
  /*   221 */  /* [D1BN4_16, Satellite#0] */
  /*   222 */  /* [D1BN4_17, Satellite#0] */
  /*   223 */  /* [D1BN4_44, Satellite#0] */
  /*   224 */  /* [D1BN4_45, Satellite#0] */
  /*   225 */  /* [D1BN4_46, Satellite#0] */
  /*   226 */  /* [D1BN4_49, Satellite#0] */
  /*   227 */  /* [D1BN4_94, Satellite#0] */
  /*   228 */  /* [D1BN9_16, Satellite#0] */
  /*   229 */  /* [D1BN9_17, Satellite#0] */
  /*   230 */  /* [D1BN9_44, Satellite#0] */
  /*   231 */  /* [D1BN9_45, Satellite#0] */
  /*   232 */  /* [D1BN9_46, Satellite#0] */
  /*   233 */  /* [D1BN9_49, Satellite#0] */
  /*   234 */  /* [D1BN9_94, Satellite#0] */
  /*   235 */  /* [D1BOG_16, Satellite#0] */
  /*   236 */  /* [D1BOG_17, Satellite#0] */
  /*   237 */  /* [D1BOG_46, Satellite#0] */
  /*   238 */  /* [D1BOG_94, Satellite#0] */
  /*   239 */  /* [D1BOH_16, Satellite#0] */
  /*   240 */  /* [D1BOH_17, Satellite#0] */
  /*   241 */  /* [D1BOH_44, Satellite#0] */
  /*   242 */  /* [D1BOH_45, Satellite#0] */
  /*   243 */  /* [D1BOH_46, Satellite#0] */
  /*   244 */  /* [D1BOH_49, Satellite#0] */
  /*   245 */  /* [D1BOH_94, Satellite#0] */
  /*   246 */  /* [D1BOI_16, Satellite#0] */
  /*   247 */  /* [D1BOI_17, Satellite#0] */
  /*   248 */  /* [D1BOI_46, Satellite#0] */
  /*   249 */  /* [D1BOI_63, Satellite#0] */
  /* Index        Referable Keys  */
  /*   250 */  /* [D1BOI_94, Satellite#0] */
  /*   251 */  /* [D1BOV_63, Satellite#0] */
  /*   252 */  /* [D1BUO_63, Satellite#0] */
  /*   253 */  /* [D1CXA_63, Satellite#0] */
  /*   254 */  /* [D1CXB_63, Satellite#0] */
  /*   255 */  /* [D1CXC_63, Satellite#0] */
  /*   256 */  /* [D1DOO_63, Satellite#0] */
  /*   257 */  /* [D1BK9_87, Satellite#0] */
  /*   258 */  /* [D1BKG_87, Satellite#0] */
  /*   259 */  /* [D1BN8_16, Satellite#0] */
  /*   260 */  /* [D1BN8_17, Satellite#0] */
  /*   261 */  /* [D1BN8_44, Satellite#0] */
  /*   262 */  /* [D1BN8_79, Satellite#0] */
  /*   263 */  /* [D1BOX_4A, Satellite#0] */
  /*   264 */  /* [D1BOY_4A, Satellite#0] */
  /*   265 */  /* [D1BUK_16, Satellite#0] */
  /*   266 */  /* [D1BUK_63, Satellite#0] */
  /*   267 */  /* [D1BUL_31, Satellite#0] */
  /*   268 */  /* [D1BUL_95, Satellite#0] */
  /*   269 */  /* [D1F0B_16, Satellite#0] */
  /*   270 */  /* [D1F0B_17, Satellite#0] */
  /*   271 */  /* [D1F0B_44, Satellite#0] */
  /*   272 */  /* [D1F0B_45, Satellite#0] */
  /*   273 */  /* [D1F0B_46, Satellite#0] */
  /*   274 */  /* [D1F0B_49, Satellite#0] */
  /*   275 */  /* [D1F0B_94, Satellite#0] */
  /*   276 */  /* [D1F0C_87, Satellite#0] */
  /*   277 */  /* [D1A6D_88, Satellite#0] */
  /*   278 */  /* [D1A6E_88, Satellite#0] */
  /*   279 */  /* [D1A6F_88, Satellite#0] */
  /*   280 */  /* [D1A6H_88, Satellite#0] */
  /*   281 */  /* [D1A6L_88, Satellite#0] */
  /*   282 */  /* [D1A7X_67, Satellite#0] */
  /*   283 */  /* [D1ACK_67, Satellite#0] */
  /*   284 */  /* [D1ACM_67, Satellite#0] */
  /*   285 */  /* [D1ACN_67, Satellite#0] */
  /*   286 */  /* [D1AD0_2F, Satellite#0] */
  /*   287 */  /* [D1AD0_4A, Satellite#0] */
  /*   288 */  /* [D1AD0_05, Satellite#0] */
  /*   289 */  /* [D1AD0_29, Satellite#0] */
  /*   290 */  /* [D1AD0_41, Satellite#0] */
  /*   291 */  /* [D1AD0_44, Satellite#0] */
  /*   292 */  /* [D1AD0_45, Satellite#0] */
  /*   293 */  /* [D1AD0_46, Satellite#0] */
  /*   294 */  /* [D1AD0_47, Satellite#0] */
  /*   295 */  /* [D1AD0_48, Satellite#0] */
  /*   296 */  /* [D1AD0_49, Satellite#0] */
  /*   297 */  /* [D1AD0_94, Satellite#0] */
  /*   298 */  /* [D1AD9_1C, Satellite#0] */
  /*   299 */  /* [D1AD9_16, Satellite#0] */
  /* Index        Referable Keys  */
  /*   300 */  /* [D1AD9_17, Satellite#0] */
  /*   301 */  /* [D1BR9_68, Satellite#0] */
  /*   302 */  /* [D1C5I_68, Satellite#0] */
  /*   303 */  /* [D1C18_67, Satellite#0] */
  /*   304 */  /* [D1E0K_11, Satellite#0] */
  /*   305 */  /* [D1E0K_12, Satellite#0] */
  /*   306 */  /* [D1E0K_13, Satellite#0] */
  /*   307 */  /* [D1E0K_16, Satellite#0] */
  /*   308 */  /* [D1E0K_17, Satellite#0] */
  /*   309 */  /* [D1E1Q_11, Satellite#0] */
  /*   310 */  /* [D1E1Q_12, Satellite#0] */
  /*   311 */  /* [D1E1Q_16, Satellite#0] */
  /*   312 */  /* [D1E1Q_17, Satellite#0] */
  /*   313 */  /* [D1E1R_11, Satellite#0] */
  /*   314 */  /* [D1E1R_12, Satellite#0] */
  /*   315 */  /* [D1E1R_16, Satellite#0] */
  /*   316 */  /* [D1E1R_17, Satellite#0] */
  /*   317 */  /* [D1E1S_11, Satellite#0] */
  /*   318 */  /* [D1E1S_12, Satellite#0] */
  /*   319 */  /* [D1E1S_13, Satellite#0] */
  /*   320 */  /* [D1E1S_16, Satellite#0] */
  /*   321 */  /* [D1E1S_17, Satellite#0] */
  /*   322 */  /* [D1E1T_11, Satellite#0] */
  /*   323 */  /* [D1E1T_12, Satellite#0] */
  /*   324 */  /* [D1E1T_13, Satellite#0] */
  /*   325 */  /* [D1E1T_16, Satellite#0] */
  /*   326 */  /* [D1E1T_17, Satellite#0] */
  /*   327 */  /* [D1E1U_11, Satellite#0] */
  /*   328 */  /* [D1E1U_12, Satellite#0] */
  /*   329 */  /* [D1E1U_13, Satellite#0] */
  /*   330 */  /* [D1E1U_16, Satellite#0] */
  /*   331 */  /* [D1E1U_17, Satellite#0] */
  /*   332 */  /* [D1E1V_11, Satellite#0] */
  /*   333 */  /* [D1E1V_12, Satellite#0] */
  /*   334 */  /* [D1E1V_13, Satellite#0] */
  /*   335 */  /* [D1E1V_16, Satellite#0] */
  /*   336 */  /* [D1E1V_17, Satellite#0] */
  /*   337 */  /* [D1E1W_11, Satellite#0] */
  /*   338 */  /* [D1E1W_12, Satellite#0] */
  /*   339 */  /* [D1E1W_13, Satellite#0] */
  /*   340 */  /* [D1E1W_16, Satellite#0] */
  /*   341 */  /* [D1E1W_17, Satellite#0] */
  /*   342 */  /* [D1E1X_11, Satellite#0] */
  /*   343 */  /* [D1E1X_13, Satellite#0] */
  /*   344 */  /* [D1E1Y_11, Satellite#0] */
  /*   345 */  /* [D1E1Y_13, Satellite#0] */
  /*   346 */  /* [D1E1Z_11, Satellite#0] */
  /*   347 */  /* [D1E1Z_13, Satellite#0] */
  /*   348 */  /* [D1E2A_11, Satellite#0] */
  /*   349 */  /* [D1E2A_13, Satellite#0] */
  /* Index        Referable Keys  */
  /*   350 */  /* [D1E2B_11, Satellite#0] */
  /*   351 */  /* [D1E2B_13, Satellite#0] */
  /*   352 */  /* [D1E2C_11, Satellite#0] */
  /*   353 */  /* [D1E2C_13, Satellite#0] */
  /*   354 */  /* [D1E2G_12, Satellite#0] */
  /*   355 */  /* [D1E2G_16, Satellite#0] */
  /*   356 */  /* [D1E2G_17, Satellite#0] */
  /*   357 */  /* [D1E2H_12, Satellite#0] */
  /*   358 */  /* [D1E2H_16, Satellite#0] */
  /*   359 */  /* [D1E2H_17, Satellite#0] */
  /*   360 */  /* [D1E2I_12, Satellite#0] */
  /*   361 */  /* [D1E2I_16, Satellite#0] */
  /*   362 */  /* [D1E2I_17, Satellite#0] */
  /*   363 */  /* [D1E2L_29, Satellite#0] */
  /*   364 */  /* [D1E2M_29, Satellite#0] */
  /*   365 */  /* [D1E4Q_88, Satellite#0] */
  /*   366 */  /* [D1E10_11, Satellite#0] */
  /*   367 */  /* [D1E10_13, Satellite#0] */
  /*   368 */  /* [D1E11_11, Satellite#0] */
  /*   369 */  /* [D1E11_13, Satellite#0] */
  /*   370 */  /* [D1E12_11, Satellite#0] */
  /*   371 */  /* [D1E12_13, Satellite#0] */
  /*   372 */  /* [D1E13_11, Satellite#0] */
  /*   373 */  /* [D1E13_13, Satellite#0] */
  /*   374 */  /* [D1E14_11, Satellite#0] */
  /*   375 */  /* [D1E14_13, Satellite#0] */
  /*   376 */  /* [D1E15_11, Satellite#0] */
  /*   377 */  /* [D1E15_13, Satellite#0] */
  /*   378 */  /* [D1E16_11, Satellite#0] */
  /*   379 */  /* [D1E16_13, Satellite#0] */
  /*   380 */  /* [D1E17_11, Satellite#0] */
  /*   381 */  /* [D1E17_13, Satellite#0] */
  /*   382 */  /* [D1E18_11, Satellite#0] */
  /*   383 */  /* [D1E18_13, Satellite#0] */
  /*   384 */  /* [D1E19_11, Satellite#0] */
  /*   385 */  /* [D1E19_13, Satellite#0] */
  /*   386 */  /* [D1F1A_11, Satellite#0] */
  /*   387 */  /* [D1F1A_13, Satellite#0] */
  /*   388 */  /* [D1F1B_11, Satellite#0] */
  /*   389 */  /* [D1F1B_13, Satellite#0] */
  /*   390 */  /* [D1F1C_11, Satellite#0] */
  /*   391 */  /* [D1F1C_13, Satellite#0] */
  /*   392 */  /* [D1F1F_87, Satellite#0] */
  /*   393 */  /* [D1FM3_11, Satellite#0] */
  /*   394 */  /* [D1FM3_12, Satellite#0] */
  /*   395 */  /* [D1FM3_13, Satellite#0] */
  /*   396 */  /* [D1FM3_98, Satellite#0] */
  /*   397 */  /* [D1FM4_11, Satellite#0] */
  /*   398 */  /* [D1FM4_12, Satellite#0] */
  /*   399 */  /* [D1FM4_13, Satellite#0] */
  /* Index        Referable Keys  */
  /*   400 */  /* [D1FM5_11, Satellite#0] */
  /*   401 */  /* [D1FM5_12, Satellite#0] */
  /*   402 */  /* [D1FM5_13, Satellite#0] */
  /*   403 */  /* [D1FM6_11, Satellite#0] */
  /*   404 */  /* [D1FM6_12, Satellite#0] */
  /*   405 */  /* [D1FM6_13, Satellite#0] */
  /*   406 */  /* [D1FM7_11, Satellite#0] */
  /*   407 */  /* [D1FM7_12, Satellite#0] */
  /*   408 */  /* [D1FM7_13, Satellite#0] */
  /*   409 */  /* [D1A8N_11, Satellite#0] */
  /*   410 */  /* [D1A8O_11, Satellite#0] */
  /*   411 */  /* [D1F0O_1E, Satellite#0] */
  /*   412 */  /* [D1FZ9_1E, Satellite#0] */
  /*   413 */  /* [D1BUP_12, Satellite#0] */
  /*   414 */  /* [D1BZE_12, Satellite#0] */
  /*   415 */  /* [AutoCreatedDemEvent_RamTstDemEventParameterRefs_RAMTST_E_RAM_FAILURE, Satellite#0] */
  /*   416 */  /* [D1AD0_1C, Satellite#0] */
  /*   417 */  /* [D1BOV_56, Satellite#0] */
  /*   418 */  /* [D1E0K_38, Satellite#0] */
  /*   419 */  /* [D1E1Q_19, Satellite#0] */
  /*   420 */  /* [D1E1R_19, Satellite#0] */
  /*   421 */  /* [D1E1S_38, Satellite#0] */
  /*   422 */  /* [D1E2H_38, Satellite#0] */
  /*   423 */  /* [D1E2I_38, Satellite#0] */
  /*   424 */  /* [D1E2G_14, Satellite#0] */
  /*   425 */  /* [D1E2H_14, Satellite#0] */
  /*   426 */  /* [D1E2I_14, Satellite#0] */
  /*   427 */  /* [D1D72_4A, Satellite#0] */
  /*   428 */  /* [D1D72_9A, Satellite#0] */
  /*   429 */  /* [D1D72_44, Satellite#0] */
  /*   430 */  /* [D1D72_45, Satellite#0] */
  /*   431 */  /* [D1D72_46, Satellite#0] */
  /*   432 */  /* [D1D72_49, Satellite#0] */
  /*   433 */  /* [D1D72_55, Satellite#0] */
  /*   434 */  /* [D1D72_92, Satellite#0] */
  /*   435 */  /* [D1D72_96, Satellite#0] */
  /*   436 */  /* [D1D73_01, Satellite#0] */
  /*   437 */  /* [D1D74_01, Satellite#0] */
  /*   438 */  /* [D1D75_01, Satellite#0] */
  /*   439 */  /* [D1D76_01, Satellite#0] */
  /*   440 */  /* [D1E8B_01, Satellite#0] */

#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_FilterInfoTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_FilterInfoTable
  \brief  size = DEM_CFG_NUMBER_OF_FILTER
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_FilterData_InfoType, DEM_VAR_NOINIT) Dem_Cfg_FilterInfoTable[1];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_FreezeFrameIteratorTable
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_FreezeFrameIteratorTable
  \brief  size = DEM_CFG_NUMBER_OF_FREEZEFRAMEITERATORS
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_FreezeFrameIterator_FilterType, DEM_VAR_NOINIT) Dem_Cfg_FreezeFrameIteratorTable[1];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_GlobalIndicatorStates
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_GlobalIndicatorStates
  \brief  size = DEM_CFG_GLOBAL_INDICATOR_COUNT, inner dimension size = DEM_G_MAX_NUMBER_J1939_NODES
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_GlobalIndicatorStatesType, DEM_VAR_NOINIT) Dem_Cfg_GlobalIndicatorStates[1];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_IndicatorBlinking
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_IndicatorBlinking
  \brief  size = DEM_CFG_GLOBAL_INDICATOR_COUNT, inner dimension size = DEM_G_MAX_NUMBER_J1939_NODES
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_IndicatorBlinkingType, DEM_VAR_NOINIT) Dem_Cfg_IndicatorBlinking[1];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_IndicatorContinuous
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_IndicatorContinuous
  \brief  size = DEM_CFG_GLOBAL_INDICATOR_COUNT, inner dimension size = DEM_G_MAX_NUMBER_J1939_NODES
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_IndicatorContinuousType, DEM_VAR_NOINIT) Dem_Cfg_IndicatorContinuous[1];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryCommitNumber
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryCommitNumber
  \brief  The array contains these items: Admin, Status, 10 * Primary
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
volatile VAR(Dem_Cfg_MemoryCommitNumberType, DEM_VAR_NOINIT) Dem_Cfg_MemoryCommitNumber[12];
#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_MemoryStatus
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_MemoryStatus
  \brief  The array contains these items: Admin, Status, 10 * Primary
*/ 
#define DEM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_MemoryStatusType, DEM_VAR_NOINIT) Dem_Cfg_MemoryStatus[12];
#define DEM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryChronology
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_PrimaryChronology
  \brief  size = DEM_CFG_GLOBAL_PRIMARY_SIZE
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_MemoryIndexType, DEM_VAR_NOINIT) Dem_Cfg_PrimaryChronology[10];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_0
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_0;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_1
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_1;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_2
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_2;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_3
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_3;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_4
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_4;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_5
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_5;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_6
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_6;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_7
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_7;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_8
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_8;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_PrimaryEntry_9
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_PrimaryEntryType, DEM_NVM_DATA_NOINIT) Dem_Cfg_PrimaryEntry_9;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_ReadoutBuffer
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_ReadoutBufferEntryType, DEM_VAR_NOINIT) Dem_Cfg_ReadoutBuffer[1];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_ReportedEventsOfFilter
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_ReportedEventsOfFilter
  \brief  size = DEM_CFG_NUMBER_OF_FILTER
*/ 
#define DEM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_ReportedEventsType, DEM_VAR_NOINIT) Dem_Cfg_ReportedEventsOfFilter[1];
#define DEM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_SatelliteInfo0
**********************************************************************************************************************/
/** 
  \var    Dem_Cfg_SatelliteInfo0
  \brief  Buffer for satellite data on OsApplication "0"
*/ 
#define DEM_START_SEC_0_VAR_ZERO_INIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_SatelliteInfoType0, DEM_VAR_ZERO_INIT) Dem_Cfg_SatelliteInfo0 = {0uL};
#define DEM_STOP_SEC_0_VAR_ZERO_INIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  Dem_Cfg_StatusData
**********************************************************************************************************************/
#define DEM_START_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(Dem_Cfg_StatusDataType, DEM_NVM_DATA_NOINIT) Dem_Cfg_StatusData;
#define DEM_STOP_SEC_VAR_SAVED_ZONE0_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/



/**********************************************************************************************************************
  LOCAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  LOCAL FUNCTIONS
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL FUNCTIONS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/



/**********************************************************************************************************************
  END OF FILE: Dem_Lcfg.c     [VolvoAB (Vector), VARIANT-PRE-COMPILE, 16.06.00.107213]
**********************************************************************************************************************/

