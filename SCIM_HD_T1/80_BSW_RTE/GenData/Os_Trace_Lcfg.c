/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Trace_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:34
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_TRACE_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_Trace_Lcfg.h"
#include "Os_Trace.h"

/* Os kernel module dependencies */
#include "Os_Common_Types.h"

/* Os hal dependencies */


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

#define OS_START_SEC_PUBLIC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Dynamic trace data: OsCore0 */
VAR(Os_TraceCoreType, OS_VAR_NOINIT) OsCfg_Trace_OsCore0_Dyn;

/*! Dynamic trace data: ASW_10ms_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_ASW_10ms_Task_Dyn;

/*! Dynamic trace data: ASW_20ms_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_ASW_20ms_Task_Dyn;

/*! Dynamic trace data: ASW_Async_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_ASW_Async_Task_Dyn;

/*! Dynamic trace data: ASW_Init_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_ASW_Init_Task_Dyn;

/*! Dynamic trace data: BSW_10ms_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_BSW_10ms_Task_Dyn;

/*! Dynamic trace data: BSW_5ms_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_BSW_5ms_Task_Dyn;

/*! Dynamic trace data: BSW_Async_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_BSW_Async_Task_Dyn;

/*! Dynamic trace data: BSW_Diag_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_BSW_Diag_Task_Dyn;

/*! Dynamic trace data: BSW_Lin_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_BSW_Lin_Task_Dyn;

/*! Dynamic trace data: CpuLoadIdleTask */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CpuLoadIdleTask_Dyn;

/*! Dynamic trace data: IdleTask_OsCore0 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_IdleTask_OsCore0_Dyn;

/*! Dynamic trace data: Init_Task */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Init_Task_Dyn;

/*! Dynamic trace data: CanIsr_0_MB00To03 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_0_MB00To03_Dyn;

/*! Dynamic trace data: CanIsr_0_MB04To07 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_0_MB04To07_Dyn;

/*! Dynamic trace data: CanIsr_0_MB08To11 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_0_MB08To11_Dyn;

/*! Dynamic trace data: CanIsr_0_MB12To15 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_0_MB12To15_Dyn;

/*! Dynamic trace data: CanIsr_0_MB16To31 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_0_MB16To31_Dyn;

/*! Dynamic trace data: CanIsr_0_MB32To63 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_0_MB32To63_Dyn;

/*! Dynamic trace data: CanIsr_0_MB64To95 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_0_MB64To95_Dyn;

/*! Dynamic trace data: CanIsr_1_MB00To03 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_1_MB00To03_Dyn;

/*! Dynamic trace data: CanIsr_1_MB04To07 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_1_MB04To07_Dyn;

/*! Dynamic trace data: CanIsr_1_MB08To11 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_1_MB08To11_Dyn;

/*! Dynamic trace data: CanIsr_1_MB12To15 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_1_MB12To15_Dyn;

/*! Dynamic trace data: CanIsr_1_MB16To31 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_1_MB16To31_Dyn;

/*! Dynamic trace data: CanIsr_1_MB32To63 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_1_MB32To63_Dyn;

/*! Dynamic trace data: CanIsr_1_MB64To95 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_1_MB64To95_Dyn;

/*! Dynamic trace data: CanIsr_2_MB00To03 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_2_MB00To03_Dyn;

/*! Dynamic trace data: CanIsr_2_MB04To07 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_2_MB04To07_Dyn;

/*! Dynamic trace data: CanIsr_2_MB08To11 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_2_MB08To11_Dyn;

/*! Dynamic trace data: CanIsr_2_MB12To15 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_2_MB12To15_Dyn;

/*! Dynamic trace data: CanIsr_2_MB16To31 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_2_MB16To31_Dyn;

/*! Dynamic trace data: CanIsr_2_MB32To63 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_2_MB32To63_Dyn;

/*! Dynamic trace data: CanIsr_2_MB64To95 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_2_MB64To95_Dyn;

/*! Dynamic trace data: CanIsr_4_MB00To03 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_4_MB00To03_Dyn;

/*! Dynamic trace data: CanIsr_4_MB04To07 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_4_MB04To07_Dyn;

/*! Dynamic trace data: CanIsr_4_MB08To11 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_4_MB08To11_Dyn;

/*! Dynamic trace data: CanIsr_4_MB12To15 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_4_MB12To15_Dyn;

/*! Dynamic trace data: CanIsr_4_MB16To31 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_4_MB16To31_Dyn;

/*! Dynamic trace data: CanIsr_4_MB32To63 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_4_MB32To63_Dyn;

/*! Dynamic trace data: CanIsr_4_MB64To95 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_4_MB64To95_Dyn;

/*! Dynamic trace data: CanIsr_6_MB00To03 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_6_MB00To03_Dyn;

/*! Dynamic trace data: CanIsr_6_MB04To07 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_6_MB04To07_Dyn;

/*! Dynamic trace data: CanIsr_6_MB08To11 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_6_MB08To11_Dyn;

/*! Dynamic trace data: CanIsr_6_MB12To15 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_6_MB12To15_Dyn;

/*! Dynamic trace data: CanIsr_6_MB16To31 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_6_MB16To31_Dyn;

/*! Dynamic trace data: CanIsr_6_MB32To63 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_6_MB32To63_Dyn;

/*! Dynamic trace data: CanIsr_6_MB64To95 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_6_MB64To95_Dyn;

/*! Dynamic trace data: CanIsr_7_MB00To03 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_7_MB00To03_Dyn;

/*! Dynamic trace data: CanIsr_7_MB04To07 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_7_MB04To07_Dyn;

/*! Dynamic trace data: CanIsr_7_MB08To11 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_7_MB08To11_Dyn;

/*! Dynamic trace data: CanIsr_7_MB12To15 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_7_MB12To15_Dyn;

/*! Dynamic trace data: CanIsr_7_MB16To31 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_7_MB16To31_Dyn;

/*! Dynamic trace data: CanIsr_7_MB32To63 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_7_MB32To63_Dyn;

/*! Dynamic trace data: CanIsr_7_MB64To95 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CanIsr_7_MB64To95_Dyn;

/*! Dynamic trace data: CounterIsr_SystemTimer */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_CounterIsr_SystemTimer_Dyn;

/*! Dynamic trace data: DOWHS1_EMIOS0_CH3_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_DOWHS1_EMIOS0_CH3_ISR_Dyn;

/*! Dynamic trace data: DOWHS2_EMIOS0_CH5_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_DOWHS2_EMIOS0_CH5_ISR_Dyn;

/*! Dynamic trace data: DOWLS2_EMIOS0_CH13_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_DOWLS2_EMIOS0_CH13_ISR_Dyn;

/*! Dynamic trace data: DOWLS2_EMIOS0_CH9_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_DOWLS2_EMIOS0_CH9_ISR_Dyn;

/*! Dynamic trace data: DOWLS3_EMIOS0_CH14_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_DOWLS3_EMIOS0_CH14_ISR_Dyn;

/*! Dynamic trace data: Gpt_PIT_0_TIMER_0_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Gpt_PIT_0_TIMER_0_ISR_Dyn;

/*! Dynamic trace data: Gpt_PIT_0_TIMER_1_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Gpt_PIT_0_TIMER_1_ISR_Dyn;

/*! Dynamic trace data: Gpt_PIT_0_TIMER_2_ISR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Gpt_PIT_0_TIMER_2_ISR_Dyn;

/*! Dynamic trace data: Lin_Channel_0_ERR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_0_ERR_Dyn;

/*! Dynamic trace data: Lin_Channel_0_RXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_0_RXI_Dyn;

/*! Dynamic trace data: Lin_Channel_0_TXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_0_TXI_Dyn;

/*! Dynamic trace data: Lin_Channel_10_ERR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_10_ERR_Dyn;

/*! Dynamic trace data: Lin_Channel_10_RXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_10_RXI_Dyn;

/*! Dynamic trace data: Lin_Channel_10_TXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_10_TXI_Dyn;

/*! Dynamic trace data: Lin_Channel_1_ERR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_1_ERR_Dyn;

/*! Dynamic trace data: Lin_Channel_1_RXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_1_RXI_Dyn;

/*! Dynamic trace data: Lin_Channel_1_TXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_1_TXI_Dyn;

/*! Dynamic trace data: Lin_Channel_4_ERR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_4_ERR_Dyn;

/*! Dynamic trace data: Lin_Channel_4_RXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_4_RXI_Dyn;

/*! Dynamic trace data: Lin_Channel_4_TXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_4_TXI_Dyn;

/*! Dynamic trace data: Lin_Channel_6_ERR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_6_ERR_Dyn;

/*! Dynamic trace data: Lin_Channel_6_RXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_6_RXI_Dyn;

/*! Dynamic trace data: Lin_Channel_6_TXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_6_TXI_Dyn;

/*! Dynamic trace data: Lin_Channel_7_ERR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_7_ERR_Dyn;

/*! Dynamic trace data: Lin_Channel_7_RXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_7_RXI_Dyn;

/*! Dynamic trace data: Lin_Channel_7_TXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_7_TXI_Dyn;

/*! Dynamic trace data: Lin_Channel_8_ERR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_8_ERR_Dyn;

/*! Dynamic trace data: Lin_Channel_8_RXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_8_RXI_Dyn;

/*! Dynamic trace data: Lin_Channel_8_TXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_8_TXI_Dyn;

/*! Dynamic trace data: Lin_Channel_9_ERR */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_9_ERR_Dyn;

/*! Dynamic trace data: Lin_Channel_9_RXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_9_RXI_Dyn;

/*! Dynamic trace data: Lin_Channel_9_TXI */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_Lin_Channel_9_TXI_Dyn;

/*! Dynamic trace data: MCU_PLL_LossOfLock */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_MCU_PLL_LossOfLock_Dyn;

/*! Dynamic trace data: WKUP_IRQ0 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_WKUP_IRQ0_Dyn;

/*! Dynamic trace data: WKUP_IRQ1 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_WKUP_IRQ1_Dyn;

/*! Dynamic trace data: WKUP_IRQ2 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_WKUP_IRQ2_Dyn;

/*! Dynamic trace data: WKUP_IRQ3 */
OS_LOCAL VAR(Os_TraceThreadType, OS_VAR_NOINIT) OsCfg_Trace_WKUP_IRQ3_Dyn;

#define OS_STOP_SEC_PUBLIC_CORE0_VAR_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! Trace configuration data: ASW_10ms_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASW_10ms_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_ASW_10ms_Task_Dyn,
  /* .Id   = */ Os_TraceId_ASW_10ms_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: ASW_20ms_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASW_20ms_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_ASW_20ms_Task_Dyn,
  /* .Id   = */ Os_TraceId_ASW_20ms_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: ASW_Async_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASW_Async_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_ASW_Async_Task_Dyn,
  /* .Id   = */ Os_TraceId_ASW_Async_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: ASW_Init_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_ASW_Init_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_ASW_Init_Task_Dyn,
  /* .Id   = */ Os_TraceId_ASW_Init_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: BSW_10ms_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_10ms_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_BSW_10ms_Task_Dyn,
  /* .Id   = */ Os_TraceId_BSW_10ms_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: BSW_5ms_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_5ms_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_BSW_5ms_Task_Dyn,
  /* .Id   = */ Os_TraceId_BSW_5ms_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: BSW_Async_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_Async_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_BSW_Async_Task_Dyn,
  /* .Id   = */ Os_TraceId_BSW_Async_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: BSW_Diag_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_Diag_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_BSW_Diag_Task_Dyn,
  /* .Id   = */ Os_TraceId_BSW_Diag_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: BSW_Lin_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_BSW_Lin_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_BSW_Lin_Task_Dyn,
  /* .Id   = */ Os_TraceId_BSW_Lin_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: CpuLoadIdleTask */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CpuLoadIdleTask =
{
  /* .Dyn  = */ &OsCfg_Trace_CpuLoadIdleTask_Dyn,
  /* .Id   = */ Os_TraceId_CpuLoadIdleTask,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: IdleTask_OsCore0 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_IdleTask_OsCore0 =
{
  /* .Dyn  = */ &OsCfg_Trace_IdleTask_OsCore0_Dyn,
  /* .Id   = */ Os_TraceId_IdleTask_OsCore0,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: Init_Task */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Init_Task =
{
  /* .Dyn  = */ &OsCfg_Trace_Init_Task_Dyn,
  /* .Id   = */ Os_TraceId_Init_Task,
  /* .Type = */ OS_TRACE_THREAD_TYPE_TASK
};

/*! Trace configuration data: CanIsr_0_MB00To03 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB00To03 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_0_MB00To03_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_0_MB00To03,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_0_MB04To07 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB04To07 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_0_MB04To07_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_0_MB04To07,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_0_MB08To11 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB08To11 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_0_MB08To11_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_0_MB08To11,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_0_MB12To15 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB12To15 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_0_MB12To15_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_0_MB12To15,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_0_MB16To31 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB16To31 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_0_MB16To31_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_0_MB16To31,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_0_MB32To63 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB32To63 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_0_MB32To63_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_0_MB32To63,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_0_MB64To95 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_0_MB64To95 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_0_MB64To95_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_0_MB64To95,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_1_MB00To03 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB00To03 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_1_MB00To03_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_1_MB00To03,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_1_MB04To07 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB04To07 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_1_MB04To07_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_1_MB04To07,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_1_MB08To11 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB08To11 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_1_MB08To11_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_1_MB08To11,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_1_MB12To15 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB12To15 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_1_MB12To15_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_1_MB12To15,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_1_MB16To31 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB16To31 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_1_MB16To31_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_1_MB16To31,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_1_MB32To63 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB32To63 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_1_MB32To63_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_1_MB32To63,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_1_MB64To95 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_1_MB64To95 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_1_MB64To95_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_1_MB64To95,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_2_MB00To03 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB00To03 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_2_MB00To03_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_2_MB00To03,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_2_MB04To07 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB04To07 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_2_MB04To07_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_2_MB04To07,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_2_MB08To11 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB08To11 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_2_MB08To11_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_2_MB08To11,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_2_MB12To15 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB12To15 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_2_MB12To15_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_2_MB12To15,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_2_MB16To31 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB16To31 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_2_MB16To31_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_2_MB16To31,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_2_MB32To63 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB32To63 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_2_MB32To63_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_2_MB32To63,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_2_MB64To95 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_2_MB64To95 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_2_MB64To95_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_2_MB64To95,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_4_MB00To03 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB00To03 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_4_MB00To03_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_4_MB00To03,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_4_MB04To07 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB04To07 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_4_MB04To07_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_4_MB04To07,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_4_MB08To11 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB08To11 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_4_MB08To11_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_4_MB08To11,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_4_MB12To15 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB12To15 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_4_MB12To15_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_4_MB12To15,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_4_MB16To31 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB16To31 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_4_MB16To31_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_4_MB16To31,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_4_MB32To63 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB32To63 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_4_MB32To63_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_4_MB32To63,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_4_MB64To95 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_4_MB64To95 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_4_MB64To95_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_4_MB64To95,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_6_MB00To03 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB00To03 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_6_MB00To03_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_6_MB00To03,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_6_MB04To07 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB04To07 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_6_MB04To07_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_6_MB04To07,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_6_MB08To11 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB08To11 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_6_MB08To11_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_6_MB08To11,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_6_MB12To15 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB12To15 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_6_MB12To15_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_6_MB12To15,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_6_MB16To31 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB16To31 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_6_MB16To31_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_6_MB16To31,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_6_MB32To63 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB32To63 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_6_MB32To63_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_6_MB32To63,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_6_MB64To95 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_6_MB64To95 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_6_MB64To95_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_6_MB64To95,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_7_MB00To03 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB00To03 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_7_MB00To03_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_7_MB00To03,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_7_MB04To07 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB04To07 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_7_MB04To07_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_7_MB04To07,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_7_MB08To11 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB08To11 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_7_MB08To11_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_7_MB08To11,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_7_MB12To15 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB12To15 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_7_MB12To15_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_7_MB12To15,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_7_MB16To31 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB16To31 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_7_MB16To31_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_7_MB16To31,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_7_MB32To63 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB32To63 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_7_MB32To63_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_7_MB32To63,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CanIsr_7_MB64To95 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CanIsr_7_MB64To95 =
{
  /* .Dyn  = */ &OsCfg_Trace_CanIsr_7_MB64To95_Dyn,
  /* .Id   = */ Os_TraceId_CanIsr_7_MB64To95,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: CounterIsr_SystemTimer */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_CounterIsr_SystemTimer =
{
  /* .Dyn  = */ &OsCfg_Trace_CounterIsr_SystemTimer_Dyn,
  /* .Id   = */ Os_TraceId_CounterIsr_SystemTimer,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: DOWHS1_EMIOS0_CH3_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWHS1_EMIOS0_CH3_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_DOWHS1_EMIOS0_CH3_ISR_Dyn,
  /* .Id   = */ Os_TraceId_DOWHS1_EMIOS0_CH3_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: DOWHS2_EMIOS0_CH5_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWHS2_EMIOS0_CH5_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_DOWHS2_EMIOS0_CH5_ISR_Dyn,
  /* .Id   = */ Os_TraceId_DOWHS2_EMIOS0_CH5_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: DOWLS2_EMIOS0_CH13_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWLS2_EMIOS0_CH13_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_DOWLS2_EMIOS0_CH13_ISR_Dyn,
  /* .Id   = */ Os_TraceId_DOWLS2_EMIOS0_CH13_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: DOWLS2_EMIOS0_CH9_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWLS2_EMIOS0_CH9_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_DOWLS2_EMIOS0_CH9_ISR_Dyn,
  /* .Id   = */ Os_TraceId_DOWLS2_EMIOS0_CH9_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: DOWLS3_EMIOS0_CH14_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_DOWLS3_EMIOS0_CH14_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_DOWLS3_EMIOS0_CH14_ISR_Dyn,
  /* .Id   = */ Os_TraceId_DOWLS3_EMIOS0_CH14_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Gpt_PIT_0_TIMER_0_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Gpt_PIT_0_TIMER_0_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_Gpt_PIT_0_TIMER_0_ISR_Dyn,
  /* .Id   = */ Os_TraceId_Gpt_PIT_0_TIMER_0_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Gpt_PIT_0_TIMER_1_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Gpt_PIT_0_TIMER_1_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_Gpt_PIT_0_TIMER_1_ISR_Dyn,
  /* .Id   = */ Os_TraceId_Gpt_PIT_0_TIMER_1_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Gpt_PIT_0_TIMER_2_ISR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Gpt_PIT_0_TIMER_2_ISR =
{
  /* .Dyn  = */ &OsCfg_Trace_Gpt_PIT_0_TIMER_2_ISR_Dyn,
  /* .Id   = */ Os_TraceId_Gpt_PIT_0_TIMER_2_ISR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_0_ERR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_0_ERR =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_0_ERR_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_0_ERR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_0_RXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_0_RXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_0_RXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_0_RXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_0_TXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_0_TXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_0_TXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_0_TXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_10_ERR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_10_ERR =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_10_ERR_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_10_ERR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_10_RXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_10_RXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_10_RXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_10_RXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_10_TXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_10_TXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_10_TXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_10_TXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_1_ERR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_1_ERR =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_1_ERR_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_1_ERR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_1_RXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_1_RXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_1_RXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_1_RXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_1_TXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_1_TXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_1_TXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_1_TXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_4_ERR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_4_ERR =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_4_ERR_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_4_ERR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_4_RXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_4_RXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_4_RXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_4_RXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_4_TXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_4_TXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_4_TXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_4_TXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_6_ERR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_6_ERR =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_6_ERR_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_6_ERR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_6_RXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_6_RXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_6_RXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_6_RXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_6_TXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_6_TXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_6_TXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_6_TXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_7_ERR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_7_ERR =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_7_ERR_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_7_ERR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_7_RXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_7_RXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_7_RXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_7_RXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_7_TXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_7_TXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_7_TXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_7_TXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_8_ERR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_8_ERR =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_8_ERR_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_8_ERR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_8_RXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_8_RXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_8_RXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_8_RXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_8_TXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_8_TXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_8_TXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_8_TXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_9_ERR */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_9_ERR =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_9_ERR_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_9_ERR,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_9_RXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_9_RXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_9_RXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_9_RXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: Lin_Channel_9_TXI */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_Lin_Channel_9_TXI =
{
  /* .Dyn  = */ &OsCfg_Trace_Lin_Channel_9_TXI_Dyn,
  /* .Id   = */ Os_TraceId_Lin_Channel_9_TXI,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: MCU_PLL_LossOfLock */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_MCU_PLL_LossOfLock =
{
  /* .Dyn  = */ &OsCfg_Trace_MCU_PLL_LossOfLock_Dyn,
  /* .Id   = */ Os_TraceId_MCU_PLL_LossOfLock,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: WKUP_IRQ0 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_WKUP_IRQ0 =
{
  /* .Dyn  = */ &OsCfg_Trace_WKUP_IRQ0_Dyn,
  /* .Id   = */ Os_TraceId_WKUP_IRQ0,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: WKUP_IRQ1 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_WKUP_IRQ1 =
{
  /* .Dyn  = */ &OsCfg_Trace_WKUP_IRQ1_Dyn,
  /* .Id   = */ Os_TraceId_WKUP_IRQ1,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: WKUP_IRQ2 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_WKUP_IRQ2 =
{
  /* .Dyn  = */ &OsCfg_Trace_WKUP_IRQ2_Dyn,
  /* .Id   = */ Os_TraceId_WKUP_IRQ2,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

/*! Trace configuration data: WKUP_IRQ3 */
CONST(Os_TraceThreadConfigType, OS_CONST) OsCfg_Trace_WKUP_IRQ3 =
{
  /* .Dyn  = */ &OsCfg_Trace_WKUP_IRQ3_Dyn,
  /* .Id   = */ Os_TraceId_WKUP_IRQ3,
  /* .Type = */ OS_TRACE_THREAD_TYPE_ISR
};

#define OS_STOP_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  END OF FILE: Os_Trace_Lcfg.c
 *********************************************************************************************************************/
