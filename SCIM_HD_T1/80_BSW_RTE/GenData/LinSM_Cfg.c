/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: LinSM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: LinSM_Cfg.c
 *   Generation Time: 2020-11-11 14:25:30
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#define LINSM_CFG_SOURCE

/**********************************************************************************************************************
 * MISRA JUSTIFICATION
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "LinSM_Cfg.h"

/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  LinSM_ChannelConfig
**********************************************************************************************************************/
/** 
  \var    LinSM_ChannelConfig
  \details
  Element                Description
  ComMChannelHandle  
  TransceiverHandling
*/ 
#define LINSM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinSM_ChannelConfigType, LINSM_CONST) LinSM_ChannelConfig[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ComMChannelHandle                       TransceiverHandling        Referable Keys */
  { /*     0 */ ComMConf_ComMChannel_CN_LIN01_5bde9749,    LINSM_TRCV_SLEEP },  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_8e3d5be2] */
  { /*     1 */ ComMConf_ComMChannel_CN_LIN04_2bb463c6,    LINSM_TRCV_SLEEP },  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_def0ca51] */
  { /*     2 */ ComMConf_ComMChannel_CN_LIN02_c2d7c6f3,    LINSM_TRCV_SLEEP },  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_08a9294c] */
  { /*     3 */ ComMConf_ComMChannel_CN_LIN00_2cd9a7df,    LINSM_TRCV_SLEEP },  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_45618847] */
  { /*     4 */ ComMConf_ComMChannel_CN_LIN03_b5d0f665,    LINSM_TRCV_SLEEP },  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_c3f5fae9] */
  { /*     5 */ ComMConf_ComMChannel_CN_LIN05_5cb35350,     LINSM_TRCV_NONE },  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_15ac19f4] */
  { /*     6 */ ComMConf_ComMChannel_CN_LIN07_b2bd327c,     LINSM_TRCV_NONE },  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_5864b8ff] */
  { /*     7 */ ComMConf_ComMChannel_CN_LIN06_c5ba02ea,     LINSM_TRCV_NONE }   /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_93386b5a] */
};
#define LINSM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinSM_ComMToLinSMChannel
**********************************************************************************************************************/
#define LINSM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinSM_ComMToLinSMChannelType, LINSM_CONST) LinSM_ComMToLinSMChannel[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     ComMToLinSMChannel               Referable Keys */
  /*     0 */  LINSM_NO_COMMTOLINSMCHANNEL,  /* [No LinSM Channel] */
  /*     1 */  LINSM_NO_COMMTOLINSMCHANNEL,  /* [No LinSM Channel] */
  /*     2 */  LINSM_NO_COMMTOLINSMCHANNEL,  /* [No LinSM Channel] */
  /*     3 */  LINSM_NO_COMMTOLINSMCHANNEL,  /* [No LinSM Channel] */
  /*     4 */  LINSM_NO_COMMTOLINSMCHANNEL,  /* [No LinSM Channel] */
  /*     5 */  LINSM_NO_COMMTOLINSMCHANNEL,  /* [No LinSM Channel] */
  /*     6 */                           3u,  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN00_2cd9a7df] */
  /*     7 */                           0u,  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN01_5bde9749] */
  /*     8 */                           2u,  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN02_c2d7c6f3] */
  /*     9 */                           4u,  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN03_b5d0f665] */
  /*    10 */                           1u,  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN04_2bb463c6] */
  /*    11 */                           5u,  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN05_5cb35350] */
  /*    12 */                           7u,  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN06_c5ba02ea] */
  /*    13 */                           6u   /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN07_b2bd327c] */
};
#define LINSM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinSM_ComState
**********************************************************************************************************************/
#define LINSM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinSM_ComStateUType, LINSM_VAR_NOINIT) LinSM_ComState;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_8e3d5be2] */
  /*     1 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_def0ca51] */
  /*     2 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_08a9294c] */
  /*     3 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_45618847] */
  /*     4 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_c3f5fae9] */
  /*     5 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_15ac19f4] */
  /*     6 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_5864b8ff] */
  /*     7 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_93386b5a] */

#define LINSM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinSM_ConfirmationTimer
**********************************************************************************************************************/
#define LINSM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinSM_ConfirmationTimerUType, LINSM_VAR_NOINIT) LinSM_ConfirmationTimer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_8e3d5be2] */
  /*     1 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_def0ca51] */
  /*     2 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_08a9294c] */
  /*     3 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_45618847] */
  /*     4 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_c3f5fae9] */
  /*     5 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_15ac19f4] */
  /*     6 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_5864b8ff] */
  /*     7 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_93386b5a] */

#define LINSM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinSM_NegativeConfirmation
**********************************************************************************************************************/
#define LINSM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinSM_NegativeConfirmationUType, LINSM_VAR_NOINIT) LinSM_NegativeConfirmation;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_8e3d5be2] */
  /*     1 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_def0ca51] */
  /*     2 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_08a9294c] */
  /*     3 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_45618847] */
  /*     4 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_c3f5fae9] */
  /*     5 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_15ac19f4] */
  /*     6 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_5864b8ff] */
  /*     7 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_93386b5a] */

#define LINSM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinSM_RequestedComMode
**********************************************************************************************************************/
#define LINSM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinSM_RequestedComModeUType, LINSM_VAR_NOINIT) LinSM_RequestedComMode;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_8e3d5be2] */
  /*     1 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_def0ca51] */
  /*     2 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_08a9294c] */
  /*     3 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_45618847] */
  /*     4 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_c3f5fae9] */
  /*     5 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_15ac19f4] */
  /*     6 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_5864b8ff] */
  /*     7 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_93386b5a] */

#define LINSM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinSM_WakeUpRetryCounter
**********************************************************************************************************************/
#define LINSM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinSM_WakeUpRetryCounterUType, LINSM_VAR_NOINIT) LinSM_WakeUpRetryCounter;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_8e3d5be2] */
  /*     1 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_def0ca51] */
  /*     2 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_08a9294c] */
  /*     3 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_45618847] */
  /*     4 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_c3f5fae9] */
  /*     5 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_15ac19f4] */
  /*     6 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_5864b8ff] */
  /*     7 */  /* [/ActiveEcuC/LinSM/LinSMConfigSet/CHNL_93386b5a] */

#define LINSM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */



/**********************************************************************************************************************
  GLOBAL FUNCTIONS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/


/**********************************************************************************************************************
  END OF FILE: LinSM_Cfg.c
**********************************************************************************************************************/

