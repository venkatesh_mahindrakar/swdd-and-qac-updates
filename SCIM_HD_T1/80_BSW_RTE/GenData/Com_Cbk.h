/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Com
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Com_Cbk.h
 *   Generation Time: 2020-11-11 14:25:31
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * WARNING: This code has been generated with reduced-severity errors. 
 * The created output files contain errors that have been ignored. Usage of the created files can lead to unpredictable behavior of the embedded code.
 * Usage of the created files happens at own risk!
 * 
 * [Warning] COM01007 - Inconsistent textual value. 
 * - [Reduced Severity due to User-Defined Parameter] Interpreted array value 0, 0, 0, 0, 0, 0, 0, 0 of string value "0 0 0 0 0 0 0 0" is too long for array signal with signal length 7.
 * Erroneous configuration elements:
 * /ActiveEcuC/Com/ComConfig/Debug_PVT_SCIM_FlexArrayData_oDebug04_CIOM_BB2_oBackbone2_975cada1_Tx[0:ComSignalInitValue](value=0 0 0 0 0 0 0 0) (DefRef: /MICROSAR/Com/ComConfig/ComSignal/ComSignalInitValue)
 *********************************************************************************************************************/

#if !defined (COM_CBK_H)
# define COM_CBK_H

/**********************************************************************************************************************
  MISRA / PClint JUSTIFICATIONS
**********************************************************************************************************************/

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
#include "Com_Cot.h"

/**********************************************************************************************************************
  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/



/**
 * \defgroup ComHandleIdsComRxPdu Handle IDs of handle space ComRxPdu.
 * \brief Rx Pdus
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define ComConf_ComIPdu_ACM_BB1_01P_oBackbone1J1939_57f87ee7_Rx       0u
#define ComConf_ComIPdu_AIR1_X_VMCU_oBackbone1J1939_8c40baab_Rx       1u
#define ComConf_ComIPdu_AMB_X_VMCU_oBackbone1J1939_7e62cfad_Rx        2u
#define ComConf_ComIPdu_Alarm_Sec_02P_oSecuritySubnet_c389f589_Rx     3u
#define ComConf_ComIPdu_Alarm_Sec_03S_oSecuritySubnet_7f4118ec_Rx     4u
#define ComConf_ComIPdu_Alarm_Sec_06S_oSecuritySubnet_9bbb790b_Rx     5u
#define ComConf_ComIPdu_Alarm_Sec_07S_oSecuritySubnet_74e9cfea_Rx     6u
#define ComConf_ComIPdu_AnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_142a14f5_Rx 7u
#define ComConf_ComIPdu_AnmMsg_BBM_Backbone2_oBackbone2_bca080bd_Rx   8u
#define ComConf_ComIPdu_AnmMsg_CCM_CabSubnet_oCabSubnet_2d9f2dda_Rx   9u
#define ComConf_ComIPdu_AnmMsg_DACU_Backbone2_oBackbone2_49517b4d_Rx  10u
#define ComConf_ComIPdu_AnmMsg_DDM_SecuritySubnet_oSecuritySubnet_c5aaf372_Rx 11u
#define ComConf_ComIPdu_AnmMsg_ECUspare1_Backbone2_oBackbone2_90f829ea_Rx 12u
#define ComConf_ComIPdu_AnmMsg_ECUspare2_Backbone2_oBackbone2_a98084aa_Rx 13u
#define ComConf_ComIPdu_AnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_44137fdb_Rx 14u
#define ComConf_ComIPdu_AnmMsg_ECUspare6_CabSubnet_oCabSubnet_7e48353e_Rx 15u
#define ComConf_ComIPdu_AnmMsg_EMS_Backbone2_oBackbone2_99fd5a83_Rx   16u
#define ComConf_ComIPdu_AnmMsg_HMIIOM_Backbone2_oBackbone2_24e5a2f5_Rx 17u
#define ComConf_ComIPdu_AnmMsg_LECM1_CabSubnet_oCabSubnet_10e92ec5_Rx 18u
#define ComConf_ComIPdu_AnmMsg_PDM_SecuritySubnet_oSecuritySubnet_7215ae09_Rx 19u
#define ComConf_ComIPdu_AnmMsg_SRS_CabSubnet_oCabSubnet_aab008be_Rx   20u
#define ComConf_ComIPdu_AnmMsg_TECU_Backbone2_oBackbone2_7840c27c_Rx  21u
#define ComConf_ComIPdu_AnmMsg_VMCU_Backbone2_oBackbone2_3a2bd89f_Rx  22u
#define ComConf_ComIPdu_AnmMsg_WRCS_CabSubnet_oCabSubnet_fa464471_Rx  23u
#define ComConf_ComIPdu_BBM_BB2_01P_oBackbone2_0d63adb7_Rx            24u
#define ComConf_ComIPdu_BBM_BB2_02P_oBackbone2_b0a9c179_Rx            25u
#define ComConf_ComIPdu_BBM_BB2_03S_CIOM_oBackbone2_d5047985_Rx       26u
#define ComConf_ComIPdu_BBM_BB2_06P_oBackbone2_7010abef_Rx            27u
#define ComConf_ComIPdu_CCFWtoCIOM_L4_oLIN03_c78ba01d_Rx              28u
#define ComConf_ComIPdu_CCM_Cab_01P_oCabSubnet_3993b0cf_Rx            29u
#define ComConf_ComIPdu_CCM_Cab_03P_oCabSubnet_59cf0584_Rx            30u
#define ComConf_ComIPdu_CCM_Cab_04P_oCabSubnet_24bc03dc_Rx            31u
#define ComConf_ComIPdu_CCM_Cab_06P_oCabSubnet_44e0b697_Rx            32u
#define ComConf_ComIPdu_CCM_Cab_07P_oCabSubnet_99766f12_Rx            33u
#define ComConf_ComIPdu_CCM_Cab_08P_oCabSubnet_be06ba27_Rx            34u
#define ComConf_ComIPdu_CCVS_X_VMCU_oBackbone1J1939_ebb768c1_Rx       35u
#define ComConf_ComIPdu_CVW_X_EBS_oBackbone1J1939_1a9bdeb1_Rx         36u
#define ComConf_ComIPdu_DACU_BB2_02P_oBackbone2_3e1257a1_Rx           37u
#define ComConf_ComIPdu_DDM_Sec_01P_oSecuritySubnet_40cf4e44_Rx       38u
#define ComConf_ComIPdu_DDM_Sec_03S_oSecuritySubnet_16817e43_Rx       39u
#define ComConf_ComIPdu_DDM_Sec_04S_oSecuritySubnet_f7af7427_Rx       40u
#define ComConf_ComIPdu_DDM_Sec_05S_oSecuritySubnet_18fdc2c6_Rx       41u
#define ComConf_ComIPdu_DI_X_TACHO_oBackbone1J1939_0dca0ef5_Rx        42u
#define ComConf_ComIPdu_DLFWtoCIOM_L4_oLIN03_65509040_Rx              43u
#define ComConf_ComIPdu_DebugCtrl1_CIOM_BB2_oBackbone2_eb80d4ff_Rx    44u
#define ComConf_ComIPdu_DiagFaultStat_Alarm_Sec_oSecuritySubnet_347db5f6_Rx 45u
#define ComConf_ComIPdu_DiagFaultStat_CCM_Cab_oCabSubnet_d4c98b2c_Rx  46u
#define ComConf_ComIPdu_DiagFaultStat_DDM_Sec_oSecuritySubnet_c65980ff_Rx 47u
#define ComConf_ComIPdu_DiagFaultStat_LECM_Cab_oCabSubnet_870f9288_Rx 48u
#define ComConf_ComIPdu_DiagFaultStat_PDM_Sec_oSecuritySubnet_c777a720_Rx 49u
#define ComConf_ComIPdu_DiagFaultStat_SRS_Cab_oCabSubnet_ac384d3f_Rx  50u
#define ComConf_ComIPdu_DiagFaultStat_WRCS_Cab_oCabSubnet_3792ace7_Rx 51u
#define ComConf_ComIPdu_EBC1_X_EBS_oBackbone1J1939_68913e3f_Rx        52u
#define ComConf_ComIPdu_EBC2_X_EBS_oBackbone1J1939_1e740702_Rx        53u
#define ComConf_ComIPdu_EBC5_X_EBS_oBackbone1J1939_68cd9fac_Rx        54u
#define ComConf_ComIPdu_EBS_BB1_01P_oBackbone1J1939_ce636b4a_Rx       55u
#define ComConf_ComIPdu_EBS_BB1_02P_oBackbone1J1939_24e5b628_Rx       56u
#define ComConf_ComIPdu_EBS_BB1_05P_oBackbone1J1939_c5cbbc4c_Rx       57u
#define ComConf_ComIPdu_EEC1_X_EMS_oBackbone1J1939_50be66ff_Rx        58u
#define ComConf_ComIPdu_EEC2_X_EMS_oBackbone1J1939_265b5fc2_Rx        59u
#define ComConf_ComIPdu_ELCP1toCIOM_L4_oLIN03_e80fe278_Rx             60u
#define ComConf_ComIPdu_ELCP2toCIOM_L4_oLIN03_bb95b9fc_Rx             61u
#define ComConf_ComIPdu_EMS_BB2_01P_oBackbone2_dd91fc9b_Rx            62u
#define ComConf_ComIPdu_EMS_BB2_04P_oBackbone2_c0be4f88_Rx            63u
#define ComConf_ComIPdu_EMS_BB2_05P_oBackbone2_1d28960d_Rx            64u
#define ComConf_ComIPdu_EMS_BB2_06P_oBackbone2_a0e2fac3_Rx            65u
#define ComConf_ComIPdu_EMS_BB2_08P_oBackbone2_5a04f673_Rx            66u
#define ComConf_ComIPdu_EMS_BB2_09S_oBackbone2_f00cfd06_Rx            67u
#define ComConf_ComIPdu_EMS_BB2_11S_oBackbone2_3700cf1d_Rx            68u
#define ComConf_ComIPdu_EMS_BB2_13P_oBackbone2_20c2a8a6_Rx            69u
#define ComConf_ComIPdu_ERC1_X_EMSRet_oBackbone1J1939_6b48ff0e_Rx     70u
#define ComConf_ComIPdu_ERC1_X_RECU_oBackbone1J1939_5afd0b6e_Rx       71u
#define ComConf_ComIPdu_ET1_X_EMS_oBackbone1J1939_b4e1bda2_Rx         72u
#define ComConf_ComIPdu_FMS1_X_HMIIOM_oBackbone1J1939_69e6dd44_Rx     73u
#define ComConf_ComIPdu_FSP1_Frame_L1_oLIN00_7e067bd3_Rx              74u
#define ComConf_ComIPdu_FSP1_Frame_L2_oLIN01_878e4ca6_Rx              75u
#define ComConf_ComIPdu_FSP1_Frame_L3_oLIN02_d22d1d82_Rx              76u
#define ComConf_ComIPdu_FSP1_Frame_L4_oLIN03_afef240d_Rx              77u
#define ComConf_ComIPdu_FSP1_Frame_L5_oLIN04_fd21b130_Rx              78u
#define ComConf_ComIPdu_FSP1_SwitchDetResp_L1_oLIN00_db048333_Rx      79u
#define ComConf_ComIPdu_FSP1_SwitchDetResp_L2_oLIN01_228cb446_Rx      80u
#define ComConf_ComIPdu_FSP1_SwitchDetResp_L3_oLIN02_772fe562_Rx      81u
#define ComConf_ComIPdu_FSP1_SwitchDetResp_L4_oLIN03_0aeddced_Rx      82u
#define ComConf_ComIPdu_FSP1_SwitchDetResp_L5_oLIN04_582349d0_Rx      83u
#define ComConf_ComIPdu_FSP2_Frame_L1_oLIN00_2d9c2057_Rx              84u
#define ComConf_ComIPdu_FSP2_Frame_L2_oLIN01_d4141722_Rx              85u
#define ComConf_ComIPdu_FSP2_Frame_L3_oLIN02_81b74606_Rx              86u
#define ComConf_ComIPdu_FSP2_SwitchDetResp_L1_oLIN00_eee93560_Rx      87u
#define ComConf_ComIPdu_FSP2_SwitchDetResp_L2_oLIN01_17610215_Rx      88u
#define ComConf_ComIPdu_FSP2_SwitchDetResp_L3_oLIN02_42c25331_Rx      89u
#define ComConf_ComIPdu_FSP3_Frame_L2_oLIN01_53b2dc61_Rx              90u
#define ComConf_ComIPdu_FSP3_SwitchDetResp_L2_oLIN01_b2ea921b_Rx      91u
#define ComConf_ComIPdu_FSP4_Frame_L2_oLIN01_7320a02a_Rx              92u
#define ComConf_ComIPdu_FSP4_SwitchDetResp_L2_oLIN01_7cba6eb3_Rx      93u
#define ComConf_ComIPdu_HMIIOM_BB2_01P_oBackbone2_6b8c9af6_Rx         94u
#define ComConf_ComIPdu_HMIIOM_BB2_02P_oBackbone2_d646f638_Rx         95u
#define ComConf_ComIPdu_HMIIOM_BB2_03P_oBackbone2_0bd02fbd_Rx         96u
#define ComConf_ComIPdu_HMIIOM_BB2_04S_oBackbone2_013dfb15_Rx         97u
#define ComConf_ComIPdu_HMIIOM_BB2_05P_oBackbone2_ab35f060_Rx         98u
#define ComConf_ComIPdu_HMIIOM_BB2_06S_oBackbone2_61614e5e_Rx         99u
#define ComConf_ComIPdu_HMIIOM_BB2_07P_oBackbone2_cb69452b_Rx         100u
#define ComConf_ComIPdu_HMIIOM_BB2_08P_oBackbone2_ec19901e_Rx         101u
#define ComConf_ComIPdu_HMIIOM_BB2_09P_oBackbone2_318f499b_Rx         102u
#define ComConf_ComIPdu_HMIIOM_BB2_10P_oBackbone2_2b15a205_Rx         103u
#define ComConf_ComIPdu_HMIIOM_BB2_11P_oBackbone2_f6837b80_Rx         104u
#define ComConf_ComIPdu_HMIIOM_BB2_12P_oBackbone2_4b49174e_Rx         105u
#define ComConf_ComIPdu_HMIIOM_BB2_13P_oBackbone2_96dfcecb_Rx         106u
#define ComConf_ComIPdu_HMIIOM_BB2_14P_oBackbone2_ebacc893_Rx         107u
#define ComConf_ComIPdu_HMIIOM_BB2_15P_oBackbone2_363a1116_Rx         108u
#define ComConf_ComIPdu_HMIIOM_BB2_16P_oBackbone2_8bf07dd8_Rx         109u
#define ComConf_ComIPdu_HMIIOM_BB2_17P_oBackbone2_5666a45d_Rx         110u
#define ComConf_ComIPdu_HMIIOM_BB2_18P_oBackbone2_71167168_Rx         111u
#define ComConf_ComIPdu_HMIIOM_BB2_19P_CIOM_oBackbone2_da8f8d8b_Rx    112u
#define ComConf_ComIPdu_HMIIOM_BB2_20S_oBackbone2_20ea552e_Rx         113u
#define ComConf_ComIPdu_HMIIOM_BB2_21S_oBackbone2_fd7c8cab_Rx         114u
#define ComConf_ComIPdu_HMIIOM_BB2_22P_oBackbone2_37283295_Rx         115u
#define ComConf_ComIPdu_HMIIOM_BB2_23P_oBackbone2_eabeeb10_Rx         116u
#define ComConf_ComIPdu_HMIIOM_BB2_24P_oBackbone2_97cded48_Rx         117u
#define ComConf_ComIPdu_HMIIOM_BB2_25P_oBackbone2_4a5b34cd_Rx         118u
#define ComConf_ComIPdu_HMIIOM_BB2_27S_oBackbone2_5d995376_Rx         119u
#define ComConf_ComIPdu_HMIIOM_BB2_33P_oBackbone2_77b10a66_Rx         120u
#define ComConf_ComIPdu_HMIIOM_BB2_34S_oBackbone2_7d5cdece_Rx         121u
#define ComConf_ComIPdu_HMIIOM_BB2_35S_oBackbone2_a0ca074b_Rx         122u
#define ComConf_ComIPdu_HMIIOM_BB2_36S_oBackbone2_1d006b85_Rx         123u
#define ComConf_ComIPdu_HMIIOM_BB2_38P_oBackbone2_9078b5c5_Rx         124u
#define ComConf_ComIPdu_HMIIOM_BB2_39P_oBackbone2_4dee6c40_Rx         125u
#define ComConf_ComIPdu_HRLFC_X_EMS_oBackbone1J1939_e671f1b7_Rx       126u
#define ComConf_ComIPdu_ILCP1toCIOM_L1_oLIN00_0eedb06b_Rx             127u
#define ComConf_ComIPdu_ILCP2toCIOM_L4_oLIN03_8c9eb431_Rx             128u
#define ComConf_ComIPdu_LECM1_Cab_02P_oCabSubnet_d045028e_Rx          129u
#define ComConf_ComIPdu_LECM1_Cab_03S_oCabSubnet_7a4d09fb_Rx          130u
#define ComConf_ComIPdu_LECM1_Cab_04P_oCabSubnet_70a0dd53_Rx          131u
#define ComConf_ComIPdu_LECM1_Cab_05S_oCabSubnet_daa8d626_Rx          132u
#define ComConf_ComIPdu_LECM2toCIOM_FR1_L1_oLIN00_3ccb97ef_Rx         133u
#define ComConf_ComIPdu_LECM2toCIOM_FR2_L1_oLIN00_a529f1ee_Rx         134u
#define ComConf_ComIPdu_LECM2toCIOM_FR3_L1_oLIN00_64a72e2e_Rx         135u
#define ComConf_ComIPdu_LECMBasic2CIOM_L1_oLIN00_da4f3dd9_Rx          136u
#define ComConf_ComIPdu_LFE_X_EMS_oBackbone1J1939_9d303f5e_Rx         137u
#define ComConf_ComIPdu_LinSlave_Frame_L6_oLIN05_ff935eac_Rx          138u
#define ComConf_ComIPdu_LinSlave_Frame_L7_oLIN06_aa300f88_Rx          139u
#define ComConf_ComIPdu_LinSlave_Frame_L8_oLIN07_04172db2_Rx          140u
#define ComConf_ComIPdu_PDM_Sec_01P_oSecuritySubnet_7ffa8e54_Rx       141u
#define ComConf_ComIPdu_PDM_Sec_03S_oSecuritySubnet_29b4be53_Rx       142u
#define ComConf_ComIPdu_PDM_Sec_04S_oSecuritySubnet_c89ab437_Rx       143u
#define ComConf_ComIPdu_PropTCO2_X_TACHO_oBackbone1J1939_d2ebdcdb_Rx  144u
#define ComConf_ComIPdu_RCECStoCIOM_FR2_L5_oLIN04_537f546f_Rx         145u
#define ComConf_ComIPdu_RCECStoCIOM_L5_oLIN04_1a7dde70_Rx             146u
#define ComConf_ComIPdu_SRS_Cab_01P_oCabSubnet_bd648bbe_Rx            147u
#define ComConf_ComIPdu_SRS_Cab_03P_oCabSubnet_dd383ef5_Rx            148u
#define ComConf_ComIPdu_SRS_Cab_04P_oCabSubnet_a04b38ad_Rx            149u
#define ComConf_ComIPdu_SRS_Cab_05P_oCabSubnet_7ddde128_Rx            150u
#define ComConf_ComIPdu_SRS_Cab_06P_oCabSubnet_c0178de6_Rx            151u
#define ComConf_ComIPdu_TCO1_X_TACHO_oBackbone1J1939_9ce49b57_Rx      152u
#define ComConf_ComIPdu_TCPtoMaster_oLIN02_0421b227_Rx                153u
#define ComConf_ComIPdu_TD_X_HMIIOM_oBackbone1J1939_221848c4_Rx       154u
#define ComConf_ComIPdu_TECU_BB2_01P_oBackbone2_de0b2e23_Rx           155u
#define ComConf_ComIPdu_TECU_BB2_02P_oBackbone2_63c142ed_Rx           156u
#define ComConf_ComIPdu_TECU_BB2_05S_oBackbone2_692c9645_Rx           157u
#define ComConf_ComIPdu_TECU_BB2_06S_oBackbone2_d4e6fa8b_Rx           158u
#define ComConf_ComIPdu_Tester_CAN6toLIN_oCAN6_f863cad4_Rx            159u
#define ComConf_ComIPdu_VDC1_X_EBS_oBackbone1J1939_5b4fe419_Rx        160u
#define ComConf_ComIPdu_VDC2_X_EBS_oBackbone1J1939_2daadd24_Rx        161u
#define ComConf_ComIPdu_VDHR_X_VMCU_oBackbone1J1939_fc2f75ee_Rx       162u
#define ComConf_ComIPdu_VMCU_BB1_01P_oBackbone1J1939_f5234d42_Rx      163u
#define ComConf_ComIPdu_VMCU_BB1_03P_oBackbone1J1939_f0f726c1_Rx      164u
#define ComConf_ComIPdu_VMCU_BB2_01P_oBackbone2_8adedbca_Rx           165u
#define ComConf_ComIPdu_VMCU_BB2_02P_oBackbone2_3714b704_Rx           166u
#define ComConf_ComIPdu_VMCU_BB2_03P_oBackbone2_ea826e81_Rx           167u
#define ComConf_ComIPdu_VMCU_BB2_04P_oBackbone2_97f168d9_Rx           168u
#define ComConf_ComIPdu_VMCU_BB2_05P_oBackbone2_4a67b15c_Rx           169u
#define ComConf_ComIPdu_VMCU_BB2_07P_oBackbone2_2a3b0417_Rx           170u
#define ComConf_ComIPdu_VMCU_BB2_08P_oBackbone2_0d4bd122_Rx           171u
#define ComConf_ComIPdu_VMCU_BB2_20P_oBackbone2_b626c6e2_Rx           172u
#define ComConf_ComIPdu_VMCU_BB2_29P_oBackbone2_31b3cc0a_Rx           173u
#define ComConf_ComIPdu_VMCU_BB2_31S_oBackbone2_81212ce1_Rx           174u
#define ComConf_ComIPdu_VMCU_BB2_32S_oBackbone2_3ceb402f_Rx           175u
#define ComConf_ComIPdu_VMCU_BB2_34S_oBackbone2_9c0e9ff2_Rx           176u
#define ComConf_ComIPdu_VMCU_BB2_51P_oBackbone2_0e7db5a7_Rx           177u
#define ComConf_ComIPdu_VMCU_BB2_52P_oBackbone2_b3b7d969_Rx           178u
#define ComConf_ComIPdu_VMCU_BB2_53P_oBackbone2_6e2100ec_Rx           179u
#define ComConf_ComIPdu_VMCU_BB2_54P_oBackbone2_135206b4_Rx           180u
#define ComConf_ComIPdu_VMCU_BB2_55P_oBackbone2_cec4df31_Rx           181u
#define ComConf_ComIPdu_VMCU_BB2_57P_oBackbone2_ae986a7a_Rx           182u
#define ComConf_ComIPdu_VMCU_BB2_58P_oBackbone2_89e8bf4f_Rx           183u
#define ComConf_ComIPdu_VMCU_BB2_73P_oBackbone2_8f4fc441_Rx           184u
#define ComConf_ComIPdu_VMCU_BB2_74P_oBackbone2_f23cc219_Rx           185u
#define ComConf_ComIPdu_VMCU_BB2_80P_oBackbone2_64111c79_Rx           186u
#define ComConf_ComIPdu_VMCU_BB2_82P_oBackbone2_044da932_Rx           187u
#define ComConf_ComIPdu_VP232_X_ERAU_oFMSNet_34122677_Rx              188u
#define ComConf_ComIPdu_WRCS_Cab_01P_oCabSubnet_5ce104ec_Rx           189u
#define ComConf_ComIPdu_WRCS_Cab_02P_oCabSubnet_e12b6822_Rx           190u
#define ComConf_ComIPdu_WRCS_Cab_03P_oCabSubnet_3cbdb1a7_Rx           191u
/**\} */

/**
 * \defgroup ComHandleIdsComTxPdu Handle IDs of handle space ComTxPdu.
 * \brief Tx Pdus
 * \{
 */

/* Handle IDs active in all predefined variants (the application has not to take the active variant into account) */
/*      Symbolic Name                                                 Value   Active in predefined variant(s) */
#define ComConf_ComIPdu_AIR1_X_CIOMFMS_oFMSNet_c1c93f86_Tx            0u
#define ComConf_ComIPdu_AMB_X_CIOMFMS_oFMSNet_e990831c_Tx             1u
#define ComConf_ComIPdu_AT1T1I1_X_CIOMFMS_oFMSNet_111e0664_Tx         2u
#define ComConf_ComIPdu_AnmMsg_CIOM_Backbone2_oBackbone2_455d35af_Tx  3u
#define ComConf_ComIPdu_AnmMsg_CIOM_CabSubnet_oCabSubnet_ce34173b_Tx  4u
#define ComConf_ComIPdu_AnmMsg_CIOM_SecuritySubnet_oSecuritySubnet_1ca8dd7a_Tx 5u
#define ComConf_ComIPdu_Backlight_FuncInd_L1_oLIN00_6273f3b1_Tx       6u
#define ComConf_ComIPdu_Backlight_FuncInd_L2_oLIN01_9bfbc4c4_Tx       7u
#define ComConf_ComIPdu_Backlight_FuncInd_L3_oLIN02_ce5895e0_Tx       8u
#define ComConf_ComIPdu_Backlight_FuncInd_L4_oLIN03_b39aac6f_Tx       9u
#define ComConf_ComIPdu_Backlight_FuncInd_L5_oLIN04_e1543952_Tx       10u
#define ComConf_ComIPdu_Backlight_FuncInd_L6_oLIN05_18dc0e27_Tx       11u
#define ComConf_ComIPdu_Backlight_FuncInd_L7_oLIN06_4d7f5f03_Tx       12u
#define ComConf_ComIPdu_Backlight_FuncInd_L8_oLIN07_e3587d39_Tx       13u
#define ComConf_ComIPdu_CCVS_X_CIOMFMS_oFMSNet_7f7a8999_Tx            14u
#define ComConf_ComIPdu_CIOM_BB1_01P_oBackbone1J1939_83fef105_Tx      15u
#define ComConf_ComIPdu_CIOM_BB2_01P_oBackbone2_0634f59b_Tx           16u
#define ComConf_ComIPdu_CIOM_BB2_02P_oBackbone2_bbfe9955_Tx           17u
#define ComConf_ComIPdu_CIOM_BB2_03P_oBackbone2_666840d0_Tx           18u
#define ComConf_ComIPdu_CIOM_BB2_04P_oBackbone2_1b1b4688_Tx           19u
#define ComConf_ComIPdu_CIOM_BB2_05P_oBackbone2_c68d9f0d_Tx           20u
#define ComConf_ComIPdu_CIOM_BB2_06P_oBackbone2_7b47f3c3_Tx           21u
#define ComConf_ComIPdu_CIOM_BB2_07P_oBackbone2_a6d12a46_Tx           22u
#define ComConf_ComIPdu_CIOM_BB2_10P_oBackbone2_46adcd68_Tx           23u
#define ComConf_ComIPdu_CIOM_BB2_11P_oBackbone2_9b3b14ed_Tx           24u
#define ComConf_ComIPdu_CIOM_BB2_12S_oBackbone2_516faad3_Tx           25u
#define ComConf_ComIPdu_CIOM_BB2_13S_oBackbone2_8cf97356_Tx           26u
#define ComConf_ComIPdu_CIOM_BB2_20S_oBackbone2_4d523a43_Tx           27u
#define ComConf_ComIPdu_CIOM_BB2_21S_oBackbone2_90c4e3c6_Tx           28u
#define ComConf_ComIPdu_CIOM_BB2_22S_oBackbone2_2d0e8f08_Tx           29u
#define ComConf_ComIPdu_CIOM_BB2_23S_oBackbone2_f098568d_Tx           30u
#define ComConf_ComIPdu_CIOM_BB2_24S_oBackbone2_8deb50d5_Tx           31u
#define ComConf_ComIPdu_CIOM_BB2_25P_oBackbone2_27e35ba0_Tx           32u
#define ComConf_ComIPdu_CIOM_BB2_26S_oBackbone2_edb7e59e_Tx           33u
#define ComConf_ComIPdu_CIOM_BB2_27S_oBackbone2_30213c1b_Tx           34u
#define ComConf_ComIPdu_CIOM_BB2_28P_oBackbone2_60cf3bde_Tx           35u
#define ComConf_ComIPdu_CIOM_BB2_29P_oBackbone2_bd59e25b_Tx           36u
#define ComConf_ComIPdu_CIOM_BB2_30S_oBackbone2_d05ddb35_Tx           37u
#define ComConf_ComIPdu_CIOM_Cab_01P_oCabSubnet_ba360fbd_Tx           38u
#define ComConf_ComIPdu_CIOM_Cab_02P_oCabSubnet_07fc6373_Tx           39u
#define ComConf_ComIPdu_CIOM_Cab_03P_oCabSubnet_da6abaf6_Tx           40u
#define ComConf_ComIPdu_CIOM_Cab_04S_oCabSubnet_d0876e5e_Tx           41u
#define ComConf_ComIPdu_CIOM_Cab_05S_oCabSubnet_0d11b7db_Tx           42u
#define ComConf_ComIPdu_CIOM_Cab_06P_oCabSubnet_c74509e5_Tx           43u
#define ComConf_ComIPdu_CIOM_Cab_07P_oCabSubnet_1ad3d060_Tx           44u
#define ComConf_ComIPdu_CIOM_Cab_08P_oCabSubnet_3da30555_Tx           45u
#define ComConf_ComIPdu_CIOM_Cab_09P_oCabSubnet_e035dcd0_Tx           46u
#define ComConf_ComIPdu_CIOM_Cab_10P_oCabSubnet_faaf374e_Tx           47u
#define ComConf_ComIPdu_CIOM_Cab_11S_oCabSubnet_50a73c3b_Tx           48u
#define ComConf_ComIPdu_CIOM_Cab_12P_oCabSubnet_9af38205_Tx           49u
#define ComConf_ComIPdu_CIOM_Cab_13S_oCabSubnet_30fb8970_Tx           50u
#define ComConf_ComIPdu_CIOM_Cab_14P_oCabSubnet_3a165dd8_Tx           51u
#define ComConf_ComIPdu_CIOM_Cab_15P_oCabSubnet_e780845d_Tx           52u
#define ComConf_ComIPdu_CIOM_Cab_16P_oCabSubnet_5a4ae893_Tx           53u
#define ComConf_ComIPdu_CIOM_Cab_17P_oCabSubnet_87dc3116_Tx           54u
#define ComConf_ComIPdu_CIOM_Cab_18P_oCabSubnet_a0ace423_Tx           55u
#define ComConf_ComIPdu_CIOM_Cab_19P_oCabSubnet_7d3a3da6_Tx           56u
#define ComConf_ComIPdu_CIOM_Cab_20S_oCabSubnet_f150c065_Tx           57u
#define ComConf_ComIPdu_CIOM_Cab_21P_oCabSubnet_5b58cb10_Tx           58u
#define ComConf_ComIPdu_CIOM_Cab_22P_oCabSubnet_e692a7de_Tx           59u
#define ComConf_ComIPdu_CIOM_Cab_23P_oCabSubnet_3b047e5b_Tx           60u
#define ComConf_ComIPdu_CIOM_Cab_24P_oCabSubnet_46777803_Tx           61u
#define ComConf_ComIPdu_CIOM_Cab_25S_oCabSubnet_ec7f7376_Tx           62u
#define ComConf_ComIPdu_CIOM_Cab_26P_oCabSubnet_262bcd48_Tx           63u
#define ComConf_ComIPdu_CIOM_Cab_27P_oCabSubnet_fbbd14cd_Tx           64u
#define ComConf_ComIPdu_CIOM_Cab_28S_oCabSubnet_ab531308_Tx           65u
#define ComConf_ComIPdu_CIOM_Cab_29S_oCabSubnet_76c5ca8d_Tx           66u
#define ComConf_ComIPdu_CIOM_Cab_30S_oCabSubnet_6c5f2113_Tx           67u
#define ComConf_ComIPdu_CIOM_Cab_31P_oCabSubnet_c6572a66_Tx           68u
#define ComConf_ComIPdu_CIOM_Cab_32P_oCabSubnet_7b9d46a8_Tx           69u
#define ComConf_ComIPdu_CIOM_Cab_33P_oCabSubnet_a60b9f2d_Tx           70u
#define ComConf_ComIPdu_CIOM_Cab_34P_oCabSubnet_db789975_Tx           71u
#define ComConf_ComIPdu_CIOM_Sec_01P_oSecuritySubnet_43c7ca84_Tx      72u
#define ComConf_ComIPdu_CIOM_Sec_02P_oSecuritySubnet_a94117e6_Tx      73u
#define ComConf_ComIPdu_CIOM_Sec_03P_oSecuritySubnet_4613a107_Tx      74u
#define ComConf_ComIPdu_CIOM_Sec_04P_oSecuritySubnet_a73dab63_Tx      75u
#define ComConf_ComIPdu_CIOM_Sec_05S_oSecuritySubnet_1bf54606_Tx      76u
#define ComConf_ComIPdu_CIOM_Sec_06S_oSecuritySubnet_f1739b64_Tx      77u
#define ComConf_ComIPdu_CIOM_Sec_07S_oSecuritySubnet_1e212d85_Tx      78u
#define ComConf_ComIPdu_CIOM_Sec_08S_oSecuritySubnet_e85e89ed_Tx      79u
#define ComConf_ComIPdu_CIOM_Sec_09S_oSecuritySubnet_070c3f0c_Tx      80u
#define ComConf_ComIPdu_CIOM_Sec_10S_oSecuritySubnet_28eda7b9_Tx      81u
#define ComConf_ComIPdu_CIOM_Sec_11S_oSecuritySubnet_c7bf1158_Tx      82u
#define ComConf_ComIPdu_CIOM_Sec_12S_oSecuritySubnet_2d39cc3a_Tx      83u
#define ComConf_ComIPdu_CIOMtoSlaves1_L1_oLIN00_8b7527bd_Tx           84u
#define ComConf_ComIPdu_CIOMtoSlaves1_L4_oLIN03_5a9c7863_Tx           85u
#define ComConf_ComIPdu_CIOMtoSlaves2_FR1_L1_oLIN00_dfa25815_Tx       86u
#define ComConf_ComIPdu_CIOMtoSlaves2_FR2_L1_oLIN00_46403e14_Tx       87u
#define ComConf_ComIPdu_CIOMtoSlaves2_FR3_L1_oLIN00_87cee1d4_Tx       88u
#define ComConf_ComIPdu_CIOMtoSlaves2_L4_oLIN03_c37e1e62_Tx           89u
#define ComConf_ComIPdu_CIOMtoSlaves_FR2_L5_oLIN04_ae0c669a_Tx        90u
#define ComConf_ComIPdu_CIOMtoSlaves_L5_oLIN04_f05bf497_Tx            91u
#define ComConf_ComIPdu_CL_X_CIOMFMS_oFMSNet_d1f9e889_Tx              92u
#define ComConf_ComIPdu_CVW_X_CIOMFMS_oFMSNet_f228048c_Tx             93u
#define ComConf_ComIPdu_DD_X_CIOMFMS_oFMSNet_391c539f_Tx              94u
#define ComConf_ComIPdu_DI_X_CIOMFMS_oFMSNet_0e37c3a0_Tx              95u
#define ComConf_ComIPdu_Debug02_CIOM_BB2_oBackbone2_79845af3_Tx       96u
#define ComConf_ComIPdu_Debug03_CIOM_BB2_oBackbone2_e65ed96d_Tx       97u
#define ComConf_ComIPdu_Debug04_CIOM_BB2_oBackbone2_8fb95f35_Tx       98u
#define ComConf_ComIPdu_Debug05_CIOM_BB2_oBackbone2_1063dcab_Tx       99u
#define ComConf_ComIPdu_Debug06_CIOM_BB2_oBackbone2_6b7d5e48_Tx       100u
#define ComConf_ComIPdu_Debug07_CIOM_BB2_oBackbone2_f4a7ddd6_Tx       101u
#define ComConf_ComIPdu_DiagFaultStat_Alarm_BB2_oBackbone2_ed9b4dd9_Tx 102u
#define ComConf_ComIPdu_DiagFaultStat_CCM_BB2_oBackbone2_9d39fab0_Tx  103u
#define ComConf_ComIPdu_DiagFaultStat_CIOM_BB2_oBackbone2_04816481_Tx 104u
#define ComConf_ComIPdu_DiagFaultStat_DDM_BB2_oBackbone2_5c858c9f_Tx  105u
#define ComConf_ComIPdu_DiagFaultStat_LECM_BB2_oBackbone2_ceffe314_Tx 106u
#define ComConf_ComIPdu_DiagFaultStat_PDM_BB2_oBackbone2_798e0781_Tx  107u
#define ComConf_ComIPdu_DiagFaultStat_SRS_BB2_oBackbone2_e5c83ca3_Tx  108u
#define ComConf_ComIPdu_DiagFaultStat_WRCS_BB2_oBackbone2_7e62dd7b_Tx 109u
#define ComConf_ComIPdu_EEC1_X_CIOMFMS_oFMSNet_2fc23128_Tx            110u
#define ComConf_ComIPdu_EEC2_X_CIOMFMS_oFMSNet_8c94b781_Tx            111u
#define ComConf_ComIPdu_EEC14_X_CIOMFMS_oFMSNet_2c0ebba5_Tx           112u
#define ComConf_ComIPdu_ERC1_x_EMSRetFMS_oFMSNet_71599960_Tx          113u
#define ComConf_ComIPdu_ERC1_x_RECUFMS_oFMSNet_64a06015_Tx            114u
#define ComConf_ComIPdu_ET1_X_CIOMFMS_oFMSNet_388be6e9_Tx             115u
#define ComConf_ComIPdu_FMS1_X_CIOMFMS_oFMSNet_d38e3e3b_Tx            116u
#define ComConf_ComIPdu_FMS_X_CIOMFMS_oFMSNet_44d7be16_Tx             117u
#define ComConf_ComIPdu_FSP_1_2_Req_L1_oLIN00_6846650e_Tx             118u
#define ComConf_ComIPdu_FSP_1_2_Req_L2_oLIN01_91ce527b_Tx             119u
#define ComConf_ComIPdu_FSP_1_2_Req_L3_oLIN02_c46d035f_Tx             120u
#define ComConf_ComIPdu_FSP_1_2_Req_L4_oLIN03_b9af3ad0_Tx             121u
#define ComConf_ComIPdu_FSP_1_2_Req_L5_oLIN04_eb61afed_Tx             122u
#define ComConf_ComIPdu_FSP_3_4_Req_L2_oLIN01_6708164e_Tx             123u
#define ComConf_ComIPdu_HOURS_X_CIOMFMS_oFMSNet_3c4de7ce_Tx           124u
#define ComConf_ComIPdu_HRLFC_X_CIOMFMS_oFMSNet_2bd59f3d_Tx           125u
#define ComConf_ComIPdu_LFC_X_CIOMFMS_oFMSNet_a4d1166f_Tx             126u
#define ComConf_ComIPdu_LFE_X_CIOMFMS_oFMSNet_390d1d7c_Tx             127u
#define ComConf_ComIPdu_MastertoTCP_oLIN02_d16528d3_Tx                128u
#define ComConf_ComIPdu_PTODE_X_CIOMFMS_oFMSNet_74e3ffab_Tx           129u
#define ComConf_ComIPdu_RQST_TACHO_CIOM_oBackbone1J1939_8b5ad5b1_Tx   130u
#define ComConf_ComIPdu_SCIM_BB1toCAN6_oCAN6_c39c482d_Tx              131u
#define ComConf_ComIPdu_SCIM_BB2toCAN6_oCAN6_7e5624e3_Tx              132u
#define ComConf_ComIPdu_SCIM_LINtoCAN6_oCAN6_fc53c204_Tx              133u
#define ComConf_ComIPdu_SERV_X_CIOMFMS_oFMSNet_50d7a0fd_Tx            134u
#define ComConf_ComIPdu_TCO1_X_CIOMFMS_oFMSNet_b4162784_Tx            135u
#define ComConf_ComIPdu_VDHR_X_CIOMFMS_oFMSNet_da3fd25a_Tx            136u
#define ComConf_ComIPdu_VI_X_CIOMFMS_oFMSNet_243f1e09_Tx              137u
#define ComConf_ComIPdu_VP236_X_CIOMFMS_oFMSNet_b08a3073_Tx           138u
#define ComConf_ComIPdu_VW_X_CIOMFMS_oFMSNet_a3003395_Tx              139u
/**\} */

/**********************************************************************************************************************
  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
#define COM_START_SEC_CODE
#include "MemMap.h"    /* PRQA S 5087 1 */ /*MD_MSR_MemMap */
/**********************************************************************************************************************
  Com_RxIndication
**********************************************************************************************************************/
/** \brief    This function is called by the lower layer after an I-PDU has been received.
    \param    RxPduId      ID of AUTOSAR COM I-PDU that has been received. Identifies the data that has been received.
                              Range: 0..(maximum number of I-PDU IDs received by AUTOSAR COM) - 1
    \param    PduInfoPtr      Payload information of the received I-PDU (pointer to data and data length).
    \return   none
    \context  The function can be called on interrupt and task level. It is not allowed to use CAT1 interrupts with Rte (BSW00326]). Due to this, the interrupt context shall be configured to a CAT2 interrupt if an Rte is used.
    \synchronous TRUE
    \reentrant   TRUE, for different Handles
    \trace    SPEC-2737026
    \note     The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(void, COM_CODE) Com_RxIndication(PduIdType RxPduId, P2CONST(PduInfoType, AUTOMATIC, COM_APPL_DATA) PduInfoPtr);

/**********************************************************************************************************************
  Com_TxConfirmation
**********************************************************************************************************************/
/** \brief    This function is called by the lower layer after the PDU has been transmitted on the network.
              A confirmation that is received for an I-PDU that does not require a confirmation is silently discarded.
    \param    TxPduId    ID of AUTOSAR COM I-PDU that has been transmitted.
                            Range: 0..(maximum number of I-PDU IDs transmitted by AUTOSAR COM) - 1
    \return   none
    \context  The function can be called on interrupt and task level. It is not allowed to use CAT1 interrupts with Rte (BSW00326]). Due to this, the interrupt context shall be configured to a CAT2 interrupt if an Rte is used.
    \synchronous TRUE
    \reentrant   TRUE, for different Handles
    \trace    SPEC-2737028
    \note     The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(void, COM_CODE) Com_TxConfirmation(PduIdType TxPduId);

/**********************************************************************************************************************
  Com_TriggerTransmit
**********************************************************************************************************************/
/** \brief    This function is called by the lower layer when an AUTOSAR COM I-PDU shall be transmitted.
              Within this function, AUTOSAR COM shall copy the contents of its I-PDU transmit buffer
              to the L-PDU buffer given by SduPtr.
              Use case:
              This function is used e.g. by the LIN Master for sending out a LIN frame. In this case, the trigger transmit
              can be initiated by the Master schedule table itself or a received LIN header.
              This function is also used by the FlexRay Interface for requesting PDUs to be sent in static part
              (synchronous to the FlexRay global time). Once the I-PDU has been successfully sent by the lower layer
              (PDU-Router), the lower layer must call Com_TxConfirmation().
    \param          TxPduId       ID of AUTOSAR COM I-PDU that is requested to be transmitted by AUTOSAR COM.
    \param[in,out]  PduInfoPtr    Contains a pointer to a buffer (SduDataPtr) where the SDU
                                  data shall be copied to, and the available buffer size in SduLengh.
                                  On return, the service will indicate the length of the copied SDU
                                  data in SduLength.
    \return         E_OK          SDU has been copied and SduLength indicates the number of copied bytes.
    \return         E_NOT_OK      No data has been copied, because
                                  Com is not initialized
                                  or TxPduId is not valid
                                  or PduInfoPtr is NULL_PTR
                                  or SduDataPtr is NULL_PTR
                                  or SduLength is too small.
    \context  TASK|ISR2
    \synchronous TRUE
    \reentrant   TRUE, for different Handles
    \trace    SPEC-2737022, SPEC-2737023, SPEC-2737024
    \note     The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(Std_ReturnType, COM_CODE) Com_TriggerTransmit(PduIdType TxPduId, P2VAR(PduInfoType, AUTOMATIC, COM_APPL_VAR) PduInfoPtr);

/**********************************************************************************************************************
  Com_TpTxConfirmation
**********************************************************************************************************************/
/** \brief    This function is called by the PduR after a large I-PDU has been transmitted via the transport protocol on its network.
    \param    PduId    ID of the I-PDU that has been transmitted.
    \param    Result   Result of the transmission of the I-PDU
    \return   None.
    \context  TASK|ISR2
    \synchronous TRUE
    \reentrant   TRUE, for different Handles
    \trace    SPEC-2737029
    \note     The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(void, COM_CODE) Com_TpTxConfirmation( PduIdType PduId, Std_ReturnType Result );

/**********************************************************************************************************************
  Com_CopyTxData
**********************************************************************************************************************/
/** \brief    This function is called by the lower layer to copy Data from the Com TP buffer to the lower layer TP buffer.
    \param    PduId               ID of Com TP I-PDU to be transmitted.
    \param    PduInfoPtr          Pointer to a PduInfoType, which indicates the number of bytes to be copied (SduLength) and the location where the data have to be copied to (SduDataPtr).
                                  An SduLength of 0 is possible in order to poll the available transmit data count. In this case no data are to be copied and SduDataPtr might be invalid.
    \param    RetryInfoPtr        If the TpDataState of the RetryInfoPtr is TP_DATARETRY no data will be copied. Otherwise, the COM module
                                  will ignore the value of this pointer, since it always keeps the complete buffer until the transmission of a large I-PDU is either confirmed or aborted.
    \param    TxDataCntPtr        Out parameter: Remaining Tx data after completion of this call.
    \return   BufReq_ReturnType   BUFREQ_OK:       Data has been copied to the transmit buffer completely as requested.
                                  BUFREQ_E_BUSY:   The transmission buffer is actually not available (implementation specific).
                                  BUFREQ_E_NOT_OK: Data has not been copied. Request failed, in case the corresponding I-PDU was stopped.
    \context  TASK|ISR2
    \synchronous TRUE
    \reentrant   TRUE, for different Handles
    \trace    SPEC-2736849, SPEC-2737039, SPEC-2737040
    \note     The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(BufReq_ReturnType, COM_CODE) Com_CopyTxData(PduIdType PduId, P2VAR(PduInfoType, AUTOMATIC, COM_APPL_VAR) PduInfoPtr, P2VAR(RetryInfoType, AUTOMATIC, COM_APPL_VAR) RetryInfoPtr, P2VAR(PduLengthType, AUTOMATIC, COM_APPL_VAR) TxDataCntPtr);

/**********************************************************************************************************************
  Com_TpRxIndication
**********************************************************************************************************************/
/** \brief    This function is called by the lower layer after a TP I-PDU has been received.
    \param    PduId      ID of AUTOSAR COM I-PDU that has been received. Identifies the data that has been received.
                         Range: 0..(maximum number of I-PDU IDs received by AUTOSAR COM) - 1
    \param    Result     Indicates whether the Message was received successfully.
    \return   none
    \context  TASK|ISR2
    \synchronous TRUE
    \reentrant   TRUE, for different Handles
    \trace    SPEC-2737027, SPEC-2737063
    \note     The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(void, COM_CODE) Com_TpRxIndication( PduIdType PduId, Std_ReturnType Result );

/**********************************************************************************************************************
  Com_StartOfReception
**********************************************************************************************************************/
/** \brief    This function is called by the lower layer to indicate the start of a incomming TP connection.
    \param    ComRxPduId         ID of AUTOSAR COM I-PDU that has been received. Identifies the data that has been received.
    \param    TpSduInfoPtr       The parameter 'TpSduInfoPtr' is currently not used by COM.
    \param    TpSduLength        complete length of the TP I-PDU to be received.
    \param    RxBufferSizePtr    The Com returns in this value the remaining TP buffer size to the lower layer.
    \return   BufReq_ReturnType  BUFREQ_OK:       Connection has been accepted.
                                                  RxBufferSizePtr indicates the available receive buffer.
                                 BUFREQ_E_NOT_OK: Connection has been rejected.
                                                  RxBufferSizePtr remains unchanged.
                                 BUFREQ_E_OVFL:   In case the configured buffer size as specified via ComPduIdRef.PduLength is smaller than TpSduLength.
                                 BUFREQ_E_BUSY:   In case the reception buffer is actually not available for a new reception (implementation specific).
    \context  TASK|ISR2
    \synchronous TRUE
    \reentrant   TRUE, for different Handles
    \trace    SPEC-2736846, SPEC-2737030, SPEC-2737031, SPEC-2737032, SPEC-2737033, SPEC-2737034, SPEC-2737035, SPEC-2737036
    \note     The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(BufReq_ReturnType, COM_CODE) Com_StartOfReception(PduIdType ComRxPduId, P2VAR(PduInfoType, AUTOMATIC, COM_APPL_VAR) TpSduInfoPtr, PduLengthType TpSduLength, P2VAR(PduLengthType, AUTOMATIC, COM_APPL_VAR) RxBufferSizePtr);

/**********************************************************************************************************************
  Com_CopyRxData
**********************************************************************************************************************/
/** \brief    This function is called by the lower layer to hand a received TP segment to Com.
              The Com copies the received segment in his internal tp buffer.
    \param    PduId              ID of AUTOSAR COM I-PDU that has been received. Identifies the data that has been received.
    \param    PduInfoPointer     Payload information of the received TP segment (pointer to data and data length).
    \param    RxBufferSizePtr    The Com returns in this value the remaining TP buffer size to the lower layer.
    \return   BufReq_ReturnType  BUFREQ_OK:       Connection has been accepted.
                                                  RxBufferSizePtr indicates the available receive buffer.
                                 BUFREQ_E_NOT_OK: Connection has been rejected.
                                                  RxBufferSizePtr remains unchanged.
    \context  TASK|ISR2
    \synchronous TRUE
    \reentrant   TRUE, for different Handles
    \trace    SPEC-2736846, SPEC-2737037, SPEC-2737038
    \note     The function is called by the lower layer.
**********************************************************************************************************************/
FUNC(BufReq_ReturnType, COM_CODE) Com_CopyRxData(PduIdType PduId, CONSTP2VAR(PduInfoType, AUTOMATIC, COM_APPL_DATA) PduInfoPointer, P2VAR(PduLengthType, AUTOMATIC, COM_APPL_VAR) RxBufferSizePtr);

#define COM_STOP_SEC_CODE
#include "MemMap.h"    /* PRQA S 5087 1 */ /* MD_MSR_MemMap */

#endif  /* COM_CBK_H */
/**********************************************************************************************************************
  END OF FILE: Com_Cbk.h
**********************************************************************************************************************/

