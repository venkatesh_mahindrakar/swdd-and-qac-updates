/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: J1939Rm
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: J1939Rm_Cfg.c
 *   Generation Time: 2020-11-11 14:25:37
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
  MISRA JUSTIFICATION
**********************************************************************************************************************/

/* PRQA S 0779 EOF */ /* MD_MSR_5.1_777 */


/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/

#define J1939RM_CFG_SOURCE

#include "J1939Rm_Cfg.h"

/* include headers with symbolic name values */
#include "PduR_J1939Rm.h"
#include "J1939Nm_Cbk.h"
#include "ComM.h"

/* include headers with callout function prototypes */
#include "Com.h"
#include "J1939Nm_Cbk.h"


/**********************************************************************************************************************
  LOCAL CONSTANT MACROS
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL FUNCTION MACROS
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  J1939Rm_AckQueueInd
**********************************************************************************************************************/
/** 
  \var    J1939Rm_AckQueueInd
  \brief  the indexes of the 1:1 sorted relation pointing to J1939Rm_AckQueue
*/ 
#define J1939RM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_AckQueueIndType, J1939RM_CONST) J1939Rm_AckQueueInd[20] = {
  /* Index    AckQueueInd      Referable Keys */
  /*     0 */          0u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     1 */          1u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     2 */          2u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     3 */          3u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     4 */          4u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     5 */          5u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     6 */          6u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     7 */          7u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     8 */          8u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     9 */          9u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*    10 */         10u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    11 */         11u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    12 */         12u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    13 */         13u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    14 */         14u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    15 */         15u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    16 */         16u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    17 */         17u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    18 */         18u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    19 */         19u   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
};
#define J1939RM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_Channel
**********************************************************************************************************************/
/** 
  \var    J1939Rm_Channel
  \brief  DefinitionRef: /MICROSAR/J1939Rm/J1939RmConfigSet/J1939RmChannel
  \details
  Element                            Description
  AckQueueIndEndIdx                  the end index of the 0:n relation pointing to J1939Rm_AckQueueInd
  AckQueueIndStartIdx                the start index of the 0:n relation pointing to J1939Rm_AckQueueInd
  AckQueueSize                       DefinitionRef: /MICROSAR/J1939Rm/J1939RmConfigSet/J1939RmChannel/J1939RmAckQueueSize
  ChannelNodeNodeTableIndEndIdx      the end index of the 0:n relation pointing to J1939Rm_ChannelNodeNodeTableInd
  ChannelNodeNodeTableIndStartIdx    the start index of the 0:n relation pointing to J1939Rm_ChannelNodeNodeTableInd
  ComMChannelId                      DefinitionRef: /[ANY]/ComM/ComMConfigSet/ComMChannel
  ReqQueueIndEndIdx                  the end index of the 0:n relation pointing to J1939Rm_ReqQueueInd
  ReqQueueIndStartIdx                the start index of the 0:n relation pointing to J1939Rm_ReqQueueInd
  ReqTimeoutIndEndIdx                the end index of the 0:n relation pointing to J1939Rm_ReqTimeoutInd
  ReqTimeoutIndStartIdx              the start index of the 0:n relation pointing to J1939Rm_ReqTimeoutInd
  Request2QueueSize                  DefinitionRef: /MICROSAR/J1939Rm/J1939RmConfigSet/J1939RmChannel/J1939RmRequest2QueueSize
  RequestQueueSize                   DefinitionRef: /MICROSAR/J1939Rm/J1939RmConfigSet/J1939RmChannel/J1939RmRequestQueueSize
  RequestTimeoutMonitors             DefinitionRef: /MICROSAR/J1939Rm/J1939RmConfigSet/J1939RmChannel/J1939RmRequestTimeoutMonitors
  RxPduRqstIdx                       the index of the 0:1 relation pointing to J1939Rm_RxPdu
  TxPduAckmIdx                       the index of the 0:1 relation pointing to J1939Rm_TxPdu
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_ChannelType, J1939RM_CONST) J1939Rm_Channel[2] = {
    /* Index    AckQueueIndEndIdx  AckQueueIndStartIdx  AckQueueSize  ChannelNodeNodeTableIndEndIdx  ChannelNodeNodeTableIndStartIdx  ComMChannelId                                    ReqQueueIndEndIdx  ReqQueueIndStartIdx  ReqTimeoutIndEndIdx  ReqTimeoutIndStartIdx  Request2QueueSize  RequestQueueSize  RequestTimeoutMonitors  RxPduRqstIdx  TxPduAckmIdx        Referable Keys */
  { /*     0 */               10u,                  0u,          10u,                            1u,                              0u, ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae,               10u,                  0u,                 10u,                    0u,                0u,              10u,                    10u,           0u,           0u },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  { /*     1 */               20u,                 10u,          10u,                            2u,                              1u,         ComMConf_ComMChannel_CN_FMSNet_fce1aae5,               20u,                 10u,                 20u,                   10u,                0u,              10u,                    10u,           1u,           1u }   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ChannelNode
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ChannelNode
  \details
  Element                Description
  NodeChannelStateIdx    the index of the 1:1 relation pointing to J1939Rm_NodeChannelState
  NodeNodeIdx            the index of the 1:1 relation pointing to J1939Rm_Node
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_ChannelNodeType, J1939RM_CONST) J1939Rm_ChannelNode[2] = {
    /* Index    NodeChannelStateIdx  NodeNodeIdx        Referable Keys */
  { /*     0 */                  0u,          0u },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  { /*     1 */                  1u,          0u }   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ChannelNodeNodeTableInd
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ChannelNodeNodeTableInd
  \brief  the indexes of the 1:1 sorted relation pointing to J1939Rm_ChannelNode
*/ 
#define J1939RM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_ChannelNodeNodeTableIndType, J1939RM_CONST) J1939Rm_ChannelNodeNodeTableInd[2] = {
  /* Index    ChannelNodeNodeTableInd      Referable Keys */
  /*     0 */                      0u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     1 */                      1u   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
};
#define J1939RM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ComIPdu
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ComIPdu
  \brief  List of requested COM I-PDUs
  \details
  Element                Description
  PGN                    PGN
  ComIPduDestEndIdx      the end index of the 1:n relation pointing to J1939Rm_ComIPduDest
  ComIPduDestStartIdx    the start index of the 1:n relation pointing to J1939Rm_ComIPduDest
  ExtId1                 ExtId Byte 1
  ExtId2                 ExtId Byte 2
  ExtId3                 ExtId Byte 3
  ExtIdUsage             ExtId Usage
  MetaDataLength         MetaData Length
  PduId                  PDU ID
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_ComIPduType, J1939RM_CONST) J1939Rm_ComIPdu[25] = {
    /* Index    PGN           ComIPduDestEndIdx  ComIPduDestStartIdx  ExtId1  ExtId2  ExtId3  ExtIdUsage                               MetaDataLength  PduId                                                        Comment                                                                                                                  Referable Keys */
  { /*     0 */ 0x0000FEEEuL,                1u,                  0u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,     ComConf_ComIPdu_ET1_X_CIOMFMS_oFMSNet_388be6e9_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/ET1_X_CIOMFMS_oFMSNet_388be6e9_Tx]     */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*     1 */ 0x0000FDC2uL,                2u,                  1u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,   ComConf_ComIPdu_EEC14_X_CIOMFMS_oFMSNet_2c0ebba5_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/EEC14_X_CIOMFMS_oFMSNet_2c0ebba5_Tx]   */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*     2 */ 0x0000FDD1uL,                3u,                  2u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,     ComConf_ComIPdu_FMS_X_CIOMFMS_oFMSNet_44d7be16_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/FMS_X_CIOMFMS_oFMSNet_44d7be16_Tx]     */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*     3 */ 0x0000FE6BuL,                4u,                  3u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             2u,      ComConf_ComIPdu_DI_X_CIOMFMS_oFMSNet_0e37c3a0_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/DI_X_CIOMFMS_oFMSNet_0e37c3a0_Tx]      */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*     4 */ 0x0000D000uL,                5u,                  4u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             2u,      ComConf_ComIPdu_CL_X_CIOMFMS_oFMSNet_d1f9e889_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/CL_X_CIOMFMS_oFMSNet_d1f9e889_Tx]      */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*     5 */ 0x0000FEF2uL,                6u,                  5u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,     ComConf_ComIPdu_LFE_X_CIOMFMS_oFMSNet_390d1d7c_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/LFE_X_CIOMFMS_oFMSNet_390d1d7c_Tx]     */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*     6 */ 0x0000FDA4uL,                7u,                  6u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,   ComConf_ComIPdu_PTODE_X_CIOMFMS_oFMSNet_74e3ffab_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/PTODE_X_CIOMFMS_oFMSNet_74e3ffab_Tx]   */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*     7 */ 0x0000FEF5uL,                8u,                  7u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,     ComConf_ComIPdu_AMB_X_CIOMFMS_oFMSNet_e990831c_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/AMB_X_CIOMFMS_oFMSNet_e990831c_Tx]     */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*     8 */ 0x0000FEEAuL,                9u,                  8u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,      ComConf_ComIPdu_VW_X_CIOMFMS_oFMSNet_a3003395_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/VW_X_CIOMFMS_oFMSNet_a3003395_Tx]      */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*     9 */ 0x0000FE6CuL,               10u,                  9u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,    ComConf_ComIPdu_TCO1_X_CIOMFMS_oFMSNet_b4162784_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/TCO1_X_CIOMFMS_oFMSNet_b4162784_Tx]    */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    10 */ 0x0000FD09uL,               11u,                 10u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,   ComConf_ComIPdu_HRLFC_X_CIOMFMS_oFMSNet_2bd59f3d_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/HRLFC_X_CIOMFMS_oFMSNet_2bd59f3d_Tx]   */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    11 */ 0x0000FEFCuL,               12u,                 11u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,      ComConf_ComIPdu_DD_X_CIOMFMS_oFMSNet_391c539f_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/DD_X_CIOMFMS_oFMSNet_391c539f_Tx]      */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    12 */ 0x0000FEAEuL,               13u,                 12u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,    ComConf_ComIPdu_AIR1_X_CIOMFMS_oFMSNet_c1c93f86_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/AIR1_X_CIOMFMS_oFMSNet_c1c93f86_Tx]    */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    13 */ 0x0000FEF1uL,               14u,                 13u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,    ComConf_ComIPdu_CCVS_X_CIOMFMS_oFMSNet_7f7a8999_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/CCVS_X_CIOMFMS_oFMSNet_7f7a8999_Tx]    */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    14 */ 0x0000FEE5uL,               15u,                 14u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,   ComConf_ComIPdu_HOURS_X_CIOMFMS_oFMSNet_3c4de7ce_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/HOURS_X_CIOMFMS_oFMSNet_3c4de7ce_Tx]   */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    15 */ 0x0000FEC0uL,               16u,                 15u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,    ComConf_ComIPdu_SERV_X_CIOMFMS_oFMSNet_50d7a0fd_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/SERV_X_CIOMFMS_oFMSNet_50d7a0fd_Tx]    */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    16 */ 0x0000FEECuL,               17u,                 16u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             2u,      ComConf_ComIPdu_VI_X_CIOMFMS_oFMSNet_243f1e09_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/VI_X_CIOMFMS_oFMSNet_243f1e09_Tx]      */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    17 */ 0x0000FEC1uL,               18u,                 17u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,    ComConf_ComIPdu_VDHR_X_CIOMFMS_oFMSNet_da3fd25a_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/VDHR_X_CIOMFMS_oFMSNet_da3fd25a_Tx]    */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    18 */ 0x0000FFECuL,               19u,                 18u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,   ComConf_ComIPdu_VP236_X_CIOMFMS_oFMSNet_b08a3073_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/VP236_X_CIOMFMS_oFMSNet_b08a3073_Tx]   */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    19 */ 0x0000FD7DuL,               20u,                 19u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,    ComConf_ComIPdu_FMS1_X_CIOMFMS_oFMSNet_d38e3e3b_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/FMS1_X_CIOMFMS_oFMSNet_d38e3e3b_Tx]    */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    20 */ 0x0000F004uL,               21u,                 20u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,    ComConf_ComIPdu_EEC1_X_CIOMFMS_oFMSNet_2fc23128_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/EEC1_X_CIOMFMS_oFMSNet_2fc23128_Tx]    */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    21 */ 0x0000FE56uL,               22u,                 21u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u, ComConf_ComIPdu_AT1T1I1_X_CIOMFMS_oFMSNet_111e0664_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/AT1T1I1_X_CIOMFMS_oFMSNet_111e0664_Tx] */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    22 */ 0x0000FE70uL,               23u,                 22u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,     ComConf_ComIPdu_CVW_X_CIOMFMS_oFMSNet_f228048c_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/CVW_X_CIOMFMS_oFMSNet_f228048c_Tx]     */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    23 */ 0x0000FEE9uL,               24u,                 23u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,     ComConf_ComIPdu_LFC_X_CIOMFMS_oFMSNet_a4d1166f_Tx },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/LFC_X_CIOMFMS_oFMSNet_a4d1166f_Tx]     */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  { /*    24 */ 0x0000F003uL,               25u,                 24u,     0u,     0u,     0u, J1939RM_EXT_ID_NONE_EXTIDUSAGEOFCOMIPDU,             0u,    ComConf_ComIPdu_EEC2_X_CIOMFMS_oFMSNet_8c94b781_Tx }   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/EEC2_X_CIOMFMS_oFMSNet_8c94b781_Tx]    */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ComIPduDest
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ComIPduDest
  \brief  List of Channels and DA addresses of requested COM I-PDUs
  \details
  Element       Description
  ChannelIdx    the index of the 1:1 relation pointing to J1939Rm_Channel
  DA            DA
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_ComIPduDestType, J1939RM_CONST) J1939Rm_ComIPduDest[25] = {
    /* Index    ChannelIdx  DA           Referable Keys */
  { /*     0 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/ET1_X_CIOMFMS_oFMSNet_388be6e9_Tx] */
  { /*     1 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/EEC14_X_CIOMFMS_oFMSNet_2c0ebba5_Tx] */
  { /*     2 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/FMS_X_CIOMFMS_oFMSNet_44d7be16_Tx] */
  { /*     3 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/DI_X_CIOMFMS_oFMSNet_0e37c3a0_Tx] */
  { /*     4 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/CL_X_CIOMFMS_oFMSNet_d1f9e889_Tx] */
  { /*     5 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/LFE_X_CIOMFMS_oFMSNet_390d1d7c_Tx] */
  { /*     6 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/PTODE_X_CIOMFMS_oFMSNet_74e3ffab_Tx] */
  { /*     7 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/AMB_X_CIOMFMS_oFMSNet_e990831c_Tx] */
  { /*     8 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/VW_X_CIOMFMS_oFMSNet_a3003395_Tx] */
  { /*     9 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/TCO1_X_CIOMFMS_oFMSNet_b4162784_Tx] */
  { /*    10 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/HRLFC_X_CIOMFMS_oFMSNet_2bd59f3d_Tx] */
  { /*    11 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/DD_X_CIOMFMS_oFMSNet_391c539f_Tx] */
  { /*    12 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/AIR1_X_CIOMFMS_oFMSNet_c1c93f86_Tx] */
  { /*    13 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/CCVS_X_CIOMFMS_oFMSNet_7f7a8999_Tx] */
  { /*    14 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/HOURS_X_CIOMFMS_oFMSNet_3c4de7ce_Tx] */
  { /*    15 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/SERV_X_CIOMFMS_oFMSNet_50d7a0fd_Tx] */
  { /*    16 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/VI_X_CIOMFMS_oFMSNet_243f1e09_Tx] */
  { /*    17 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/VDHR_X_CIOMFMS_oFMSNet_da3fd25a_Tx] */
  { /*    18 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/VP236_X_CIOMFMS_oFMSNet_b08a3073_Tx] */
  { /*    19 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/FMS1_X_CIOMFMS_oFMSNet_d38e3e3b_Tx] */
  { /*    20 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/EEC1_X_CIOMFMS_oFMSNet_2fc23128_Tx] */
  { /*    21 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/AT1T1I1_X_CIOMFMS_oFMSNet_111e0664_Tx] */
  { /*    22 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/CVW_X_CIOMFMS_oFMSNet_f228048c_Tx] */
  { /*    23 */         1u, 0xFEu },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/LFC_X_CIOMFMS_oFMSNet_a4d1166f_Tx] */
  { /*    24 */         1u, 0xFEu }   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289/EEC2_X_CIOMFMS_oFMSNet_8c94b781_Tx] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ComIPduInd
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ComIPduInd
  \brief  the indexes of the 1:1 sorted relation pointing to J1939Rm_ComIPdu
*/ 
#define J1939RM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_ComIPduIndType, J1939RM_CONST) J1939Rm_ComIPduInd[25] = {
  /* Index    ComIPduInd      Referable Keys */
  /*     0 */         0u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*     1 */         1u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*     2 */         2u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*     3 */         3u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*     4 */         4u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*     5 */         5u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*     6 */         6u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*     7 */         7u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*     8 */         8u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*     9 */         9u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    10 */        10u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    11 */        11u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    12 */        12u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    13 */        13u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    14 */        14u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    15 */        15u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    16 */        16u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    17 */        17u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    18 */        18u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    19 */        19u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    20 */        20u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    21 */        21u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    22 */        22u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    23 */        23u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
  /*    24 */        24u   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
};
#define J1939RM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ComMChannel
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ComMChannel
  \brief  DefinitionRef: /[ANY]/ComM/ComMConfigSet/ComMChannel
  \details
  Element       Description
  ChannelIdx    the index of the 0:1 relation pointing to J1939Rm_Channel
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_ComMChannelType, J1939RM_CONST) J1939Rm_ComMChannel[6] = {
    /* Index    ChannelIdx                                Referable Keys */
  { /*     0 */ J1939RM_NO_CHANNELIDXOFCOMMCHANNEL },  /* [unused] */
  { /*     1 */ J1939RM_NO_CHANNELIDXOFCOMMCHANNEL },  /* [unused] */
  { /*     2 */ J1939RM_NO_CHANNELIDXOFCOMMCHANNEL },  /* [unused] */
  { /*     3 */ J1939RM_NO_CHANNELIDXOFCOMMCHANNEL },  /* [unused] */
  { /*     4 */                                 0u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone1J1939_0b1f4bae] */
  { /*     5 */                                 1u }   /* [/ActiveEcuC/ComM/ComMConfigSet/CN_FMSNet_fce1aae5] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_NmNode
**********************************************************************************************************************/
/** 
  \var    J1939Rm_NmNode
  \brief  DefinitionRef: /[ANY]/J1939Nm/J1939NmConfigSet/J1939NmNode
  \details
  Element    Description
  NodeIdx    the index of the 0:1 relation pointing to J1939Rm_Node
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_NmNodeType, J1939RM_CONST) J1939Rm_NmNode[1] = {
    /* Index    NodeIdx        Referable Keys */
  { /*     0 */      0u }   /* [/ActiveEcuC/J1939Nm/J1939NmConfigSet/CIOM_4d5cd289] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_Node
**********************************************************************************************************************/
/** 
  \var    J1939Rm_Node
  \brief  DefinitionRef: /MICROSAR/J1939Rm/J1939RmConfigSet/J1939RmNode
  \details
  Element                               Description
  NodeAddress                           DefinitionRef: /[ANY]/J1939Nm/J1939NmConfigSet/J1939NmNode/J1939NmNodePreferredAddress
  NodeChannelChannelTableIndEndIdx      the end index of the 0:n relation pointing to J1939Rm_NodeChannelChannelTableInd
  NodeChannelChannelTableIndStartIdx    the start index of the 0:n relation pointing to J1939Rm_NodeChannelChannelTableInd
  NodeId                                DefinitionRef: /[ANY]/J1939Nm/J1939NmConfigSet/J1939NmNode/J1939NmNodeId
  NodeUserUserTableIndEndIdx            the end index of the 0:n relation pointing to J1939Rm_NodeUserUserTableInd
  NodeUserUserTableIndStartIdx          the start index of the 0:n relation pointing to J1939Rm_NodeUserUserTableInd
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_NodeType, J1939RM_CONST) J1939Rm_Node[1] = {
    /* Index    NodeAddress  NodeChannelChannelTableIndEndIdx  NodeChannelChannelTableIndStartIdx  NodeId                                 NodeUserUserTableIndEndIdx  NodeUserUserTableIndStartIdx        Referable Keys */
  { /*     0 */         49u,                               2u,                                 0u, J1939NmConf_J1939NmNode_CIOM_4d5cd289,                         2u,                           0u }   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_NodeChannel
**********************************************************************************************************************/
/** 
  \var    J1939Rm_NodeChannel
  \details
  Element                Description
  ChannelChannelIdx      the index of the 1:1 relation pointing to J1939Rm_Channel
  NodeChannelStateIdx    the index of the 1:1 relation pointing to J1939Rm_NodeChannelState
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_NodeChannelType, J1939RM_CONST) J1939Rm_NodeChannel[2] = {
    /* Index    ChannelChannelIdx  NodeChannelStateIdx        Referable Keys */
  { /*     0 */                0u,                  0u },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289] */
  { /*     1 */                1u,                  1u }   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_NodeChannelChannelTableInd
**********************************************************************************************************************/
/** 
  \var    J1939Rm_NodeChannelChannelTableInd
  \brief  the indexes of the 1:1 sorted relation pointing to J1939Rm_NodeChannel
*/ 
#define J1939RM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_NodeChannelChannelTableIndType, J1939RM_CONST) J1939Rm_NodeChannelChannelTableInd[2] = {
  /* Index    NodeChannelChannelTableInd      Referable Keys */
  /*     0 */                         0u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289] */
  /*     1 */                         1u   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289] */
};
#define J1939RM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_NodeUser
**********************************************************************************************************************/
/** 
  \var    J1939Rm_NodeUser
  \details
  Element        Description
  UserUserIdx    the index of the 1:1 relation pointing to J1939Rm_User
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_NodeUserType, J1939RM_CONST) J1939Rm_NodeUser[2] = {
    /* Index    UserUserIdx        Referable Keys */
  { /*     0 */          0u },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289] */
  { /*     1 */          1u }   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_NodeUserUserTableInd
**********************************************************************************************************************/
/** 
  \var    J1939Rm_NodeUserUserTableInd
  \brief  the indexes of the 1:1 sorted relation pointing to J1939Rm_NodeUser
*/ 
#define J1939RM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_NodeUserUserTableIndType, J1939RM_CONST) J1939Rm_NodeUserUserTableInd[2] = {
  /* Index    NodeUserUserTableInd      Referable Keys */
  /*     0 */                   0u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289] */
  /*     1 */                   1u   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289] */
};
#define J1939RM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ReqQueueInd
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ReqQueueInd
  \brief  the indexes of the 1:1 sorted relation pointing to J1939Rm_ReqQueue
*/ 
#define J1939RM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_ReqQueueIndType, J1939RM_CONST) J1939Rm_ReqQueueInd[20] = {
  /* Index    ReqQueueInd      Referable Keys */
  /*     0 */          0u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     1 */          1u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     2 */          2u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     3 */          3u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     4 */          4u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     5 */          5u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     6 */          6u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     7 */          7u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     8 */          8u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     9 */          9u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*    10 */         10u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    11 */         11u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    12 */         12u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    13 */         13u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    14 */         14u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    15 */         15u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    16 */         16u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    17 */         17u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    18 */         18u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    19 */         19u   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
};
#define J1939RM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ReqTimeoutInd
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ReqTimeoutInd
  \brief  the indexes of the 1:1 sorted relation pointing to J1939Rm_ReqTimeout
*/ 
#define J1939RM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_ReqTimeoutIndType, J1939RM_CONST) J1939Rm_ReqTimeoutInd[20] = {
  /* Index    ReqTimeoutInd      Referable Keys */
  /*     0 */            0u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     1 */            1u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     2 */            2u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     3 */            3u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     4 */            4u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     5 */            5u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     6 */            6u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     7 */            7u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     8 */            8u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     9 */            9u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*    10 */           10u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    11 */           11u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    12 */           12u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    13 */           13u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    14 */           14u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    15 */           15u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    16 */           16u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    17 */           17u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    18 */           18u,  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    19 */           19u   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
};
#define J1939RM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_RequestIndication
**********************************************************************************************************************/
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_RequestIndicationFctPtrType, J1939RM_CONST) J1939Rm_RequestIndication[1] = {
  /* Index    RequestIndication              Referable Keys */
  /*     0 */J1939Nm_RequestIndication    /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/CIOM_cbc3dd0f] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_RxPdu
**********************************************************************************************************************/
/** 
  \var    J1939Rm_RxPdu
  \brief  List of received PDUs
  \details
  Element           Description
  ChannelIdx        the index of the 1:1 relation pointing to J1939Rm_Channel
  MetaDataLength    MetaData Length
  PduType           PDU Type
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_RxPduType, J1939RM_CONST) J1939Rm_RxPdu[2] = {
    /* Index    ChannelIdx  MetaDataLength  PduType                                Referable Keys */
  { /*     0 */         0u,             4u, J1939RM_PDU_RQST_PDUTYPEOFRXPDU },  /* [/ActiveEcuC/EcuC/EcucPduCollection/RqstRxPdu_Backbone1J1939_54966c1b] */
  { /*     1 */         1u,             4u, J1939RM_PDU_RQST_PDUTYPEOFRXPDU }   /* [/ActiveEcuC/EcuC/EcucPduCollection/RqstRxPdu_FMSNet_J1939_44d89c3b] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_TxPdu
**********************************************************************************************************************/
/** 
  \var    J1939Rm_TxPdu
  \brief  List of transmitted PDUs
  \details
  Element           Description
  PduId             PDU ID
  ChannelIdx        the index of the 1:1 relation pointing to J1939Rm_Channel
  MetaDataLength    MetaData Length
  PduType           PDU Type
  TxPduStateIdx     the index of the 1:1 relation pointing to J1939Rm_TxPduState
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_TxPduType, J1939RM_CONST) J1939Rm_TxPdu[2] = {
    /* Index    PduId                             ChannelIdx  MetaDataLength  PduType                          TxPduStateIdx        Referable Keys */
  { /*     0 */   PduRConf_PduRSrcPdu_PduRSrcPdu,         0u,             4u, J1939RM_PDU_ACKM_PDUTYPEOFTXPDU,            0u },  /* [/ActiveEcuC/EcuC/EcucPduCollection/AckmTxPdu_Backbone1J1939_54966c1b] */
  { /*     1 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1,         1u,             4u, J1939RM_PDU_ACKM_PDUTYPEOFTXPDU,            1u }   /* [/ActiveEcuC/EcuC/EcucPduCollection/AckmTxPdu_FMSNet_J1939_44d89c3b] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_User
**********************************************************************************************************************/
/** 
  \var    J1939Rm_User
  \brief  DefinitionRef: /MICROSAR/J1939Rm/J1939RmConfigSet/J1939RmNode/J1939RmUser
  \details
  Element                  Description
  UserRequestIndication    DefinitionRef: /MICROSAR/J1939Rm/J1939RmConfigSet/J1939RmNode/J1939RmUser/J1939RmUserRequestIndication
  ComIPduIndEndIdx         the end index of the 0:n relation pointing to J1939Rm_ComIPduInd
  ComIPduIndStartIdx       the start index of the 0:n relation pointing to J1939Rm_ComIPduInd
  NodeIdx                  the index of the 1:1 relation pointing to J1939Rm_Node
  RequestIndicationIdx     the index of the 0:1 relation pointing to J1939Rm_RequestIndication
  UserPGNIndEndIdx         the end index of the 0:n relation pointing to J1939Rm_UserPGNInd
  UserPGNIndStartIdx       the start index of the 0:n relation pointing to J1939Rm_UserPGNInd
  UserType                 DefinitionRef: /MICROSAR/J1939Rm/J1939RmConfigSet/J1939RmNode/J1939RmUser/J1939RmUserType
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_UserType, J1939RM_CONST) J1939Rm_User[2] = {
    /* Index    UserRequestIndication  ComIPduIndEndIdx                   ComIPduIndStartIdx                   NodeIdx  RequestIndicationIdx                   UserPGNIndEndIdx                   UserPGNIndStartIdx                   UserType                                   Referable Keys */
  { /*     0 */                  TRUE, J1939RM_NO_COMIPDUINDENDIDXOFUSER, J1939RM_NO_COMIPDUINDSTARTIDXOFUSER,      0u,                                    0u,                                1u,                                  0u, J1939RM_USER_J1939NM_USERTYPEOFUSER },  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/CIOM_cbc3dd0f] */
  { /*     1 */                 FALSE,                               25u,                                  0u,      0u, J1939RM_NO_REQUESTINDICATIONIDXOFUSER, J1939RM_NO_USERPGNINDENDIDXOFUSER, J1939RM_NO_USERPGNINDSTARTIDXOFUSER,     J1939RM_USER_COM_USERTYPEOFUSER }   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/Com_CIOM_4d5cd289] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_UserPGN
**********************************************************************************************************************/
/** 
  \var    J1939Rm_UserPGN
  \details
  Element    Description
  PGN    
*/ 
#define J1939RM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_UserPGNType, J1939RM_CONST) J1939Rm_UserPGN[1] = {
    /* Index    PGN                 Referable Keys */
  { /*     0 */ 0x0000EE00uL }   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/CIOM_cbc3dd0f] */
};
#define J1939RM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_UserPGNInd
**********************************************************************************************************************/
/** 
  \var    J1939Rm_UserPGNInd
  \brief  the indexes of the 1:1 sorted relation pointing to J1939Rm_UserPGN
*/ 
#define J1939RM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
CONST(J1939Rm_UserPGNIndType, J1939RM_CONST) J1939Rm_UserPGNInd[1] = {
  /* Index    UserPGNInd      Referable Keys */
  /*     0 */         0u   /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289/CIOM_cbc3dd0f] */
};
#define J1939RM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_AckQueue
**********************************************************************************************************************/
/** 
  \var    J1939Rm_AckQueue
  \brief  Queued Acknowledgement messages per J1939Rm channel
*/ 
#define J1939RM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(J1939Rm_AckQueueUType, J1939RM_VAR_NOINIT) J1939Rm_AckQueue;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     1 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     2 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     3 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     4 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     5 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     6 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     7 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     8 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     9 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*    10 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    11 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    12 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    13 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    14 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    15 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    16 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    17 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    18 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    19 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */

#define J1939RM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ChannelState
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ChannelState
  \brief  State of J1939Rm channel
*/ 
#define J1939RM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(J1939Rm_ChannelStateUType, J1939RM_VAR_NOINIT) J1939Rm_ChannelState;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     1 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */

#define J1939RM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_NodeChannelState
**********************************************************************************************************************/
/** 
  \var    J1939Rm_NodeChannelState
  \brief  State of J1939Rm node per channel
*/ 
#define J1939RM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(J1939Rm_NodeChannelStateType, J1939RM_VAR_NOINIT) J1939Rm_NodeChannelState[2];
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289::/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     1 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/CIOM_4d5cd289::/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */

#define J1939RM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ReqQueue
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ReqQueue
  \brief  Queued Request messages per J1939Rm channel
*/ 
#define J1939RM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(J1939Rm_ReqQueueUType, J1939RM_VAR_NOINIT) J1939Rm_ReqQueue;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     1 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     2 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     3 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     4 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     5 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     6 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     7 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     8 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     9 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*    10 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    11 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    12 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    13 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    14 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    15 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    16 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    17 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    18 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    19 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */

#define J1939RM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_ReqTimeout
**********************************************************************************************************************/
/** 
  \var    J1939Rm_ReqTimeout
  \brief  Timeout monitors per J1939Rm channel
*/ 
#define J1939RM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(J1939Rm_ReqTimeoutUType, J1939RM_VAR_NOINIT) J1939Rm_ReqTimeout;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     1 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     2 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     3 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     4 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     5 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     6 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     7 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     8 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*     9 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/Backbone1J1939_54966c1b] */
  /*    10 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    11 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    12 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    13 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    14 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    15 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    16 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    17 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    18 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */
  /*    19 */  /* [/ActiveEcuC/J1939Rm/J1939RmConfigSet/FMSNet_J1939_44d89c3b] */

#define J1939RM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  J1939Rm_TxPduState
**********************************************************************************************************************/
/** 
  \var    J1939Rm_TxPduState
  \brief  State of J1939Rm Tx PDU
*/ 
#define J1939RM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
VAR(J1939Rm_TxPduStateUType, J1939RM_VAR_NOINIT) J1939Rm_TxPduState;  /* PRQA S 0759 */  /* MD_CSL_18.4 */
  /* Index        Referable Keys  */
  /*     0 */  /* [/ActiveEcuC/EcuC/EcucPduCollection/AckmTxPdu_Backbone1J1939_54966c1b] */
  /*     1 */  /* [/ActiveEcuC/EcuC/EcucPduCollection/AckmTxPdu_FMSNet_J1939_44d89c3b] */

#define J1939RM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */



/**********************************************************************************************************************
  LOCAL FUNCTION PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL FUNCTIONS
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL FUNCTIONS
**********************************************************************************************************************/


/**********************************************************************************************************************
  END OF FILE: J1939Rm_Cfg.c
**********************************************************************************************************************/

