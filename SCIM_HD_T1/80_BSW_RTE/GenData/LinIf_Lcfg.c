/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: LinIf
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: LinIf_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:33
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


#define LINIF_LCFG_SOURCE

/**********************************************************************************************************************
 *  MISRA
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  Includes
 *********************************************************************************************************************/

#include "LinIf.h"
#include "PduR_LinIf.h"
#include "LinSM_Cbk.h"
#include "LinIf_LinTrcv.h"


/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/



/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/



/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  LinIf_ChannelConfig
**********************************************************************************************************************/
/** 
  \var    LinIf_ChannelConfig
  \details
  Element                         Description
  ScheduleChangeBeforeSlotEnd     Point of schedule table change. True = after maximum frame time, False = at end of schedule slot.
  ChannelFuncCallCycle            Channel function call cycle
  FrameLengthDelayListEndIdx      the end index of the 0:n relation pointing to LinIf_FrameLengthDelayList
  FrameLengthDelayListStartIdx    the start index of the 0:n relation pointing to LinIf_FrameLengthDelayList
  FrameListEndIdx                 the end index of the 1:n relation pointing to LinIf_FrameList
  FrameListStartIdx               the start index of the 1:n relation pointing to LinIf_FrameList
  LoadBalancingOffset             Load balancing offset in ticks
  NumberOfSchedules               Number of schedule tables
  ScheduleTableListIndEndIdx      the end index of the 1:n relation pointing to LinIf_ScheduleTableListInd
  ScheduleTableListIndStartIdx    the start index of the 1:n relation pointing to LinIf_ScheduleTableListInd
  StartupState                    Startup state of channel: 0 : sleep 1 : operational state
  TimeBase                        Channel time base
  WakeupDelayExternal             External wakeup delay in ticks
  WakeupDelayInternal             Internal wakeup delay in ticks
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_ChannelConfigType, LINIF_CONST) LinIf_ChannelConfig[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ScheduleChangeBeforeSlotEnd  ChannelFuncCallCycle  FrameLengthDelayListEndIdx  FrameLengthDelayListStartIdx  FrameListEndIdx  FrameListStartIdx  LoadBalancingOffset  NumberOfSchedules  ScheduleTableListIndEndIdx  ScheduleTableListIndStartIdx  StartupState  TimeBase  WakeupDelayExternal  WakeupDelayInternal */
  { /*     0 */                       FALSE,                   1u,                         8u,                           0u,             17u,                0u,                  1u,                6u,                         7u,                           0u,           0u,       5u,                  1u,                  8u },
  { /*     1 */                       FALSE,                   1u,                        16u,                           8u,             30u,               17u,                  1u,                4u,                        12u,                           7u,           0u,       5u,                  1u,                  8u },
  { /*     2 */                       FALSE,                   1u,                        24u,                          16u,             40u,               30u,                  1u,                6u,                        19u,                          12u,           0u,       5u,                  1u,                  8u },
  { /*     3 */                       FALSE,                   1u,                        32u,                          24u,             53u,               40u,                  1u,                6u,                        26u,                          19u,           0u,       5u,                  1u,                  8u },
  { /*     4 */                       FALSE,                   1u,                        40u,                          32u,             63u,               53u,                  1u,                8u,                        35u,                          26u,           0u,       5u,                  1u,                  8u },
  { /*     5 */                       FALSE,                   1u,                        48u,                          40u,             67u,               63u,                  1u,                3u,                        39u,                          35u,           0u,       5u,                 20u,                  8u },
  { /*     6 */                       FALSE,                   1u,                        56u,                          48u,             71u,               67u,                  1u,                3u,                        43u,                          39u,           0u,       5u,                 20u,                  8u },
  { /*     7 */                       FALSE,                   1u,                        64u,                          56u,             75u,               71u,                  1u,                3u,                        47u,                          43u,           0u,       5u,                 20u,                  8u }
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_ChannelIndTable
**********************************************************************************************************************/
/** 
  \var    LinIf_ChannelIndTable
  \details
  Element               Description
  LinChannelIndex   
  SystemChannelIndex
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_ChannelIndTableType, LINIF_CONST) LinIf_ChannelIndTable[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    LinChannelIndex                               SystemChannelIndex                              */
  { /*     0 */ 0 /* LinConf_LinChannel_CN_LIN00_2cd9a7df */,  6 /* ComMConf_ComMChannel_CN_LIN00_2cd9a7df */ },
  { /*     1 */ 1 /* LinConf_LinChannel_CN_LIN01_5bde9749 */,  7 /* ComMConf_ComMChannel_CN_LIN01_5bde9749 */ },
  { /*     2 */ 2 /* LinConf_LinChannel_CN_LIN02_c2d7c6f3 */,  8 /* ComMConf_ComMChannel_CN_LIN02_c2d7c6f3 */ },
  { /*     3 */ 3 /* LinConf_LinChannel_CN_LIN03_b5d0f665 */,  9 /* ComMConf_ComMChannel_CN_LIN03_b5d0f665 */ },
  { /*     4 */ 4 /* LinConf_LinChannel_CN_LIN04_2bb463c6 */, 10 /* ComMConf_ComMChannel_CN_LIN04_2bb463c6 */ },
  { /*     5 */ 5 /* LinConf_LinChannel_CN_LIN05_5cb35350 */, 11 /* ComMConf_ComMChannel_CN_LIN05_5cb35350 */ },
  { /*     6 */ 6 /* LinConf_LinChannel_CN_LIN06_c5ba02ea */, 12 /* ComMConf_ComMChannel_CN_LIN06_c5ba02ea */ },
  { /*     7 */ 7 /* LinConf_LinChannel_CN_LIN07_b2bd327c */, 13 /* ComMConf_ComMChannel_CN_LIN07_b2bd327c */ }
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_ChannelToDrvWakeupInfo
**********************************************************************************************************************/
/** 
  \var    LinIf_ChannelToDrvWakeupInfo
  \details
  Element            Description
  LinWakeupSource    Defines the ID of the wakeup source. Zero means invalid source and that external wakeup is not supported.
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_ChannelToDrvWakeupInfoType, LINIF_CONST) LinIf_ChannelToDrvWakeupInfo[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    LinWakeupSource */
  { /*     0 */     0x00000800u },
  { /*     1 */     0x00001000u },
  { /*     2 */     0x00002000u },
  { /*     3 */     0x00004000u },
  { /*     4 */     0x00008000u },
  { /*     5 */     0x00020000u },
  { /*     6 */     0x00080000u },
  { /*     7 */     0x00200000u }
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_Entry
**********************************************************************************************************************/
/** 
  \var    LinIf_Entry
  \details
  Element                 Description
  DelayInTimebaseTicks
  FrameListIdx            the index of the 1:1 relation pointing to LinIf_FrameList
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_EntryType, LINIF_CONST) LinIf_Entry[190] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    DelayInTimebaseTicks  FrameListIdx        Comment                                            Referable Keys */
  { /*     0 */                   4u,          15u },  /* [FT_MasterReq_355bc768_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_03d8aeb9] */
  { /*     1 */                   4u,          16u },  /* [FT_SlaveResp_9d5cbedc_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_03d8aeb9] */
  { /*     2 */                   4u,          15u },  /* [FT_MasterReq_355bc768_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_1_2a158a89] */
  { /*     3 */                   4u,          16u },  /* [FT_SlaveResp_9d5cbedc_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_1_2a158a89] */
  { /*     4 */                   2u,           3u },  /* [FT_Backlight_FuncInd_L1_7f2b7abe_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_1_2a158a89] */
  { /*     5 */                   3u,           4u },  /* [FT_FSP_1_2_Req_L1_d20f4458_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_1_2a158a89] */
  { /*     6 */                   2u,           8u },  /* [FT_FSP1_Frame_L1_11528ace_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_1_2a158a89] */
  { /*     7 */                   2u,           7u },  /* [FT_FSP2_Frame_L1_fa6531cd_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_1_2a158a89] */
  { /*     8 */                   2u,           0u },  /* [FT_CIOMtoSlaves1_L1_417ce0d9_Tx]      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_1_2a158a89] */
  { /*     9 */                   3u,          12u },  /* [FT_ILCP1toCIOM_L1_c9f6c6bc_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_1_2a158a89] */
  { /*    10 */                   4u,          15u },  /* [FT_MasterReq_355bc768_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_2_b31cdb33] */
  { /*    11 */                   4u,          16u },  /* [FT_SlaveResp_9d5cbedc_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_2_b31cdb33] */
  { /*    12 */                   3u,           2u },  /* [FT_CIOMtoSlaves2_FR1_L1_82486131_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_2_b31cdb33] */
  { /*    13 */                   4u,          14u },  /* [FT_LECM2toCIOM_FR1_L1_5fb10dc0_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_2_b31cdb33] */
  { /*    14 */                   2u,           6u },  /* [FT_LECM2toCIOM_FR2_L1_4d04a22e_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_2_b31cdb33] */
  { /*    15 */                   3u,          13u },  /* [FT_LECMBasic2CIOM_L1_c4710275_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_2_b31cdb33] */
  { /*    16 */                   4u,           1u },  /* [FT_CIOMtoSlaves2_FR2_L1_90fdcedf_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_2_b31cdb33] */
  { /*    17 */                   3u,           5u },  /* [FT_CIOMtoSlaves2_FR3_L1_2841a9ba_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_2_b31cdb33] */
  { /*    18 */                   3u,           9u },  /* [FT_LECM2toCIOM_FR3_L1_f5b8c54b_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/MasterReq_SlaveResp_Table_2_b31cdb33] */
  { /*    19 */                   2u,           3u },  /* [FT_Backlight_FuncInd_L1_7f2b7abe_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_1_2abffe93] */
  { /*    20 */                   3u,           4u },  /* [FT_FSP_1_2_Req_L1_d20f4458_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_1_2abffe93] */
  { /*    21 */                   2u,           8u },  /* [FT_FSP1_Frame_L1_11528ace_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_1_2abffe93] */
  { /*    22 */                   2u,           7u },  /* [FT_FSP2_Frame_L1_fa6531cd_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_1_2abffe93] */
  { /*    23 */                   2u,           0u },  /* [FT_CIOMtoSlaves1_L1_417ce0d9_Tx]      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_1_2abffe93] */
  { /*    24 */                   3u,          12u },  /* [FT_ILCP1toCIOM_L1_c9f6c6bc_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_1_2abffe93] */
  { /*    25 */                   3u,           2u },  /* [FT_CIOMtoSlaves2_FR1_L1_82486131_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    26 */                   4u,          14u },  /* [FT_LECM2toCIOM_FR1_L1_5fb10dc0_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    27 */                   2u,           6u },  /* [FT_LECM2toCIOM_FR2_L1_4d04a22e_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    28 */                   3u,          13u },  /* [FT_LECMBasic2CIOM_L1_c4710275_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    29 */                   4u,           1u },  /* [FT_CIOMtoSlaves2_FR2_L1_90fdcedf_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    30 */                   3u,           2u },  /* [FT_CIOMtoSlaves2_FR1_L1_82486131_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    31 */                   4u,          14u },  /* [FT_LECM2toCIOM_FR1_L1_5fb10dc0_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    32 */                   2u,           6u },  /* [FT_LECM2toCIOM_FR2_L1_4d04a22e_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    33 */                   3u,          13u },  /* [FT_LECMBasic2CIOM_L1_c4710275_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    34 */                   3u,           5u },  /* [FT_CIOMtoSlaves2_FR3_L1_2841a9ba_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    35 */                   3u,           2u },  /* [FT_CIOMtoSlaves2_FR1_L1_82486131_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    36 */                   4u,          14u },  /* [FT_LECM2toCIOM_FR1_L1_5fb10dc0_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    37 */                   2u,           6u },  /* [FT_LECM2toCIOM_FR2_L1_4d04a22e_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    38 */                   3u,          13u },  /* [FT_LECMBasic2CIOM_L1_c4710275_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    39 */                   3u,           9u },  /* [FT_LECM2toCIOM_FR3_L1_f5b8c54b_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    40 */                   3u,           2u },  /* [FT_CIOMtoSlaves2_FR1_L1_82486131_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    41 */                   4u,          14u },  /* [FT_LECM2toCIOM_FR1_L1_5fb10dc0_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    42 */                   2u,           6u },  /* [FT_LECM2toCIOM_FR2_L1_4d04a22e_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    43 */                   3u,          13u },  /* [FT_LECMBasic2CIOM_L1_c4710275_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_2_b3b6af29] */
  { /*    44 */                   4u,          11u },  /* [FT_FSP1_SwitchDetResp_L1_3c4fc6c8_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_e_46b96b7e] */
  { /*    45 */                   4u,          10u },  /* [FT_FSP2_SwitchDetResp_L1_d6c91baa_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_e_46b96b7e] */
  { /*    46 */                   2u,           0u },  /* [FT_CIOMtoSlaves1_L1_417ce0d9_Tx]      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_e_46b96b7e] */
  { /*    47 */                   3u,          12u },  /* [FT_ILCP1toCIOM_L1_c9f6c6bc_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/Table_e_46b96b7e] */
  { /*    48 */                   4u,          28u },  /* [FT_MasterReq_e2b94730_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_e4c5082e] */
  { /*    49 */                   4u,          29u },  /* [FT_SlaveResp_4abe3e84_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_e4c5082e] */
    /* Index    DelayInTimebaseTicks  FrameListIdx        Comment                                            Referable Keys */
  { /*    50 */                   4u,          28u },  /* [FT_MasterReq_e2b94730_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_Table0_9e3481d6] */
  { /*    51 */                   4u,          29u },  /* [FT_SlaveResp_4abe3e84_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_Table0_9e3481d6] */
  { /*    52 */                   2u,          17u },  /* [FT_Backlight_FuncInd_L2_da42c80c_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_Table0_9e3481d6] */
  { /*    53 */                   3u,          19u },  /* [FT_FSP_1_2_Req_L2_ca2370c5_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_Table0_9e3481d6] */
  { /*    54 */                   3u,          18u },  /* [FT_FSP_3_4_Req_L2_08b9a97f_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_Table0_9e3481d6] */
  { /*    55 */                   2u,          27u },  /* [FT_FSP1_Frame_L2_132831a0_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_Table0_9e3481d6] */
  { /*    56 */                   2u,          25u },  /* [FT_FSP2_Frame_L2_f81f8aa3_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_Table0_9e3481d6] */
  { /*    57 */                   2u,          20u },  /* [FT_FSP3_Frame_L2_17dde19d_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_Table0_9e3481d6] */
  { /*    58 */                   2u,          26u },  /* [FT_FSP4_Frame_L2_f501fae4_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/MasterReq_SlaveResp_Table0_9e3481d6] */
  { /*    59 */                   2u,          17u },  /* [FT_Backlight_FuncInd_L2_da42c80c_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table0_df24d2a0] */
  { /*    60 */                   3u,          19u },  /* [FT_FSP_1_2_Req_L2_ca2370c5_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table0_df24d2a0] */
  { /*    61 */                   3u,          18u },  /* [FT_FSP_3_4_Req_L2_08b9a97f_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table0_df24d2a0] */
  { /*    62 */                   2u,          27u },  /* [FT_FSP1_Frame_L2_132831a0_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table0_df24d2a0] */
  { /*    63 */                   2u,          25u },  /* [FT_FSP2_Frame_L2_f81f8aa3_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table0_df24d2a0] */
  { /*    64 */                   2u,          20u },  /* [FT_FSP3_Frame_L2_17dde19d_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table0_df24d2a0] */
  { /*    65 */                   2u,          26u },  /* [FT_FSP4_Frame_L2_f501fae4_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table0_df24d2a0] */
  { /*    66 */                   4u,          22u },  /* [FT_FSP1_SwitchDetResp_L2_aba17fa3_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table_e_dbb68a08] */
  { /*    67 */                   4u,          24u },  /* [FT_FSP2_SwitchDetResp_L2_4127a2c1_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table_e_dbb68a08] */
  { /*    68 */                   4u,          23u },  /* [FT_FSP3_SwitchDetResp_L2_ae751420_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table_e_dbb68a08] */
  { /*    69 */                   4u,          21u },  /* [FT_FSP4_SwitchDetResp_L2_4f5b1e44_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/Table_e_dbb68a08] */
  { /*    70 */                   4u,          38u },  /* [FT_MasterReq_41efc199_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_1692e5d6] */
  { /*    71 */                   4u,          39u },  /* [FT_SlaveResp_e9e8b82d_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_1692e5d6] */
  { /*    72 */                   4u,          38u },  /* [FT_MasterReq_41efc199_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_1_631128e5] */
  { /*    73 */                   4u,          39u },  /* [FT_SlaveResp_e9e8b82d_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_1_631128e5] */
  { /*    74 */                   2u,          32u },  /* [FT_Backlight_FuncInd_L3_e9e4dd82_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_1_631128e5] */
  { /*    75 */                   3u,          31u },  /* [FT_FSP_1_2_Req_L3_e53ae97b_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_1_631128e5] */
  { /*    76 */                   2u,          37u },  /* [FT_FSP1_Frame_L3_12ca380b_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_1_631128e5] */
  { /*    77 */                   2u,          33u },  /* [FT_FSP2_Frame_L3_f9fd8308_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_1_631128e5] */
  { /*    78 */                   2u,          30u },  /* [FT_MastertoTCP_a2bc639c_Tx]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_1_631128e5] */
  { /*    79 */                   3u,          36u },  /* [FT_TCPtoMaster_4ab36a9a_Rx]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_1_631128e5] */
  { /*    80 */                   4u,          38u },  /* [FT_MasterReq_41efc199_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_2_fa18795f] */
  { /*    81 */                   4u,          39u },  /* [FT_SlaveResp_e9e8b82d_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_2_fa18795f] */
  { /*    82 */                   2u,          32u },  /* [FT_Backlight_FuncInd_L3_e9e4dd82_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_2_fa18795f] */
  { /*    83 */                   3u,          31u },  /* [FT_FSP_1_2_Req_L3_e53ae97b_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_2_fa18795f] */
  { /*    84 */                   2u,          37u },  /* [FT_FSP1_Frame_L3_12ca380b_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/MasterReq_SlaveResp_Table_2_fa18795f] */
  { /*    85 */                   2u,          32u },  /* [FT_Backlight_FuncInd_L3_e9e4dd82_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_1_cbd13a3e] */
  { /*    86 */                   3u,          31u },  /* [FT_FSP_1_2_Req_L3_e53ae97b_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_1_cbd13a3e] */
  { /*    87 */                   2u,          37u },  /* [FT_FSP1_Frame_L3_12ca380b_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_1_cbd13a3e] */
  { /*    88 */                   2u,          33u },  /* [FT_FSP2_Frame_L3_f9fd8308_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_1_cbd13a3e] */
  { /*    89 */                   2u,          30u },  /* [FT_MastertoTCP_a2bc639c_Tx]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_1_cbd13a3e] */
  { /*    90 */                   3u,          36u },  /* [FT_TCPtoMaster_4ab36a9a_Rx]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_1_cbd13a3e] */
  { /*    91 */                   2u,          32u },  /* [FT_Backlight_FuncInd_L3_e9e4dd82_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_2_52d86b84] */
  { /*    92 */                   3u,          31u },  /* [FT_FSP_1_2_Req_L3_e53ae97b_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_2_52d86b84] */
  { /*    93 */                   2u,          37u },  /* [FT_FSP1_Frame_L3_12ca380b_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_2_52d86b84] */
  { /*    94 */                   4u,          35u },  /* [FT_FSP1_SwitchDetResp_L3_cf8e7646_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_e_a7d7afd3] */
  { /*    95 */                   4u,          34u },  /* [FT_FSP2_SwitchDetResp_L3_2508ab24_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/Table_e_a7d7afd3] */
  { /*    96 */                   4u,          51u },  /* [FT_MasterReq_960d41c1_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_f18f4341] */
  { /*    97 */                   4u,          52u },  /* [FT_SlaveResp_3e0a3875_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_f18f4341] */
  { /*    98 */                   4u,          51u },  /* [FT_MasterReq_960d41c1_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_1_479379d3] */
  { /*    99 */                   4u,          52u },  /* [FT_SlaveResp_3e0a3875_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_1_479379d3] */
    /* Index    DelayInTimebaseTicks  FrameListIdx        Comment                                            Referable Keys */
  { /*   100 */                   2u,          41u },  /* [FT_CIOMtoSlaves1_L4_c341f9ae_Tx]      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_1_479379d3] */
  { /*   101 */                   2u,          40u },  /* [FT_Backlight_FuncInd_L4_4be0ab29_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_1_479379d3] */
  { /*   102 */                   3u,          43u },  /* [FT_FSP_1_2_Req_L4_fa7b19ff_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_1_479379d3] */
  { /*   103 */                   2u,          45u },  /* [FT_FSP1_Frame_L4_17dd477c_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_1_479379d3] */
  { /*   104 */                   3u,          50u },  /* [FT_ELCP1toCIOM_L4_cb770a36_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_1_479379d3] */
  { /*   105 */                   4u,          51u },  /* [FT_MasterReq_960d41c1_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_2_de9a2869] */
  { /*   106 */                   4u,          52u },  /* [FT_SlaveResp_3e0a3875_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_2_de9a2869] */
  { /*   107 */                   2u,          42u },  /* [FT_CIOMtoSlaves2_L4_d1f45640_Tx]      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_2_de9a2869] */
  { /*   108 */                   2u,          40u },  /* [FT_Backlight_FuncInd_L4_4be0ab29_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_2_de9a2869] */
  { /*   109 */                   3u,          46u },  /* [FT_ELCP2toCIOM_L4_2040b135_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_2_de9a2869] */
  { /*   110 */                   3u,          48u },  /* [FT_CCFWtoCIOM_L4_41a1d427_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_2_de9a2869] */
  { /*   111 */                   3u,          49u },  /* [FT_DLFWtoCIOM_L4_4e464d0e_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_2_de9a2869] */
  { /*   112 */                   3u,          47u },  /* [FT_ILCP2toCIOM_L4_0ab52018_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/MasterReq_SlaveResp_Table_2_de9a2869] */
  { /*   113 */                   2u,          41u },  /* [FT_CIOMtoSlaves1_L4_c341f9ae_Tx]      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_1_56dedb48] */
  { /*   114 */                   2u,          40u },  /* [FT_Backlight_FuncInd_L4_4be0ab29_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_1_56dedb48] */
  { /*   115 */                   3u,          43u },  /* [FT_FSP_1_2_Req_L4_fa7b19ff_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_1_56dedb48] */
  { /*   116 */                   2u,          45u },  /* [FT_FSP1_Frame_L4_17dd477c_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_1_56dedb48] */
  { /*   117 */                   3u,          50u },  /* [FT_ELCP1toCIOM_L4_cb770a36_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_1_56dedb48] */
  { /*   118 */                   2u,          42u },  /* [FT_CIOMtoSlaves2_L4_d1f45640_Tx]      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_2_cfd78af2] */
  { /*   119 */                   2u,          40u },  /* [FT_Backlight_FuncInd_L4_4be0ab29_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_2_cfd78af2] */
  { /*   120 */                   3u,          46u },  /* [FT_ELCP2toCIOM_L4_2040b135_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_2_cfd78af2] */
  { /*   121 */                   3u,          48u },  /* [FT_CCFWtoCIOM_L4_41a1d427_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_2_cfd78af2] */
  { /*   122 */                   3u,          49u },  /* [FT_DLFWtoCIOM_L4_4e464d0e_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_2_cfd78af2] */
  { /*   123 */                   3u,          47u },  /* [FT_ILCP2toCIOM_L4_0ab52018_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_2_cfd78af2] */
  { /*   124 */                   4u,          44u },  /* [FT_FSP1_SwitchDetResp_L4_5f0d0b34_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_e_3ad84ea5] */
  { /*   125 */                   2u,          41u },  /* [FT_CIOMtoSlaves1_L4_c341f9ae_Tx]      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_e_3ad84ea5] */
  { /*   126 */                   3u,          50u },  /* [FT_ELCP1toCIOM_L4_cb770a36_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/Table_e_3ad84ea5] */
  { /*   127 */                   4u,          61u },  /* [FT_MasterReq_dc33ca8a_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_294c3867] */
  { /*   128 */                   4u,          62u },  /* [FT_SlaveResp_7434b33e_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_294c3867] */
  { /*   129 */                   4u,          61u },  /* [FT_MasterReq_dc33ca8a_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_1_b81cce51] */
  { /*   130 */                   4u,          62u },  /* [FT_SlaveResp_7434b33e_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_1_b81cce51] */
  { /*   131 */                   3u,          57u },  /* [FT_RCECStoCIOM_L5_a45c9d29_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_1_b81cce51] */
  { /*   132 */                   2u,          53u },  /* [FT_CIOMtoSlaves_L5_8632ed67_Tx]       */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_1_b81cce51] */
  { /*   133 */                   2u,          54u },  /* [FT_Backlight_FuncInd_L5_89c53287_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_1_b81cce51] */
  { /*   134 */                   3u,          55u },  /* [FT_FSP_1_2_Req_L5_bc641e1e_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_1_b81cce51] */
  { /*   135 */                   2u,          59u },  /* [FT_FSP1_Frame_L5_1663ef44_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_1_b81cce51] */
  { /*   136 */                   4u,          61u },  /* [FT_MasterReq_dc33ca8a_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_2_21159feb] */
  { /*   137 */                   4u,          62u },  /* [FT_SlaveResp_7434b33e_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_2_21159feb] */
  { /*   138 */                   2u,          53u },  /* [FT_CIOMtoSlaves_L5_8632ed67_Tx]       */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_2_21159feb] */
  { /*   139 */                   2u,          54u },  /* [FT_Backlight_FuncInd_L5_89c53287_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_2_21159feb] */
  { /*   140 */                   3u,          57u },  /* [FT_RCECStoCIOM_L5_a45c9d29_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_2_21159feb] */
  { /*   141 */                   4u,          61u },  /* [FT_MasterReq_dc33ca8a_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_3_5612af7d] */
  { /*   142 */                   4u,          62u },  /* [FT_SlaveResp_7434b33e_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_3_5612af7d] */
  { /*   143 */                   3u,          57u },  /* [FT_RCECStoCIOM_L5_a45c9d29_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_3_5612af7d] */
  { /*   144 */                   2u,          60u },  /* [FT_RCECStoCIOM_FR2_L5_73347414_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_3_5612af7d] */
  { /*   145 */                   2u,          53u },  /* [FT_CIOMtoSlaves_L5_8632ed67_Tx]       */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_3_5612af7d] */
  { /*   146 */                   2u,          56u },  /* [FT_CIOMtoSlaves_FR2_L5_72103487_Tx]   */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_3_5612af7d] */
  { /*   147 */                   2u,          54u },  /* [FT_Backlight_FuncInd_L5_89c53287_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/MasterReq_SlaveResp_Table_3_5612af7d] */
  { /*   148 */                   3u,          57u },  /* [FT_RCECStoCIOM_L5_a45c9d29_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_1_33137188] */
  { /*   149 */                   2u,          53u },  /* [FT_CIOMtoSlaves_L5_8632ed67_Tx]       */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_1_33137188] */
    /* Index    DelayInTimebaseTicks  FrameListIdx        Comment                                            Referable Keys */
  { /*   150 */                   2u,          54u },  /* [FT_Backlight_FuncInd_L5_89c53287_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_1_33137188] */
  { /*   151 */                   3u,          57u },  /* [FT_RCECStoCIOM_L5_a45c9d29_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_1_33137188] */
  { /*   152 */                   3u,          55u },  /* [FT_FSP_1_2_Req_L5_bc641e1e_Tx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_1_33137188] */
  { /*   153 */                   2u,          59u },  /* [FT_FSP1_Frame_L5_1663ef44_Rx]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_1_33137188] */
  { /*   154 */                   2u,          53u },  /* [FT_CIOMtoSlaves_L5_8632ed67_Tx]       */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_2_aa1a2032] */
  { /*   155 */                   2u,          54u },  /* [FT_Backlight_FuncInd_L5_89c53287_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_2_aa1a2032] */
  { /*   156 */                   3u,          57u },  /* [FT_RCECStoCIOM_L5_a45c9d29_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_2_aa1a2032] */
  { /*   157 */                   3u,          57u },  /* [FT_RCECStoCIOM_L5_a45c9d29_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_3_dd1d10a4] */
  { /*   158 */                   2u,          60u },  /* [FT_RCECStoCIOM_FR2_L5_73347414_Rx]    */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_3_dd1d10a4] */
  { /*   159 */                   2u,          53u },  /* [FT_CIOMtoSlaves_L5_8632ed67_Tx]       */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_3_dd1d10a4] */
  { /*   160 */                   3u,          57u },  /* [FT_RCECStoCIOM_L5_a45c9d29_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_3_dd1d10a4] */
  { /*   161 */                   2u,          56u },  /* [FT_CIOMtoSlaves_FR2_L5_72103487_Tx]   */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_3_dd1d10a4] */
  { /*   162 */                   2u,          54u },  /* [FT_Backlight_FuncInd_L5_89c53287_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_3_dd1d10a4] */
  { /*   163 */                   4u,          58u },  /* [FT_FSP1_SwitchDetResp_L5_00bda195_Rx] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_e_5f15e465] */
  { /*   164 */                   2u,          53u },  /* [FT_CIOMtoSlaves_L5_8632ed67_Tx]       */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_e_5f15e465] */
  { /*   165 */                   3u,          57u },  /* [FT_RCECStoCIOM_L5_a45c9d29_Rx]        */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/Table_e_5f15e465] */
  { /*   166 */                   4u,          65u },  /* [FT_MasterReq_0bd14ad2_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/MasterReq_SlaveResp_ce519ef0] */
  { /*   167 */                   4u,          66u },  /* [FT_SlaveResp_a3d63366_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/MasterReq_SlaveResp_ce519ef0] */
  { /*   168 */                   4u,          65u },  /* [FT_MasterReq_0bd14ad2_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/MasterReq_SlaveResp_Table0_45fec21a] */
  { /*   169 */                   4u,          66u },  /* [FT_SlaveResp_a3d63366_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/MasterReq_SlaveResp_Table0_45fec21a] */
  { /*   170 */                   2u,          63u },  /* [FT_Backlight_FuncInd_L6_2cac8035_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/MasterReq_SlaveResp_Table0_45fec21a] */
  { /*   171 */                   2u,          64u },  /* [FT_LinSlave_Frame_L6_c5a71f34_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/MasterReq_SlaveResp_Table0_45fec21a] */
  { /*   172 */                   2u,          63u },  /* [FT_Backlight_FuncInd_L6_2cac8035_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/Table0_1f9db836] */
  { /*   173 */                   2u,          64u },  /* [FT_LinSlave_Frame_L6_c5a71f34_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/Table0_1f9db836] */
  { /*   174 */                   4u,          69u },  /* [FT_MasterReq_a887cc7b_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/MasterReq_SlaveResp_3c067308] */
  { /*   175 */                   4u,          70u },  /* [FT_SlaveResp_0080b5cf_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/MasterReq_SlaveResp_3c067308] */
  { /*   176 */                   4u,          69u },  /* [FT_MasterReq_a887cc7b_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/MasterReq_SlaveResp_Table0_1ee9730f] */
  { /*   177 */                   4u,          70u },  /* [FT_SlaveResp_0080b5cf_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/MasterReq_SlaveResp_Table0_1ee9730f] */
  { /*   178 */                   2u,          67u },  /* [FT_Backlight_FuncInd_L7_1f0a95bb_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/MasterReq_SlaveResp_Table0_1ee9730f] */
  { /*   179 */                   2u,          68u },  /* [FT_LinSlave_Frame_L7_01340261_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/MasterReq_SlaveResp_Table0_1ee9730f] */
  { /*   180 */                   2u,          67u },  /* [FT_Backlight_FuncInd_L7_1f0a95bb_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/Table0_a257d4f8] */
  { /*   181 */                   2u,          68u },  /* [FT_LinSlave_Frame_L7_01340261_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/Table0_a257d4f8] */
  { /*   182 */                   4u,          73u },  /* [FT_MasterReq_7f654c23_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/MasterReq_SlaveResp_db1bd59f] */
  { /*   183 */                   4u,          74u },  /* [FT_SlaveResp_d7623597_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/MasterReq_SlaveResp_db1bd59f] */
  { /*   184 */                   4u,          73u },  /* [FT_MasterReq_7f654c23_Tx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/MasterReq_SlaveResp_Table0_281be3fc] */
  { /*   185 */                   4u,          74u },  /* [FT_SlaveResp_d7623597_Rx]             */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/MasterReq_SlaveResp_Table0_281be3fc] */
  { /*   186 */                   2u,          71u },  /* [FT_Backlight_FuncInd_L8_b3d56b22_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/MasterReq_SlaveResp_Table0_281be3fc] */
  { /*   187 */                   2u,          72u },  /* [FT_LinSlave_Frame_L8_ff0704b1_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/MasterReq_SlaveResp_Table0_281be3fc] */
  { /*   188 */                   2u,          71u },  /* [FT_Backlight_FuncInd_L8_b3d56b22_Tx]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/Table0_7fc10d7d] */
  { /*   189 */                   2u,          72u }   /* [FT_LinSlave_Frame_L8_ff0704b1_Rx]     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/Table0_7fc10d7d] */
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_FrameLengthDelayList
**********************************************************************************************************************/
/** 
  \var    LinIf_FrameLengthDelayList
  \details
  Element             Description
  FrameLengthDelay
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_FrameLengthDelayListType, LINIF_CONST) LinIf_FrameLengthDelayList[64] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    FrameLengthDelay        Comment                     Referable Keys */
  { /*     0 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     1 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     2 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     3 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     4 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     5 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     6 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     7 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     8 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*     9 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    10 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    11 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    12 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    13 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    14 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    15 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    16 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    17 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    18 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    19 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    20 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    21 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    22 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    23 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    24 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    25 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    26 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    27 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    28 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    29 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    30 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    31 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    32 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    33 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    34 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    35 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    36 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    37 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    38 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    39 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    40 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    41 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    42 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    43 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    44 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    45 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    46 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    47 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    48 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    49 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
    /* Index    FrameLengthDelay        Comment                     Referable Keys */
  { /*    50 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    51 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    52 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    53 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    54 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    55 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    56 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    57 */               2u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    58 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    59 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    60 */               3u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    61 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    62 */               4u },  /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    63 */               4u }   /* [Bitrate: 9600] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_FrameList
**********************************************************************************************************************/
/** 
  \var    LinIf_FrameList
  \details
  Element         Description
  PduId           Upper layer PduId
  ChecksumType    CLASSIC ENHANCED
  Direction       TX RX S2S
  FrameType       UNCONDITIONAL : 0 MRF : 1 SRF : 2 EVENT_TRIGGERED : 4 SPORADIC : 5 ASSIGN : 6 UNASSIGN : 7 ASSIGN_NAD : 8 FREE : 9 CONDITIONAL : 10 ASSIGN_FRAME_ID_RANGE : 11 SAVE_CONFIGURATION : 12
  Length          Frame length in bytes
  Pid             Protected Identifier
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_FrameListType, LINIF_CONST) LinIf_FrameList[75] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    PduId                                                         ChecksumType     Direction            FrameType  Length  Pid          Referable Keys */
  { /*     0 */ PduRConf_PduRDestPdu_CIOMtoSlaves1_L1_oLIN00_09e24f2a_Tx    , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     2u, 0x78u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_CIOMtoSlaves1_L1_417ce0d9_Tx] */
  { /*     1 */ PduRConf_PduRDestPdu_CIOMtoSlaves2_FR2_L1_oLIN00_a56bd28e_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     8u, 0xBAu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_CIOMtoSlaves2_FR2_L1_90fdcedf_Tx] */
  { /*     2 */ PduRConf_PduRDestPdu_CIOMtoSlaves2_FR1_L1_oLIN00_573c3f76_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     5u, 0x39u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_CIOMtoSlaves2_FR1_L1_82486131_Tx] */
  { /*     3 */ PduRConf_PduRDestPdu_Backlight_FuncInd_L1_oLIN00_841aab0e_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     1u, 0xB4u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_Backlight_FuncInd_L1_7f2b7abe_Tx] */
  { /*     4 */ PduRConf_PduRDestPdu_FSP_1_2_Req_L1_oLIN00_bfd0179c_Tx      , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     4u, 0xF5u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_FSP_1_2_Req_L1_d20f4458_Tx] */
  { /*     5 */ PduRConf_PduRDestPdu_CIOMtoSlaves2_FR3_L1_oLIN00_42767419_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     5u, 0xFBu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_CIOMtoSlaves2_FR3_L1_2841a9ba_Tx] */
  { /*     6 */ PduRConf_PduRSrcPdu_PduRSrcPdu_786aa0cd                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     1u, 0x2Bu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_LECM2toCIOM_FR2_L1_4d04a22e_Rx] */
  { /*     7 */ PduRConf_PduRSrcPdu_PduRSrcPdu_3e9ce6c9                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x03u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_FSP2_Frame_L1_fa6531cd_Rx] */
  { /*     8 */ PduRConf_PduRSrcPdu_PduRSrcPdu_e89970ce                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x42u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_FSP1_Frame_L1_11528ace_Rx] */
  { /*     9 */ PduRConf_PduRSrcPdu_PduRSrcPdu_73b12bc9                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     5u, 0xECu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_LECM2toCIOM_FR3_L1_f5b8c54b_Rx] */
  { /*    10 */ PduRConf_PduRSrcPdu_PduRSrcPdu_807c58f3                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0xB1u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_FSP2_SwitchDetResp_L1_d6c91baa_Rx] */
  { /*    11 */ PduRConf_PduRSrcPdu_PduRSrcPdu_7615762b                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0xF0u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_FSP1_SwitchDetResp_L1_3c4fc6c8_Rx] */
  { /*    12 */ PduRConf_PduRSrcPdu_PduRSrcPdu_c4212af7                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     3u, 0x8Bu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_ILCP1toCIOM_L1_c9f6c6bc_Rx] */
  { /*    13 */ PduRConf_PduRSrcPdu_PduRSrcPdu_7332fc25                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     3u, 0x5Eu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_LECMBasic2CIOM_L1_c4710275_Rx] */
  { /*    14 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8cd02125                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0xDDu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_LECM2toCIOM_FR1_L1_5fb10dc0_Rx] */
  { /*    15 */ 0                                                           , LIN_CLASSIC_CS , LIN_MASTER_RESPONSE,        1u,     8u, 0x3Cu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_MasterReq_355bc768_Tx] */
  { /*    16 */ 0                                                           , LIN_CLASSIC_CS , LIN_SLAVE_RESPONSE ,        2u,     8u, 0x7Du },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847/FT_SlaveResp_9d5cbedc_Rx] */
  { /*    17 */ PduRConf_PduRDestPdu_Backlight_FuncInd_L2_oLIN01_2b903334_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     1u, 0xB4u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_Backlight_FuncInd_L2_da42c80c_Tx] */
  { /*    18 */ PduRConf_PduRDestPdu_FSP_3_4_Req_L2_oLIN01_a1ddc883_Tx      , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     4u, 0x76u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP_3_4_Req_L2_08b9a97f_Tx] */
  { /*    19 */ PduRConf_PduRDestPdu_FSP_1_2_Req_L2_oLIN01_105a8fa6_Tx      , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     4u, 0xF5u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP_1_2_Req_L2_ca2370c5_Tx] */
  { /*    20 */ PduRConf_PduRSrcPdu_PduRSrcPdu_9649cb45                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0xC4u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP3_Frame_L2_17dde19d_Rx] */
  { /*    21 */ PduRConf_PduRSrcPdu_PduRSrcPdu_21916aa6                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0x73u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP4_SwitchDetResp_L2_4f5b1e44_Rx] */
  { /*    22 */ PduRConf_PduRSrcPdu_PduRSrcPdu_d86f19ae                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0xF0u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP1_SwitchDetResp_L2_aba17fa3_Rx] */
  { /*    23 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1d7b2b95                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0x32u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP3_SwitchDetResp_L2_ae751420_Rx] */
  { /*    24 */ PduRConf_PduRSrcPdu_PduRSrcPdu_7877d56d                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0xB1u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP2_SwitchDetResp_L2_4127a2c1_Rx] */
  { /*    25 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1d5f459f                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x03u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP2_Frame_L2_f81f8aa3_Rx] */
  { /*    26 */ PduRConf_PduRSrcPdu_PduRSrcPdu_be569886                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x85u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP4_Frame_L2_f501fae4_Rx] */
  { /*    27 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8621b565                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x42u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_FSP1_Frame_L2_132831a0_Rx] */
  { /*    28 */ 0                                                           , LIN_CLASSIC_CS , LIN_MASTER_RESPONSE,        1u,     8u, 0x3Cu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_MasterReq_e2b94730_Tx] */
  { /*    29 */ 0                                                           , LIN_CLASSIC_CS , LIN_SLAVE_RESPONSE ,        2u,     8u, 0x7Du },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2/FT_SlaveResp_4abe3e84_Rx] */
  { /*    30 */ PduRConf_PduRDestPdu_MastertoTCP_oLIN02_7c7ed3d1_Tx         , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     2u, 0x2Eu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_MastertoTCP_a2bc639c_Tx] */
  { /*    31 */ PduRConf_PduRDestPdu_FSP_1_2_Req_L3_oLIN02_3be8803a_Tx      , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     4u, 0xF5u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_FSP_1_2_Req_L3_e53ae97b_Tx] */
  { /*    32 */ PduRConf_PduRDestPdu_Backlight_FuncInd_L3_oLIN02_00223ca8_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     1u, 0xB4u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_Backlight_FuncInd_L3_e9e4dd82_Tx] */
  { /*    33 */ PduRConf_PduRSrcPdu_PduRSrcPdu_e2611c34                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x03u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_FSP2_Frame_L3_f9fd8308_Rx] */
  { /*    34 */ PduRConf_PduRSrcPdu_PduRSrcPdu_25794016                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0xB1u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_FSP2_SwitchDetResp_L3_2508ab24_Rx] */
  { /*    35 */ PduRConf_PduRSrcPdu_PduRSrcPdu_7bebd14f                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0xF0u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_FSP1_SwitchDetResp_L3_cf8e7646_Rx] */
  { /*    36 */ PduRConf_PduRSrcPdu_PduRSrcPdu_93b09a1a                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     5u, 0xE7u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_TCPtoMaster_4ab36a9a_Rx] */
  { /*    37 */ PduRConf_PduRSrcPdu_PduRSrcPdu_65a06375                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x42u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_FSP1_Frame_L3_12ca380b_Rx] */
  { /*    38 */ 0                                                           , LIN_CLASSIC_CS , LIN_MASTER_RESPONSE,        1u,     8u, 0x3Cu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_MasterReq_41efc199_Tx] */
  { /*    39 */ 0                                                           , LIN_CLASSIC_CS , LIN_SLAVE_RESPONSE ,        2u,     8u, 0x7Du },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c/FT_SlaveResp_e9e8b82d_Rx] */
  { /*    40 */ PduRConf_PduRDestPdu_Backlight_FuncInd_L4_oLIN03_aff40501_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     1u, 0xB4u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_Backlight_FuncInd_L4_4be0ab29_Tx] */
  { /*    41 */ PduRConf_PduRDestPdu_CIOMtoSlaves1_L4_oLIN03_220ce125_Tx    , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     2u, 0x78u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_CIOMtoSlaves1_L4_c341f9ae_Tx] */
  { /*    42 */ PduRConf_PduRDestPdu_CIOMtoSlaves2_L4_oLIN03_d05b0cdd_Tx    , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     2u, 0xBAu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_CIOMtoSlaves2_L4_d1f45640_Tx] */
  { /*    43 */ PduRConf_PduRDestPdu_FSP_1_2_Req_L4_oLIN03_943eb993_Tx      , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     4u, 0xF5u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_FSP_1_2_Req_L4_fa7b19ff_Tx] */
  { /*    44 */ PduRConf_PduRSrcPdu_PduRSrcPdu_d430c522                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0xF0u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_FSP1_SwitchDetResp_L4_5f0d0b34_Rx] */
  { /*    45 */ PduRConf_PduRSrcPdu_PduRSrcPdu_7272736c                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x42u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_FSP1_Frame_L4_17dd477c_Rx] */
  { /*    46 */ PduRConf_PduRSrcPdu_PduRSrcPdu_97ced8ff                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     3u, 0xA3u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_ELCP2toCIOM_L4_2040b135_Rx] */
  { /*    47 */ PduRConf_PduRSrcPdu_PduRSrcPdu_8f05a91a                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     3u, 0x20u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_ILCP2toCIOM_L4_0ab52018_Rx] */
  { /*    48 */ PduRConf_PduRSrcPdu_PduRSrcPdu_c27f66fb                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     3u, 0xD3u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_CCFWtoCIOM_L4_41a1d427_Rx] */
  { /*    49 */ PduRConf_PduRSrcPdu_PduRSrcPdu_a0a38a40                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     3u, 0x92u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_DLFWtoCIOM_L4_4e464d0e_Rx] */
    /* Index    PduId                                                         ChecksumType     Direction            FrameType  Length  Pid          Referable Keys */
  { /*    50 */ PduRConf_PduRSrcPdu_PduRSrcPdu_b63a736f                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     4u, 0x8Eu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_ELCP1toCIOM_L4_cb770a36_Rx] */
  { /*    51 */ 0                                                           , LIN_CLASSIC_CS , LIN_MASTER_RESPONSE,        1u,     8u, 0x3Cu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_MasterReq_960d41c1_Tx] */
  { /*    52 */ 0                                                           , LIN_CLASSIC_CS , LIN_SLAVE_RESPONSE ,        2u,     8u, 0x7Du },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9/FT_SlaveResp_3e0a3875_Rx] */
  { /*    53 */ PduRConf_PduRDestPdu_CIOMtoSlaves_L5_oLIN04_251caba0_Tx     , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     2u, 0x78u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_CIOMtoSlaves_L5_8632ed67_Tx] */
  { /*    54 */ PduRConf_PduRDestPdu_Backlight_FuncInd_L5_oLIN04_571a8203_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     1u, 0xB4u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_Backlight_FuncInd_L5_89c53287_Tx] */
  { /*    55 */ PduRConf_PduRDestPdu_FSP_1_2_Req_L5_oLIN04_6cd03e91_Tx      , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     4u, 0xF5u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_FSP_1_2_Req_L5_bc641e1e_Tx] */
  { /*    56 */ PduRConf_PduRDestPdu_CIOMtoSlaves_FR2_L5_oLIN04_7c0e76b4_Tx , LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     2u, 0x6Fu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_CIOMtoSlaves_FR2_L5_72103487_Tx] */
  { /*    57 */ PduRConf_PduRSrcPdu_PduRSrcPdu_5d6ab1b6                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     3u, 0x50u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_RCECStoCIOM_L5_a45c9d29_Rx] */
  { /*    58 */ PduRConf_PduRSrcPdu_PduRSrcPdu_c187c3bc                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     8u, 0xF0u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_FSP1_SwitchDetResp_L5_00bda195_Rx] */
  { /*    59 */ PduRConf_PduRSrcPdu_PduRSrcPdu_1d41a162                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x42u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_FSP1_Frame_L5_1663ef44_Rx] */
  { /*    60 */ PduRConf_PduRSrcPdu_PduRSrcPdu_256e501a                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     2u, 0x4Cu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_RCECStoCIOM_FR2_L5_73347414_Rx] */
  { /*    61 */ 0                                                           , LIN_CLASSIC_CS , LIN_MASTER_RESPONSE,        1u,     8u, 0x3Cu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_MasterReq_dc33ca8a_Tx] */
  { /*    62 */ 0                                                           , LIN_CLASSIC_CS , LIN_SLAVE_RESPONSE ,        2u,     8u, 0x7Du },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51/FT_SlaveResp_7434b33e_Rx] */
  { /*    63 */ PduRConf_PduRDestPdu_Backlight_FuncInd_L6_oLIN05_f8901a39_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     1u, 0xB4u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/FT_Backlight_FuncInd_L6_2cac8035_Tx] */
  { /*    64 */ PduRConf_PduRSrcPdu_PduRSrcPdu_562027d3                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     1u, 0x42u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/FT_LinSlave_Frame_L6_c5a71f34_Rx] */
  { /*    65 */ 0                                                           , LIN_CLASSIC_CS , LIN_MASTER_RESPONSE,        1u,     8u, 0x3Cu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/FT_MasterReq_0bd14ad2_Tx] */
  { /*    66 */ 0                                                           , LIN_CLASSIC_CS , LIN_SLAVE_RESPONSE ,        2u,     8u, 0x7Du },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4/FT_SlaveResp_a3d63366_Rx] */
  { /*    67 */ PduRConf_PduRDestPdu_Backlight_FuncInd_L7_oLIN06_d32215a5_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     1u, 0xB4u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/FT_Backlight_FuncInd_L7_1f0a95bb_Tx] */
  { /*    68 */ PduRConf_PduRSrcPdu_PduRSrcPdu_10738768                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     1u, 0x42u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/FT_LinSlave_Frame_L7_01340261_Rx] */
  { /*    69 */ 0                                                           , LIN_CLASSIC_CS , LIN_MASTER_RESPONSE,        1u,     8u, 0x3Cu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/FT_MasterReq_a887cc7b_Tx] */
  { /*    70 */ 0                                                           , LIN_CLASSIC_CS , LIN_SLAVE_RESPONSE ,        2u,     8u, 0x7Du },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a/FT_SlaveResp_0080b5cf_Rx] */
  { /*    71 */ PduRConf_PduRDestPdu_Backlight_FuncInd_L8_oLIN07_7c4d6f2a_Tx, LIN_ENHANCED_CS, LIN_MASTER_RESPONSE,        0u,     1u, 0xB4u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/FT_Backlight_FuncInd_L8_b3d56b22_Tx] */
  { /*    72 */ PduRConf_PduRSrcPdu_PduRSrcPdu_b4f1c06a                     , LIN_ENHANCED_CS, LIN_SLAVE_RESPONSE ,        0u,     1u, 0x42u },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/FT_LinSlave_Frame_L8_ff0704b1_Rx] */
  { /*    73 */ 0                                                           , LIN_CLASSIC_CS , LIN_MASTER_RESPONSE,        1u,     8u, 0x3Cu },  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/FT_MasterReq_7f654c23_Tx] */
  { /*    74 */ 0                                                           , LIN_CLASSIC_CS , LIN_SLAVE_RESPONSE ,        2u,     8u, 0x7Du }   /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff, /ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff/FT_SlaveResp_d7623597_Rx] */
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_LinIfToLinTrcvChannel
**********************************************************************************************************************/
/** 
  \var    LinIf_LinIfToLinTrcvChannel
  \details
  Element        Description
  TrcvChannel
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_LinIfToLinTrcvChannelType, LINIF_CONST) LinIf_LinIfToLinTrcvChannel[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TrcvChannel                                                                                       */
  { /*     0 */ 0 /* LinIfConf_LinIfChannel_CHNL_45618847 -> LinTrcvConf_LinTrcvChannel_LinTrcvChannel_LIN1_EN */ },
  { /*     1 */ 1 /* LinIfConf_LinIfChannel_CHNL_8e3d5be2 -> LinTrcvConf_LinTrcvChannel_LinTrcvChannel_LIN2_EN */ },
  { /*     2 */ 2 /* LinIfConf_LinIfChannel_CHNL_08a9294c -> LinTrcvConf_LinTrcvChannel_LinTrcvChannel_LIN3_EN */ },
  { /*     3 */ 3 /* LinIfConf_LinIfChannel_CHNL_c3f5fae9 -> LinTrcvConf_LinTrcvChannel_LinTrcvChannel_LIN4_EN */ },
  { /*     4 */ 4 /* LinIfConf_LinIfChannel_CHNL_def0ca51 -> LinTrcvConf_LinTrcvChannel_LinTrcvChannel_LIN5_EN */ },
  { /*     5 */                   255 /* LinIfConf_LinIfChannel_CHNL_15ac19f4 not used by transceiver channel. */ },
  { /*     6 */                   255 /* LinIfConf_LinIfChannel_CHNL_93386b5a not used by transceiver channel. */ },
  { /*     7 */                   255 /* LinIfConf_LinIfChannel_CHNL_5864b8ff not used by transceiver channel. */ }
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_LinIfToLinTrcvNr
**********************************************************************************************************************/
/** 
  \var    LinIf_LinIfToLinTrcvNr
  \details
  Element    Description
  TrcvNr 
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_LinIfToLinTrcvNrType, LINIF_CONST) LinIf_LinIfToLinTrcvNr[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TrcvNr                                                                 */
  { /*     0 */     0 /* LinIfConf_LinIfChannel_CHNL_45618847 -> LinTrcv_30_Generic */ },
  { /*     1 */     0 /* LinIfConf_LinIfChannel_CHNL_8e3d5be2 -> LinTrcv_30_Generic */ },
  { /*     2 */     0 /* LinIfConf_LinIfChannel_CHNL_08a9294c -> LinTrcv_30_Generic */ },
  { /*     3 */     0 /* LinIfConf_LinIfChannel_CHNL_c3f5fae9 -> LinTrcv_30_Generic */ },
  { /*     4 */     0 /* LinIfConf_LinIfChannel_CHNL_def0ca51 -> LinTrcv_30_Generic */ },
  { /*     5 */ 255 /* LinIfConf_LinIfChannel_CHNL_15ac19f4 not used by transceiver */ },
  { /*     6 */ 255 /* LinIfConf_LinIfChannel_CHNL_93386b5a not used by transceiver */ },
  { /*     7 */ 255 /* LinIfConf_LinIfChannel_CHNL_5864b8ff not used by transceiver */ }
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_LinTrcv_CheckWakeupFct
**********************************************************************************************************************/
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_LinTrcv_CheckWakeupFctType, LINIF_CONST) LinIf_LinTrcv_CheckWakeupFct[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     LinTrcv_CheckWakeupFct          */
  /*     0 */ LinTrcv_30_Generic_CheckWakeup 
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_LinTrcv_GetBusWuReasonFct
**********************************************************************************************************************/
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_LinTrcv_GetBusWuReasonFctType, LINIF_CONST) LinIf_LinTrcv_GetBusWuReasonFct[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     LinTrcv_GetBusWuReasonFct          */
  /*     0 */ LinTrcv_30_Generic_GetBusWuReason 
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_LinTrcv_GetOpModeFct
**********************************************************************************************************************/
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_LinTrcv_GetOpModeFctType, LINIF_CONST) LinIf_LinTrcv_GetOpModeFct[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     LinTrcv_GetOpModeFct          */
  /*     0 */ LinTrcv_30_Generic_GetOpMode 
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_LinTrcv_SetOpModeFct
**********************************************************************************************************************/
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_LinTrcv_SetOpModeFctType, LINIF_CONST) LinIf_LinTrcv_SetOpModeFct[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     LinTrcv_SetOpModeFct          */
  /*     0 */ LinTrcv_30_Generic_SetOpMode 
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_LinTrcv_SetWakeupModeFct
**********************************************************************************************************************/
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_LinTrcv_SetWakeupModeFctType, LINIF_CONST) LinIf_LinTrcv_SetWakeupModeFct[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     LinTrcv_SetWakeupModeFct          */
  /*     0 */ LinTrcv_30_Generic_SetWakeupMode 
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_ScheduleTableList
**********************************************************************************************************************/
/** 
  \var    LinIf_ScheduleTableList
  \details
  Element                    Description
  EntryEndIdx                the end index of the 0:n relation pointing to LinIf_Entry
  EntryStartIdx              the start index of the 0:n relation pointing to LinIf_Entry
  NumberOfScheduleEntries    number of entries
  ResumePosition             CONTINUE_AT_IT_POINT : 0 START_FROM_BEGINNING : 1
  RunMode                    RUN_CONTINUOUS : 0 RUN_ONCE : 1
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_ScheduleTableListType, LINIF_CONST) LinIf_ScheduleTableList[47] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    EntryEndIdx                              EntryStartIdx                              NumberOfScheduleEntries  ResumePosition  RunMode        Comment                                            Referable Keys */
  { /*     0 */ LINIF_NO_ENTRYENDIDXOFSCHEDULETABLELIST, LINIF_NO_ENTRYSTARTIDXOFSCHEDULETABLELIST,                   0x00u,          0x00u,   0x00u },  /* [NULLSchedule_CHNL_45618847]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     1 */                                      2u,                                        0u,                   0x02u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_03d8aeb9]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     2 */                                     10u,                                        2u,                   0x08u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table_1_2a158a89] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     3 */                                     19u,                                       10u,                   0x09u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table_2_b31cdb33] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     4 */                                     25u,                                       19u,                   0x06u,          0x01u,   0x00u },  /* [Table_1_2abffe93]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     5 */                                     44u,                                       25u,                   0x13u,          0x01u,   0x00u },  /* [Table_2_b3b6af29]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     6 */                                     48u,                                       44u,                   0x04u,          0x01u,   0x00u },  /* [Table_e_46b96b7e]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  { /*     7 */ LINIF_NO_ENTRYENDIDXOFSCHEDULETABLELIST, LINIF_NO_ENTRYSTARTIDXOFSCHEDULETABLELIST,                   0x00u,          0x00u,   0x00u },  /* [NULLSchedule_CHNL_8e3d5be2]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*     8 */                                     50u,                                       48u,                   0x02u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_e4c5082e]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*     9 */                                     59u,                                       50u,                   0x09u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table0_9e3481d6]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    10 */                                     66u,                                       59u,                   0x07u,          0x01u,   0x00u },  /* [Table0_df24d2a0]                      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    11 */                                     70u,                                       66u,                   0x04u,          0x01u,   0x00u },  /* [Table_e_dbb68a08]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  { /*    12 */ LINIF_NO_ENTRYENDIDXOFSCHEDULETABLELIST, LINIF_NO_ENTRYSTARTIDXOFSCHEDULETABLELIST,                   0x00u,          0x00u,   0x00u },  /* [NULLSchedule_CHNL_08a9294c]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    13 */                                     72u,                                       70u,                   0x02u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_1692e5d6]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    14 */                                     80u,                                       72u,                   0x08u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table_1_631128e5] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    15 */                                     85u,                                       80u,                   0x05u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table_2_fa18795f] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    16 */                                     91u,                                       85u,                   0x06u,          0x01u,   0x00u },  /* [Table_1_cbd13a3e]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    17 */                                     94u,                                       91u,                   0x03u,          0x01u,   0x00u },  /* [Table_2_52d86b84]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    18 */                                     96u,                                       94u,                   0x02u,          0x01u,   0x00u },  /* [Table_e_a7d7afd3]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  { /*    19 */ LINIF_NO_ENTRYENDIDXOFSCHEDULETABLELIST, LINIF_NO_ENTRYSTARTIDXOFSCHEDULETABLELIST,                   0x00u,          0x00u,   0x00u },  /* [NULLSchedule_CHNL_c3f5fae9]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    20 */                                     98u,                                       96u,                   0x02u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_f18f4341]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    21 */                                    105u,                                       98u,                   0x07u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table_1_479379d3] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    22 */                                    113u,                                      105u,                   0x08u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table_2_de9a2869] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    23 */                                    118u,                                      113u,                   0x05u,          0x01u,   0x00u },  /* [Table_1_56dedb48]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    24 */                                    124u,                                      118u,                   0x06u,          0x01u,   0x00u },  /* [Table_2_cfd78af2]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    25 */                                    127u,                                      124u,                   0x03u,          0x01u,   0x00u },  /* [Table_e_3ad84ea5]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  { /*    26 */ LINIF_NO_ENTRYENDIDXOFSCHEDULETABLELIST, LINIF_NO_ENTRYSTARTIDXOFSCHEDULETABLELIST,                   0x00u,          0x00u,   0x00u },  /* [NULLSchedule]                         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    27 */                                    129u,                                      127u,                   0x02u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_294c3867]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    28 */                                    136u,                                      129u,                   0x07u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table_1_b81cce51] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    29 */                                    141u,                                      136u,                   0x05u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table_2_21159feb] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    30 */                                    148u,                                      141u,                   0x07u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table_3_5612af7d] */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    31 */                                    154u,                                      148u,                   0x06u,          0x01u,   0x00u },  /* [Table_1_33137188]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    32 */                                    157u,                                      154u,                   0x03u,          0x01u,   0x00u },  /* [Table_2_aa1a2032]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    33 */                                    163u,                                      157u,                   0x06u,          0x01u,   0x00u },  /* [Table_3_dd1d10a4]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    34 */                                    166u,                                      163u,                   0x03u,          0x01u,   0x00u },  /* [Table_e_5f15e465]                     */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  { /*    35 */ LINIF_NO_ENTRYENDIDXOFSCHEDULETABLELIST, LINIF_NO_ENTRYSTARTIDXOFSCHEDULETABLELIST,                   0x00u,          0x00u,   0x00u },  /* [NULLSchedule_CHNL_15ac19f4]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    36 */                                    168u,                                      166u,                   0x02u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_ce519ef0]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    37 */                                    172u,                                      168u,                   0x04u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table0_45fec21a]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    38 */                                    174u,                                      172u,                   0x02u,          0x01u,   0x00u },  /* [Table0_1f9db836]                      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  { /*    39 */ LINIF_NO_ENTRYENDIDXOFSCHEDULETABLELIST, LINIF_NO_ENTRYSTARTIDXOFSCHEDULETABLELIST,                   0x00u,          0x00u,   0x00u },  /* [NULLSchedule_CHNL_93386b5a]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    40 */                                    176u,                                      174u,                   0x02u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_3c067308]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    41 */                                    180u,                                      176u,                   0x04u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table0_1ee9730f]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    42 */                                    182u,                                      180u,                   0x02u,          0x01u,   0x00u },  /* [Table0_a257d4f8]                      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  { /*    43 */ LINIF_NO_ENTRYENDIDXOFSCHEDULETABLELIST, LINIF_NO_ENTRYSTARTIDXOFSCHEDULETABLELIST,                   0x00u,          0x00u,   0x00u },  /* [NULLSchedule_CHNL_5864b8ff]           */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    44 */                                    184u,                                      182u,                   0x02u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_db1bd59f]         */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    45 */                                    188u,                                      184u,                   0x04u,          0x01u,   0x00u },  /* [MasterReq_SlaveResp_Table0_281be3fc]  */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  { /*    46 */                                    190u,                                      188u,                   0x02u,          0x01u,   0x00u }   /* [Table0_7fc10d7d]                      */  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_ScheduleTableListInd
**********************************************************************************************************************/
/** 
  \var    LinIf_ScheduleTableListInd
  \brief  the indexes of the 1:1 sorted relation pointing to LinIf_ScheduleTableList
*/ 
#define LINIF_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_ScheduleTableListIndType, LINIF_CONST) LinIf_ScheduleTableListInd[47] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     ScheduleTableListInd      Referable Keys */
  /*     0 */                    0u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  /*     1 */                    1u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  /*     2 */                    2u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  /*     3 */                    3u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  /*     4 */                    4u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  /*     5 */                    5u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  /*     6 */                    6u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_45618847] */
  /*     7 */                    7u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  /*     8 */                    8u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  /*     9 */                    9u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  /*    10 */                   10u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  /*    11 */                   11u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_8e3d5be2] */
  /*    12 */                   12u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  /*    13 */                   13u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  /*    14 */                   14u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  /*    15 */                   15u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  /*    16 */                   16u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  /*    17 */                   17u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  /*    18 */                   18u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_08a9294c] */
  /*    19 */                   19u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  /*    20 */                   20u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  /*    21 */                   21u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  /*    22 */                   22u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  /*    23 */                   23u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  /*    24 */                   24u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  /*    25 */                   25u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_c3f5fae9] */
  /*    26 */                   26u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  /*    27 */                   27u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  /*    28 */                   28u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  /*    29 */                   29u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  /*    30 */                   30u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  /*    31 */                   31u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  /*    32 */                   32u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  /*    33 */                   33u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  /*    34 */                   34u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_def0ca51] */
  /*    35 */                   35u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  /*    36 */                   36u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  /*    37 */                   37u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  /*    38 */                   38u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_15ac19f4] */
  /*    39 */                   39u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  /*    40 */                   40u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  /*    41 */                   41u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  /*    42 */                   42u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_93386b5a] */
  /*    43 */                   43u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  /*    44 */                   44u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  /*    45 */                   45u,  /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
  /*    46 */                   46u   /* [/ActiveEcuC/LinIf/LinIfGlobalConfig/CHNL_5864b8ff] */
};
#define LINIF_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_SystemToLinIfChannel
**********************************************************************************************************************/
/** 
  \var    LinIf_SystemToLinIfChannel
  \details
  Element              Description
  LinIfChannelIndex
*/ 
#define LINIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(LinIf_SystemToLinIfChannelType, LINIF_CONST) LinIf_SystemToLinIfChannel[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    LinIfChannelIndex                                                                       */
  { /*     0 */                                                       255U /* 0xFF -> No Lin channel */ },
  { /*     1 */                                                       255U /* 0xFF -> No Lin channel */ },
  { /*     2 */                                                       255U /* 0xFF -> No Lin channel */ },
  { /*     3 */                                                       255U /* 0xFF -> No Lin channel */ },
  { /*     4 */                                                       255U /* 0xFF -> No Lin channel */ },
  { /*     5 */                                                       255U /* 0xFF -> No Lin channel */ },
  { /*     6 */ 0U /* ComMConf_ComMChannel_CN_LIN00_2cd9a7df -> LinIfConf_LinIfChannel_CHNL_45618847 */ },
  { /*     7 */ 1U /* ComMConf_ComMChannel_CN_LIN01_5bde9749 -> LinIfConf_LinIfChannel_CHNL_8e3d5be2 */ },
  { /*     8 */ 2U /* ComMConf_ComMChannel_CN_LIN02_c2d7c6f3 -> LinIfConf_LinIfChannel_CHNL_08a9294c */ },
  { /*     9 */ 3U /* ComMConf_ComMChannel_CN_LIN03_b5d0f665 -> LinIfConf_LinIfChannel_CHNL_c3f5fae9 */ },
  { /*    10 */ 4U /* ComMConf_ComMChannel_CN_LIN04_2bb463c6 -> LinIfConf_LinIfChannel_CHNL_def0ca51 */ },
  { /*    11 */ 5U /* ComMConf_ComMChannel_CN_LIN05_5cb35350 -> LinIfConf_LinIfChannel_CHNL_15ac19f4 */ },
  { /*    12 */ 6U /* ComMConf_ComMChannel_CN_LIN06_c5ba02ea -> LinIfConf_LinIfChannel_CHNL_93386b5a */ },
  { /*    13 */ 7U /* ComMConf_ComMChannel_CN_LIN07_b2bd327c -> LinIfConf_LinIfChannel_CHNL_5864b8ff */ }
};
#define LINIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  LinIf_ChannelData
**********************************************************************************************************************/
#define LINIF_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinIf_ChannelDataType, LINIF_VAR_NOINIT) LinIf_ChannelData[8];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */  /* Data structure per LinIf channel */
#define LINIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/






