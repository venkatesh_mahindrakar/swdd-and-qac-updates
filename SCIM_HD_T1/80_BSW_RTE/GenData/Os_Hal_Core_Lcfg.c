/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Hal_Core_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:32
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_HAL_CORE_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_Hal_Core_Lcfg.h"
#include "Os_Hal_Core.h"

/* Os kernel module dependencies */

/* Os hal dependencies */
#include "Os_Hal_Context_Lcfg.h"
#include "Os_Hal_Entry_Lcfg.h"
#include "Os_Hal_Interrupt_Lcfg.h"
#include "Os_Hal_Kernel_Lcfg.h"


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_VAR_FAST_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! HAL dynamic core to thread data: OsCore0 */
OS_LOCAL VAR(Os_Hal_Core2ThreadType, OS_VAR_NOINIT_FAST) OsCfg_Hal_Core2Thread_OsCore0_Dyn;

#define OS_STOP_SEC_CORE0_VAR_FAST_NOINIT_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

#define OS_START_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! HAL core initialized interrupt sources: OsCore0 */
CONSTP2CONST(Os_IsrHwConfigType, OS_CONST, OS_CONST)
  OsCfg_Hal_Core_OsCore0_InterruptSourceRefs[OS_CFG_NUM_CORE_OSCORE0_INTERRUPTSOURCEREFS + 1u] =
{
  /* No core exclusive interrupt sources to be initialized by OsCore0. */
  NULL_PTR
};

/*! HAL core configuration data: OsCore0 */
CONST(Os_Hal_CoreConfigType, OS_CONST) OsCfg_Hal_Core_OsCore0 =
{
  /* .CoreIdPhysical    = */ 0uL,
  /* .CoreIdLogical     = */ 0uL,
  /* .CoreStartAddress  = */ (uint32)(&_start), /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .CoreIntcCprOffset = */ OS_HAL_CORE0_INTC_CPR_OFFSET,
  /* .CoreGateLockValue = */ OS_HAL_CORE0_GATELOCK_VALUE,
  /* .CoreSpeSupported  = */ OS_HAL_CORE0_SPE_SUPPORTED
}
;

/*! HAL AUTOSAR core configuration data: OsCore0 */
CONST(Os_Hal_CoreAsrConfigType, OS_CONST) OsCfg_Hal_CoreAsr_OsCore0 =
{
  /* .Exception table address = */ (Os_Hal_ExceptionTableType)&_OS_EXCVEC_CORE0_CODE_START,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .Interrupt table address = */ (Os_Hal_VectorTableType)&_OS_INTVEC_CODE_START,  /* PRQA S 0306, 0324 */ /* MD_Os_Hal_Rule11.4_0306, MD_Os_Hal_Rule11.2_0324 */ /* COMP_WARN_OS_HAL_EXPR_NOT_ARITHMETIC_TYPE */ /* COMP_WARN_OS_CONV_PTR_TO_INT */
  /* .NumberOfWritableIVORs   = */ OS_HAL_CORE0_NUMBER_OF_WRITABLE_IVORS
}
;

#define OS_STOP_SEC_CORE0_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define OS_START_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! HAL core to thread configuration data. */
CONST(Os_Hal_Core2ThreadConfigType, OS_CONST) OsCfg_Hal_Core2Thread =
{
  /* .Core2Thread = */
  {
    &OsCfg_Hal_Core2Thread_OsCore0_Dyn, /* OS_CORE_ID_0 */
  }
};

/*! HAL system configuration data. */
CONST(Os_Hal_SystemConfigType, OS_CONST) OsCfg_Hal_System =
{
  /* .CoreInformation = */
  {
    &OsCfg_Hal_Core_OsCore0, /* OS_CORE_ID_0 */
    NULL_PTR, /* OS_CORE_ID_1 */
    NULL_PTR, /* OS_CORE_ID_2 */
  }
};

/*! Interrupt sources which are initialized by the hardware init core. */
CONSTP2CONST(Os_IsrHwConfigType, OS_CONST, OS_CONST)
  OsCfg_Hal_System_InterruptSourceRefs[OS_CFG_NUM_SYSTEM_INTERRUPTSOURCEREFS + 1u] =
{
  &OsCfg_Isr_CanIsr_0_MB00To03_HwConfig,
  &OsCfg_Isr_CanIsr_0_MB04To07_HwConfig,
  &OsCfg_Isr_CanIsr_0_MB08To11_HwConfig,
  &OsCfg_Isr_CanIsr_0_MB12To15_HwConfig,
  &OsCfg_Isr_CanIsr_0_MB16To31_HwConfig,
  &OsCfg_Isr_CanIsr_0_MB32To63_HwConfig,
  &OsCfg_Isr_CanIsr_0_MB64To95_HwConfig,
  &OsCfg_Isr_CanIsr_1_MB00To03_HwConfig,
  &OsCfg_Isr_CanIsr_1_MB04To07_HwConfig,
  &OsCfg_Isr_CanIsr_1_MB08To11_HwConfig,
  &OsCfg_Isr_CanIsr_1_MB12To15_HwConfig,
  &OsCfg_Isr_CanIsr_1_MB16To31_HwConfig,
  &OsCfg_Isr_CanIsr_1_MB32To63_HwConfig,
  &OsCfg_Isr_CanIsr_1_MB64To95_HwConfig,
  &OsCfg_Isr_CanIsr_2_MB00To03_HwConfig,
  &OsCfg_Isr_CanIsr_2_MB04To07_HwConfig,
  &OsCfg_Isr_CanIsr_2_MB08To11_HwConfig,
  &OsCfg_Isr_CanIsr_2_MB12To15_HwConfig,
  &OsCfg_Isr_CanIsr_2_MB16To31_HwConfig,
  &OsCfg_Isr_CanIsr_2_MB32To63_HwConfig,
  &OsCfg_Isr_CanIsr_2_MB64To95_HwConfig,
  &OsCfg_Isr_CanIsr_4_MB00To03_HwConfig,
  &OsCfg_Isr_CanIsr_4_MB04To07_HwConfig,
  &OsCfg_Isr_CanIsr_4_MB08To11_HwConfig,
  &OsCfg_Isr_CanIsr_4_MB12To15_HwConfig,
  &OsCfg_Isr_CanIsr_4_MB16To31_HwConfig,
  &OsCfg_Isr_CanIsr_4_MB32To63_HwConfig,
  &OsCfg_Isr_CanIsr_4_MB64To95_HwConfig,
  &OsCfg_Isr_CanIsr_6_MB00To03_HwConfig,
  &OsCfg_Isr_CanIsr_6_MB04To07_HwConfig,
  &OsCfg_Isr_CanIsr_6_MB08To11_HwConfig,
  &OsCfg_Isr_CanIsr_6_MB12To15_HwConfig,
  &OsCfg_Isr_CanIsr_6_MB16To31_HwConfig,
  &OsCfg_Isr_CanIsr_6_MB32To63_HwConfig,
  &OsCfg_Isr_CanIsr_6_MB64To95_HwConfig,
  &OsCfg_Isr_CanIsr_7_MB00To03_HwConfig,
  &OsCfg_Isr_CanIsr_7_MB04To07_HwConfig,
  &OsCfg_Isr_CanIsr_7_MB08To11_HwConfig,
  &OsCfg_Isr_CanIsr_7_MB12To15_HwConfig,
  &OsCfg_Isr_CanIsr_7_MB16To31_HwConfig,
  &OsCfg_Isr_CanIsr_7_MB32To63_HwConfig,
  &OsCfg_Isr_CanIsr_7_MB64To95_HwConfig,
  &OsCfg_Isr_CounterIsr_SystemTimer_HwConfig,
  &OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR_HwConfig,
  &OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR_HwConfig,
  &OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR_HwConfig,
  &OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR_HwConfig,
  &OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR_HwConfig,
  &OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR_HwConfig,
  &OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR_HwConfig,
  &OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR_HwConfig,
  &OsCfg_Isr_Lin_Channel_0_ERR_HwConfig,
  &OsCfg_Isr_Lin_Channel_0_RXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_0_TXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_10_ERR_HwConfig,
  &OsCfg_Isr_Lin_Channel_10_RXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_10_TXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_1_ERR_HwConfig,
  &OsCfg_Isr_Lin_Channel_1_RXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_1_TXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_4_ERR_HwConfig,
  &OsCfg_Isr_Lin_Channel_4_RXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_4_TXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_6_ERR_HwConfig,
  &OsCfg_Isr_Lin_Channel_6_RXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_6_TXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_7_ERR_HwConfig,
  &OsCfg_Isr_Lin_Channel_7_RXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_7_TXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_8_ERR_HwConfig,
  &OsCfg_Isr_Lin_Channel_8_RXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_8_TXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_9_ERR_HwConfig,
  &OsCfg_Isr_Lin_Channel_9_RXI_HwConfig,
  &OsCfg_Isr_Lin_Channel_9_TXI_HwConfig,
  &OsCfg_Isr_MCU_PLL_LossOfLock_HwConfig,
  &OsCfg_Isr_WKUP_IRQ0_HwConfig,
  &OsCfg_Isr_WKUP_IRQ1_HwConfig,
  &OsCfg_Isr_WKUP_IRQ2_HwConfig,
  &OsCfg_Isr_WKUP_IRQ3_HwConfig,
  NULL_PTR
};


#define OS_STOP_SEC_CONST_UNSPECIFIED
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
 *  LOCAL FUNCTION PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTIONS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL FUNCTIONS
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *  END OF FILE: Os_Hal_Core_Lcfg.c
 *********************************************************************************************************************/

