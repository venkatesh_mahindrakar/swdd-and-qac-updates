/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Csm
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Csm_Cfg.c
 *   Generation Time: 2020-11-11 14:25:29
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#define CSM_CFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/
#include "Csm_Types.h"
#include "Rte_Csm.h"

/* Include of configured services */
#include "Cry_30_LibCv.h" 


#ifndef STATIC
# define STATIC static
#endif


/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

#define CSM_START_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

CONST(Csm_AsymDecryptConfigType, CSM_CONST) Csm_AsymDecryptConfigs[CSM_NUMBER_OF_ASYM_DECRYPT_CONFIGS] = 
{
  {
    0u /*  Id  */ , 
    Cry_30_LibCv_RsaDecryptStart /*  Start  */ , 
    Cry_30_LibCv_RsaDecryptUpdate /*  Update  */ , 
    Cry_30_LibCv_RsaDecryptFinish /*  Finish  */ , 
    &Cry_30_LibCvConf_CryRsaDecryptConfig_CryRsaDecryptConfig /*  Init  */ 
  }
}; /* PRQA S 3408 */ /* MD_CSM_8.8 */

CONST(Csm_RandomGenerateConfigType, CSM_CONST) Csm_RandomGenerateConfigs[CSM_NUMBER_OF_RANDOM_GENERATE_CONFIGS] = 
{
  {
    1u /*  Id  */ , 
    Cry_30_LibCv_Fips186Generate /*  Function  */ , 
    &Cry_30_LibCvConf_CryFips186Config_CryFips186Config /*  Init  */ 
  }
}; /* PRQA S 3408 */ /* MD_CSM_8.8 */

CONST(Csm_RandomSeedConfigType, CSM_CONST) Csm_RandomSeedConfigs[CSM_NUMBER_OF_RANDOM_SEED_CONFIGS] = 
{
  {
    2u /*  Id  */ , 
    Cry_30_LibCv_Fips186SeedStart /*  Start  */ , 
    Cry_30_LibCv_Fips186SeedUpdate /*  Update  */ , 
    Cry_30_LibCv_Fips186SeedFinish /*  Finish  */ , 
    &Cry_30_LibCvConf_CryFips186Config_CryFips186Config /*  Init  */ 
  }
}; /* PRQA S 3408 */ /* MD_CSM_8.8 */

CONST(Csm_SignatureVerifyConfigType, CSM_CONST) Csm_SignatureVerifyConfigs[CSM_NUMBER_OF_SIGNATURE_VERIFY_CONFIGS] = 
{
  {
    3u /*  Id  */ , 
    Cry_30_LibCv_RsaSha1SigVerStart /*  Start  */ , 
    Cry_30_LibCv_RsaSha1SigVerUpdate /*  Update  */ , 
    Cry_30_LibCv_RsaSha1SigVerFinish /*  Finish  */ , 
    &Cry_30_LibCvConf_CryRsaSha1SigVerConfig_CryRsaSha1SigVerConfig /*  Init  */ 
  }
}; /* PRQA S 3408 */ /* MD_CSM_8.8 */

CONST(Csm_SymDecryptConfigType, CSM_CONST) Csm_SymDecryptConfigs[CSM_NUMBER_OF_SYM_DECRYPT_CONFIGS] = 
{
  {
    4u /*  Id  */ , 
    Cry_30_LibCv_AesDecrypt128Start /*  Start  */ , 
    Cry_30_LibCv_AesDecrypt128Update /*  Update  */ , 
    Cry_30_LibCv_AesDecrypt128Finish /*  Finish  */ , 
    &Cry_30_LibCvConf_CryAesDecrypt128Config_CryAesDecrypt128Config /*  Init  */ 
  }
}; /* PRQA S 3408 */ /* MD_CSM_8.8 */

CONST(Csm_SymEncryptConfigType, CSM_CONST) Csm_SymEncryptConfigs[CSM_NUMBER_OF_SYM_ENCRYPT_CONFIGS] = 
{
  {
    5u /*  Id  */ , 
    Cry_30_LibCv_AesEncrypt128Start /*  Start  */ , 
    Cry_30_LibCv_AesEncrypt128Update /*  Update  */ , 
    Cry_30_LibCv_AesEncrypt128Finish /*  Finish  */ , 
    &Cry_30_LibCvConf_CryAesEncrypt128Config_CryAesEncrypt128Config /*  Init  */ 
  }
}; /* PRQA S 3408 */ /* MD_CSM_8.8 */


#define CSM_STOP_SEC_CONST_UNSPECIFIED
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

