/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: CanTp
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: CanTp_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:34
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#define CANTP_LCFG_SOURCE

/* -----------------------------------------------------------------------------
    &&&~ Include files
 ----------------------------------------------------------------------------- */

#include "CanTp_Lcfg.h"
#if (CANTP_LOLAYER_CANIF == STD_ON)
# include "CanIf.h"
#endif
#include "PduR_CanTp.h"
/* -----------------------------------------------------------------------------
    &&&~ Data definitions
 ----------------------------------------------------------------------------- */



/**********************************************************************************************************************
 *  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CanTp_RxPduMap
**********************************************************************************************************************/
/** 
  \var    CanTp_RxPduMap
  \details
  Element                Description
  RxSduCfgIndEndIdx      the end index of the 0:n relation pointing to CanTp_RxSduCfgInd
  RxSduCfgIndStartIdx    the start index of the 0:n relation pointing to CanTp_RxSduCfgInd
  TxSduCfgIndEndIdx      the end index of the 0:n relation pointing to CanTp_TxSduCfgInd
  TxSduCfgIndStartIdx    the start index of the 0:n relation pointing to CanTp_TxSduCfgInd
*/ 
#define CANTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTp_RxPduMapType, CANTP_CONST) CanTp_RxPduMap[96] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RxSduCfgIndEndIdx                     RxSduCfgIndStartIdx                     TxSduCfgIndEndIdx                     TxSduCfgIndStartIdx                    */
  { /*     0 */                                   1u,                                     0u,                                   1u,                                     0u },
  { /*     1 */                                   2u,                                     1u,                                   2u,                                     1u },
  { /*     2 */                                   3u,                                     2u,                                   3u,                                     2u },
  { /*     3 */                                   4u,                                     3u,                                   4u,                                     3u },
  { /*     4 */                                   5u,                                     4u,                                   5u,                                     4u },
  { /*     5 */                                   6u,                                     5u,                                   6u,                                     5u },
  { /*     6 */                                   7u,                                     6u,                                   7u,                                     6u },
  { /*     7 */                                   8u,                                     7u,                                   8u,                                     7u },
  { /*     8 */                                   9u,                                     8u,                                   9u,                                     8u },
  { /*     9 */                                  10u,                                     9u,                                  10u,                                     9u },
  { /*    10 */                                  11u,                                    10u,                                  11u,                                    10u },
  { /*    11 */                                  12u,                                    11u,                                  12u,                                    11u },
  { /*    12 */                                  13u,                                    12u,                                  13u,                                    12u },
  { /*    13 */                                  14u,                                    13u,                                  14u,                                    13u },
  { /*    14 */                                  15u,                                    14u,                                  15u,                                    14u },
  { /*    15 */                                  16u,                                    15u,                                  16u,                                    15u },
  { /*    16 */                                  17u,                                    16u,                                  17u,                                    16u },
  { /*    17 */                                  18u,                                    17u,                                  18u,                                    17u },
  { /*    18 */                                  19u,                                    18u,                                  19u,                                    18u },
  { /*    19 */                                  20u,                                    19u,                                  20u,                                    19u },
  { /*    20 */                                  21u,                                    20u,                                  21u,                                    20u },
  { /*    21 */                                  22u,                                    21u,                                  22u,                                    21u },
  { /*    22 */                                  23u,                                    22u,                                  23u,                                    22u },
  { /*    23 */                                  24u,                                    23u,                                  24u,                                    23u },
  { /*    24 */                                  25u,                                    24u,                                  25u,                                    24u },
  { /*    25 */                                  26u,                                    25u,                                  26u,                                    25u },
  { /*    26 */                                  27u,                                    26u,                                  27u,                                    26u },
  { /*    27 */                                  28u,                                    27u,                                  28u,                                    27u },
  { /*    28 */                                  29u,                                    28u,                                  29u,                                    28u },
  { /*    29 */                                  30u,                                    29u,                                  30u,                                    29u },
  { /*    30 */                                  31u,                                    30u,                                  31u,                                    30u },
  { /*    31 */                                  32u,                                    31u,                                  32u,                                    31u },
  { /*    32 */                                  33u,                                    32u,                                  33u,                                    32u },
  { /*    33 */                                  34u,                                    33u,                                  34u,                                    33u },
  { /*    34 */                                  35u,                                    34u,                                  35u,                                    34u },
  { /*    35 */                                  36u,                                    35u,                                  36u,                                    35u },
  { /*    36 */                                  37u,                                    36u,                                  37u,                                    36u },
  { /*    37 */                                  38u,                                    37u,                                  38u,                                    37u },
  { /*    38 */                                  39u,                                    38u,                                  39u,                                    38u },
  { /*    39 */                                  40u,                                    39u,                                  40u,                                    39u },
  { /*    40 */                                  41u,                                    40u,                                  41u,                                    40u },
  { /*    41 */                                  42u,                                    41u,                                  42u,                                    41u },
  { /*    42 */                                  43u,                                    42u,                                  43u,                                    42u },
  { /*    43 */                                  44u,                                    43u,                                  44u,                                    43u },
  { /*    44 */                                  45u,                                    44u,                                  45u,                                    44u },
  { /*    45 */                                  46u,                                    45u,                                  46u,                                    45u },
  { /*    46 */                                  47u,                                    46u,                                  47u,                                    46u },
  { /*    47 */                                  48u,                                    47u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    48 */                                  49u,                                    48u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    49 */                                  50u,                                    49u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
    /* Index    RxSduCfgIndEndIdx                     RxSduCfgIndStartIdx                     TxSduCfgIndEndIdx                     TxSduCfgIndStartIdx                    */
  { /*    50 */                                  51u,                                    50u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    51 */                                  52u,                                    51u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    52 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  48u,                                    47u },
  { /*    53 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  49u,                                    48u },
  { /*    54 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  50u,                                    49u },
  { /*    55 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  51u,                                    50u },
  { /*    56 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  52u,                                    51u },
  { /*    57 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  53u,                                    52u },
  { /*    58 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  54u,                                    53u },
  { /*    59 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  55u,                                    54u },
  { /*    60 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  56u,                                    55u },
  { /*    61 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  57u,                                    56u },
  { /*    62 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  58u,                                    57u },
  { /*    63 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  59u,                                    58u },
  { /*    64 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  60u,                                    59u },
  { /*    65 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  61u,                                    60u },
  { /*    66 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  62u,                                    61u },
  { /*    67 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  63u,                                    62u },
  { /*    68 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  64u,                                    63u },
  { /*    69 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  65u,                                    64u },
  { /*    70 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  66u,                                    65u },
  { /*    71 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  67u,                                    66u },
  { /*    72 */ CANTP_NO_RXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_RXSDUCFGINDSTARTIDXOFRXPDUMAP,                                  68u,                                    67u },
  { /*    73 */                                  53u,                                    52u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    74 */                                  54u,                                    53u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    75 */                                  55u,                                    54u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    76 */                                  56u,                                    55u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    77 */                                  57u,                                    56u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    78 */                                  58u,                                    57u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    79 */                                  59u,                                    58u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    80 */                                  60u,                                    59u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    81 */                                  61u,                                    60u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    82 */                                  62u,                                    61u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    83 */                                  63u,                                    62u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    84 */                                  64u,                                    63u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    85 */                                  65u,                                    64u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    86 */                                  66u,                                    65u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    87 */                                  67u,                                    66u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    88 */                                  68u,                                    67u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    89 */                                  69u,                                    68u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    90 */                                  70u,                                    69u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    91 */                                  71u,                                    70u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    92 */                                  72u,                                    71u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    93 */                                  73u,                                    72u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    94 */                                  74u,                                    73u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP },
  { /*    95 */                                  75u,                                    74u, CANTP_NO_TXSDUCFGINDENDIDXOFRXPDUMAP, CANTP_NO_TXSDUCFGINDSTARTIDXOFRXPDUMAP }
};
#define CANTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_RxSduCfg
**********************************************************************************************************************/
/** 
  \var    CanTp_RxSduCfg
  \details
  Element                     Description
  LoLayerTxFcPduId        
  PduRRxSduId             
  RxPduId                 
  TxFcPduConfirmationPduId
  NAr                     
  NBr                     
  NCr                     
  RxTaType                
  RxWftMax                
  TxSduCfgIdx                 the index of the 0:1 relation pointing to CanTp_TxSduCfg
*/ 
#define CANTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTp_RxSduCfgType, CANTP_CONST) CanTp_RxSduCfg[75] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    LoLayerTxFcPduId                                                                      PduRRxSduId                              RxPduId                                     TxFcPduConfirmationPduId                        NAr  NBr  NCr   RxTaType                             RxWftMax  TxSduCfgIdx                           Comment                            Referable Keys */
  { /*     0 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_53_F3_Cab_Tp_oCabSubnet_e9059d48_Tx     , PduRConf_PduRSrcPdu_PduRSrcPdu_1ac6f203, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_74e116b6, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_74e116b6, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                             0u },  /* [CanTpRxNSdu_0b4e5ed3] */  /* [CanTpRxNSdu_0b4e5ed3, 17] */
  { /*     1 */ CanIfConf_CanIfTxPduCfg_TECU_BB2_05S_FCM_Tp_oBackbone2_69d48940_Tx                  , PduRConf_PduRSrcPdu_PduRSrcPdu_0aeda82a, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_b76b68d2, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_b76b68d2,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_0bfbcf81] */  /* [CanTpRxNSdu_0bfbcf81, 89] */
  { /*     2 */ CanIfConf_CanIfTxPduCfg_PDM_Sec_03S_FCM_Tp_oSecuritySubnet_2750bd9e_Tx              , PduRConf_PduRSrcPdu_PduRSrcPdu_c4655c6f, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_421ce650, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_421ce650,  3u,  2u,   5u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_0c44e225] */  /* [CanTpRxNSdu_0c44e225, 87] */
  { /*     3 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_A0_BB2_Tp_oBackbone2_f4db796b_Tx    , PduRConf_PduRSrcPdu_PduRSrcPdu_71b786d1, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c81a00f6, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c81a00f6, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                             3u },  /* [CanTpRxNSdu_0fc83382] */  /* [CanTpRxNSdu_0fc83382, 4] */
  { /*     4 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_98_F4_Cab_Tp_oCabSubnet_2e8c3543_Tx       , PduRConf_PduRSrcPdu_PduRSrcPdu_8c488735, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_65cbdaf5, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_65cbdaf5, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                             4u },  /* [CanTpRxNSdu_1a4aae9e] */  /* [CanTpRxNSdu_1a4aae9e, 25] */
  { /*     5 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_A2_F3_Cab_Tp_oCabSubnet_fcbc8d7d_Tx     , PduRConf_PduRSrcPdu_PduRSrcPdu_bbc023c9, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_e2f34c3e, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_e2f34c3e, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                             8u },  /* [CanTpRxNSdu_2ed2671b] */  /* [CanTpRxNSdu_2ed2671b, 21] */
  { /*     6 */ CANTP_INVALID_HDL                                                                   , PduRConf_PduRSrcPdu_PduRSrcPdu_f1553bf5, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_21f32ee4, CANTP_INVALID_HDL                             , 81u, 51u, 171u, CANTP_FUNCTIONAL_RXTATYPEOFRXSDUCFG,       4u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_3cc05d5a] */  /* [CanTpRxNSdu_3cc05d5a, 91] */
  { /*     7 */ CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_06S_FCM_Tp_oBackbone2_c2df8d7c_Tx                , PduRConf_PduRSrcPdu_PduRSrcPdu_d6988d26, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_ef1549ec, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_ef1549ec,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_4d8c7144] */  /* [CanTpRxNSdu_4d8c7144, 79] */
  { /*     8 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F1_53_BB2_Tp_oBackbone2_be0c70d1_Tx         , PduRConf_PduRSrcPdu_PduRSrcPdu_da0a8dca, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_28810efa, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_28810efa, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            10u },  /* [CanTpRxNSdu_6b50c215] */  /* [CanTpRxNSdu_6b50c215, 32] */
  { /*     9 */ CanIfConf_CanIfTxPduCfg_Alarm_Sec_06S_FCM_Tp_oSecuritySubnet_a3ee35ea_Tx            , PduRConf_PduRSrcPdu_PduRSrcPdu_eb982057, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_fc653524, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_fc653524,  3u,  2u,   5u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_6c38d736] */  /* [CanTpRxNSdu_6c38d736, 48] */
  { /*    10 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_C0_F4_Sec_Tp_oSecuritySubnet_0ba24808_Tx  , PduRConf_PduRSrcPdu_PduRSrcPdu_8259d96a, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_7ebb8e19, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_7ebb8e19, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            11u },  /* [CanTpRxNSdu_6eda9336] */  /* [CanTpRxNSdu_6eda9336, 29] */
  { /*    11 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_A2_F2_Cab_Tp_oCabSubnet_6d74e481_Tx          , PduRConf_PduRSrcPdu_PduRSrcPdu_d9d30309, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_e80a3615, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_e80a3615, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            12u },  /* [CanTpRxNSdu_9bc05ec2] */  /* [CanTpRxNSdu_9bc05ec2, 45] */
  { /*    12 */ CANTP_INVALID_HDL                                                                   , PduRConf_PduRSrcPdu_PduRSrcPdu_446f53c1, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_e9176fdb, CANTP_INVALID_HDL                             , 81u, 51u, 171u, CANTP_FUNCTIONAL_RXTATYPEOFRXSDUCFG,       4u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_9c809a2e] */  /* [CanTpRxNSdu_9c809a2e, 86] */
  { /*    13 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_40_BB2_Tp_oBackbone2_bd4d3e52_Tx         , PduRConf_PduRSrcPdu_PduRSrcPdu_d62010ae, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_47b3cfa9, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_47b3cfa9, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            13u },  /* [CanTpRxNSdu_9fd60b61] */  /* [CanTpRxNSdu_9fd60b61, 31] */
  { /*    14 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_A0_F2_Sec_Tp_oSecuritySubnet_cb87d97b_Tx     , PduRConf_PduRSrcPdu_PduRSrcPdu_b18fc6c3, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c2ef7286, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c2ef7286, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            16u },  /* [CanTpRxNSdu_05d82786] */  /* [CanTpRxNSdu_05d82786, 43] */
  { /*    15 */ CanIfConf_CanIfTxPduCfg_BBM_BB2_03S_CIOM_FCM_Tp_oBackbone2_29c4ac8e_Tx              , PduRConf_PduRSrcPdu_PduRSrcPdu_bd0d068c, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_8db8bc2c, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_8db8bc2c,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_08e2bc01] */  /* [CanTpRxNSdu_08e2bc01, 50] */
  { /*    16 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_53_BB2_Tp_oBackbone2_930c5799_Tx    , PduRConf_PduRSrcPdu_PduRSrcPdu_c0680d6c, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_52d44fc8, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_52d44fc8, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            17u },  /* [CanTpRxNSdu_15cc9c64] */  /* [CanTpRxNSdu_15cc9c64, 2] */
  { /*    17 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_26_BB2_Tp_oBackbone2_5b036461_Tx      , PduRConf_PduRSrcPdu_PduRSrcPdu_01528206, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_f54d4d82, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_f54d4d82, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            18u },  /* [CanTpRxNSdu_23c20b77] */  /* [CanTpRxNSdu_23c20b77, 8] */
  { /*    18 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_53_BB2_Tp_oBackbone2_ef1139d4_Tx         , PduRConf_PduRSrcPdu_PduRSrcPdu_266542d7, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c70ab37c, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c70ab37c, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            19u },  /* [CanTpRxNSdu_33cb3e9e] */  /* [CanTpRxNSdu_33cb3e9e, 33] */
  { /*    19 */ CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_27S_FCM_Tp_oBackbone2_16072c1e_Tx                , PduRConf_PduRSrcPdu_PduRSrcPdu_7040bac2, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_fdd87b12, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_fdd87b12,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_37a24ae1] */  /* [CanTpRxNSdu_37a24ae1, 83] */
  { /*    20 */ CanIfConf_CanIfTxPduCfg_Alarm_Sec_03S_FCM_Tp_oSecuritySubnet_48e47132_Tx            , PduRConf_PduRSrcPdu_PduRSrcPdu_304197c1, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_0630f603, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_0630f603,  3u,  2u,   5u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_53d85cb5] */  /* [CanTpRxNSdu_53d85cb5, 47] */
  { /*    21 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_C0_F3_Sec_Tp_oSecuritySubnet_3aaf1649_Tx, PduRConf_PduRSrcPdu_PduRSrcPdu_b4b812c0, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_93bdf061, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_93bdf061, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            21u },  /* [CanTpRxNSdu_66ceed3b] */  /* [CanTpRxNSdu_66ceed3b, 22] */
  { /*    22 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_40_BB2_Tp_oBackbone2_f5224357_Tx      , PduRConf_PduRSrcPdu_PduRSrcPdu_59f7faf1, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_2a0fd320, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_2a0fd320, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            22u },  /* [CanTpRxNSdu_67a44a47] */  /* [CanTpRxNSdu_67a44a47, 9] */
  { /*    23 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_53_F1_Cab_Tp_oCabSubnet_682570ce_Tx          , PduRConf_PduRSrcPdu_PduRSrcPdu_5cfa406e, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_4564c8c7, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_4564c8c7, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            23u },  /* [CanTpRxNSdu_68a1d221] */  /* [CanTpRxNSdu_68a1d221, 39] */
  { /*    24 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_A1_BB2_Tp_oBackbone2_08540fc1_Tx    , PduRConf_PduRSrcPdu_PduRSrcPdu_86e04d33, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_7c55d78f, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_7c55d78f, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            24u },  /* [CanTpRxNSdu_72ac09b7] */  /* [CanTpRxNSdu_72ac09b7, 5] */
  { /*    25 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_A2_F4_Cab_Tp_oCabSubnet_fc611706_Tx       , PduRConf_PduRSrcPdu_PduRSrcPdu_9e5d6760, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_95c73622, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_95c73622, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            26u },  /* [CanTpRxNSdu_90b4ee1f] */  /* [CanTpRxNSdu_90b4ee1f, 28] */
  { /*    26 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_53_F2_Cab_Tp_oCabSubnet_9abe6f67_Tx          , PduRConf_PduRSrcPdu_PduRSrcPdu_ccea7767, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_361163ac, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_361163ac, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            27u },  /* [CanTpRxNSdu_97d435bf] */  /* [CanTpRxNSdu_97d435bf, 41] */
  { /*    27 */ CanIfConf_CanIfTxPduCfg_Alarm_Sec_07S_FCM_Tp_oSecuritySubnet_94ec21d2_Tx            , PduRConf_PduRSrcPdu_PduRSrcPdu_26a1cbb9, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_f5215d2b, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_f5215d2b,  3u,  2u,   5u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_135f1a6f] */  /* [CanTpRxNSdu_135f1a6f, 49] */
  { /*    28 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_A0_BB2_Tp_oBackbone2_d77a4b8a_Tx         , PduRConf_PduRSrcPdu_PduRSrcPdu_1b947dce, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_4c679115, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_4c679115, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            29u },  /* [CanTpRxNSdu_155b67c1] */  /* [CanTpRxNSdu_155b67c1, 35] */
  { /*    29 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_A1_F2_Sec_Tp_oSecuritySubnet_5d2e792a_Tx     , PduRConf_PduRSrcPdu_PduRSrcPdu_6ea67099, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_3610aff9, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_3610aff9, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            30u },  /* [CanTpRxNSdu_160b460d] */  /* [CanTpRxNSdu_160b460d, 44] */
  { /*    30 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_A0_BB2_Tp_oBackbone2_7d9615e1_Tx      , PduRConf_PduRSrcPdu_PduRSrcPdu_d91d7573, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_1895635d, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_1895635d, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            32u },  /* [CanTpRxNSdu_374c2f98] */  /* [CanTpRxNSdu_374c2f98, 12] */
  { /*    31 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_C0_BB2_Tp_oBackbone2_9924f26b_Tx    , PduRConf_PduRSrcPdu_PduRSrcPdu_c050dec8, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_50bab166, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_50bab166, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            33u },  /* [CanTpRxNSdu_420eda30] */  /* [CanTpRxNSdu_420eda30, 7] */
  { /*    32 */ CanIfConf_CanIfTxPduCfg_VMCU_BB2_32S_FCM_Tp_oBackbone2_8a7e4a00_Tx                  , PduRConf_PduRSrcPdu_PduRSrcPdu_8c4f8f58, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_5a70888a, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_5a70888a,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_835e9534] */  /* [CanTpRxNSdu_835e9534, 93] */
  { /*    33 */ CanIfConf_CanIfTxPduCfg_CCM_Cab_03P_FCM_Tp_oCabSubnet_af75b436_Tx                   , PduRConf_PduRSrcPdu_PduRSrcPdu_8a32f8f6, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_2b3a4000, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_2b3a4000,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_871bc7b1] */  /* [CanTpRxNSdu_871bc7b1, 51] */
  { /*    34 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_A0_F4_Sec_Tp_oSecuritySubnet_8adef0bc_Tx  , PduRConf_PduRSrcPdu_PduRSrcPdu_064dc9ab, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_0229ec59, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_0229ec59, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            36u },  /* [CanTpRxNSdu_917ba177] */  /* [CanTpRxNSdu_917ba177, 26] */
  { /*    35 */ CanIfConf_CanIfTxPduCfg_VMCU_BB2_34S_FCM_Tp_oBackbone2_c87b48e1_Tx                  , PduRConf_PduRSrcPdu_PduRSrcPdu_656dbd2a, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_7f7d9999, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_7f7d9999,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_948ebe8b] */  /* [CanTpRxNSdu_948ebe8b, 94] */
  { /*    36 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_26_BB2_Tp_oBackbone2_aba3af99_Tx    , PduRConf_PduRSrcPdu_PduRSrcPdu_304f257a, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_34450317, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_34450317, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            37u },  /* [CanTpRxNSdu_1047cc45] */  /* [CanTpRxNSdu_1047cc45, 0] */
  { /*    37 */ CanIfConf_CanIfTxPduCfg_DDM_Sec_04S_FCM_Tp_oSecuritySubnet_eddd4cee_Tx              , PduRConf_PduRSrcPdu_PduRSrcPdu_06bdd5c2, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c0beb877, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c0beb877,  3u,  2u,   5u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_7107e017] */  /* [CanTpRxNSdu_7107e017, 74] */
  { /*    38 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_98_BB2_Tp_oBackbone2_0c1730ea_Tx         , PduRConf_PduRSrcPdu_PduRSrcPdu_246f1e3b, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_0cd86411, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_0cd86411, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            38u },  /* [CanTpRxNSdu_8729f19d] */  /* [CanTpRxNSdu_8729f19d, 34] */
  { /*    39 */ CanIfConf_CanIfTxPduCfg_DDM_Sec_05S_FCM_Tp_oSecuritySubnet_d14dc345_Tx              , PduRConf_PduRSrcPdu_PduRSrcPdu_0948f4eb, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_51e36fbf, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_51e36fbf,  3u,  2u,   5u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_598434b3] */  /* [CanTpRxNSdu_598434b3, 75] */
  { /*    40 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_A1_F4_Sec_Tp_oSecuritySubnet_eb89cc56_Tx  , PduRConf_PduRSrcPdu_PduRSrcPdu_df9a30f3, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_28dbd0e5, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_28dbd0e5, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            41u },  /* [CanTpRxNSdu_2609472a] */  /* [CanTpRxNSdu_2609472a, 27] */
  { /*    41 */ CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_36S_FCM_Tp_oBackbone2_bf84bd48_Tx                , PduRConf_PduRSrcPdu_PduRSrcPdu_270fccd3, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_904bcd5d, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_904bcd5d,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_4374674a] */  /* [CanTpRxNSdu_4374674a, 85] */
  { /*    42 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_26_F3_Cab_Tp_oCabSubnet_2e1f9ada_Tx     , PduRConf_PduRSrcPdu_PduRSrcPdu_b9d033d4, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_27a9b571, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_27a9b571, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            42u },  /* [CanTpRxNSdu_8388957a] */  /* [CanTpRxNSdu_8388957a, 16] */
  { /*    43 */ CanIfConf_CanIfTxPduCfg_TECU_BB2_06S_FCM_Tp_oBackbone2_a56e8b10_Tx                  , PduRConf_PduRSrcPdu_PduRSrcPdu_187ffacf, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_349983d6, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_349983d6,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_33326932] */  /* [CanTpRxNSdu_33326932, 90] */
  { /*    44 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_26_F4_Cab_Tp_oCabSubnet_b42402a4_Tx       , PduRConf_PduRSrcPdu_PduRSrcPdu_25a59123, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_70297ecd, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_70297ecd, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            44u },  /* [CanTpRxNSdu_93056798] */  /* [CanTpRxNSdu_93056798, 23] */
  { /*    45 */ CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_21S_FCM_Tp_oBackbone2_af5a2700_Tx                , PduRConf_PduRSrcPdu_PduRSrcPdu_be2dd0ee, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_629a014b, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_629a014b,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_a8f40adf] */  /* [CanTpRxNSdu_a8f40adf, 82] */
  { /*    46 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_26_F2_Cab_Tp_oCabSubnet_e2a6386f_Tx          , PduRConf_PduRSrcPdu_PduRSrcPdu_62ae1653, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c7e1ce5c, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c7e1ce5c, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            45u },  /* [CanTpRxNSdu_a23f8213] */  /* [CanTpRxNSdu_a23f8213, 40] */
  { /*    47 */ CanIfConf_CanIfTxPduCfg_DDM_Sec_03S_FCM_Tp_oSecuritySubnet_5a2ee2bf_Tx              , PduRConf_PduRSrcPdu_PduRSrcPdu_fdae10f0, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_9778b9b1, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_9778b9b1,  3u,  2u,   5u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_a9722d89] */  /* [CanTpRxNSdu_a9722d89, 73] */
  { /*    48 */ CanIfConf_CanIfTxPduCfg_VMCU_BB2_31S_FCM_Tp_oBackbone2_46c44850_Tx                  , PduRConf_PduRSrcPdu_PduRSrcPdu_962aaefb, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_19b1d643, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_19b1d643,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_b7bc20cf] */  /* [CanTpRxNSdu_b7bc20cf, 92] */
  { /*    49 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_C0_BB2_Tp_oBackbone2_ae5b54dc_Tx         , PduRConf_PduRSrcPdu_PduRSrcPdu_2e6f76ae, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_5e3f6ed5, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_5e3f6ed5, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            49u },  /* [CanTpRxNSdu_b397195b] */  /* [CanTpRxNSdu_b397195b, 38] */
    /* Index    LoLayerTxFcPduId                                                                      PduRRxSduId                              RxPduId                                     TxFcPduConfirmationPduId                        NAr  NBr  NCr   RxTaType                             RxWftMax  TxSduCfgIdx                           Comment                            Referable Keys */
  { /*    50 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_98_BB2_Tp_oBackbone2_8516ddd7_Tx      , PduRConf_PduRSrcPdu_PduRSrcPdu_63947842, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_6ee112f0, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_6ee112f0, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            51u },  /* [CanTpRxNSdu_bc085821] */  /* [CanTpRxNSdu_bc085821, 11] */
  { /*    51 */ CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_20S_FCM_Tp_oBackbone2_2def59ba_Tx                , PduRConf_PduRSrcPdu_PduRSrcPdu_fed5ac9b, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_ada8fbb1, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_ada8fbb1,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_bd494c6b] */  /* [CanTpRxNSdu_bd494c6b, 81] */
  { /*    52 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_26_BB2_Tp_oBackbone2_ebb70ff2_Tx         , PduRConf_PduRSrcPdu_PduRSrcPdu_dfacef7f, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_d579a88f, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_d579a88f, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            52u },  /* [CanTpRxNSdu_bf49619c] */  /* [CanTpRxNSdu_bf49619c, 30] */
  { /*    53 */ CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_19P_CIOM_FCM_Tp_oBackbone2_1a616940_Tx           , PduRConf_PduRSrcPdu_PduRSrcPdu_75cfe088, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_d667832b, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_d667832b,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_c3d81543] */  /* [CanTpRxNSdu_c3d81543, 80] */
  { /*    54 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_A1_BB2_Tp_oBackbone2_bca8fbcc_Tx      , PduRConf_PduRSrcPdu_PduRSrcPdu_39fe25b5, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_e98bdc69, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_e98bdc69, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            53u },  /* [CanTpRxNSdu_c8d90e4f] */  /* [CanTpRxNSdu_c8d90e4f, 13] */
  { /*    55 */ CanIfConf_CanIfTxPduCfg_EMS_BB2_09S_FCM_Tp_oBackbone2_41cc37fa_Tx                   , PduRConf_PduRSrcPdu_PduRSrcPdu_7ab1f990, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_715a9875, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_715a9875,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_c9b42c98] */  /* [CanTpRxNSdu_c9b42c98, 76] */
  { /*    56 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_98_F3_Cab_Tp_oCabSubnet_2daafb4e_Tx     , PduRConf_PduRSrcPdu_PduRSrcPdu_c6acd63d, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_9bc423ff, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_9bc423ff, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            54u },  /* [CanTpRxNSdu_c66f942c] */  /* [CanTpRxNSdu_c66f942c, 18] */
  { /*    57 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_A1_BB2_Tp_oBackbone2_441131ae_Tx         , PduRConf_PduRSrcPdu_PduRSrcPdu_30b4cd32, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_8c756e81, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_8c756e81, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            56u },  /* [CanTpRxNSdu_c046cd74] */  /* [CanTpRxNSdu_c046cd74, 36] */
  { /*    58 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_40_BB2_Tp_oBackbone2_7b130fa6_Tx    , PduRConf_PduRSrcPdu_PduRSrcPdu_8771577a, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_0b125426, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_0b125426, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            57u },  /* [CanTpRxNSdu_c8855cd9] */  /* [CanTpRxNSdu_c8855cd9, 1] */
  { /*    59 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_C0_F2_Sec_Tp_oSecuritySubnet_f27248ff_Tx     , PduRConf_PduRSrcPdu_PduRSrcPdu_89263a8e, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_80566eb2, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_80566eb2, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            59u },  /* [CanTpRxNSdu_d7af4a84] */  /* [CanTpRxNSdu_d7af4a84, 46] */
  { /*    60 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_A2_BB2_Tp_oBackbone2_d6b4927e_Tx    , PduRConf_PduRSrcPdu_PduRSrcPdu_ac4babc0, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_8ab9e6a2, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_8ab9e6a2, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            60u },  /* [CanTpRxNSdu_d137db99] */  /* [CanTpRxNSdu_d137db99, 6] */
  { /*    61 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_53_BB2_Tp_oBackbone2_280e15fa_Tx      , PduRConf_PduRSrcPdu_PduRSrcPdu_a40032de, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_459b5584, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_459b5584, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            61u },  /* [CanTpRxNSdu_dbf2493d] */  /* [CanTpRxNSdu_dbf2493d, 10] */
  { /*    62 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_98_F2_Cab_Tp_oCabSubnet_4c51814f_Tx          , PduRConf_PduRSrcPdu_PduRSrcPdu_cd1487f2, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_5b7a8082, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_5b7a8082, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            62u },  /* [CanTpRxNSdu_de4f6c03] */  /* [CanTpRxNSdu_de4f6c03, 42] */
  { /*    63 */ CanIfConf_CanIfTxPduCfg_EMS_BB2_11S_FCM_Tp_oBackbone2_6cd5cf95_Tx                   , PduRConf_PduRSrcPdu_PduRSrcPdu_ab3d4554, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_25603869, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_25603869,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_e0b16505] */  /* [CanTpRxNSdu_e0b16505, 77] */
  { /*    64 */ CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_04S_FCM_Tp_oBackbone2_1cc47649_Tx                , PduRConf_PduRSrcPdu_PduRSrcPdu_5cdf6731, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_409d9ec2, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_409d9ec2,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_e66abd8d] */  /* [CanTpRxNSdu_e66abd8d, 78] */
  { /*    65 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_C0_BB2_Tp_oBackbone2_f7aad0d7_Tx      , PduRConf_PduRSrcPdu_PduRSrcPdu_bd1ce89a, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_556088d3, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_556088d3, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            63u },  /* [CanTpRxNSdu_e128ac5e] */  /* [CanTpRxNSdu_e128ac5e, 15] */
  { /*    66 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_98_BB2_Tp_oBackbone2_73235a33_Tx    , PduRConf_PduRSrcPdu_PduRSrcPdu_bfda77cf, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_b9a1646f, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_b9a1646f, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            64u },  /* [CanTpRxNSdu_f0ca165a] */  /* [CanTpRxNSdu_f0ca165a, 3] */
  { /*    67 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_A1_F3_Sec_Tp_oSecuritySubnet_a1b03b66_Tx, PduRConf_PduRSrcPdu_PduRSrcPdu_c28e0d5d, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_7c980d9a, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_7c980d9a, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            65u },  /* [CanTpRxNSdu_f0daeab1] */  /* [CanTpRxNSdu_f0daeab1, 20] */
  { /*    68 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_A0_F3_Sec_Tp_oSecuritySubnet_09265670_Tx, PduRConf_PduRSrcPdu_PduRSrcPdu_ff6a6468, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_64268354, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_64268354, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            67u },  /* [CanTpRxNSdu_f5b24442] */  /* [CanTpRxNSdu_f5b24442, 19] */
  { /*    69 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_A2_BB2_Tp_oBackbone2_2addb983_Tx         , PduRConf_PduRSrcPdu_PduRSrcPdu_3c553d77, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_1f316795, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_1f316795, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            68u },  /* [CanTpRxNSdu_f5e91b1b] */  /* [CanTpRxNSdu_f5e91b1b, 37] */
  { /*    70 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_A2_BB2_Tp_oBackbone2_249acffa_Tx      , PduRConf_PduRSrcPdu_PduRSrcPdu_ea4e2140, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_bfb3ee4b, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_bfb3ee4b, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            69u },  /* [CanTpRxNSdu_f7a6d049] */  /* [CanTpRxNSdu_f7a6d049, 14] */
  { /*    71 */ CanIfConf_CanIfTxPduCfg_PDM_Sec_04S_FCM_Tp_oSecuritySubnet_90a313cf_Tx              , PduRConf_PduRSrcPdu_PduRSrcPdu_26b85268, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_ddd13b60, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_ddd13b60,  3u,  2u,   5u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_f932f12a] */  /* [CanTpRxNSdu_f932f12a, 88] */
  { /*    72 */ CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_35S_FCM_Tp_oBackbone2_e32a38c7_Tx                , PduRConf_PduRSrcPdu_PduRSrcPdu_99763ffd, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_465d75f5, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_465d75f5,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_fd5af81d] */  /* [CanTpRxNSdu_fd5af81d, 84] */
  { /*    73 */ CanIfConf_CanIfTxPduCfg_VMCU_BB2_57P_FCM_Tp_oBackbone2_65fb58e9_Tx                  , PduRConf_PduRSrcPdu_PduRSrcPdu_46a397e3, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_28a0dc1f, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_28a0dc1f,  6u,  6u,  31u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,      50u, CANTP_NO_TXSDUCFGIDXOFRXSDUCFG },  /* [CanTpRxNSdu_fe9c6258] */  /* [CanTpRxNSdu_fe9c6258, 95] */
  { /*    74 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_53_F4_Cab_Tp_oCabSubnet_75ef26c1_Tx       , PduRConf_PduRSrcPdu_PduRSrcPdu_99e30330, CanTpConf_CanTpRxNPdu_CanTpRxNPdu_c096cb56, CanTpConf_CanTpTxFcNPdu_CanTpTxFcNPdu_c096cb56, 81u, 51u, 171u,   CANTP_PHYSICAL_RXTATYPEOFRXSDUCFG,       4u,                            71u }   /* [CanTpRxNSdu_ff43a8b1] */  /* [CanTpRxNSdu_ff43a8b1, 24] */
};
#define CANTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_RxSduCfgInd
**********************************************************************************************************************/
/** 
  \var    CanTp_RxSduCfgInd
  \brief  the indexes of the 1:1 sorted relation pointing to CanTp_RxSduCfg
*/ 
#define CANTP_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTp_RxSduCfgIndType, CANTP_CONST) CanTp_RxSduCfgInd[75] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     RxSduCfgInd      Referable Keys */
  /*     0 */          36u,  /* [0] */
  /*     1 */          58u,  /* [1] */
  /*     2 */          16u,  /* [2] */
  /*     3 */          66u,  /* [3] */
  /*     4 */           3u,  /* [4] */
  /*     5 */          24u,  /* [5] */
  /*     6 */          60u,  /* [6] */
  /*     7 */          31u,  /* [7] */
  /*     8 */          17u,  /* [8] */
  /*     9 */          22u,  /* [9] */
  /*    10 */          61u,  /* [10] */
  /*    11 */          50u,  /* [11] */
  /*    12 */          30u,  /* [12] */
  /*    13 */          54u,  /* [13] */
  /*    14 */          70u,  /* [14] */
  /*    15 */          65u,  /* [15] */
  /*    16 */          42u,  /* [16] */
  /*    17 */           0u,  /* [17] */
  /*    18 */          56u,  /* [18] */
  /*    19 */          68u,  /* [19] */
  /*    20 */          67u,  /* [20] */
  /*    21 */           5u,  /* [21] */
  /*    22 */          21u,  /* [22] */
  /*    23 */          44u,  /* [23] */
  /*    24 */          74u,  /* [24] */
  /*    25 */           4u,  /* [25] */
  /*    26 */          34u,  /* [26] */
  /*    27 */          40u,  /* [27] */
  /*    28 */          25u,  /* [28] */
  /*    29 */          10u,  /* [29] */
  /*    30 */          52u,  /* [30] */
  /*    31 */          13u,  /* [31] */
  /*    32 */           8u,  /* [32] */
  /*    33 */          18u,  /* [33] */
  /*    34 */          38u,  /* [34] */
  /*    35 */          28u,  /* [35] */
  /*    36 */          57u,  /* [36] */
  /*    37 */          69u,  /* [37] */
  /*    38 */          49u,  /* [38] */
  /*    39 */          23u,  /* [39] */
  /*    40 */          46u,  /* [40] */
  /*    41 */          26u,  /* [41] */
  /*    42 */          62u,  /* [42] */
  /*    43 */          14u,  /* [43] */
  /*    44 */          29u,  /* [44] */
  /*    45 */          11u,  /* [45] */
  /*    46 */          59u,  /* [46] */
  /*    47 */          20u,  /* [47] */
  /*    48 */           9u,  /* [48] */
  /*    49 */          27u,  /* [49] */
  /* Index     RxSduCfgInd      Referable Keys */
  /*    50 */          15u,  /* [50] */
  /*    51 */          33u,  /* [51] */
  /*    52 */          47u,  /* [73] */
  /*    53 */          37u,  /* [74] */
  /*    54 */          39u,  /* [75] */
  /*    55 */          55u,  /* [76] */
  /*    56 */          63u,  /* [77] */
  /*    57 */          64u,  /* [78] */
  /*    58 */           7u,  /* [79] */
  /*    59 */          53u,  /* [80] */
  /*    60 */          51u,  /* [81] */
  /*    61 */          45u,  /* [82] */
  /*    62 */          19u,  /* [83] */
  /*    63 */          72u,  /* [84] */
  /*    64 */          41u,  /* [85] */
  /*    65 */          12u,  /* [86] */
  /*    66 */           2u,  /* [87] */
  /*    67 */          71u,  /* [88] */
  /*    68 */           1u,  /* [89] */
  /*    69 */          43u,  /* [90] */
  /*    70 */           6u,  /* [91] */
  /*    71 */          48u,  /* [92] */
  /*    72 */          32u,  /* [93] */
  /*    73 */          35u,  /* [94] */
  /*    74 */          73u   /* [95] */
};
#define CANTP_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_RxSduSnv2Hdl
**********************************************************************************************************************/
/** 
  \var    CanTp_RxSduSnv2Hdl
  \details
  Element        Description
  RxSduCfgIdx    the index of the 0:1 relation pointing to CanTp_RxSduCfg
*/ 
#define CANTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTp_RxSduSnv2HdlType, CANTP_CONST) CanTp_RxSduSnv2Hdl[75] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RxSduCfgIdx        Comment */
  { /*     0 */          0u },  /* [CanTpRxNSdu_0b4e5ed3] */
  { /*     1 */          1u },  /* [CanTpRxNSdu_0bfbcf81] */
  { /*     2 */          2u },  /* [CanTpRxNSdu_0c44e225] */
  { /*     3 */          3u },  /* [CanTpRxNSdu_0fc83382] */
  { /*     4 */          4u },  /* [CanTpRxNSdu_1a4aae9e] */
  { /*     5 */          5u },  /* [CanTpRxNSdu_2ed2671b] */
  { /*     6 */          6u },  /* [CanTpRxNSdu_3cc05d5a] */
  { /*     7 */          7u },  /* [CanTpRxNSdu_4d8c7144] */
  { /*     8 */          8u },  /* [CanTpRxNSdu_6b50c215] */
  { /*     9 */          9u },  /* [CanTpRxNSdu_6c38d736] */
  { /*    10 */         10u },  /* [CanTpRxNSdu_6eda9336] */
  { /*    11 */         11u },  /* [CanTpRxNSdu_9bc05ec2] */
  { /*    12 */         12u },  /* [CanTpRxNSdu_9c809a2e] */
  { /*    13 */         13u },  /* [CanTpRxNSdu_9fd60b61] */
  { /*    14 */         14u },  /* [CanTpRxNSdu_05d82786] */
  { /*    15 */         15u },  /* [CanTpRxNSdu_08e2bc01] */
  { /*    16 */         16u },  /* [CanTpRxNSdu_15cc9c64] */
  { /*    17 */         17u },  /* [CanTpRxNSdu_23c20b77] */
  { /*    18 */         18u },  /* [CanTpRxNSdu_33cb3e9e] */
  { /*    19 */         19u },  /* [CanTpRxNSdu_37a24ae1] */
  { /*    20 */         20u },  /* [CanTpRxNSdu_53d85cb5] */
  { /*    21 */         21u },  /* [CanTpRxNSdu_66ceed3b] */
  { /*    22 */         22u },  /* [CanTpRxNSdu_67a44a47] */
  { /*    23 */         23u },  /* [CanTpRxNSdu_68a1d221] */
  { /*    24 */         24u },  /* [CanTpRxNSdu_72ac09b7] */
  { /*    25 */         25u },  /* [CanTpRxNSdu_90b4ee1f] */
  { /*    26 */         26u },  /* [CanTpRxNSdu_97d435bf] */
  { /*    27 */         27u },  /* [CanTpRxNSdu_135f1a6f] */
  { /*    28 */         28u },  /* [CanTpRxNSdu_155b67c1] */
  { /*    29 */         29u },  /* [CanTpRxNSdu_160b460d] */
  { /*    30 */         30u },  /* [CanTpRxNSdu_374c2f98] */
  { /*    31 */         31u },  /* [CanTpRxNSdu_420eda30] */
  { /*    32 */         32u },  /* [CanTpRxNSdu_835e9534] */
  { /*    33 */         33u },  /* [CanTpRxNSdu_871bc7b1] */
  { /*    34 */         34u },  /* [CanTpRxNSdu_917ba177] */
  { /*    35 */         35u },  /* [CanTpRxNSdu_948ebe8b] */
  { /*    36 */         36u },  /* [CanTpRxNSdu_1047cc45] */
  { /*    37 */         37u },  /* [CanTpRxNSdu_7107e017] */
  { /*    38 */         38u },  /* [CanTpRxNSdu_8729f19d] */
  { /*    39 */         39u },  /* [CanTpRxNSdu_598434b3] */
  { /*    40 */         40u },  /* [CanTpRxNSdu_2609472a] */
  { /*    41 */         41u },  /* [CanTpRxNSdu_4374674a] */
  { /*    42 */         42u },  /* [CanTpRxNSdu_8388957a] */
  { /*    43 */         43u },  /* [CanTpRxNSdu_33326932] */
  { /*    44 */         44u },  /* [CanTpRxNSdu_93056798] */
  { /*    45 */         45u },  /* [CanTpRxNSdu_a8f40adf] */
  { /*    46 */         46u },  /* [CanTpRxNSdu_a23f8213] */
  { /*    47 */         47u },  /* [CanTpRxNSdu_a9722d89] */
  { /*    48 */         48u },  /* [CanTpRxNSdu_b7bc20cf] */
  { /*    49 */         49u },  /* [CanTpRxNSdu_b397195b] */
    /* Index    RxSduCfgIdx        Comment */
  { /*    50 */         50u },  /* [CanTpRxNSdu_bc085821] */
  { /*    51 */         51u },  /* [CanTpRxNSdu_bd494c6b] */
  { /*    52 */         52u },  /* [CanTpRxNSdu_bf49619c] */
  { /*    53 */         53u },  /* [CanTpRxNSdu_c3d81543] */
  { /*    54 */         54u },  /* [CanTpRxNSdu_c8d90e4f] */
  { /*    55 */         55u },  /* [CanTpRxNSdu_c9b42c98] */
  { /*    56 */         56u },  /* [CanTpRxNSdu_c66f942c] */
  { /*    57 */         57u },  /* [CanTpRxNSdu_c046cd74] */
  { /*    58 */         58u },  /* [CanTpRxNSdu_c8855cd9] */
  { /*    59 */         59u },  /* [CanTpRxNSdu_d7af4a84] */
  { /*    60 */         60u },  /* [CanTpRxNSdu_d137db99] */
  { /*    61 */         61u },  /* [CanTpRxNSdu_dbf2493d] */
  { /*    62 */         62u },  /* [CanTpRxNSdu_de4f6c03] */
  { /*    63 */         63u },  /* [CanTpRxNSdu_e0b16505] */
  { /*    64 */         64u },  /* [CanTpRxNSdu_e66abd8d] */
  { /*    65 */         65u },  /* [CanTpRxNSdu_e128ac5e] */
  { /*    66 */         66u },  /* [CanTpRxNSdu_f0ca165a] */
  { /*    67 */         67u },  /* [CanTpRxNSdu_f0daeab1] */
  { /*    68 */         68u },  /* [CanTpRxNSdu_f5b24442] */
  { /*    69 */         69u },  /* [CanTpRxNSdu_f5e91b1b] */
  { /*    70 */         70u },  /* [CanTpRxNSdu_f7a6d049] */
  { /*    71 */         71u },  /* [CanTpRxNSdu_f932f12a] */
  { /*    72 */         72u },  /* [CanTpRxNSdu_fd5af81d] */
  { /*    73 */         73u },  /* [CanTpRxNSdu_fe9c6258] */
  { /*    74 */         74u }   /* [CanTpRxNSdu_ff43a8b1] */
};
#define CANTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_TxSduCfg
**********************************************************************************************************************/
/** 
  \var    CanTp_TxSduCfg
  \details
  Element                   Description
  LoLayerTxPduId        
  PduRTxSduId           
  RxFcPduId             
  TxPduConfirmationPduId
  TransmitCancellation  
  NAs                   
  NBs                   
  NCs                   
  RxSduCfgIdx               the index of the 0:1 relation pointing to CanTp_RxSduCfg
  TxTaType              
*/ 
#define CANTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTp_TxSduCfgType, CANTP_CONST) CanTp_TxSduCfg[72] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    LoLayerTxPduId                                                                        PduRTxSduId                                                                 RxFcPduId                                       TxPduConfirmationPduId                      TransmitCancellation  NAs  NBs   NCs  RxSduCfgIdx                     TxTaType                                   Comment                            Referable Keys */
  { /*     0 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_53_F3_Cab_Tp_oCabSubnet_e9059d48_Tx     , PduRConf_PduRDestPdu_CIOM_14cb3836_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_561bc244, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_561bc244,                FALSE, 81u, 171u, 51u,                             0u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_0b4e5ed3] */  /* [CanTpTxNSdu_0b4e5ed3, 17] */
  { /*     1 */ CanIfConf_CanIfTxPduCfg_CIOM_Cab_04S_Tp_oCabSubnet_e3ca2569_Tx                      , PduRConf_PduRDestPdu_CIOM_Cab_04S_oCabSubnet_779a15ce_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_89f8ff59, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_89f8ff59,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_0b28b3c4] */  /* [CanTpTxNSdu_0b28b3c4, 56] */
  { /*     2 */ CanIfConf_CanIfTxPduCfg_IntTesterTGW2FuncDiagMsg_Sec_Tp_oSecuritySubnet_57d96404_Tx , PduRConf_PduRDestPdu_CIOM_9aa9598b_Tx                                     , CANTP_INVALID_HDL                             , CanTpConf_CanTpTxNPdu_CanTpTxNPdu_2e007fb2,                FALSE, 81u, 171u, 51u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG, CANTP_FUNCTIONAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_0f9a4f9c] */  /* [CanTpTxNSdu_0f9a4f9c, 65535] */
  { /*     3 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_A0_BB2_Tp_oBackbone2_f4db796b_Tx    , PduRConf_PduRDestPdu_CIOM_9f94b5ae_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_eae0d404, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_eae0d404,                FALSE, 81u, 171u, 51u,                             3u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_0fc83382] */  /* [CanTpTxNSdu_0fc83382, 4] */
  { /*     4 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_98_F4_Cab_Tp_oCabSubnet_2e8c3543_Tx       , PduRConf_PduRDestPdu_CIOM_ee94b160_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_47310e07, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_47310e07,                FALSE, 81u, 171u, 51u,                             4u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_1a4aae9e] */  /* [CanTpTxNSdu_1a4aae9e, 25] */
  { /*     5 */ CanIfConf_CanIfTxPduCfg_CIOM_Cab_13S_Tp_oCabSubnet_e50870d8_Tx                      , PduRConf_PduRDestPdu_CIOM_Cab_13S_oCabSubnet_f9fd97a8_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_4322dc05, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_4322dc05,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_2b6a0a70] */  /* [CanTpTxNSdu_2b6a0a70, 59] */
  { /*     6 */ CanIfConf_CanIfTxPduCfg_CIOM_Sec_06S_Tp_oSecuritySubnet_39a2f744_Tx                 , PduRConf_PduRDestPdu_CIOM_Sec_06S_oSecuritySubnet_423b684c_Tx             , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_7d9d2994, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_7d9d2994,                FALSE,  3u,   5u,  2u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_2c8e4400] */  /* [CanTpTxNSdu_2c8e4400, 66] */
  { /*     7 */ CanIfConf_CanIfTxPduCfg_CIOM_Cab_11S_Tp_oCabSubnet_ec1435f4_Tx                      , PduRConf_PduRDestPdu_CIOM_Cab_11S_oCabSubnet_c1389abd_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_1b772fca, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_1b772fca,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_2c8eef3f] */  /* [CanTpTxNSdu_2c8eef3f, 58] */
  { /*     8 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_A2_F3_Cab_Tp_oCabSubnet_fcbc8d7d_Tx     , PduRConf_PduRDestPdu_CIOM_e217a053_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_c00998cc, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_c00998cc,                FALSE, 81u, 171u, 51u,                             5u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_2ed2671b] */  /* [CanTpTxNSdu_2ed2671b, 21] */
  { /*     9 */ CanIfConf_CanIfTxPduCfg_CIOM_Sec_11S_Tp_oSecuritySubnet_dc8daaf0_Tx                 , PduRConf_PduRDestPdu_CIOM_Sec_11S_oSecuritySubnet_d4660cbf_Tx             , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_adf550fb, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_adf550fb,                FALSE,  3u,   5u,  2u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_4c161f1d] */  /* [CanTpTxNSdu_4c161f1d, 71] */
  { /*    10 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F1_53_BB2_Tp_oBackbone2_be0c70d1_Tx         , PduRConf_PduRDestPdu_CIOM_e5f820d2_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_0a7bda08, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_0a7bda08,                FALSE, 81u, 171u, 51u,                             8u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_6b50c215] */  /* [CanTpTxNSdu_6b50c215, 32] */
  { /*    11 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_C0_F4_Sec_Tp_oSecuritySubnet_0ba24808_Tx  , PduRConf_PduRDestPdu_CIOM_98a0f91f_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_5c415aeb, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_5c415aeb,                FALSE, 81u, 171u, 51u,                            10u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_6eda9336] */  /* [CanTpTxNSdu_6eda9336, 29] */
  { /*    12 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_A2_F2_Cab_Tp_oCabSubnet_6d74e481_Tx          , PduRConf_PduRDestPdu_CIOM_50cc5439_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_caf0e2e7, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_caf0e2e7,                FALSE, 81u, 171u, 51u,                            11u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_9bc05ec2] */  /* [CanTpTxNSdu_9bc05ec2, 45] */
  { /*    13 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_40_BB2_Tp_oBackbone2_bd4d3e52_Tx         , PduRConf_PduRDestPdu_PhysDiagRespMsg_F2_40_BB2_oBackbone2_1ef42df7_Tx     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_65491b5b, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_65491b5b,                FALSE, 81u, 171u, 51u,                            13u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_9fd60b61] */  /* [CanTpTxNSdu_9fd60b61, 31] */
  { /*    14 */ CanIfConf_CanIfTxPduCfg_CIOM_Sec_08S_Tp_oSecuritySubnet_eafb50f2_Tx                 , PduRConf_PduRDestPdu_CIOM_Sec_08S_oSecuritySubnet_3bd74a1d_Tx             , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_1bcf3d59, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_1bcf3d59,                FALSE,  3u,   5u,  2u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_9fe90810] */  /* [CanTpTxNSdu_9fe90810, 68] */
  { /*    15 */ CanIfConf_CanIfTxPduCfg_CIOM_Cab_25S_Tp_oCabSubnet_d5957759_Tx                      , PduRConf_PduRDestPdu_CIOM_Cab_25S_oCabSubnet_eba53182_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_9a489703, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_9a489703,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_05a0c60a] */  /* [CanTpTxNSdu_05a0c60a, 61] */
  { /*    16 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_A0_F2_Sec_Tp_oSecuritySubnet_cb87d97b_Tx     , PduRConf_PduRDestPdu_CIOM_0fefd926_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_e015a674, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_e015a674,                FALSE, 81u, 171u, 51u,                            14u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_05d82786] */  /* [CanTpTxNSdu_05d82786, 43] */
  { /*    17 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_53_BB2_Tp_oBackbone2_930c5799_Tx    , PduRConf_PduRDestPdu_CIOM_e742f677_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_702e9b3a, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_702e9b3a,                FALSE, 81u, 171u, 51u,                            16u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_15cc9c64] */  /* [CanTpTxNSdu_15cc9c64, 2] */
  { /*    18 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_26_BB2_Tp_oBackbone2_5b036461_Tx      , PduRConf_PduRDestPdu_CIOM_5cd3a27b_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_d7b79970, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_d7b79970,                FALSE, 81u, 171u, 51u,                            17u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_23c20b77] */  /* [CanTpTxNSdu_23c20b77, 8] */
  { /*    19 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_53_BB2_Tp_oBackbone2_ef1139d4_Tx         , PduRConf_PduRDestPdu_CIOM_a81020b5_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_e5f0678e, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_e5f0678e,                FALSE, 81u, 171u, 51u,                            18u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_33cb3e9e] */  /* [CanTpTxNSdu_33cb3e9e, 33] */
  { /*    20 */ CanIfConf_CanIfTxPduCfg_CIOM_Cab_34P_Tp_oCabSubnet_377e6916_Tx                      , PduRConf_PduRDestPdu_CIOM_Cab_34P_oCabSubnet_3fa59da8_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_d3983306, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_d3983306,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_39c7c1db] */  /* [CanTpTxNSdu_39c7c1db, 64] */
  { /*    21 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_C0_F3_Sec_Tp_oSecuritySubnet_3aaf1649_Tx, PduRConf_PduRDestPdu_CIOM_2b6cb3ad_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_b1472493, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_b1472493,                FALSE, 81u, 171u, 51u,                            21u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_66ceed3b] */  /* [CanTpTxNSdu_66ceed3b, 22] */
  { /*    22 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_40_BB2_Tp_oBackbone2_f5224357_Tx      , PduRConf_PduRDestPdu_DiagRespMsgIntTGW2_F4_40_BB2_oBackbone2_3b72250e_Tx  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_08f507d2, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_08f507d2,                FALSE, 81u, 171u, 51u,                            22u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_67a44a47] */  /* [CanTpTxNSdu_67a44a47, 9] */
  { /*    23 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_53_F1_Cab_Tp_oCabSubnet_682570ce_Tx          , PduRConf_PduRDestPdu_CIOM_2dc3f245_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_679e1c35, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_679e1c35,                FALSE, 81u, 171u, 81u,                            23u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_68a1d221] */  /* [CanTpTxNSdu_68a1d221, 39] */
  { /*    24 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_A1_BB2_Tp_oBackbone2_08540fc1_Tx    , PduRConf_PduRDestPdu_CIOM_500aa266_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_5eaf037d, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_5eaf037d,                FALSE, 81u, 171u, 51u,                            24u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_72ac09b7] */  /* [CanTpTxNSdu_72ac09b7, 5] */
  { /*    25 */ CanIfConf_CanIfTxPduCfg_TesterFuncDiagMsg_Cab_Tp_oCabSubnet_d293ecaa_Tx             , PduRConf_PduRDestPdu_CIOM_017be91a_Tx                                     , CANTP_INVALID_HDL                             , CanTpConf_CanTpTxNPdu_CanTpTxNPdu_9516a986,                FALSE, 81u, 171u, 51u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG, CANTP_FUNCTIONAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_85a43433] */  /* [CanTpTxNSdu_85a43433, 65535] */
  { /*    26 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_A2_F4_Cab_Tp_oCabSubnet_fc611706_Tx       , PduRConf_PduRDestPdu_CIOM_1a4fac9a_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_b73de2d0, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_b73de2d0,                FALSE, 81u, 171u, 51u,                            25u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_90b4ee1f] */  /* [CanTpTxNSdu_90b4ee1f, 28] */
  { /*    27 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_53_F2_Cab_Tp_oCabSubnet_9abe6f67_Tx          , PduRConf_PduRDestPdu_CIOM_a610cc5c_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_14ebb75e, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_14ebb75e,                FALSE, 81u, 171u, 51u,                            26u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_97d435bf] */  /* [CanTpTxNSdu_97d435bf, 41] */
  { /*    28 */ CanIfConf_CanIfTxPduCfg_CIOM_Cab_29S_Tp_oCabSubnet_e3dce9b1_Tx                      , PduRConf_PduRDestPdu_CIOM_Cab_29S_oCabSubnet_793b1ffc_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_4ce4798a, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_4ce4798a,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_072ad05f] */  /* [CanTpTxNSdu_072ad05f, 62] */
  { /*    29 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_A0_BB2_Tp_oBackbone2_d77a4b8a_Tx         , PduRConf_PduRDestPdu_CIOM_d0c6636c_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_6e9d45e7, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_6e9d45e7,                FALSE, 81u, 171u, 51u,                            28u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_155b67c1] */  /* [CanTpTxNSdu_155b67c1, 35] */
  { /*    30 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_A1_F2_Sec_Tp_oSecuritySubnet_5d2e792a_Tx     , PduRConf_PduRDestPdu_CIOM_325e35fa_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_14ea7b0b, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_14ea7b0b,                FALSE, 81u, 171u, 51u,                            29u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_160b460d] */  /* [CanTpTxNSdu_160b460d, 44] */
  { /*    31 */ CanIfConf_CanIfTxPduCfg_IntTesterTGW2FuncDiagMsg_Cab_Tp_oCabSubnet_96ec8334_Tx      , PduRConf_PduRDestPdu_CIOM_52939f57_Tx                                     , CANTP_INVALID_HDL                             , CanTpConf_CanTpTxNPdu_CanTpTxNPdu_f3f911f3,                FALSE, 81u, 171u, 51u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG, CANTP_FUNCTIONAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_338fdf39] */  /* [CanTpTxNSdu_338fdf39, 65535] */
  { /*    32 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_A0_BB2_Tp_oBackbone2_7d9615e1_Tx      , PduRConf_PduRDestPdu_CIOM_3fb186a1_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_3a6fb7af, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_3a6fb7af,                FALSE, 81u, 171u, 51u,                            30u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_374c2f98] */  /* [CanTpTxNSdu_374c2f98, 12] */
  { /*    33 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_C0_BB2_Tp_oBackbone2_9924f26b_Tx    , PduRConf_PduRDestPdu_CIOM_6e041ac5_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_72406594, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_72406594,                FALSE, 81u, 171u, 51u,                            31u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_420eda30] */  /* [CanTpTxNSdu_420eda30, 7] */
  { /*    34 */ CanIfConf_CanIfTxPduCfg_CIOM_Sec_09S_Tp_oSecuritySubnet_ba09ccac_Tx                 , PduRConf_PduRDestPdu_CIOM_Sec_09S_oSecuritySubnet_9aeecef3_Tx             , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_16d15085, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_16d15085,                FALSE,  3u,   5u,  2u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_421e4ec4] */  /* [CanTpTxNSdu_421e4ec4, 69] */
  { /*    35 */ CanIfConf_CanIfTxPduCfg_CIOM_BB2_13S_Tp_oBackbone2_bd99285a_Tx                      , PduRConf_PduRDestPdu_CIOM_BB2_13S_oBackbone2_0fe85b2c_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_0cd94d72, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_0cd94d72,                 TRUE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_638f93bd] */  /* [CanTpTxNSdu_638f93bd, 53] */
  { /*    36 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_A0_F4_Sec_Tp_oSecuritySubnet_8adef0bc_Tx  , PduRConf_PduRDestPdu_CIOM_5d61a178_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_20d338ab, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_20d338ab,                FALSE, 81u, 171u, 51u,                            34u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_917ba177] */  /* [CanTpTxNSdu_917ba177, 26] */
  { /*    37 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_26_BB2_Tp_oBackbone2_aba3af99_Tx    , PduRConf_PduRDestPdu_CIOM_fcf69174_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_16bfd7e5, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_16bfd7e5,                FALSE, 81u, 171u, 51u,                            36u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_1047cc45] */  /* [CanTpTxNSdu_1047cc45, 0] */
  { /*    38 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_98_BB2_Tp_oBackbone2_0c1730ea_Tx         , PduRConf_PduRDestPdu_CIOM_a1765610_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_2e22b0e3, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_2e22b0e3,                FALSE, 81u, 171u, 51u,                            38u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_8729f19d] */  /* [CanTpTxNSdu_8729f19d, 34] */
  { /*    39 */ CanIfConf_CanIfTxPduCfg_CIOM_Cab_05S_Tp_oCabSubnet_e74407ff_Tx                      , PduRConf_PduRDestPdu_CIOM_Cab_05S_oCabSubnet_86401064_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_8b06b311, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_8b06b311,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_67565e28] */  /* [CanTpTxNSdu_67565e28, 57] */
  { /*    40 */ CanIfConf_CanIfTxPduCfg_TesterFuncDiagMsg_Sec_Tp_oSecuritySubnet_7f5a7347_Tx        , PduRConf_PduRDestPdu_CIOM_305ca23a_Tx                                     , CANTP_INVALID_HDL                             , CanTpConf_CanTpTxNPdu_CanTpTxNPdu_0fcb0b72,                FALSE, 81u, 171u, 51u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG, CANTP_FUNCTIONAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_81649aaa] */  /* [CanTpTxNSdu_81649aaa, 65535] */
  { /*    41 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_A1_F4_Sec_Tp_oSecuritySubnet_eb89cc56_Tx  , PduRConf_PduRDestPdu_CIOM_60d04da4_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_0a210417, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_0a210417,                FALSE, 81u, 171u, 51u,                            40u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_2609472a] */  /* [CanTpTxNSdu_2609472a, 27] */
  { /*    42 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_26_F3_Cab_Tp_oCabSubnet_2e1f9ada_Tx     , PduRConf_PduRDestPdu_CIOM_f03d0c81_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_05536183, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_05536183,                FALSE, 81u, 171u, 51u,                            42u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_8388957a] */  /* [CanTpTxNSdu_8388957a, 16] */
  { /*    43 */ CanIfConf_CanIfTxPduCfg_CIOM_Sec_12S_Tp_oSecuritySubnet_2d9a0e12_Tx                 , PduRConf_PduRDestPdu_CIOM_Sec_12S_oSecuritySubnet_ec5d87cc_Tx             , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_426219dc, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_426219dc,                FALSE,  3u,   5u,  2u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_40316546] */  /* [CanTpTxNSdu_40316546, 72] */
  { /*    44 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_26_F4_Cab_Tp_oCabSubnet_b42402a4_Tx       , PduRConf_PduRDestPdu_CIOM_08650048_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_52d3aa3f, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_52d3aa3f,                FALSE, 81u, 171u, 51u,                            44u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_93056798] */  /* [CanTpTxNSdu_93056798, 23] */
  { /*    45 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_26_F2_Cab_Tp_oCabSubnet_e2a6386f_Tx          , PduRConf_PduRDestPdu_CIOM_42e6f8eb_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_e51b1aae, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_e51b1aae,                FALSE, 81u, 171u, 51u,                            46u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_a23f8213] */  /* [CanTpTxNSdu_a23f8213, 40] */
  { /*    46 */ CanIfConf_CanIfTxPduCfg_CIOM_Cab_30S_Tp_oCabSubnet_da4b67c4_Tx                      , PduRConf_PduRDestPdu_CIOM_Cab_30S_oCabSubnet_5d07bef1_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_2679860a, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_2679860a,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_ac62b136] */  /* [CanTpTxNSdu_ac62b136, 63] */
  { /*    47 */ CanIfConf_CanIfTxPduCfg_CIOM_BB2_12S_Tp_oBackbone2_b9170acc_Tx                      , PduRConf_PduRDestPdu_CIOM_BB2_12S_oBackbone2_fe325e86_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_41bdbd59, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_41bdbd59,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_b27caf41] */  /* [CanTpTxNSdu_b27caf41, 52] */
  { /*    48 */ CanIfConf_CanIfTxPduCfg_CIOM_Sec_07S_Tp_oSecuritySubnet_69506b1a_Tx                 , PduRConf_PduRDestPdu_CIOM_Sec_07S_oSecuritySubnet_e302eca2_Tx             , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_ae8e313f, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_ae8e313f,                FALSE,  3u,   5u,  2u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_b77300d6] */  /* [CanTpTxNSdu_b77300d6, 67] */
  { /*    49 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_C0_BB2_Tp_oBackbone2_ae5b54dc_Tx         , PduRConf_PduRDestPdu_CIOM_2156cc07_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_7cc5ba27, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_7cc5ba27,                FALSE, 81u, 171u, 51u,                            49u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_b397195b] */  /* [CanTpTxNSdu_b397195b, 38] */
    /* Index    LoLayerTxPduId                                                                        PduRTxSduId                                                                 RxFcPduId                                       TxPduConfirmationPduId                      TransmitCancellation  NAs  NBs   NCs  RxSduCfgIdx                     TxTaType                                   Comment                            Referable Keys */
  { /*    50 */ CanIfConf_CanIfTxPduCfg_CIOM_BB2_30S_Tp_oBackbone2_82da3f46_Tx                      , PduRConf_PduRDestPdu_CIOM_BB2_30S_oBackbone2_ab127275_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_2242705f, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_2242705f,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_bb7fa63e] */  /* [CanTpTxNSdu_bb7fa63e, 55] */
  { /*    51 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_98_BB2_Tp_oBackbone2_8516ddd7_Tx      , PduRConf_PduRDestPdu_CIOM_4e01b3dd_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_4c1bc602, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_4c1bc602,                FALSE, 81u, 171u, 51u,                            50u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_bc085821] */  /* [CanTpTxNSdu_bc085821, 11] */
  { /*    52 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_26_BB2_Tp_oBackbone2_ebb70ff2_Tx         , PduRConf_PduRDestPdu_CIOM_b3a447b6_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_f7837c7d, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_f7837c7d,                FALSE, 81u, 171u, 51u,                            52u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_bf49619c] */  /* [CanTpTxNSdu_bf49619c, 30] */
  { /*    53 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_A1_BB2_Tp_oBackbone2_bca8fbcc_Tx      , PduRConf_PduRDestPdu_CIOM_f02f9169_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_cb71089b, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_cb71089b,                FALSE, 81u, 171u, 51u,                            54u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_c8d90e4f] */  /* [CanTpTxNSdu_c8d90e4f, 13] */
  { /*    54 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_98_F3_Cab_Tp_oCabSubnet_2daafb4e_Tx     , PduRConf_PduRDestPdu_CIOM_16ccbda9_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_b93ef70d, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_b93ef70d,                FALSE, 81u, 171u, 51u,                            56u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_c66f942c] */  /* [CanTpTxNSdu_c66f942c, 18] */
  { /*    55 */ CanIfConf_CanIfTxPduCfg_CIOM_BB2_22S_Tp_oBackbone2_92aec239_Tx                      , PduRConf_PduRDestPdu_CIOM_BB2_22S_oBackbone2_a525ef93_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_cf81800e, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_cf81800e,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_c90c57b6] */  /* [CanTpTxNSdu_c90c57b6, 54] */
  { /*    56 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_A1_BB2_Tp_oBackbone2_441131ae_Tx         , PduRConf_PduRDestPdu_CIOM_1f5874a4_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_ae8fba73, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_ae8fba73,                FALSE, 81u, 171u, 51u,                            57u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_c046cd74] */  /* [CanTpTxNSdu_c046cd74, 36] */
  { /*    57 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_40_BB2_Tp_oBackbone2_7b130fa6_Tx    , PduRConf_PduRDestPdu_DiagRespMsgIntHMIIOM_F3_40_BB2_oBackbone2_f7679e64_Tx, CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_29e880d4, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_29e880d4,                FALSE, 81u, 171u, 51u,                            58u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_c8855cd9] */  /* [CanTpTxNSdu_c8855cd9, 1] */
  { /*    58 */ CanIfConf_CanIfTxPduCfg_CIOM_Sec_05S_Tp_oSecuritySubnet_c8b553a6_Tx                 , PduRConf_PduRDestPdu_CIOM_Sec_05S_oSecuritySubnet_7a00e33f_Tx             , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_cc0bb21d, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_cc0bb21d,                FALSE,  3u,   5u,  2u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_cfd2742b] */  /* [CanTpTxNSdu_cfd2742b, 65] */
  { /*    59 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_C0_F2_Sec_Tp_oSecuritySubnet_f27248ff_Tx     , PduRConf_PduRDestPdu_CIOM_ca2e8141_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_a2acba40, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_a2acba40,                FALSE, 81u, 171u, 51u,                            59u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_d7af4a84] */  /* [CanTpTxNSdu_d7af4a84, 46] */
  { /*    60 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_A2_BB2_Tp_oBackbone2_d6b4927e_Tx    , PduRConf_PduRDestPdu_CIOM_dbd99c7f_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_a8433250, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_a8433250,                FALSE, 81u, 171u, 51u,                            60u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_d137db99] */  /* [CanTpTxNSdu_d137db99, 6] */
  { /*    61 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_53_BB2_Tp_oBackbone2_280e15fa_Tx      , PduRConf_PduRDestPdu_CIOM_4767c578_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_67618176, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_67618176,                FALSE, 81u, 171u, 51u,                            61u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_dbf2493d] */  /* [CanTpTxNSdu_dbf2493d, 10] */
  { /*    62 */ CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_98_F2_Cab_Tp_oCabSubnet_4c51814f_Tx          , PduRConf_PduRDestPdu_CIOM_a41749c3_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_79805470, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_79805470,                FALSE, 81u, 171u, 51u,                            62u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_de4f6c03] */  /* [CanTpTxNSdu_de4f6c03, 42] */
  { /*    63 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_C0_BB2_Tp_oBackbone2_f7aad0d7_Tx      , PduRConf_PduRDestPdu_CIOM_ce2129ca_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_779a5c21, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_779a5c21,                FALSE, 81u, 171u, 51u,                            65u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_e128ac5e] */  /* [CanTpTxNSdu_e128ac5e, 15] */
  { /*    64 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_98_BB2_Tp_oBackbone2_73235a33_Tx    , PduRConf_PduRDestPdu_CIOM_ee2480d2_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_9b5bb09d, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_9b5bb09d,                FALSE, 81u, 171u, 51u,                            66u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_f0ca165a] */  /* [CanTpTxNSdu_f0ca165a, 3] */
  { /*    65 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_A1_F3_Sec_Tp_oSecuritySubnet_a1b03b66_Tx, PduRConf_PduRDestPdu_CIOM_d31c0716_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_5e62d968, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_5e62d968,                FALSE, 81u, 171u, 51u,                            67u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_f0daeab1] */  /* [CanTpTxNSdu_f0daeab1, 20] */
  { /*    66 */ CanIfConf_CanIfTxPduCfg_CIOM_Cab_20S_Tp_oCabSubnet_c323df97_Tx                      , PduRConf_PduRDestPdu_CIOM_Cab_20S_oCabSubnet_6bf52e02_Tx                  , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_51ea11e5, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_51ea11e5,                FALSE,  6u,  16u,  6u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_f3c4bb5a] */  /* [CanTpTxNSdu_f3c4bb5a, 60] */
  { /*    67 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_A0_F3_Sec_Tp_oSecuritySubnet_09265670_Tx, PduRConf_PduRDestPdu_CIOM_eeadebca_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_46dc57a6, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_46dc57a6,                FALSE, 81u, 171u, 51u,                            68u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_f5b24442] */  /* [CanTpTxNSdu_f5b24442, 19] */
  { /*    68 */ CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_A2_BB2_Tp_oBackbone2_2addb983_Tx         , PduRConf_PduRDestPdu_CIOM_948b4abd_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_3dcbb367, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_3dcbb367,                FALSE, 81u, 171u, 51u,                            69u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_f5e91b1b] */  /* [CanTpTxNSdu_f5e91b1b, 37] */
  { /*    69 */ CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_A2_BB2_Tp_oBackbone2_249acffa_Tx      , PduRConf_PduRDestPdu_CIOM_7bfcaf70_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_9d493ab9, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_9d493ab9,                FALSE, 81u, 171u, 51u,                            70u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_f7a6d049] */  /* [CanTpTxNSdu_f7a6d049, 14] */
  { /*    70 */ CanIfConf_CanIfTxPduCfg_CIOM_Sec_10S_Tp_oSecuritySubnet_8c7f36ae_Tx                 , PduRConf_PduRDestPdu_CIOM_Sec_10S_oSecuritySubnet_755f8851_Tx             , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_6def313d, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_6def313d,                FALSE,  3u,   5u,  2u, CANTP_NO_RXSDUCFGIDXOFTXSDUCFG,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG },  /* [CanTpTxNSdu_f8c8b245] */  /* [CanTpTxNSdu_f8c8b245, 70] */
  { /*    71 */ CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_53_F4_Cab_Tp_oCabSubnet_75ef26c1_Tx       , PduRConf_PduRDestPdu_CIOM_ec9334ff_Tx                                     , CanTpConf_CanTpRxFcNPdu_CanTpRxFcNPdu_e26c1fa4, CanTpConf_CanTpTxNPdu_CanTpTxNPdu_e26c1fa4,                FALSE, 81u, 171u, 51u,                            74u,   CANTP_PHYSICAL_TXTATYPEOFTXSDUCFG }   /* [CanTpTxNSdu_ff43a8b1] */  /* [CanTpTxNSdu_ff43a8b1, 24] */
};
#define CANTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_TxSduCfgInd
**********************************************************************************************************************/
/** 
  \var    CanTp_TxSduCfgInd
  \brief  the indexes of the 1:1 sorted relation pointing to CanTp_TxSduCfg
*/ 
#define CANTP_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTp_TxSduCfgIndType, CANTP_CONST) CanTp_TxSduCfgInd[68] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     TxSduCfgInd      Referable Keys */
  /*     0 */          37u,  /* [0] */
  /*     1 */          57u,  /* [1] */
  /*     2 */          17u,  /* [2] */
  /*     3 */          64u,  /* [3] */
  /*     4 */           3u,  /* [4] */
  /*     5 */          24u,  /* [5] */
  /*     6 */          60u,  /* [6] */
  /*     7 */          33u,  /* [7] */
  /*     8 */          18u,  /* [8] */
  /*     9 */          22u,  /* [9] */
  /*    10 */          61u,  /* [10] */
  /*    11 */          51u,  /* [11] */
  /*    12 */          32u,  /* [12] */
  /*    13 */          53u,  /* [13] */
  /*    14 */          69u,  /* [14] */
  /*    15 */          63u,  /* [15] */
  /*    16 */          42u,  /* [16] */
  /*    17 */           0u,  /* [17] */
  /*    18 */          54u,  /* [18] */
  /*    19 */          67u,  /* [19] */
  /*    20 */          65u,  /* [20] */
  /*    21 */           8u,  /* [21] */
  /*    22 */          21u,  /* [22] */
  /*    23 */          44u,  /* [23] */
  /*    24 */          71u,  /* [24] */
  /*    25 */           4u,  /* [25] */
  /*    26 */          36u,  /* [26] */
  /*    27 */          41u,  /* [27] */
  /*    28 */          26u,  /* [28] */
  /*    29 */          11u,  /* [29] */
  /*    30 */          52u,  /* [30] */
  /*    31 */          13u,  /* [31] */
  /*    32 */          10u,  /* [32] */
  /*    33 */          19u,  /* [33] */
  /*    34 */          38u,  /* [34] */
  /*    35 */          29u,  /* [35] */
  /*    36 */          56u,  /* [36] */
  /*    37 */          68u,  /* [37] */
  /*    38 */          49u,  /* [38] */
  /*    39 */          23u,  /* [39] */
  /*    40 */          45u,  /* [40] */
  /*    41 */          27u,  /* [41] */
  /*    42 */          62u,  /* [42] */
  /*    43 */          16u,  /* [43] */
  /*    44 */          30u,  /* [44] */
  /*    45 */          12u,  /* [45] */
  /*    46 */          59u,  /* [46] */
  /*    47 */          47u,  /* [52] */
  /*    48 */          35u,  /* [53] */
  /*    49 */          55u,  /* [54] */
  /* Index     TxSduCfgInd      Referable Keys */
  /*    50 */          50u,  /* [55] */
  /*    51 */           1u,  /* [56] */
  /*    52 */          39u,  /* [57] */
  /*    53 */           7u,  /* [58] */
  /*    54 */           5u,  /* [59] */
  /*    55 */          66u,  /* [60] */
  /*    56 */          15u,  /* [61] */
  /*    57 */          28u,  /* [62] */
  /*    58 */          46u,  /* [63] */
  /*    59 */          20u,  /* [64] */
  /*    60 */          58u,  /* [65] */
  /*    61 */           6u,  /* [66] */
  /*    62 */          48u,  /* [67] */
  /*    63 */          14u,  /* [68] */
  /*    64 */          34u,  /* [69] */
  /*    65 */          70u,  /* [70] */
  /*    66 */           9u,  /* [71] */
  /*    67 */          43u   /* [72] */
};
#define CANTP_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_TxSduSnv2Hdl
**********************************************************************************************************************/
/** 
  \var    CanTp_TxSduSnv2Hdl
  \details
  Element        Description
  TxSduCfgIdx    the index of the 0:1 relation pointing to CanTp_TxSduCfg
*/ 
#define CANTP_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(CanTp_TxSduSnv2HdlType, CANTP_CONST) CanTp_TxSduSnv2Hdl[72] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    TxSduCfgIdx        Comment */
  { /*     0 */          0u },  /* [CanTpTxNSdu_0b4e5ed3] */
  { /*     1 */          1u },  /* [CanTpTxNSdu_0b28b3c4] */
  { /*     2 */          2u },  /* [CanTpTxNSdu_0f9a4f9c] */
  { /*     3 */          3u },  /* [CanTpTxNSdu_0fc83382] */
  { /*     4 */          4u },  /* [CanTpTxNSdu_1a4aae9e] */
  { /*     5 */          5u },  /* [CanTpTxNSdu_2b6a0a70] */
  { /*     6 */          6u },  /* [CanTpTxNSdu_2c8e4400] */
  { /*     7 */          7u },  /* [CanTpTxNSdu_2c8eef3f] */
  { /*     8 */          8u },  /* [CanTpTxNSdu_2ed2671b] */
  { /*     9 */          9u },  /* [CanTpTxNSdu_4c161f1d] */
  { /*    10 */         10u },  /* [CanTpTxNSdu_6b50c215] */
  { /*    11 */         11u },  /* [CanTpTxNSdu_6eda9336] */
  { /*    12 */         12u },  /* [CanTpTxNSdu_9bc05ec2] */
  { /*    13 */         13u },  /* [CanTpTxNSdu_9fd60b61] */
  { /*    14 */         14u },  /* [CanTpTxNSdu_9fe90810] */
  { /*    15 */         15u },  /* [CanTpTxNSdu_05a0c60a] */
  { /*    16 */         16u },  /* [CanTpTxNSdu_05d82786] */
  { /*    17 */         17u },  /* [CanTpTxNSdu_15cc9c64] */
  { /*    18 */         18u },  /* [CanTpTxNSdu_23c20b77] */
  { /*    19 */         19u },  /* [CanTpTxNSdu_33cb3e9e] */
  { /*    20 */         20u },  /* [CanTpTxNSdu_39c7c1db] */
  { /*    21 */         21u },  /* [CanTpTxNSdu_66ceed3b] */
  { /*    22 */         22u },  /* [CanTpTxNSdu_67a44a47] */
  { /*    23 */         23u },  /* [CanTpTxNSdu_68a1d221] */
  { /*    24 */         24u },  /* [CanTpTxNSdu_72ac09b7] */
  { /*    25 */         25u },  /* [CanTpTxNSdu_85a43433] */
  { /*    26 */         26u },  /* [CanTpTxNSdu_90b4ee1f] */
  { /*    27 */         27u },  /* [CanTpTxNSdu_97d435bf] */
  { /*    28 */         28u },  /* [CanTpTxNSdu_072ad05f] */
  { /*    29 */         29u },  /* [CanTpTxNSdu_155b67c1] */
  { /*    30 */         30u },  /* [CanTpTxNSdu_160b460d] */
  { /*    31 */         31u },  /* [CanTpTxNSdu_338fdf39] */
  { /*    32 */         32u },  /* [CanTpTxNSdu_374c2f98] */
  { /*    33 */         33u },  /* [CanTpTxNSdu_420eda30] */
  { /*    34 */         34u },  /* [CanTpTxNSdu_421e4ec4] */
  { /*    35 */         35u },  /* [CanTpTxNSdu_638f93bd] */
  { /*    36 */         36u },  /* [CanTpTxNSdu_917ba177] */
  { /*    37 */         37u },  /* [CanTpTxNSdu_1047cc45] */
  { /*    38 */         38u },  /* [CanTpTxNSdu_8729f19d] */
  { /*    39 */         39u },  /* [CanTpTxNSdu_67565e28] */
  { /*    40 */         40u },  /* [CanTpTxNSdu_81649aaa] */
  { /*    41 */         41u },  /* [CanTpTxNSdu_2609472a] */
  { /*    42 */         42u },  /* [CanTpTxNSdu_8388957a] */
  { /*    43 */         43u },  /* [CanTpTxNSdu_40316546] */
  { /*    44 */         44u },  /* [CanTpTxNSdu_93056798] */
  { /*    45 */         45u },  /* [CanTpTxNSdu_a23f8213] */
  { /*    46 */         46u },  /* [CanTpTxNSdu_ac62b136] */
  { /*    47 */         47u },  /* [CanTpTxNSdu_b27caf41] */
  { /*    48 */         48u },  /* [CanTpTxNSdu_b77300d6] */
  { /*    49 */         49u },  /* [CanTpTxNSdu_b397195b] */
    /* Index    TxSduCfgIdx        Comment */
  { /*    50 */         50u },  /* [CanTpTxNSdu_bb7fa63e] */
  { /*    51 */         51u },  /* [CanTpTxNSdu_bc085821] */
  { /*    52 */         52u },  /* [CanTpTxNSdu_bf49619c] */
  { /*    53 */         53u },  /* [CanTpTxNSdu_c8d90e4f] */
  { /*    54 */         54u },  /* [CanTpTxNSdu_c66f942c] */
  { /*    55 */         55u },  /* [CanTpTxNSdu_c90c57b6] */
  { /*    56 */         56u },  /* [CanTpTxNSdu_c046cd74] */
  { /*    57 */         57u },  /* [CanTpTxNSdu_c8855cd9] */
  { /*    58 */         58u },  /* [CanTpTxNSdu_cfd2742b] */
  { /*    59 */         59u },  /* [CanTpTxNSdu_d7af4a84] */
  { /*    60 */         60u },  /* [CanTpTxNSdu_d137db99] */
  { /*    61 */         61u },  /* [CanTpTxNSdu_dbf2493d] */
  { /*    62 */         62u },  /* [CanTpTxNSdu_de4f6c03] */
  { /*    63 */         63u },  /* [CanTpTxNSdu_e128ac5e] */
  { /*    64 */         64u },  /* [CanTpTxNSdu_f0ca165a] */
  { /*    65 */         65u },  /* [CanTpTxNSdu_f0daeab1] */
  { /*    66 */         66u },  /* [CanTpTxNSdu_f3c4bb5a] */
  { /*    67 */         67u },  /* [CanTpTxNSdu_f5b24442] */
  { /*    68 */         68u },  /* [CanTpTxNSdu_f5e91b1b] */
  { /*    69 */         69u },  /* [CanTpTxNSdu_f7a6d049] */
  { /*    70 */         70u },  /* [CanTpTxNSdu_f8c8b245] */
  { /*    71 */         71u }   /* [CanTpTxNSdu_ff43a8b1] */
};
#define CANTP_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_CalcBS
**********************************************************************************************************************/
#define CANTP_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanTp_CalcBSType, CANTP_VAR_NOINIT) CanTp_CalcBS[75];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CANTP_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_RxState
**********************************************************************************************************************/
#define CANTP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanTp_RxStateType, CANTP_VAR_NOINIT) CanTp_RxState[75];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CANTP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_TxSemaphores
**********************************************************************************************************************/
#define CANTP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanTp_TxSemaphoreType, CANTP_VAR_NOINIT) CanTp_TxSemaphores[98];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CANTP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTp_TxState
**********************************************************************************************************************/
#define CANTP_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanTp_TxStateType, CANTP_VAR_NOINIT) CanTp_TxState[72];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define CANTP_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
 * FUNCTIONS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/




