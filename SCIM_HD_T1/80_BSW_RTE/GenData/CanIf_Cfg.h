/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: CanIf
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: CanIf_Cfg.h
 *   Generation Time: 2020-11-11 14:25:33
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * WARNING: This code has been generated with reduced-severity errors. 
 * The created output files contain errors that have been ignored. Usage of the created files can lead to unpredictable behavior of the embedded code.
 * Usage of the created files happens at own risk!
 * 
 * [Warning] J1939NM01021 - J1939 network addresses mismatch 
 * - [Reduced Severity due to User-Defined Parameter] The CanIf Tx Pdu contains a source address (value=0x0F) that is not configured for any J1939Nm node.
 * Erroneous configuration elements:
 * /ActiveEcuC/J1939Nm/J1939NmConfigSet/FMSNet_J1939_44d89c3b (DefRef: /MICROSAR/J1939Nm/J1939NmConfigSet/J1939NmChannel)
 * /ActiveEcuC/CanIf/CanIfInitCfg/ERC1_x_EMSRetFMS_oFMSNet_d863a207_Tx[0:CanIfTxPduCanId](value=418381839) (DefRef: /MICROSAR/CanIf/CanIfInitCfg/CanIfTxPduCfg/CanIfTxPduCanId)
 * /ActiveEcuC/J1939Nm/J1939NmConfigSet/CIOM_4d5cd289[0:J1939NmNodePreferredAddress](value=49) (DefRef: /MICROSAR/J1939Nm/J1939NmConfigSet/J1939NmNode/J1939NmNodePreferredAddress)
 * 
 * [Warning] J1939NM01021 - J1939 network addresses mismatch 
 * - [Reduced Severity due to User-Defined Parameter] The CanIf Tx Pdu contains a source address (value=0x10) that is not configured for any J1939Nm node.
 * Erroneous configuration elements:
 * /ActiveEcuC/J1939Nm/J1939NmConfigSet/FMSNet_J1939_44d89c3b (DefRef: /MICROSAR/J1939Nm/J1939NmConfigSet/J1939NmChannel)
 * /ActiveEcuC/CanIf/CanIfInitCfg/ERC1_x_RECUFMS_oFMSNet_338e7918_Tx[0:CanIfTxPduCanId](value=418381840) (DefRef: /MICROSAR/CanIf/CanIfInitCfg/CanIfTxPduCfg/CanIfTxPduCanId)
 * /ActiveEcuC/J1939Nm/J1939NmConfigSet/CIOM_4d5cd289[0:J1939NmNodePreferredAddress](value=49) (DefRef: /MICROSAR/J1939Nm/J1939NmConfigSet/J1939NmNode/J1939NmNodePreferredAddress)
 *********************************************************************************************************************/
#if !defined(CANIF_CFG_H)
#define CANIF_CFG_H

/**********************************************************************************************************************
  \file  Includes
**********************************************************************************************************************/
/** 
  \brief  Required external files.
*/
#include "Can.h"




/**********************************************************************************************************************
  \def  Version defines
**********************************************************************************************************************/

#define CANIF_CFG5_VERSION                                 0x0411u
#define CANIF_CFG5_RELEASE_VERSION                         0x01u
#define IF_ASRIFCAN_GENTOOL_CFG5_MAJOR_VERSION             0x04u
#define IF_ASRIFCAN_GENTOOL_CFG5_MINOR_VERSION             0x11u
#define IF_ASRIFCAN_GENTOOL_CFG5_PATCH_VERSION             0x01u

#define CANIF_CFG5_GENERATOR_COMPATIBILITY_VERSION         0x0212u

/**********************************************************************************************************************
  \def  Switches
**********************************************************************************************************************/

#define CANIF_TRANSMIT_BUFFER                              STD_ON /* Signals if Tx-buffer is enabled at all in CanIf. The value is determined from parameter "CanIfPublicTxBuffering". */
#define CANIF_TRANSMIT_BUFFER_PRIO_BY_CANID                STD_ON
#define CANIF_TRANSMIT_BUFFER_FIFO                         STD_OFF
#define CANIF_BITQUEUE                                     STD_OFF
#define CANIF_STATIC_FD_TXQUEUE                            STD_OFF
#define CANIF_WAKEUP_SUPPORT                               STD_OFF
#define CANIF_WAKEUP_VALIDATION                            STD_OFF
#define CANIF_WAKEUP_VALID_ALL_RX_MSGS                     STD_OFF
#define CANIF_WAKEUP_VALID_ONLY_NM_RX_MSGS                 STD_OFF
#define CANIF_DEV_ERROR_DETECT                             STD_OFF
#define CANIF_DEV_ERROR_REPORT                             STD_OFF
#define CANIF_TRANSMIT_CANCELLATION                        STD_OFF
#define CANIF_CANCEL_SUPPORT_API                           STD_ON
#define CANIF_VERSION_INFO_API                             STD_OFF
#define CANIF_DLC_CHECK                                    STD_ON
#define CANIF_SUPPORT_NMOSEK_INDICATION                    STD_OFF
#define CANIF_TRCV_HANDLING                                STD_ON
#define CANIF_TRCV_MAPPING                                 STD_OFF
#define CANIF_PN_TRCV_HANDLING                             STD_OFF
#define CANIF_EXTENDEDID_SUPPORT                           STD_ON
#define CANIF_ONE_CONTROLLER_OPTIMIZATION                  STD_OFF
#define CANIF_SETDYNAMICTXID_API                           STD_OFF
#define CANIF_PN_WU_TX_PDU_FILTER                          STD_OFF
#define CANIF_PUBLIC_TX_CONFIRM_POLLING_SUPPORT            STD_OFF
#define CANIF_RX_INDICATION_TYPE_I_IS_USED                 STD_OFF
#define CANIF_RX_INDICATION_TYPE_IV_IS_USED                STD_OFF
#define CANIF_CHANGE_BAUDRATE_SUPPORT                      STD_OFF
#define CANIF_SET_BAUDRATE_API                             STD_OFF
#define CANIF_META_DATA_RX_SUPPORT                         STD_ON
#define CANIF_META_DATA_TX_SUPPORT                         STD_ON
#define CANIF_J1939_DYN_ADDR_SUPPORT                       CANIF_J1939_DYN_ADDR_DISABLED
#define CANIF_MULTIPLE_CANDRV_SUPPORT                      STD_OFF
#define CANIF_RX_SEARCH_CONSIDER_MSG_TYPE                  STD_OFF
#define CANIF_ECUC_SAFE_BSW_CHECKS                         STD_OFF
#define CANIF_EXTENDED_RAM_CHECK_SUPPORT                   STD_OFF
#define CANIF_DATA_CHECKSUM_RX_SUPPORT                     STD_OFF
#define CANIF_DATA_CHECKSUM_TX_SUPPORT                     STD_OFF
#define CANIF_SET_PDU_RECEPTION_MODE_SUPPORT               STD_OFF 
#define CANIF_BUS_MIRRORING_SUPPORT                        STD_OFF 

#define CANIF_SUPPRESS_EXTENDED_VERSION_CHECK

#ifndef CANIF_USE_DUMMY_STATEMENT
#define CANIF_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef CANIF_DUMMY_STATEMENT
#define CANIF_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CANIF_DUMMY_STATEMENT_CONST
#define CANIF_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CANIF_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define CANIF_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef CANIF_ATOMIC_VARIABLE_ACCESS
#define CANIF_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef CANIF_PROCESSOR_MPC5746C
#define CANIF_PROCESSOR_MPC5746C
#endif
#ifndef CANIF_COMP_DIAB
#define CANIF_COMP_DIAB
#endif
#ifndef CANIF_GEN_GENERATOR_MSR
#define CANIF_GEN_GENERATOR_MSR
#endif
#ifndef CANIF_CPUTYPE_BITORDER_MSB2LSB
#define CANIF_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef CANIF_CONFIGURATION_VARIANT_PRECOMPILE
#define CANIF_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef CANIF_CONFIGURATION_VARIANT_LINKTIME
#define CANIF_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef CANIF_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define CANIF_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef CANIF_CONFIGURATION_VARIANT
#define CANIF_CONFIGURATION_VARIANT CANIF_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef CANIF_POSTBUILD_VARIANT_SUPPORT
#define CANIF_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


#define CANIF_CPU_TYPE_SET_IN_ECUC_MODULE                  CPU_TYPE_32

/**********************************************************************************************************************
  \def  Search algorithm
**********************************************************************************************************************/
#define CANIF_SEARCH_ALGORITHM                             CANIF_LINEAR


/**********************************************************************************************************************
  \def  Configuration variant
**********************************************************************************************************************/
#define CANIF_CONFIG_VARIANT                               CANIF_CFGVAR_PRECOMPILETIME


/**********************************************************************************************************************
  Type definitions
**********************************************************************************************************************/

/**********************************************************************************************************************
  \def  Tx-buffer - queue size type
**********************************************************************************************************************/
typedef uint8 CanIf_TxBufferSizeType;


/**********************************************************************************************************************
  \file  Includes
**********************************************************************************************************************/
#include "CanIf_Types.h"

/**********************************************************************************************************************
  \def  Memory mapping keywords
**********************************************************************************************************************/
#define CANIF_XCFG                                         CANIF_CONST
#define CANIF_VAR_XCFG_NOINIT                              CANIF_VAR_NOINIT


/**********************************************************************************************************************
  \def  Invalid PDU handle
**********************************************************************************************************************/
#define CanIf_TxPduHnd_INVALID                             0xFFFFu
#define CanIf_RxPduHnd_INVALID                             0xFFFFu


/**********************************************************************************************************************
  \def  Tx-buffer - handling types
**********************************************************************************************************************/
#define CANIF_TXBUFFER_HANDLINGTYPE_PRIOBYCANID                                                             1u
#define CANIF_TXBUFFER_HANDLINGTYPE_FIFO                                                                    2u
#define CANIF_TXBUFFER_HANDLINGTYPE_NONE                                                                    3u



/**********************************************************************************************************************
  \def  CAN controller channels - symbolic handles
**********************************************************************************************************************/

#define CanIfConf_CanIfCtrlCfg_CT_Backbone1J1939_198bcf1c                                           0u
#define CanIfConf_CanIfCtrlCfg_CT_Backbone2_34cfe263                                                1u
#define CanIfConf_CanIfCtrlCfg_CT_CAN6_120de18e                                                     2u
#define CanIfConf_CanIfCtrlCfg_CT_CabSubnet_d2ff0fbe                                                3u
#define CanIfConf_CanIfCtrlCfg_CT_FMSNet_119a8706                                                   4u
#define CanIfConf_CanIfCtrlCfg_CT_SecuritySubnet_f5346ae6                                           5u


/**********************************************************************************************************************
  \def  CAN transceiver channels - symbolic handles
**********************************************************************************************************************/

#define CanIfConf_CanIfTrcvCfg_CanIfTrcvCfg_CAN2STB                                                 0u
#define CanIfConf_CanIfTrcvCfg_CanIfTrcvCfg_CAN3STB                                                 1u
#define CanIfConf_CanIfTrcvCfg_CanIfTrcvCfg_CAN4STB                                                 2u





/**********************************************************************************************************************
  \def  Transceiver handling APIs
**********************************************************************************************************************/
 
#define CanTrcv_SetOpMode                                  CanTrcv_30_GenericCan_SetOpMode
#define CanTrcv_GetOpMode                                  CanTrcv_30_GenericCan_GetOpMode
#define CanTrcv_GetBusWuReason                             CanTrcv_30_GenericCan_GetBusWuReason
#define CanTrcv_SetWakeupMode                              CanTrcv_30_GenericCan_SetWakeupMode
#define CanTrcv_CheckWakeup                                CanTrcv_30_GenericCan_CheckWakeup


/**********************************************************************************************************************
  \def  Tx PDU handles
**********************************************************************************************************************/

#define CanIfTxPduHnd_RQST_TACHO_CIOM_oBackbone1J1939_e082a6b2_Tx                                   0u
#define CanIfTxPduHnd_J1939NmTxPdu_fa509995                                                         1u
#define CanIfTxPduHnd_CIOM_BB1_01P_oBackbone1J1939_55a00301_Tx                                      2u
#define CanIfTxPduHnd_FcNPdu_Backbone1J1939_dba64907                                                3u
#define CanIfTxPduHnd_AckmTxPdu_Backbone1J1939_54966c1b                                             4u
#define CanIfTxPduHnd_DiagUUDTRespMsg1_F2_C0_BB2_oBackbone2_603e2338_Tx                             5u
#define CanIfTxPduHnd_DiagUUDTRespMsg1_F2_A2_BB2_oBackbone2_8af81a14_Tx                             6u
#define CanIfTxPduHnd_DiagUUDTRespMsg1_F2_A1_BB2_oBackbone2_26f22879_Tx                             7u
#define CanIfTxPduHnd_DiagUUDTRespMsg1_F2_A0_BB2_oBackbone2_42f439a2_Tx                             8u
#define CanIfTxPduHnd_DiagUUDTRespMsg1_F2_98_BB2_oBackbone2_5178e6a4_Tx                             9u
#define CanIfTxPduHnd_DiagUUDTRespMsg1_F2_53_BB2_oBackbone2_a51c09cf_Tx                             10u
#define CanIfTxPduHnd_DiagUUDTRespMsg1_F2_40_BB2_oBackbone2_187336ef_Tx                             11u
#define CanIfTxPduHnd_DiagUUDTRespMsg1_F2_26_BB2_oBackbone2_fc487bda_Tx                             12u
#define CanIfTxPduHnd_XCP_FA_40_BB2_oBackbone2_9cf6862c_Tx                                          13u
#define CanIfTxPduHnd_DiagRespMsgIntTGW2_F4_C0_BB2_Tp_oBackbone2_f7aad0d7_Tx                        14u
#define CanIfTxPduHnd_DiagRespMsgIntTGW2_F4_A2_BB2_Tp_oBackbone2_249acffa_Tx                        15u
#define CanIfTxPduHnd_DiagRespMsgIntTGW2_F4_A1_BB2_Tp_oBackbone2_bca8fbcc_Tx                        16u
#define CanIfTxPduHnd_DiagRespMsgIntTGW2_F4_A0_BB2_Tp_oBackbone2_7d9615e1_Tx                        17u
#define CanIfTxPduHnd_DiagRespMsgIntTGW2_F4_98_BB2_Tp_oBackbone2_8516ddd7_Tx                        18u
#define CanIfTxPduHnd_DiagRespMsgIntTGW2_F4_53_BB2_Tp_oBackbone2_280e15fa_Tx                        19u
#define CanIfTxPduHnd_DiagRespMsgIntTGW2_F4_40_BB2_Tp_oBackbone2_f5224357_Tx                        20u
#define CanIfTxPduHnd_DiagRespMsgIntTGW2_F4_26_BB2_Tp_oBackbone2_5b036461_Tx                        21u
#define CanIfTxPduHnd_DiagRespMsgIntHMIIOM_F3_C0_BB2_Tp_oBackbone2_9924f26b_Tx                      22u
#define CanIfTxPduHnd_DiagRespMsgIntHMIIOM_F3_A2_BB2_Tp_oBackbone2_d6b4927e_Tx                      23u
#define CanIfTxPduHnd_DiagRespMsgIntHMIIOM_F3_A1_BB2_Tp_oBackbone2_08540fc1_Tx                      24u
#define CanIfTxPduHnd_DiagRespMsgIntHMIIOM_F3_A0_BB2_Tp_oBackbone2_f4db796b_Tx                      25u
#define CanIfTxPduHnd_DiagRespMsgIntHMIIOM_F3_98_BB2_Tp_oBackbone2_73235a33_Tx                      26u
#define CanIfTxPduHnd_DiagRespMsgIntHMIIOM_F3_53_BB2_Tp_oBackbone2_930c5799_Tx                      27u
#define CanIfTxPduHnd_DiagRespMsgIntHMIIOM_F3_40_BB2_Tp_oBackbone2_7b130fa6_Tx                      28u
#define CanIfTxPduHnd_DiagRespMsgIntHMIIOM_F3_26_BB2_Tp_oBackbone2_aba3af99_Tx                      29u
#define CanIfTxPduHnd_PhysDiagRespMsg_F2_C0_BB2_Tp_oBackbone2_ae5b54dc_Tx                           30u
#define CanIfTxPduHnd_PhysDiagRespMsg_F2_A2_BB2_Tp_oBackbone2_2addb983_Tx                           31u
#define CanIfTxPduHnd_PhysDiagRespMsg_F2_A1_BB2_Tp_oBackbone2_441131ae_Tx                           32u
#define CanIfTxPduHnd_PhysDiagRespMsg_F2_A0_BB2_Tp_oBackbone2_d77a4b8a_Tx                           33u
#define CanIfTxPduHnd_PhysDiagRespMsg_F2_98_BB2_Tp_oBackbone2_0c1730ea_Tx                           34u
#define CanIfTxPduHnd_PhysDiagRespMsg_F2_53_BB2_Tp_oBackbone2_ef1139d4_Tx                           35u
#define CanIfTxPduHnd_PhysDiagRespMsg_F2_26_BB2_Tp_oBackbone2_ebb70ff2_Tx                           36u
#define CanIfTxPduHnd_NmAsr_CIOM_BB2_oBackbone2_9b0338b4_Tx                                         37u
#define CanIfTxPduHnd_DiagFaultStat_SRS_BB2_oBackbone2_e1ef7eee_Tx                                  38u
#define CanIfTxPduHnd_DiagFaultStat_LECM_BB2_oBackbone2_0cb60c21_Tx                                 39u
#define CanIfTxPduHnd_DiagFaultStat_CCM_BB2_oBackbone2_2f6e7814_Tx                                  40u
#define CanIfTxPduHnd_DiagFaultStat_WRCS_BB2_oBackbone2_dad83051_Tx                                 41u
#define CanIfTxPduHnd_DiagFaultStat_Alarm_BB2_oBackbone2_7b6d0a04_Tx                                42u
#define CanIfTxPduHnd_DiagFaultStat_PDM_BB2_oBackbone2_b720c987_Tx                                  43u
#define CanIfTxPduHnd_DiagFaultStat_DDM_BB2_oBackbone2_c5068234_Tx                                  44u
#define CanIfTxPduHnd_DiagFaultStat_CIOM_BB2_oBackbone2_04d7302e_Tx                                 45u
#define CanIfTxPduHnd_VMCU_BB2_57P_FCM_Tp_oBackbone2_65fb58e9_Tx                                    46u
#define CanIfTxPduHnd_HMIIOM_BB2_36S_FCM_Tp_oBackbone2_bf84bd48_Tx                                  47u
#define CanIfTxPduHnd_TECU_BB2_06S_FCM_Tp_oBackbone2_a56e8b10_Tx                                    48u
#define CanIfTxPduHnd_HMIIOM_BB2_27S_FCM_Tp_oBackbone2_16072c1e_Tx                                  49u
#define CanIfTxPduHnd_HMIIOM_BB2_19P_CIOM_FCM_Tp_oBackbone2_1a616940_Tx                             50u
#define CanIfTxPduHnd_VMCU_BB2_31S_FCM_Tp_oBackbone2_46c44850_Tx                                    51u
#define CanIfTxPduHnd_VMCU_BB2_32S_FCM_Tp_oBackbone2_8a7e4a00_Tx                                    52u
#define CanIfTxPduHnd_HMIIOM_BB2_20S_FCM_Tp_oBackbone2_2def59ba_Tx                                  53u
#define CanIfTxPduHnd_EMS_BB2_09S_FCM_Tp_oBackbone2_41cc37fa_Tx                                     54u
#define CanIfTxPduHnd_PhysDiagRespMsg_F1_53_BB2_Tp_oBackbone2_be0c70d1_Tx                           55u
#define CanIfTxPduHnd_BBM_BB2_03S_CIOM_FCM_Tp_oBackbone2_29c4ac8e_Tx                                56u
#define CanIfTxPduHnd_CIOM_BB2_21S_oBackbone2_140cc697_Tx                                           57u
#define CanIfTxPduHnd_CIOM_BB2_24S_oBackbone2_37b045d6_Tx                                           58u
#define CanIfTxPduHnd_CIOM_BB2_23S_oBackbone2_db8bc497_Tx                                           59u
#define CanIfTxPduHnd_CIOM_BB2_22S_Tp_oBackbone2_92aec239_Tx                                        60u
#define CanIfTxPduHnd_CIOM_BB2_20S_oBackbone2_73cf4797_Tx                                           61u
#define CanIfTxPduHnd_VMCU_BB2_34S_FCM_Tp_oBackbone2_c87b48e1_Tx                                    62u
#define CanIfTxPduHnd_TECU_BB2_05S_FCM_Tp_oBackbone2_69d48940_Tx                                    63u
#define CanIfTxPduHnd_HMIIOM_BB2_21S_FCM_Tp_oBackbone2_af5a2700_Tx                                  64u
#define CanIfTxPduHnd_CIOM_BB2_26S_oBackbone2_f83747d6_Tx                                           65u
#define CanIfTxPduHnd_EMS_BB2_11S_FCM_Tp_oBackbone2_6cd5cf95_Tx                                     66u
#define CanIfTxPduHnd_HMIIOM_BB2_35S_FCM_Tp_oBackbone2_e32a38c7_Tx                                  67u
#define CanIfTxPduHnd_HMIIOM_BB2_06S_FCM_Tp_oBackbone2_c2df8d7c_Tx                                  68u
#define CanIfTxPduHnd_CIOM_BB2_13S_Tp_oBackbone2_bd99285a_Tx                                        69u
#define CanIfTxPduHnd_CIOM_BB2_30S_Tp_oBackbone2_82da3f46_Tx                                        70u
#define CanIfTxPduHnd_CIOM_BB2_12S_Tp_oBackbone2_b9170acc_Tx                                        71u
#define CanIfTxPduHnd_HMIIOM_BB2_04S_FCM_Tp_oBackbone2_1cc47649_Tx                                  72u
#define CanIfTxPduHnd_CIOM_BB2_27S_oBackbone2_9ff4c6d6_Tx                                           73u
#define CanIfTxPduHnd_CIOM_BB2_25P_oBackbone2_732f917b_Tx                                           74u
#define CanIfTxPduHnd_PhysDiagRespMsg_F2_40_BB2_Tp_oBackbone2_bd4d3e52_Tx                           75u
#define CanIfTxPduHnd_Debug07_CIOM_BB2_oBackbone2_4ac5dfa9_Tx                                       76u
#define CanIfTxPduHnd_Debug06_CIOM_BB2_oBackbone2_25ede671_Tx                                       77u
#define CanIfTxPduHnd_Debug05_CIOM_BB2_oBackbone2_9495ac19_Tx                                       78u
#define CanIfTxPduHnd_Debug04_CIOM_BB2_oBackbone2_fbbd95c1_Tx                                       79u
#define CanIfTxPduHnd_Debug03_CIOM_BB2_oBackbone2_2d143e88_Tx                                       80u
#define CanIfTxPduHnd_Debug02_CIOM_BB2_oBackbone2_423c0750_Tx                                       81u
#define CanIfTxPduHnd_AnmMsg_CIOM_Backbone2_oBackbone2_d362d40e_Tx                                  82u
#define CanIfTxPduHnd_CIOM_BB2_28P_oBackbone2_d86d16b8_Tx                                           83u
#define CanIfTxPduHnd_CIOM_BB2_07P_oBackbone2_bc671479_Tx                                           84u
#define CanIfTxPduHnd_CIOM_BB2_11P_oBackbone2_37f8d7b9_Tx                                           85u
#define CanIfTxPduHnd_CIOM_BB2_10P_oBackbone2_503b56b9_Tx                                           86u
#define CanIfTxPduHnd_CIOM_BB2_06P_oBackbone2_dba49579_Tx                                           87u
#define CanIfTxPduHnd_CIOM_BB2_05P_oBackbone2_73e01679_Tx                                           88u
#define CanIfTxPduHnd_CIOM_BB2_29P_oBackbone2_bfae97b8_Tx                                           89u
#define CanIfTxPduHnd_CIOM_BB2_01P_oBackbone2_379f1438_Tx                                           90u
#define CanIfTxPduHnd_CIOM_BB2_03P_oBackbone2_f8181638_Tx                                           91u
#define CanIfTxPduHnd_CIOM_BB2_02P_oBackbone2_9fdb9738_Tx                                           92u
#define CanIfTxPduHnd_CIOM_BB2_04P_oBackbone2_14239779_Tx                                           93u
#define CanIfTxPduHnd_SCIM_BB2toCAN6_oCAN6_adf5f68f_Tx                                              94u
#define CanIfTxPduHnd_SCIM_BB1toCAN6_oCAN6_cb3fc040_Tx                                              95u
#define CanIfTxPduHnd_SCIM_LINtoCAN6_oCAN6_6b855022_Tx                                              96u
#define CanIfTxPduHnd_IntTesterTGW2FuncDiagMsg_Cab_Tp_oCabSubnet_96ec8334_Tx                        97u
#define CanIfTxPduHnd_TesterFuncDiagMsg_Cab_Tp_oCabSubnet_d293ecaa_Tx                               98u
#define CanIfTxPduHnd_DiagReqMsgIntTGW2_A2_F4_Cab_Tp_oCabSubnet_fc611706_Tx                         99u
#define CanIfTxPduHnd_DiagReqMsgIntHMIIOM_A2_F3_Cab_Tp_oCabSubnet_fcbc8d7d_Tx                       100u
#define CanIfTxPduHnd_PhysDiagReqMsg_A2_F2_Cab_Tp_oCabSubnet_6d74e481_Tx                            101u
#define CanIfTxPduHnd_DiagReqMsgIntTGW2_98_F4_Cab_Tp_oCabSubnet_2e8c3543_Tx                         102u
#define CanIfTxPduHnd_DiagReqMsgIntHMIIOM_98_F3_Cab_Tp_oCabSubnet_2daafb4e_Tx                       103u
#define CanIfTxPduHnd_PhysDiagReqMsg_98_F2_Cab_Tp_oCabSubnet_4c51814f_Tx                            104u
#define CanIfTxPduHnd_DiagReqMsgIntTGW2_53_F4_Cab_Tp_oCabSubnet_75ef26c1_Tx                         105u
#define CanIfTxPduHnd_DiagReqMsgIntHMIIOM_53_F3_Cab_Tp_oCabSubnet_e9059d48_Tx                       106u
#define CanIfTxPduHnd_PhysDiagReqMsg_53_F2_Cab_Tp_oCabSubnet_9abe6f67_Tx                            107u
#define CanIfTxPduHnd_DiagReqMsgIntTGW2_26_F4_Cab_Tp_oCabSubnet_b42402a4_Tx                         108u
#define CanIfTxPduHnd_DiagReqMsgIntHMIIOM_26_F3_Cab_Tp_oCabSubnet_2e1f9ada_Tx                       109u
#define CanIfTxPduHnd_PhysDiagReqMsg_26_F2_Cab_Tp_oCabSubnet_e2a6386f_Tx                            110u
#define CanIfTxPduHnd_NmAsr_CIOM_Cab_oCabSubnet_153d3c7d_Tx                                         111u
#define CanIfTxPduHnd_CIOM_Cab_21P_oCabSubnet_c1ce1ce8_Tx                                           112u
#define CanIfTxPduHnd_CIOM_Cab_20S_Tp_oCabSubnet_c323df97_Tx                                        113u
#define CanIfTxPduHnd_CIOM_Cab_19P_oCabSubnet_49985ce9_Tx                                           114u
#define CanIfTxPduHnd_CIOM_Cab_18P_oCabSubnet_2e5bdde9_Tx                                           115u
#define CanIfTxPduHnd_CIOM_Cab_17P_oCabSubnet_4a9e582a_Tx                                           116u
#define CanIfTxPduHnd_CIOM_Cab_16P_oCabSubnet_2d5dd92a_Tx                                           117u
#define CanIfTxPduHnd_CIOM_Cab_34P_Tp_oCabSubnet_377e6916_Tx                                        118u
#define CanIfTxPduHnd_CIOM_Cab_30S_Tp_oCabSubnet_da4b67c4_Tx                                        119u
#define CanIfTxPduHnd_CIOM_Cab_25S_Tp_oCabSubnet_d5957759_Tx                                        120u
#define CanIfTxPduHnd_CIOM_Cab_15P_oCabSubnet_85195a2a_Tx                                           121u
#define CanIfTxPduHnd_CIOM_Cab_14P_oCabSubnet_e2dadb2a_Tx                                           122u
#define CanIfTxPduHnd_CIOM_Cab_13S_Tp_oCabSubnet_e50870d8_Tx                                        123u
#define CanIfTxPduHnd_CIOM_Cab_12P_oCabSubnet_6922db6b_Tx                                           124u
#define CanIfTxPduHnd_CIOM_Cab_11S_Tp_oCabSubnet_ec1435f4_Tx                                        125u
#define CanIfTxPduHnd_PhysDiagReqMsg_53_F1_Cab_Tp_oCabSubnet_682570ce_Tx                            126u
#define CanIfTxPduHnd_CIOM_Cab_08P_oCabSubnet_2e3c1e68_Tx                                           127u
#define CanIfTxPduHnd_CIOM_Cab_07P_oCabSubnet_4af99bab_Tx                                           128u
#define CanIfTxPduHnd_CIOM_Cab_22P_oCabSubnet_698a9fe8_Tx                                           129u
#define CanIfTxPduHnd_CIOM_Cab_29S_Tp_oCabSubnet_e3dce9b1_Tx                                        130u
#define CanIfTxPduHnd_CIOM_Cab_05S_Tp_oCabSubnet_e74407ff_Tx                                        131u
#define CanIfTxPduHnd_CIOM_Cab_04S_Tp_oCabSubnet_e3ca2569_Tx                                        132u
#define CanIfTxPduHnd_CIOM_Cab_28S_oCabSubnet_0dafccc7_Tx                                           133u
#define CanIfTxPduHnd_CCM_Cab_03P_FCM_Tp_oCabSubnet_af75b436_Tx                                     134u
#define CanIfTxPduHnd_AnmMsg_CIOM_CabSubnet_oCabSubnet_e295c8bd_Tx                                  135u
#define CanIfTxPduHnd_CIOM_Cab_31P_oCabSubnet_c1a9df69_Tx                                           136u
#define CanIfTxPduHnd_CIOM_Cab_24P_oCabSubnet_e2729fa9_Tx                                           137u
#define CanIfTxPduHnd_CIOM_Cab_10P_oCabSubnet_a6a5d96b_Tx                                           138u
#define CanIfTxPduHnd_CIOM_Cab_26P_oCabSubnet_2df59da9_Tx                                           139u
#define CanIfTxPduHnd_CIOM_Cab_23P_oCabSubnet_0e491ee8_Tx                                           140u
#define CanIfTxPduHnd_CIOM_Cab_27P_oCabSubnet_4a361ca9_Tx                                           141u
#define CanIfTxPduHnd_CIOM_Cab_09P_oCabSubnet_49ff9f68_Tx                                           142u
#define CanIfTxPduHnd_CIOM_Cab_33P_oCabSubnet_0e2edd69_Tx                                           143u
#define CanIfTxPduHnd_CIOM_Cab_06P_oCabSubnet_2d3a1aab_Tx                                           144u
#define CanIfTxPduHnd_CIOM_Cab_03P_oCabSubnet_0e8699ea_Tx                                           145u
#define CanIfTxPduHnd_CIOM_Cab_01P_oCabSubnet_c1019bea_Tx                                           146u
#define CanIfTxPduHnd_CIOM_Cab_32P_oCabSubnet_69ed5c69_Tx                                           147u
#define CanIfTxPduHnd_CIOM_Cab_02P_oCabSubnet_694518ea_Tx                                           148u
#define CanIfTxPduHnd_FMS_X_CIOMFMS_oFMSNet_68f86b37_Tx                                             149u
#define CanIfTxPduHnd_TPCM_Tp_oFMSNet_55e2f930_Tx                                                   150u
#define CanIfTxPduHnd_TPDT_Tp_oFMSNet_3018618a_Tx                                                   151u
#define CanIfTxPduHnd_VP236_X_CIOMFMS_oFMSNet_524bf569_Tx                                           152u
#define CanIfTxPduHnd_DD_X_CIOMFMS_oFMSNet_95f62c09_Tx                                              153u
#define CanIfTxPduHnd_AMB_X_CIOMFMS_oFMSNet_f6109b97_Tx                                             154u
#define CanIfTxPduHnd_ET1_X_CIOMFMS_oFMSNet_ff39068f_Tx                                             155u
#define CanIfTxPduHnd_VW_X_CIOMFMS_oFMSNet_06d77621_Tx                                              156u
#define CanIfTxPduHnd_LFC_X_CIOMFMS_oFMSNet_16e703d3_Tx                                             157u
#define CanIfTxPduHnd_HOURS_X_CIOMFMS_oFMSNet_bfc991fc_Tx                                           158u
#define CanIfTxPduHnd_VDHR_X_CIOMFMS_oFMSNet_67a02fb8_Tx                                            159u
#define CanIfTxPduHnd_SERV_X_CIOMFMS_oFMSNet_0f3b4ff2_Tx                                            160u
#define CanIfTxPduHnd_AIR1_X_CIOMFMS_oFMSNet_9b1ec537_Tx                                            161u
#define CanIfTxPduHnd_CVW_X_CIOMFMS_oFMSNet_5e9635e8_Tx                                             162u
#define CanIfTxPduHnd_TPDirect_0FE6B_Tp_oFMSNet_5c137171_Tx                                         163u
#define CanIfTxPduHnd_AT1T1I1_X_CIOMFMS_oFMSNet_4f6adf75_Tx                                         164u
#define CanIfTxPduHnd_EEC14_X_CIOMFMS_oFMSNet_3551b912_Tx                                           165u
#define CanIfTxPduHnd_FMS1_X_CIOMFMS_oFMSNet_fb3c9fbd_Tx                                            166u
#define CanIfTxPduHnd_HRLFC_X_CIOMFMS_oFMSNet_c55a7a2b_Tx                                           167u
#define CanIfTxPduHnd_J1939NmTxPdu_2068bfea                                                         168u
#define CanIfTxPduHnd_CL_X_CIOMFMS_oFMSNet_fc518547_Tx                                              169u
#define CanIfTxPduHnd_AckmTxPdu_FMSNet_J1939_44d89c3b                                               170u
#define CanIfTxPduHnd_LFE_X_CIOMFMS_oFMSNet_adac24c8_Tx                                             171u
#define CanIfTxPduHnd_CCVS_X_CIOMFMS_oFMSNet_1dbcfa70_Tx                                            172u
#define CanIfTxPduHnd_PTODE_X_CIOMFMS_oFMSNet_8abc23b8_Tx                                           173u
#define CanIfTxPduHnd_ERC1_x_RECUFMS_oFMSNet_338e7918_Tx                                            174u
#define CanIfTxPduHnd_ERC1_x_EMSRetFMS_oFMSNet_d863a207_Tx                                          175u
#define CanIfTxPduHnd_TCO1_X_CIOMFMS_oFMSNet_567baf3d_Tx                                            176u
#define CanIfTxPduHnd_EEC1_X_CIOMFMS_oFMSNet_954d78de_Tx                                            177u
#define CanIfTxPduHnd_EEC2_X_CIOMFMS_oFMSNet_d3e60e6a_Tx                                            178u
#define CanIfTxPduHnd_NmAsr_CIOM_Sec_oSecuritySubnet_dae88b7d_Tx                                    179u
#define CanIfTxPduHnd_PDM_Sec_04S_FCM_Tp_oSecuritySubnet_90a313cf_Tx                                180u
#define CanIfTxPduHnd_CIOM_Sec_05S_Tp_oSecuritySubnet_c8b553a6_Tx                                   181u
#define CanIfTxPduHnd_PDM_Sec_03S_FCM_Tp_oSecuritySubnet_2750bd9e_Tx                                182u
#define CanIfTxPduHnd_DDM_Sec_05S_FCM_Tp_oSecuritySubnet_d14dc345_Tx                                183u
#define CanIfTxPduHnd_Alarm_Sec_07S_FCM_Tp_oSecuritySubnet_94ec21d2_Tx                              184u
#define CanIfTxPduHnd_DDM_Sec_04S_FCM_Tp_oSecuritySubnet_eddd4cee_Tx                                185u
#define CanIfTxPduHnd_Alarm_Sec_06S_FCM_Tp_oSecuritySubnet_a3ee35ea_Tx                              186u
#define CanIfTxPduHnd_DDM_Sec_03S_FCM_Tp_oSecuritySubnet_5a2ee2bf_Tx                                187u
#define CanIfTxPduHnd_CIOM_Sec_07S_Tp_oSecuritySubnet_69506b1a_Tx                                   188u
#define CanIfTxPduHnd_CIOM_Sec_11S_Tp_oSecuritySubnet_dc8daaf0_Tx                                   189u
#define CanIfTxPduHnd_CIOM_Sec_09S_Tp_oSecuritySubnet_ba09ccac_Tx                                   190u
#define CanIfTxPduHnd_CIOM_Sec_06S_Tp_oSecuritySubnet_39a2f744_Tx                                   191u
#define CanIfTxPduHnd_Alarm_Sec_03S_FCM_Tp_oSecuritySubnet_48e47132_Tx                              192u
#define CanIfTxPduHnd_CIOM_Sec_08S_Tp_oSecuritySubnet_eafb50f2_Tx                                   193u
#define CanIfTxPduHnd_IntTesterTGW2FuncDiagMsg_Sec_Tp_oSecuritySubnet_57d96404_Tx                   194u
#define CanIfTxPduHnd_TesterFuncDiagMsg_Sec_Tp_oSecuritySubnet_7f5a7347_Tx                          195u
#define CanIfTxPduHnd_DiagReqMsgIntTGW2_C0_F4_Sec_Tp_oSecuritySubnet_0ba24808_Tx                    196u
#define CanIfTxPduHnd_DiagReqMsgIntHMIIOM_C0_F3_Sec_Tp_oSecuritySubnet_3aaf1649_Tx                  197u
#define CanIfTxPduHnd_PhysDiagReqMsg_C0_F2_Sec_Tp_oSecuritySubnet_f27248ff_Tx                       198u
#define CanIfTxPduHnd_DiagReqMsgIntTGW2_A1_F4_Sec_Tp_oSecuritySubnet_eb89cc56_Tx                    199u
#define CanIfTxPduHnd_DiagReqMsgIntHMIIOM_A1_F3_Sec_Tp_oSecuritySubnet_a1b03b66_Tx                  200u
#define CanIfTxPduHnd_PhysDiagReqMsg_A1_F2_Sec_Tp_oSecuritySubnet_5d2e792a_Tx                       201u
#define CanIfTxPduHnd_DiagReqMsgIntTGW2_A0_F4_Sec_Tp_oSecuritySubnet_8adef0bc_Tx                    202u
#define CanIfTxPduHnd_DiagReqMsgIntHMIIOM_A0_F3_Sec_Tp_oSecuritySubnet_09265670_Tx                  203u
#define CanIfTxPduHnd_PhysDiagReqMsg_A0_F2_Sec_Tp_oSecuritySubnet_cb87d97b_Tx                       204u
#define CanIfTxPduHnd_AnmMsg_CIOM_SecuritySubnet_oSecuritySubnet_d0267e59_Tx                        205u
#define CanIfTxPduHnd_CIOM_Sec_02P_oSecuritySubnet_04f0ad2a_Tx                                      206u
#define CanIfTxPduHnd_CIOM_Sec_03P_oSecuritySubnet_4d58f6b7_Tx                                      207u
#define CanIfTxPduHnd_CIOM_Sec_12S_Tp_oSecuritySubnet_2d9a0e12_Tx                                   208u
#define CanIfTxPduHnd_CIOM_Sec_10S_Tp_oSecuritySubnet_8c7f36ae_Tx                                   209u
#define CanIfTxPduHnd_CIOM_Sec_04P_oSecuritySubnet_6a707225_Tx                                      210u
#define CanIfTxPduHnd_CIOM_Sec_01P_oSecuritySubnet_de08418d_Tx                                      211u


/**********************************************************************************************************************
  \def  Rx PDU handles (Rx indication function specific ones)
**********************************************************************************************************************/

/* These definitions can change at Link-time and Post-build configuration time. Use them wisely. */
/* Assigned to: NULL_PTR*/
/* Assigned to: CanNm_RxIndication*/
#define CanIfRxPduHnd_CIOM_263e35c1_Rx                                                              0u
#define CanIfRxPduHnd_CIOM_c15ca2a0_Rx                                                              1u
#define CanIfRxPduHnd_CIOM_0462631a_Rx                                                              2u
/* Assigned to: CanTp_RxIndication*/
#define CanIfRxPduHnd_DiagReqMsgIntTGW2_C0_F4_BB2_Tp_oBackbone2_92584732_Rx                         0u
#define CanIfRxPduHnd_DiagReqMsgIntHMIIOM_C0_F3_BB2_Tp_oBackbone2_2bec4c32_Rx                       1u
#define CanIfRxPduHnd_PhysDiagReqMsg_C0_F2_BB2_Tp_oBackbone2_bd293e55_Rx                            2u
#define CanIfRxPduHnd_DiagReqMsgIntTGW2_98_F4_BB2_Tp_oBackbone2_54fed8f2_Rx                         3u
#define CanIfRxPduHnd_DiagReqMsgIntHMIIOM_98_F3_BB2_Tp_oBackbone2_3994cb72_Rx                       4u
#define CanIfRxPduHnd_PhysDiagReqMsg_98_F2_BB2_Tp_oBackbone2_c6fd39ec_Rx                            5u
#define CanIfRxPduHnd_DiagReqMsgIntTGW2_53_F4_BB2_Tp_oBackbone2_0f9dcb70_Rx                         6u
#define CanIfRxPduHnd_DiagReqMsgIntHMIIOM_53_F3_BB2_Tp_oBackbone2_fd3bad74_Rx                       7u
#define CanIfRxPduHnd_PhysDiagReqMsg_53_F2_BB2_Tp_oBackbone2_1012d7c4_Rx                            8u
#define CanIfRxPduHnd_DiagReqMsgIntTGW2_40_F4_BB2_Tp_oBackbone2_f28a299a_Rx                         9u
#define CanIfRxPduHnd_DiagReqMsgIntHMIIOM_40_F3_BB2_Tp_oBackbone2_a4e3bf32_Rx                       10u
#define CanIfRxPduHnd_PhysDiagReqMsg_40_F2_BB2_Tp_oBackbone2_87192655_Rx                            11u
#define CanIfRxPduHnd_VMCU_BB2_57P_Tp_oBackbone2_925193df_Rx                                        12u
#define CanIfRxPduHnd_HMIIOM_BB2_36S_Tp_oBackbone2_95bfb681_Rx                                      13u
#define CanIfRxPduHnd_HMIIOM_BB2_20S_Tp_oBackbone2_ce8859ea_Rx                                      14u
#define CanIfRxPduHnd_PhysDiagReqMsg_53_F1_BB2_Tp_oBackbone2_e289c86d_Rx                            15u
#define CanIfRxPduHnd_VMCU_BB2_34S_Tp_oBackbone2_37bde105_Rx                                        16u
#define CanIfRxPduHnd_TECU_BB2_05S_Tp_oBackbone2_01d5ac34_Rx                                        17u
#define CanIfRxPduHnd_HMIIOM_BB2_21S_Tp_oBackbone2_9d546738_Rx                                      18u
#define CanIfRxPduHnd_EMS_BB2_11S_Tp_oBackbone2_f0eef471_Rx                                         19u
#define CanIfRxPduHnd_CIOM_BB2_13S_FCM_Tp_oBackbone2_57d6a4a9_Rx                                    20u
#define CanIfRxPduHnd_CIOM_BB2_12S_FCM_Tp_oBackbone2_13bf5a99_Rx                                    21u
#define CanIfRxPduHnd_HMIIOM_BB2_04S_Tp_oBackbone2_8b957b6f_Rx                                      22u
#define CanIfRxPduHnd_IntTesterTGW2FuncDiagMsg_BB2_Tp_oBackbone2_c6fcfce9_Rx                        23u
#define CanIfRxPduHnd_TesterFuncDiagMsg_BB2_Tp_oBackbone2_6ec04186_Rx                               24u
#define CanIfRxPduHnd_DiagReqMsgIntTGW2_A2_F4_BB2_Tp_oBackbone2_8613fab7_Rx                         25u
#define CanIfRxPduHnd_DiagReqMsgIntHMIIOM_A2_F3_BB2_Tp_oBackbone2_e882bd41_Rx                       26u
#define CanIfRxPduHnd_PhysDiagReqMsg_A2_F2_BB2_Tp_oBackbone2_e7d85c22_Rx                            27u
#define CanIfRxPduHnd_DiagReqMsgIntTGW2_A1_F4_BB2_Tp_oBackbone2_91dc1a3b_Rx                         28u
#define CanIfRxPduHnd_DiagReqMsgIntHMIIOM_A1_F3_BB2_Tp_oBackbone2_ba062351_Rx                       29u
#define CanIfRxPduHnd_PhysDiagReqMsg_A1_F2_BB2_Tp_oBackbone2_9bf67e6d_Rx                            30u
#define CanIfRxPduHnd_DiagReqMsgIntTGW2_A0_F4_BB2_Tp_oBackbone2_9c9945bf_Rx                         31u
#define CanIfRxPduHnd_DiagReqMsgIntHMIIOM_A0_F3_BB2_Tp_oBackbone2_3d55549e_Rx                       32u
#define CanIfRxPduHnd_PhysDiagReqMsg_A0_F2_BB2_Tp_oBackbone2_b0139fa8_Rx                            33u
#define CanIfRxPduHnd_DiagReqMsgIntTGW2_26_F4_BB2_Tp_oBackbone2_ce56ef15_Rx                         34u
#define CanIfRxPduHnd_DiagReqMsgIntHMIIOM_26_F3_BB2_Tp_oBackbone2_3a21aae6_Rx                       35u
#define CanIfRxPduHnd_PhysDiagReqMsg_26_F2_BB2_Tp_oBackbone2_680a80cc_Rx                            36u
#define CanIfRxPduHnd_TECU_BB2_06S_Tp_oBackbone2_0c47cb8e_Rx                                        37u
#define CanIfRxPduHnd_HMIIOM_BB2_27S_Tp_oBackbone2_aeede795_Rx                                      38u
#define CanIfRxPduHnd_HMIIOM_BB2_19P_CIOM_Tp_oBackbone2_470ca910_Rx                                 39u
#define CanIfRxPduHnd_VMCU_BB2_31S_Tp_oBackbone2_210b49cb_Rx                                        40u
#define CanIfRxPduHnd_VMCU_BB2_32S_Tp_oBackbone2_2c992e71_Rx                                        41u
#define CanIfRxPduHnd_EMS_BB2_09S_Tp_oBackbone2_ef0c9958_Rx                                         42u
#define CanIfRxPduHnd_BBM_BB2_03S_CIOM_Tp_oBackbone2_8a8d09f2_Rx                                    43u
#define CanIfRxPduHnd_CIOM_BB2_22S_FCM_Tp_oBackbone2_7818b16f_Rx                                    44u
#define CanIfRxPduHnd_HMIIOM_BB2_35S_Tp_oBackbone2_61dbf5f7_Rx                                      45u
#define CanIfRxPduHnd_HMIIOM_BB2_06S_Tp_oBackbone2_2c2d06cb_Rx                                      46u
#define CanIfRxPduHnd_CIOM_BB2_30S_FCM_Tp_oBackbone2_d656145d_Rx                                    47u
#define CanIfRxPduHnd_DiagRespMsgIntTGW2_F4_A2_Cab_Tp_oCabSubnet_748ab027_Rx                        48u
#define CanIfRxPduHnd_DiagRespMsgIntTGW2_F4_98_Cab_Tp_oCabSubnet_d506a20a_Rx                        49u
#define CanIfRxPduHnd_DiagRespMsgIntTGW2_F4_53_Cab_Tp_oCabSubnet_781e6a27_Rx                        50u
#define CanIfRxPduHnd_DiagRespMsgIntTGW2_F4_26_Cab_Tp_oCabSubnet_0b131bbc_Rx                        51u
#define CanIfRxPduHnd_DiagRespMsgIntHMIIOM_F3_A2_Cab_Tp_oCabSubnet_15c35ee3_Rx                      52u
#define CanIfRxPduHnd_DiagRespMsgIntHMIIOM_F3_98_Cab_Tp_oCabSubnet_b05496ae_Rx                      53u
#define CanIfRxPduHnd_DiagRespMsgIntHMIIOM_F3_53_Cab_Tp_oCabSubnet_507b9b04_Rx                      54u
#define CanIfRxPduHnd_DiagRespMsgIntHMIIOM_F3_26_Cab_Tp_oCabSubnet_68d46304_Rx                      55u
#define CanIfRxPduHnd_PhysDiagRespMsg_F2_A2_Cab_Tp_oCabSubnet_89846943_Rx                           56u
#define CanIfRxPduHnd_PhysDiagRespMsg_F2_98_Cab_Tp_oCabSubnet_af4ee02a_Rx                           57u
#define CanIfRxPduHnd_PhysDiagRespMsg_F2_53_Cab_Tp_oCabSubnet_4c48e914_Rx                           58u
#define CanIfRxPduHnd_PhysDiagRespMsg_F2_26_Cab_Tp_oCabSubnet_48eedf32_Rx                           59u
#define CanIfRxPduHnd_CIOM_Cab_20S_FCM_Tp_oCabSubnet_88858df9_Rx                                    60u
#define CanIfRxPduHnd_CIOM_Cab_34P_FCM_Tp_oCabSubnet_d2bbef9e_Rx                                    61u
#define CanIfRxPduHnd_CIOM_Cab_30S_FCM_Tp_oCabSubnet_ae18d4ab_Rx                                    62u
#define CanIfRxPduHnd_CIOM_Cab_25S_FCM_Tp_oCabSubnet_063a8d48_Rx                                    63u
#define CanIfRxPduHnd_CIOM_Cab_13S_FCM_Tp_oCabSubnet_2f98645f_Rx                                    64u
#define CanIfRxPduHnd_CIOM_Cab_11S_FCM_Tp_oCabSubnet_a74b983f_Rx                                    65u
#define CanIfRxPduHnd_PhysDiagRespMsg_F1_53_Cab_Tp_oCabSubnet_1d55a011_Rx                           66u
#define CanIfRxPduHnd_CIOM_Cab_29S_FCM_Tp_oCabSubnet_8230888a_Rx                                    67u
#define CanIfRxPduHnd_CIOM_Cab_05P_FCM_Tp_oCabSubnet_fd75fa58_Rx                                    68u
#define CanIfRxPduHnd_CIOM_Cab_04P_FCM_Tp_oCabSubnet_b91c0468_Rx                                    69u
#define CanIfRxPduHnd_CCM_Cab_03P_Tp_oCabSubnet_bfb45635_Rx                                         70u
#define CanIfRxPduHnd_DiagRespMsgIntTGW2_F4_C0_Sec_Tp_oSecuritySubnet_7a975d32_Rx                   71u
#define CanIfRxPduHnd_DiagRespMsgIntTGW2_F4_A1_Sec_Tp_oSecuritySubnet_efa76b9d_Rx                   72u
#define CanIfRxPduHnd_DiagRespMsgIntTGW2_F4_A0_Sec_Tp_oSecuritySubnet_d9469eac_Rx                   73u
#define CanIfRxPduHnd_DiagRespMsgIntHMIIOM_F3_C0_Sec_Tp_oSecuritySubnet_77f0a36b_Rx                 74u
#define CanIfRxPduHnd_DiagRespMsgIntHMIIOM_F3_A1_Sec_Tp_oSecuritySubnet_052f8796_Rx                 75u
#define CanIfRxPduHnd_DiagRespMsgIntHMIIOM_F3_A0_Sec_Tp_oSecuritySubnet_95b29252_Rx                 76u
#define CanIfRxPduHnd_PhysDiagRespMsg_F2_C0_Sec_Tp_oSecuritySubnet_614a0693_Rx                      77u
#define CanIfRxPduHnd_PhysDiagRespMsg_F2_A1_Sec_Tp_oSecuritySubnet_d47cc691_Rx                      78u
#define CanIfRxPduHnd_PhysDiagRespMsg_F2_A0_Sec_Tp_oSecuritySubnet_207d0e64_Rx                      79u
#define CanIfRxPduHnd_PDM_Sec_04S_Tp_oSecuritySubnet_596ba3c3_Rx                                    80u
#define CanIfRxPduHnd_CIOM_Sec_05S_FCM_Tp_oSecuritySubnet_02ea2344_Rx                               81u
#define CanIfRxPduHnd_PDM_Sec_03S_Tp_oSecuritySubnet_225f8534_Rx                                    82u
#define CanIfRxPduHnd_DDM_Sec_05S_Tp_oSecuritySubnet_5a1ed97b_Rx                                    83u
#define CanIfRxPduHnd_Alarm_Sec_07S_Tp_oSecuritySubnet_bf7e0710_Rx                                  84u
#define CanIfRxPduHnd_DDM_Sec_04S_Tp_oSecuritySubnet_c3efbadc_Rx                                    85u
#define CanIfRxPduHnd_Alarm_Sec_06S_Tp_oSecuritySubnet_2b24d209_Rx                                  86u
#define CanIfRxPduHnd_DDM_Sec_03S_Tp_oSecuritySubnet_b8db9c2b_Rx                                    87u
#define CanIfRxPduHnd_CIOM_Sec_12S_FCM_Tp_oSecuritySubnet_9cba7397_Rx                               88u
#define CanIfRxPduHnd_CIOM_Sec_07S_FCM_Tp_oSecuritySubnet_5acc46b8_Rx                               89u
#define CanIfRxPduHnd_CIOM_Sec_11S_FCM_Tp_oSecuritySubnet_e88f2495_Rx                               90u
#define CanIfRxPduHnd_CIOM_Sec_10S_FCM_Tp_oSecuritySubnet_c49c166b_Rx                               91u
#define CanIfRxPduHnd_CIOM_Sec_09S_FCM_Tp_oSecuritySubnet_094e790d_Rx                               92u
#define CanIfRxPduHnd_CIOM_Sec_06S_FCM_Tp_oSecuritySubnet_76df7446_Rx                               93u
#define CanIfRxPduHnd_Alarm_Sec_03S_Tp_oSecuritySubnet_838659b7_Rx                                  94u
#define CanIfRxPduHnd_CIOM_Sec_08S_FCM_Tp_oSecuritySubnet_255d4bf3_Rx                               95u
/* Assigned to: J1939Nm_RxIndication*/
#define CanIfRxPduHnd_J1939NmRxPdu_fa509995                                                         0u
#define CanIfRxPduHnd_J1939NmRxPdu_2068bfea                                                         1u
/* Assigned to: J1939Tp_RxIndication*/
#define CanIfRxPduHnd_TPDT_Tp_oBackbone1J1939_7785b746_Rx                                           0u
#define CanIfRxPduHnd_TPCM_Tp_oBackbone1J1939_ffabc0bc_Rx                                           1u
#define CanIfRxPduHnd_TPDirect_0FE6B_Tp_oBackbone1J1939_e14130fb_Rx                                 2u
#define CanIfRxPduHnd_FcNPdu_FMSNet_J1939_5ab570f8                                                  3u
/* Assigned to: PduR_CanIfRxIndication*/
#define CanIfRxPduHnd_RqstRxPdu_Backbone1J1939_54966c1b                                             0u
#define CanIfRxPduHnd_EEC1_X_EMS_oBackbone1J1939_5b1d5ab3_Rx                                        1u
#define CanIfRxPduHnd_VMCU_BB1_01P_oBackbone1J1939_c293b9cd_Rx                                      2u
#define CanIfRxPduHnd_EBC2_X_EBS_oBackbone1J1939_416c8820_Rx                                        3u
#define CanIfRxPduHnd_EEC2_X_EMS_oBackbone1J1939_b0cfb70f_Rx                                        4u
#define CanIfRxPduHnd_EBC5_X_EBS_oBackbone1J1939_a226f3b2_Rx                                        5u
#define CanIfRxPduHnd_HRLFC_X_EMS_oBackbone1J1939_5e7f7408_Rx                                       6u
#define CanIfRxPduHnd_TCO1_X_TACHO_oBackbone1J1939_884a7499_Rx                                      7u
#define CanIfRxPduHnd_ACM_BB1_01P_oBackbone1J1939_9e50bfac_Rx                                       8u
#define CanIfRxPduHnd_ERC1_X_RECU_oBackbone1J1939_3104dca7_Rx                                       9u
#define CanIfRxPduHnd_VDC2_X_EBS_oBackbone1J1939_e250e643_Rx                                        10u
#define CanIfRxPduHnd_AIR1_X_VMCU_oBackbone1J1939_30174316_Rx                                       11u
#define CanIfRxPduHnd_CVW_X_EBS_oBackbone1J1939_2dfa9bbe_Rx                                         12u
#define CanIfRxPduHnd_CCVS_X_VMCU_oBackbone1J1939_d16316a3_Rx                                       13u
#define CanIfRxPduHnd_EBS_BB1_05P_oBackbone1J1939_43953999_Rx                                       14u
#define CanIfRxPduHnd_LFE_X_EMS_oBackbone1J1939_592edea5_Rx                                         15u
#define CanIfRxPduHnd_VDC1_X_EBS_oBackbone1J1939_09820bff_Rx                                        16u
#define CanIfRxPduHnd_EBS_BB1_02P_oBackbone1J1939_b875028c_Rx                                       17u
#define CanIfRxPduHnd_ET1_X_EMS_oBackbone1J1939_a6b0a91f_Rx                                         18u
#define CanIfRxPduHnd_VDHR_X_VMCU_oBackbone1J1939_0e2cb4b8_Rx                                       19u
#define CanIfRxPduHnd_EBS_BB1_01P_oBackbone1J1939_9205779e_Rx                                       20u
#define CanIfRxPduHnd_EBC1_X_EBS_oBackbone1J1939_aabe659c_Rx                                        21u
#define CanIfRxPduHnd_AMB_X_VMCU_oBackbone1J1939_d5496e9a_Rx                                        22u
#define CanIfRxPduHnd_VMCU_BB1_03P_oBackbone1J1939_51c30ef7_Rx                                      23u
#define CanIfRxPduHnd_FMS1_X_HMIIOM_oBackbone1J1939_f8c70f73_Rx                                     24u
#define CanIfRxPduHnd_TD_X_HMIIOM_oBackbone1J1939_49e6bcf2_Rx                                       25u
#define CanIfRxPduHnd_ERC1_X_EMSRet_oBackbone1J1939_35cada34_Rx                                     26u
#define CanIfRxPduHnd_AnmMsg_ECUspare2_Backbone2_oBackbone2_181d947f_Rx                             27u
#define CanIfRxPduHnd_AnmMsg_ECUspare1_Backbone2_oBackbone2_c849277d_Rx                             28u
#define CanIfRxPduHnd_AnmMsg_VMCU_Backbone2_oBackbone2_e6fd44da_Rx                                  29u
#define CanIfRxPduHnd_AnmMsg_TECU_Backbone2_oBackbone2_9bd86e99_Rx                                  30u
#define CanIfRxPduHnd_AnmMsg_HMIIOM_Backbone2_oBackbone2_7a9b7e63_Rx                                31u
#define CanIfRxPduHnd_AnmMsg_EMS_Backbone2_oBackbone2_5f08a43f_Rx                                   32u
#define CanIfRxPduHnd_AnmMsg_DACU_Backbone2_oBackbone2_fb766361_Rx                                  33u
#define CanIfRxPduHnd_AnmMsg_BBM_Backbone2_oBackbone2_604b553c_Rx                                   34u
#define CanIfRxPduHnd_HMIIOM_BB2_17P_oBackbone2_8d736566_Rx                                         35u
#define CanIfRxPduHnd_HMIIOM_BB2_16P_oBackbone2_b66be737_Rx                                         36u
#define CanIfRxPduHnd_HMIIOM_BB2_14P_oBackbone2_c05ae395_Rx                                         37u
#define CanIfRxPduHnd_HMIIOM_BB2_39P_oBackbone2_2c3b8f89_Rx                                         38u
#define CanIfRxPduHnd_HMIIOM_BB2_13P_oBackbone2_61116c22_Rx                                         39u
#define CanIfRxPduHnd_HMIIOM_BB2_22P_oBackbone2_7ef06453_Rx                                         40u
#define CanIfRxPduHnd_VMCU_BB2_58P_oBackbone2_0848ab9e_Rx                                           41u
#define CanIfRxPduHnd_VMCU_BB2_29P_oBackbone2_6ebc6019_Rx                                           42u
#define CanIfRxPduHnd_HMIIOM_BB2_10P_oBackbone2_2c38ead1_Rx                                         43u
#define CanIfRxPduHnd_VMCU_BB2_08P_oBackbone2_09b0661b_Rx                                           44u
#define CanIfRxPduHnd_HMIIOM_BB2_25P_oBackbone2_dfbbebe4_Rx                                         45u
#define CanIfRxPduHnd_TECU_BB2_02P_oBackbone2_09dfdabc_Rx                                           46u
#define CanIfRxPduHnd_VMCU_BB2_74P_oBackbone2_c4062a5f_Rx                                           47u
#define CanIfRxPduHnd_VMCU_BB2_73P_oBackbone2_283dab1e_Rx                                           48u
#define CanIfRxPduHnd_HMIIOM_BB2_08P_oBackbone2_33da87f8_Rx                                         49u
#define CanIfRxPduHnd_VMCU_BB2_07P_oBackbone2_6d75e3d8_Rx                                           50u
#define CanIfRxPduHnd_HMIIOM_BB2_07P_oBackbone2_91241c86_Rx                                         51u
#define CanIfRxPduHnd_HMIIOM_BB2_24P_oBackbone2_e4a369b5_Rx                                         52u
#define CanIfRxPduHnd_BBM_BB2_06P_oBackbone2_89015c9b_Rx                                            53u
#define CanIfRxPduHnd_VMCU_BB2_80P_oBackbone2_82707e91_Rx                                           54u
#define CanIfRxPduHnd_VMCU_BB2_20P_oBackbone2_8181e59b_Rx                                           55u
#define CanIfRxPduHnd_BBM_BB2_02P_oBackbone2_912f6fea_Rx                                            56u
#define CanIfRxPduHnd_VMCU_BB2_51P_oBackbone2_e7752e1c_Rx                                           57u
#define CanIfRxPduHnd_EMS_BB2_01P_oBackbone2_9dd8bb44_Rx                                            58u
#define CanIfRxPduHnd_VMCU_BB2_03P_oBackbone2_290ae199_Rx                                           59u
#define CanIfRxPduHnd_VMCU_BB2_01P_oBackbone2_e68de399_Rx                                           60u
#define CanIfRxPduHnd_TECU_BB2_01P_oBackbone2_a19b59bc_Rx                                           61u
#define CanIfRxPduHnd_HMIIOM_BB2_01P_oBackbone2_0b771160_Rx                                         62u
#define CanIfRxPduHnd_HMIIOM_BB2_02P_oBackbone2_465e9793_Rx                                         63u
#define CanIfRxPduHnd_DebugCtrl1_CIOM_BB2_oBackbone2_ca351e0d_Rx                                    64u
#define CanIfRxPduHnd_HMIIOM_BB2_18P_oBackbone2_2f8dfe18_Rx                                         65u
#define CanIfRxPduHnd_HMIIOM_BB2_15P_oBackbone2_fb4261c4_Rx                                         66u
#define CanIfRxPduHnd_HMIIOM_BB2_38P_oBackbone2_17230dd8_Rx                                         67u
#define CanIfRxPduHnd_HMIIOM_BB2_12P_oBackbone2_5a09ee73_Rx                                         68u
#define CanIfRxPduHnd_HMIIOM_BB2_11P_oBackbone2_17206880_Rx                                         69u
#define CanIfRxPduHnd_EMS_BB2_08P_oBackbone2_dd5310ea_Rx                                            70u
#define CanIfRxPduHnd_EMS_BB2_06P_oBackbone2_148edfe1_Rx                                            71u
#define CanIfRxPduHnd_EMS_BB2_04P_oBackbone2_f5214579_Rx                                            72u
#define CanIfRxPduHnd_EMS_BB2_13P_oBackbone2_036dfbaa_Rx                                            73u
#define CanIfRxPduHnd_EMS_BB2_05P_oBackbone2_85f68835_Rx                                            74u
#define CanIfRxPduHnd_HMIIOM_BB2_05P_oBackbone2_e7151824_Rx                                         75u
#define CanIfRxPduHnd_VMCU_BB2_55P_oBackbone2_a30a2c5d_Rx                                           76u
#define CanIfRxPduHnd_HMIIOM_BB2_23P_oBackbone2_45e8e602_Rx                                         77u
#define CanIfRxPduHnd_HMIIOM_BB2_09P_oBackbone2_08c205a9_Rx                                         78u
#define CanIfRxPduHnd_HMIIOM_BB2_34S_oBackbone2_6313bb35_Rx                                         79u
#define CanIfRxPduHnd_VMCU_BB2_54P_oBackbone2_c4c9ad5d_Rx                                           80u
#define CanIfRxPduHnd_VMCU_BB2_53P_oBackbone2_28f22c1c_Rx                                           81u
#define CanIfRxPduHnd_HMIIOM_BB2_33P_oBackbone2_59bf9fe2_Rx                                         82u
#define CanIfRxPduHnd_HMIIOM_BB2_03P_oBackbone2_7d4615c2_Rx                                         83u
#define CanIfRxPduHnd_VMCU_BB2_52P_oBackbone2_4f31ad1c_Rx                                           84u
#define CanIfRxPduHnd_BBM_BB2_01P_oBackbone2_0057383e_Rx                                            85u
#define CanIfRxPduHnd_VMCU_BB2_05P_oBackbone2_a2f2e1d8_Rx                                           86u
#define CanIfRxPduHnd_VMCU_BB2_04P_oBackbone2_c53160d8_Rx                                           87u
#define CanIfRxPduHnd_DACU_BB2_02P_oBackbone2_0b8371c6_Rx                                           88u
#define CanIfRxPduHnd_VMCU_BB2_82P_oBackbone2_4df77c91_Rx                                           89u
#define CanIfRxPduHnd_VMCU_BB2_02P_oBackbone2_4ec96099_Rx                                           90u
#define CanIfRxPduHnd_Tester_CAN6toLIN_oCAN6_8915fe12_Rx                                            91u
#define CanIfRxPduHnd_WRCS_Cab_01P_oCabSubnet_f823b5c4_Rx                                           92u
#define CanIfRxPduHnd_DiagUUDTRespMsg1_F2_A2_Cab_oCabSubnet_faf51f79_Rx                             93u
#define CanIfRxPduHnd_DiagUUDTRespMsg1_F2_98_Cab_oCabSubnet_2175e3c9_Rx                             94u
#define CanIfRxPduHnd_DiagUUDTRespMsg1_F2_53_Cab_oCabSubnet_d5110ca2_Rx                             95u
#define CanIfRxPduHnd_DiagUUDTRespMsg1_F2_26_Cab_oCabSubnet_8c457eb7_Rx                             96u
#define CanIfRxPduHnd_AnmMsg_ECUspare6_CabSubnet_oCabSubnet_582cafe8_Rx                             97u
#define CanIfRxPduHnd_AnmMsg_WRCS_CabSubnet_oCabSubnet_62e3be6f_Rx                                  98u
#define CanIfRxPduHnd_AnmMsg_SRS_CabSubnet_oCabSubnet_690513e9_Rx                                   99u
#define CanIfRxPduHnd_AnmMsg_LECM1_CabSubnet_oCabSubnet_ba3b1140_Rx                                 100u
#define CanIfRxPduHnd_AnmMsg_CCM_CabSubnet_oCabSubnet_7e592096_Rx                                   101u
#define CanIfRxPduHnd_DiagFaultStat_WRCS_Cab_oCabSubnet_b86e4ab7_Rx                                 102u
#define CanIfRxPduHnd_DiagFaultStat_SRS_Cab_oCabSubnet_e5f44b73_Rx                                  103u
#define CanIfRxPduHnd_DiagFaultStat_LECM_Cab_oCabSubnet_6e0076c7_Rx                                 104u
#define CanIfRxPduHnd_DiagFaultStat_CCM_Cab_oCabSubnet_2b754d89_Rx                                  105u
#define CanIfRxPduHnd_SRS_Cab_06P_oCabSubnet_bec0c2b2_Rx                                            106u
#define CanIfRxPduHnd_SRS_Cab_05P_oCabSubnet_2fb89566_Rx                                            107u
#define CanIfRxPduHnd_LECM1_Cab_03S_oCabSubnet_e20ca5cb_Rx                                          108u
#define CanIfRxPduHnd_SRS_Cab_04P_oCabSubnet_5f6f582a_Rx                                            109u
#define CanIfRxPduHnd_SRS_Cab_03P_oCabSubnet_d6393c8f_Rx                                            110u
#define CanIfRxPduHnd_LECM1_Cab_05S_oCabSubnet_b18bf7f9_Rx                                          111u
#define CanIfRxPduHnd_WRCS_Cab_03P_oCabSubnet_37a4b7c4_Rx                                           112u
#define CanIfRxPduHnd_LECM1_Cab_04P_oCabSubnet_b355ee77_Rx                                          113u
#define CanIfRxPduHnd_LECM1_Cab_02P_oCabSubnet_e0d2bc45_Rx                                          114u
#define CanIfRxPduHnd_SRS_Cab_01P_oCabSubnet_3796a617_Rx                                            115u
#define CanIfRxPduHnd_WRCS_Cab_02P_oCabSubnet_506736c4_Rx                                           116u
#define CanIfRxPduHnd_CCM_Cab_06P_oCabSubnet_ae969834_Rx                                            117u
#define CanIfRxPduHnd_CCM_Cab_07P_oCabSubnet_de415578_Rx                                            118u
#define CanIfRxPduHnd_CCM_Cab_08P_oCabSubnet_674b573f_Rx                                            119u
#define CanIfRxPduHnd_CCM_Cab_04P_oCabSubnet_4f3902ac_Rx                                            120u
#define CanIfRxPduHnd_CCM_Cab_01P_oCabSubnet_27c0fc91_Rx                                            121u
#define CanIfRxPduHnd_RqstRxPdu_FMSNet_J1939_44d89c3b                                               122u
#define CanIfRxPduHnd_VP232_X_ERAU_oFMSNet_8c33d56b_Rx                                              123u
#define CanIfRxPduHnd_DiagUUDTRespMsg1_F2_C0_Sec_oSecuritySubnet_d0c97a87_Rx                        124u
#define CanIfRxPduHnd_DiagUUDTRespMsg1_F2_A1_Sec_oSecuritySubnet_67c5eccf_Rx                        125u
#define CanIfRxPduHnd_DiagUUDTRespMsg1_F2_A0_Sec_oSecuritySubnet_46825d6a_Rx                        126u
#define CanIfRxPduHnd_AnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_25af973c_Rx                   127u
#define CanIfRxPduHnd_AnmMsg_PDM_SecuritySubnet_oSecuritySubnet_d922132b_Rx                         128u
#define CanIfRxPduHnd_AnmMsg_DDM_SecuritySubnet_oSecuritySubnet_669221a0_Rx                         129u
#define CanIfRxPduHnd_AnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_38c19358_Rx                       130u
#define CanIfRxPduHnd_DiagFaultStat_PDM_Sec_oSecuritySubnet_b17b40b8_Rx                             131u
#define CanIfRxPduHnd_DiagFaultStat_DDM_Sec_oSecuritySubnet_3b5b41e4_Rx                             132u
#define CanIfRxPduHnd_DiagFaultStat_Alarm_Sec_oSecuritySubnet_6cedaf7c_Rx                           133u
#define CanIfRxPduHnd_PDM_Sec_01P_oSecuritySubnet_073fa36a_Rx                                       134u
#define CanIfRxPduHnd_DDM_Sec_01P_oSecuritySubnet_09e2b30b_Rx                                       135u
#define CanIfRxPduHnd_Alarm_Sec_02P_oSecuritySubnet_a902993f_Rx                                     136u
/* Assigned to: Xcp_CanIfRxIndication*/
#define CanIfRxPduHnd_XCP_40_FA_BB2_oBackbone2_0da36f9d_Rx                                          0u



/**********************************************************************************************************************
  \def  AUTOSAR Tx PDU handles
**********************************************************************************************************************/

#define CanIfConf_CanIfTxPduCfg_RQST_TACHO_CIOM_oBackbone1J1939_e082a6b2_Tx                         0u
#define CanIfConf_CanIfTxPduCfg_J1939NmTxPdu_fa509995                                               1u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB1_01P_oBackbone1J1939_55a00301_Tx                            2u
#define CanIfConf_CanIfTxPduCfg_FcNPdu_Backbone1J1939_dba64907                                      3u
#define CanIfConf_CanIfTxPduCfg_AckmTxPdu_Backbone1J1939_54966c1b                                   4u
#define CanIfConf_CanIfTxPduCfg_DiagUUDTRespMsg1_F2_C0_BB2_oBackbone2_603e2338_Tx                   5u
#define CanIfConf_CanIfTxPduCfg_DiagUUDTRespMsg1_F2_A2_BB2_oBackbone2_8af81a14_Tx                   6u
#define CanIfConf_CanIfTxPduCfg_DiagUUDTRespMsg1_F2_A1_BB2_oBackbone2_26f22879_Tx                   7u
#define CanIfConf_CanIfTxPduCfg_DiagUUDTRespMsg1_F2_A0_BB2_oBackbone2_42f439a2_Tx                   8u
#define CanIfConf_CanIfTxPduCfg_DiagUUDTRespMsg1_F2_98_BB2_oBackbone2_5178e6a4_Tx                   9u
#define CanIfConf_CanIfTxPduCfg_DiagUUDTRespMsg1_F2_53_BB2_oBackbone2_a51c09cf_Tx                   10u
#define CanIfConf_CanIfTxPduCfg_DiagUUDTRespMsg1_F2_40_BB2_oBackbone2_187336ef_Tx                   11u
#define CanIfConf_CanIfTxPduCfg_DiagUUDTRespMsg1_F2_26_BB2_oBackbone2_fc487bda_Tx                   12u
#define CanIfConf_CanIfTxPduCfg_XCP_FA_40_BB2_oBackbone2_9cf6862c_Tx                                13u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_C0_BB2_Tp_oBackbone2_f7aad0d7_Tx              14u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_A2_BB2_Tp_oBackbone2_249acffa_Tx              15u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_A1_BB2_Tp_oBackbone2_bca8fbcc_Tx              16u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_A0_BB2_Tp_oBackbone2_7d9615e1_Tx              17u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_98_BB2_Tp_oBackbone2_8516ddd7_Tx              18u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_53_BB2_Tp_oBackbone2_280e15fa_Tx              19u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_40_BB2_Tp_oBackbone2_f5224357_Tx              20u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntTGW2_F4_26_BB2_Tp_oBackbone2_5b036461_Tx              21u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_C0_BB2_Tp_oBackbone2_9924f26b_Tx            22u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_A2_BB2_Tp_oBackbone2_d6b4927e_Tx            23u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_A1_BB2_Tp_oBackbone2_08540fc1_Tx            24u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_A0_BB2_Tp_oBackbone2_f4db796b_Tx            25u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_98_BB2_Tp_oBackbone2_73235a33_Tx            26u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_53_BB2_Tp_oBackbone2_930c5799_Tx            27u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_40_BB2_Tp_oBackbone2_7b130fa6_Tx            28u
#define CanIfConf_CanIfTxPduCfg_DiagRespMsgIntHMIIOM_F3_26_BB2_Tp_oBackbone2_aba3af99_Tx            29u
#define CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_C0_BB2_Tp_oBackbone2_ae5b54dc_Tx                 30u
#define CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_A2_BB2_Tp_oBackbone2_2addb983_Tx                 31u
#define CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_A1_BB2_Tp_oBackbone2_441131ae_Tx                 32u
#define CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_A0_BB2_Tp_oBackbone2_d77a4b8a_Tx                 33u
#define CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_98_BB2_Tp_oBackbone2_0c1730ea_Tx                 34u
#define CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_53_BB2_Tp_oBackbone2_ef1139d4_Tx                 35u
#define CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_26_BB2_Tp_oBackbone2_ebb70ff2_Tx                 36u
#define CanIfConf_CanIfTxPduCfg_NmAsr_CIOM_BB2_oBackbone2_9b0338b4_Tx                               37u
#define CanIfConf_CanIfTxPduCfg_DiagFaultStat_SRS_BB2_oBackbone2_e1ef7eee_Tx                        38u
#define CanIfConf_CanIfTxPduCfg_DiagFaultStat_LECM_BB2_oBackbone2_0cb60c21_Tx                       39u
#define CanIfConf_CanIfTxPduCfg_DiagFaultStat_CCM_BB2_oBackbone2_2f6e7814_Tx                        40u
#define CanIfConf_CanIfTxPduCfg_DiagFaultStat_WRCS_BB2_oBackbone2_dad83051_Tx                       41u
#define CanIfConf_CanIfTxPduCfg_DiagFaultStat_Alarm_BB2_oBackbone2_7b6d0a04_Tx                      42u
#define CanIfConf_CanIfTxPduCfg_DiagFaultStat_PDM_BB2_oBackbone2_b720c987_Tx                        43u
#define CanIfConf_CanIfTxPduCfg_DiagFaultStat_DDM_BB2_oBackbone2_c5068234_Tx                        44u
#define CanIfConf_CanIfTxPduCfg_DiagFaultStat_CIOM_BB2_oBackbone2_04d7302e_Tx                       45u
#define CanIfConf_CanIfTxPduCfg_VMCU_BB2_57P_FCM_Tp_oBackbone2_65fb58e9_Tx                          46u
#define CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_36S_FCM_Tp_oBackbone2_bf84bd48_Tx                        47u
#define CanIfConf_CanIfTxPduCfg_TECU_BB2_06S_FCM_Tp_oBackbone2_a56e8b10_Tx                          48u
#define CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_27S_FCM_Tp_oBackbone2_16072c1e_Tx                        49u
#define CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_19P_CIOM_FCM_Tp_oBackbone2_1a616940_Tx                   50u
#define CanIfConf_CanIfTxPduCfg_VMCU_BB2_31S_FCM_Tp_oBackbone2_46c44850_Tx                          51u
#define CanIfConf_CanIfTxPduCfg_VMCU_BB2_32S_FCM_Tp_oBackbone2_8a7e4a00_Tx                          52u
#define CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_20S_FCM_Tp_oBackbone2_2def59ba_Tx                        53u
#define CanIfConf_CanIfTxPduCfg_EMS_BB2_09S_FCM_Tp_oBackbone2_41cc37fa_Tx                           54u
#define CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F1_53_BB2_Tp_oBackbone2_be0c70d1_Tx                 55u
#define CanIfConf_CanIfTxPduCfg_BBM_BB2_03S_CIOM_FCM_Tp_oBackbone2_29c4ac8e_Tx                      56u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_21S_oBackbone2_140cc697_Tx                                 57u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_24S_oBackbone2_37b045d6_Tx                                 58u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_23S_oBackbone2_db8bc497_Tx                                 59u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_22S_Tp_oBackbone2_92aec239_Tx                              60u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_20S_oBackbone2_73cf4797_Tx                                 61u
#define CanIfConf_CanIfTxPduCfg_VMCU_BB2_34S_FCM_Tp_oBackbone2_c87b48e1_Tx                          62u
#define CanIfConf_CanIfTxPduCfg_TECU_BB2_05S_FCM_Tp_oBackbone2_69d48940_Tx                          63u
#define CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_21S_FCM_Tp_oBackbone2_af5a2700_Tx                        64u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_26S_oBackbone2_f83747d6_Tx                                 65u
#define CanIfConf_CanIfTxPduCfg_EMS_BB2_11S_FCM_Tp_oBackbone2_6cd5cf95_Tx                           66u
#define CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_35S_FCM_Tp_oBackbone2_e32a38c7_Tx                        67u
#define CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_06S_FCM_Tp_oBackbone2_c2df8d7c_Tx                        68u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_13S_Tp_oBackbone2_bd99285a_Tx                              69u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_30S_Tp_oBackbone2_82da3f46_Tx                              70u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_12S_Tp_oBackbone2_b9170acc_Tx                              71u
#define CanIfConf_CanIfTxPduCfg_HMIIOM_BB2_04S_FCM_Tp_oBackbone2_1cc47649_Tx                        72u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_27S_oBackbone2_9ff4c6d6_Tx                                 73u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_25P_oBackbone2_732f917b_Tx                                 74u
#define CanIfConf_CanIfTxPduCfg_PhysDiagRespMsg_F2_40_BB2_Tp_oBackbone2_bd4d3e52_Tx                 75u
#define CanIfConf_CanIfTxPduCfg_Debug07_CIOM_BB2_oBackbone2_4ac5dfa9_Tx                             76u
#define CanIfConf_CanIfTxPduCfg_Debug06_CIOM_BB2_oBackbone2_25ede671_Tx                             77u
#define CanIfConf_CanIfTxPduCfg_Debug05_CIOM_BB2_oBackbone2_9495ac19_Tx                             78u
#define CanIfConf_CanIfTxPduCfg_Debug04_CIOM_BB2_oBackbone2_fbbd95c1_Tx                             79u
#define CanIfConf_CanIfTxPduCfg_Debug03_CIOM_BB2_oBackbone2_2d143e88_Tx                             80u
#define CanIfConf_CanIfTxPduCfg_Debug02_CIOM_BB2_oBackbone2_423c0750_Tx                             81u
#define CanIfConf_CanIfTxPduCfg_AnmMsg_CIOM_Backbone2_oBackbone2_d362d40e_Tx                        82u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_28P_oBackbone2_d86d16b8_Tx                                 83u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_07P_oBackbone2_bc671479_Tx                                 84u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_11P_oBackbone2_37f8d7b9_Tx                                 85u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_10P_oBackbone2_503b56b9_Tx                                 86u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_06P_oBackbone2_dba49579_Tx                                 87u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_05P_oBackbone2_73e01679_Tx                                 88u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_29P_oBackbone2_bfae97b8_Tx                                 89u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_01P_oBackbone2_379f1438_Tx                                 90u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_03P_oBackbone2_f8181638_Tx                                 91u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_02P_oBackbone2_9fdb9738_Tx                                 92u
#define CanIfConf_CanIfTxPduCfg_CIOM_BB2_04P_oBackbone2_14239779_Tx                                 93u
#define CanIfConf_CanIfTxPduCfg_SCIM_BB2toCAN6_oCAN6_adf5f68f_Tx                                    94u
#define CanIfConf_CanIfTxPduCfg_SCIM_BB1toCAN6_oCAN6_cb3fc040_Tx                                    95u
#define CanIfConf_CanIfTxPduCfg_SCIM_LINtoCAN6_oCAN6_6b855022_Tx                                    96u
#define CanIfConf_CanIfTxPduCfg_IntTesterTGW2FuncDiagMsg_Cab_Tp_oCabSubnet_96ec8334_Tx              97u
#define CanIfConf_CanIfTxPduCfg_TesterFuncDiagMsg_Cab_Tp_oCabSubnet_d293ecaa_Tx                     98u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_A2_F4_Cab_Tp_oCabSubnet_fc611706_Tx               99u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_A2_F3_Cab_Tp_oCabSubnet_fcbc8d7d_Tx             100u
#define CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_A2_F2_Cab_Tp_oCabSubnet_6d74e481_Tx                  101u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_98_F4_Cab_Tp_oCabSubnet_2e8c3543_Tx               102u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_98_F3_Cab_Tp_oCabSubnet_2daafb4e_Tx             103u
#define CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_98_F2_Cab_Tp_oCabSubnet_4c51814f_Tx                  104u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_53_F4_Cab_Tp_oCabSubnet_75ef26c1_Tx               105u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_53_F3_Cab_Tp_oCabSubnet_e9059d48_Tx             106u
#define CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_53_F2_Cab_Tp_oCabSubnet_9abe6f67_Tx                  107u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_26_F4_Cab_Tp_oCabSubnet_b42402a4_Tx               108u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_26_F3_Cab_Tp_oCabSubnet_2e1f9ada_Tx             109u
#define CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_26_F2_Cab_Tp_oCabSubnet_e2a6386f_Tx                  110u
#define CanIfConf_CanIfTxPduCfg_NmAsr_CIOM_Cab_oCabSubnet_153d3c7d_Tx                               111u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_21P_oCabSubnet_c1ce1ce8_Tx                                 112u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_20S_Tp_oCabSubnet_c323df97_Tx                              113u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_19P_oCabSubnet_49985ce9_Tx                                 114u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_18P_oCabSubnet_2e5bdde9_Tx                                 115u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_17P_oCabSubnet_4a9e582a_Tx                                 116u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_16P_oCabSubnet_2d5dd92a_Tx                                 117u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_34P_Tp_oCabSubnet_377e6916_Tx                              118u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_30S_Tp_oCabSubnet_da4b67c4_Tx                              119u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_25S_Tp_oCabSubnet_d5957759_Tx                              120u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_15P_oCabSubnet_85195a2a_Tx                                 121u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_14P_oCabSubnet_e2dadb2a_Tx                                 122u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_13S_Tp_oCabSubnet_e50870d8_Tx                              123u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_12P_oCabSubnet_6922db6b_Tx                                 124u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_11S_Tp_oCabSubnet_ec1435f4_Tx                              125u
#define CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_53_F1_Cab_Tp_oCabSubnet_682570ce_Tx                  126u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_08P_oCabSubnet_2e3c1e68_Tx                                 127u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_07P_oCabSubnet_4af99bab_Tx                                 128u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_22P_oCabSubnet_698a9fe8_Tx                                 129u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_29S_Tp_oCabSubnet_e3dce9b1_Tx                              130u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_05S_Tp_oCabSubnet_e74407ff_Tx                              131u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_04S_Tp_oCabSubnet_e3ca2569_Tx                              132u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_28S_oCabSubnet_0dafccc7_Tx                                 133u
#define CanIfConf_CanIfTxPduCfg_CCM_Cab_03P_FCM_Tp_oCabSubnet_af75b436_Tx                           134u
#define CanIfConf_CanIfTxPduCfg_AnmMsg_CIOM_CabSubnet_oCabSubnet_e295c8bd_Tx                        135u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_31P_oCabSubnet_c1a9df69_Tx                                 136u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_24P_oCabSubnet_e2729fa9_Tx                                 137u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_10P_oCabSubnet_a6a5d96b_Tx                                 138u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_26P_oCabSubnet_2df59da9_Tx                                 139u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_23P_oCabSubnet_0e491ee8_Tx                                 140u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_27P_oCabSubnet_4a361ca9_Tx                                 141u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_09P_oCabSubnet_49ff9f68_Tx                                 142u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_33P_oCabSubnet_0e2edd69_Tx                                 143u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_06P_oCabSubnet_2d3a1aab_Tx                                 144u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_03P_oCabSubnet_0e8699ea_Tx                                 145u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_01P_oCabSubnet_c1019bea_Tx                                 146u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_32P_oCabSubnet_69ed5c69_Tx                                 147u
#define CanIfConf_CanIfTxPduCfg_CIOM_Cab_02P_oCabSubnet_694518ea_Tx                                 148u
#define CanIfConf_CanIfTxPduCfg_FMS_X_CIOMFMS_oFMSNet_68f86b37_Tx                                   149u
#define CanIfConf_CanIfTxPduCfg_TPCM_Tp_oFMSNet_55e2f930_Tx                                         150u
#define CanIfConf_CanIfTxPduCfg_TPDT_Tp_oFMSNet_3018618a_Tx                                         151u
#define CanIfConf_CanIfTxPduCfg_VP236_X_CIOMFMS_oFMSNet_524bf569_Tx                                 152u
#define CanIfConf_CanIfTxPduCfg_DD_X_CIOMFMS_oFMSNet_95f62c09_Tx                                    153u
#define CanIfConf_CanIfTxPduCfg_AMB_X_CIOMFMS_oFMSNet_f6109b97_Tx                                   154u
#define CanIfConf_CanIfTxPduCfg_ET1_X_CIOMFMS_oFMSNet_ff39068f_Tx                                   155u
#define CanIfConf_CanIfTxPduCfg_VW_X_CIOMFMS_oFMSNet_06d77621_Tx                                    156u
#define CanIfConf_CanIfTxPduCfg_LFC_X_CIOMFMS_oFMSNet_16e703d3_Tx                                   157u
#define CanIfConf_CanIfTxPduCfg_HOURS_X_CIOMFMS_oFMSNet_bfc991fc_Tx                                 158u
#define CanIfConf_CanIfTxPduCfg_VDHR_X_CIOMFMS_oFMSNet_67a02fb8_Tx                                  159u
#define CanIfConf_CanIfTxPduCfg_SERV_X_CIOMFMS_oFMSNet_0f3b4ff2_Tx                                  160u
#define CanIfConf_CanIfTxPduCfg_AIR1_X_CIOMFMS_oFMSNet_9b1ec537_Tx                                  161u
#define CanIfConf_CanIfTxPduCfg_CVW_X_CIOMFMS_oFMSNet_5e9635e8_Tx                                   162u
#define CanIfConf_CanIfTxPduCfg_TPDirect_0FE6B_Tp_oFMSNet_5c137171_Tx                               163u
#define CanIfConf_CanIfTxPduCfg_AT1T1I1_X_CIOMFMS_oFMSNet_4f6adf75_Tx                               164u
#define CanIfConf_CanIfTxPduCfg_EEC14_X_CIOMFMS_oFMSNet_3551b912_Tx                                 165u
#define CanIfConf_CanIfTxPduCfg_FMS1_X_CIOMFMS_oFMSNet_fb3c9fbd_Tx                                  166u
#define CanIfConf_CanIfTxPduCfg_HRLFC_X_CIOMFMS_oFMSNet_c55a7a2b_Tx                                 167u
#define CanIfConf_CanIfTxPduCfg_J1939NmTxPdu_2068bfea                                               168u
#define CanIfConf_CanIfTxPduCfg_CL_X_CIOMFMS_oFMSNet_fc518547_Tx                                    169u
#define CanIfConf_CanIfTxPduCfg_AckmTxPdu_FMSNet_J1939_44d89c3b                                     170u
#define CanIfConf_CanIfTxPduCfg_LFE_X_CIOMFMS_oFMSNet_adac24c8_Tx                                   171u
#define CanIfConf_CanIfTxPduCfg_CCVS_X_CIOMFMS_oFMSNet_1dbcfa70_Tx                                  172u
#define CanIfConf_CanIfTxPduCfg_PTODE_X_CIOMFMS_oFMSNet_8abc23b8_Tx                                 173u
#define CanIfConf_CanIfTxPduCfg_ERC1_x_RECUFMS_oFMSNet_338e7918_Tx                                  174u
#define CanIfConf_CanIfTxPduCfg_ERC1_x_EMSRetFMS_oFMSNet_d863a207_Tx                                175u
#define CanIfConf_CanIfTxPduCfg_TCO1_X_CIOMFMS_oFMSNet_567baf3d_Tx                                  176u
#define CanIfConf_CanIfTxPduCfg_EEC1_X_CIOMFMS_oFMSNet_954d78de_Tx                                  177u
#define CanIfConf_CanIfTxPduCfg_EEC2_X_CIOMFMS_oFMSNet_d3e60e6a_Tx                                  178u
#define CanIfConf_CanIfTxPduCfg_NmAsr_CIOM_Sec_oSecuritySubnet_dae88b7d_Tx                          179u
#define CanIfConf_CanIfTxPduCfg_PDM_Sec_04S_FCM_Tp_oSecuritySubnet_90a313cf_Tx                      180u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_05S_Tp_oSecuritySubnet_c8b553a6_Tx                         181u
#define CanIfConf_CanIfTxPduCfg_PDM_Sec_03S_FCM_Tp_oSecuritySubnet_2750bd9e_Tx                      182u
#define CanIfConf_CanIfTxPduCfg_DDM_Sec_05S_FCM_Tp_oSecuritySubnet_d14dc345_Tx                      183u
#define CanIfConf_CanIfTxPduCfg_Alarm_Sec_07S_FCM_Tp_oSecuritySubnet_94ec21d2_Tx                    184u
#define CanIfConf_CanIfTxPduCfg_DDM_Sec_04S_FCM_Tp_oSecuritySubnet_eddd4cee_Tx                      185u
#define CanIfConf_CanIfTxPduCfg_Alarm_Sec_06S_FCM_Tp_oSecuritySubnet_a3ee35ea_Tx                    186u
#define CanIfConf_CanIfTxPduCfg_DDM_Sec_03S_FCM_Tp_oSecuritySubnet_5a2ee2bf_Tx                      187u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_07S_Tp_oSecuritySubnet_69506b1a_Tx                         188u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_11S_Tp_oSecuritySubnet_dc8daaf0_Tx                         189u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_09S_Tp_oSecuritySubnet_ba09ccac_Tx                         190u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_06S_Tp_oSecuritySubnet_39a2f744_Tx                         191u
#define CanIfConf_CanIfTxPduCfg_Alarm_Sec_03S_FCM_Tp_oSecuritySubnet_48e47132_Tx                    192u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_08S_Tp_oSecuritySubnet_eafb50f2_Tx                         193u
#define CanIfConf_CanIfTxPduCfg_IntTesterTGW2FuncDiagMsg_Sec_Tp_oSecuritySubnet_57d96404_Tx         194u
#define CanIfConf_CanIfTxPduCfg_TesterFuncDiagMsg_Sec_Tp_oSecuritySubnet_7f5a7347_Tx                195u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_C0_F4_Sec_Tp_oSecuritySubnet_0ba24808_Tx          196u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_C0_F3_Sec_Tp_oSecuritySubnet_3aaf1649_Tx        197u
#define CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_C0_F2_Sec_Tp_oSecuritySubnet_f27248ff_Tx             198u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_A1_F4_Sec_Tp_oSecuritySubnet_eb89cc56_Tx          199u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_A1_F3_Sec_Tp_oSecuritySubnet_a1b03b66_Tx        200u
#define CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_A1_F2_Sec_Tp_oSecuritySubnet_5d2e792a_Tx             201u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntTGW2_A0_F4_Sec_Tp_oSecuritySubnet_8adef0bc_Tx          202u
#define CanIfConf_CanIfTxPduCfg_DiagReqMsgIntHMIIOM_A0_F3_Sec_Tp_oSecuritySubnet_09265670_Tx        203u
#define CanIfConf_CanIfTxPduCfg_PhysDiagReqMsg_A0_F2_Sec_Tp_oSecuritySubnet_cb87d97b_Tx             204u
#define CanIfConf_CanIfTxPduCfg_AnmMsg_CIOM_SecuritySubnet_oSecuritySubnet_d0267e59_Tx              205u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_02P_oSecuritySubnet_04f0ad2a_Tx                            206u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_03P_oSecuritySubnet_4d58f6b7_Tx                            207u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_12S_Tp_oSecuritySubnet_2d9a0e12_Tx                         208u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_10S_Tp_oSecuritySubnet_8c7f36ae_Tx                         209u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_04P_oSecuritySubnet_6a707225_Tx                            210u
#define CanIfConf_CanIfTxPduCfg_CIOM_Sec_01P_oSecuritySubnet_de08418d_Tx                            211u


/**********************************************************************************************************************
  \def  AUTOSAR Rx PDU handles
**********************************************************************************************************************/

#define CanIfConf_CanIfRxPduCfg_TPDT_Tp_oBackbone1J1939_7785b746_Rx                                 0u
#define CanIfConf_CanIfRxPduCfg_TPCM_Tp_oBackbone1J1939_ffabc0bc_Rx                                 1u
#define CanIfConf_CanIfRxPduCfg_RqstRxPdu_Backbone1J1939_54966c1b                                   2u
#define CanIfConf_CanIfRxPduCfg_TPDirect_0FE6B_Tp_oBackbone1J1939_e14130fb_Rx                       3u
#define CanIfConf_CanIfRxPduCfg_J1939NmRxPdu_fa509995                                               4u
#define CanIfConf_CanIfRxPduCfg_EEC1_X_EMS_oBackbone1J1939_5b1d5ab3_Rx                              5u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB1_01P_oBackbone1J1939_c293b9cd_Rx                            6u
#define CanIfConf_CanIfRxPduCfg_EBC2_X_EBS_oBackbone1J1939_416c8820_Rx                              7u
#define CanIfConf_CanIfRxPduCfg_EEC2_X_EMS_oBackbone1J1939_b0cfb70f_Rx                              8u
#define CanIfConf_CanIfRxPduCfg_EBC5_X_EBS_oBackbone1J1939_a226f3b2_Rx                              9u
#define CanIfConf_CanIfRxPduCfg_HRLFC_X_EMS_oBackbone1J1939_5e7f7408_Rx                             10u
#define CanIfConf_CanIfRxPduCfg_TCO1_X_TACHO_oBackbone1J1939_884a7499_Rx                            11u
#define CanIfConf_CanIfRxPduCfg_ACM_BB1_01P_oBackbone1J1939_9e50bfac_Rx                             12u
#define CanIfConf_CanIfRxPduCfg_ERC1_X_RECU_oBackbone1J1939_3104dca7_Rx                             13u
#define CanIfConf_CanIfRxPduCfg_VDC2_X_EBS_oBackbone1J1939_e250e643_Rx                              14u
#define CanIfConf_CanIfRxPduCfg_AIR1_X_VMCU_oBackbone1J1939_30174316_Rx                             15u
#define CanIfConf_CanIfRxPduCfg_CVW_X_EBS_oBackbone1J1939_2dfa9bbe_Rx                               16u
#define CanIfConf_CanIfRxPduCfg_CCVS_X_VMCU_oBackbone1J1939_d16316a3_Rx                             17u
#define CanIfConf_CanIfRxPduCfg_EBS_BB1_05P_oBackbone1J1939_43953999_Rx                             18u
#define CanIfConf_CanIfRxPduCfg_LFE_X_EMS_oBackbone1J1939_592edea5_Rx                               19u
#define CanIfConf_CanIfRxPduCfg_VDC1_X_EBS_oBackbone1J1939_09820bff_Rx                              20u
#define CanIfConf_CanIfRxPduCfg_EBS_BB1_02P_oBackbone1J1939_b875028c_Rx                             21u
#define CanIfConf_CanIfRxPduCfg_ET1_X_EMS_oBackbone1J1939_a6b0a91f_Rx                               22u
#define CanIfConf_CanIfRxPduCfg_VDHR_X_VMCU_oBackbone1J1939_0e2cb4b8_Rx                             23u
#define CanIfConf_CanIfRxPduCfg_EBS_BB1_01P_oBackbone1J1939_9205779e_Rx                             24u
#define CanIfConf_CanIfRxPduCfg_EBC1_X_EBS_oBackbone1J1939_aabe659c_Rx                              25u
#define CanIfConf_CanIfRxPduCfg_AMB_X_VMCU_oBackbone1J1939_d5496e9a_Rx                              26u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB1_03P_oBackbone1J1939_51c30ef7_Rx                            27u
#define CanIfConf_CanIfRxPduCfg_FMS1_X_HMIIOM_oBackbone1J1939_f8c70f73_Rx                           28u
#define CanIfConf_CanIfRxPduCfg_TD_X_HMIIOM_oBackbone1J1939_49e6bcf2_Rx                             29u
#define CanIfConf_CanIfRxPduCfg_ERC1_X_EMSRet_oBackbone1J1939_35cada34_Rx                           30u
#define CanIfConf_CanIfRxPduCfg_CIOM_263e35c1_Rx                                                    31u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntTGW2_C0_F4_BB2_Tp_oBackbone2_92584732_Rx               32u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntHMIIOM_C0_F3_BB2_Tp_oBackbone2_2bec4c32_Rx             33u
#define CanIfConf_CanIfRxPduCfg_PhysDiagReqMsg_C0_F2_BB2_Tp_oBackbone2_bd293e55_Rx                  34u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntTGW2_98_F4_BB2_Tp_oBackbone2_54fed8f2_Rx               35u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntHMIIOM_98_F3_BB2_Tp_oBackbone2_3994cb72_Rx             36u
#define CanIfConf_CanIfRxPduCfg_PhysDiagReqMsg_98_F2_BB2_Tp_oBackbone2_c6fd39ec_Rx                  37u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntTGW2_53_F4_BB2_Tp_oBackbone2_0f9dcb70_Rx               38u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntHMIIOM_53_F3_BB2_Tp_oBackbone2_fd3bad74_Rx             39u
#define CanIfConf_CanIfRxPduCfg_PhysDiagReqMsg_53_F2_BB2_Tp_oBackbone2_1012d7c4_Rx                  40u
#define CanIfConf_CanIfRxPduCfg_XCP_40_FA_BB2_oBackbone2_0da36f9d_Rx                                41u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntTGW2_40_F4_BB2_Tp_oBackbone2_f28a299a_Rx               42u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntHMIIOM_40_F3_BB2_Tp_oBackbone2_a4e3bf32_Rx             43u
#define CanIfConf_CanIfRxPduCfg_PhysDiagReqMsg_40_F2_BB2_Tp_oBackbone2_87192655_Rx                  44u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_ECUspare2_Backbone2_oBackbone2_181d947f_Rx                   45u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_ECUspare1_Backbone2_oBackbone2_c849277d_Rx                   46u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_VMCU_Backbone2_oBackbone2_e6fd44da_Rx                        47u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_TECU_Backbone2_oBackbone2_9bd86e99_Rx                        48u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_HMIIOM_Backbone2_oBackbone2_7a9b7e63_Rx                      49u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_EMS_Backbone2_oBackbone2_5f08a43f_Rx                         50u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_DACU_Backbone2_oBackbone2_fb766361_Rx                        51u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_BBM_Backbone2_oBackbone2_604b553c_Rx                         52u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_17P_oBackbone2_8d736566_Rx                               53u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_16P_oBackbone2_b66be737_Rx                               54u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_14P_oBackbone2_c05ae395_Rx                               55u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_39P_oBackbone2_2c3b8f89_Rx                               56u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_13P_oBackbone2_61116c22_Rx                               57u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_57P_Tp_oBackbone2_925193df_Rx                              58u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_36S_Tp_oBackbone2_95bfb681_Rx                            59u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_20S_Tp_oBackbone2_ce8859ea_Rx                            60u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_22P_oBackbone2_7ef06453_Rx                               61u
#define CanIfConf_CanIfRxPduCfg_PhysDiagReqMsg_53_F1_BB2_Tp_oBackbone2_e289c86d_Rx                  62u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_58P_oBackbone2_0848ab9e_Rx                                 63u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_29P_oBackbone2_6ebc6019_Rx                                 64u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_10P_oBackbone2_2c38ead1_Rx                               65u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_08P_oBackbone2_09b0661b_Rx                                 66u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_25P_oBackbone2_dfbbebe4_Rx                               67u
#define CanIfConf_CanIfRxPduCfg_TECU_BB2_02P_oBackbone2_09dfdabc_Rx                                 68u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_74P_oBackbone2_c4062a5f_Rx                                 69u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_73P_oBackbone2_283dab1e_Rx                                 70u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_34S_Tp_oBackbone2_37bde105_Rx                              71u
#define CanIfConf_CanIfRxPduCfg_TECU_BB2_05S_Tp_oBackbone2_01d5ac34_Rx                              72u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_08P_oBackbone2_33da87f8_Rx                               73u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_21S_Tp_oBackbone2_9d546738_Rx                            74u
#define CanIfConf_CanIfRxPduCfg_EMS_BB2_11S_Tp_oBackbone2_f0eef471_Rx                               75u
#define CanIfConf_CanIfRxPduCfg_CIOM_BB2_13S_FCM_Tp_oBackbone2_57d6a4a9_Rx                          76u
#define CanIfConf_CanIfRxPduCfg_CIOM_BB2_12S_FCM_Tp_oBackbone2_13bf5a99_Rx                          77u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_04S_Tp_oBackbone2_8b957b6f_Rx                            78u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_07P_oBackbone2_6d75e3d8_Rx                                 79u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_07P_oBackbone2_91241c86_Rx                               80u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_24P_oBackbone2_e4a369b5_Rx                               81u
#define CanIfConf_CanIfRxPduCfg_BBM_BB2_06P_oBackbone2_89015c9b_Rx                                  82u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_80P_oBackbone2_82707e91_Rx                                 83u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_20P_oBackbone2_8181e59b_Rx                                 84u
#define CanIfConf_CanIfRxPduCfg_BBM_BB2_02P_oBackbone2_912f6fea_Rx                                  85u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_51P_oBackbone2_e7752e1c_Rx                                 86u
#define CanIfConf_CanIfRxPduCfg_EMS_BB2_01P_oBackbone2_9dd8bb44_Rx                                  87u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_03P_oBackbone2_290ae199_Rx                                 88u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_01P_oBackbone2_e68de399_Rx                                 89u
#define CanIfConf_CanIfRxPduCfg_TECU_BB2_01P_oBackbone2_a19b59bc_Rx                                 90u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_01P_oBackbone2_0b771160_Rx                               91u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_02P_oBackbone2_465e9793_Rx                               92u
#define CanIfConf_CanIfRxPduCfg_IntTesterTGW2FuncDiagMsg_BB2_Tp_oBackbone2_c6fcfce9_Rx              93u
#define CanIfConf_CanIfRxPduCfg_TesterFuncDiagMsg_BB2_Tp_oBackbone2_6ec04186_Rx                     94u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntTGW2_A2_F4_BB2_Tp_oBackbone2_8613fab7_Rx               95u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntHMIIOM_A2_F3_BB2_Tp_oBackbone2_e882bd41_Rx             96u
#define CanIfConf_CanIfRxPduCfg_PhysDiagReqMsg_A2_F2_BB2_Tp_oBackbone2_e7d85c22_Rx                  97u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntTGW2_A1_F4_BB2_Tp_oBackbone2_91dc1a3b_Rx               98u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntHMIIOM_A1_F3_BB2_Tp_oBackbone2_ba062351_Rx             99u
#define CanIfConf_CanIfRxPduCfg_PhysDiagReqMsg_A1_F2_BB2_Tp_oBackbone2_9bf67e6d_Rx                  100u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntTGW2_A0_F4_BB2_Tp_oBackbone2_9c9945bf_Rx               101u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntHMIIOM_A0_F3_BB2_Tp_oBackbone2_3d55549e_Rx             102u
#define CanIfConf_CanIfRxPduCfg_PhysDiagReqMsg_A0_F2_BB2_Tp_oBackbone2_b0139fa8_Rx                  103u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntTGW2_26_F4_BB2_Tp_oBackbone2_ce56ef15_Rx               104u
#define CanIfConf_CanIfRxPduCfg_DiagReqMsgIntHMIIOM_26_F3_BB2_Tp_oBackbone2_3a21aae6_Rx             105u
#define CanIfConf_CanIfRxPduCfg_PhysDiagReqMsg_26_F2_BB2_Tp_oBackbone2_680a80cc_Rx                  106u
#define CanIfConf_CanIfRxPduCfg_DebugCtrl1_CIOM_BB2_oBackbone2_ca351e0d_Rx                          107u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_18P_oBackbone2_2f8dfe18_Rx                               108u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_15P_oBackbone2_fb4261c4_Rx                               109u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_38P_oBackbone2_17230dd8_Rx                               110u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_12P_oBackbone2_5a09ee73_Rx                               111u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_11P_oBackbone2_17206880_Rx                               112u
#define CanIfConf_CanIfRxPduCfg_TECU_BB2_06S_Tp_oBackbone2_0c47cb8e_Rx                              113u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_27S_Tp_oBackbone2_aeede795_Rx                            114u
#define CanIfConf_CanIfRxPduCfg_EMS_BB2_08P_oBackbone2_dd5310ea_Rx                                  115u
#define CanIfConf_CanIfRxPduCfg_EMS_BB2_06P_oBackbone2_148edfe1_Rx                                  116u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_19P_CIOM_Tp_oBackbone2_470ca910_Rx                       117u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_31S_Tp_oBackbone2_210b49cb_Rx                              118u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_32S_Tp_oBackbone2_2c992e71_Rx                              119u
#define CanIfConf_CanIfRxPduCfg_EMS_BB2_09S_Tp_oBackbone2_ef0c9958_Rx                               120u
#define CanIfConf_CanIfRxPduCfg_EMS_BB2_04P_oBackbone2_f5214579_Rx                                  121u
#define CanIfConf_CanIfRxPduCfg_EMS_BB2_13P_oBackbone2_036dfbaa_Rx                                  122u
#define CanIfConf_CanIfRxPduCfg_EMS_BB2_05P_oBackbone2_85f68835_Rx                                  123u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_05P_oBackbone2_e7151824_Rx                               124u
#define CanIfConf_CanIfRxPduCfg_BBM_BB2_03S_CIOM_Tp_oBackbone2_8a8d09f2_Rx                          125u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_55P_oBackbone2_a30a2c5d_Rx                                 126u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_23P_oBackbone2_45e8e602_Rx                               127u
#define CanIfConf_CanIfRxPduCfg_CIOM_BB2_22S_FCM_Tp_oBackbone2_7818b16f_Rx                          128u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_09P_oBackbone2_08c205a9_Rx                               129u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_34S_oBackbone2_6313bb35_Rx                               130u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_35S_Tp_oBackbone2_61dbf5f7_Rx                            131u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_06S_Tp_oBackbone2_2c2d06cb_Rx                            132u
#define CanIfConf_CanIfRxPduCfg_CIOM_BB2_30S_FCM_Tp_oBackbone2_d656145d_Rx                          133u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_54P_oBackbone2_c4c9ad5d_Rx                                 134u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_53P_oBackbone2_28f22c1c_Rx                                 135u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_33P_oBackbone2_59bf9fe2_Rx                               136u
#define CanIfConf_CanIfRxPduCfg_HMIIOM_BB2_03P_oBackbone2_7d4615c2_Rx                               137u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_52P_oBackbone2_4f31ad1c_Rx                                 138u
#define CanIfConf_CanIfRxPduCfg_BBM_BB2_01P_oBackbone2_0057383e_Rx                                  139u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_05P_oBackbone2_a2f2e1d8_Rx                                 140u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_04P_oBackbone2_c53160d8_Rx                                 141u
#define CanIfConf_CanIfRxPduCfg_DACU_BB2_02P_oBackbone2_0b8371c6_Rx                                 142u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_82P_oBackbone2_4df77c91_Rx                                 143u
#define CanIfConf_CanIfRxPduCfg_VMCU_BB2_02P_oBackbone2_4ec96099_Rx                                 144u
#define CanIfConf_CanIfRxPduCfg_Tester_CAN6toLIN_oCAN6_8915fe12_Rx                                  145u
#define CanIfConf_CanIfRxPduCfg_CIOM_c15ca2a0_Rx                                                    146u
#define CanIfConf_CanIfRxPduCfg_WRCS_Cab_01P_oCabSubnet_f823b5c4_Rx                                 147u
#define CanIfConf_CanIfRxPduCfg_DiagUUDTRespMsg1_F2_A2_Cab_oCabSubnet_faf51f79_Rx                   148u
#define CanIfConf_CanIfRxPduCfg_DiagUUDTRespMsg1_F2_98_Cab_oCabSubnet_2175e3c9_Rx                   149u
#define CanIfConf_CanIfRxPduCfg_DiagUUDTRespMsg1_F2_53_Cab_oCabSubnet_d5110ca2_Rx                   150u
#define CanIfConf_CanIfRxPduCfg_DiagUUDTRespMsg1_F2_26_Cab_oCabSubnet_8c457eb7_Rx                   151u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntTGW2_F4_A2_Cab_Tp_oCabSubnet_748ab027_Rx              152u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntTGW2_F4_98_Cab_Tp_oCabSubnet_d506a20a_Rx              153u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntTGW2_F4_53_Cab_Tp_oCabSubnet_781e6a27_Rx              154u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntTGW2_F4_26_Cab_Tp_oCabSubnet_0b131bbc_Rx              155u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntHMIIOM_F3_A2_Cab_Tp_oCabSubnet_15c35ee3_Rx            156u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntHMIIOM_F3_98_Cab_Tp_oCabSubnet_b05496ae_Rx            157u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntHMIIOM_F3_53_Cab_Tp_oCabSubnet_507b9b04_Rx            158u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntHMIIOM_F3_26_Cab_Tp_oCabSubnet_68d46304_Rx            159u
#define CanIfConf_CanIfRxPduCfg_PhysDiagRespMsg_F2_A2_Cab_Tp_oCabSubnet_89846943_Rx                 160u
#define CanIfConf_CanIfRxPduCfg_PhysDiagRespMsg_F2_98_Cab_Tp_oCabSubnet_af4ee02a_Rx                 161u
#define CanIfConf_CanIfRxPduCfg_PhysDiagRespMsg_F2_53_Cab_Tp_oCabSubnet_4c48e914_Rx                 162u
#define CanIfConf_CanIfRxPduCfg_PhysDiagRespMsg_F2_26_Cab_Tp_oCabSubnet_48eedf32_Rx                 163u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_ECUspare6_CabSubnet_oCabSubnet_582cafe8_Rx                   164u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_WRCS_CabSubnet_oCabSubnet_62e3be6f_Rx                        165u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_SRS_CabSubnet_oCabSubnet_690513e9_Rx                         166u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_LECM1_CabSubnet_oCabSubnet_ba3b1140_Rx                       167u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_CCM_CabSubnet_oCabSubnet_7e592096_Rx                         168u
#define CanIfConf_CanIfRxPduCfg_DiagFaultStat_WRCS_Cab_oCabSubnet_b86e4ab7_Rx                       169u
#define CanIfConf_CanIfRxPduCfg_DiagFaultStat_SRS_Cab_oCabSubnet_e5f44b73_Rx                        170u
#define CanIfConf_CanIfRxPduCfg_DiagFaultStat_LECM_Cab_oCabSubnet_6e0076c7_Rx                       171u
#define CanIfConf_CanIfRxPduCfg_DiagFaultStat_CCM_Cab_oCabSubnet_2b754d89_Rx                        172u
#define CanIfConf_CanIfRxPduCfg_CIOM_Cab_20S_FCM_Tp_oCabSubnet_88858df9_Rx                          173u
#define CanIfConf_CanIfRxPduCfg_SRS_Cab_06P_oCabSubnet_bec0c2b2_Rx                                  174u
#define CanIfConf_CanIfRxPduCfg_SRS_Cab_05P_oCabSubnet_2fb89566_Rx                                  175u
#define CanIfConf_CanIfRxPduCfg_CIOM_Cab_34P_FCM_Tp_oCabSubnet_d2bbef9e_Rx                          176u
#define CanIfConf_CanIfRxPduCfg_CIOM_Cab_30S_FCM_Tp_oCabSubnet_ae18d4ab_Rx                          177u
#define CanIfConf_CanIfRxPduCfg_CIOM_Cab_25S_FCM_Tp_oCabSubnet_063a8d48_Rx                          178u
#define CanIfConf_CanIfRxPduCfg_LECM1_Cab_03S_oCabSubnet_e20ca5cb_Rx                                179u
#define CanIfConf_CanIfRxPduCfg_CIOM_Cab_13S_FCM_Tp_oCabSubnet_2f98645f_Rx                          180u
#define CanIfConf_CanIfRxPduCfg_CIOM_Cab_11S_FCM_Tp_oCabSubnet_a74b983f_Rx                          181u
#define CanIfConf_CanIfRxPduCfg_PhysDiagRespMsg_F1_53_Cab_Tp_oCabSubnet_1d55a011_Rx                 182u
#define CanIfConf_CanIfRxPduCfg_SRS_Cab_04P_oCabSubnet_5f6f582a_Rx                                  183u
#define CanIfConf_CanIfRxPduCfg_SRS_Cab_03P_oCabSubnet_d6393c8f_Rx                                  184u
#define CanIfConf_CanIfRxPduCfg_LECM1_Cab_05S_oCabSubnet_b18bf7f9_Rx                                185u
#define CanIfConf_CanIfRxPduCfg_WRCS_Cab_03P_oCabSubnet_37a4b7c4_Rx                                 186u
#define CanIfConf_CanIfRxPduCfg_LECM1_Cab_04P_oCabSubnet_b355ee77_Rx                                187u
#define CanIfConf_CanIfRxPduCfg_LECM1_Cab_02P_oCabSubnet_e0d2bc45_Rx                                188u
#define CanIfConf_CanIfRxPduCfg_SRS_Cab_01P_oCabSubnet_3796a617_Rx                                  189u
#define CanIfConf_CanIfRxPduCfg_CIOM_Cab_29S_FCM_Tp_oCabSubnet_8230888a_Rx                          190u
#define CanIfConf_CanIfRxPduCfg_WRCS_Cab_02P_oCabSubnet_506736c4_Rx                                 191u
#define CanIfConf_CanIfRxPduCfg_CIOM_Cab_05P_FCM_Tp_oCabSubnet_fd75fa58_Rx                          192u
#define CanIfConf_CanIfRxPduCfg_CIOM_Cab_04P_FCM_Tp_oCabSubnet_b91c0468_Rx                          193u
#define CanIfConf_CanIfRxPduCfg_CCM_Cab_06P_oCabSubnet_ae969834_Rx                                  194u
#define CanIfConf_CanIfRxPduCfg_CCM_Cab_07P_oCabSubnet_de415578_Rx                                  195u
#define CanIfConf_CanIfRxPduCfg_CCM_Cab_08P_oCabSubnet_674b573f_Rx                                  196u
#define CanIfConf_CanIfRxPduCfg_CCM_Cab_04P_oCabSubnet_4f3902ac_Rx                                  197u
#define CanIfConf_CanIfRxPduCfg_CCM_Cab_01P_oCabSubnet_27c0fc91_Rx                                  198u
#define CanIfConf_CanIfRxPduCfg_CCM_Cab_03P_Tp_oCabSubnet_bfb45635_Rx                               199u
#define CanIfConf_CanIfRxPduCfg_RqstRxPdu_FMSNet_J1939_44d89c3b                                     200u
#define CanIfConf_CanIfRxPduCfg_FcNPdu_FMSNet_J1939_5ab570f8                                        201u
#define CanIfConf_CanIfRxPduCfg_J1939NmRxPdu_2068bfea                                               202u
#define CanIfConf_CanIfRxPduCfg_VP232_X_ERAU_oFMSNet_8c33d56b_Rx                                    203u
#define CanIfConf_CanIfRxPduCfg_CIOM_0462631a_Rx                                                    204u
#define CanIfConf_CanIfRxPduCfg_DiagUUDTRespMsg1_F2_C0_Sec_oSecuritySubnet_d0c97a87_Rx              205u
#define CanIfConf_CanIfRxPduCfg_DiagUUDTRespMsg1_F2_A1_Sec_oSecuritySubnet_67c5eccf_Rx              206u
#define CanIfConf_CanIfRxPduCfg_DiagUUDTRespMsg1_F2_A0_Sec_oSecuritySubnet_46825d6a_Rx              207u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntTGW2_F4_C0_Sec_Tp_oSecuritySubnet_7a975d32_Rx         208u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntTGW2_F4_A1_Sec_Tp_oSecuritySubnet_efa76b9d_Rx         209u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntTGW2_F4_A0_Sec_Tp_oSecuritySubnet_d9469eac_Rx         210u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntHMIIOM_F3_C0_Sec_Tp_oSecuritySubnet_77f0a36b_Rx       211u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntHMIIOM_F3_A1_Sec_Tp_oSecuritySubnet_052f8796_Rx       212u
#define CanIfConf_CanIfRxPduCfg_DiagRespMsgIntHMIIOM_F3_A0_Sec_Tp_oSecuritySubnet_95b29252_Rx       213u
#define CanIfConf_CanIfRxPduCfg_PhysDiagRespMsg_F2_C0_Sec_Tp_oSecuritySubnet_614a0693_Rx            214u
#define CanIfConf_CanIfRxPduCfg_PhysDiagRespMsg_F2_A1_Sec_Tp_oSecuritySubnet_d47cc691_Rx            215u
#define CanIfConf_CanIfRxPduCfg_PhysDiagRespMsg_F2_A0_Sec_Tp_oSecuritySubnet_207d0e64_Rx            216u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_25af973c_Rx         217u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_PDM_SecuritySubnet_oSecuritySubnet_d922132b_Rx               218u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_DDM_SecuritySubnet_oSecuritySubnet_669221a0_Rx               219u
#define CanIfConf_CanIfRxPduCfg_AnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_38c19358_Rx             220u
#define CanIfConf_CanIfRxPduCfg_DiagFaultStat_PDM_Sec_oSecuritySubnet_b17b40b8_Rx                   221u
#define CanIfConf_CanIfRxPduCfg_DiagFaultStat_DDM_Sec_oSecuritySubnet_3b5b41e4_Rx                   222u
#define CanIfConf_CanIfRxPduCfg_DiagFaultStat_Alarm_Sec_oSecuritySubnet_6cedaf7c_Rx                 223u
#define CanIfConf_CanIfRxPduCfg_PDM_Sec_04S_Tp_oSecuritySubnet_596ba3c3_Rx                          224u
#define CanIfConf_CanIfRxPduCfg_CIOM_Sec_05S_FCM_Tp_oSecuritySubnet_02ea2344_Rx                     225u
#define CanIfConf_CanIfRxPduCfg_PDM_Sec_03S_Tp_oSecuritySubnet_225f8534_Rx                          226u
#define CanIfConf_CanIfRxPduCfg_DDM_Sec_05S_Tp_oSecuritySubnet_5a1ed97b_Rx                          227u
#define CanIfConf_CanIfRxPduCfg_Alarm_Sec_07S_Tp_oSecuritySubnet_bf7e0710_Rx                        228u
#define CanIfConf_CanIfRxPduCfg_DDM_Sec_04S_Tp_oSecuritySubnet_c3efbadc_Rx                          229u
#define CanIfConf_CanIfRxPduCfg_Alarm_Sec_06S_Tp_oSecuritySubnet_2b24d209_Rx                        230u
#define CanIfConf_CanIfRxPduCfg_DDM_Sec_03S_Tp_oSecuritySubnet_b8db9c2b_Rx                          231u
#define CanIfConf_CanIfRxPduCfg_CIOM_Sec_12S_FCM_Tp_oSecuritySubnet_9cba7397_Rx                     232u
#define CanIfConf_CanIfRxPduCfg_CIOM_Sec_07S_FCM_Tp_oSecuritySubnet_5acc46b8_Rx                     233u
#define CanIfConf_CanIfRxPduCfg_CIOM_Sec_11S_FCM_Tp_oSecuritySubnet_e88f2495_Rx                     234u
#define CanIfConf_CanIfRxPduCfg_CIOM_Sec_10S_FCM_Tp_oSecuritySubnet_c49c166b_Rx                     235u
#define CanIfConf_CanIfRxPduCfg_CIOM_Sec_09S_FCM_Tp_oSecuritySubnet_094e790d_Rx                     236u
#define CanIfConf_CanIfRxPduCfg_CIOM_Sec_06S_FCM_Tp_oSecuritySubnet_76df7446_Rx                     237u
#define CanIfConf_CanIfRxPduCfg_Alarm_Sec_03S_Tp_oSecuritySubnet_838659b7_Rx                        238u
#define CanIfConf_CanIfRxPduCfg_CIOM_Sec_08S_FCM_Tp_oSecuritySubnet_255d4bf3_Rx                     239u
#define CanIfConf_CanIfRxPduCfg_PDM_Sec_01P_oSecuritySubnet_073fa36a_Rx                             240u
#define CanIfConf_CanIfRxPduCfg_DDM_Sec_01P_oSecuritySubnet_09e2b30b_Rx                             241u
#define CanIfConf_CanIfRxPduCfg_Alarm_Sec_02P_oSecuritySubnet_a902993f_Rx                           242u



/**********************************************************************************************************************
  \def  Tx buffer type
**********************************************************************************************************************/

#define CanIfTxBufferType                    BYTE_QUEUE

/**********************************************************************************************************************
  \def  For ASR4.0.3/ASR4.2.2 compatibility - indirection macros
**********************************************************************************************************************/
#define  CanIf_RxIndication(Hrh, CanId, CanDlc, CanSduPtr) CanIf_RxIndicationAsr403((Hrh), (CanId), (CanDlc), (CanSduPtr)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */




/**********************************************************************************************************************
  \def  Transceiver handling indirection macros
**********************************************************************************************************************/

#define CanIf_30_GenericCan_TrcvModeIndication(Transceiver, TransceiverMode)                        (CanIf_TrcvModeIndication((Transceiver), TransceiverMode)) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */




/**********************************************************************************************************************
  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
#define CANIF_CFG_MAXRXDLC_PLUS_MAXMETADATA     12u



/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanIfPCDataSwitches  CanIf Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CANIF_BUSOFFNOTIFICATIONFCTPTR                                STD_ON
#define CANIF_CANCHANNELIDUPTOLOWMAP                                  STD_OFF  /**< Deactivateable: 'CanIf_CanChannelIdUpToLowMap' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_CANDRVFCTTBLIDXOFCANCHANNELIDUPTOLOWMAP                 STD_OFF  /**< Deactivateable: 'CanIf_CanChannelIdUpToLowMap.CanDrvFctTblIdx' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_CANDRVFCTTBLUSEDOFCANCHANNELIDUPTOLOWMAP                STD_OFF  /**< Deactivateable: 'CanIf_CanChannelIdUpToLowMap.CanDrvFctTblUsed' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_CHANNELINDEXOFCANCHANNELIDUPTOLOWMAP                    STD_OFF  /**< Deactivateable: 'CanIf_CanChannelIdUpToLowMap.ChannelIndex' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_CANDRVFCTTBL                                            STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_CANCELTXFCTOFCANDRVFCTTBL                               STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.CancelTxFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_CHANGEBAUDRATEFCTOFCANDRVFCTTBL                         STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.ChangeBaudrateFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_CHECKBAUDRATEFCTOFCANDRVFCTTBL                          STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.CheckBaudrateFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_CHECKWAKEUPFCTOFCANDRVFCTTBL                            STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.CheckWakeupFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_GETCONTROLLERERRORSTATEFCTOFCANDRVFCTTBL                STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.GetControllerErrorStateFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_GETCONTROLLERRXERRORCOUNTEROFCANDRVFCTTBL               STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.GetControllerRxErrorCounter' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_GETCONTROLLERTXERRORCOUNTEROFCANDRVFCTTBL               STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.GetControllerTxErrorCounter' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_RAMCHECKENABLECONTROLLERFCTOFCANDRVFCTTBL               STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.RamCheckEnableControllerFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_RAMCHECKENABLEMAILBOXFCTOFCANDRVFCTTBL                  STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.RamCheckEnableMailboxFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_RAMCHECKEXECUTEFCTOFCANDRVFCTTBL                        STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.RamCheckExecuteFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_SETBAUDRATEFCTOFCANDRVFCTTBL                            STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.SetBaudrateFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_SETCONTROLLERMODEFCTOFCANDRVFCTTBL                      STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.SetControllerModeFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_WRITEFCTOFCANDRVFCTTBL                                  STD_OFF  /**< Deactivateable: 'CanIf_CanDrvFctTbl.WriteFct' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_CANIFCTRLID2MAPPEDTXBUFFERSCONFIG                       STD_ON
#define CANIF_INVALIDHNDOFCANIFCTRLID2MAPPEDTXBUFFERSCONFIG           STD_OFF  /**< Deactivateable: 'CanIf_CanIfCtrlId2MappedTxBuffersConfig.InvalidHnd' Reason: 'the value of CanIf_InvalidHndOfCanIfCtrlId2MappedTxBuffersConfig is always 'false' due to this, the array is deactivated.' */
#define CANIF_MAPPEDTXBUFFERSCONFIGENDIDXOFCANIFCTRLID2MAPPEDTXBUFFERSCONFIG STD_ON
#define CANIF_MAPPEDTXBUFFERSCONFIGSTARTIDXOFCANIFCTRLID2MAPPEDTXBUFFERSCONFIG STD_ON
#define CANIF_CANTRCVFCTTBL                                           STD_OFF  /**< Deactivateable: 'CanIf_CanTrcvFctTbl' Reason: 'Transceiver handling and transceiver mapping are deactivated.' */
#define CANIF_CHECKWAKEFLAGFCTOFCANTRCVFCTTBL                         STD_OFF  /**< Deactivateable: 'CanIf_CanTrcvFctTbl.CheckWakeFlagFct' Reason: 'Transceiver handling and transceiver mapping are deactivated.' */
#define CANIF_CHECKWAKEUPFCTOFCANTRCVFCTTBL                           STD_OFF  /**< Deactivateable: 'CanIf_CanTrcvFctTbl.CheckWakeupFct' Reason: 'Transceiver handling and transceiver mapping are deactivated.' */
#define CANIF_CLEARTRCVWUFFLAGFCTOFCANTRCVFCTTBL                      STD_OFF  /**< Deactivateable: 'CanIf_CanTrcvFctTbl.ClearTrcvWufFlagFct' Reason: 'Transceiver handling and transceiver mapping are deactivated.' */
#define CANIF_GETBUSWUREASONFCTOFCANTRCVFCTTBL                        STD_OFF  /**< Deactivateable: 'CanIf_CanTrcvFctTbl.GetBusWuReasonFct' Reason: 'Transceiver handling and transceiver mapping are deactivated.' */
#define CANIF_GETOPMODEFCTOFCANTRCVFCTTBL                             STD_OFF  /**< Deactivateable: 'CanIf_CanTrcvFctTbl.GetOpModeFct' Reason: 'Transceiver handling and transceiver mapping are deactivated.' */
#define CANIF_SETOPMODEFCTOFCANTRCVFCTTBL                             STD_OFF  /**< Deactivateable: 'CanIf_CanTrcvFctTbl.SetOpModeFct' Reason: 'Transceiver handling and transceiver mapping are deactivated.' */
#define CANIF_SETWAKEUPMODEFCTOFCANTRCVFCTTBL                         STD_OFF  /**< Deactivateable: 'CanIf_CanTrcvFctTbl.SetWakeupModeFct' Reason: 'Transceiver handling and transceiver mapping are deactivated.' */
#define CANIF_CTRLCONFIG                                              STD_OFF  /**< Deactivateable: 'CanIf_CtrlConfig' Reason: 'Table: CanIf_CtrlConfig[] is not required.' */
#define CANIF_FEATUREPNWUTXPDUFILTERENABLEDOFCTRLCONFIG               STD_OFF  /**< Deactivateable: 'CanIf_CtrlConfig.FeaturePnWuTxPduFilterEnabled' Reason: 'Table: CanIf_CtrlConfig[] is not required.' */
#define CANIF_J1939DYNADDROFFSETOFCTRLCONFIG                          STD_OFF  /**< Deactivateable: 'CanIf_CtrlConfig.J1939DynAddrOffset' Reason: 'Table: CanIf_CtrlConfig[] is not required.' */
#define CANIF_J1939DYNADDRSUPPORTOFCTRLCONFIG                         STD_OFF  /**< Deactivateable: 'CanIf_CtrlConfig.J1939DynAddrSupport' Reason: 'Table: CanIf_CtrlConfig[] is not required.' */
#define CANIF_RXDHRANDOMNUMBER1OFCTRLCONFIG                           STD_OFF  /**< Deactivateable: 'CanIf_CtrlConfig.RxDHRandomNumber1' Reason: 'Table: CanIf_CtrlConfig[] is not required.' */
#define CANIF_RXDHRANDOMNUMBER2OFCTRLCONFIG                           STD_OFF  /**< Deactivateable: 'CanIf_CtrlConfig.RxDHRandomNumber2' Reason: 'Table: CanIf_CtrlConfig[] is not required.' */
#define CANIF_CTRLMODEINDICATIONFCTPTR                                STD_ON
#define CANIF_CTRLSTATES                                              STD_ON
#define CANIF_CTRLMODEOFCTRLSTATES                                    STD_ON
#define CANIF_PDUMODEOFCTRLSTATES                                     STD_ON
#define CANIF_FINALMAGICNUMBER                                        STD_OFF  /**< Deactivateable: 'CanIf_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define CANIF_GENERATORCOMPATIBILITYVERSION                           STD_ON
#define CANIF_GENERATORVERSION                                        STD_ON
#define CANIF_HXHOFFSET                                               STD_OFF  /**< Deactivateable: 'CanIf_HxhOffset' Reason: 'Multiple CAN driver support is deactivated.' */
#define CANIF_INITDATAHASHCODE                                        STD_OFF  /**< Deactivateable: 'CanIf_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define CANIF_MAILBOXCONFIG                                           STD_ON
#define CANIF_CTRLSTATESIDXOFMAILBOXCONFIG                            STD_ON
#define CANIF_MAILBOXTYPEOFMAILBOXCONFIG                              STD_ON
#define CANIF_PDUIDFIRSTOFMAILBOXCONFIG                               STD_ON
#define CANIF_PDUIDLASTOFMAILBOXCONFIG                                STD_ON
#define CANIF_TXBUFFERCFGIDXOFMAILBOXCONFIG                           STD_ON
#define CANIF_TXBUFFERCFGUSEDOFMAILBOXCONFIG                          STD_ON
#define CANIF_TXBUFFERHANDLINGTYPEOFMAILBOXCONFIG                     STD_ON
#define CANIF_MAPPEDTXBUFFERSCONFIG                                   STD_ON
#define CANIF_MAILBOXCONFIGIDXOFMAPPEDTXBUFFERSCONFIG                 STD_ON
#define CANIF_MAXTRCVHANDLEIDPLUSONE                                  STD_ON
#define CANIF_PDURXMODE                                               STD_OFF  /**< Deactivateable: 'CanIf_PduRxMode' Reason: 'the array is deactivated because the size is 0 in all variants and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANIF_RXDHADJUST                                              STD_OFF  /**< Deactivateable: 'CanIf_RxDHAdjust' Reason: 'Search algorithm "double hash" is deactivated.' */
#define CANIF_RXINDICATIONFCTLIST                                     STD_ON
#define CANIF_RXINDICATIONFCTOFRXINDICATIONFCTLIST                    STD_ON
#define CANIF_RXINDICATIONLAYOUTOFRXINDICATIONFCTLIST                 STD_ON
#define CANIF_RXPDUCONFIG                                             STD_ON
#define CANIF_CANIFRXPDUIDOFRXPDUCONFIG                               STD_OFF  /**< Deactivateable: 'CanIf_RxPduConfig.CanIfRxPduId' Reason: '"CanIfDataChecksumRxSupport" is deactivated.' */
#define CANIF_DLCOFRXPDUCONFIG                                        STD_ON
#define CANIF_ISDATACHECKSUMRXPDUOFRXPDUCONFIG                        STD_OFF  /**< Deactivateable: 'CanIf_RxPduConfig.IsDataChecksumRxPdu' Reason: '"CanIfDataChecksumRxSupport" is deactivated.' */
#define CANIF_MSGTYPEOFRXPDUCONFIG                                    STD_OFF  /**< Deactivateable: 'CanIf_RxPduConfig.MsgType' Reason: '"CanIfRxSearchConsiderMsgType" is deactivated.' */
#define CANIF_RXINDICATIONFCTLISTIDXOFRXPDUCONFIG                     STD_ON
#define CANIF_RXMETADATALENGTHOFRXPDUCONFIG                           STD_ON
#define CANIF_RXPDUCANIDOFRXPDUCONFIG                                 STD_ON
#define CANIF_RXPDUMASKOFRXPDUCONFIG                                  STD_ON
#define CANIF_UPPERPDUIDOFRXPDUCONFIG                                 STD_ON
#define CANIF_SIZEOFCANIFCTRLID2MAPPEDTXBUFFERSCONFIG                 STD_ON
#define CANIF_SIZEOFCTRLSTATES                                        STD_ON
#define CANIF_SIZEOFMAILBOXCONFIG                                     STD_ON
#define CANIF_SIZEOFMAPPEDTXBUFFERSCONFIG                             STD_ON
#define CANIF_SIZEOFRXINDICATIONFCTLIST                               STD_ON
#define CANIF_SIZEOFRXPDUCONFIG                                       STD_ON
#define CANIF_SIZEOFTRCVTOCTRLMAP                                     STD_ON
#define CANIF_SIZEOFTXBUFFERPRIOBYCANIDBASE                           STD_ON
#define CANIF_SIZEOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG                STD_ON
#define CANIF_SIZEOFTXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUS          STD_ON
#define CANIF_SIZEOFTXCONFIRMATIONFCTLIST                             STD_ON
#define CANIF_SIZEOFTXPDUCONFIG                                       STD_ON
#define CANIF_SIZEOFTXPDUQUEUEINDEX                                   STD_ON
#define CANIF_SIZEOFTXQUEUE                                           STD_ON
#define CANIF_TRANSCEIVERUPTOLOWMAP                                   STD_OFF  /**< Deactivateable: 'CanIf_TransceiverUpToLowMap' Reason: 'Transceiver handling and transceiver mapping are deactivated and one controller optimization is activated.' */
#define CANIF_CANTRCVFCTTBLIDXOFTRANSCEIVERUPTOLOWMAP                 STD_OFF  /**< Deactivateable: 'CanIf_TransceiverUpToLowMap.CanTrcvFctTblIdx' Reason: 'Transceiver handling and transceiver mapping are deactivated and one controller optimization is activated.' */
#define CANIF_CHANNELINDEXOFTRANSCEIVERUPTOLOWMAP                     STD_OFF  /**< Deactivateable: 'CanIf_TransceiverUpToLowMap.ChannelIndex' Reason: 'Transceiver handling and transceiver mapping are deactivated and one controller optimization is activated.' */
#define CANIF_TRANSCEIVERUPTOUPPERMAP                                 STD_OFF  /**< Deactivateable: 'CanIf_TransceiverUpToUpperMap' Reason: 'Transceiver handling and transceiver mapping are deactivated and one controller optimization is activated.' */
#define CANIF_TRCVMODEINDICATIONFCTPTR                                STD_ON
#define CANIF_TRCVTOCTRLMAP                                           STD_ON
#define CANIF_TXBUFFERFIFOCONFIG                                      STD_OFF  /**< Deactivateable: 'CanIf_TxBufferFifoConfig' Reason: 'Tx-buffer FIFO support is deactivated.' */
#define CANIF_SIZEOFONEPAYLOADELOFTXBUFFERFIFOCONFIG                  STD_OFF  /**< Deactivateable: 'CanIf_TxBufferFifoConfig.SizeOfOnePayloadEl' Reason: 'Tx-buffer FIFO support is deactivated.' */
#define CANIF_TXBUFFERPRIOBYCANIDBASE                                 STD_ON
#define CANIF_TXBUFFERPRIOBYCANIDBITQUEUECONFIG                       STD_OFF  /**< Deactivateable: 'CanIf_TxBufferPrioByCanIdBitQueueConfig' Reason: 'Tx-buffer PRIO_BY_CANID support as BIT_QUEUE is deactivated.' */
#define CANIF_BITPOS2TXPDUIDOFFSETOFTXBUFFERPRIOBYCANIDBITQUEUECONFIG STD_OFF  /**< Deactivateable: 'CanIf_TxBufferPrioByCanIdBitQueueConfig.BitPos2TxPduIdOffset' Reason: 'Tx-buffer PRIO_BY_CANID support as BIT_QUEUE is deactivated.' */
#define CANIF_TXBUFFERPRIOBYCANIDBYTEQUEUECONFIG                      STD_ON
#define CANIF_TXBUFFERPRIOBYCANIDBASEIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG STD_ON
#define CANIF_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSENDIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG STD_ON
#define CANIF_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSLENGTHOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG STD_ON
#define CANIF_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSSTARTIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG STD_ON
#define CANIF_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUS                STD_ON
#define CANIF_TXPDUCONFIGIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUS STD_ON
#define CANIF_TXCONFIRMATIONFCTLIST                                   STD_ON
#define CANIF_TXPDUCONFIG                                             STD_ON
#define CANIF_CANIDOFTXPDUCONFIG                                      STD_ON
#define CANIF_CANIDTXMASKOFTXPDUCONFIG                                STD_ON
#define CANIF_CTRLSTATESIDXOFTXPDUCONFIG                              STD_ON
#define CANIF_DLCOFTXPDUCONFIG                                        STD_ON
#define CANIF_ISDATACHECKSUMTXPDUOFTXPDUCONFIG                        STD_OFF  /**< Deactivateable: 'CanIf_TxPduConfig.IsDataChecksumTxPdu' Reason: '"CanIfDataChecksumTxSupport" is deactivated.' */
#define CANIF_MAILBOXCONFIGIDXOFTXPDUCONFIG                           STD_ON
#define CANIF_TXCONFIRMATIONFCTLISTIDXOFTXPDUCONFIG                   STD_ON
#define CANIF_TXMETADATALENGTHOFTXPDUCONFIG                           STD_ON
#define CANIF_UPPERLAYERTXPDUIDOFTXPDUCONFIG                          STD_ON
#define CANIF_TXPDUQUEUEINDEX                                         STD_ON
#define CANIF_TXQUEUEIDXOFTXPDUQUEUEINDEX                             STD_ON
#define CANIF_TXQUEUEUSEDOFTXPDUQUEUEINDEX                            STD_ON
#define CANIF_TXPDUSTATIC2DYNAMICINDIRECTION                          STD_OFF  /**< Deactivateable: 'CanIf_TxPduStatic2DynamicIndirection' Reason: 'Dynamic Tx-CanId support is deactivated.' */
#define CANIF_TXQUEUE                                                 STD_ON
#define CANIF_TXQUEUEINDEX2DATASTARTSTOP                              STD_OFF  /**< Deactivateable: 'CanIf_TxQueueIndex2DataStartStop' Reason: 'Static FD Tx-buffer is deactivated.' */
#define CANIF_ULRXPDUID2INTERNALRXPDUID                               STD_OFF  /**< Deactivateable: 'CanIf_UlRxPduId2InternalRxPduId' Reason: 'Configuration variant is Pre-compile and the feature to be able to set the reception mode of a Rx-PDU at runtime is disabled!' */
#define CANIF_RXPDUCONFIGIDXOFULRXPDUID2INTERNALRXPDUID               STD_OFF  /**< Deactivateable: 'CanIf_UlRxPduId2InternalRxPduId.RxPduConfigIdx' Reason: 'Configuration variant is Pre-compile and the feature to be able to set the reception mode of a Rx-PDU at runtime is disabled!' */
#define CANIF_RXPDUCONFIGUSEDOFULRXPDUID2INTERNALRXPDUID              STD_OFF  /**< Deactivateable: 'CanIf_UlRxPduId2InternalRxPduId.RxPduConfigUsed' Reason: 'Configuration variant is Pre-compile and the feature to be able to set the reception mode of a Rx-PDU at runtime is disabled!' */
#define CANIF_ULTXPDUID2INTERNALTXPDUID                               STD_OFF  /**< Deactivateable: 'CanIf_UlTxPduId2InternalTxPduId' Reason: 'Configuration variant is Pre-compile!' */
#define CANIF_WAKEUPCONFIG                                            STD_OFF  /**< Deactivateable: 'CanIf_WakeUpConfig' Reason: 'Wake-up support is deactivated.' */
#define CANIF_CONTROLLEROFWAKEUPCONFIG                                STD_OFF  /**< Deactivateable: 'CanIf_WakeUpConfig.Controller' Reason: 'Wake-up support is deactivated.' */
#define CANIF_WAKEUPSOURCEOFWAKEUPCONFIG                              STD_OFF  /**< Deactivateable: 'CanIf_WakeUpConfig.WakeUpSource' Reason: 'Wake-up support is deactivated.' */
#define CANIF_WAKEUPTARGETADDRESSOFWAKEUPCONFIG                       STD_OFF  /**< Deactivateable: 'CanIf_WakeUpConfig.WakeUpTargetAddress' Reason: 'Wake-up support is deactivated.' */
#define CANIF_WAKEUPTARGETMODULEOFWAKEUPCONFIG                        STD_OFF  /**< Deactivateable: 'CanIf_WakeUpConfig.WakeUpTargetModule' Reason: 'Wake-up support is deactivated.' */
#define CANIF_PCCONFIG                                                STD_ON
#define CANIF_BUSOFFNOTIFICATIONFCTPTROFPCCONFIG                      STD_ON
#define CANIF_CANIFCTRLID2MAPPEDTXBUFFERSCONFIGOFPCCONFIG             STD_ON
#define CANIF_CTRLMODEINDICATIONFCTPTROFPCCONFIG                      STD_ON
#define CANIF_CTRLSTATESOFPCCONFIG                                    STD_ON
#define CANIF_FINALMAGICNUMBEROFPCCONFIG                              STD_OFF  /**< Deactivateable: 'CanIf_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define CANIF_GENERATORCOMPATIBILITYVERSIONOFPCCONFIG                 STD_ON
#define CANIF_GENERATORVERSIONOFPCCONFIG                              STD_ON
#define CANIF_INITDATAHASHCODEOFPCCONFIG                              STD_OFF  /**< Deactivateable: 'CanIf_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define CANIF_MAILBOXCONFIGOFPCCONFIG                                 STD_ON
#define CANIF_MAPPEDTXBUFFERSCONFIGOFPCCONFIG                         STD_ON
#define CANIF_MAXTRCVHANDLEIDPLUSONEOFPCCONFIG                        STD_ON
#define CANIF_RXINDICATIONFCTLISTOFPCCONFIG                           STD_ON
#define CANIF_RXPDUCONFIGOFPCCONFIG                                   STD_ON
#define CANIF_SIZEOFCANIFCTRLID2MAPPEDTXBUFFERSCONFIGOFPCCONFIG       STD_ON
#define CANIF_SIZEOFCTRLSTATESOFPCCONFIG                              STD_ON
#define CANIF_SIZEOFMAILBOXCONFIGOFPCCONFIG                           STD_ON
#define CANIF_SIZEOFMAPPEDTXBUFFERSCONFIGOFPCCONFIG                   STD_ON
#define CANIF_SIZEOFRXINDICATIONFCTLISTOFPCCONFIG                     STD_ON
#define CANIF_SIZEOFRXPDUCONFIGOFPCCONFIG                             STD_ON
#define CANIF_SIZEOFTRCVTOCTRLMAPOFPCCONFIG                           STD_ON
#define CANIF_SIZEOFTXBUFFERPRIOBYCANIDBASEOFPCCONFIG                 STD_ON
#define CANIF_SIZEOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIGOFPCCONFIG      STD_ON
#define CANIF_SIZEOFTXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSOFPCCONFIG STD_ON
#define CANIF_SIZEOFTXCONFIRMATIONFCTLISTOFPCCONFIG                   STD_ON
#define CANIF_SIZEOFTXPDUCONFIGOFPCCONFIG                             STD_ON
#define CANIF_SIZEOFTXPDUQUEUEINDEXOFPCCONFIG                         STD_ON
#define CANIF_SIZEOFTXQUEUEOFPCCONFIG                                 STD_ON
#define CANIF_TRCVMODEINDICATIONFCTPTROFPCCONFIG                      STD_ON
#define CANIF_TRCVTOCTRLMAPOFPCCONFIG                                 STD_ON
#define CANIF_TXBUFFERPRIOBYCANIDBASEOFPCCONFIG                       STD_ON
#define CANIF_TXBUFFERPRIOBYCANIDBYTEQUEUECONFIGOFPCCONFIG            STD_ON
#define CANIF_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSOFPCCONFIG      STD_ON
#define CANIF_TXCONFIRMATIONFCTLISTOFPCCONFIG                         STD_ON
#define CANIF_TXPDUCONFIGOFPCCONFIG                                   STD_ON
#define CANIF_TXPDUQUEUEINDEXOFPCCONFIG                               STD_ON
#define CANIF_TXQUEUEOFPCCONFIG                                       STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCNoReferenceDefines  CanIf No Reference Defines (PRE_COMPILE)
  \brief  These defines are used to indicate unused indexes in data relations.
  \{
*/ 
#define CANIF_NO_TXBUFFERCFGIDXOFMAILBOXCONFIG                        255u
#define CANIF_NO_TXQUEUEIDXOFTXPDUQUEUEINDEX                          255u
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCIsReducedToDefineDefines  CanIf Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define CANIF_ISDEF_MAPPEDTXBUFFERSCONFIGENDIDXOFCANIFCTRLID2MAPPEDTXBUFFERSCONFIG STD_OFF
#define CANIF_ISDEF_MAPPEDTXBUFFERSCONFIGSTARTIDXOFCANIFCTRLID2MAPPEDTXBUFFERSCONFIG STD_OFF
#define CANIF_ISDEF_CTRLSTATESIDXOFMAILBOXCONFIG                      STD_OFF
#define CANIF_ISDEF_MAILBOXTYPEOFMAILBOXCONFIG                        STD_OFF
#define CANIF_ISDEF_PDUIDFIRSTOFMAILBOXCONFIG                         STD_OFF
#define CANIF_ISDEF_PDUIDLASTOFMAILBOXCONFIG                          STD_OFF
#define CANIF_ISDEF_TXBUFFERCFGIDXOFMAILBOXCONFIG                     STD_OFF
#define CANIF_ISDEF_TXBUFFERCFGUSEDOFMAILBOXCONFIG                    STD_OFF
#define CANIF_ISDEF_TXBUFFERHANDLINGTYPEOFMAILBOXCONFIG               STD_OFF
#define CANIF_ISDEF_MAILBOXCONFIGIDXOFMAPPEDTXBUFFERSCONFIG           STD_OFF
#define CANIF_ISDEF_RXINDICATIONFCTOFRXINDICATIONFCTLIST              STD_OFF
#define CANIF_ISDEF_RXINDICATIONLAYOUTOFRXINDICATIONFCTLIST           STD_OFF
#define CANIF_ISDEF_DLCOFRXPDUCONFIG                                  STD_OFF
#define CANIF_ISDEF_RXINDICATIONFCTLISTIDXOFRXPDUCONFIG               STD_OFF
#define CANIF_ISDEF_RXMETADATALENGTHOFRXPDUCONFIG                     STD_OFF
#define CANIF_ISDEF_RXPDUCANIDOFRXPDUCONFIG                           STD_OFF
#define CANIF_ISDEF_RXPDUMASKOFRXPDUCONFIG                            STD_OFF
#define CANIF_ISDEF_UPPERPDUIDOFRXPDUCONFIG                           STD_OFF
#define CANIF_ISDEF_TRCVTOCTRLMAP                                     STD_OFF
#define CANIF_ISDEF_TXBUFFERPRIOBYCANIDBASEIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG STD_OFF
#define CANIF_ISDEF_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSENDIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG STD_OFF
#define CANIF_ISDEF_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSLENGTHOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG STD_OFF
#define CANIF_ISDEF_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSSTARTIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG STD_OFF
#define CANIF_ISDEF_TXPDUCONFIGIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUS STD_OFF
#define CANIF_ISDEF_TXCONFIRMATIONFCTLIST                             STD_OFF
#define CANIF_ISDEF_CANIDOFTXPDUCONFIG                                STD_OFF
#define CANIF_ISDEF_CANIDTXMASKOFTXPDUCONFIG                          STD_OFF
#define CANIF_ISDEF_CTRLSTATESIDXOFTXPDUCONFIG                        STD_OFF
#define CANIF_ISDEF_DLCOFTXPDUCONFIG                                  STD_OFF
#define CANIF_ISDEF_MAILBOXCONFIGIDXOFTXPDUCONFIG                     STD_OFF
#define CANIF_ISDEF_TXCONFIRMATIONFCTLISTIDXOFTXPDUCONFIG             STD_OFF
#define CANIF_ISDEF_TXMETADATALENGTHOFTXPDUCONFIG                     STD_OFF
#define CANIF_ISDEF_UPPERLAYERTXPDUIDOFTXPDUCONFIG                    STD_OFF
#define CANIF_ISDEF_TXQUEUEIDXOFTXPDUQUEUEINDEX                       STD_OFF
#define CANIF_ISDEF_TXQUEUEUSEDOFTXPDUQUEUEINDEX                      STD_OFF
#define CANIF_ISDEF_BUSOFFNOTIFICATIONFCTPTROFPCCONFIG                STD_ON
#define CANIF_ISDEF_CANIFCTRLID2MAPPEDTXBUFFERSCONFIGOFPCCONFIG       STD_ON
#define CANIF_ISDEF_CTRLMODEINDICATIONFCTPTROFPCCONFIG                STD_ON
#define CANIF_ISDEF_CTRLSTATESOFPCCONFIG                              STD_ON
#define CANIF_ISDEF_MAILBOXCONFIGOFPCCONFIG                           STD_ON
#define CANIF_ISDEF_MAPPEDTXBUFFERSCONFIGOFPCCONFIG                   STD_ON
#define CANIF_ISDEF_RXINDICATIONFCTLISTOFPCCONFIG                     STD_ON
#define CANIF_ISDEF_RXPDUCONFIGOFPCCONFIG                             STD_ON
#define CANIF_ISDEF_TRCVMODEINDICATIONFCTPTROFPCCONFIG                STD_ON
#define CANIF_ISDEF_TRCVTOCTRLMAPOFPCCONFIG                           STD_ON
#define CANIF_ISDEF_TXBUFFERPRIOBYCANIDBASEOFPCCONFIG                 STD_ON
#define CANIF_ISDEF_TXBUFFERPRIOBYCANIDBYTEQUEUECONFIGOFPCCONFIG      STD_ON
#define CANIF_ISDEF_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSOFPCCONFIG STD_ON
#define CANIF_ISDEF_TXCONFIRMATIONFCTLISTOFPCCONFIG                   STD_ON
#define CANIF_ISDEF_TXPDUCONFIGOFPCCONFIG                             STD_ON
#define CANIF_ISDEF_TXPDUQUEUEINDEXOFPCCONFIG                         STD_ON
#define CANIF_ISDEF_TXQUEUEOFPCCONFIG                                 STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCEqualsAlwaysToDefines  CanIf Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define CANIF_EQ2_MAPPEDTXBUFFERSCONFIGENDIDXOFCANIFCTRLID2MAPPEDTXBUFFERSCONFIG 
#define CANIF_EQ2_MAPPEDTXBUFFERSCONFIGSTARTIDXOFCANIFCTRLID2MAPPEDTXBUFFERSCONFIG 
#define CANIF_EQ2_CTRLSTATESIDXOFMAILBOXCONFIG                        
#define CANIF_EQ2_MAILBOXTYPEOFMAILBOXCONFIG                          
#define CANIF_EQ2_PDUIDFIRSTOFMAILBOXCONFIG                           
#define CANIF_EQ2_PDUIDLASTOFMAILBOXCONFIG                            
#define CANIF_EQ2_TXBUFFERCFGIDXOFMAILBOXCONFIG                       
#define CANIF_EQ2_TXBUFFERCFGUSEDOFMAILBOXCONFIG                      
#define CANIF_EQ2_TXBUFFERHANDLINGTYPEOFMAILBOXCONFIG                 
#define CANIF_EQ2_MAILBOXCONFIGIDXOFMAPPEDTXBUFFERSCONFIG             
#define CANIF_EQ2_RXINDICATIONFCTOFRXINDICATIONFCTLIST                
#define CANIF_EQ2_RXINDICATIONLAYOUTOFRXINDICATIONFCTLIST             
#define CANIF_EQ2_DLCOFRXPDUCONFIG                                    
#define CANIF_EQ2_RXINDICATIONFCTLISTIDXOFRXPDUCONFIG                 
#define CANIF_EQ2_RXMETADATALENGTHOFRXPDUCONFIG                       
#define CANIF_EQ2_RXPDUCANIDOFRXPDUCONFIG                             
#define CANIF_EQ2_RXPDUMASKOFRXPDUCONFIG                              
#define CANIF_EQ2_UPPERPDUIDOFRXPDUCONFIG                             
#define CANIF_EQ2_TRCVTOCTRLMAP                                       
#define CANIF_EQ2_TXBUFFERPRIOBYCANIDBASEIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG 
#define CANIF_EQ2_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSENDIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG 
#define CANIF_EQ2_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSLENGTHOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG 
#define CANIF_EQ2_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSSTARTIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUECONFIG 
#define CANIF_EQ2_TXPDUCONFIGIDXOFTXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUS 
#define CANIF_EQ2_TXCONFIRMATIONFCTLIST                               
#define CANIF_EQ2_CANIDOFTXPDUCONFIG                                  
#define CANIF_EQ2_CANIDTXMASKOFTXPDUCONFIG                            
#define CANIF_EQ2_CTRLSTATESIDXOFTXPDUCONFIG                          
#define CANIF_EQ2_DLCOFTXPDUCONFIG                                    
#define CANIF_EQ2_MAILBOXCONFIGIDXOFTXPDUCONFIG                       
#define CANIF_EQ2_TXCONFIRMATIONFCTLISTIDXOFTXPDUCONFIG               
#define CANIF_EQ2_TXMETADATALENGTHOFTXPDUCONFIG                       
#define CANIF_EQ2_UPPERLAYERTXPDUIDOFTXPDUCONFIG                      
#define CANIF_EQ2_TXQUEUEIDXOFTXPDUQUEUEINDEX                         
#define CANIF_EQ2_TXQUEUEUSEDOFTXPDUQUEUEINDEX                        
#define CANIF_EQ2_BUSOFFNOTIFICATIONFCTPTROFPCCONFIG                  CanIf_BusOffNotificationFctPtr
#define CANIF_EQ2_CANIFCTRLID2MAPPEDTXBUFFERSCONFIGOFPCCONFIG         CanIf_CanIfCtrlId2MappedTxBuffersConfig
#define CANIF_EQ2_CTRLMODEINDICATIONFCTPTROFPCCONFIG                  CanIf_CtrlModeIndicationFctPtr
#define CANIF_EQ2_CTRLSTATESOFPCCONFIG                                CanIf_CtrlStates.raw
#define CANIF_EQ2_MAILBOXCONFIGOFPCCONFIG                             CanIf_MailBoxConfig
#define CANIF_EQ2_MAPPEDTXBUFFERSCONFIGOFPCCONFIG                     CanIf_MappedTxBuffersConfig
#define CANIF_EQ2_RXINDICATIONFCTLISTOFPCCONFIG                       CanIf_RxIndicationFctList
#define CANIF_EQ2_RXPDUCONFIGOFPCCONFIG                               CanIf_RxPduConfig
#define CANIF_EQ2_TRCVMODEINDICATIONFCTPTROFPCCONFIG                  CanIf_TrcvModeIndicationFctPtr
#define CANIF_EQ2_TRCVTOCTRLMAPOFPCCONFIG                             CanIf_TrcvToCtrlMap
#define CANIF_EQ2_TXBUFFERPRIOBYCANIDBASEOFPCCONFIG                   CanIf_TxBufferPrioByCanIdBase.raw
#define CANIF_EQ2_TXBUFFERPRIOBYCANIDBYTEQUEUECONFIGOFPCCONFIG        CanIf_TxBufferPrioByCanIdByteQueueConfig
#define CANIF_EQ2_TXBUFFERPRIOBYCANIDBYTEQUEUEMAPPEDTXPDUSOFPCCONFIG  CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
#define CANIF_EQ2_TXCONFIRMATIONFCTLISTOFPCCONFIG                     CanIf_TxConfirmationFctList
#define CANIF_EQ2_TXPDUCONFIGOFPCCONFIG                               CanIf_TxPduConfig
#define CANIF_EQ2_TXPDUQUEUEINDEXOFPCCONFIG                           CanIf_TxPduQueueIndex
#define CANIF_EQ2_TXQUEUEOFPCCONFIG                                   CanIf_TxQueue.raw
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCSymbolicInitializationPointers  CanIf Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define CanIf_Config_Ptr                                              NULL_PTR  /**< symbolic identifier which shall be used to initialize 'CanIf' */
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCInitializationSymbols  CanIf Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define CanIf_Config                                                  NULL_PTR  /**< symbolic identifier which could be used to initialize 'CanIf */
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCGeneral  CanIf General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define CANIF_CHECK_INIT_POINTER                                      STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define CANIF_FINAL_MAGIC_NUMBER                                      0x3C1Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of CanIf */
#define CANIF_INDIVIDUAL_POSTBUILD                                    STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'CanIf' is not configured to be postbuild capable. */
#define CANIF_INIT_DATA                                               CANIF_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define CANIF_INIT_DATA_HASH_CODE                                     -713533440  /**< the precompile constant to validate the initialization structure at initialization time of CanIf with a hashcode. The seed value is '0x3C1Eu' */
#define CANIF_USE_ECUM_BSW_ERROR_HOOK                                 STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define CANIF_USE_INIT_POINTER                                        STD_OFF  /**< STD_ON if the init pointer CanIf shall be used. */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanIfLTDataSwitches  CanIf Data Switches  (LINK)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CANIF_LTCONFIG                                                STD_OFF  /**< Deactivateable: 'CanIf_LTConfig' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanIfPBDataSwitches  CanIf Data Switches  (POST_BUILD)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CANIF_PBCONFIG                                                STD_OFF  /**< Deactivateable: 'CanIf_PBConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define CANIF_LTCONFIGIDXOFPBCONFIG                                   STD_OFF  /**< Deactivateable: 'CanIf_PBConfig.LTConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define CANIF_PCCONFIGIDXOFPBCONFIG                                   STD_OFF  /**< Deactivateable: 'CanIf_PBConfig.PCConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
/** 
  \}
*/ 



/**********************************************************************************************************************
  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanIfPCGetConstantDuplicatedRootDataMacros  CanIf Get Constant Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated by constance root data elements.
  \{
*/ 
#define CanIf_GetBusOffNotificationFctPtrOfPCConfig()                 CanIf_BusOffNotificationFctPtr  /**< the pointer to CanIf_BusOffNotificationFctPtr */
#define CanIf_GetCanIfCtrlId2MappedTxBuffersConfigOfPCConfig()        CanIf_CanIfCtrlId2MappedTxBuffersConfig  /**< the pointer to CanIf_CanIfCtrlId2MappedTxBuffersConfig */
#define CanIf_GetCtrlModeIndicationFctPtrOfPCConfig()                 CanIf_CtrlModeIndicationFctPtr  /**< the pointer to CanIf_CtrlModeIndicationFctPtr */
#define CanIf_GetCtrlStatesOfPCConfig()                               CanIf_CtrlStates.raw  /**< the pointer to CanIf_CtrlStates */
#define CanIf_GetGeneratorCompatibilityVersionOfPCConfig()            0x0212u
#define CanIf_GetGeneratorVersionOfPCConfig()                         0x00041101u
#define CanIf_GetMailBoxConfigOfPCConfig()                            CanIf_MailBoxConfig  /**< the pointer to CanIf_MailBoxConfig */
#define CanIf_GetMappedTxBuffersConfigOfPCConfig()                    CanIf_MappedTxBuffersConfig  /**< the pointer to CanIf_MappedTxBuffersConfig */
#define CanIf_GetMaxTrcvHandleIdPlusOneOfPCConfig()                   3u
#define CanIf_GetRxIndicationFctListOfPCConfig()                      CanIf_RxIndicationFctList  /**< the pointer to CanIf_RxIndicationFctList */
#define CanIf_GetRxPduConfigOfPCConfig()                              CanIf_RxPduConfig  /**< the pointer to CanIf_RxPduConfig */
#define CanIf_GetSizeOfCanIfCtrlId2MappedTxBuffersConfigOfPCConfig()  6u  /**< the number of accomplishable value elements in CanIf_CanIfCtrlId2MappedTxBuffersConfig */
#define CanIf_GetSizeOfCtrlStatesOfPCConfig()                         6u  /**< the number of accomplishable value elements in CanIf_CtrlStates */
#define CanIf_GetSizeOfMailBoxConfigOfPCConfig()                      226u  /**< the number of accomplishable value elements in CanIf_MailBoxConfig */
#define CanIf_GetSizeOfMappedTxBuffersConfigOfPCConfig()              5u  /**< the number of accomplishable value elements in CanIf_MappedTxBuffersConfig */
#define CanIf_GetSizeOfRxIndicationFctListOfPCConfig()                7u  /**< the number of accomplishable value elements in CanIf_RxIndicationFctList */
#define CanIf_GetSizeOfRxPduConfigOfPCConfig()                        243u  /**< the number of accomplishable value elements in CanIf_RxPduConfig */
#define CanIf_GetSizeOfTrcvToCtrlMapOfPCConfig()                      3u  /**< the number of accomplishable value elements in CanIf_TrcvToCtrlMap */
#define CanIf_GetSizeOfTxBufferPrioByCanIdBaseOfPCConfig()            5u  /**< the number of accomplishable value elements in CanIf_TxBufferPrioByCanIdBase */
#define CanIf_GetSizeOfTxBufferPrioByCanIdByteQueueConfigOfPCConfig() 5u  /**< the number of accomplishable value elements in CanIf_TxBufferPrioByCanIdByteQueueConfig */
#define CanIf_GetSizeOfTxBufferPrioByCanIdByteQueueMappedTxPdusOfPCConfig() 146u  /**< the number of accomplishable value elements in CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus */
#define CanIf_GetSizeOfTxConfirmationFctListOfPCConfig()              7u  /**< the number of accomplishable value elements in CanIf_TxConfirmationFctList */
#define CanIf_GetSizeOfTxPduConfigOfPCConfig()                        212u  /**< the number of accomplishable value elements in CanIf_TxPduConfig */
#define CanIf_GetSizeOfTxPduQueueIndexOfPCConfig()                    212u  /**< the number of accomplishable value elements in CanIf_TxPduQueueIndex */
#define CanIf_GetSizeOfTxQueueOfPCConfig()                            146u  /**< the number of accomplishable value elements in CanIf_TxQueue */
#define CanIf_GetTrcvModeIndicationFctPtrOfPCConfig()                 CanIf_TrcvModeIndicationFctPtr  /**< the pointer to CanIf_TrcvModeIndicationFctPtr */
#define CanIf_GetTrcvToCtrlMapOfPCConfig()                            CanIf_TrcvToCtrlMap  /**< the pointer to CanIf_TrcvToCtrlMap */
#define CanIf_GetTxBufferPrioByCanIdBaseOfPCConfig()                  CanIf_TxBufferPrioByCanIdBase.raw  /**< the pointer to CanIf_TxBufferPrioByCanIdBase */
#define CanIf_GetTxBufferPrioByCanIdByteQueueConfigOfPCConfig()       CanIf_TxBufferPrioByCanIdByteQueueConfig  /**< the pointer to CanIf_TxBufferPrioByCanIdByteQueueConfig */
#define CanIf_GetTxBufferPrioByCanIdByteQueueMappedTxPdusOfPCConfig() CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus  /**< the pointer to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus */
#define CanIf_GetTxConfirmationFctListOfPCConfig()                    CanIf_TxConfirmationFctList  /**< the pointer to CanIf_TxConfirmationFctList */
#define CanIf_GetTxPduConfigOfPCConfig()                              CanIf_TxPduConfig  /**< the pointer to CanIf_TxPduConfig */
#define CanIf_GetTxPduQueueIndexOfPCConfig()                          CanIf_TxPduQueueIndex  /**< the pointer to CanIf_TxPduQueueIndex */
#define CanIf_GetTxQueueOfPCConfig()                                  CanIf_TxQueue.raw  /**< the pointer to CanIf_TxQueue */
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCGetDataMacros  CanIf Get Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read CONST and VAR data.
  \{
*/ 
#define CanIf_GetBusOffNotificationFctPtr()                           (CanIf_GetBusOffNotificationFctPtrOfPCConfig())
#define CanIf_GetMappedTxBuffersConfigEndIdxOfCanIfCtrlId2MappedTxBuffersConfig(Index) (CanIf_GetCanIfCtrlId2MappedTxBuffersConfigOfPCConfig()[(Index)].MappedTxBuffersConfigEndIdxOfCanIfCtrlId2MappedTxBuffersConfig)
#define CanIf_GetMappedTxBuffersConfigStartIdxOfCanIfCtrlId2MappedTxBuffersConfig(Index) (CanIf_GetCanIfCtrlId2MappedTxBuffersConfigOfPCConfig()[(Index)].MappedTxBuffersConfigStartIdxOfCanIfCtrlId2MappedTxBuffersConfig)
#define CanIf_GetCtrlModeIndicationFctPtr()                           (CanIf_GetCtrlModeIndicationFctPtrOfPCConfig())
#define CanIf_GetCtrlModeOfCtrlStates(Index)                          (CanIf_GetCtrlStatesOfPCConfig()[(Index)].CtrlModeOfCtrlStates)
#define CanIf_GetPduModeOfCtrlStates(Index)                           (CanIf_GetCtrlStatesOfPCConfig()[(Index)].PduModeOfCtrlStates)
#define CanIf_GetCtrlStatesIdxOfMailBoxConfig(Index)                  (CanIf_GetMailBoxConfigOfPCConfig()[(Index)].CtrlStatesIdxOfMailBoxConfig)
#define CanIf_GetMailBoxTypeOfMailBoxConfig(Index)                    (CanIf_GetMailBoxConfigOfPCConfig()[(Index)].MailBoxTypeOfMailBoxConfig)
#define CanIf_GetPduIdFirstOfMailBoxConfig(Index)                     (CanIf_GetMailBoxConfigOfPCConfig()[(Index)].PduIdFirstOfMailBoxConfig)
#define CanIf_GetPduIdLastOfMailBoxConfig(Index)                      (CanIf_GetMailBoxConfigOfPCConfig()[(Index)].PduIdLastOfMailBoxConfig)
#define CanIf_GetTxBufferCfgIdxOfMailBoxConfig(Index)                 (CanIf_GetMailBoxConfigOfPCConfig()[(Index)].TxBufferCfgIdxOfMailBoxConfig)
#define CanIf_GetTxBufferHandlingTypeOfMailBoxConfig(Index)           (CanIf_GetMailBoxConfigOfPCConfig()[(Index)].TxBufferHandlingTypeOfMailBoxConfig)
#define CanIf_GetMailBoxConfigIdxOfMappedTxBuffersConfig(Index)       (CanIf_GetMappedTxBuffersConfigOfPCConfig()[(Index)].MailBoxConfigIdxOfMappedTxBuffersConfig)
#define CanIf_GetRxIndicationFctOfRxIndicationFctList(Index)          (CanIf_GetRxIndicationFctListOfPCConfig()[(Index)].RxIndicationFctOfRxIndicationFctList)
#define CanIf_GetRxIndicationLayoutOfRxIndicationFctList(Index)       (CanIf_GetRxIndicationFctListOfPCConfig()[(Index)].RxIndicationLayoutOfRxIndicationFctList)
#define CanIf_GetDlcOfRxPduConfig(Index)                              (CanIf_GetRxPduConfigOfPCConfig()[(Index)].DlcOfRxPduConfig)
#define CanIf_GetRxIndicationFctListIdxOfRxPduConfig(Index)           (CanIf_GetRxPduConfigOfPCConfig()[(Index)].RxIndicationFctListIdxOfRxPduConfig)
#define CanIf_GetRxMetaDataLengthOfRxPduConfig(Index)                 (CanIf_GetRxPduConfigOfPCConfig()[(Index)].RxMetaDataLengthOfRxPduConfig)
#define CanIf_GetRxPduCanIdOfRxPduConfig(Index)                       (CanIf_GetRxPduConfigOfPCConfig()[(Index)].RxPduCanIdOfRxPduConfig)
#define CanIf_GetRxPduMaskOfRxPduConfig(Index)                        (CanIf_GetRxPduConfigOfPCConfig()[(Index)].RxPduMaskOfRxPduConfig)
#define CanIf_GetUpperPduIdOfRxPduConfig(Index)                       (CanIf_GetRxPduConfigOfPCConfig()[(Index)].UpperPduIdOfRxPduConfig)
#define CanIf_GetTrcvModeIndicationFctPtr()                           (CanIf_GetTrcvModeIndicationFctPtrOfPCConfig())
#define CanIf_GetTrcvToCtrlMap(Index)                                 (CanIf_GetTrcvToCtrlMapOfPCConfig()[(Index)])
#define CanIf_GetTxBufferPrioByCanIdBase(Index)                       (CanIf_GetTxBufferPrioByCanIdBaseOfPCConfig()[(Index)])
#define CanIf_GetTxBufferPrioByCanIdBaseIdxOfTxBufferPrioByCanIdByteQueueConfig(Index) (CanIf_GetTxBufferPrioByCanIdByteQueueConfigOfPCConfig()[(Index)].TxBufferPrioByCanIdBaseIdxOfTxBufferPrioByCanIdByteQueueConfig)
#define CanIf_GetTxBufferPrioByCanIdByteQueueMappedTxPdusEndIdxOfTxBufferPrioByCanIdByteQueueConfig(Index) (CanIf_GetTxBufferPrioByCanIdByteQueueConfigOfPCConfig()[(Index)].TxBufferPrioByCanIdByteQueueMappedTxPdusEndIdxOfTxBufferPrioByCanIdByteQueueConfig)
#define CanIf_GetTxBufferPrioByCanIdByteQueueMappedTxPdusLengthOfTxBufferPrioByCanIdByteQueueConfig(Index) (CanIf_GetTxBufferPrioByCanIdByteQueueConfigOfPCConfig()[(Index)].TxBufferPrioByCanIdByteQueueMappedTxPdusLengthOfTxBufferPrioByCanIdByteQueueConfig)
#define CanIf_GetTxBufferPrioByCanIdByteQueueMappedTxPdusStartIdxOfTxBufferPrioByCanIdByteQueueConfig(Index) (CanIf_GetTxBufferPrioByCanIdByteQueueConfigOfPCConfig()[(Index)].TxBufferPrioByCanIdByteQueueMappedTxPdusStartIdxOfTxBufferPrioByCanIdByteQueueConfig)
#define CanIf_GetTxPduConfigIdxOfTxBufferPrioByCanIdByteQueueMappedTxPdus(Index) (CanIf_GetTxBufferPrioByCanIdByteQueueMappedTxPdusOfPCConfig()[(Index)].TxPduConfigIdxOfTxBufferPrioByCanIdByteQueueMappedTxPdus)
#define CanIf_GetTxConfirmationFctList(Index)                         (CanIf_GetTxConfirmationFctListOfPCConfig()[(Index)])
#define CanIf_GetCanIdOfTxPduConfig(Index)                            (CanIf_GetTxPduConfigOfPCConfig()[(Index)].CanIdOfTxPduConfig)
#define CanIf_GetCanIdTxMaskOfTxPduConfig(Index)                      (CanIf_GetTxPduConfigOfPCConfig()[(Index)].CanIdTxMaskOfTxPduConfig)
#define CanIf_GetCtrlStatesIdxOfTxPduConfig(Index)                    (CanIf_GetTxPduConfigOfPCConfig()[(Index)].CtrlStatesIdxOfTxPduConfig)
#define CanIf_GetDlcOfTxPduConfig(Index)                              (CanIf_GetTxPduConfigOfPCConfig()[(Index)].DlcOfTxPduConfig)
#define CanIf_GetMailBoxConfigIdxOfTxPduConfig(Index)                 (CanIf_GetTxPduConfigOfPCConfig()[(Index)].MailBoxConfigIdxOfTxPduConfig)
#define CanIf_GetTxConfirmationFctListIdxOfTxPduConfig(Index)         (CanIf_GetTxPduConfigOfPCConfig()[(Index)].TxConfirmationFctListIdxOfTxPduConfig)
#define CanIf_GetTxMetaDataLengthOfTxPduConfig(Index)                 (CanIf_GetTxPduConfigOfPCConfig()[(Index)].TxMetaDataLengthOfTxPduConfig)
#define CanIf_GetUpperLayerTxPduIdOfTxPduConfig(Index)                (CanIf_GetTxPduConfigOfPCConfig()[(Index)].UpperLayerTxPduIdOfTxPduConfig)
#define CanIf_GetTxQueueIdxOfTxPduQueueIndex(Index)                   (CanIf_GetTxPduQueueIndexOfPCConfig()[(Index)].TxQueueIdxOfTxPduQueueIndex)
#define CanIf_GetTxQueue(Index)                                       (CanIf_GetTxQueueOfPCConfig()[(Index)])
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCGetDeduplicatedDataMacros  CanIf Get Deduplicated Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated data elements.
  \{
*/ 
#define CanIf_GetGeneratorCompatibilityVersion()                      CanIf_GetGeneratorCompatibilityVersionOfPCConfig()
#define CanIf_GetGeneratorVersion()                                   CanIf_GetGeneratorVersionOfPCConfig()
#define CanIf_IsTxBufferCfgUsedOfMailBoxConfig(Index)                 (((boolean)(CanIf_GetTxBufferCfgIdxOfMailBoxConfig(Index) != CANIF_NO_TXBUFFERCFGIDXOFMAILBOXCONFIG)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to CanIf_TxBufferPrioByCanIdByteQueueConfig */
#define CanIf_GetMaxTrcvHandleIdPlusOne()                             CanIf_GetMaxTrcvHandleIdPlusOneOfPCConfig()
#define CanIf_GetSizeOfCanIfCtrlId2MappedTxBuffersConfig()            CanIf_GetSizeOfCanIfCtrlId2MappedTxBuffersConfigOfPCConfig()
#define CanIf_GetSizeOfCtrlStates()                                   CanIf_GetSizeOfCtrlStatesOfPCConfig()
#define CanIf_GetSizeOfMailBoxConfig()                                CanIf_GetSizeOfMailBoxConfigOfPCConfig()
#define CanIf_GetSizeOfMappedTxBuffersConfig()                        CanIf_GetSizeOfMappedTxBuffersConfigOfPCConfig()
#define CanIf_GetSizeOfRxIndicationFctList()                          CanIf_GetSizeOfRxIndicationFctListOfPCConfig()
#define CanIf_GetSizeOfRxPduConfig()                                  CanIf_GetSizeOfRxPduConfigOfPCConfig()
#define CanIf_GetSizeOfTrcvToCtrlMap()                                CanIf_GetSizeOfTrcvToCtrlMapOfPCConfig()
#define CanIf_GetSizeOfTxBufferPrioByCanIdBase()                      CanIf_GetSizeOfTxBufferPrioByCanIdBaseOfPCConfig()
#define CanIf_GetSizeOfTxBufferPrioByCanIdByteQueueConfig()           CanIf_GetSizeOfTxBufferPrioByCanIdByteQueueConfigOfPCConfig()
#define CanIf_GetSizeOfTxBufferPrioByCanIdByteQueueMappedTxPdus()     CanIf_GetSizeOfTxBufferPrioByCanIdByteQueueMappedTxPdusOfPCConfig()
#define CanIf_GetSizeOfTxConfirmationFctList()                        CanIf_GetSizeOfTxConfirmationFctListOfPCConfig()
#define CanIf_GetSizeOfTxPduConfig()                                  CanIf_GetSizeOfTxPduConfigOfPCConfig()
#define CanIf_GetSizeOfTxPduQueueIndex()                              CanIf_GetSizeOfTxPduQueueIndexOfPCConfig()
#define CanIf_GetSizeOfTxQueue()                                      CanIf_GetSizeOfTxQueueOfPCConfig()
#define CanIf_IsTxQueueUsedOfTxPduQueueIndex(Index)                   (((boolean)(CanIf_GetTxQueueIdxOfTxPduQueueIndex(Index) != CANIF_NO_TXQUEUEIDXOFTXPDUQUEUEINDEX)) != FALSE)  /**< TRUE, if the 0:1 relation has minimum 1 relation pointing to CanIf_TxQueue */
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCSetDataMacros  CanIf Set Data Macros (PRE_COMPILE)
  \brief  These macros can be used to write data.
  \{
*/ 
#define CanIf_SetCtrlModeOfCtrlStates(Index, Value)                   CanIf_GetCtrlStatesOfPCConfig()[(Index)].CtrlModeOfCtrlStates = (Value)
#define CanIf_SetPduModeOfCtrlStates(Index, Value)                    CanIf_GetCtrlStatesOfPCConfig()[(Index)].PduModeOfCtrlStates = (Value)
#define CanIf_SetTxBufferPrioByCanIdBase(Index, Value)                CanIf_GetTxBufferPrioByCanIdBaseOfPCConfig()[(Index)] = (Value)
#define CanIf_SetTxQueue(Index, Value)                                CanIf_GetTxQueueOfPCConfig()[(Index)] = (Value)
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCHasMacros  CanIf Has Macros (PRE_COMPILE)
  \brief  These macros can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
#define CanIf_HasBusOffNotificationFctPtr()                           (TRUE != FALSE)
#define CanIf_HasCanIfCtrlId2MappedTxBuffersConfig()                  (TRUE != FALSE)
#define CanIf_HasMappedTxBuffersConfigEndIdxOfCanIfCtrlId2MappedTxBuffersConfig() (TRUE != FALSE)
#define CanIf_HasMappedTxBuffersConfigStartIdxOfCanIfCtrlId2MappedTxBuffersConfig() (TRUE != FALSE)
#define CanIf_HasCtrlModeIndicationFctPtr()                           (TRUE != FALSE)
#define CanIf_HasCtrlStates()                                         (TRUE != FALSE)
#define CanIf_HasCtrlModeOfCtrlStates()                               (TRUE != FALSE)
#define CanIf_HasPduModeOfCtrlStates()                                (TRUE != FALSE)
#define CanIf_HasGeneratorCompatibilityVersion()                      (TRUE != FALSE)
#define CanIf_HasGeneratorVersion()                                   (TRUE != FALSE)
#define CanIf_HasMailBoxConfig()                                      (TRUE != FALSE)
#define CanIf_HasCtrlStatesIdxOfMailBoxConfig()                       (TRUE != FALSE)
#define CanIf_HasMailBoxTypeOfMailBoxConfig()                         (TRUE != FALSE)
#define CanIf_HasPduIdFirstOfMailBoxConfig()                          (TRUE != FALSE)
#define CanIf_HasPduIdLastOfMailBoxConfig()                           (TRUE != FALSE)
#define CanIf_HasTxBufferCfgIdxOfMailBoxConfig()                      (TRUE != FALSE)
#define CanIf_HasTxBufferCfgUsedOfMailBoxConfig()                     (TRUE != FALSE)
#define CanIf_HasTxBufferHandlingTypeOfMailBoxConfig()                (TRUE != FALSE)
#define CanIf_HasMappedTxBuffersConfig()                              (TRUE != FALSE)
#define CanIf_HasMailBoxConfigIdxOfMappedTxBuffersConfig()            (TRUE != FALSE)
#define CanIf_HasMaxTrcvHandleIdPlusOne()                             (TRUE != FALSE)
#define CanIf_HasRxIndicationFctList()                                (TRUE != FALSE)
#define CanIf_HasRxIndicationFctOfRxIndicationFctList()               (TRUE != FALSE)
#define CanIf_HasRxIndicationLayoutOfRxIndicationFctList()            (TRUE != FALSE)
#define CanIf_HasRxPduConfig()                                        (TRUE != FALSE)
#define CanIf_HasDlcOfRxPduConfig()                                   (TRUE != FALSE)
#define CanIf_HasRxIndicationFctListIdxOfRxPduConfig()                (TRUE != FALSE)
#define CanIf_HasRxMetaDataLengthOfRxPduConfig()                      (TRUE != FALSE)
#define CanIf_HasRxPduCanIdOfRxPduConfig()                            (TRUE != FALSE)
#define CanIf_HasRxPduMaskOfRxPduConfig()                             (TRUE != FALSE)
#define CanIf_HasUpperPduIdOfRxPduConfig()                            (TRUE != FALSE)
#define CanIf_HasSizeOfCanIfCtrlId2MappedTxBuffersConfig()            (TRUE != FALSE)
#define CanIf_HasSizeOfCtrlStates()                                   (TRUE != FALSE)
#define CanIf_HasSizeOfMailBoxConfig()                                (TRUE != FALSE)
#define CanIf_HasSizeOfMappedTxBuffersConfig()                        (TRUE != FALSE)
#define CanIf_HasSizeOfRxIndicationFctList()                          (TRUE != FALSE)
#define CanIf_HasSizeOfRxPduConfig()                                  (TRUE != FALSE)
#define CanIf_HasSizeOfTrcvToCtrlMap()                                (TRUE != FALSE)
#define CanIf_HasSizeOfTxBufferPrioByCanIdBase()                      (TRUE != FALSE)
#define CanIf_HasSizeOfTxBufferPrioByCanIdByteQueueConfig()           (TRUE != FALSE)
#define CanIf_HasSizeOfTxBufferPrioByCanIdByteQueueMappedTxPdus()     (TRUE != FALSE)
#define CanIf_HasSizeOfTxConfirmationFctList()                        (TRUE != FALSE)
#define CanIf_HasSizeOfTxPduConfig()                                  (TRUE != FALSE)
#define CanIf_HasSizeOfTxPduQueueIndex()                              (TRUE != FALSE)
#define CanIf_HasSizeOfTxQueue()                                      (TRUE != FALSE)
#define CanIf_HasTrcvModeIndicationFctPtr()                           (TRUE != FALSE)
#define CanIf_HasTrcvToCtrlMap()                                      (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdBase()                            (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdByteQueueConfig()                 (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdBaseIdxOfTxBufferPrioByCanIdByteQueueConfig() (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdByteQueueMappedTxPdusEndIdxOfTxBufferPrioByCanIdByteQueueConfig() (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdByteQueueMappedTxPdusLengthOfTxBufferPrioByCanIdByteQueueConfig() (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdByteQueueMappedTxPdusStartIdxOfTxBufferPrioByCanIdByteQueueConfig() (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdByteQueueMappedTxPdus()           (TRUE != FALSE)
#define CanIf_HasTxPduConfigIdxOfTxBufferPrioByCanIdByteQueueMappedTxPdus() (TRUE != FALSE)
#define CanIf_HasTxConfirmationFctList()                              (TRUE != FALSE)
#define CanIf_HasTxPduConfig()                                        (TRUE != FALSE)
#define CanIf_HasCanIdOfTxPduConfig()                                 (TRUE != FALSE)
#define CanIf_HasCanIdTxMaskOfTxPduConfig()                           (TRUE != FALSE)
#define CanIf_HasCtrlStatesIdxOfTxPduConfig()                         (TRUE != FALSE)
#define CanIf_HasDlcOfTxPduConfig()                                   (TRUE != FALSE)
#define CanIf_HasMailBoxConfigIdxOfTxPduConfig()                      (TRUE != FALSE)
#define CanIf_HasTxConfirmationFctListIdxOfTxPduConfig()              (TRUE != FALSE)
#define CanIf_HasTxMetaDataLengthOfTxPduConfig()                      (TRUE != FALSE)
#define CanIf_HasUpperLayerTxPduIdOfTxPduConfig()                     (TRUE != FALSE)
#define CanIf_HasTxPduQueueIndex()                                    (TRUE != FALSE)
#define CanIf_HasTxQueueIdxOfTxPduQueueIndex()                        (TRUE != FALSE)
#define CanIf_HasTxQueueUsedOfTxPduQueueIndex()                       (TRUE != FALSE)
#define CanIf_HasTxQueue()                                            (TRUE != FALSE)
#define CanIf_HasPCConfig()                                           (TRUE != FALSE)
#define CanIf_HasBusOffNotificationFctPtrOfPCConfig()                 (TRUE != FALSE)
#define CanIf_HasCanIfCtrlId2MappedTxBuffersConfigOfPCConfig()        (TRUE != FALSE)
#define CanIf_HasCtrlModeIndicationFctPtrOfPCConfig()                 (TRUE != FALSE)
#define CanIf_HasCtrlStatesOfPCConfig()                               (TRUE != FALSE)
#define CanIf_HasGeneratorCompatibilityVersionOfPCConfig()            (TRUE != FALSE)
#define CanIf_HasGeneratorVersionOfPCConfig()                         (TRUE != FALSE)
#define CanIf_HasMailBoxConfigOfPCConfig()                            (TRUE != FALSE)
#define CanIf_HasMappedTxBuffersConfigOfPCConfig()                    (TRUE != FALSE)
#define CanIf_HasMaxTrcvHandleIdPlusOneOfPCConfig()                   (TRUE != FALSE)
#define CanIf_HasRxIndicationFctListOfPCConfig()                      (TRUE != FALSE)
#define CanIf_HasRxPduConfigOfPCConfig()                              (TRUE != FALSE)
#define CanIf_HasSizeOfCanIfCtrlId2MappedTxBuffersConfigOfPCConfig()  (TRUE != FALSE)
#define CanIf_HasSizeOfCtrlStatesOfPCConfig()                         (TRUE != FALSE)
#define CanIf_HasSizeOfMailBoxConfigOfPCConfig()                      (TRUE != FALSE)
#define CanIf_HasSizeOfMappedTxBuffersConfigOfPCConfig()              (TRUE != FALSE)
#define CanIf_HasSizeOfRxIndicationFctListOfPCConfig()                (TRUE != FALSE)
#define CanIf_HasSizeOfRxPduConfigOfPCConfig()                        (TRUE != FALSE)
#define CanIf_HasSizeOfTrcvToCtrlMapOfPCConfig()                      (TRUE != FALSE)
#define CanIf_HasSizeOfTxBufferPrioByCanIdBaseOfPCConfig()            (TRUE != FALSE)
#define CanIf_HasSizeOfTxBufferPrioByCanIdByteQueueConfigOfPCConfig() (TRUE != FALSE)
#define CanIf_HasSizeOfTxBufferPrioByCanIdByteQueueMappedTxPdusOfPCConfig() (TRUE != FALSE)
#define CanIf_HasSizeOfTxConfirmationFctListOfPCConfig()              (TRUE != FALSE)
#define CanIf_HasSizeOfTxPduConfigOfPCConfig()                        (TRUE != FALSE)
#define CanIf_HasSizeOfTxPduQueueIndexOfPCConfig()                    (TRUE != FALSE)
#define CanIf_HasSizeOfTxQueueOfPCConfig()                            (TRUE != FALSE)
#define CanIf_HasTrcvModeIndicationFctPtrOfPCConfig()                 (TRUE != FALSE)
#define CanIf_HasTrcvToCtrlMapOfPCConfig()                            (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdBaseOfPCConfig()                  (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdByteQueueConfigOfPCConfig()       (TRUE != FALSE)
#define CanIf_HasTxBufferPrioByCanIdByteQueueMappedTxPdusOfPCConfig() (TRUE != FALSE)
#define CanIf_HasTxConfirmationFctListOfPCConfig()                    (TRUE != FALSE)
#define CanIf_HasTxPduConfigOfPCConfig()                              (TRUE != FALSE)
#define CanIf_HasTxPduQueueIndexOfPCConfig()                          (TRUE != FALSE)
#define CanIf_HasTxQueueOfPCConfig()                                  (TRUE != FALSE)
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCIncrementDataMacros  CanIf Increment Data Macros (PRE_COMPILE)
  \brief  These macros can be used to increment VAR data with numerical nature.
  \{
*/ 
#define CanIf_IncCtrlModeOfCtrlStates(Index)                          CanIf_GetCtrlModeOfCtrlStates(Index)++
#define CanIf_IncPduModeOfCtrlStates(Index)                           CanIf_GetPduModeOfCtrlStates(Index)++
#define CanIf_IncTxBufferPrioByCanIdBase(Index)                       CanIf_GetTxBufferPrioByCanIdBase(Index)++
#define CanIf_IncTxQueue(Index)                                       CanIf_GetTxQueue(Index)++
/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCDecrementDataMacros  CanIf Decrement Data Macros (PRE_COMPILE)
  \brief  These macros can be used to decrement VAR data with numerical nature.
  \{
*/ 
#define CanIf_DecCtrlModeOfCtrlStates(Index)                          CanIf_GetCtrlModeOfCtrlStates(Index)--
#define CanIf_DecPduModeOfCtrlStates(Index)                           CanIf_GetPduModeOfCtrlStates(Index)--
#define CanIf_DecTxBufferPrioByCanIdBase(Index)                       CanIf_GetTxBufferPrioByCanIdBase(Index)--
#define CanIf_DecTxQueue(Index)                                       CanIf_GetTxQueue(Index)--
/** 
  \}
*/ 

  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  CanIfPCIterableTypes  CanIf Iterable Types (PRE_COMPILE)
  \brief  These type definitions are used to iterate over an array with least processor cycles for variable access as possible.
  \{
*/ 
/**   \brief  type used to iterate CanIf_CanIfCtrlId2MappedTxBuffersConfig */
typedef uint8_least CanIf_CanIfCtrlId2MappedTxBuffersConfigIterType;

/**   \brief  type used to iterate CanIf_CtrlStates */
typedef uint8_least CanIf_CtrlStatesIterType;

/**   \brief  type used to iterate CanIf_MailBoxConfig */
typedef uint8_least CanIf_MailBoxConfigIterType;

/**   \brief  type used to iterate CanIf_MappedTxBuffersConfig */
typedef uint8_least CanIf_MappedTxBuffersConfigIterType;

/**   \brief  type used to iterate CanIf_RxIndicationFctList */
typedef uint8_least CanIf_RxIndicationFctListIterType;

/**   \brief  type used to iterate CanIf_RxPduConfig */
typedef uint8_least CanIf_RxPduConfigIterType;

/**   \brief  type used to iterate CanIf_TrcvToCtrlMap */
typedef uint8_least CanIf_TrcvToCtrlMapIterType;

/**   \brief  type used to iterate CanIf_TxBufferPrioByCanIdBase */
typedef uint8_least CanIf_TxBufferPrioByCanIdBaseIterType;

/**   \brief  type used to iterate CanIf_TxBufferPrioByCanIdByteQueueConfig */
typedef uint8_least CanIf_TxBufferPrioByCanIdByteQueueConfigIterType;

/**   \brief  type used to iterate CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus */
typedef uint8_least CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusIterType;

/**   \brief  type used to iterate CanIf_TxConfirmationFctList */
typedef uint8_least CanIf_TxConfirmationFctListIterType;

/**   \brief  type used to iterate CanIf_TxPduConfig */
typedef uint8_least CanIf_TxPduConfigIterType;

/**   \brief  type used to iterate CanIf_TxPduQueueIndex */
typedef uint8_least CanIf_TxPduQueueIndexIterType;

/**   \brief  type used to iterate CanIf_TxQueue */
typedef uint8_least CanIf_TxQueueIterType;

/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCValueTypes  CanIf Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value based data representations.
  \{
*/ 
/**   \brief  value based type definition for CanIf_MappedTxBuffersConfigEndIdxOfCanIfCtrlId2MappedTxBuffersConfig */
typedef uint8 CanIf_MappedTxBuffersConfigEndIdxOfCanIfCtrlId2MappedTxBuffersConfigType;

/**   \brief  value based type definition for CanIf_MappedTxBuffersConfigStartIdxOfCanIfCtrlId2MappedTxBuffersConfig */
typedef uint8 CanIf_MappedTxBuffersConfigStartIdxOfCanIfCtrlId2MappedTxBuffersConfigType;

/**   \brief  value based type definition for CanIf_GeneratorCompatibilityVersion */
typedef uint16 CanIf_GeneratorCompatibilityVersionType;

/**   \brief  value based type definition for CanIf_GeneratorVersion */
typedef uint32 CanIf_GeneratorVersionType;

/**   \brief  value based type definition for CanIf_CtrlStatesIdxOfMailBoxConfig */
typedef uint8 CanIf_CtrlStatesIdxOfMailBoxConfigType;

/**   \brief  value based type definition for CanIf_PduIdFirstOfMailBoxConfig */
typedef uint8 CanIf_PduIdFirstOfMailBoxConfigType;

/**   \brief  value based type definition for CanIf_PduIdLastOfMailBoxConfig */
typedef uint8 CanIf_PduIdLastOfMailBoxConfigType;

/**   \brief  value based type definition for CanIf_TxBufferCfgIdxOfMailBoxConfig */
typedef uint8 CanIf_TxBufferCfgIdxOfMailBoxConfigType;

/**   \brief  value based type definition for CanIf_TxBufferCfgUsedOfMailBoxConfig */
typedef boolean CanIf_TxBufferCfgUsedOfMailBoxConfigType;

/**   \brief  value based type definition for CanIf_TxBufferHandlingTypeOfMailBoxConfig */
typedef uint8 CanIf_TxBufferHandlingTypeOfMailBoxConfigType;

/**   \brief  value based type definition for CanIf_MailBoxConfigIdxOfMappedTxBuffersConfig */
typedef uint8 CanIf_MailBoxConfigIdxOfMappedTxBuffersConfigType;

/**   \brief  value based type definition for CanIf_MaxTrcvHandleIdPlusOne */
typedef uint8 CanIf_MaxTrcvHandleIdPlusOneType;

/**   \brief  value based type definition for CanIf_DlcOfRxPduConfig */
typedef uint8 CanIf_DlcOfRxPduConfigType;

/**   \brief  value based type definition for CanIf_RxIndicationFctListIdxOfRxPduConfig */
typedef uint8 CanIf_RxIndicationFctListIdxOfRxPduConfigType;

/**   \brief  value based type definition for CanIf_RxMetaDataLengthOfRxPduConfig */
typedef uint8 CanIf_RxMetaDataLengthOfRxPduConfigType;

/**   \brief  value based type definition for CanIf_RxPduCanIdOfRxPduConfig */
typedef uint32 CanIf_RxPduCanIdOfRxPduConfigType;

/**   \brief  value based type definition for CanIf_RxPduMaskOfRxPduConfig */
typedef uint32 CanIf_RxPduMaskOfRxPduConfigType;

/**   \brief  value based type definition for CanIf_UpperPduIdOfRxPduConfig */
typedef PduIdType CanIf_UpperPduIdOfRxPduConfigType;

/**   \brief  value based type definition for CanIf_SizeOfCanIfCtrlId2MappedTxBuffersConfig */
typedef uint8 CanIf_SizeOfCanIfCtrlId2MappedTxBuffersConfigType;

/**   \brief  value based type definition for CanIf_SizeOfCtrlStates */
typedef uint8 CanIf_SizeOfCtrlStatesType;

/**   \brief  value based type definition for CanIf_SizeOfMailBoxConfig */
typedef uint8 CanIf_SizeOfMailBoxConfigType;

/**   \brief  value based type definition for CanIf_SizeOfMappedTxBuffersConfig */
typedef uint8 CanIf_SizeOfMappedTxBuffersConfigType;

/**   \brief  value based type definition for CanIf_SizeOfRxIndicationFctList */
typedef uint8 CanIf_SizeOfRxIndicationFctListType;

/**   \brief  value based type definition for CanIf_SizeOfRxPduConfig */
typedef uint8 CanIf_SizeOfRxPduConfigType;

/**   \brief  value based type definition for CanIf_SizeOfTrcvToCtrlMap */
typedef uint8 CanIf_SizeOfTrcvToCtrlMapType;

/**   \brief  value based type definition for CanIf_SizeOfTxBufferPrioByCanIdBase */
typedef uint8 CanIf_SizeOfTxBufferPrioByCanIdBaseType;

/**   \brief  value based type definition for CanIf_SizeOfTxBufferPrioByCanIdByteQueueConfig */
typedef uint8 CanIf_SizeOfTxBufferPrioByCanIdByteQueueConfigType;

/**   \brief  value based type definition for CanIf_SizeOfTxBufferPrioByCanIdByteQueueMappedTxPdus */
typedef uint8 CanIf_SizeOfTxBufferPrioByCanIdByteQueueMappedTxPdusType;

/**   \brief  value based type definition for CanIf_SizeOfTxConfirmationFctList */
typedef uint8 CanIf_SizeOfTxConfirmationFctListType;

/**   \brief  value based type definition for CanIf_SizeOfTxPduConfig */
typedef uint8 CanIf_SizeOfTxPduConfigType;

/**   \brief  value based type definition for CanIf_SizeOfTxPduQueueIndex */
typedef uint8 CanIf_SizeOfTxPduQueueIndexType;

/**   \brief  value based type definition for CanIf_SizeOfTxQueue */
typedef uint8 CanIf_SizeOfTxQueueType;

/**   \brief  value based type definition for CanIf_TrcvToCtrlMap */
typedef uint8 CanIf_TrcvToCtrlMapType;

/**   \brief  value based type definition for CanIf_TxBufferPrioByCanIdBaseIdxOfTxBufferPrioByCanIdByteQueueConfig */
typedef uint8 CanIf_TxBufferPrioByCanIdBaseIdxOfTxBufferPrioByCanIdByteQueueConfigType;

/**   \brief  value based type definition for CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusEndIdxOfTxBufferPrioByCanIdByteQueueConfig */
typedef uint8 CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusEndIdxOfTxBufferPrioByCanIdByteQueueConfigType;

/**   \brief  value based type definition for CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusLengthOfTxBufferPrioByCanIdByteQueueConfig */
typedef uint8 CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusLengthOfTxBufferPrioByCanIdByteQueueConfigType;

/**   \brief  value based type definition for CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusStartIdxOfTxBufferPrioByCanIdByteQueueConfig */
typedef uint8 CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusStartIdxOfTxBufferPrioByCanIdByteQueueConfigType;

/**   \brief  value based type definition for CanIf_TxPduConfigIdxOfTxBufferPrioByCanIdByteQueueMappedTxPdus */
typedef uint8 CanIf_TxPduConfigIdxOfTxBufferPrioByCanIdByteQueueMappedTxPdusType;

/**   \brief  value based type definition for CanIf_CanIdOfTxPduConfig */
typedef uint32 CanIf_CanIdOfTxPduConfigType;

/**   \brief  value based type definition for CanIf_CanIdTxMaskOfTxPduConfig */
typedef uint32 CanIf_CanIdTxMaskOfTxPduConfigType;

/**   \brief  value based type definition for CanIf_CtrlStatesIdxOfTxPduConfig */
typedef uint8 CanIf_CtrlStatesIdxOfTxPduConfigType;

/**   \brief  value based type definition for CanIf_DlcOfTxPduConfig */
typedef uint8 CanIf_DlcOfTxPduConfigType;

/**   \brief  value based type definition for CanIf_MailBoxConfigIdxOfTxPduConfig */
typedef uint8 CanIf_MailBoxConfigIdxOfTxPduConfigType;

/**   \brief  value based type definition for CanIf_TxConfirmationFctListIdxOfTxPduConfig */
typedef uint8 CanIf_TxConfirmationFctListIdxOfTxPduConfigType;

/**   \brief  value based type definition for CanIf_TxMetaDataLengthOfTxPduConfig */
typedef uint8 CanIf_TxMetaDataLengthOfTxPduConfigType;

/**   \brief  value based type definition for CanIf_UpperLayerTxPduIdOfTxPduConfig */
typedef PduIdType CanIf_UpperLayerTxPduIdOfTxPduConfigType;

/**   \brief  value based type definition for CanIf_TxQueueIdxOfTxPduQueueIndex */
typedef uint8 CanIf_TxQueueIdxOfTxPduQueueIndexType;

/**   \brief  value based type definition for CanIf_TxQueueUsedOfTxPduQueueIndex */
typedef boolean CanIf_TxQueueUsedOfTxPduQueueIndexType;

/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  CanIfPCStructTypes  CanIf Struct Types (PRE_COMPILE)
  \brief  These type definitions are used for structured data representations.
  \{
*/ 
/**   \brief  type used in CanIf_CanIfCtrlId2MappedTxBuffersConfig */
typedef struct sCanIf_CanIfCtrlId2MappedTxBuffersConfigType
{
  CanIf_MappedTxBuffersConfigEndIdxOfCanIfCtrlId2MappedTxBuffersConfigType MappedTxBuffersConfigEndIdxOfCanIfCtrlId2MappedTxBuffersConfig;  /**< the end index of the 1:n relation pointing to CanIf_MappedTxBuffersConfig */
  CanIf_MappedTxBuffersConfigStartIdxOfCanIfCtrlId2MappedTxBuffersConfigType MappedTxBuffersConfigStartIdxOfCanIfCtrlId2MappedTxBuffersConfig;  /**< the start index of the 1:n relation pointing to CanIf_MappedTxBuffersConfig */
} CanIf_CanIfCtrlId2MappedTxBuffersConfigType;

/**   \brief  type used in CanIf_CtrlStates */
typedef struct sCanIf_CtrlStatesType
{
  CanIf_ControllerModeType CtrlModeOfCtrlStates;  /**< Controller mode. */
  CanIf_PduGetModeType PduModeOfCtrlStates;  /**< PDU mode state. */
} CanIf_CtrlStatesType;

/**   \brief  type used in CanIf_MailBoxConfig */
typedef struct sCanIf_MailBoxConfigType
{
  CanIf_CtrlStatesIdxOfMailBoxConfigType CtrlStatesIdxOfMailBoxConfig;  /**< the index of the 1:1 relation pointing to CanIf_CtrlStates */
  CanIf_PduIdFirstOfMailBoxConfigType PduIdFirstOfMailBoxConfig;  /**< "First" PDU mapped to mailbox. */
  CanIf_PduIdLastOfMailBoxConfigType PduIdLastOfMailBoxConfig;  /**< "Last" PDU mapped to mailbox. */
  CanIf_TxBufferCfgIdxOfMailBoxConfigType TxBufferCfgIdxOfMailBoxConfig;  /**< the index of the 0:1 relation pointing to CanIf_TxBufferPrioByCanIdByteQueueConfig */
  CanIf_TxBufferHandlingTypeOfMailBoxConfigType TxBufferHandlingTypeOfMailBoxConfig;
  CanIf_MailBoxTypeType MailBoxTypeOfMailBoxConfig;  /**< Type of mailbox: Rx-/Tx- BasicCAN/FullCAN/unused. */
} CanIf_MailBoxConfigType;

/**   \brief  type used in CanIf_MappedTxBuffersConfig */
typedef struct sCanIf_MappedTxBuffersConfigType
{
  CanIf_MailBoxConfigIdxOfMappedTxBuffersConfigType MailBoxConfigIdxOfMappedTxBuffersConfig;  /**< the index of the 1:1 relation pointing to CanIf_MailBoxConfig */
} CanIf_MappedTxBuffersConfigType;

/**   \brief  type used in CanIf_RxIndicationFctList */
typedef struct sCanIf_RxIndicationFctListType
{
  CanIf_RxIndicationFctType RxIndicationFctOfRxIndicationFctList;  /**< Rx indication function. */
  CanIf_RxIndicationLayoutType RxIndicationLayoutOfRxIndicationFctList;  /**< Layout of Rx indication function. */
} CanIf_RxIndicationFctListType;

/**   \brief  type used in CanIf_RxPduConfig */
typedef struct sCanIf_RxPduConfigType
{
  CanIf_RxPduCanIdOfRxPduConfigType RxPduCanIdOfRxPduConfig;  /**< Rx-PDU: CAN identifier. */
  CanIf_RxPduMaskOfRxPduConfigType RxPduMaskOfRxPduConfig;  /**< Rx-PDU: CAN identifier mask. */
  CanIf_UpperPduIdOfRxPduConfigType UpperPduIdOfRxPduConfig;  /**< PDU ID defined by upper layer. */
  CanIf_DlcOfRxPduConfigType DlcOfRxPduConfig;  /**< Data length code. */
  CanIf_RxIndicationFctListIdxOfRxPduConfigType RxIndicationFctListIdxOfRxPduConfig;  /**< the index of the 1:1 relation pointing to CanIf_RxIndicationFctList */
  CanIf_RxMetaDataLengthOfRxPduConfigType RxMetaDataLengthOfRxPduConfig;  /**< Meta data length. */
} CanIf_RxPduConfigType;

/**   \brief  type used in CanIf_TxBufferPrioByCanIdByteQueueConfig */
typedef struct sCanIf_TxBufferPrioByCanIdByteQueueConfigType
{
  CanIf_TxBufferPrioByCanIdBaseIdxOfTxBufferPrioByCanIdByteQueueConfigType TxBufferPrioByCanIdBaseIdxOfTxBufferPrioByCanIdByteQueueConfig;  /**< the index of the 1:1 relation pointing to CanIf_TxBufferPrioByCanIdBase */
  CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusEndIdxOfTxBufferPrioByCanIdByteQueueConfigType TxBufferPrioByCanIdByteQueueMappedTxPdusEndIdxOfTxBufferPrioByCanIdByteQueueConfig;  /**< the end index of the 1:n relation pointing to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus */
  CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusLengthOfTxBufferPrioByCanIdByteQueueConfigType TxBufferPrioByCanIdByteQueueMappedTxPdusLengthOfTxBufferPrioByCanIdByteQueueConfig;  /**< the number of relations pointing to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus */
  CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusStartIdxOfTxBufferPrioByCanIdByteQueueConfigType TxBufferPrioByCanIdByteQueueMappedTxPdusStartIdxOfTxBufferPrioByCanIdByteQueueConfig;  /**< the start index of the 1:n relation pointing to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus */
} CanIf_TxBufferPrioByCanIdByteQueueConfigType;

/**   \brief  type used in CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus */
typedef struct sCanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusType
{
  CanIf_TxPduConfigIdxOfTxBufferPrioByCanIdByteQueueMappedTxPdusType TxPduConfigIdxOfTxBufferPrioByCanIdByteQueueMappedTxPdus;  /**< the index of the 1:1 relation pointing to CanIf_TxPduConfig */
} CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusType;

/**   \brief  type used in CanIf_TxPduConfig */
typedef struct sCanIf_TxPduConfigType
{
  CanIf_CanIdOfTxPduConfigType CanIdOfTxPduConfig;  /**< CAN identifier (16bit / 32bit). */
  CanIf_CanIdTxMaskOfTxPduConfigType CanIdTxMaskOfTxPduConfig;  /**< CAN identifier mask (16bit / 32bit). */
  CanIf_UpperLayerTxPduIdOfTxPduConfigType UpperLayerTxPduIdOfTxPduConfig;  /**< Upper layer handle ID (8bit / 16bit). */
  CanIf_CtrlStatesIdxOfTxPduConfigType CtrlStatesIdxOfTxPduConfig;  /**< the index of the 1:1 relation pointing to CanIf_CtrlStates */
  CanIf_DlcOfTxPduConfigType DlcOfTxPduConfig;  /**< Data length code. */
  CanIf_MailBoxConfigIdxOfTxPduConfigType MailBoxConfigIdxOfTxPduConfig;  /**< the index of the 1:1 relation pointing to CanIf_MailBoxConfig */
  CanIf_TxConfirmationFctListIdxOfTxPduConfigType TxConfirmationFctListIdxOfTxPduConfig;  /**< the index of the 1:1 relation pointing to CanIf_TxConfirmationFctList */
  CanIf_TxMetaDataLengthOfTxPduConfigType TxMetaDataLengthOfTxPduConfig;  /**< Meta data length. */
} CanIf_TxPduConfigType;

/**   \brief  type used in CanIf_TxPduQueueIndex */
typedef struct sCanIf_TxPduQueueIndexType
{
  CanIf_TxQueueIdxOfTxPduQueueIndexType TxQueueIdxOfTxPduQueueIndex;  /**< the index of the 0:1 relation pointing to CanIf_TxQueue */
} CanIf_TxPduQueueIndexType;

/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCSymbolicStructTypes  CanIf Symbolic Struct Types (PRE_COMPILE)
  \brief  These structs are used in unions to have a symbol based data representation style.
  \{
*/ 
/**   \brief  type to be used as symbolic data element access to CanIf_CtrlStates */
typedef struct CanIf_CtrlStatesStructSTag
{
  CanIf_CtrlStatesType CT_Backbone1J1939_198bcf1c;
  CanIf_CtrlStatesType CT_Backbone2_34cfe263;
  CanIf_CtrlStatesType CT_CAN6_120de18e;
  CanIf_CtrlStatesType CT_CabSubnet_d2ff0fbe;
  CanIf_CtrlStatesType CT_FMSNet_119a8706;
  CanIf_CtrlStatesType CT_SecuritySubnet_f5346ae6;
} CanIf_CtrlStatesStructSType;

/**   \brief  type to be used as symbolic data element access to CanIf_TxBufferPrioByCanIdBase */
typedef struct CanIf_TxBufferPrioByCanIdBaseStructSTag
{
  CanIf_TxBufferPrioByCanIdBaseType CHNL_17344684;
  CanIf_TxBufferPrioByCanIdBaseType CHNL_a6b6a922;
  CanIf_TxBufferPrioByCanIdBaseType CHNL_df893ad5;
  CanIf_TxBufferPrioByCanIdBaseType CHNL_e058b4ae;
  CanIf_TxBufferPrioByCanIdBaseType CHNL_53cce282;
} CanIf_TxBufferPrioByCanIdBaseStructSType;

/**   \brief  type to be used as symbolic data element access to CanIf_TxQueue */
typedef struct CanIf_TxQueueStructSTag
{
  CanIf_TxPrioByCanIdByteQueueType RQST_TACHO_CIOM_oBackbone1J1939_e082a6b2_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagUUDTRespMsg1_F2_C0_BB2_oBackbone2_603e2338_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagUUDTRespMsg1_F2_A2_BB2_oBackbone2_8af81a14_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagUUDTRespMsg1_F2_A1_BB2_oBackbone2_26f22879_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagUUDTRespMsg1_F2_A0_BB2_oBackbone2_42f439a2_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagUUDTRespMsg1_F2_98_BB2_oBackbone2_5178e6a4_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagUUDTRespMsg1_F2_53_BB2_oBackbone2_a51c09cf_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagUUDTRespMsg1_F2_40_BB2_oBackbone2_187336ef_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagUUDTRespMsg1_F2_26_BB2_oBackbone2_fc487bda_Tx;
  CanIf_TxPrioByCanIdByteQueueType XCP_FA_40_BB2_oBackbone2_9cf6862c_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntTGW2_F4_C0_BB2_Tp_oBackbone2_f7aad0d7_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntTGW2_F4_A2_BB2_Tp_oBackbone2_249acffa_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntTGW2_F4_A1_BB2_Tp_oBackbone2_bca8fbcc_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntTGW2_F4_A0_BB2_Tp_oBackbone2_7d9615e1_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntTGW2_F4_98_BB2_Tp_oBackbone2_8516ddd7_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntTGW2_F4_53_BB2_Tp_oBackbone2_280e15fa_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntTGW2_F4_40_BB2_Tp_oBackbone2_f5224357_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntTGW2_F4_26_BB2_Tp_oBackbone2_5b036461_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntHMIIOM_F3_C0_BB2_Tp_oBackbone2_9924f26b_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntHMIIOM_F3_A2_BB2_Tp_oBackbone2_d6b4927e_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntHMIIOM_F3_A1_BB2_Tp_oBackbone2_08540fc1_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntHMIIOM_F3_A0_BB2_Tp_oBackbone2_f4db796b_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntHMIIOM_F3_98_BB2_Tp_oBackbone2_73235a33_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntHMIIOM_F3_53_BB2_Tp_oBackbone2_930c5799_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntHMIIOM_F3_40_BB2_Tp_oBackbone2_7b130fa6_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagRespMsgIntHMIIOM_F3_26_BB2_Tp_oBackbone2_aba3af99_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagRespMsg_F2_C0_BB2_Tp_oBackbone2_ae5b54dc_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagRespMsg_F2_A2_BB2_Tp_oBackbone2_2addb983_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagRespMsg_F2_A1_BB2_Tp_oBackbone2_441131ae_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagRespMsg_F2_A0_BB2_Tp_oBackbone2_d77a4b8a_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagRespMsg_F2_98_BB2_Tp_oBackbone2_0c1730ea_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagRespMsg_F2_53_BB2_Tp_oBackbone2_ef1139d4_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagRespMsg_F2_26_BB2_Tp_oBackbone2_ebb70ff2_Tx;
  CanIf_TxPrioByCanIdByteQueueType NmAsr_CIOM_BB2_oBackbone2_9b0338b4_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagFaultStat_SRS_BB2_oBackbone2_e1ef7eee_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagFaultStat_LECM_BB2_oBackbone2_0cb60c21_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagFaultStat_CCM_BB2_oBackbone2_2f6e7814_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagFaultStat_WRCS_BB2_oBackbone2_dad83051_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagFaultStat_Alarm_BB2_oBackbone2_7b6d0a04_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagFaultStat_PDM_BB2_oBackbone2_b720c987_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagFaultStat_DDM_BB2_oBackbone2_c5068234_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagFaultStat_CIOM_BB2_oBackbone2_04d7302e_Tx;
  CanIf_TxPrioByCanIdByteQueueType VMCU_BB2_57P_FCM_Tp_oBackbone2_65fb58e9_Tx;
  CanIf_TxPrioByCanIdByteQueueType HMIIOM_BB2_36S_FCM_Tp_oBackbone2_bf84bd48_Tx;
  CanIf_TxPrioByCanIdByteQueueType TECU_BB2_06S_FCM_Tp_oBackbone2_a56e8b10_Tx;
  CanIf_TxPrioByCanIdByteQueueType HMIIOM_BB2_27S_FCM_Tp_oBackbone2_16072c1e_Tx;
  CanIf_TxPrioByCanIdByteQueueType HMIIOM_BB2_19P_CIOM_FCM_Tp_oBackbone2_1a616940_Tx;
  CanIf_TxPrioByCanIdByteQueueType VMCU_BB2_31S_FCM_Tp_oBackbone2_46c44850_Tx;
  CanIf_TxPrioByCanIdByteQueueType VMCU_BB2_32S_FCM_Tp_oBackbone2_8a7e4a00_Tx;
  CanIf_TxPrioByCanIdByteQueueType HMIIOM_BB2_20S_FCM_Tp_oBackbone2_2def59ba_Tx;
  CanIf_TxPrioByCanIdByteQueueType EMS_BB2_09S_FCM_Tp_oBackbone2_41cc37fa_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagRespMsg_F1_53_BB2_Tp_oBackbone2_be0c70d1_Tx;
  CanIf_TxPrioByCanIdByteQueueType BBM_BB2_03S_CIOM_FCM_Tp_oBackbone2_29c4ac8e_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_21S_oBackbone2_140cc697_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_24S_oBackbone2_37b045d6_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_23S_oBackbone2_db8bc497_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_22S_Tp_oBackbone2_92aec239_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_20S_oBackbone2_73cf4797_Tx;
  CanIf_TxPrioByCanIdByteQueueType VMCU_BB2_34S_FCM_Tp_oBackbone2_c87b48e1_Tx;
  CanIf_TxPrioByCanIdByteQueueType TECU_BB2_05S_FCM_Tp_oBackbone2_69d48940_Tx;
  CanIf_TxPrioByCanIdByteQueueType HMIIOM_BB2_21S_FCM_Tp_oBackbone2_af5a2700_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_26S_oBackbone2_f83747d6_Tx;
  CanIf_TxPrioByCanIdByteQueueType EMS_BB2_11S_FCM_Tp_oBackbone2_6cd5cf95_Tx;
  CanIf_TxPrioByCanIdByteQueueType HMIIOM_BB2_35S_FCM_Tp_oBackbone2_e32a38c7_Tx;
  CanIf_TxPrioByCanIdByteQueueType HMIIOM_BB2_06S_FCM_Tp_oBackbone2_c2df8d7c_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_13S_Tp_oBackbone2_bd99285a_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_30S_Tp_oBackbone2_82da3f46_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_12S_Tp_oBackbone2_b9170acc_Tx;
  CanIf_TxPrioByCanIdByteQueueType HMIIOM_BB2_04S_FCM_Tp_oBackbone2_1cc47649_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_27S_oBackbone2_9ff4c6d6_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_BB2_25P_oBackbone2_732f917b_Tx;
  CanIf_TxPrioByCanIdByteQueueType IntTesterTGW2FuncDiagMsg_Cab_Tp_oCabSubnet_96ec8334_Tx;
  CanIf_TxPrioByCanIdByteQueueType TesterFuncDiagMsg_Cab_Tp_oCabSubnet_d293ecaa_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagReqMsgIntTGW2_A2_F4_Cab_Tp_oCabSubnet_fc611706_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagReqMsgIntHMIIOM_A2_F3_Cab_Tp_oCabSubnet_fcbc8d7d_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagReqMsg_A2_F2_Cab_Tp_oCabSubnet_6d74e481_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagReqMsgIntTGW2_98_F4_Cab_Tp_oCabSubnet_2e8c3543_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagReqMsgIntHMIIOM_98_F3_Cab_Tp_oCabSubnet_2daafb4e_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagReqMsg_98_F2_Cab_Tp_oCabSubnet_4c51814f_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagReqMsgIntTGW2_53_F4_Cab_Tp_oCabSubnet_75ef26c1_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagReqMsgIntHMIIOM_53_F3_Cab_Tp_oCabSubnet_e9059d48_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagReqMsg_53_F2_Cab_Tp_oCabSubnet_9abe6f67_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagReqMsgIntTGW2_26_F4_Cab_Tp_oCabSubnet_b42402a4_Tx;
  CanIf_TxPrioByCanIdByteQueueType DiagReqMsgIntHMIIOM_26_F3_Cab_Tp_oCabSubnet_2e1f9ada_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagReqMsg_26_F2_Cab_Tp_oCabSubnet_e2a6386f_Tx;
  CanIf_TxPrioByCanIdByteQueueType NmAsr_CIOM_Cab_oCabSubnet_153d3c7d_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_21P_oCabSubnet_c1ce1ce8_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_20S_Tp_oCabSubnet_c323df97_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_19P_oCabSubnet_49985ce9_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_18P_oCabSubnet_2e5bdde9_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_17P_oCabSubnet_4a9e582a_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_16P_oCabSubnet_2d5dd92a_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_34P_Tp_oCabSubnet_377e6916_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_30S_Tp_oCabSubnet_da4b67c4_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_25S_Tp_oCabSubnet_d5957759_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_15P_oCabSubnet_85195a2a_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_14P_oCabSubnet_e2dadb2a_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_13S_Tp_oCabSubnet_e50870d8_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_12P_oCabSubnet_6922db6b_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_11S_Tp_oCabSubnet_ec1435f4_Tx;
  CanIf_TxPrioByCanIdByteQueueType PhysDiagReqMsg_53_F1_Cab_Tp_oCabSubnet_682570ce_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_08P_oCabSubnet_2e3c1e68_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_07P_oCabSubnet_4af99bab_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_22P_oCabSubnet_698a9fe8_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_29S_Tp_oCabSubnet_e3dce9b1_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_05S_Tp_oCabSubnet_e74407ff_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_04S_Tp_oCabSubnet_e3ca2569_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Cab_28S_oCabSubnet_0dafccc7_Tx;
  CanIf_TxPrioByCanIdByteQueueType CCM_Cab_03P_FCM_Tp_oCabSubnet_af75b436_Tx;
  CanIf_TxPrioByCanIdByteQueueType FMS_X_CIOMFMS_oFMSNet_68f86b37_Tx;
  CanIf_TxPrioByCanIdByteQueueType TPCM_Tp_oFMSNet_55e2f930_Tx;
  CanIf_TxPrioByCanIdByteQueueType TPDT_Tp_oFMSNet_3018618a_Tx;
  CanIf_TxPrioByCanIdByteQueueType VP236_X_CIOMFMS_oFMSNet_524bf569_Tx;
  CanIf_TxPrioByCanIdByteQueueType DD_X_CIOMFMS_oFMSNet_95f62c09_Tx;
  CanIf_TxPrioByCanIdByteQueueType AMB_X_CIOMFMS_oFMSNet_f6109b97_Tx;
  CanIf_TxPrioByCanIdByteQueueType ET1_X_CIOMFMS_oFMSNet_ff39068f_Tx;
  CanIf_TxPrioByCanIdByteQueueType VW_X_CIOMFMS_oFMSNet_06d77621_Tx;
  CanIf_TxPrioByCanIdByteQueueType LFC_X_CIOMFMS_oFMSNet_16e703d3_Tx;
  CanIf_TxPrioByCanIdByteQueueType HOURS_X_CIOMFMS_oFMSNet_bfc991fc_Tx;
  CanIf_TxPrioByCanIdByteQueueType VDHR_X_CIOMFMS_oFMSNet_67a02fb8_Tx;
  CanIf_TxPrioByCanIdByteQueueType SERV_X_CIOMFMS_oFMSNet_0f3b4ff2_Tx;
  CanIf_TxPrioByCanIdByteQueueType AIR1_X_CIOMFMS_oFMSNet_9b1ec537_Tx;
  CanIf_TxPrioByCanIdByteQueueType CVW_X_CIOMFMS_oFMSNet_5e9635e8_Tx;
  CanIf_TxPrioByCanIdByteQueueType TPDirect_0FE6B_Tp_oFMSNet_5c137171_Tx;
  CanIf_TxPrioByCanIdByteQueueType AT1T1I1_X_CIOMFMS_oFMSNet_4f6adf75_Tx;
  CanIf_TxPrioByCanIdByteQueueType EEC14_X_CIOMFMS_oFMSNet_3551b912_Tx;
  CanIf_TxPrioByCanIdByteQueueType FMS1_X_CIOMFMS_oFMSNet_fb3c9fbd_Tx;
  CanIf_TxPrioByCanIdByteQueueType HRLFC_X_CIOMFMS_oFMSNet_c55a7a2b_Tx;
  CanIf_TxPrioByCanIdByteQueueType J1939NmTxPdu_2068bfea;
  CanIf_TxPrioByCanIdByteQueueType CL_X_CIOMFMS_oFMSNet_fc518547_Tx;
  CanIf_TxPrioByCanIdByteQueueType AckmTxPdu_FMSNet_J1939_44d89c3b;
  CanIf_TxPrioByCanIdByteQueueType NmAsr_CIOM_Sec_oSecuritySubnet_dae88b7d_Tx;
  CanIf_TxPrioByCanIdByteQueueType PDM_Sec_04S_FCM_Tp_oSecuritySubnet_90a313cf_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Sec_05S_Tp_oSecuritySubnet_c8b553a6_Tx;
  CanIf_TxPrioByCanIdByteQueueType PDM_Sec_03S_FCM_Tp_oSecuritySubnet_2750bd9e_Tx;
  CanIf_TxPrioByCanIdByteQueueType DDM_Sec_05S_FCM_Tp_oSecuritySubnet_d14dc345_Tx;
  CanIf_TxPrioByCanIdByteQueueType Alarm_Sec_07S_FCM_Tp_oSecuritySubnet_94ec21d2_Tx;
  CanIf_TxPrioByCanIdByteQueueType DDM_Sec_04S_FCM_Tp_oSecuritySubnet_eddd4cee_Tx;
  CanIf_TxPrioByCanIdByteQueueType Alarm_Sec_06S_FCM_Tp_oSecuritySubnet_a3ee35ea_Tx;
  CanIf_TxPrioByCanIdByteQueueType DDM_Sec_03S_FCM_Tp_oSecuritySubnet_5a2ee2bf_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Sec_07S_Tp_oSecuritySubnet_69506b1a_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Sec_11S_Tp_oSecuritySubnet_dc8daaf0_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Sec_09S_Tp_oSecuritySubnet_ba09ccac_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Sec_06S_Tp_oSecuritySubnet_39a2f744_Tx;
  CanIf_TxPrioByCanIdByteQueueType Alarm_Sec_03S_FCM_Tp_oSecuritySubnet_48e47132_Tx;
  CanIf_TxPrioByCanIdByteQueueType CIOM_Sec_08S_Tp_oSecuritySubnet_eafb50f2_Tx;
} CanIf_TxQueueStructSType;  /* PRQA S 0639 */  /* MD_MSR_Dir1.1 */

/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCUnionIndexAndSymbolTypes  CanIf Union Index And Symbol Types (PRE_COMPILE)
  \brief  These unions are used to access arrays in an index and symbol based style.
  \{
*/ 
/**   \brief  type to access CanIf_CtrlStates in an index and symbol based style. */
typedef union CanIf_CtrlStatesUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  CanIf_CtrlStatesType raw[6];
  CanIf_CtrlStatesStructSType str;
} CanIf_CtrlStatesUType;

/**   \brief  type to access CanIf_TxBufferPrioByCanIdBase in an index and symbol based style. */
typedef union CanIf_TxBufferPrioByCanIdBaseUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  CanIf_TxBufferPrioByCanIdBaseType raw[5];
  CanIf_TxBufferPrioByCanIdBaseStructSType str;
} CanIf_TxBufferPrioByCanIdBaseUType;

/**   \brief  type to access CanIf_TxQueue in an index and symbol based style. */
typedef union CanIf_TxQueueUTag
{  /* PRQA S 0750 */  /* MD_CSL_Union */
  CanIf_TxPrioByCanIdByteQueueType raw[146];
  CanIf_TxQueueStructSType str;
} CanIf_TxQueueUType;

/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCRootPointerTypes  CanIf Root Pointer Types (PRE_COMPILE)
  \brief  These type definitions are used to point from the config root to symbol instances.
  \{
*/ 
/**   \brief  type used to point to CanIf_CanIfCtrlId2MappedTxBuffersConfig */
typedef P2CONST(CanIf_CanIfCtrlId2MappedTxBuffersConfigType, TYPEDEF, CANIF_CONST) CanIf_CanIfCtrlId2MappedTxBuffersConfigPtrType;

/**   \brief  type used to point to CanIf_CtrlStates */
typedef P2VAR(CanIf_CtrlStatesType, TYPEDEF, CANIF_VAR_NOINIT) CanIf_CtrlStatesPtrType;

/**   \brief  type used to point to CanIf_MailBoxConfig */
typedef P2CONST(CanIf_MailBoxConfigType, TYPEDEF, CANIF_CONST) CanIf_MailBoxConfigPtrType;

/**   \brief  type used to point to CanIf_MappedTxBuffersConfig */
typedef P2CONST(CanIf_MappedTxBuffersConfigType, TYPEDEF, CANIF_CONST) CanIf_MappedTxBuffersConfigPtrType;

/**   \brief  type used to point to CanIf_RxIndicationFctList */
typedef P2CONST(CanIf_RxIndicationFctListType, TYPEDEF, CANIF_CONST) CanIf_RxIndicationFctListPtrType;

/**   \brief  type used to point to CanIf_RxPduConfig */
typedef P2CONST(CanIf_RxPduConfigType, TYPEDEF, CANIF_CONST) CanIf_RxPduConfigPtrType;

/**   \brief  type used to point to CanIf_TrcvToCtrlMap */
typedef P2CONST(CanIf_TrcvToCtrlMapType, TYPEDEF, CANIF_CONST) CanIf_TrcvToCtrlMapPtrType;

/**   \brief  type used to point to CanIf_TxBufferPrioByCanIdBase */
typedef P2VAR(CanIf_TxBufferPrioByCanIdBaseType, TYPEDEF, CANIF_VAR_NOINIT) CanIf_TxBufferPrioByCanIdBasePtrType;

/**   \brief  type used to point to CanIf_TxBufferPrioByCanIdByteQueueConfig */
typedef P2CONST(CanIf_TxBufferPrioByCanIdByteQueueConfigType, TYPEDEF, CANIF_CONST) CanIf_TxBufferPrioByCanIdByteQueueConfigPtrType;

/**   \brief  type used to point to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus */
typedef P2CONST(CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusType, TYPEDEF, CANIF_CONST) CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusPtrType;

/**   \brief  type used to point to CanIf_TxConfirmationFctList */
typedef P2CONST(CanIf_TxConfirmationFctType, TYPEDEF, CANIF_CONST) CanIf_TxConfirmationFctListPtrType;

/**   \brief  type used to point to CanIf_TxPduConfig */
typedef P2CONST(CanIf_TxPduConfigType, TYPEDEF, CANIF_CONST) CanIf_TxPduConfigPtrType;

/**   \brief  type used to point to CanIf_TxPduQueueIndex */
typedef P2CONST(CanIf_TxPduQueueIndexType, TYPEDEF, CANIF_CONST) CanIf_TxPduQueueIndexPtrType;

/**   \brief  type used to point to CanIf_TxQueue */
typedef P2VAR(CanIf_TxPrioByCanIdByteQueueType, TYPEDEF, CANIF_VAR_NOINIT) CanIf_TxQueuePtrType;

/** 
  \}
*/ 

/** 
  \defgroup  CanIfPCRootValueTypes  CanIf Root Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value representations in root arrays.
  \{
*/ 
/**   \brief  type used in CanIf_PCConfig */
typedef struct sCanIf_PCConfigType
{
  uint8 CanIf_PCConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} CanIf_PCConfigType;

typedef CanIf_PCConfigType CanIf_ConfigType;  /**< A structure type is present for data in each configuration class. This typedef redefines the probably different name to the specified one. */

/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CanIf_BusOffNotificationFctPtr
**********************************************************************************************************************/
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_BusOffNotificationFctType, CANIF_CONST) CanIf_BusOffNotificationFctPtr;
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_CanIfCtrlId2MappedTxBuffersConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_CanIfCtrlId2MappedTxBuffersConfig
  \brief  CAN controller configuration - mapped Tx-buffer(s).
  \details
  Element                          Description
  MappedTxBuffersConfigEndIdx      the end index of the 1:n relation pointing to CanIf_MappedTxBuffersConfig
  MappedTxBuffersConfigStartIdx    the start index of the 1:n relation pointing to CanIf_MappedTxBuffersConfig
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_CanIfCtrlId2MappedTxBuffersConfigType, CANIF_CONST) CanIf_CanIfCtrlId2MappedTxBuffersConfig[6];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_CtrlModeIndicationFctPtr
**********************************************************************************************************************/
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_CtrlModeIndicationFctType, CANIF_CONST) CanIf_CtrlModeIndicationFctPtr;
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_MailBoxConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_MailBoxConfig
  \brief  Mailbox table.
  \details
  Element                 Description
  CtrlStatesIdx           the index of the 1:1 relation pointing to CanIf_CtrlStates
  PduIdFirst              "First" PDU mapped to mailbox.
  PduIdLast               "Last" PDU mapped to mailbox.
  TxBufferCfgIdx          the index of the 0:1 relation pointing to CanIf_TxBufferPrioByCanIdByteQueueConfig
  TxBufferHandlingType
  MailBoxType             Type of mailbox: Rx-/Tx- BasicCAN/FullCAN/unused.
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_MailBoxConfigType, CANIF_CONST) CanIf_MailBoxConfig[226];
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_MappedTxBuffersConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_MappedTxBuffersConfig
  \brief  Mapped Tx-buffer(s)
  \details
  Element             Description
  MailBoxConfigIdx    the index of the 1:1 relation pointing to CanIf_MailBoxConfig
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_MappedTxBuffersConfigType, CANIF_CONST) CanIf_MappedTxBuffersConfig[5];
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_RxIndicationFctList
**********************************************************************************************************************/
/** 
  \var    CanIf_RxIndicationFctList
  \brief  Rx indication functions table.
  \details
  Element               Description
  RxIndicationFct       Rx indication function.
  RxIndicationLayout    Layout of Rx indication function.
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_RxIndicationFctListType, CANIF_CONST) CanIf_RxIndicationFctList[7];
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_RxPduConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_RxPduConfig
  \brief  Rx-PDU configuration table.
  \details
  Element                   Description
  RxPduCanId                Rx-PDU: CAN identifier.
  RxPduMask                 Rx-PDU: CAN identifier mask.
  UpperPduId                PDU ID defined by upper layer.
  Dlc                       Data length code.
  RxIndicationFctListIdx    the index of the 1:1 relation pointing to CanIf_RxIndicationFctList
  RxMetaDataLength          Meta data length.
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_RxPduConfigType, CANIF_CONST) CanIf_RxPduConfig[243];
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TrcvModeIndicationFctPtr
**********************************************************************************************************************/
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_TrcvModeIndicationFctType, CANIF_CONST) CanIf_TrcvModeIndicationFctPtr;
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TrcvToCtrlMap
**********************************************************************************************************************/
/** 
  \var    CanIf_TrcvToCtrlMap
  \brief  Indirection table: logical transceiver index to CAN controller index.
*/ 
#define CANIF_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_TrcvToCtrlMapType, CANIF_CONST) CanIf_TrcvToCtrlMap[3];
#define CANIF_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxBufferPrioByCanIdByteQueueConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_TxBufferPrioByCanIdByteQueueConfig
  \brief  Tx-buffer: PRIO_BY_CANID as BYTE_QUEUE
  \details
  Element                                             Description
  TxBufferPrioByCanIdBaseIdx                          the index of the 1:1 relation pointing to CanIf_TxBufferPrioByCanIdBase
  TxBufferPrioByCanIdByteQueueMappedTxPdusEndIdx      the end index of the 1:n relation pointing to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
  TxBufferPrioByCanIdByteQueueMappedTxPdusLength      the number of relations pointing to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
  TxBufferPrioByCanIdByteQueueMappedTxPdusStartIdx    the start index of the 1:n relation pointing to CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_TxBufferPrioByCanIdByteQueueConfigType, CANIF_CONST) CanIf_TxBufferPrioByCanIdByteQueueConfig[5];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
**********************************************************************************************************************/
/** 
  \var    CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus
  \brief  Tx-buffer: PRIO_BY_CANID as BYTE_QUEUE: Mapped Tx-PDUs
  \details
  Element           Description
  TxPduConfigIdx    the index of the 1:1 relation pointing to CanIf_TxPduConfig
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdusType, CANIF_CONST) CanIf_TxBufferPrioByCanIdByteQueueMappedTxPdus[146];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxConfirmationFctList
**********************************************************************************************************************/
/** 
  \var    CanIf_TxConfirmationFctList
  \brief  Tx confirmation functions table.
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_TxConfirmationFctType, CANIF_CONST) CanIf_TxConfirmationFctList[7];
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxPduConfig
**********************************************************************************************************************/
/** 
  \var    CanIf_TxPduConfig
  \brief  Tx-PDUs - configuration.
  \details
  Element                     Description
  CanId                       CAN identifier (16bit / 32bit).
  CanIdTxMask                 CAN identifier mask (16bit / 32bit).
  UpperLayerTxPduId           Upper layer handle ID (8bit / 16bit).
  CtrlStatesIdx               the index of the 1:1 relation pointing to CanIf_CtrlStates
  Dlc                         Data length code.
  MailBoxConfigIdx            the index of the 1:1 relation pointing to CanIf_MailBoxConfig
  TxConfirmationFctListIdx    the index of the 1:1 relation pointing to CanIf_TxConfirmationFctList
  TxMetaDataLength            Meta data length.
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_TxPduConfigType, CANIF_CONST) CanIf_TxPduConfig[212];
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxPduQueueIndex
**********************************************************************************************************************/
/** 
  \var    CanIf_TxPduQueueIndex
  \brief  Indirection table: Tx-PDU handle ID to corresponding Tx buffer handle ID. NOTE: Only BasicCAN Tx-PDUs have a valid indirection into the Tx buffer.
  \details
  Element       Description
  TxQueueIdx    the index of the 0:1 relation pointing to CanIf_TxQueue
*/ 
#define CANIF_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanIf_TxPduQueueIndexType, CANIF_CONST) CanIf_TxPduQueueIndex[212];
#define CANIF_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_CtrlStates
**********************************************************************************************************************/
/** 
  \var    CanIf_CtrlStates
  \details
  Element     Description
  CtrlMode    Controller mode.
  PduMode     PDU mode state.
*/ 
#define CANIF_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(CanIf_CtrlStatesUType, CANIF_VAR_NOINIT) CanIf_CtrlStates;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define CANIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxBufferPrioByCanIdBase
**********************************************************************************************************************/
/** 
  \var    CanIf_TxBufferPrioByCanIdBase
  \brief  Variable declaration - Tx-buffer: PRIO_BY_CANID as byte/bit-queue. Stores at least the QueueCounter.
*/ 
#define CANIF_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(CanIf_TxBufferPrioByCanIdBaseUType, CANIF_VAR_NOINIT) CanIf_TxBufferPrioByCanIdBase;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define CANIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanIf_TxQueue
**********************************************************************************************************************/
/** 
  \var    CanIf_TxQueue
  \brief  Variable declaration - Tx byte queue.
*/ 
#define CANIF_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(CanIf_TxQueueUType, CANIF_VAR_NOINIT) CanIf_TxQueue;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define CANIF_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/




#endif  /* CANIF_CFG_H */
/**********************************************************************************************************************
  END OF FILE: CanIf_Cfg.h
**********************************************************************************************************************/


