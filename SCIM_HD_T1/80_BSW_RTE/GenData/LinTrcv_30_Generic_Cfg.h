/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: LinTrcv
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: LinTrcv_30_Generic_Cfg.h
 *   Generation Time: 2020-11-11 14:25:30
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/



/* PRQA S 0779, 0883 EOF */ /* MD_MSR_5.1_779 */

#if !defined(LINTRCV_30_GENERIC_CFG_H)
#define LINTRCV_30_GENERIC_CFG_H

/* -----------------------------------------------------------------------------
    &&&~ Includes
 ----------------------------------------------------------------------------- */
#include "LinTrcv_GeneralTypes.h"
#include "EcuM_Cbk.h"
#include "Dio.h"

/* -----------------------------------------------------------------------------
    &&&~ Version defines
 ----------------------------------------------------------------------------- */
#define DRVTRANS_LIN_30_GENERIC_GENTOOL_CFG5_MAJOR_VERSION 0x06u
#define DRVTRANS_LIN_30_GENERIC_GENTOOL_CFG5_MINOR_VERSION 0x01u
#define DRVTRANS_LIN_30_GENERIC_GENTOOL_CFG5_PATCH_VERSION 0x01u

/* -----------------------------------------------------------------------------
    &&&~ Defines
 ----------------------------------------------------------------------------- */
#define LINTRCV_30_GENERIC_GET_VERSION_INFO         STD_OFF
#define LINTRCV_30_GENERIC_DEV_ERROR_DETECT         STD_OFF
#define LINTRCV_30_GENERIC_DEV_ERROR_REPORT         STD_OFF

#define LINTRCV_30_GENERIC_CRC_CHECK                STD_OFF
#define LINTRCV_30_GENERIC_NROFCHANNELS             5
#define LINTRCV_30_GENERIC_INSTANCE_ID              0

#define LINTRCV_30_GENERIC_WAKEUPSUPPORT            STD_OFF

#define LINTRCV_30_GENERIC_WAIT_COUNT_USED          STD_OFF
#define LINTRCV_30_GENERIC_WAIT_COUNT               0

/**********************************************************************************************************************
 *  General Defines
 *********************************************************************************************************************/

#ifndef LINTRCV_30_GENERIC_USE_DUMMY_STATEMENT
#define LINTRCV_30_GENERIC_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef LINTRCV_30_GENERIC_DUMMY_STATEMENT
#define LINTRCV_30_GENERIC_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef LINTRCV_30_GENERIC_DUMMY_STATEMENT_CONST
#define LINTRCV_30_GENERIC_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef LINTRCV_30_GENERIC_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define LINTRCV_30_GENERIC_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef LINTRCV_30_GENERIC_ATOMIC_VARIABLE_ACCESS
#define LINTRCV_30_GENERIC_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef LINTRCV_30_GENERIC_PROCESSOR_MPC5746C
#define LINTRCV_30_GENERIC_PROCESSOR_MPC5746C
#endif
#ifndef LINTRCV_30_GENERIC_COMP_DIAB
#define LINTRCV_30_GENERIC_COMP_DIAB
#endif
#ifndef LINTRCV_30_GENERIC_GEN_GENERATOR_MSR
#define LINTRCV_30_GENERIC_GEN_GENERATOR_MSR
#endif
#ifndef LINTRCV_30_GENERIC_CPUTYPE_BITORDER_MSB2LSB
#define LINTRCV_30_GENERIC_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef LINTRCV_30_GENERIC_CONFIGURATION_VARIANT_PRECOMPILE
#define LINTRCV_30_GENERIC_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef LINTRCV_30_GENERIC_CONFIGURATION_VARIANT_LINKTIME
#define LINTRCV_30_GENERIC_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef LINTRCV_30_GENERIC_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define LINTRCV_30_GENERIC_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef LINTRCV_30_GENERIC_CONFIGURATION_VARIANT
#define LINTRCV_30_GENERIC_CONFIGURATION_VARIANT LINTRCV_30_GENERIC_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef LINTRCV_30_GENERIC_POSTBUILD_VARIANT_SUPPORT
#define LINTRCV_30_GENERIC_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif



/* -----------------------------------------------------------------------------
    &&&~ Structs
 ----------------------------------------------------------------------------- */

/**********************************************************************************************************************
  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  LinTrcv_30_GenericPCDataSwitches  LinTrcv_30_Generic Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define LINTRCV_30_GENERIC_CHANNEL                                    STD_ON
#define LINTRCV_30_GENERIC_CHANNELUSEDOFCHANNEL                       STD_ON
#define LINTRCV_30_GENERIC_ECUMWAKEUPSOURCEIDOFCHANNEL                STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_Channel.EcuMWakeupSourceId' Reason: 'the value of LinTrcv_30_Generic_EcuMWakeupSourceIdOfChannel is always 'LINTRCV_30_GENERIC_NO_ECUMWAKEUPSOURCEIDOFCHANNEL' due to this, the array is deactivated.' */
#define LINTRCV_30_GENERIC_ICUCHANNELIDOFCHANNEL                      STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_Channel.IcuChannelId' Reason: 'the value of LinTrcv_30_Generic_IcuChannelIdOfChannel is always 'LINTRCV_30_GENERIC_NO_ICUCHANNELIDOFCHANNEL' due to this, the array is deactivated.' */
#define LINTRCV_30_GENERIC_ICUCHANNELIDUSEDOFCHANNEL                  STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_Channel.IcuChannelIdUsed' Reason: 'the value of LinTrcv_30_Generic_IcuChannelIdUsedOfChannel is always 'false' due to this, the array is deactivated.' */
#define LINTRCV_30_GENERIC_INITSTATEOFCHANNEL                         STD_ON
#define LINTRCV_30_GENERIC_WAKEUPBYBUSUSEDOFCHANNEL                   STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_Channel.WakeupByBusUsed' Reason: 'the value of LinTrcv_30_Generic_WakeupByBusUsedOfChannel is always 'false' due to this, the array is deactivated.' */
#define LINTRCV_30_GENERIC_WAKEUPBYPINUSEDOFCHANNEL                   STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_Channel.WakeupByPinUsed' Reason: 'the value of LinTrcv_30_Generic_WakeupByPinUsedOfChannel is always 'false' due to this, the array is deactivated.' */
#define LINTRCV_30_GENERIC_DIOCHANNEL                                 STD_ON
#define LINTRCV_30_GENERIC_LINDIOENOFDIOCHANNEL                       STD_ON
#define LINTRCV_30_GENERIC_FINALMAGICNUMBER                           STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define LINTRCV_30_GENERIC_INITDATAHASHCODE                           STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define LINTRCV_30_GENERIC_PCCONFIG                                   STD_ON
#define LINTRCV_30_GENERIC_CHANNELOFPCCONFIG                          STD_ON
#define LINTRCV_30_GENERIC_DIOCHANNELOFPCCONFIG                       STD_ON
#define LINTRCV_30_GENERIC_FINALMAGICNUMBEROFPCCONFIG                 STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define LINTRCV_30_GENERIC_INITDATAHASHCODEOFPCCONFIG                 STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
/** 
  \}
*/ 

/** 
  \defgroup  LinTrcv_30_GenericPCIsReducedToDefineDefines  LinTrcv_30_Generic Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define LINTRCV_30_GENERIC_ISDEF_CHANNELUSEDOFCHANNEL                 STD_OFF
#define LINTRCV_30_GENERIC_ISDEF_INITSTATEOFCHANNEL                   STD_OFF
#define LINTRCV_30_GENERIC_ISDEF_LINDIOENOFDIOCHANNEL                 STD_OFF
#define LINTRCV_30_GENERIC_ISDEF_CHANNELOFPCCONFIG                    STD_ON
#define LINTRCV_30_GENERIC_ISDEF_DIOCHANNELOFPCCONFIG                 STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  LinTrcv_30_GenericPCEqualsAlwaysToDefines  LinTrcv_30_Generic Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define LINTRCV_30_GENERIC_EQ2_CHANNELUSEDOFCHANNEL                   
#define LINTRCV_30_GENERIC_EQ2_INITSTATEOFCHANNEL                     
#define LINTRCV_30_GENERIC_EQ2_LINDIOENOFDIOCHANNEL                   
#define LINTRCV_30_GENERIC_EQ2_CHANNELOFPCCONFIG                      LinTrcv_30_Generic_Channel
#define LINTRCV_30_GENERIC_EQ2_DIOCHANNELOFPCCONFIG                   LinTrcv_30_Generic_DioChannel
/** 
  \}
*/ 

/** 
  \defgroup  LinTrcv_30_GenericPCSymbolicInitializationPointers  LinTrcv_30_Generic Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define LinTrcv_30_Generic_Config_Ptr                                 NULL_PTR  /**< symbolic identifier which shall be used to initialize 'LinTrcv_30_Generic' */
/** 
  \}
*/ 

/** 
  \defgroup  LinTrcv_30_GenericPCInitializationSymbols  LinTrcv_30_Generic Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define LinTrcv_30_Generic_Config                                     NULL_PTR  /**< symbolic identifier which could be used to initialize 'LinTrcv_30_Generic */
/** 
  \}
*/ 

/** 
  \defgroup  LinTrcv_30_GenericPCGeneral  LinTrcv_30_Generic General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define LINTRCV_30_GENERIC_CHECK_INIT_POINTER                         STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define LINTRCV_30_GENERIC_FINAL_MAGIC_NUMBER                         0x401Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of LinTrcv_30_Generic */
#define LINTRCV_30_GENERIC_INDIVIDUAL_POSTBUILD                       STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'LinTrcv_30_Generic' is not configured to be postbuild capable. */
#define LINTRCV_30_GENERIC_INIT_DATA                                  LINTRCV_30_GENERIC_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define LINTRCV_30_GENERIC_INIT_DATA_HASH_CODE                        -168160307L  /**< the precompile constant to validate the initialization structure at initialization time of LinTrcv_30_Generic with a hashcode. The seed value is '0x401Eu' */
#define LINTRCV_30_GENERIC_USE_ECUM_BSW_ERROR_HOOK                    STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define LINTRCV_30_GENERIC_USE_INIT_POINTER                           STD_OFF  /**< STD_ON if the init pointer LinTrcv_30_Generic shall be used. */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  LinTrcv_30_GenericLTDataSwitches  LinTrcv_30_Generic Data Switches  (LINK)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define LINTRCV_30_GENERIC_LTCONFIG                                   STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_LTConfig' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  LinTrcv_30_GenericPBDataSwitches  LinTrcv_30_Generic Data Switches  (POST_BUILD)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define LINTRCV_30_GENERIC_PBCONFIG                                   STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_PBConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define LINTRCV_30_GENERIC_LTCONFIGIDXOFPBCONFIG                      STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_PBConfig.LTConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define LINTRCV_30_GENERIC_PCCONFIGIDXOFPBCONFIG                      STD_OFF  /**< Deactivateable: 'LinTrcv_30_Generic_PBConfig.PCConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
/** 
  \}
*/ 



/**********************************************************************************************************************
  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 MACROS_3453 */  /* MD_CSL_3453 */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  LinTrcv_30_GenericPCGetConstantDuplicatedRootDataMacros  LinTrcv_30_Generic Get Constant Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated by constance root data elements.
  \{
*/ 
#define LinTrcv_30_Generic_GetChannelOfPCConfig()                     LinTrcv_30_Generic_Channel  /**< the pointer to LinTrcv_30_Generic_Channel */
#define LinTrcv_30_Generic_GetDioChannelOfPCConfig()                  LinTrcv_30_Generic_DioChannel  /**< the pointer to LinTrcv_30_Generic_DioChannel */
/** 
  \}
*/ 

/** 
  \defgroup  LinTrcv_30_GenericPCGetDataMacros  LinTrcv_30_Generic Get Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read CONST and VAR data.
  \{
*/ 
#define LinTrcv_30_Generic_IsChannelUsedOfChannel(Index)              ((LinTrcv_30_Generic_GetChannelOfPCConfig()[(Index)].ChannelUsedOfChannel) != FALSE)
#define LinTrcv_30_Generic_GetInitStateOfChannel(Index)               (LinTrcv_30_Generic_GetChannelOfPCConfig()[(Index)].InitStateOfChannel)
#define LinTrcv_30_Generic_GetLinDioEnOfDioChannel(Index)             (LinTrcv_30_Generic_GetDioChannelOfPCConfig()[(Index)].LinDioEnOfDioChannel)
/** 
  \}
*/ 

/** 
  \defgroup  LinTrcv_30_GenericPCHasMacros  LinTrcv_30_Generic Has Macros (PRE_COMPILE)
  \brief  These macros can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
#define LinTrcv_30_Generic_HasChannel()                               (TRUE != FALSE)
#define LinTrcv_30_Generic_HasChannelUsedOfChannel()                  (TRUE != FALSE)
#define LinTrcv_30_Generic_HasInitStateOfChannel()                    (TRUE != FALSE)
#define LinTrcv_30_Generic_HasDioChannel()                            (TRUE != FALSE)
#define LinTrcv_30_Generic_HasLinDioEnOfDioChannel()                  (TRUE != FALSE)
#define LinTrcv_30_Generic_HasPCConfig()                              (TRUE != FALSE)
#define LinTrcv_30_Generic_HasChannelOfPCConfig()                     (TRUE != FALSE)
#define LinTrcv_30_Generic_HasDioChannelOfPCConfig()                  (TRUE != FALSE)
/** 
  \}
*/ 

  /* PRQA L:MACROS_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 MACROS_3453 */  /* MD_CSL_3453 */
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:MACROS_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 MACROS_3453 */  /* MD_CSL_3453 */
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:MACROS_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  LinTrcv_30_GenericPCIterableTypes  LinTrcv_30_Generic Iterable Types (PRE_COMPILE)
  \brief  These type definitions are used to iterate over an array with least processor cycles for variable access as possible.
  \{
*/ 
/**   \brief  type used to iterate LinTrcv_30_Generic_Channel */
typedef uint8_least LinTrcv_30_Generic_ChannelIterType;

/**   \brief  type used to iterate LinTrcv_30_Generic_DioChannel */
typedef uint8_least LinTrcv_30_Generic_DioChannelIterType;

/** 
  \}
*/ 

/** 
  \defgroup  LinTrcv_30_GenericPCValueTypes  LinTrcv_30_Generic Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value based data representations.
  \{
*/ 
/**   \brief  value based type definition for LinTrcv_30_Generic_ChannelUsedOfChannel */
typedef boolean LinTrcv_30_Generic_ChannelUsedOfChannelType;

/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  LinTrcv_30_GenericPCStructTypes  LinTrcv_30_Generic Struct Types (PRE_COMPILE)
  \brief  These type definitions are used for structured data representations.
  \{
*/ 
/**   \brief  type used in LinTrcv_30_Generic_Channel */
typedef struct sLinTrcv_30_Generic_ChannelType
{
  LinTrcv_30_Generic_ChannelUsedOfChannelType ChannelUsedOfChannel;
  LinTrcv_TrcvModeType InitStateOfChannel;
} LinTrcv_30_Generic_ChannelType;

/**   \brief  type used in LinTrcv_30_Generic_DioChannel */
typedef struct sLinTrcv_30_Generic_DioChannelType
{
  Dio_ChannelType LinDioEnOfDioChannel;
} LinTrcv_30_Generic_DioChannelType;

/** 
  \}
*/ 

/** 
  \defgroup  LinTrcv_30_GenericPCRootValueTypes  LinTrcv_30_Generic Root Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value representations in root arrays.
  \{
*/ 
/**   \brief  type used in LinTrcv_30_Generic_PCConfig */
typedef struct sLinTrcv_30_Generic_PCConfigType
{
  uint8 LinTrcv_30_Generic_PCConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} LinTrcv_30_Generic_PCConfigType;

typedef LinTrcv_30_Generic_PCConfigType LinTrcv_30_Generic_ConfigType;  /**< A structure type is present for data in each configuration class. This typedef redefines the probably different name to the specified one. */

/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  LinTrcv_30_Generic_Channel
**********************************************************************************************************************/
/** 
  \var    LinTrcv_30_Generic_Channel
  \details
  Element        Description
  ChannelUsed
  InitState  
*/ 
#define LINTRCV_30_GENERIC_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
extern CONST(LinTrcv_30_Generic_ChannelType, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_Channel[5];
#define LINTRCV_30_GENERIC_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */

/**********************************************************************************************************************
  LinTrcv_30_Generic_DioChannel
**********************************************************************************************************************/
/** 
  \var    LinTrcv_30_Generic_DioChannel
  \details
  Element     Description
  LinDioEn
*/ 
#define LINTRCV_30_GENERIC_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */
extern CONST(LinTrcv_30_Generic_DioChannelType, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_DioChannel[5];
#define LINTRCV_30_GENERIC_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_19.1 */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/


typedef struct sLinTrcv_30_Generic_ChannelDataTypeTag
{
  VAR(LinTrcv_TrcvWakeupReasonType , AUTOMATIC)   LinTrcv_30_Generic_WakeupReason;
  VAR(LinTrcv_TrcvWakeupModeType , AUTOMATIC)     LinTrcv_30_Generic_WakeupMode;
  VAR(LinTrcv_TrcvModeType , AUTOMATIC)           LinTrcv_30_Generic_State;
}LinTrcv_30_Generic_ChannelDataType;

#define LINTRCV_30_GENERIC_START_SEC_CONST_8BIT
/* MISRA RULE 87 VIOLATION: Only preprocessor statements and comments before '#include'. */
#include "MemMap.h"

extern CONST(uint8, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_NrOfChannels;
extern CONST(uint8, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_InstanceId;
extern CONST(uint32, LINTRCV_30_GENERIC_CONST) LinTrcv_30_Generic_WaitCount;

#define LINTRCV_30_GENERIC_STOP_SEC_CONST_8BIT
/* MISRA RULE 87 VIOLATION: Only preprocessor statements and comments before '#include'. */
#include "MemMap.h"


#define LINTRCV_30_GENERIC_START_SEC_VAR_NOINIT_UNSPECIFIED
/* MISRA RULE 87 VIOLATION: Only preprocessor statements and comments before '#include'. */
#include "MemMap.h"

extern VAR(LinTrcv_30_Generic_ChannelDataType, LINTRCV_30_GENERIC_VAR_NOINIT) LinTrcv_30_Generic_ChannelData[LINTRCV_30_GENERIC_NROFCHANNELS];
#define LINTRCV_30_GENERIC_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/* MISRA RULE 87 VIOLATION: Only preprocessor statements and comments before '#include'. */
#include "MemMap.h"





#endif /* LINTRCV_30_GENERIC_CFG_H */

