/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: CanTrcv
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: CanTrcv_30_GenericCan_Cfg.h
 *   Generation Time: 2020-11-11 14:25:28
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


#if !defined(CANTRCV_30_GENERICCAN_CFG_H)
#define CANTRCV_30_GENERICCAN_CFG_H

/**********************************************************************************************************************
  INCLUDES
**********************************************************************************************************************/
/* SREQ00010148 */
# include "ComStack_Types.h" 
# include "Can_GeneralTypes.h"


#include "Dio.h"
#include "CanIf.h"
#include "EcuM_Cbk.h"

#include "Icu.h"


/**********************************************************************************************************************
  VERSION DEFINES (Adapted with: ESCAN00086365)
**********************************************************************************************************************/
#define DRVTRANS_CAN_30_GENERICCAN_GENTOOL_CFG5_MAJOR_VERSION 0x03u
#define DRVTRANS_CAN_30_GENERICCAN_GENTOOL_CFG5_MINOR_VERSION 0x01u
#define DRVTRANS_CAN_30_GENERICCAN_GENTOOL_CFG5_PATCH_VERSION 0x00u

#define CANTRCV_30_GENERICCAN_GENTOOL_CFG5_BASE_COMP_VERSION 0x0105u
#define CANTRCV_30_GENERICCAN_GENTOOL_CFG5_HW_COMP_VERSION   0x0102u


/**********************************************************************************************************************
  SWITCHES (BASE)
**********************************************************************************************************************/
#define CANTRCV_30_GENERICCAN_MAX_CHANNEL                       3u
#define CANTRCV_30_GENERICCAN_GENERAL_WAKE_UP_SUPPORT           CANTRCV_30_GENERICCAN_WAKEUP_NOT_SUPPORTED
#define CANTRCV_30_GENERICCAN_WAKEUP_BY_BUS_USED                STD_OFF
#define CANTRCV_30_GENERICCAN_GET_VERSION_INFO                  STD_OFF
#define CANTRCV_30_GENERICCAN_DEV_ERROR_DETECT                  STD_OFF
#define CANTRCV_30_GENERICCAN_DEV_ERROR_REPORT                  STD_OFF
#define CANTRCV_30_GENERICCAN_ECUC_SAFE_BSW_CHECKS              STD_OFF
#define CANTRCV_30_GENERICCAN_PROD_ERROR_DETECT                 STD_OFF 
#define CANTRCV_30_GENERICCAN_INSTANCE_ID                       0
#define CANTRCV_30_GENERICCAN_HW_PN_SUPPORT                     STD_OFF
#define CANTRCV_30_GENERICCAN_BUS_ERR_FLAG                      STD_OFF
#define CANTRCV_30_GENERICCAN_VERIFY_DATA                       STD_ON  
#define CANTRCV_30_GENERICCAN_SPI_ACCESS_SYNCHRONOUS            STD_OFF
#define CANTRCV_30_GENERICCAN_USE_ICU                           STD_ON
#define CANTRCV_30_GENERICCAN_USE_EXTENDED_WU_SRC               STD_OFF
#define CANTRCV_30_GENERICCAN_SET_OP_MODE_WAIT_TIME_SUPPORT     STD_OFF


#define CanTrcv_30_GenericCan_CanTrcvChannel_CAN2STB 0u 
#define CanTrcv_30_GenericCan_CanTrcvChannel_CAN3STB 1u 
#define CanTrcv_30_GenericCan_CanTrcvChannel_CAN4STB 2u 


#ifndef CANTRCV_30_GENERICCAN_USE_DUMMY_STATEMENT
#define CANTRCV_30_GENERICCAN_USE_DUMMY_STATEMENT STD_OFF /* /MICROSAR/EcuC/EcucGeneral/DummyStatement */
#endif
#ifndef CANTRCV_30_GENERICCAN_DUMMY_STATEMENT
#define CANTRCV_30_GENERICCAN_DUMMY_STATEMENT(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CANTRCV_30_GENERICCAN_DUMMY_STATEMENT_CONST
#define CANTRCV_30_GENERICCAN_DUMMY_STATEMENT_CONST(v)  /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */  /* /MICROSAR/vSet/vSetGeneral/vSetDummyStatementKind */
#endif
#ifndef CANTRCV_30_GENERICCAN_ATOMIC_BIT_ACCESS_IN_BITFIELD
#define CANTRCV_30_GENERICCAN_ATOMIC_BIT_ACCESS_IN_BITFIELD STD_OFF /* /MICROSAR/EcuC/EcucGeneral/AtomicBitAccessInBitfield */
#endif
#ifndef CANTRCV_30_GENERICCAN_ATOMIC_VARIABLE_ACCESS
#define CANTRCV_30_GENERICCAN_ATOMIC_VARIABLE_ACCESS 32u /* /MICROSAR/EcuC/EcucGeneral/AtomicVariableAccess */
#endif
#ifndef CANTRCV_30_GENERICCAN_PROCESSOR_MPC5746C
#define CANTRCV_30_GENERICCAN_PROCESSOR_MPC5746C
#endif
#ifndef CANTRCV_30_GENERICCAN_COMP_DIAB
#define CANTRCV_30_GENERICCAN_COMP_DIAB
#endif
#ifndef CANTRCV_30_GENERICCAN_GEN_GENERATOR_MSR
#define CANTRCV_30_GENERICCAN_GEN_GENERATOR_MSR
#endif
#ifndef CANTRCV_30_GENERICCAN_CPUTYPE_BITORDER_MSB2LSB
#define CANTRCV_30_GENERICCAN_CPUTYPE_BITORDER_MSB2LSB /* /MICROSAR/vSet/vSetPlatform/vSetBitOrder */
#endif
#ifndef CANTRCV_30_GENERICCAN_CONFIGURATION_VARIANT_PRECOMPILE
#define CANTRCV_30_GENERICCAN_CONFIGURATION_VARIANT_PRECOMPILE 1
#endif
#ifndef CANTRCV_30_GENERICCAN_CONFIGURATION_VARIANT_LINKTIME
#define CANTRCV_30_GENERICCAN_CONFIGURATION_VARIANT_LINKTIME 2
#endif
#ifndef CANTRCV_30_GENERICCAN_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE
#define CANTRCV_30_GENERICCAN_CONFIGURATION_VARIANT_POSTBUILD_LOADABLE 3
#endif
#ifndef CANTRCV_30_GENERICCAN_CONFIGURATION_VARIANT
#define CANTRCV_30_GENERICCAN_CONFIGURATION_VARIANT CANTRCV_30_GENERICCAN_CONFIGURATION_VARIANT_PRECOMPILE
#endif
#ifndef CANTRCV_30_GENERICCAN_POSTBUILD_VARIANT_SUPPORT
#define CANTRCV_30_GENERICCAN_POSTBUILD_VARIANT_SUPPORT STD_OFF
#endif


/**********************************************************************************************************************
  SWITCHES (HW SPECIFIC)
**********************************************************************************************************************/

#define CANTRCV_30_GENERICCAN_INTERFACE_DIO


/**********************************************************************************************************************
  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanTrcv_30_GenericCanPCDataSwitches  CanTrcv_30_GenericCan Data Switches  (PRE_COMPILE)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CANTRCV_30_GENERICCAN_CHANNEL                                 STD_ON
#define CANTRCV_30_GENERICCAN_ICUCHANNELOFCHANNEL                     STD_ON
#define CANTRCV_30_GENERICCAN_ICUCHANNELSETOFCHANNEL                  STD_ON
#define CANTRCV_30_GENERICCAN_CHANNELUSED                             STD_ON
#define CANTRCV_30_GENERICCAN_DIOCONFIGURATION                        STD_ON
#define CANTRCV_30_GENERICCAN_CANSTBOFDIOCONFIGURATION                STD_ON
#define CANTRCV_30_GENERICCAN_FINALMAGICNUMBER                        STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define CANTRCV_30_GENERICCAN_GENERATORCOMPATIBILITYVERSION           STD_ON
#define CANTRCV_30_GENERICCAN_INITDATAHASHCODE                        STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define CANTRCV_30_GENERICCAN_PNCFG                                   STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnCfg' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define CANTRCV_30_GENERICCAN_BAUDRATEOFPNCFG                         STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnCfg.Baudrate' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_CANIDMASKOFPNCFG                        STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnCfg.CanIdMask' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_CANIDOFPNCFG                            STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnCfg.CanId' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_DLCOFPNCFG                              STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnCfg.Dlc' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_EXTENDEDCANIDOFPNCFG                    STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnCfg.ExtendedCanId' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_PNPAYLOADCFG                            STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnPayloadCfg' Reason: 'the struct is deactivated because all elements are deactivated.' */
#define CANTRCV_30_GENERICCAN_DATAMASK0OFPNPAYLOADCFG                 STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnPayloadCfg.DataMask0' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_DATAMASK1OFPNPAYLOADCFG                 STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnPayloadCfg.DataMask1' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_DATAMASK2OFPNPAYLOADCFG                 STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnPayloadCfg.DataMask2' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_DATAMASK3OFPNPAYLOADCFG                 STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnPayloadCfg.DataMask3' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_DATAMASK4OFPNPAYLOADCFG                 STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnPayloadCfg.DataMask4' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_DATAMASK5OFPNPAYLOADCFG                 STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnPayloadCfg.DataMask5' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_DATAMASK6OFPNPAYLOADCFG                 STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnPayloadCfg.DataMask6' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_DATAMASK7OFPNPAYLOADCFG                 STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PnPayloadCfg.DataMask7' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_SIZEOFCHANNEL                           STD_ON
#define CANTRCV_30_GENERICCAN_SIZEOFCHANNELUSED                       STD_ON
#define CANTRCV_30_GENERICCAN_SIZEOFDIOCONFIGURATION                  STD_ON
#define CANTRCV_30_GENERICCAN_SIZEOFWAKEUPBYBUSUSED                   STD_ON
#define CANTRCV_30_GENERICCAN_SIZEOFWAKEUPSOURCE                      STD_ON
#define CANTRCV_30_GENERICCAN_WAKEUPBYBUSUSED                         STD_ON
#define CANTRCV_30_GENERICCAN_WAKEUPSOURCE                            STD_ON
#define CANTRCV_30_GENERICCAN_WUSRCPOR                                STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_WuSrcPor' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_WUSRCSYSERR                             STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_WuSrcSyserr' Reason: 'the array is deactivated because the size is 0 and the piece of data is in the configuration class: PRE_COMPILE' */
#define CANTRCV_30_GENERICCAN_PCCONFIG                                STD_ON
#define CANTRCV_30_GENERICCAN_CHANNELOFPCCONFIG                       STD_ON
#define CANTRCV_30_GENERICCAN_CHANNELUSEDOFPCCONFIG                   STD_ON
#define CANTRCV_30_GENERICCAN_DIOCONFIGURATIONOFPCCONFIG              STD_ON
#define CANTRCV_30_GENERICCAN_FINALMAGICNUMBEROFPCCONFIG              STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PCConfig.FinalMagicNumber' Reason: 'the module configuration does not support flashing of data.' */
#define CANTRCV_30_GENERICCAN_GENERATORCOMPATIBILITYVERSIONOFPCCONFIG STD_ON
#define CANTRCV_30_GENERICCAN_INITDATAHASHCODEOFPCCONFIG              STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PCConfig.InitDataHashCode' Reason: 'the module configuration does not support flashing of data.' */
#define CANTRCV_30_GENERICCAN_SIZEOFCHANNELOFPCCONFIG                 STD_ON
#define CANTRCV_30_GENERICCAN_SIZEOFCHANNELUSEDOFPCCONFIG             STD_ON
#define CANTRCV_30_GENERICCAN_SIZEOFDIOCONFIGURATIONOFPCCONFIG        STD_ON
#define CANTRCV_30_GENERICCAN_SIZEOFWAKEUPBYBUSUSEDOFPCCONFIG         STD_ON
#define CANTRCV_30_GENERICCAN_SIZEOFWAKEUPSOURCEOFPCCONFIG            STD_ON
#define CANTRCV_30_GENERICCAN_WAKEUPBYBUSUSEDOFPCCONFIG               STD_ON
#define CANTRCV_30_GENERICCAN_WAKEUPSOURCEOFPCCONFIG                  STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCIsReducedToDefineDefines  CanTrcv_30_GenericCan Is Reduced To Define Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define is STD_ON else STD_OFF.
  \{
*/ 
#define CANTRCV_30_GENERICCAN_ISDEF_ICUCHANNELOFCHANNEL               STD_OFF
#define CANTRCV_30_GENERICCAN_ISDEF_ICUCHANNELSETOFCHANNEL            STD_OFF
#define CANTRCV_30_GENERICCAN_ISDEF_CHANNELUSED                       STD_OFF
#define CANTRCV_30_GENERICCAN_ISDEF_CANSTBOFDIOCONFIGURATION          STD_OFF
#define CANTRCV_30_GENERICCAN_ISDEF_WAKEUPBYBUSUSED                   STD_OFF
#define CANTRCV_30_GENERICCAN_ISDEF_WAKEUPSOURCE                      STD_OFF
#define CANTRCV_30_GENERICCAN_ISDEF_CHANNELOFPCCONFIG                 STD_ON
#define CANTRCV_30_GENERICCAN_ISDEF_CHANNELUSEDOFPCCONFIG             STD_ON
#define CANTRCV_30_GENERICCAN_ISDEF_DIOCONFIGURATIONOFPCCONFIG        STD_ON
#define CANTRCV_30_GENERICCAN_ISDEF_WAKEUPBYBUSUSEDOFPCCONFIG         STD_ON
#define CANTRCV_30_GENERICCAN_ISDEF_WAKEUPSOURCEOFPCCONFIG            STD_ON
/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCEqualsAlwaysToDefines  CanTrcv_30_GenericCan Equals Always To Defines (PRE_COMPILE)
  \brief  If all values in a CONST array or an element in a CONST array of structs are equal, the define contains the always equals value.
  \{
*/ 
#define CANTRCV_30_GENERICCAN_EQ2_ICUCHANNELOFCHANNEL                 
#define CANTRCV_30_GENERICCAN_EQ2_ICUCHANNELSETOFCHANNEL              
#define CANTRCV_30_GENERICCAN_EQ2_CHANNELUSED                         
#define CANTRCV_30_GENERICCAN_EQ2_CANSTBOFDIOCONFIGURATION            
#define CANTRCV_30_GENERICCAN_EQ2_WAKEUPBYBUSUSED                     
#define CANTRCV_30_GENERICCAN_EQ2_WAKEUPSOURCE                        
#define CANTRCV_30_GENERICCAN_EQ2_CHANNELOFPCCONFIG                   CanTrcv_30_GenericCan_Channel
#define CANTRCV_30_GENERICCAN_EQ2_CHANNELUSEDOFPCCONFIG               CanTrcv_30_GenericCan_ChannelUsed
#define CANTRCV_30_GENERICCAN_EQ2_DIOCONFIGURATIONOFPCCONFIG          CanTrcv_30_GenericCan_DioConfiguration
#define CANTRCV_30_GENERICCAN_EQ2_WAKEUPBYBUSUSEDOFPCCONFIG           CanTrcv_30_GenericCan_WakeupByBusUsed
#define CANTRCV_30_GENERICCAN_EQ2_WAKEUPSOURCEOFPCCONFIG              CanTrcv_30_GenericCan_WakeupSource
/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCSymbolicInitializationPointers  CanTrcv_30_GenericCan Symbolic Initialization Pointers (PRE_COMPILE)
  \brief  Symbolic initialization pointers to be used in the call of a preinit or init function.
  \{
*/ 
#define CanTrcv_30_GenericCan_Config_Ptr                              NULL_PTR  /**< symbolic identifier which shall be used to initialize 'CanTrcv_30_GenericCan' */
/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCInitializationSymbols  CanTrcv_30_GenericCan Initialization Symbols (PRE_COMPILE)
  \brief  Symbolic initialization pointers which may be used in the call of a preinit or init function. Please note, that the defined value can be a 'NULL_PTR' and the address operator is not usable.
  \{
*/ 
#define CanTrcv_30_GenericCan_Config                                  NULL_PTR  /**< symbolic identifier which could be used to initialize 'CanTrcv_30_GenericCan */
/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCGeneral  CanTrcv_30_GenericCan General (PRE_COMPILE)
  \brief  General constant defines not associated with a group of defines.
  \{
*/ 
#define CANTRCV_30_GENERICCAN_CHECK_INIT_POINTER                      STD_OFF  /**< STD_ON if the init pointer shall not be used as NULL_PTR and a check shall validate this. */
#define CANTRCV_30_GENERICCAN_FINAL_MAGIC_NUMBER                      0x461Eu  /**< the precompile constant to validate the size of the initialization structure at initialization time of CanTrcv_30_GenericCan */
#define CANTRCV_30_GENERICCAN_INDIVIDUAL_POSTBUILD                    STD_OFF  /**< the precompile constant to check, that the module is individual postbuildable. The module 'CanTrcv_30_GenericCan' is not configured to be postbuild capable. */
#define CANTRCV_30_GENERICCAN_INIT_DATA                               CANTRCV_30_GENERICCAN_CONST  /**< CompilerMemClassDefine for the initialization data. */
#define CANTRCV_30_GENERICCAN_INIT_DATA_HASH_CODE                     200520931  /**< the precompile constant to validate the initialization structure at initialization time of CanTrcv_30_GenericCan with a hashcode. The seed value is '0x461Eu' */
#define CANTRCV_30_GENERICCAN_USE_ECUM_BSW_ERROR_HOOK                 STD_OFF  /**< STD_ON if the EcuM_BswErrorHook shall be called in the ConfigPtr check. */
#define CANTRCV_30_GENERICCAN_USE_INIT_POINTER                        STD_OFF  /**< STD_ON if the init pointer CanTrcv_30_GenericCan shall be used. */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanTrcv_30_GenericCanLTDataSwitches  CanTrcv_30_GenericCan Data Switches  (LINK)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CANTRCV_30_GENERICCAN_LTCONFIG                                STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_LTConfig' Reason: 'the struct is deactivated because all elements are deactivated in all variants.' */
/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL CONSTANT MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanTrcv_30_GenericCanPBDataSwitches  CanTrcv_30_GenericCan Data Switches  (POST_BUILD)
  \brief  These defines are used to deactivate data and their processing.
  \{
*/ 
#define CANTRCV_30_GENERICCAN_PBCONFIG                                STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PBConfig' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define CANTRCV_30_GENERICCAN_LTCONFIGIDXOFPBCONFIG                   STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PBConfig.LTConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
#define CANTRCV_30_GENERICCAN_PCCONFIGIDXOFPBCONFIG                   STD_OFF  /**< Deactivateable: 'CanTrcv_30_GenericCan_PBConfig.PCConfigIdx' Reason: 'the module configuration is VARIANT_PRE_COMPILE.' */
/** 
  \}
*/ 



/**********************************************************************************************************************
  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  CanTrcv_30_GenericCanPCGetConstantDuplicatedRootDataMacros  CanTrcv_30_GenericCan Get Constant Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated by constance root data elements.
  \{
*/ 
#define CanTrcv_30_GenericCan_GetChannelOfPCConfig()                  CanTrcv_30_GenericCan_Channel  /**< the pointer to CanTrcv_30_GenericCan_Channel */
#define CanTrcv_30_GenericCan_GetChannelUsedOfPCConfig()              CanTrcv_30_GenericCan_ChannelUsed  /**< the pointer to CanTrcv_30_GenericCan_ChannelUsed */
#define CanTrcv_30_GenericCan_GetDioConfigurationOfPCConfig()         CanTrcv_30_GenericCan_DioConfiguration  /**< the pointer to CanTrcv_30_GenericCan_DioConfiguration */
#define CanTrcv_30_GenericCan_GetGeneratorCompatibilityVersionOfPCConfig() 0x01050102u
#define CanTrcv_30_GenericCan_GetSizeOfChannelOfPCConfig()            3u  /**< the number of accomplishable value elements in CanTrcv_30_GenericCan_Channel */
#define CanTrcv_30_GenericCan_GetSizeOfChannelUsedOfPCConfig()        3u  /**< the number of accomplishable value elements in CanTrcv_30_GenericCan_ChannelUsed */
#define CanTrcv_30_GenericCan_GetSizeOfDioConfigurationOfPCConfig()   3u  /**< the number of accomplishable value elements in CanTrcv_30_GenericCan_DioConfiguration */
#define CanTrcv_30_GenericCan_GetSizeOfWakeupByBusUsedOfPCConfig()    3u  /**< the number of accomplishable value elements in CanTrcv_30_GenericCan_WakeupByBusUsed */
#define CanTrcv_30_GenericCan_GetSizeOfWakeupSourceOfPCConfig()       3u  /**< the number of accomplishable value elements in CanTrcv_30_GenericCan_WakeupSource */
#define CanTrcv_30_GenericCan_GetWakeupByBusUsedOfPCConfig()          CanTrcv_30_GenericCan_WakeupByBusUsed  /**< the pointer to CanTrcv_30_GenericCan_WakeupByBusUsed */
#define CanTrcv_30_GenericCan_GetWakeupSourceOfPCConfig()             CanTrcv_30_GenericCan_WakeupSource  /**< the pointer to CanTrcv_30_GenericCan_WakeupSource */
/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCGetDataMacros  CanTrcv_30_GenericCan Get Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read CONST and VAR data.
  \{
*/ 
#define CanTrcv_30_GenericCan_GetIcuChannelOfChannel(Index)           (CanTrcv_30_GenericCan_GetChannelOfPCConfig()[(Index)].IcuChannelOfChannel)
#define CanTrcv_30_GenericCan_IsIcuChannelSetOfChannel(Index)         ((CanTrcv_30_GenericCan_GetChannelOfPCConfig()[(Index)].IcuChannelSetOfChannel) != FALSE)
#define CanTrcv_30_GenericCan_IsChannelUsed(Index)                    ((CanTrcv_30_GenericCan_GetChannelUsedOfPCConfig()[(Index)]) != FALSE)
#define CanTrcv_30_GenericCan_GetCANSTBOfDioConfiguration(Index)      (CanTrcv_30_GenericCan_GetDioConfigurationOfPCConfig()[(Index)].CANSTBOfDioConfiguration)
#define CanTrcv_30_GenericCan_IsWakeupByBusUsed(Index)                ((CanTrcv_30_GenericCan_GetWakeupByBusUsedOfPCConfig()[(Index)]) != FALSE)
#define CanTrcv_30_GenericCan_GetWakeupSource(Index)                  ((EcuM_WakeupSourceType)CanTrcv_30_GenericCan_GetWakeupSourceOfPCConfig()[(Index)])
/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCGetDeduplicatedDataMacros  CanTrcv_30_GenericCan Get Deduplicated Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated data elements.
  \{
*/ 
#define CanTrcv_30_GenericCan_GetGeneratorCompatibilityVersion()      CanTrcv_30_GenericCan_GetGeneratorCompatibilityVersionOfPCConfig()
#define CanTrcv_30_GenericCan_GetSizeOfChannel()                      CanTrcv_30_GenericCan_GetSizeOfChannelOfPCConfig()
#define CanTrcv_30_GenericCan_GetSizeOfChannelUsed()                  CanTrcv_30_GenericCan_GetSizeOfChannelUsedOfPCConfig()
#define CanTrcv_30_GenericCan_GetSizeOfDioConfiguration()             CanTrcv_30_GenericCan_GetSizeOfDioConfigurationOfPCConfig()
#define CanTrcv_30_GenericCan_GetSizeOfWakeupByBusUsed()              CanTrcv_30_GenericCan_GetSizeOfWakeupByBusUsedOfPCConfig()
#define CanTrcv_30_GenericCan_GetSizeOfWakeupSource()                 CanTrcv_30_GenericCan_GetSizeOfWakeupSourceOfPCConfig()
/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCHasMacros  CanTrcv_30_GenericCan Has Macros (PRE_COMPILE)
  \brief  These macros can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
#define CanTrcv_30_GenericCan_HasChannel()                            (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasIcuChannelOfChannel()                (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasIcuChannelSetOfChannel()             (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasChannelUsed()                        (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasDioConfiguration()                   (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasCANSTBOfDioConfiguration()           (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasGeneratorCompatibilityVersion()      (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfChannel()                      (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfChannelUsed()                  (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfDioConfiguration()             (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfWakeupByBusUsed()              (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfWakeupSource()                 (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasWakeupByBusUsed()                    (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasWakeupSource()                       (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasPCConfig()                           (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasChannelOfPCConfig()                  (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasChannelUsedOfPCConfig()              (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasDioConfigurationOfPCConfig()         (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasGeneratorCompatibilityVersionOfPCConfig() (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfChannelOfPCConfig()            (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfChannelUsedOfPCConfig()        (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfDioConfigurationOfPCConfig()   (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfWakeupByBusUsedOfPCConfig()    (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasSizeOfWakeupSourceOfPCConfig()       (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasWakeupByBusUsedOfPCConfig()          (TRUE != FALSE)
#define CanTrcv_30_GenericCan_HasWakeupSourceOfPCConfig()             (TRUE != FALSE)
/** 
  \}
*/ 

  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/

/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  CanTrcv_30_GenericCanPCIterableTypes  CanTrcv_30_GenericCan Iterable Types (PRE_COMPILE)
  \brief  These type definitions are used to iterate over an array with least processor cycles for variable access as possible.
  \{
*/ 
/**   \brief  type used to iterate CanTrcv_30_GenericCan_Channel */
typedef uint8_least CanTrcv_30_GenericCan_ChannelIterType;

/**   \brief  type used to iterate CanTrcv_30_GenericCan_ChannelUsed */
typedef uint8_least CanTrcv_30_GenericCan_ChannelUsedIterType;

/**   \brief  type used to iterate CanTrcv_30_GenericCan_DioConfiguration */
typedef uint8_least CanTrcv_30_GenericCan_DioConfigurationIterType;

/**   \brief  type used to iterate CanTrcv_30_GenericCan_WakeupByBusUsed */
typedef uint8_least CanTrcv_30_GenericCan_WakeupByBusUsedIterType;

/**   \brief  type used to iterate CanTrcv_30_GenericCan_WakeupSource */
typedef uint8_least CanTrcv_30_GenericCan_WakeupSourceIterType;

/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCValueTypes  CanTrcv_30_GenericCan Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value based data representations.
  \{
*/ 
/**   \brief  value based type definition for CanTrcv_30_GenericCan_IcuChannelSetOfChannel */
typedef boolean CanTrcv_30_GenericCan_IcuChannelSetOfChannelType;

/**   \brief  value based type definition for CanTrcv_30_GenericCan_ChannelUsed */
typedef boolean CanTrcv_30_GenericCan_ChannelUsedType;

/**   \brief  value based type definition for CanTrcv_30_GenericCan_GeneratorCompatibilityVersion */
typedef uint32 CanTrcv_30_GenericCan_GeneratorCompatibilityVersionType;

/**   \brief  value based type definition for CanTrcv_30_GenericCan_SizeOfChannel */
typedef uint8 CanTrcv_30_GenericCan_SizeOfChannelType;

/**   \brief  value based type definition for CanTrcv_30_GenericCan_SizeOfChannelUsed */
typedef uint8 CanTrcv_30_GenericCan_SizeOfChannelUsedType;

/**   \brief  value based type definition for CanTrcv_30_GenericCan_SizeOfDioConfiguration */
typedef uint8 CanTrcv_30_GenericCan_SizeOfDioConfigurationType;

/**   \brief  value based type definition for CanTrcv_30_GenericCan_SizeOfWakeupByBusUsed */
typedef uint8 CanTrcv_30_GenericCan_SizeOfWakeupByBusUsedType;

/**   \brief  value based type definition for CanTrcv_30_GenericCan_SizeOfWakeupSource */
typedef uint8 CanTrcv_30_GenericCan_SizeOfWakeupSourceType;

/**   \brief  value based type definition for CanTrcv_30_GenericCan_WakeupByBusUsed */
typedef boolean CanTrcv_30_GenericCan_WakeupByBusUsedType;

/**   \brief  value based type definition for CanTrcv_30_GenericCan_WakeupSource */
typedef uint8 CanTrcv_30_GenericCan_WakeupSourceType;

/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/** 
  \defgroup  CanTrcv_30_GenericCanPCStructTypes  CanTrcv_30_GenericCan Struct Types (PRE_COMPILE)
  \brief  These type definitions are used for structured data representations.
  \{
*/ 
/**   \brief  type used in CanTrcv_30_GenericCan_Channel */
typedef struct sCanTrcv_30_GenericCan_ChannelType
{
  CanTrcv_30_GenericCan_IcuChannelSetOfChannelType IcuChannelSetOfChannel;
  Icu_ChannelType IcuChannelOfChannel;
} CanTrcv_30_GenericCan_ChannelType;

/**   \brief  type used in CanTrcv_30_GenericCan_DioConfiguration */
typedef struct sCanTrcv_30_GenericCan_DioConfigurationType
{
  Dio_ChannelType CANSTBOfDioConfiguration;
} CanTrcv_30_GenericCan_DioConfigurationType;

/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCRootPointerTypes  CanTrcv_30_GenericCan Root Pointer Types (PRE_COMPILE)
  \brief  These type definitions are used to point from the config root to symbol instances.
  \{
*/ 
/**   \brief  type used to point to CanTrcv_30_GenericCan_Channel */
typedef P2CONST(CanTrcv_30_GenericCan_ChannelType, TYPEDEF, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_ChannelPtrType;

/**   \brief  type used to point to CanTrcv_30_GenericCan_ChannelUsed */
typedef P2CONST(CanTrcv_30_GenericCan_ChannelUsedType, TYPEDEF, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_ChannelUsedPtrType;

/**   \brief  type used to point to CanTrcv_30_GenericCan_DioConfiguration */
typedef P2CONST(CanTrcv_30_GenericCan_DioConfigurationType, TYPEDEF, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_DioConfigurationPtrType;

/**   \brief  type used to point to CanTrcv_30_GenericCan_WakeupByBusUsed */
typedef P2CONST(CanTrcv_30_GenericCan_WakeupByBusUsedType, TYPEDEF, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_WakeupByBusUsedPtrType;

/**   \brief  type used to point to CanTrcv_30_GenericCan_WakeupSource */
typedef P2CONST(CanTrcv_30_GenericCan_WakeupSourceType, TYPEDEF, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_WakeupSourcePtrType;

/** 
  \}
*/ 

/** 
  \defgroup  CanTrcv_30_GenericCanPCRootValueTypes  CanTrcv_30_GenericCan Root Value Types (PRE_COMPILE)
  \brief  These type definitions are used for value representations in root arrays.
  \{
*/ 
/**   \brief  type used in CanTrcv_30_GenericCan_PCConfig */
typedef struct sCanTrcv_30_GenericCan_PCConfigType
{
  uint8 CanTrcv_30_GenericCan_PCConfigNeverUsed;  /**< dummy entry for the structure in the configuration variant precompile which is not used by the code. */
} CanTrcv_30_GenericCan_PCConfigType;

typedef CanTrcv_30_GenericCan_PCConfigType CanTrcv_30_GenericCan_ConfigType;  /**< A structure type is present for data in each configuration class. This typedef redefines the probably different name to the specified one. */

/** 
  \}
*/ 


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL SIMPLE DATA TYPES AND STRUCTURES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL COMPLEX DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CanTrcv_30_GenericCan_Channel
**********************************************************************************************************************/
/** 
  \var    CanTrcv_30_GenericCan_Channel
  \details
  Element          Description
  IcuChannelSet
  IcuChannel   
*/ 
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanTrcv_30_GenericCan_ChannelType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_Channel[3];
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTrcv_30_GenericCan_ChannelUsed
**********************************************************************************************************************/
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanTrcv_30_GenericCan_ChannelUsedType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_ChannelUsed[3];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTrcv_30_GenericCan_DioConfiguration
**********************************************************************************************************************/
/** 
  \var    CanTrcv_30_GenericCan_DioConfiguration
  \details
  Element    Description
  CANSTB 
*/ 
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanTrcv_30_GenericCan_DioConfigurationType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_DioConfiguration[3];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTrcv_30_GenericCan_WakeupByBusUsed
**********************************************************************************************************************/
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanTrcv_30_GenericCan_WakeupByBusUsedType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_WakeupByBusUsed[3];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  CanTrcv_30_GenericCan_WakeupSource
**********************************************************************************************************************/
#define CANTRCV_30_GENERICCAN_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(CanTrcv_30_GenericCan_WakeupSourceType, CANTRCV_30_GENERICCAN_CONST) CanTrcv_30_GenericCan_WakeupSource[3];  /* PRQA S 0777 */  /* MD_MSR_Rule5.1 */
#define CANTRCV_30_GENERICCAN_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: LINK
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/





#endif /* CANTRCV_30_GENERICCAN_CFG_H */

