/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Issm
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Issm_Cfg.c
 *   Generation Time: 2020-11-11 14:25:32
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#define ISSM_CFG_SOURCE

/**********************************************************************************************************************
 * INCLUDES
 *********************************************************************************************************************/
#include "Issm_Cbk.h"

/**********************************************************************************************************************
  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  Issm_ChannelConfig
**********************************************************************************************************************/
/** 
  \var    Issm_ChannelConfig
  \details
  Element               Description
  RxSignalIdEndIdx      the end index of the 0:n relation pointing to Issm_RxSignalId
  RxSignalIdStartIdx    the start index of the 0:n relation pointing to Issm_RxSignalId
  TxSignalId        
*/ 
#define ISSM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Issm_ChannelConfigType, ISSM_CONST) Issm_ChannelConfig[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RxSignalIdEndIdx                                                                            RxSignalIdStartIdx                                                                            TxSignalId                                                                                                  Comment */
  { /*     0 */                                      8u  /* /ActiveEcuC/Issm/IssmGeneral/Backbone2 */     ,                                        0u  /* /ActiveEcuC/Issm/IssmGeneral/Backbone2 */     ,                ComConf_ComSignal_AnmSig_CIOM_Backbone2_oAnmMsg_CIOM_Backbone2_oBackbone2_8a1cbf56_Tx },  /* [Backbone2] */
  { /*     1 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/CAN6 */          , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/CAN6 */          ,                        ComConf_ComSignal_AnmSig_SCIM_CAN6_ISig_206_oSCIM_BB2toCAN6_oCAN6_209e8e76_Tx },  /* [CAN6] */
  { /*     2 */                                     13u  /* /ActiveEcuC/Issm/IssmGeneral/CabSubnet */     ,                                        8u  /* /ActiveEcuC/Issm/IssmGeneral/CabSubnet */     ,                ComConf_ComSignal_AnmSig_CIOM_CabSubnet_oAnmMsg_CIOM_CabSubnet_oCabSubnet_5a34740d_Tx },  /* [CabSubnet] */
  { /*     3 */                                     17u  /* /ActiveEcuC/Issm/IssmGeneral/SecuritySubnet */,                                       13u  /* /ActiveEcuC/Issm/IssmGeneral/SecuritySubnet */, ComConf_ComSignal_AnmSig_CIOM_SecuritySubnet_oAnmMsg_CIOM_SecuritySubnet_oSecuritySubnet_c3c93d33_Tx },  /* [SecuritySubnet] */
  { /*     4 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/Backbone1J1939 */, ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/Backbone1J1939 */,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG },  /* [Backbone1J1939] */
  { /*     5 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/FMSNet */        , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/FMSNet */        ,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG },  /* [FMSNet] */
  { /*     6 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN1_CIOM */     , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN1_CIOM */     ,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG },  /* [LIN1_CIOM] */
  { /*     7 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN2_CIOM */     , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN2_CIOM */     ,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG },  /* [LIN2_CIOM] */
  { /*     8 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN3_CIOM */     , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN3_CIOM */     ,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG },  /* [LIN3_CIOM] */
  { /*     9 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN4_CIOM */     , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN4_CIOM */     ,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG },  /* [LIN4_CIOM] */
  { /*    10 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN5_CIOM */     , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN5_CIOM */     ,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG },  /* [LIN5_CIOM] */
  { /*    11 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN6_CIOM */     , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN6_CIOM */     ,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG },  /* [LIN6_CIOM] */
  { /*    12 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN7_CIOM */     , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN7_CIOM */     ,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG },  /* [LIN7_CIOM] */
  { /*    13 */ ISSM_NO_RXSIGNALIDENDIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN8_CIOM */     , ISSM_NO_RXSIGNALIDSTARTIDXOFCHANNELCONFIG  /* /ActiveEcuC/Issm/IssmGeneral/LIN8_CIOM */     ,                                                                    ISSM_NO_TXSIGNALIDOFCHANNELCONFIG }   /* [LIN8_CIOM] */
};
#define ISSM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_ChannelPostBuildConfig
**********************************************************************************************************************/
/** 
  \var    Issm_ChannelPostBuildConfig
  \details
  Element          Description
  MappedIssMask
*/ 
#define ISSM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Issm_ChannelPostBuildConfigType, ISSM_CONST) Issm_ChannelPostBuildConfig[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MappedIssMask        Comment */
  { /*     0 */   0x80000002u },  /* [Backbone2] */
  { /*     1 */   0x80000000u },  /* [CAN6] */
  { /*     2 */   0x80000008u },  /* [CabSubnet] */
  { /*     3 */   0x80000020u },  /* [SecuritySubnet] */
  { /*     4 */   0x80000080u },  /* [Backbone1J1939] */
  { /*     5 */   0x80000000u },  /* [FMSNet] */
  { /*     6 */   0x80000100u },  /* [LIN1_CIOM] */
  { /*     7 */   0x80000200u },  /* [LIN2_CIOM] */
  { /*     8 */   0x80000400u },  /* [LIN3_CIOM] */
  { /*     9 */   0x80000800u },  /* [LIN4_CIOM] */
  { /*    10 */   0x80001000u },  /* [LIN5_CIOM] */
  { /*    11 */   0x80000000u },  /* [LIN6_CIOM] */
  { /*    12 */   0x80000000u },  /* [LIN7_CIOM] */
  { /*    13 */   0x80000000u }   /* [LIN8_CIOM] */
};
#define ISSM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_RxSignalId
**********************************************************************************************************************/
#define ISSM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Issm_RxSignalIdType, ISSM_CONST) Issm_RxSignalId[17] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     RxSignalId                                                                                                          Referable Keys */
  /*     0 */                             ComConf_ComSignal_AnmSig_BBM_Backbone2_oAnmMsg_BBM_Backbone2_oBackbone2_396d1d95_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     1 */                           ComConf_ComSignal_AnmSig_DACU_Backbone2_oAnmMsg_DACU_Backbone2_oBackbone2_82fc6af4_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     2 */                 ComConf_ComSignal_AnmSig_ECUspare1_Backbone2_oAnmMsg_ECUspare1_Backbone2_oBackbone2_9471a62d_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     3 */                 ComConf_ComSignal_AnmSig_ECUspare2_Backbone2_oAnmMsg_ECUspare2_Backbone2_oBackbone2_fa2b273f_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     4 */                             ComConf_ComSignal_AnmSig_EMS_Backbone2_oAnmMsg_EMS_Backbone2_oBackbone2_c10b5f2c_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     5 */                       ComConf_ComSignal_AnmSig_HMIIOM_Backbone2_oAnmMsg_HMIIOM_Backbone2_oBackbone2_9eb0ae63_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     6 */                           ComConf_ComSignal_AnmSig_TECU_Backbone2_oAnmMsg_TECU_Backbone2_oBackbone2_a0843952_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     7 */                           ComConf_ComSignal_AnmSig_VMCU_Backbone2_oAnmMsg_VMCU_Backbone2_oBackbone2_64a6ada3_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     8 */                             ComConf_ComSignal_AnmSig_CCM_CabSubnet_oAnmMsg_CCM_CabSubnet_oCabSubnet_eb4ad192_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*     9 */                 ComConf_ComSignal_AnmSig_ECUspare6_CabSubnet_oAnmMsg_ECUspare6_CabSubnet_oCabSubnet_b891e97c_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*    10 */                         ComConf_ComSignal_AnmSig_LECM1_CabSubnet_oAnmMsg_LECM1_CabSubnet_oCabSubnet_e04b322d_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*    11 */                             ComConf_ComSignal_AnmSig_SRS_CabSubnet_oAnmMsg_SRS_CabSubnet_oCabSubnet_deb2073e_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*    12 */                           ComConf_ComSignal_AnmSig_WRCS_CabSubnet_oAnmMsg_WRCS_CabSubnet_oCabSubnet_a4bc01b1_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*    13 */          ComConf_ComSignal_AnmSig_Alarm_SecuritySubnet_oAnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_b4bb691c_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */
  /*    14 */              ComConf_ComSignal_AnmSig_DDM_SecuritySubnet_oAnmMsg_DDM_SecuritySubnet_oSecuritySubnet_b331a2fd_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */
  /*    15 */  ComConf_ComSignal_AnmSig_ECUspare5_SecuritySubnet_oAnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_c77f9883_Rx,  /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */
  /*    16 */              ComConf_ComSignal_AnmSig_PDM_SecuritySubnet_oAnmMsg_PDM_SecuritySubnet_oSecuritySubnet_99da07cf_Rx   /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */
};
#define ISSM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_UserPbConfig
**********************************************************************************************************************/
/** 
  \var    Issm_UserPbConfig
  \details
  Element          Description
  MappedIssMask
*/ 
#define ISSM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(Issm_UserPbConfigType, ISSM_CONST) Issm_UserPbConfig[51] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MappedIssMask */
  { /*     0 */   0x00001F83u },
  { /*     1 */   0x00001FA3u },
  { /*     2 */   0x00001FA3u },
  { /*     3 */   0x00000021u },
  { /*     4 */   0x00000083u },
  { /*     5 */   0x0000018Bu },
  { /*     6 */   0x0000018Bu },
  { /*     7 */   0x00000087u },
  { /*     8 */   0x00001F83u },
  { /*     9 */   0x80000001u },
  { /*    10 */   0x00000F83u },
  { /*    11 */   0x00000087u },
  { /*    12 */   0x0000018Bu },
  { /*    13 */   0x00001FAAu },
  { /*    14 */   0x00000883u },
  { /*    15 */   0x00000883u },
  { /*    16 */   0x00000883u },
  { /*    17 */   0x00000083u },
  { /*    18 */   0x00000083u },
  { /*    19 */   0x000010CFu },
  { /*    20 */   0x00001087u },
  { /*    21 */   0x00000883u },
  { /*    22 */   0x00000883u },
  { /*    23 */   0x00001F8Bu },
  { /*    24 */   0x00001F83u },
  { /*    25 */   0x00001F01u },
  { /*    26 */   0x000000C3u },
  { /*    27 */   0x00001F8Bu },
  { /*    28 */   0x00000083u },
  { /*    29 */   0x00001FABu },
  { /*    30 */   0x00005F29u },
  { /*    31 */   0x00000601u },
  { /*    32 */   0x00000001u },
  { /*    33 */   0x00001F8Bu },
  { /*    34 */   0x00001F8Bu },
  { /*    35 */   0x00001F8Bu },
  { /*    36 */   0x00001F8Bu },
  { /*    37 */   0x00011F8Fu },
  { /*    38 */   0x00011F8Fu },
  { /*    39 */   0x00011F8Fu },
  { /*    40 */   0x00000087u },
  { /*    41 */   0x000000C7u },
  { /*    42 */   0x0000C129u },
  { /*    43 */   0x0000C129u },
  { /*    44 */   0x00001F83u },
  { /*    45 */   0x00000183u },
  { /*    46 */   0x00000001u },
  { /*    47 */   0x00000083u },
  { /*    48 */   0x00000883u },
  { /*    49 */   0x00001F83u },
    /* Index    MappedIssMask */
  { /*    50 */   0x00000601u }
};
#define ISSM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_ActiveIssField
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_ActiveIssFieldType, ISSM_VAR_NOINIT) Issm_ActiveIssField;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_EcuRunState
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_EcuRunStateType, ISSM_VAR_NOINIT) Issm_EcuRunState;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_ExternalRequestField
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_ExternalRequestFieldUType, ISSM_VAR_NOINIT) Issm_ExternalRequestField;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*   ... */  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     7 */  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     8 */  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*   ... */  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*    12 */  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*    13 */  /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */
  /*   ... */  /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */
  /*    16 */  /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */

#define ISSM_STOP_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_ExternalRequestsChanged
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_ExternalRequestsChangedType, ISSM_VAR_NOINIT) Issm_ExternalRequestsChanged;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_InternalRequestField
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_InternalRequestFieldType, ISSM_VAR_NOINIT) Issm_InternalRequestField;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_InternalRequestsChanged
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_InternalRequestsChangedType, ISSM_VAR_NOINIT) Issm_InternalRequestsChanged;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_MinActiveTimer
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_MinActiveTimerType, ISSM_VAR_NOINIT) Issm_MinActiveTimer[32];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_NetworkOutputField
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_NetworkOutputFieldType, ISSM_VAR_NOINIT) Issm_NetworkOutputField[14];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_NetworkRequested
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_NetworkRequestedType, ISSM_VAR_NOINIT) Issm_NetworkRequested[14];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_OverallRequestField
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_OverallRequestFieldType, ISSM_VAR_NOINIT) Issm_OverallRequestField;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_RxSignalTimer
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_RxSignalTimerUType, ISSM_VAR_NOINIT) Issm_RxSignalTimer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*   ... */  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     7 */  /* [/ActiveEcuC/Issm/IssmGeneral/Backbone2] */
  /*     8 */  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*   ... */  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*    12 */  /* [/ActiveEcuC/Issm/IssmGeneral/CabSubnet] */
  /*    13 */  /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */
  /*   ... */  /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */
  /*    16 */  /* [/ActiveEcuC/Issm/IssmGeneral/SecuritySubnet] */

#define ISSM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  Issm_UserRequested
**********************************************************************************************************************/
#define ISSM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Issm_UserRequestedType, ISSM_VAR_NOINIT) Issm_UserRequested[51];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define ISSM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */



/**********************************************************************************************************************
  GLOBAL FUNCTIONS
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTIONS
**********************************************************************************************************************/


#define ISSM_START_SEC_CODE
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* Signal Rx Indication functions */
FUNC(void, ISSM_CODE) Issm_Com_AnmSig_BBM_Backbone2_oAnmMsg_BBM_Backbone2_oBackbone2_396d1d95_Rx_intId0_Notification( void )
{
  Issm_RxSignalNotification( 0u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_DACU_Backbone2_oAnmMsg_DACU_Backbone2_oBackbone2_82fc6af4_Rx_intId1_Notification( void )
{
  Issm_RxSignalNotification( 1u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_ECUspare1_Backbone2_oAnmMsg_ECUspare1_Backbone2_oBackbone2_9471a62d_Rx_intId2_Notification( void )
{
  Issm_RxSignalNotification( 2u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_ECUspare2_Backbone2_oAnmMsg_ECUspare2_Backbone2_oBackbone2_fa2b273f_Rx_intId3_Notification( void )
{
  Issm_RxSignalNotification( 3u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_EMS_Backbone2_oAnmMsg_EMS_Backbone2_oBackbone2_c10b5f2c_Rx_intId4_Notification( void )
{
  Issm_RxSignalNotification( 4u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_HMIIOM_Backbone2_oAnmMsg_HMIIOM_Backbone2_oBackbone2_9eb0ae63_Rx_intId5_Notification( void )
{
  Issm_RxSignalNotification( 5u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_TECU_Backbone2_oAnmMsg_TECU_Backbone2_oBackbone2_a0843952_Rx_intId6_Notification( void )
{
  Issm_RxSignalNotification( 6u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_VMCU_Backbone2_oAnmMsg_VMCU_Backbone2_oBackbone2_64a6ada3_Rx_intId7_Notification( void )
{
  Issm_RxSignalNotification( 7u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_CCM_CabSubnet_oAnmMsg_CCM_CabSubnet_oCabSubnet_eb4ad192_Rx_intId8_Notification( void )
{
  Issm_RxSignalNotification( 8u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_ECUspare6_CabSubnet_oAnmMsg_ECUspare6_CabSubnet_oCabSubnet_b891e97c_Rx_intId9_Notification( void )
{
  Issm_RxSignalNotification( 9u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_LECM1_CabSubnet_oAnmMsg_LECM1_CabSubnet_oCabSubnet_e04b322d_Rx_intId10_Notification( void )
{
  Issm_RxSignalNotification( 10u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_SRS_CabSubnet_oAnmMsg_SRS_CabSubnet_oCabSubnet_deb2073e_Rx_intId11_Notification( void )
{
  Issm_RxSignalNotification( 11u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_WRCS_CabSubnet_oAnmMsg_WRCS_CabSubnet_oCabSubnet_a4bc01b1_Rx_intId12_Notification( void )
{
  Issm_RxSignalNotification( 12u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_Alarm_SecuritySubnet_oAnmMsg_Alarm_SecuritySubnet_oSecuritySubnet_b4bb691c_Rx_intId13_Notification( void )
{
  Issm_RxSignalNotification( 13u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_DDM_SecuritySubnet_oAnmMsg_DDM_SecuritySubnet_oSecuritySubnet_b331a2fd_Rx_intId14_Notification( void )
{
  Issm_RxSignalNotification( 14u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_ECUspare5_SecuritySubnet_oAnmMsg_ECUspare5_SecuritySubnet_oSecuritySubnet_c77f9883_Rx_intId15_Notification( void )
{
  Issm_RxSignalNotification( 15u );
}

FUNC(void, ISSM_CODE) Issm_Com_AnmSig_PDM_SecuritySubnet_oAnmMsg_PDM_SecuritySubnet_oSecuritySubnet_99da07cf_Rx_intId16_Notification( void )
{
  Issm_RxSignalNotification( 16u );
}



#define ISSM_STOP_SEC_CODE
#include "MemMap.h"  /* PRQA S 5087 */ /* MD_MSR_MemMap */


/**********************************************************************************************************************
  END OF FILE: Issm_Cfg.c
**********************************************************************************************************************/

