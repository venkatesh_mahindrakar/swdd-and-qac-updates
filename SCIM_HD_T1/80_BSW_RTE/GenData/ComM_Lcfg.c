/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: ComM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: ComM_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:32
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


#define CCL_ASR_COMM_LCFG_MODULE

/**********************************************************************************************************************
   LOCAL MISRA / PCLINT JUSTIFICATION
**********************************************************************************************************************/
/* PRQA S 0779 EOF */ /* MD_MSR_Rule5.2_0779 */
/* PRQA S 0777 EOF */ /* MD_MSR_Rule5.1_0777 */
/* PRQA S 3453 EOF */ /* MD_MSR_FctLikeMacro */

/* -----------------------------------------------------------------------------
    &&&~ INCLUDES
 ----------------------------------------------------------------------------- */

#include "ComM_Private_Cfg.h"
#include "CanSM_ComM.h"
#include "CanSM.h"
#include "LinSM.h"


#include "Rte_ComM.h"

#if defined( COMM_LOCAL )
#else
# define COMM_LOCAL static
#endif

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA TYPES AND STRUCTURES
**********************************************************************************************************************/


/**********************************************************************************************************************
 *  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  ComM_Channel
**********************************************************************************************************************/
/** 
  \var    ComM_Channel
  \brief  Contains PreCompile configuration parameters of channels
  \details
  Element            Description
  MinFullComTime     Minimal full communication time for the channel, relevant for NmTypes LIGHT and FULL
  NmLightDuration    Nm Light Timeout
  BusType            The channel bus type
  J1939Support       Decides if the channel supports J1939
  NmSupport          Decides if the channel has NmType FULL or PASSIVE
  SilentSupport      Decides if the channel supports Silent mode (TRUE if ETH or CAN without J1939NM and Nm or NmLightSilentDuration)
  NmType             The Network Management type fo the channel
  WakeupState        Target channel state after a Passive Wake-up
*/ 
#define COMM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(ComM_ChannelType, COMM_CONST) ComM_Channel[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MinFullComTime  NmLightDuration  BusType            J1939Support  NmSupport  SilentSupport  NmType                      WakeupState                            Referable Keys */
  { /*     0 */             0u,              0u, COMM_BUS_TYPE_CAN,        FALSE,      TRUE,          TRUE,  COMM_FULL_NMTYPEOFCHANNEL, COMM_FULL_COM_READY_SLEEP       },  /* [ComMChannel_0] */
  { /*     1 */          1000u,           1000u, COMM_BUS_TYPE_CAN,        FALSE,     FALSE,         FALSE, COMM_LIGHT_NMTYPEOFCHANNEL, COMM_FULL_COM_NETWORK_REQUESTED },  /* [ComMChannel_1] */
  { /*     2 */             0u,              0u, COMM_BUS_TYPE_CAN,        FALSE,      TRUE,          TRUE,  COMM_FULL_NMTYPEOFCHANNEL, COMM_FULL_COM_READY_SLEEP       },  /* [ComMChannel_2] */
  { /*     3 */             0u,              0u, COMM_BUS_TYPE_CAN,        FALSE,      TRUE,          TRUE,  COMM_FULL_NMTYPEOFCHANNEL, COMM_FULL_COM_READY_SLEEP       },  /* [ComMChannel_3] */
  { /*     4 */             0u,              0u, COMM_BUS_TYPE_CAN,         TRUE,      TRUE,         FALSE,  COMM_FULL_NMTYPEOFCHANNEL, COMM_FULL_COM_READY_SLEEP       },  /* [ComMChannel_4] */
  { /*     5 */             0u,              0u, COMM_BUS_TYPE_CAN,         TRUE,      TRUE,         FALSE,  COMM_FULL_NMTYPEOFCHANNEL, COMM_FULL_COM_READY_SLEEP       },  /* [ComMChannel_5] */
  { /*     6 */          1000u,            400u, COMM_BUS_TYPE_LIN,        FALSE,     FALSE,         FALSE, COMM_LIGHT_NMTYPEOFCHANNEL, COMM_FULL_COM_NETWORK_REQUESTED },  /* [ComMChannel_6] */
  { /*     7 */          1000u,            400u, COMM_BUS_TYPE_LIN,        FALSE,     FALSE,         FALSE, COMM_LIGHT_NMTYPEOFCHANNEL, COMM_FULL_COM_NETWORK_REQUESTED },  /* [ComMChannel_7] */
  { /*     8 */          1000u,            400u, COMM_BUS_TYPE_LIN,        FALSE,     FALSE,         FALSE, COMM_LIGHT_NMTYPEOFCHANNEL, COMM_FULL_COM_NETWORK_REQUESTED },  /* [ComMChannel_8] */
  { /*     9 */          1000u,            400u, COMM_BUS_TYPE_LIN,        FALSE,     FALSE,         FALSE, COMM_LIGHT_NMTYPEOFCHANNEL, COMM_FULL_COM_NETWORK_REQUESTED },  /* [ComMChannel_9] */
  { /*    10 */          1000u,            400u, COMM_BUS_TYPE_LIN,        FALSE,     FALSE,         FALSE, COMM_LIGHT_NMTYPEOFCHANNEL, COMM_FULL_COM_NETWORK_REQUESTED },  /* [ComMChannel_10] */
  { /*    11 */          1000u,            400u, COMM_BUS_TYPE_LIN,        FALSE,     FALSE,         FALSE, COMM_LIGHT_NMTYPEOFCHANNEL, COMM_FULL_COM_NETWORK_REQUESTED },  /* [ComMChannel_11] */
  { /*    12 */          1000u,            400u, COMM_BUS_TYPE_LIN,        FALSE,     FALSE,         FALSE, COMM_LIGHT_NMTYPEOFCHANNEL, COMM_FULL_COM_NETWORK_REQUESTED },  /* [ComMChannel_12] */
  { /*    13 */          1000u,            400u, COMM_BUS_TYPE_LIN,        FALSE,     FALSE,         FALSE, COMM_LIGHT_NMTYPEOFCHANNEL, COMM_FULL_COM_NETWORK_REQUESTED }   /* [ComMChannel_13] */
};
#define COMM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  ComM_ChannelPb
**********************************************************************************************************************/
/** 
  \var    ComM_ChannelPb
  \brief  Contains PostBuild configuration parameters of channels
  \details
  Element                   Description
  UserReqFullComEndIdx      the end index of the 0:n relation pointing to ComM_UserReqFullCom
  UserReqFullComStartIdx    the start index of the 0:n relation pointing to ComM_UserReqFullCom
*/ 
#define COMM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(ComM_ChannelPbType, COMM_CONST) ComM_ChannelPb[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    UserReqFullComEndIdx  UserReqFullComStartIdx */
  { /*     0 */                   1u,                     0u },
  { /*     1 */                   2u,                     1u },
  { /*     2 */                   3u,                     2u },
  { /*     3 */                   4u,                     3u },
  { /*     4 */                   5u,                     4u },
  { /*     5 */                   6u,                     5u },
  { /*     6 */                   7u,                     6u },
  { /*     7 */                   8u,                     7u },
  { /*     8 */                   9u,                     8u },
  { /*     9 */                  10u,                     9u },
  { /*    10 */                  11u,                    10u },
  { /*    11 */                  12u,                    11u },
  { /*    12 */                  13u,                    12u },
  { /*    13 */                  14u,                    13u }
};
#define COMM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  ComM_User
**********************************************************************************************************************/
/** 
  \var    ComM_User
  \brief  Information about ComM users
  \details
  Element                 Description
  PncUser                 decides if a user is a partial network user or a direct channel user
  UserByteMaskEndIdx      the end index of the 0:n relation pointing to ComM_UserByteMask
  UserByteMaskStartIdx    the start index of the 0:n relation pointing to ComM_UserByteMask
*/ 
#define COMM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(ComM_UserType, COMM_CONST) ComM_User[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    PncUser  UserByteMaskEndIdx  UserByteMaskStartIdx        Comment */
  { /*     0 */   FALSE,                 1u,                   0u },  /* [CN_Backbone2_3f947ba3] */
  { /*     1 */   FALSE,                 2u,                   1u },  /* [CN_CAN6_64ab9b37] */
  { /*     2 */   FALSE,                 3u,                   2u },  /* [CN_CabSubnet_aa77b0b0] */
  { /*     3 */   FALSE,                 4u,                   3u },  /* [CN_SecuritySubnet_7cec9a61] */
  { /*     4 */   FALSE,                 5u,                   4u },  /* [CN_Backbone1J1939_2a22de3d] */
  { /*     5 */   FALSE,                 6u,                   5u },  /* [CN_FMSNet_05490e3d] */
  { /*     6 */   FALSE,                 7u,                   6u },  /* [CN_LIN00_ace1a6ba] */
  { /*     7 */   FALSE,                 8u,                   7u },  /* [CN_LIN01_4323cd84] */
  { /*     8 */   FALSE,                 9u,                   8u },  /* [CN_LIN02_a8147687] */
  { /*     9 */   FALSE,                10u,                   9u },  /* [CN_LIN03_47d61db9] */
  { /*    10 */   FALSE,                11u,                  10u },  /* [CN_LIN04_a50a06c0] */
  { /*    11 */   FALSE,                12u,                  11u },  /* [CN_LIN05_4ac86dfe] */
  { /*    12 */   FALSE,                13u,                  12u },  /* [CN_LIN06_a1ffd6fd] */
  { /*    13 */   FALSE,                14u,                  13u }   /* [CN_LIN07_4e3dbdc3] */
};
#define COMM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  ComM_UserByteMask
**********************************************************************************************************************/
/** 
  \var    ComM_UserByteMask
  \brief  Each user has N entries in this array (N = # channels attached to this user, directly or through PNC). Each entry describes a Byte Position and a Mask for storing/clearing the user request in UserReqFullCom
  \details
  Element              Description
  Channel              ID of the channel which is requested by this entry.
  UserReqFullComIdx    the index of the 1:1 relation pointing to ComM_UserReqFullCom
*/ 
#define COMM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(ComM_UserByteMaskType, COMM_CONST) ComM_UserByteMask[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    Channel  UserReqFullComIdx        Referable Keys */
  { /*     0 */      0u,                0u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone2_3f947ba3] */
  { /*     1 */      1u,                1u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CAN6_64ab9b37] */
  { /*     2 */      2u,                2u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_CabSubnet_aa77b0b0] */
  { /*     3 */      3u,                3u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_SecuritySubnet_7cec9a61] */
  { /*     4 */      4u,                4u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_Backbone1J1939_2a22de3d] */
  { /*     5 */      5u,                5u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_FMSNet_05490e3d] */
  { /*     6 */      6u,                6u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN00_ace1a6ba] */
  { /*     7 */      7u,                7u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN01_4323cd84] */
  { /*     8 */      8u,                8u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN02_a8147687] */
  { /*     9 */      9u,                9u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN03_47d61db9] */
  { /*    10 */     10u,               10u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN04_a50a06c0] */
  { /*    11 */     11u,               11u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN05_4ac86dfe] */
  { /*    12 */     12u,               12u },  /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN06_a1ffd6fd] */
  { /*    13 */     13u,               13u }   /* [/ActiveEcuC/ComM/ComMConfigSet/CN_LIN07_4e3dbdc3] */
};
#define COMM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  ComM_DcmRequestActive
**********************************************************************************************************************/
/** 
  \var    ComM_DcmRequestActive
  \brief  Status of Dcm active diagnostic request, TRUE if requested, FALSE otherwise
*/ 
#define COMM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(ComM_DcmRequestActiveUType, COMM_VAR_NOINIT) ComM_DcmRequestActive;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [ComMChannel_0] */
  /*     1 */  /* [ComMChannel_1] */
  /*     2 */  /* [ComMChannel_2] */
  /*     3 */  /* [ComMChannel_3] */
  /*     4 */  /* [ComMChannel_4] */
  /*     5 */  /* [ComMChannel_5] */
  /*     6 */  /* [ComMChannel_6] */
  /*     7 */  /* [ComMChannel_7] */
  /*     8 */  /* [ComMChannel_8] */
  /*     9 */  /* [ComMChannel_9] */
  /*    10 */  /* [ComMChannel_10] */
  /*    11 */  /* [ComMChannel_11] */
  /*    12 */  /* [ComMChannel_12] */
  /*    13 */  /* [ComMChannel_13] */

#define COMM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  ComM_MinFullComModeTimer
**********************************************************************************************************************/
/** 
  \var    ComM_MinFullComModeTimer
  \brief  The current value of Min Full Com Mode timer
*/ 
#define COMM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(ComM_MinFullComModeTimerUType, COMM_VAR_NOINIT) ComM_MinFullComModeTimer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [ComMChannel_0] */
  /*     1 */  /* [ComMChannel_1] */
  /*     2 */  /* [ComMChannel_2] */
  /*     3 */  /* [ComMChannel_3] */
  /*     4 */  /* [ComMChannel_4] */
  /*     5 */  /* [ComMChannel_5] */
  /*     6 */  /* [ComMChannel_6] */
  /*     7 */  /* [ComMChannel_7] */
  /*     8 */  /* [ComMChannel_8] */
  /*     9 */  /* [ComMChannel_9] */
  /*    10 */  /* [ComMChannel_10] */
  /*    11 */  /* [ComMChannel_11] */
  /*    12 */  /* [ComMChannel_12] */
  /*    13 */  /* [ComMChannel_13] */

#define COMM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  ComM_NmLightTimer
**********************************************************************************************************************/
/** 
  \var    ComM_NmLightTimer
  \brief  The current value of Nm Light or Nm Light Silent timer
*/ 
#define COMM_START_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(ComM_NmLightTimerUType, COMM_VAR_NOINIT) ComM_NmLightTimer;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [ComMChannel_0] */
  /*     1 */  /* [ComMChannel_1] */
  /*     2 */  /* [ComMChannel_2] */
  /*     3 */  /* [ComMChannel_3] */
  /*     4 */  /* [ComMChannel_4] */
  /*     5 */  /* [ComMChannel_5] */
  /*     6 */  /* [ComMChannel_6] */
  /*     7 */  /* [ComMChannel_7] */
  /*     8 */  /* [ComMChannel_8] */
  /*     9 */  /* [ComMChannel_9] */
  /*    10 */  /* [ComMChannel_10] */
  /*    11 */  /* [ComMChannel_11] */
  /*    12 */  /* [ComMChannel_12] */
  /*    13 */  /* [ComMChannel_13] */

#define COMM_STOP_SEC_VAR_NOINIT_16BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  ComM_UserReqFullCom
**********************************************************************************************************************/
/** 
  \var    ComM_UserReqFullCom
  \brief  RAM array used to store user requests for channels as bitmasks. Each channel 'owns' 1..n bytes in this array, depending on the number of users assigned to it.
*/ 
#define COMM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(ComM_UserReqFullComType, COMM_VAR_NOINIT) ComM_UserReqFullCom[14];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [ComMChannel_0, Channel0_To_User0] */
  /*     1 */  /* [ComMChannel_1, Channel1_To_User1] */
  /*     2 */  /* [ComMChannel_2, Channel2_To_User2] */
  /*     3 */  /* [ComMChannel_3, Channel3_To_User3] */
  /*     4 */  /* [ComMChannel_4, Channel4_To_User4] */
  /*     5 */  /* [ComMChannel_5, Channel5_To_User5] */
  /*     6 */  /* [ComMChannel_6, Channel6_To_User6] */
  /*     7 */  /* [ComMChannel_7, Channel7_To_User7] */
  /*     8 */  /* [ComMChannel_8, Channel8_To_User8] */
  /*     9 */  /* [ComMChannel_9, Channel9_To_User9] */
  /*    10 */  /* [ComMChannel_10, Channel10_To_User10] */
  /*    11 */  /* [ComMChannel_11, Channel11_To_User11] */
  /*    12 */  /* [ComMChannel_12, Channel12_To_User12] */
  /*    13 */  /* [ComMChannel_13, Channel13_To_User13] */

#define COMM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */




/* -----------------------------------------------------------------------------
    &&&~ GLOBAL DATA ROM
 ----------------------------------------------------------------------------- */

#define COMM_START_SEC_CONST_UNSPECIFIED
 /* PRQA S 5087 1 */ /* MD_MSR_MemMap */ 
#include "MemMap.h"

CONST(ComM_InhibitionStatusType, COMM_CONST) ComM_ECUGroupClassInit = 0x0; /* PRQA S 1533 */ /* MD_ComM_1533 */


#define COMM_STOP_SEC_CONST_UNSPECIFIED
 /* PRQA S 5087 1 */ /* MD_MSR_MemMap */ 
#include "MemMap.h"


/* -----------------------------------------------------------------------------
    &&&~ INTERNAL DATA RAM
 ----------------------------------------------------------------------------- */
/* -----------------------------------------------------------------------------
    &&&~ GLOBAL DATA RAM
 ----------------------------------------------------------------------------- */

#define COMM_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "MemMap.h"

/* -----------------------------------------------------------------------------
    &&&~ GLOBAL FUNCTION PROTOTYPES
 ----------------------------------------------------------------------------- */

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_0
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_0(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)0 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_1
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_1(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)1 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_2
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_2(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)2 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_3
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_3(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)3 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_4
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_4(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)4 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_5
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_5(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)5 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_6
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_6(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)6 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_7
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_7(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)7 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_8
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_8(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)8 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_9
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_9(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)9 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_10
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_10(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)10 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_11
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_11(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)11 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_12
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_12(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)12 );
}

/*********************************************************************************************************************
FUNCTION: ComM_MainFunction_13
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Call the ComM_MainFunction() for the corresponding channel.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_MainFunction_13(void)
{
  /* ----- Development Error Checks ------------------------------------- */
  /* Not needed, ComM_MainFunction() performs an initialization check. */

  /* ----- Implementation ----------------------------------------------- */
  /* #10 Call the ComM_MainFunction() for the corresponding channel. */
  ComM_MainFunction((NetworkHandleType)13 );
}



/*********************************************************************************************************************
  FUNCTION: ComM_RequestBusSMMode
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Request the communication mode from the corresponding BusSM.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_RequestBusSMMode(NetworkHandleType Channel, ComM_ModeType ComMode)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Request the communication mode from the corresponding BusSM. */
  switch(Channel)
  {
    case 0:
      (void)CanSM_RequestComMode(Channel, ComMode);
      break;
    case 1:
      (void)CanSM_RequestComMode(Channel, ComMode);
      break;
    case 2:
      (void)CanSM_RequestComMode(Channel, ComMode);
      break;
    case 3:
      (void)CanSM_RequestComMode(Channel, ComMode);
      break;
    case 4:
      (void)CanSM_RequestComMode(Channel, ComMode);
      break;
    case 5:
      (void)CanSM_RequestComMode(Channel, ComMode);
      break;
    case 6:
      (void)LinSM_RequestComMode(Channel, ComMode);
      break;
    case 7:
      (void)LinSM_RequestComMode(Channel, ComMode);
      break;
    case 8:
      (void)LinSM_RequestComMode(Channel, ComMode);
      break;
    case 9:
      (void)LinSM_RequestComMode(Channel, ComMode);
      break;
    case 10:
      (void)LinSM_RequestComMode(Channel, ComMode);
      break;
    case 11:
      (void)LinSM_RequestComMode(Channel, ComMode);
      break;
    case 12:
      (void)LinSM_RequestComMode(Channel, ComMode);
      break;
    case 13:
      (void)LinSM_RequestComMode(Channel, ComMode);
      break;
    default: /* PRQA S 2016 */ /* MD_MSR_EmptyClause */
      break;
  }
} /* PRQA S 6030 */ /* MD_MSR_STCYC */


/*********************************************************************************************************************
  FUNCTION: ComM_GetCurrentBusSMMode
*********************************************************************************************************************/
/*!
 * \internal
 * - #10 Query the current communication mode from the corresponding BusSM.
 * \endinternal
 */
FUNC(void, COMM_CODE) ComM_GetCurrentBusSMMode(NetworkHandleType Channel, P2VAR(ComM_ModeType, AUTOMATIC, AUTOMATIC) ComMode)
{
  /* ----- Implementation ----------------------------------------------- */
  /* #10 Query the current communication mode from the corresponding BusSM. */
  switch(Channel)
  {
    case 0:
      (void)CanSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 1:
      (void)CanSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 2:
      (void)CanSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 3:
      (void)CanSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 4:
      (void)CanSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 5:
      (void)CanSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 6:
      (void)LinSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 7:
      (void)LinSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 8:
      (void)LinSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 9:
      (void)LinSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 10:
      (void)LinSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 11:
      (void)LinSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 12:
      (void)LinSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    case 13:
      (void)LinSM_GetCurrentComMode(Channel, ComMode); /* SBSW_COMM_CALL_BUSSM_GET_CURRENT_COMMODE */
      break;
    default: /* PRQA S 2016 */ /* MD_MSR_EmptyClause */
      break;
  }
} /* PRQA S 6030 */ /* MD_MSR_STCYC */




#define COMM_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "MemMap.h"

