/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Dcm
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Dcm_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:32
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/



#define DCM_LCFG_SOURCE
/* ----------------------------------------------
 ~&&&   Includes
---------------------------------------------- */
                                                                                                                                                     /* PRQA S 1533 EOF */ /* MD_Dcm_ObjectOnlyAccessedOnce */
#include "Dcm.h"
#include "Rte_Dcm.h"
#include "Dcm_Int.h"
#include "PduR_Dcm.h"
#include "ComM_Dcm.h"
/* ----------------------------------------------
 ~&&&   Defines
---------------------------------------------- */
#if (DCM_DIDMGR_NVM_READ_ENABLED == STD_ON) || \
    (DCM_DIDMGR_NVM_WRITE_ENABLED == STD_ON)
# if defined(NVM_VENDOR_ID)
#  if (NVM_VENDOR_ID == 30u)
/* Only Vector NvM supports this feature up to now */
#   define Dcm_GetDcmNvMBlockId(blockId)                             (uint16)(NvM_GetDcmBlockId(blockId))                                            /* PRQA S 3453 */ /* QAC 7.0:  A function could probably be used instead of this function-like macro */ /* Macro is more efficient! */
#  endif
# endif

/* Default NvM handle offset */
# if !defined(Dcm_GetDcmNvMBlockId)
#  define Dcm_GetDcmNvMBlockId(blockId)                              (uint16)(blockId)                                                               /* PRQA S 3453 */ /* QAC 7.0:  A function could probably be used instead of this function-like macro */ /* Macro is more efficient! */
# endif
#endif
/* ----------------------------------------------
 ~&&&   Call-back function declarations
---------------------------------------------- */
#define DCM_START_SEC_CALLOUT_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/***********************************************************************************************************************
 *  Dcm_RidMgr_0401_Start()
***********************************************************************************************************************/
/*! \brief         Wraps RIDs execution interface.
 *  \details       Converts uint8 arrays to other signal types required by the RID execution interface and vice versa.
 *  \param[in]     OpStatus           The operation status
 *  \param[in,out] pMsgContext        Message-related information for one diagnostic protocol identifier
 *  \param[in,out] DataLength         IN: Concrete length of the dynamic request signal
 *                                    OUT: Concrete length of the dynamic response Signal
 *  \param[out]    ErrorCode          Negative response code
 *  \return        E_OK               The operation is finished
 *  \return        DCM_E_PENDING      The operation is not yet finished
 *  \return        DCM_E_FORCE_RCRRP  Forces a RCR-RP response
 *                                    The call out will called again once the response is sent. The OpStatus parameter
 *                                    will contain the transmission result
 *  \return        E_NOT_OK           The operation has failed. A concrete NRC shall be set, otherwise the DCM sends NRC
 *                                    0x22
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_RidMgr_0401_Start(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_RidMgrRidLengthPtrType DataLength, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_SvcWrapper_DcmAddOn_Service87_Processor()
***********************************************************************************************************************/
/*! \brief         Wraps a specific externally implemented service-processor.
 *  \details       Encapsulates the internally used pContext and ErrorCode parameter and transforms the return value of
 *                 the called service-processor.
 *  \param[in]     pContext              Pointer to the context
 *  \param[in]     opStatus              The operation status
 *  \param[in,out] pMsgContext           Message-related information for one diagnostic protocol identifier
 *                                       The pointers in pMsgContext points behind the SID.
 *  \param[out]    ErrorCode             Negative response code in case return value is DCM_E_NOT_OK
 *  \return        DCM_E_OK              Job processing finished, send positive response
 *  \return        DCM_E_PENDING         Job processing is not yet finished
 *  \return        DCM_E_FORCE_RCRRP     (Vendor extension) Forces a RCR-RP response.
 *                                       The call out will called again once the response is sent. The OpStatus
 *                                       parameter will contain the transmission result
 *  \return        DCM_E_PROCESSINGDONE  (Vendor extension) Can be returned instead of calling Dcm_ProcessingDone() for
 *                                       the current pMsgContext.
 *                                       Saves application code and stack usage.
 *  \return        DCM_E_STOP_REPEATER   Stops the repeater proxy
 *  \return        DCM_E_NOT_OK          Job processing finished, send NRC from the ErrorCode
 *  \context       TASK
 *  \reentrant     FALSE
 *  \synchronous   FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SvcWrapper_DcmAddOn_Service87_Processor(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_ServiceNoPostProcessor()
***********************************************************************************************************************/
/*! \brief         Dummy post-processor
 *  \details       This post-processor is called for diagnostic services which do not require any post processing.
 *  \param[in]     pContext  Pointer to the context
 *  \param[in]     status    The post-processing status
 *  \context       TASK
 *  \reentrant     FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(void, DCM_CALLOUT_CODE) Dcm_ServiceNoPostProcessor(Dcm_ContextPtrType pContext, Dcm_ConfirmationStatusType status);
/***********************************************************************************************************************
 *  Dcm_ServiceNoUpdater()
***********************************************************************************************************************/
/*! \brief         Realizes a dummy paged buffer updater.
 *  \details       This function is never called.
 *  \param[in]     pContext      Pointer to the context
 *  \param[in]     opStatus      The operation status
 *  \param[in,out] pDataContext  Pointer to the data context
 *  \param[out]    ErrorCode     Negative response code
 *  \return        DCM_E_NOT_OK  Operation failed. Take the NRC from ErrorCode. Do not call again
 *  \context       TASK
 *  \reentrant     FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_ServiceNoUpdater(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_DiagDataContextPtrType pDataContext, Dcm_NegativeResponseCodePtrType ErrorCode);
/***********************************************************************************************************************
 *  Dcm_ServiceNoCancel()
***********************************************************************************************************************/
/*! \brief         Dummy service cancellation.
 *  \details       -
 *  \param[in,out] pDataContext  Pointer to the data context
 *  \context       TASK
 *  \reentrant     FALSE
 *  \pre           -
***********************************************************************************************************************/
DCM_LOCAL FUNC(void, DCM_CALLOUT_CODE) Dcm_ServiceNoCancel(Dcm_ContextPtrType pContext, Dcm_DiagDataContextPtrType pDataContext);
#define DCM_STOP_SEC_CALLOUT_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   Uninitialized RAM 8-Bit
---------------------------------------------- */
#define DCM_START_SEC_VAR_NO_INIT_8
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/*! DCM protocol descriptor */
DCM_LOCAL VAR(Dcm_MsgItemType, DCM_VAR_NOINIT) Dcm_CfgNetBuffer_000[2000];
#define DCM_STOP_SEC_VAR_NO_INIT_8
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   ROM 8-Bit
---------------------------------------------- */
#define DCM_START_SEC_CONST_8
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/*! TxPduId to DCM connection map */
CONST(Dcm_NetConnRefMemType, DCM_CONST) Dcm_CfgNetTxPduInfo[4]=
{
    0u
  , 1u
  , 2u
  , 2u
};
/*! Map of DCM relevant network handles */
CONST(Dcm_CfgNetNetIdRefMemType, DCM_CONST) Dcm_CfgNetConnComMChannelMap[1]=
{
    0u
};
/*! Service 0x28 list of channels for the all-comm-channel parameter */
CONST(Dcm_CfgNetNetIdRefMemType, DCM_CONST) Dcm_CfgNetComCtrlChannelListAll[11]=
{
   10u
  , 0u
  , 1u
  , 2u
  , 3u
  , 4u
  , 5u
  , 6u
  , 7u
  , 8u
  , 9u
};
/*! Look up table of all supported ALFIDs */
CONST(uint8, DCM_CONST) Dcm_CfgMemMgrAlfidLookUpTable[2]=
{
   1u
  ,0x25u
};
/*! Look up table of all supported MIDs */
CONST(uint8, DCM_CONST) Dcm_CfgMemMgrMidLookUpTable[2]=
{
   1u
  ,0x01u
};
/*! Look up table of DCM service identifiers */
CONST(uint8, DCM_CONST) Dcm_CfgDiagSvcIdLookUpTable[16]=
{
   15u
  ,0x10u
  ,0x11u
  ,0x14u
  ,0x19u
  ,0x22u
  ,0x23u
  ,0x27u
  ,0x28u
  ,0x2Eu
  ,0x2Fu
  ,0x31u
  ,0x3Du
  ,0x3Eu
  ,0x85u
  ,0x87u
};
/*! Service 0x10 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc10SubFuncLookUpTable[4]=
{
   3u
  ,0x01u
  ,0x02u
  ,0x03u
};
/*! Service 0x11 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc11SubFuncLookUpTable[3]=
{
   2u
  ,0x01u
  ,0x02u
};
/*! Service 0x19 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc19SubFuncLookUpTable[7]=
{
   6u
  ,0x01u
  ,0x02u
  ,0x03u
  ,0x04u
  ,0x06u
  ,0x0Au
};
/*! Service 0x27 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc27SubFuncLookUpTable[35]=
{
   34u
  ,0x01u
  ,0x02u
  ,0x07u
  ,0x08u
  ,0x09u
  ,0x0Au
  ,0x0Bu
  ,0x0Cu
  ,0x0Du
  ,0x0Eu
  ,0x0Fu
  ,0x10u
  ,0x11u
  ,0x12u
  ,0x15u
  ,0x16u
  ,0x17u
  ,0x18u
  ,0x1Bu
  ,0x1Cu
  ,0x29u
  ,0x2Au
  ,0x2Bu
  ,0x2Cu
  ,0x2Du
  ,0x2Eu
  ,0x2Fu
  ,0x30u
  ,0x31u
  ,0x32u
  ,0x33u
  ,0x34u
  ,0x37u
  ,0x38u
};
/*! Service 0x28 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc28SubFuncLookUpTable[3]=
{
   2u
  ,0x00u
  ,0x01u
};
/*! Service 0x28 look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc28MessageTypeLookUpTable[4]=
{
   3u
  ,0x01u
  ,0x02u
  ,0x03u
};
/*! Service 0x28 network ID lookup */
CONST(uint8, DCM_CONST) Dcm_CfgSvc28SubNetIdLookUp[3]=
{
   2u
  ,0x00u
  ,0x0Fu
};
/*! Service 0x3E look up table  */
CONST(uint8, DCM_CONST) Dcm_CfgSvc3ESubFuncLookUpTable[2]=
{
   1u
  ,0x00u
};
/*! Look up table of service 0x85 */
CONST(uint8, DCM_CONST) Dcm_CfgSvc85SubFuncLookUpTable[3]=
{
   2u
  ,0x01u
  ,0x02u
};
#define DCM_STOP_SEC_CONST_8
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   ROM 16-Bit
---------------------------------------------- */
#define DCM_START_SEC_CONST_16
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/*! DID look up table  */
CONST(uint16, DCM_CONST) Dcm_CfgDidMgrDidLookUpTable[113]=
{
   112u
  ,0x0100u
  ,0x0104u
  ,0x1100u
  ,0x1102u
  ,0x1104u
  ,0x1207u
  ,0x1208u
  ,0x1209u
  ,0x120Au
  ,0x120Du
  ,0x120Eu
  ,0x120Fu
  ,0x1211u
  ,0x1212u
  ,0x1213u
  ,0x1214u
  ,0x1216u
  ,0x1217u
  ,0x1218u
  ,0x1219u
  ,0x121Bu
  ,0x121Cu
  ,0x121Du
  ,0x121Eu
  ,0x1220u
  ,0x1221u
  ,0x1225u
  ,0x1227u
  ,0x122Cu
  ,0x122Eu
  ,0x1232u
  ,0x123Bu
  ,0x1244u
  ,0x1245u
  ,0x1246u
  ,0x1247u
  ,0x1248u
  ,0x1250u
  ,0x1253u
  ,0x1254u
  ,0x1256u
  ,0x1257u
  ,0x1258u
  ,0x125Au
  ,0x125Cu
  ,0x125Eu
  ,0x1276u
  ,0x1277u
  ,0x127Au
  ,0x127Bu
  ,0x127Cu
  ,0x127Du
  ,0x127Eu
  ,0x127Fu
  ,0x1280u
  ,0x1282u
  ,0x1283u
  ,0x1284u
  ,0x1285u
  ,0x1286u
  ,0x1287u
  ,0x1288u
  ,0x1289u
  ,0x128Au
  ,0x128Bu
  ,0x128Cu
  ,0x128Du
  ,0x128Eu
  ,0x128Fu
  ,0x1290u
  ,0x1291u
  ,0x1292u
  ,0x1293u
  ,0x1294u
  ,0x1295u
  ,0x1296u
  ,0x1297u
  ,0x1298u
  ,0x1299u
  ,0x129Du
  ,0x129Eu
  ,0x129Fu
  ,0x12A0u
  ,0x12A1u
  ,0x12A2u
  ,0x12A3u
  ,0x12A4u
  ,0x12A5u
  ,0x12A6u
  ,0x12A7u
  ,0x12A8u
  ,0x12A9u
  ,0x12AAu
  ,0x12ABu
  ,0x12ACu
  ,0x12ADu
  ,0xE002u
  ,0xE003u
  ,0xE009u
  ,0xE00Au
  ,0xE00Bu
  ,0xE00Cu
  ,0xE00Du
  ,0xF180u
  ,0xF181u
  ,0xF182u
  ,0xF184u
  ,0xF185u
  ,0xF186u
  ,0xF190u
  ,0xF191u
  ,0xF197u
};
/*! RID look up table  */
CONST(uint16, DCM_CONST) Dcm_CfgRidMgrRidLookUpTable[6]=
{
   5u
  ,0x0400u
  ,0x0401u
  ,0x0402u
  ,0xE007u
  ,0xE00Eu
};
#define DCM_STOP_SEC_CONST_16
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   ROM of unspecified size
---------------------------------------------- */
#define DCM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/*! DCM buffer descriptor */
CONST(Dcm_CfgNetBufferInfoType, DCM_CONST) Dcm_CfgNetBufferInfo[1]=
{
   { Dcm_CfgNetBuffer_000,2000u}
};
/*! RxPduId map */
CONST(Dcm_CfgNetRxPduInfoType, DCM_CONST) Dcm_CfgNetRxPduInfo[5]=
{
   { FALSE, 0u}
  ,{ FALSE, 1u}
  ,{ TRUE, 1u}
  ,{ FALSE, 2u}
  ,{ TRUE, 2u}
};
/*! DCM connection descriptor */
CONST(Dcm_CfgNetConnectionInfoType, DCM_CONST) Dcm_CfgNetConnectionInfo[3]=
{
   { 0x00F3u,PduRConf_PduRSrcPdu_PduRSrcPdu_3ec0669e, 0u,0u}
  ,{ 0x00F4u,PduRConf_PduRSrcPdu_PduRSrcPdu_c1ab151b, 0u,0u}
  ,{ 0x00F2u,PduRConf_PduRSrcPdu_PduRSrcPdu_09be0918, 0u,0u}
};
/*! DCM protocol descriptor */
CONST(Dcm_CfgNetProtocolInfoType, DCM_CONST) Dcm_CfgNetProtocolInfo[1]=
{
   { {    4u, 100u},4095u, 3u,TRUE,0u,DemConf_DemClient_DemClient, 0u}
};
/*! Map of all relevant for DCM network handles */
CONST(NetworkHandleType, DCM_CONST) Dcm_CfgNetAllComMChannelMap[10]=
{
   ComMConf_ComMChannel_CN_Backbone2_78967e2c
  ,ComMConf_ComMChannel_CN_CabSubnet_9ea693f1
  ,ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54
  ,ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae
  ,ComMConf_ComMChannel_CN_FMSNet_fce1aae5
  ,ComMConf_ComMChannel_CN_LIN00_2cd9a7df
  ,ComMConf_ComMChannel_CN_LIN01_5bde9749
  ,ComMConf_ComMChannel_CN_LIN02_c2d7c6f3
  ,ComMConf_ComMChannel_CN_LIN03_b5d0f665
  ,ComMConf_ComMChannel_CN_LIN04_2bb463c6
};
/*! Look up table of DCM relevant network handles */
CONST(NetworkHandleType, DCM_CONST) Dcm_CfgNetNetworkHandleLookUpTable[2]=
{
   1u
  ,ComMConf_ComMChannel_CN_Backbone2_78967e2c
};
/*! Diagnostic service execution conditions */
CONST(Dcm_CfgStatePreconditionInfoType, DCM_CONST) Dcm_CfgStatePreconditions[26]=
{
   { { 0x000007u,0x03FFFFu}}
  ,{ { 0x000005u,0x03FFFFu}}
  ,{ { 0x000004u,0x027800u}}
  ,{ { 0x000006u,0x03FFFFu}}
  ,{ { 0x000004u,0x03FFFFu}}
  ,{ { 0x000004u,0x00FF0Cu}}
  ,{ { 0x000004u,0x008004u}}
  ,{ { 0x000004u,0x00000Cu}}
  ,{ { 0x000004u,0x007800u}}
  ,{ { 0x000004u,0x010010u}}
  ,{ { 0x000004u,0x000020u}}
  ,{ { 0x000004u,0x000400u}}
  ,{ { 0x000004u,0x000010u}}
  ,{ { 0x000004u,0x000040u}}
  ,{ { 0x000004u,0x000004u}}
  ,{ { 0x000004u,0x000100u}}
  ,{ { 0x000004u,0x000200u}}
  ,{ { 0x000005u,0x03FDDDu}}
  ,{ { 0x000004u,0x03FFFEu}}
  ,{ { 0x000004u,0x000008u}}
  ,{ { 0x000007u,0x000800u}}
  ,{ { 0x000007u,0x001000u}}
  ,{ { 0x000007u,0x021000u}}
  ,{ { 0x000007u,0x002000u}}
  ,{ { 0x000007u,0x022000u}}
  ,{ { 0x000007u,0x004000u}}
};
/*! Session state properties */
CONST(Dcm_CfgStateSessionInfoType, DCM_CONST) Dcm_CfgStateSessionInfo[3]=
{
   { {    5u, 500u},RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION,0x01u}
  ,{ {    5u, 500u},RTE_MODE_DcmDiagnosticSessionControl_PROGRAMMING_SESSION,0x02u}
  ,{ {    5u, 500u},RTE_MODE_DcmDiagnosticSessionControl_EXTENDED_SESSION,0x03u}
};
/*! Security Access state properties */
CONST(Dcm_CfgStateSecurityInfoType, DCM_CONST) Dcm_CfgStateSecurityInfo[17]=
{
   { 1000u,   0u,FALSE, 3u,0x01u} /* SecLvl: SA_Seed_01 */
  ,{ 1000u,   0u,FALSE, 3u,0x04u} /* SecLvl: SA_Seed_07 */
  ,{ 1000u,   0u,FALSE, 3u,0x05u} /* SecLvl: SA_Seed_09 */
  ,{ 1000u,   0u,FALSE, 3u,0x06u} /* SecLvl: SA_Seed_0B */
  ,{ 1000u,   0u,FALSE, 3u,0x07u} /* SecLvl: SA_Seed_0D */
  ,{ 1000u,   0u,FALSE, 3u,0x08u} /* SecLvl: SA_Seed_0F */
  ,{ 1000u,   0u,FALSE, 3u,0x09u} /* SecLvl: SA_Seed_11 */
  ,{ 1000u,   0u,FALSE, 3u,0x0Bu} /* SecLvl: SA_Seed_15 */
  ,{ 1000u,   0u,FALSE, 3u,0x0Cu} /* SecLvl: SA_Seed_17 */
  ,{ 1000u,   0u,FALSE, 3u,0x0Eu} /* SecLvl: SA_Seed_1B */
  ,{ 1000u,   0u,FALSE, 3u,0x15u} /* SecLvl: SA_Seed_29 */
  ,{ 1000u,   0u,FALSE, 3u,0x16u} /* SecLvl: SA_Seed_2B */
  ,{ 1000u,   0u,FALSE, 3u,0x17u} /* SecLvl: SA_Seed_2D */
  ,{ 1000u,   0u,FALSE, 3u,0x18u} /* SecLvl: SA_Seed_2F */
  ,{ 1000u,   0u,FALSE, 3u,0x19u} /* SecLvl: SA_Seed_31 */
  ,{ 1000u,   0u,FALSE, 3u,0x1Au} /* SecLvl: SA_Seed_33 */
  ,{ 1000u,   0u,FALSE, 3u,0x1Cu} /* SecLvl: SA_Seed_37 */
};
/*! DID properties */
CONST(Dcm_CfgDidMgrDidInfoType, DCM_CONST) Dcm_CfgDidMgrDidInfo[112]=
{
   {   16u,  16u,  16u,   0u,   0u,0x01u} /* DID: 0x0100 */
  ,{   64u,  64u,  64u,   1u,   0u,0x01u} /* DID: 0x0104 */
  ,{    1u,   1u,   1u,   2u,   0u,0x01u} /* DID: 0x1100 */
  ,{    2u,   2u,   2u,   3u,   0u,0x01u} /* DID: 0x1102 */
  ,{    4u,   4u,   4u,   4u,   0u,0x01u} /* DID: 0x1104 */
  ,{    1u,   1u,   1u,   5u,   0u,0x01u} /* DID: 0x1207 */
  ,{    1u,   1u,   1u,   6u,   0u,0x01u} /* DID: 0x1208 */
  ,{    5u,   5u,   5u,   7u,   0u,0x01u} /* DID: 0x1209 */
  ,{    1u,   1u,   1u,   8u,   0u,0x01u} /* DID: 0x120A */
  ,{   12u,  12u,  12u,   9u,   0u,0x01u} /* DID: 0x120D */
  ,{    8u,   8u,   8u,  10u,   0u,0x01u} /* DID: 0x120E */
  ,{    8u,   8u,   8u,  11u,   0u,0x01u} /* DID: 0x120F */
  ,{    8u,   8u,   8u,  12u,   0u,0x01u} /* DID: 0x1211 */
  ,{    8u,   8u,   8u,  13u,   0u,0x01u} /* DID: 0x1212 */
  ,{    8u,   8u,   8u,  14u,   0u,0x01u} /* DID: 0x1213 */
  ,{    8u,   8u,   8u,  15u,   0u,0x01u} /* DID: 0x1214 */
  ,{    8u,   8u,   8u,  16u,   0u,0x01u} /* DID: 0x1216 */
  ,{    8u,   8u,   8u,  17u,   0u,0x01u} /* DID: 0x1217 */
  ,{    8u,   8u,   8u,  18u,   0u,0x01u} /* DID: 0x1218 */
  ,{    8u,   8u,   8u,  19u,   0u,0x01u} /* DID: 0x1219 */
  ,{    8u,   8u,   8u,  20u,   0u,0x01u} /* DID: 0x121B */
  ,{    8u,   8u,   8u,  21u,   0u,0x01u} /* DID: 0x121C */
  ,{    8u,   8u,   8u,  22u,   0u,0x01u} /* DID: 0x121D */
  ,{    8u,   8u,   8u,  23u,   0u,0x01u} /* DID: 0x121E */
  ,{    8u,   8u,   8u,  24u,   0u,0x01u} /* DID: 0x1220 */
  ,{    8u,   8u,   8u,  25u,   0u,0x01u} /* DID: 0x1221 */
  ,{    8u,   8u,   8u,  26u,   0u,0x01u} /* DID: 0x1225 */
  ,{    8u,   8u,   8u,  27u,   0u,0x01u} /* DID: 0x1227 */
  ,{    8u,   8u,   8u,  28u,   0u,0x01u} /* DID: 0x122C */
  ,{    8u,   8u,   8u,  29u,   0u,0x01u} /* DID: 0x122E */
  ,{    1u,   1u,   1u,  30u,   0u,0x01u} /* DID: 0x1232 */
  ,{    1u,   1u,   1u,  31u,   0u,0x05u} /* DID: 0x123B */
  ,{    1u,   1u,   1u,  33u,   1u,0x05u} /* DID: 0x1244 */
  ,{    1u,   1u,   1u,  35u,   2u,0x05u} /* DID: 0x1245 */
  ,{    1u,   1u,   1u,  37u,   3u,0x05u} /* DID: 0x1246 */
  ,{    1u,   1u,   1u,  39u,   4u,0x05u} /* DID: 0x1247 */
  ,{    1u,   1u,   1u,  41u,   5u,0x05u} /* DID: 0x1248 */
  ,{    6u,   6u,   6u,  43u,   0u,0x01u} /* DID: 0x1250 */
  ,{    1u,   1u,   1u,  44u,   6u,0x05u} /* DID: 0x1253 */
  ,{    1u,   1u,   1u,  46u,   7u,0x05u} /* DID: 0x1254 */
  ,{    1u,   1u,   1u,  48u,   0u,0x01u} /* DID: 0x1256 */
  ,{    1u,   1u,   1u,  49u,   0u,0x01u} /* DID: 0x1257 */
  ,{    2u,   2u,   2u,  50u,   0u,0x01u} /* DID: 0x1258 */
  ,{  120u, 120u, 120u,  51u,   0u,0x01u} /* DID: 0x125A */
  ,{    5u,   5u,   5u,  52u,   0u,0x01u} /* DID: 0x125C */
  ,{  256u, 256u, 256u,  53u,   0u,0x03u} /* DID: 0x125E */
  ,{   64u,  64u,  64u,  55u,   0u,0x01u} /* DID: 0x1276 */
  ,{    1u,   1u,   1u,  56u,   0u,0x01u} /* DID: 0x1277 */
  ,{    1u,   1u,   1u,  57u,   0u,0x01u} /* DID: 0x127A */
  ,{    4u,   4u,   4u,  58u,   0u,0x01u} /* DID: 0x127B */
  ,{    2u,   2u,   2u,  59u,   0u,0x01u} /* DID: 0x127C */
  ,{    2u,   2u,   2u,  60u,   0u,0x01u} /* DID: 0x127D */
  ,{    2u,   2u,   2u,  61u,   0u,0x01u} /* DID: 0x127E */
  ,{    1u,   1u,   1u,  62u,   0u,0x01u} /* DID: 0x127F */
  ,{    4u,   4u,   4u,  63u,   0u,0x01u} /* DID: 0x1280 */
  ,{    1u,   1u,   1u,  64u,   0u,0x01u} /* DID: 0x1282 */
  ,{    1u,   1u,   1u,  65u,   0u,0x01u} /* DID: 0x1283 */
  ,{    4u,   4u,   4u,  66u,   0u,0x01u} /* DID: 0x1284 */
  ,{    4u,   4u,   4u,  67u,   0u,0x01u} /* DID: 0x1285 */
  ,{    1u,   1u,   1u,  68u,   0u,0x01u} /* DID: 0x1286 */
  ,{    1u,   1u,   1u,  69u,   0u,0x01u} /* DID: 0x1287 */
  ,{    1u,   1u,   1u,  70u,   0u,0x01u} /* DID: 0x1288 */
  ,{    1u,   1u,   1u,  71u,   0u,0x01u} /* DID: 0x1289 */
  ,{    1u,   1u,   1u,  72u,   0u,0x01u} /* DID: 0x128A */
  ,{    1u,   1u,   1u,  73u,   0u,0x01u} /* DID: 0x128B */
  ,{    1u,   1u,   1u,  74u,   0u,0x01u} /* DID: 0x128C */
  ,{    1u,   1u,   1u,  75u,   0u,0x01u} /* DID: 0x128D */
  ,{    1u,   1u,   1u,  76u,   0u,0x01u} /* DID: 0x128E */
  ,{    1u,   1u,   1u,  77u,   0u,0x01u} /* DID: 0x128F */
  ,{    1u,   1u,   1u,  78u,   0u,0x01u} /* DID: 0x1290 */
  ,{    1u,   1u,   1u,  79u,   0u,0x01u} /* DID: 0x1291 */
  ,{    1u,   1u,   1u,  80u,   0u,0x01u} /* DID: 0x1292 */
  ,{    1u,   1u,   1u,  81u,   0u,0x01u} /* DID: 0x1293 */
  ,{    1u,   1u,   1u,  82u,   0u,0x01u} /* DID: 0x1294 */
  ,{    1u,   1u,   1u,  83u,   0u,0x01u} /* DID: 0x1295 */
  ,{    1u,   1u,   1u,  84u,   0u,0x01u} /* DID: 0x1296 */
  ,{    1u,   1u,   1u,  85u,   0u,0x01u} /* DID: 0x1297 */
  ,{    1u,   1u,   1u,  86u,   0u,0x01u} /* DID: 0x1298 */
  ,{    1u,   1u,   1u,  87u,   0u,0x01u} /* DID: 0x1299 */
  ,{    4u,   4u,   4u,  88u,   0u,0x01u} /* DID: 0x129D */
  ,{    4u,   4u,   4u,  89u,   0u,0x01u} /* DID: 0x129E */
  ,{    4u,   4u,   4u,  90u,   0u,0x01u} /* DID: 0x129F */
  ,{    1u,   1u,   1u,  91u,   0u,0x01u} /* DID: 0x12A0 */
  ,{    1u,   1u,   1u,  92u,   0u,0x01u} /* DID: 0x12A1 */
  ,{    1u,   1u,   1u,  93u,   8u,0x05u} /* DID: 0x12A2 */
  ,{    4u,   4u,   4u,  95u,   9u,0x05u} /* DID: 0x12A3 */
  ,{    4u,   4u,   4u,  97u,  10u,0x05u} /* DID: 0x12A4 */
  ,{    1u,   1u,   1u,  99u,  11u,0x05u} /* DID: 0x12A5 */
  ,{    1u,   1u,   1u, 101u,  12u,0x05u} /* DID: 0x12A6 */
  ,{    1u,   1u,   1u, 103u,  13u,0x05u} /* DID: 0x12A7 */
  ,{    1u,   1u,   1u, 105u,  14u,0x05u} /* DID: 0x12A8 */
  ,{    4u,   4u,   4u, 107u,  15u,0x05u} /* DID: 0x12A9 */
  ,{    4u,   4u,   4u, 109u,  16u,0x05u} /* DID: 0x12AA */
  ,{    4u,   4u,   4u, 111u,  17u,0x05u} /* DID: 0x12AB */
  ,{    1u,   1u,   1u, 113u,  18u,0x05u} /* DID: 0x12AC */
  ,{    5u,   5u,   5u, 115u,   0u,0x01u} /* DID: 0x12AD */
  ,{    1u,   1u,   1u, 116u,   0u,0x01u} /* DID: 0xE002 */
  ,{    1u,   1u,   1u, 117u,  19u,0x05u} /* DID: 0xE003 */
  ,{  126u, 126u, 126u, 119u,   0u,0x03u} /* DID: 0xE009 */
  ,{   60u,  60u,  60u, 121u,   0u,0x01u} /* DID: 0xE00A */
  ,{   80u,  80u,  80u, 122u,   0u,0x03u} /* DID: 0xE00B */
  ,{  130u, 130u, 130u, 124u,   0u,0x03u} /* DID: 0xE00C */
  ,{   40u,  40u,  40u, 126u,   0u,0x03u} /* DID: 0xE00D */
  ,{  221u, 221u, 221u, 128u,   0u,0x01u} /* DID: 0xF180 */
  ,{    0u, 241u, 241u, 129u,   0u,0x01u} /* DID: 0xF181 */
  ,{    0u, 241u, 241u, 130u,   0u,0x01u} /* DID: 0xF182 */
  ,{    0u,  86u,  86u, 131u,   0u,0x01u} /* DID: 0xF184 */
  ,{    0u,  86u,  86u, 132u,   0u,0x03u} /* DID: 0xF185 */
  ,{    1u,   1u,   1u, 134u,   0u,0x01u} /* DID: 0xF186 */
  ,{   17u,  17u,  17u, 135u,   0u,0x01u} /* DID: 0xF190 */
  ,{    0u, 406u, 406u, 136u,   0u,0x01u} /* DID: 0xF191 */
  ,{    8u,   8u,   8u, 137u,   0u,0x01u} /* DID: 0xF197 */
};
/*! DID operation properties */
CONST(Dcm_CfgDidMgrDidOpInfoType, DCM_CONST) Dcm_CfgDidMgrDidOpInfo[138]=
{
   {    1u,   0u,0x01u} /* DID: 0x0100 */
  ,{    1u,   1u,0x01u} /* DID: 0x0104 */
  ,{    1u,   2u,0x01u} /* DID: 0x1100 */
  ,{    1u,   3u,0x01u} /* DID: 0x1102 */
  ,{    1u,   4u,0x01u} /* DID: 0x1104 */
  ,{    1u,   5u,0x01u} /* DID: 0x1207 */
  ,{    1u,   6u,0x01u} /* DID: 0x1208 */
  ,{    1u,   7u,0x01u} /* DID: 0x1209 */
  ,{    1u,   8u,0x01u} /* DID: 0x120A */
  ,{    1u,   9u,0x01u} /* DID: 0x120D */
  ,{    9u,  10u,0x01u} /* DID: 0x120E */
  ,{    9u,  11u,0x01u} /* DID: 0x120F */
  ,{    9u,  12u,0x01u} /* DID: 0x1211 */
  ,{    9u,  13u,0x01u} /* DID: 0x1212 */
  ,{    9u,  14u,0x01u} /* DID: 0x1213 */
  ,{    9u,  15u,0x01u} /* DID: 0x1214 */
  ,{    9u,  16u,0x01u} /* DID: 0x1216 */
  ,{    9u,  17u,0x01u} /* DID: 0x1217 */
  ,{    9u,  18u,0x01u} /* DID: 0x1218 */
  ,{    9u,  19u,0x01u} /* DID: 0x1219 */
  ,{    9u,  20u,0x01u} /* DID: 0x121B */
  ,{    9u,  21u,0x01u} /* DID: 0x121C */
  ,{    9u,  22u,0x01u} /* DID: 0x121D */
  ,{    9u,  23u,0x01u} /* DID: 0x121E */
  ,{    9u,  24u,0x01u} /* DID: 0x1220 */
  ,{    9u,  25u,0x01u} /* DID: 0x1221 */
  ,{    9u,  26u,0x01u} /* DID: 0x1225 */
  ,{    9u,  27u,0x01u} /* DID: 0x1227 */
  ,{    9u,  28u,0x01u} /* DID: 0x122C */
  ,{    9u,  29u,0x01u} /* DID: 0x122E */
  ,{    1u,  30u,0x01u} /* DID: 0x1232 */
  ,{    6u,  31u,0x01u} /* DID: 0x123B */
  ,{    6u,  32u,0x0Du} /* DID: 0x123B */
  ,{    6u,  35u,0x01u} /* DID: 0x1244 */
  ,{    6u,  36u,0x0Du} /* DID: 0x1244 */
  ,{    6u,  39u,0x01u} /* DID: 0x1245 */
  ,{    6u,  40u,0x0Du} /* DID: 0x1245 */
  ,{    6u,  43u,0x01u} /* DID: 0x1246 */
  ,{    6u,  44u,0x0Du} /* DID: 0x1246 */
  ,{    6u,  47u,0x01u} /* DID: 0x1247 */
  ,{    6u,  48u,0x0Du} /* DID: 0x1247 */
  ,{    6u,  51u,0x01u} /* DID: 0x1248 */
  ,{    6u,  52u,0x0Du} /* DID: 0x1248 */
  ,{    1u,  55u,0x01u} /* DID: 0x1250 */
  ,{    6u,  56u,0x01u} /* DID: 0x1253 */
  ,{    6u,  57u,0x0Du} /* DID: 0x1253 */
  ,{    6u,  60u,0x01u} /* DID: 0x1254 */
  ,{    6u,  61u,0x0Du} /* DID: 0x1254 */
  ,{    9u,  64u,0x01u} /* DID: 0x1256 */
  ,{    1u,  65u,0x01u} /* DID: 0x1257 */
  ,{   10u,  66u,0x01u} /* DID: 0x1258 */
  ,{    1u,  67u,0x01u} /* DID: 0x125A */
  ,{    1u,  68u,0x01u} /* DID: 0x125C */
  ,{   11u,  69u,0x01u} /* DID: 0x125E */
  ,{   11u,  70u,0x01u} /* DID: 0x125E */
  ,{    1u,  71u,0x01u} /* DID: 0x1276 */
  ,{    1u,  72u,0x01u} /* DID: 0x1277 */
  ,{    1u,  73u,0x01u} /* DID: 0x127A */
  ,{    1u,  74u,0x01u} /* DID: 0x127B */
  ,{    1u,  75u,0x01u} /* DID: 0x127C */
  ,{    1u,  76u,0x01u} /* DID: 0x127D */
  ,{    1u,  77u,0x01u} /* DID: 0x127E */
  ,{    1u,  78u,0x01u} /* DID: 0x127F */
  ,{    1u,  79u,0x01u} /* DID: 0x1280 */
  ,{    1u,  80u,0x01u} /* DID: 0x1282 */
  ,{    1u,  81u,0x01u} /* DID: 0x1283 */
  ,{    1u,  82u,0x01u} /* DID: 0x1284 */
  ,{    1u,  83u,0x01u} /* DID: 0x1285 */
  ,{    1u,  84u,0x01u} /* DID: 0x1286 */
  ,{    1u,  85u,0x01u} /* DID: 0x1287 */
  ,{    1u,  86u,0x01u} /* DID: 0x1288 */
  ,{    1u,  87u,0x01u} /* DID: 0x1289 */
  ,{    1u,  88u,0x01u} /* DID: 0x128A */
  ,{    1u,  89u,0x01u} /* DID: 0x128B */
  ,{    1u,  90u,0x01u} /* DID: 0x128C */
  ,{    1u,  91u,0x01u} /* DID: 0x128D */
  ,{    1u,  92u,0x01u} /* DID: 0x128E */
  ,{    1u,  93u,0x01u} /* DID: 0x128F */
  ,{    1u,  94u,0x01u} /* DID: 0x1290 */
  ,{    1u,  95u,0x01u} /* DID: 0x1291 */
  ,{    1u,  96u,0x01u} /* DID: 0x1292 */
  ,{    1u,  97u,0x01u} /* DID: 0x1293 */
  ,{    1u,  98u,0x01u} /* DID: 0x1294 */
  ,{    1u,  99u,0x01u} /* DID: 0x1295 */
  ,{    1u, 100u,0x01u} /* DID: 0x1296 */
  ,{    1u, 101u,0x01u} /* DID: 0x1297 */
  ,{    1u, 102u,0x01u} /* DID: 0x1298 */
  ,{    1u, 103u,0x01u} /* DID: 0x1299 */
  ,{    1u, 104u,0x01u} /* DID: 0x129D */
  ,{    1u, 105u,0x01u} /* DID: 0x129E */
  ,{    1u, 106u,0x01u} /* DID: 0x129F */
  ,{    1u, 107u,0x01u} /* DID: 0x12A0 */
  ,{    1u, 108u,0x01u} /* DID: 0x12A1 */
  ,{    6u, 109u,0x01u} /* DID: 0x12A2 */
  ,{    6u, 110u,0x0Du} /* DID: 0x12A2 */
  ,{    6u, 113u,0x01u} /* DID: 0x12A3 */
  ,{    6u, 114u,0x0Du} /* DID: 0x12A3 */
  ,{    6u, 117u,0x01u} /* DID: 0x12A4 */
  ,{    6u, 118u,0x0Du} /* DID: 0x12A4 */
  ,{    6u, 121u,0x01u} /* DID: 0x12A5 */
  ,{    6u, 122u,0x0Du} /* DID: 0x12A5 */
  ,{    6u, 125u,0x01u} /* DID: 0x12A6 */
  ,{    6u, 126u,0x0Du} /* DID: 0x12A6 */
  ,{    6u, 129u,0x01u} /* DID: 0x12A7 */
  ,{    6u, 130u,0x0Du} /* DID: 0x12A7 */
  ,{    6u, 133u,0x01u} /* DID: 0x12A8 */
  ,{    6u, 134u,0x0Du} /* DID: 0x12A8 */
  ,{    6u, 137u,0x01u} /* DID: 0x12A9 */
  ,{    6u, 138u,0x0Du} /* DID: 0x12A9 */
  ,{    6u, 141u,0x01u} /* DID: 0x12AA */
  ,{    6u, 142u,0x0Du} /* DID: 0x12AA */
  ,{    6u, 145u,0x01u} /* DID: 0x12AB */
  ,{    6u, 146u,0x0Du} /* DID: 0x12AB */
  ,{    6u, 149u,0x01u} /* DID: 0x12AC */
  ,{    6u, 150u,0x0Du} /* DID: 0x12AC */
  ,{   12u, 153u,0x01u} /* DID: 0x12AD */
  ,{   13u, 154u,0x01u} /* DID: 0xE002 */
  ,{   14u, 155u,0x01u} /* DID: 0xE003 */
  ,{   14u, 156u,0x0Du} /* DID: 0xE003 */
  ,{   15u, 159u,0x01u} /* DID: 0xE009 */
  ,{   15u, 160u,0x01u} /* DID: 0xE009 */
  ,{   12u, 161u,0x01u} /* DID: 0xE00A */
  ,{   15u, 162u,0x01u} /* DID: 0xE00B */
  ,{   15u, 163u,0x01u} /* DID: 0xE00B */
  ,{   15u, 164u,0x01u} /* DID: 0xE00C */
  ,{   15u, 165u,0x01u} /* DID: 0xE00C */
  ,{   16u, 166u,0x01u} /* DID: 0xE00D */
  ,{   16u, 167u,0x01u} /* DID: 0xE00D */
  ,{    1u, 168u,0x01u} /* DID: 0xF180 */
  ,{    1u, 169u,0x03u} /* DID: 0xF181 */
  ,{    1u, 171u,0x03u} /* DID: 0xF182 */
  ,{   17u, 173u,0x03u} /* DID: 0xF184 */
  ,{   17u, 175u,0x03u} /* DID: 0xF185 */
  ,{   18u, 177u,0x01u} /* DID: 0xF185 */
  ,{    1u, 178u,0x01u} /* DID: 0xF186 */
  ,{    1u, 179u,0x01u} /* DID: 0xF190 */
  ,{    1u, 180u,0x03u} /* DID: 0xF191 */
  ,{    1u, 182u,0x01u} /* DID: 0xF197 */
};
/*! DID operation classes */
CONST(Dcm_CfgDidMgrDidOpClassInfoType, DCM_CONST) Dcm_CfgDidMgrDidOpClassInfo[184]=
{
   {  0u}
  ,{  1u}
  ,{  2u}
  ,{  3u}
  ,{  4u}
  ,{  5u}
  ,{  6u}
  ,{  7u}
  ,{  8u}
  ,{  9u}
  ,{ 10u}
  ,{ 11u}
  ,{ 12u}
  ,{ 13u}
  ,{ 14u}
  ,{ 15u}
  ,{ 16u}
  ,{ 17u}
  ,{ 18u}
  ,{ 19u}
  ,{ 20u}
  ,{ 21u}
  ,{ 22u}
  ,{ 23u}
  ,{ 24u}
  ,{ 25u}
  ,{ 26u}
  ,{ 27u}
  ,{ 28u}
  ,{ 29u}
  ,{ 30u}
  ,{ 31u}
  ,{ 32u}
  ,{ 33u}
  ,{ 34u}
  ,{ 35u}
  ,{ 36u}
  ,{ 37u}
  ,{ 38u}
  ,{ 39u}
  ,{ 40u}
  ,{ 41u}
  ,{ 42u}
  ,{ 43u}
  ,{ 44u}
  ,{ 45u}
  ,{ 46u}
  ,{ 47u}
  ,{ 48u}
  ,{ 49u}
  ,{ 50u}
  ,{ 51u}
  ,{ 52u}
  ,{ 53u}
  ,{ 54u}
  ,{ 55u}
  ,{ 56u}
  ,{ 57u}
  ,{ 58u}
  ,{ 59u}
  ,{ 60u}
  ,{ 61u}
  ,{ 62u}
  ,{ 63u}
  ,{ 64u}
  ,{ 65u}
  ,{ 66u}
  ,{ 67u}
  ,{ 68u}
  ,{ 69u}
  ,{ 70u}
  ,{ 71u}
  ,{ 72u}
  ,{ 73u}
  ,{ 74u}
  ,{ 75u}
  ,{ 76u}
  ,{ 77u}
  ,{ 78u}
  ,{ 79u}
  ,{ 80u}
  ,{ 81u}
  ,{ 82u}
  ,{ 83u}
  ,{ 84u}
  ,{ 85u}
  ,{ 86u}
  ,{ 87u}
  ,{ 88u}
  ,{ 89u}
  ,{ 90u}
  ,{ 91u}
  ,{ 92u}
  ,{ 93u}
  ,{ 94u}
  ,{ 95u}
  ,{ 96u}
  ,{ 97u}
  ,{ 98u}
  ,{ 99u}
  ,{ 100u}
  ,{ 101u}
  ,{ 102u}
  ,{ 103u}
  ,{ 104u}
  ,{ 105u}
  ,{ 106u}
  ,{ 107u}
  ,{ 108u}
  ,{ 109u}
  ,{ 110u}
  ,{ 111u}
  ,{ 112u}
  ,{ 113u}
  ,{ 114u}
  ,{ 115u}
  ,{ 116u}
  ,{ 117u}
  ,{ 118u}
  ,{ 119u}
  ,{ 120u}
  ,{ 121u}
  ,{ 122u}
  ,{ 123u}
  ,{ 124u}
  ,{ 125u}
  ,{ 126u}
  ,{ 127u}
  ,{ 128u}
  ,{ 129u}
  ,{ 130u}
  ,{ 131u}
  ,{ 132u}
  ,{ 133u}
  ,{ 134u}
  ,{ 135u}
  ,{ 136u}
  ,{ 137u}
  ,{ 138u}
  ,{ 139u}
  ,{ 140u}
  ,{ 141u}
  ,{ 142u}
  ,{ 143u}
  ,{ 144u}
  ,{ 145u}
  ,{ 146u}
  ,{ 147u}
  ,{ 148u}
  ,{ 149u}
  ,{ 150u}
  ,{ 151u}
  ,{ 152u}
  ,{ 153u}
  ,{ 154u}
  ,{ 155u}
  ,{ 156u}
  ,{ 157u}
  ,{ 158u}
  ,{ 159u}
  ,{ 160u}
  ,{ 161u}
  ,{ 162u}
  ,{ 163u}
  ,{ 164u}
  ,{ 165u}
  ,{ 166u}
  ,{ 167u}
  ,{ 168u}
  ,{ 169u}
  ,{ 170u}
  ,{ 171u}
  ,{ 172u}
  ,{ 173u}
  ,{ 174u}
  ,{ 175u}
  ,{ 176u}
  ,{ 177u}
  ,{ 178u}
  ,{ 179u}
  ,{ 180u}
  ,{ 181u}
  ,{ 182u}
  ,{ 183u}
};
/*! DID signal operation classes */
CONST(Dcm_CfgDidMgrSignalOpClassInfoType, DCM_CONST) Dcm_CfgDidMgrSignalOpClassInfo[183]=
{
   { ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_CHANO_Data_CHANO_ReadData)),  16u,  16u,0x0001u} /* DID: 0x0100 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1Q82_Data_P1Q82_ReadData)),  64u,  64u,0x0001u} /* DID: 0x0104 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1AFT_Data_P1AFT_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1100 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1AFR_Data_P1AFR_ReadData)),   2u,   2u,0x0001u} /* DID: 0x1102 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1AFS_Data_P1AFS_ReadData)),   4u,   4u,0x0001u} /* DID: 0x1104 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1B0T_Data_P1B0T_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1207 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1DCT_Data_P1DCT_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1208 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1DCU_Data_P1DCU_ReadData)),   5u,   5u,0x0001u} /* DID: 0x1209 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1CXF_Data_P1CXF_ReadData)),   1u,   1u,0x0001u} /* DID: 0x120A */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1DJ8_Data_P1DJ8_ReadData)),  12u,  12u,0x0001u} /* DID: 0x120D */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BW0_Data_P1BW0_ReadData)),   8u,   8u,0x0001u} /* DID: 0x120E */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BW1_Data_P1BW1_ReadData)),   8u,   8u,0x0001u} /* DID: 0x120F */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BW3_Data_P1BW3_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1211 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BW4_Data_P1BW4_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1212 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BW5_Data_P1BW5_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1213 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BW6_Data_P1BW6_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1214 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BW8_Data_P1BW8_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1216 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BW9_Data_P1BW9_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1217 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWI_Data_P1BWI_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1218 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWJ_Data_P1BWJ_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1219 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWL_Data_P1BWL_ReadData)),   8u,   8u,0x0001u} /* DID: 0x121B */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWM_Data_P1BWM_ReadData)),   8u,   8u,0x0001u} /* DID: 0x121C */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWN_Data_P1BWN_ReadData)),   8u,   8u,0x0001u} /* DID: 0x121D */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWO_Data_P1BWO_ReadData)),   8u,   8u,0x0001u} /* DID: 0x121E */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWQ_Data_P1BWQ_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1220 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWR_Data_P1BWR_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1221 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWV_Data_P1BWV_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1225 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BWX_Data_P1BWX_ReadData)),   8u,   8u,0x0001u} /* DID: 0x1227 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BXD_Data_P1BXD_ReadData)),   8u,   8u,0x0001u} /* DID: 0x122C */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1BXF_Data_P1BXF_ReadData)),   8u,   8u,0x0001u} /* DID: 0x122E */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1DS3_Data_P1DS3_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1232 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EIJ_Data_P1EIJ_ReadData)),   1u,   1u,0x0001u} /* DID: 0x123B */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EIJ_Data_P1EIJ_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x123B */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EIJ_Data_P1EIJ_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x123B */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EIJ_Data_P1EIJ_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x123B */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOQ_Data_P1EOQ_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1244 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOQ_Data_P1EOQ_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x1244 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOQ_Data_P1EOQ_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x1244 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOQ_Data_P1EOQ_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x1244 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOR_Data_P1EOR_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1245 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOR_Data_P1EOR_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x1245 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOR_Data_P1EOR_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x1245 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOR_Data_P1EOR_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x1245 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOT_Data_P1EOT_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1246 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOT_Data_P1EOT_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x1246 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOT_Data_P1EOT_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x1246 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOT_Data_P1EOT_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x1246 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOV_Data_P1EOV_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1247 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOV_Data_P1EOV_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x1247 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOV_Data_P1EOV_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x1247 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOV_Data_P1EOV_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x1247 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOW_Data_P1EOW_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1248 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOW_Data_P1EOW_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x1248 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOW_Data_P1EOW_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x1248 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOW_Data_P1EOW_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x1248 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1C1R_Data_P1C1R_ReadData)),   6u,   6u,0x0001u} /* DID: 0x1250 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOS_Data_P1EOS_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1253 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOS_Data_P1EOS_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x1253 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOS_Data_P1EOS_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x1253 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOS_Data_P1EOS_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x1253 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOU_Data_P1EOU_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1254 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOU_Data_P1EOU_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x1254 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOU_Data_P1EOU_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x1254 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1EOU_Data_P1EOU_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x1254 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1FDL_Data_P1FDL_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1256 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1FM6_Data_P1FM6_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1257 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1DVZ_Data_P1DVZ_ReadData)),   2u,   2u,0x0001u} /* DID: 0x1258 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1GCM_Data_P1GCM_ReadData)), 120u, 120u,0x0001u} /* DID: 0x125A */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1ILR_Data_P1ILR_ReadData)),   5u,   5u,0x0001u} /* DID: 0x125C */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1KAO_Data_P1KAO_ReadData)), 256u, 256u,0x0001u} /* DID: 0x125E */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1KAO_Data_P1KAO_WriteData)), 256u, 256u,0x1001u} /* DID: 0x125E */                               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1OLT_Data_P1OLT_ReadData)),  64u,  64u,0x0001u} /* DID: 0x1276 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VKK_Data_P1VKK_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1277 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1QXI_Data_P1QXI_ReadData)),   1u,   1u,0x0001u} /* DID: 0x127A */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1QXJ_Data_P1QXJ_ReadData)),   4u,   4u,0x0001u} /* DID: 0x127B */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1QXM_Data_P1QXM_ReadData)),   2u,   2u,0x0001u} /* DID: 0x127C */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1QXP_Data_P1QXP_ReadData)),   2u,   2u,0x0001u} /* DID: 0x127D */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1QXR_Data_P1QXR_ReadData)),   2u,   2u,0x0001u} /* DID: 0x127E */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1QXU_Data_P1QXU_ReadData)),   1u,   1u,0x0001u} /* DID: 0x127F */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1RG1_Data_P1RG1_ReadData)),   4u,   4u,0x0001u} /* DID: 0x1280 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VQ1_Data_P1VQ1_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1282 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VQ2_Data_P1VQ2_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1283 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VQ3_Data_P1VQ3_ReadData)),   4u,   4u,0x0001u} /* DID: 0x1284 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VQ4_Data_P1VQ4_ReadData)),   4u,   4u,0x0001u} /* DID: 0x1285 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VQ5_Data_P1VQ5_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1286 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VQ6_Data_P1VQ6_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1287 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VQ7_Data_P1VQ7_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1288 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VQ8_Data_P1VQ8_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1289 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VQ9_Data_P1VQ9_ReadData)),   1u,   1u,0x0001u} /* DID: 0x128A */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRA_Data_P1VRA_ReadData)),   1u,   1u,0x0001u} /* DID: 0x128B */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRB_Data_P1VRB_ReadData)),   1u,   1u,0x0001u} /* DID: 0x128C */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRC_Data_P1VRC_ReadData)),   1u,   1u,0x0001u} /* DID: 0x128D */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRD_Data_P1VRD_ReadData)),   1u,   1u,0x0001u} /* DID: 0x128E */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRE_Data_P1VRE_ReadData)),   1u,   1u,0x0001u} /* DID: 0x128F */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRF_Data_P1VRF_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1290 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRG_Data_P1VRG_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1291 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRH_Data_P1VRH_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1292 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRI_Data_P1VRI_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1293 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRJ_Data_P1VRJ_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1294 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRK_Data_P1VRK_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1295 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRL_Data_P1VRL_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1296 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRM_Data_P1VRM_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1297 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRN_Data_P1VRN_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1298 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRO_Data_P1VRO_ReadData)),   1u,   1u,0x0001u} /* DID: 0x1299 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRS_Data_P1VRS_ReadData)),   4u,   4u,0x0001u} /* DID: 0x129D */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRT_Data_P1VRT_ReadData)),   4u,   4u,0x0001u} /* DID: 0x129E */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRU_Data_P1VRU_ReadData)),   4u,   4u,0x0001u} /* DID: 0x129F */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRV_Data_P1VRV_ReadData)),   1u,   1u,0x0001u} /* DID: 0x12A0 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRW_Data_P1VRW_ReadData)),   1u,   1u,0x0001u} /* DID: 0x12A1 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRZ_Data_P1VRZ_ReadData)),   1u,   1u,0x0001u} /* DID: 0x12A2 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRZ_Data_P1VRZ_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x12A2 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRZ_Data_P1VRZ_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x12A2 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VRZ_Data_P1VRZ_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x12A2 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR0_Data_P1VR0_ReadData)),   4u,   4u,0x0001u} /* DID: 0x12A3 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR0_Data_P1VR0_ReturnControlToECU)),   4u,   4u,0x2000u} /* DID: 0x12A3 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR0_Data_P1VR0_FreezeCurrentState)),   4u,   4u,0x2000u} /* DID: 0x12A3 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR0_Data_P1VR0_ShortTermAdjustment)),   4u,   4u,0x2001u} /* DID: 0x12A3 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR4_Data_P1VR4_ReadData)),   4u,   4u,0x0001u} /* DID: 0x12A4 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR4_Data_P1VR4_ReturnControlToECU)),   4u,   4u,0x2000u} /* DID: 0x12A4 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR4_Data_P1VR4_FreezeCurrentState)),   4u,   4u,0x2000u} /* DID: 0x12A4 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR4_Data_P1VR4_ShortTermAdjustment)),   4u,   4u,0x2001u} /* DID: 0x12A4 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR6_Data_P1VR6_ReadData)),   1u,   1u,0x0001u} /* DID: 0x12A5 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR6_Data_P1VR6_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x12A5 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR6_Data_P1VR6_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x12A5 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR6_Data_P1VR6_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x12A5 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR7_Data_P1VR7_ReadData)),   1u,   1u,0x0001u} /* DID: 0x12A6 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR7_Data_P1VR7_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x12A6 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR7_Data_P1VR7_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x12A6 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR7_Data_P1VR7_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x12A6 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR8_Data_P1VR8_ReadData)),   1u,   1u,0x0001u} /* DID: 0x12A7 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR8_Data_P1VR8_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x12A7 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR8_Data_P1VR8_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x12A7 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR8_Data_P1VR8_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x12A7 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR9_Data_P1VR9_ReadData)),   1u,   1u,0x0001u} /* DID: 0x12A8 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR9_Data_P1VR9_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x12A8 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR9_Data_P1VR9_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x12A8 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VR9_Data_P1VR9_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x12A8 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSB_Data_P1VSB_ReadData)),   4u,   4u,0x0001u} /* DID: 0x12A9 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSB_Data_P1VSB_ReturnControlToECU)),   4u,   4u,0x2000u} /* DID: 0x12A9 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSB_Data_P1VSB_FreezeCurrentState)),   4u,   4u,0x2000u} /* DID: 0x12A9 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSB_Data_P1VSB_ShortTermAdjustment)),   4u,   4u,0x2001u} /* DID: 0x12A9 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSC_Data_P1VSC_ReadData)),   4u,   4u,0x0001u} /* DID: 0x12AA */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSC_Data_P1VSC_ReturnControlToECU)),   4u,   4u,0x2000u} /* DID: 0x12AA */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSC_Data_P1VSC_FreezeCurrentState)),   4u,   4u,0x2000u} /* DID: 0x12AA */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSC_Data_P1VSC_ShortTermAdjustment)),   4u,   4u,0x2001u} /* DID: 0x12AA */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSD_Data_P1VSD_ReadData)),   4u,   4u,0x0001u} /* DID: 0x12AB */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSD_Data_P1VSD_ReturnControlToECU)),   4u,   4u,0x2000u} /* DID: 0x12AB */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSD_Data_P1VSD_FreezeCurrentState)),   4u,   4u,0x2000u} /* DID: 0x12AB */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSD_Data_P1VSD_ShortTermAdjustment)),   4u,   4u,0x2001u} /* DID: 0x12AB */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSG_Data_P1VSG_ReadData)),   1u,   1u,0x0001u} /* DID: 0x12AC */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSG_Data_P1VSG_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0x12AC */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSG_Data_P1VSG_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0x12AC */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VSG_Data_P1VSG_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0x12AC */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1VKH_Data_P1VKH_ReadData)),   5u,   5u,0x0001u} /* DID: 0x12AD */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1CV5_Data_X1CV5_ReadData)),   1u,   1u,0x0001u} /* DID: 0xE002 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1CV7_Data_X1CV7_ReadData)),   1u,   1u,0x0001u} /* DID: 0xE003 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1CV7_Data_X1CV7_ReturnControlToECU)),   1u,   1u,0x2000u} /* DID: 0xE003 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1CV7_Data_X1CV7_FreezeCurrentState)),   1u,   1u,0x2000u} /* DID: 0xE003 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1CV7_Data_X1CV7_ShortTermAdjustment)),   1u,   1u,0x2001u} /* DID: 0xE003 */                     /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1C1U_Data_X1C1U_ReadData)), 126u, 126u,0x0001u} /* DID: 0xE009 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1C1U_Data_X1C1U_WriteData)), 126u, 126u,0x1001u} /* DID: 0xE009 */                               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1C1Z_Data_X1C1Z_ReadData)),  60u,  60u,0x0001u} /* DID: 0xE00A */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1C12_Data_X1C12_ReadData)),  80u,  80u,0x0001u} /* DID: 0xE00B */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1C12_Data_X1C12_WriteData)),  80u,  80u,0x1001u} /* DID: 0xE00B */                               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1C13_Data_X1C13_ReadData)), 130u, 130u,0x0001u} /* DID: 0xE00C */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1C13_Data_X1C13_WriteData)), 130u, 130u,0x1001u} /* DID: 0xE00C */                               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1CY4_Data_X1CY4_ReadData)),  40u,  40u,0x0001u} /* DID: 0xE00D */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_X1CY4_Data_X1CY4_WriteData)),  40u,  40u,0x1001u} /* DID: 0xE00D */                               /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1B1O_Data_P1B1O_ReadData)), 221u, 221u,0x0001u} /* DID: 0xF180 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1ALQ_Data_P1ALQ_ReadData)),   0u, 241u,0x0001u} /* DID: 0xF181 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1ALQ_Data_P1ALQ_ReadDataLength)),   0u,   0u,0x0101u} /* DID: 0xF181 */                          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1ALP_Data_P1ALP_ReadData)),   0u, 241u,0x0001u} /* DID: 0xF182 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1ALP_Data_P1ALP_ReadDataLength)),   0u,   0u,0x0101u} /* DID: 0xF182 */                          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(DcmAddOn_Data_Application_Software_Fingerprint_ReadData)),   0u,  86u,0x0002u} /* DID: 0xF184 */                        /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(DcmAddOn_Data_Application_Software_Fingerprint_ReadDataLength)),   0u,   0u,0x0102u} /* DID: 0xF184 */                  /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(DcmAddOn_Data_Application_Data_Fingerprint_ReadData)),   0u,  86u,0x0002u} /* DID: 0xF185 */                            /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(DcmAddOn_Data_Application_Data_Fingerprint_ReadDataLength)),   0u,   0u,0x0102u} /* DID: 0xF185 */                      /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(DcmAddOn_Data_Application_Data_Fingerprint_WriteData)),   0u,  86u,0x1004u} /* DID: 0xF185 */                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1DIH_Data_P1DIH_ReadData)),   1u,   1u,0x0001u} /* DID: 0xF186 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_VINNO_Data_VINNO_ReadData)),  17u,  17u,0x0001u} /* DID: 0xF190 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1ALA_Data_P1ALA_ReadData)),   0u, 406u,0x0002u} /* DID: 0xF191 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1ALA_Data_P1ALA_ReadDataLength)),   0u,   0u,0x0102u} /* DID: 0xF191 */                          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_DidMgrOpFuncType)(Rte_Call_DataServices_P1ALB_Data_P1ALB_ReadData)),   8u,   8u,0x0001u} /* DID: 0xF197 */                                /* PRQA S 0313 */ /* MD_Dcm_0313 */
};
/*! IO control DID operation properties */
CONST(Dcm_CfgDidMgrOpInfoIoControlType, DCM_CONST) Dcm_CfgDidMgrOpInfoIoControl[20]=
{
   {   32u,   6u} /* DID: 0x123B */
  ,{   36u,   6u} /* DID: 0x1244 */
  ,{   40u,   6u} /* DID: 0x1245 */
  ,{   44u,   6u} /* DID: 0x1246 */
  ,{   48u,   6u} /* DID: 0x1247 */
  ,{   52u,   6u} /* DID: 0x1248 */
  ,{   57u,   6u} /* DID: 0x1253 */
  ,{   61u,   6u} /* DID: 0x1254 */
  ,{  110u,   6u} /* DID: 0x12A2 */
  ,{  114u,   6u} /* DID: 0x12A3 */
  ,{  118u,   6u} /* DID: 0x12A4 */
  ,{  122u,   6u} /* DID: 0x12A5 */
  ,{  126u,   6u} /* DID: 0x12A6 */
  ,{  130u,   6u} /* DID: 0x12A7 */
  ,{  134u,   6u} /* DID: 0x12A8 */
  ,{  138u,   6u} /* DID: 0x12A9 */
  ,{  142u,   6u} /* DID: 0x12AA */
  ,{  146u,   6u} /* DID: 0x12AB */
  ,{  150u,   6u} /* DID: 0x12AC */
  ,{  156u,  14u} /* DID: 0xE003 */
};
/*! RID properties */
CONST(Dcm_CfgRidMgrRidInfoType, DCM_CONST) Dcm_CfgRidMgrRidInfo[5]=
{
   {    0u,  14u,0x07u, 0u} /* RID: 0x0400 */
  ,{    3u,  14u,0x07u, 0u} /* RID: 0x0401 */
  ,{    6u,  19u,0x07u, 0u} /* RID: 0x0402 */
  ,{    9u,  14u,0x01u, 0u} /* RID: 0xE007 */
  ,{   10u,  14u,0x01u, 0u} /* RID: 0xE00E */
};
/*! RID operation properties */
CONST(Dcm_CfgRidMgrOpInfoType, DCM_CONST) Dcm_CfgRidMgrOpInfo[11]=
{
   { ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_R1AAJ_Start)),   0u,   0u,   2u,   2u, 3u} /* RID: 0x0400 */                                   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_R1AAJ_Stop)),   0u,   0u,   0u,   0u, 0u} /* RID: 0x0400 */                                    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_R1AAJ_RequestResults)),   0u,   0u,   2u,   2u, 3u} /* RID: 0x0400 */                          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Dcm_RidMgr_0401_Start)),   1u,   1u,   2u,   2u, 9u} /* RID: 0x0401 */                                                  /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_R1AAI_Stop)),   0u,   0u,   0u,   0u, 0u} /* RID: 0x0401 */                                    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_R1AAI_RequestResults)),   0u,   0u,   7u,   7u, 3u} /* RID: 0x0401 */                          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_R1AAA_Start)),   0u,   0u,   2u,   2u, 3u} /* RID: 0x0402 */                                   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_R1AAA_Stop)),   0u,   0u,   0u,   0u, 0u} /* RID: 0x0402 */                                    /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_R1AAA_RequestResults)),   0u,   0u,   2u,   2u, 3u} /* RID: 0x0402 */                          /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Y1ABD_Start)),   4u,   4u,   3u,   3u, 2u} /* RID: 0xE007 */                                   /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_RidMgrOpFuncType)(Rte_Call_RoutineServices_Y1ABE_Start)),   0u,   0u,   2u,   2u, 3u} /* RID: 0xE00E */                                   /* PRQA S 0313 */ /* MD_Dcm_0313 */
};
/*! Properties of the MIDs */
CONST(Dcm_CfgMemMgrMemIdInfoType, DCM_CONST) Dcm_CfgMemMgrMidInfo[1]=
{
   { Dcm_CfgMemMgrMemMap,   6u}
};
/*! Properties of the memory map of a specific MID */
CONST(Dcm_CfgMemMgrMemMapInfoType, DCM_CONST) Dcm_CfgMemMgrMemMap[6]=
{
   { {   21u,  21u},0x00F9C100UL,0x00F9C4FFUL}
  ,{ {   22u,  22u},0x00F9C500UL,0x00F9C8FFUL}
  ,{ {   23u,  22u},0x00F9C900UL,0x00F9CCFFUL}
  ,{ {   24u,  24u},0x00F9CD00UL,0x00F9D4FFUL}
  ,{ {   25u,  24u},0x00F9D500UL,0x00F9D50FUL}
  ,{ {   26u,  26u},0x00F9D600UL,0x00F9D9FFUL}
};
/*! DCM service initializers */
CONST(Dcm_DiagSvcInitFuncType, DCM_CONST) Dcm_CfgDiagSvcInitializers[3]=
{
   Dcm_Service27Init
  ,Dcm_Service2FInit
  ,NULL_PTR /* end marker */
};
/*! DCM service properties */
CONST(Dcm_CfgDiagServiceInfoType, DCM_CONST) Dcm_CfgDiagServiceInfo[16]=
{
   { Dcm_Service10Processor,0x01u, 1u,   1u,   2u, 0u, 0u} /* SID: 0x10 */
  ,{ Dcm_Service11Processor,0x01u, 1u,   3u,   4u, 0u, 0u} /* SID: 0x11 */
  ,{ Dcm_Service14Processor,0x00u, 3u,   0u,   0u, 0u, 0u} /* SID: 0x14 */
  ,{ Dcm_Service19Processor,0x01u, 1u,   5u,   0u, 2u, 0u} /* SID: 0x19 */
  ,{ Dcm_Service22Processor,0x00u, 2u,   0u,   0u, 0u, 0u} /* SID: 0x22 */
  ,{ Dcm_Service23Processor,0x00u, 3u,   0u,   0u, 0u, 0u} /* SID: 0x23 */
  ,{ Dcm_Service27Processor,0x03u, 1u,   6u,   0u, 0u, 0u} /* SID: 0x27 */
  ,{ Dcm_Service28Processor,0x01u, 1u,   7u,   0u, 0u, 0u} /* SID: 0x28 */
  ,{ Dcm_Service2EProcessor,0x00u, 3u,   0u,   0u, 0u, 0u} /* SID: 0x2E */
  ,{ Dcm_Service2FProcessor,0x00u, 3u,   8u,   0u, 0u, 0u} /* SID: 0x2F */
  ,{ Dcm_Service31Processor,0x01u, 3u,   0u,   0u, 0u, 0u} /* SID: 0x31 */
  ,{ Dcm_Service3DProcessor,0x00u, 4u,   0u,   0u, 0u, 0u} /* SID: 0x3D */
  ,{ Dcm_Service3EProcessor,0x01u, 1u,   0u,   0u, 0u, 0u} /* SID: 0x3E */
  ,{ Dcm_Service85Processor,0x01u, 1u,   9u,   0u, 0u, 0u} /* SID: 0x85 */
  ,{ Dcm_SvcWrapper_DcmAddOn_Service87_Processor,0x01u, 0u,   0u,   0u, 0u, 0u} /* SID: 0x87 */
  ,{ Dcm_RepeaterDeadEnd,0x00u, 0u,   0u,   0u, 0u, 0u} /* Dcm_RepeaterDeadEnd */
};
/*! Indirection from diag service info to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgDiagSvcIdExecPrecondTable[15]=
{
      0u /* SID: 0x10 */
  ,   0u /* SID: 0x11 */
  ,   1u /* SID: 0x14 */
  ,   1u /* SID: 0x19 */
  ,   1u /* SID: 0x22 */
  ,   2u /* SID: 0x23 */
  ,   3u /* SID: 0x27 */
  ,   4u /* SID: 0x28 */
  ,   5u /* SID: 0x2E */
  ,   6u /* SID: 0x2F */
  ,   7u /* SID: 0x31 */
  ,   8u /* SID: 0x3D */
  ,   1u /* SID: 0x3E */
  ,   4u /* SID: 0x85 */
  ,   4u /* SID: 0x87 */
};
/*! DCM service post processors */
CONST(Dcm_DiagSvcConfirmationFuncType, DCM_CONST) Dcm_CfgDiagSvcPostProcessors[10]=
{
   Dcm_ServiceNoPostProcessor
  ,Dcm_Service10PostProcessor
  ,Dcm_Service10FastPostProcessor
  ,Dcm_Service11PostProcessor
  ,Dcm_Service11FastPostProcessor
  ,Dcm_Service19PostProcessor
  ,Dcm_Service27PostProcessor
  ,Dcm_Service28PostProcessor
  ,Dcm_Service2FPostProcessor
  ,Dcm_Service85PostProcessor
};
/*! DCM service paged buffer updater */
CONST(Dcm_DiagSvcUpdateFuncType, DCM_CONST) Dcm_CfgDiagSvcUpdaters[3]=
{
   Dcm_ServiceNoUpdater
  ,Dcm_PagedBufferDataPadding
  ,Dcm_Service19Updater
};
/*! DCM service paged buffer canceller */
CONST(Dcm_DiagSvcCancelFuncType, DCM_CONST) Dcm_CfgDiagSvcCancellers[1]=
{
   Dcm_ServiceNoCancel
};
/*! OEM notification functions */
CONST(Dcm_CfgDiagNotificationInfoType, DCM_CONST) Dcm_CfgDiagOemNotificationInfo[2]=
{
   { Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication,Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation}
  ,{ NULL_PTR,NULL_PTR}
};
/*! Service 0x10 sub-service properties table  */
CONST(Dcm_CfgSvc10SubFuncInfoType, DCM_CONST) Dcm_CfgSvc10SubFuncInfo[3]=
{
   { { 50u, 500u}, 0u} /* Session ID: 0x01 */
  ,{ { 50u, 500u}, 1u} /* Session ID: 0x02 */
  ,{ { 50u, 500u}, 0u} /* Session ID: 0x03 */
};
/*! Indirection from service 0x10 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc10SubFuncExecPrecondTable[3]=
{
      1u /* Session ID: 0x01 */
  ,   0u /* Session ID: 0x02 */
  ,   1u /* Session ID: 0x03 */
};
/*! Service 0x11 sub-service properties table  */
CONST(Dcm_CfgSvc11SubFuncInfoType, DCM_CONST) Dcm_CfgSvc11SubFuncInfo[2]=
{
   { Dcm_Service11_01Processor} /* SF: 0x01 */
  ,{ Dcm_Service11_02Processor} /* SF: 0x02 */
};
/*! Indirection from service 0x11 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc11SubFuncExecPrecondTable[2]=
{
      0u /* SF: 0x01 */
  ,   1u /* SF: 0x02 */
};
/*! Service 0x19 sub-service properties table  */
CONST(Dcm_CfgSvc19SubFuncInfoType, DCM_CONST) Dcm_CfgSvc19SubFuncInfo[6]=
{
   { Dcm_Service19_01Processor, 2u} /* SF: 0x01 */
  ,{ Dcm_Service19_02Processor, 2u} /* SF: 0x02 */
  ,{ Dcm_Service19_03Processor, 1u} /* SF: 0x03 */
  ,{ Dcm_Service19_04Processor, 5u} /* SF: 0x04 */
  ,{ Dcm_Service19_06Processor, 5u} /* SF: 0x06 */
  ,{ Dcm_Service19_0AProcessor, 1u} /* SF: 0x0A */
};
/*! Indirection from service 0x19 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc19SubFuncExecPrecondTable[6]=
{
      1u /* SF: 0x01 */
  ,   1u /* SF: 0x02 */
  ,   1u /* SF: 0x03 */
  ,   0u /* SF: 0x04 */
  ,   1u /* SF: 0x06 */
  ,   1u /* SF: 0x0A */
};
/*! Service 0x27 sub-service properties table  */
CONST(Dcm_CfgSvc27SubFuncInfoType, DCM_CONST) Dcm_CfgSvc27SubFuncInfo[34]=
{
   {    1u} /* SF: 0x01 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x02 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x07 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x08 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x09 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x0A */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x0B */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x0C */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x0D */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x0E */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x0F */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x10 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x11 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x12 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x15 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x16 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x17 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x18 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x1B */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x1C */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x29 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x2A */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x2B */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x2C */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x2D */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x2E */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x2F */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x30 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x31 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x32 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x33 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x34 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{    1u} /* SF: 0x37 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{  129u} /* SF: 0x38 */                                                                                                                           /* PRQA S 0313 */ /* MD_Dcm_0313 */
};
/*! Service 0x27 security level properties table  */
CONST(Dcm_CfgSvc27SecLevelInfoType, DCM_CONST) Dcm_CfgSvc27SecLevelInfo[17]=
{
   { ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_01_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_01_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_01 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_07_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_07_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_07 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_09_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_09_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_09 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_0B_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_0B_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_0B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_0D_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_0D_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_0D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_0F_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_0F_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_0F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_11_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_11_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_11 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_15_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_15_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_15 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_17_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_17_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_17 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_1B_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_1B_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_1B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_29_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_29_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_29 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_2B_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_2B_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_2B */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_2D_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_2D_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_2D */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_2F_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_2F_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_2F */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_31_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_31_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_31 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_33_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_33_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_33 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
  ,{ ((Dcm_Svc27GetSeedFuncType)(Rte_Call_SecurityAccess_SA_Seed_37_GetSeed)),Rte_Call_SecurityAccess_SA_Seed_37_CompareKey, 116u, 0u} /* SecLvl: SA_Seed_37 */ /* PRQA S 0313 */ /* MD_Dcm_0313 */
};
/*! Indirection from service 0x27 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc27SubFuncExecPrecondTable[34]=
{
      3u /* SF: 0x01 */
  ,   3u /* SF: 0x02 */
  ,   4u /* SF: 0x07 */
  ,   4u /* SF: 0x08 */
  ,   4u /* SF: 0x09 */
  ,   4u /* SF: 0x0A */
  ,   4u /* SF: 0x0B */
  ,   4u /* SF: 0x0C */
  ,   3u /* SF: 0x0D */
  ,   3u /* SF: 0x0E */
  ,   4u /* SF: 0x0F */
  ,   4u /* SF: 0x10 */
  ,   4u /* SF: 0x11 */
  ,   4u /* SF: 0x12 */
  ,   4u /* SF: 0x15 */
  ,   4u /* SF: 0x16 */
  ,   3u /* SF: 0x17 */
  ,   3u /* SF: 0x18 */
  ,   4u /* SF: 0x1B */
  ,   4u /* SF: 0x1C */
  ,   4u /* SF: 0x29 */
  ,   4u /* SF: 0x2A */
  ,   4u /* SF: 0x2B */
  ,   4u /* SF: 0x2C */
  ,   4u /* SF: 0x2D */
  ,   4u /* SF: 0x2E */
  ,   4u /* SF: 0x2F */
  ,   4u /* SF: 0x30 */
  ,   4u /* SF: 0x31 */
  ,   4u /* SF: 0x32 */
  ,   4u /* SF: 0x33 */
  ,   4u /* SF: 0x34 */
  ,   4u /* SF: 0x37 */
  ,   4u /* SF: 0x38 */
};
/*! Service 0x28 sub-service properties table  */
CONST(Dcm_CfgSvc28SubFuncInfoType, DCM_CONST) Dcm_CfgSvc28SubFuncInfo[2]=
{
   { Dcm_Service28_XXProcessor, 2u} /* SF: 0x00 */
  ,{ Dcm_Service28_XXProcessor, 2u} /* SF: 0x01 */
};
/*! Indirection from service 0x28 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc28SubFuncExecPrecondTable[2]=
{
      4u /* SF: 0x00 */
  ,   4u /* SF: 0x01 */
};
/*! Service 0x28 network ID to ComM channel map */
CONST(NetworkHandleType, DCM_CONST) Dcm_CfgSvc28SubNetIdMap[2]=
{
   DCM_SVC_28_NETWORK_ALL
  ,DCM_SVC_28_NETWORK_CURRENT
};
/*! Indirection from service 0x3E sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc3ESubFuncExecPrecondTable[1]=
{
      1u /* SF: 0x00 */
};
/*! Indirection from service 0x85 sub functions to execution pre conditions */
CONST(Dcm_CfgStateRefMemType, DCM_CONST) Dcm_CfgSvc85SubFuncExecPrecondTable[2]=
{
      4u /* SF: 0x01 */
  ,   4u /* SF: 0x02 */
};
/*! DCM service 0x85 properties */
CONST(Dcm_CfgSvc85SubFuncInfoType, DCM_CONST) Dcm_CfgSvc85SubFuncInfo[2]=
{
   { Dem_EnableDTCSetting,RTE_MODE_DcmControlDtcSetting_ENABLEDTCSETTING} /* SF: 0x01 */
  ,{ Dem_DisableDTCSetting,RTE_MODE_DcmControlDtcSetting_DISABLEDTCSETTING} /* SF: 0x02 */
};
#define DCM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   Module internal API function implementations
---------------------------------------------- */
#define DCM_START_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/***********************************************************************************************************************
 *  Dcm_ModeOnComControlModeChange()
***********************************************************************************************************************/
/* Implements CDD Dcm_ModeOnComControlModeChange() */
FUNC(void, DCM_CODE) Dcm_ModeOnComControlModeChange(NetworkHandleType channelId, Dcm_CommunicationModeType mode)
{
  switch(channelId)
  {
    case ComMConf_ComMChannel_CN_Backbone2_78967e2c:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c(mode)));
      break;
    case ComMConf_ComMChannel_CN_CabSubnet_9ea693f1:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1(mode)));
      break;
    case ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54(mode)));
      break;
    case ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae(mode)));
      break;
    case ComMConf_ComMChannel_CN_FMSNet_fce1aae5:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5(mode)));
      break;
    case ComMConf_ComMChannel_CN_LIN00_2cd9a7df:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df(mode)));
      break;
    case ComMConf_ComMChannel_CN_LIN01_5bde9749:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749(mode)));
      break;
    case ComMConf_ComMChannel_CN_LIN02_c2d7c6f3:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3(mode)));
      break;
    case ComMConf_ComMChannel_CN_LIN03_b5d0f665:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665(mode)));
      break;
    case ComMConf_ComMChannel_CN_LIN04_2bb463c6:
      ((void)(Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6(mode)));
      break;
    default: /* Just exit the switch case */
      break;
  }
}
#define DCM_STOP_SEC_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ----------------------------------------------
 ~&&&   Module call-out implementations
---------------------------------------------- */
#define DCM_START_SEC_CALLOUT_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/***********************************************************************************************************************
 *  Dcm_RidMgr_0401_Start()
***********************************************************************************************************************/
/* Implements CDD Dcm_RidMgr<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_RidMgr_0401_Start(Dcm_OpStatusType OpStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_RidMgrRidLengthPtrType DataLength, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  uint8 dataInIn_Common_Diagnostics_DataRecord;

  dataInIn_Common_Diagnostics_DataRecord = ((uint8)(Dcm_DiagGetReqDataAsU8Rel(pMsgContext, 0u)))                                                     /* PRQA S 2985 */ /* MD_Dcm_Redundant_2985 */;

  DCM_IGNORE_UNREF_PARAM(DataLength);                                                                                                                /* PRQA S 3112 */ /* MD_Dcm_3112 */

  return Rte_Call_RoutineServices_R1AAI_Start(dataInIn_Common_Diagnostics_DataRecord
                                             , OpStatus
                                             , Dcm_DiagGetResDataRel(pMsgContext, 0u)                                                                /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */ /* PRQA S 2985 */ /* MD_Dcm_Redundant_2985 */
                                             , ErrorCode
                                             );                                                                                                      /* SBSW_DCM_GEN_COMB_PARAM_PTR_FORWARD */ /* SBSW_DCM_GEN_RID_WRAPPER */
}
/***********************************************************************************************************************
 *  Dcm_SvcWrapper_DcmAddOn_Service87_Processor()
***********************************************************************************************************************/
/* Implements CDD Dcm_SvcWrapper_<XXX>() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_SvcWrapper_DcmAddOn_Service87_Processor(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_MsgContextPtrType pMsgContext, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  Std_ReturnType lStdResult;
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(ErrorCode);                                                                                                                 /* PRQA S 3112 */ /* MD_Dcm_3112 */
  lStdResult = DcmAddOn_Service87_Processor(opStatus, pMsgContext);                                                                                  /* SBSW_DCM_GEN_PARAM_PTR_FORWARD */
  switch(lStdResult)
  {
    case DCM_E_PROCESSINGDONE:
    case DCM_E_PENDING:
    case DCM_E_FORCE_RCRRP:
      break;
    default:
      lStdResult = DCM_E_STOP_REPEATER;
      break;
  }
  return lStdResult;
}
/***********************************************************************************************************************
 *  Dcm_ServiceNoPostProcessor()
***********************************************************************************************************************/
/* Implements CDD Dcm_ServiceNoPostProcessor() */
DCM_LOCAL FUNC(void, DCM_CALLOUT_CODE) Dcm_ServiceNoPostProcessor(Dcm_ContextPtrType pContext, Dcm_ConfirmationStatusType status)                    /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(status);                                                                                                                    /* PRQA S 3112 */ /* MD_Dcm_3112 */
}
/***********************************************************************************************************************
 *  Dcm_ServiceNoUpdater()
***********************************************************************************************************************/
/* Implements CDD Dcm_ServiceNoUpdater() */
DCM_LOCAL FUNC(Std_ReturnType, DCM_CALLOUT_CODE) Dcm_ServiceNoUpdater(Dcm_ContextPtrType pContext, Dcm_OpStatusType opStatus, Dcm_DiagDataContextPtrType pDataContext, Dcm_NegativeResponseCodePtrType ErrorCode) /* PRQA S 3673 */ /* MD_Dcm_APIStd_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(opStatus);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(pDataContext);                                                                                                              /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(ErrorCode);                                                                                                                 /* PRQA S 3112 */ /* MD_Dcm_3112 */
  return E_NOT_OK;
}
/***********************************************************************************************************************
 *  Dcm_ServiceNoCancel()
***********************************************************************************************************************/
/* Implements CDD Dcm_ServiceNoCancel() */
DCM_LOCAL FUNC(void, DCM_CALLOUT_CODE) Dcm_ServiceNoCancel(Dcm_ContextPtrType pContext, Dcm_DiagDataContextPtrType pDataContext)                     /* PRQA S 3673 */ /* MD_Dcm_Design_3673 */
{
  DCM_IGNORE_UNREF_PARAM(pContext);                                                                                                                  /* PRQA S 3112 */ /* MD_Dcm_3112 */
  DCM_IGNORE_UNREF_PARAM(pDataContext);                                                                                                              /* PRQA S 3112 */ /* MD_Dcm_3112 */
  /* nothing to do */
}
#define DCM_STOP_SEC_CALLOUT_CODE
/*lint -save -esym(961, 19.1) */
#include "MemMap.h"                                                                                                                                  /* PRQA S 5087 */ /* MD_MSR_MemMap */
/*lint -restore */
/* ********************************************************************************************************************
 * END OF FILE: Dcm_Lcfg.c
 * ******************************************************************************************************************** */

