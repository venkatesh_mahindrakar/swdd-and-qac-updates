/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Hal_Entry_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:32
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

/* PRQA S 0777, 0779, 0828 EOF */ /* MD_MSR_Rule5.1, MD_MSR_Rule5.2, MD_MSR_Dir1.1 */

#define OS_HAL_ENTRY_LCFG_SOURCE

/**********************************************************************************************************************
 *  INCLUDES
 *********************************************************************************************************************/

/* AUTOSAR includes */
#include "Std_Types.h"

/* Os module declarations */
#include "Os_Hal_Entry_Cfg.h"
#include "Os_Hal_Entry_Lcfg.h"
#include "Os_Hal_Entry.h"

/* Os kernel module dependencies */
#include "Os_Isr_Lcfg.h"
#include "Os.h"

/* Os hal dependencies */
#include "Os_Hal_Cfg.h"
#include "Os_Hal_Core_Lcfg.h"
#include "Os_Hal_Interrupt_Cfg.h"
#include "Os_Hal_Trap.h"


/**********************************************************************************************************************
 *  LOCAL CONSTANT MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL FUNCTION MACROS
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  GLOBAL CONSTANT DATA
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  EXCEPTION VECTOR TABLE CORE 0
 *********************************************************************************************************************/

#define OS_START_SEC_EXCVEC_CORE0_CODE
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

OS_HAL_CODE_SECTION(OS_EXCVEC_CORE0_CODE)
Os_Hal_Asm("   .globl OS_EXCVEC_CORE0_CODE");  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_Asm("OS_EXCVEC_CORE0_CODE:");  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Unhandled_0)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Ivor1AsmHandler)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Data)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Instruction)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_ExternalInterrupt)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Unhandled_5)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Program)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Unhandled_7)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Trap)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Unhandled_9)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Unhandled_10)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_CoreException(Os_Hal_Exception_Unhandled_11)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */

#define OS_STOP_SEC_EXCVEC_CORE0_CODE
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */




/**********************************************************************************************************************
 *  INTERRUPT VECTOR TABLE CORE 0
 *********************************************************************************************************************/

#define OS_START_SEC_INTVEC_CODE
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

OS_HAL_CODE_SECTION(OS_INTVEC_CODE)
Os_Hal_Asm("   .globl OS_INTVEC_CODE");  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_Asm("OS_INTVEC_CODE:");  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */

Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt36)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt226)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt227)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt228)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt376)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt377)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt378)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt379)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt380)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt381)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt388)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt389)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt390)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt394)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt395)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt396)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt397)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt398)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt399)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt400)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt401)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt402)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt403)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt404)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt405)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt406)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt407)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt408)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt480)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt568)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt569)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt570)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt571)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt572)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt573)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt574)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt580)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt581)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt582)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt583)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt584)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt585)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt586)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt592)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt593)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt594)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt595)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt596)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt597)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt598)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt616)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt617)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt618)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt619)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt620)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt621)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt622)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt640)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt641)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt642)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt643)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt644)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt645)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt646)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt652)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt653)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt654)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt655)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt656)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt657)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt658)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt668)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt669)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt670)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt671)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt707)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt708)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt710)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt712)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Isr_Core0_Interrupt713)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Hal_ExternalInterrupt(Os_Irq_Unhandled)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */

#define OS_STOP_SEC_INTVEC_CODE
#include "Os_MemMap_OsSections.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#define OS_START_SEC_CODE
#include "Os_MemMap_OsCode.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt618, OsCfg_Isr_CanIsr_4_MB08To11)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt619, OsCfg_Isr_CanIsr_4_MB12To15)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt616, OsCfg_Isr_CanIsr_4_MB00To03)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt617, OsCfg_Isr_CanIsr_4_MB04To07)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt658, OsCfg_Isr_CanIsr_7_MB64To95)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt656, OsCfg_Isr_CanIsr_7_MB16To31)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt657, OsCfg_Isr_CanIsr_7_MB32To63)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt379, OsCfg_Isr_Lin_Channel_1_RXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt654, OsCfg_Isr_CanIsr_7_MB08To11)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt655, OsCfg_Isr_CanIsr_7_MB12To15)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt377, OsCfg_Isr_Lin_Channel_0_TXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt652, OsCfg_Isr_CanIsr_7_MB00To03)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt378, OsCfg_Isr_Lin_Channel_0_ERR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt653, OsCfg_Isr_CanIsr_7_MB04To07)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt573, OsCfg_Isr_CanIsr_0_MB32To63)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt376, OsCfg_Isr_Lin_Channel_0_RXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt574, OsCfg_Isr_CanIsr_0_MB64To95)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt571, OsCfg_Isr_CanIsr_0_MB12To15)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt572, OsCfg_Isr_CanIsr_0_MB16To31)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt570, OsCfg_Isr_CanIsr_0_MB08To11)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt36, OsCfg_Isr_CounterIsr_SystemTimer)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt407, OsCfg_Isr_Lin_Channel_10_TXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt408, OsCfg_Isr_Lin_Channel_10_ERR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt405, OsCfg_Isr_Lin_Channel_9_ERR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt406, OsCfg_Isr_Lin_Channel_10_RXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt403, OsCfg_Isr_Lin_Channel_9_RXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt568, OsCfg_Isr_CanIsr_0_MB00To03)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt645, OsCfg_Isr_CanIsr_6_MB32To63)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt404, OsCfg_Isr_Lin_Channel_9_TXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt569, OsCfg_Isr_CanIsr_0_MB04To07)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt646, OsCfg_Isr_CanIsr_6_MB64To95)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt401, OsCfg_Isr_Lin_Channel_8_TXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt643, OsCfg_Isr_CanIsr_6_MB12To15)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt402, OsCfg_Isr_Lin_Channel_8_ERR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt644, OsCfg_Isr_CanIsr_6_MB16To31)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt641, OsCfg_Isr_CanIsr_6_MB04To07)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt400, OsCfg_Isr_Lin_Channel_8_RXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt642, OsCfg_Isr_CanIsr_6_MB08To11)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt640, OsCfg_Isr_CanIsr_6_MB00To03)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt480, OsCfg_Isr_MCU_PLL_LossOfLock)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt713, OsCfg_Isr_DOWLS3_EMIOS0_CH14_ISR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt712, OsCfg_Isr_DOWLS2_EMIOS0_CH13_ISR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt710, OsCfg_Isr_DOWLS2_EMIOS0_CH9_ISR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt399, OsCfg_Isr_Lin_Channel_7_ERR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt597, OsCfg_Isr_CanIsr_2_MB32To63)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt598, OsCfg_Isr_CanIsr_2_MB64To95)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt397, OsCfg_Isr_Lin_Channel_7_RXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt595, OsCfg_Isr_CanIsr_2_MB12To15)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt398, OsCfg_Isr_Lin_Channel_7_TXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt596, OsCfg_Isr_CanIsr_2_MB16To31)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt395, OsCfg_Isr_Lin_Channel_6_TXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt593, OsCfg_Isr_CanIsr_2_MB04To07)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt670, OsCfg_Isr_WKUP_IRQ2)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt396, OsCfg_Isr_Lin_Channel_6_ERR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt594, OsCfg_Isr_CanIsr_2_MB08To11)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt671, OsCfg_Isr_WKUP_IRQ3)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt394, OsCfg_Isr_Lin_Channel_6_RXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt592, OsCfg_Isr_CanIsr_2_MB00To03)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt390, OsCfg_Isr_Lin_Channel_4_ERR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt708, OsCfg_Isr_DOWHS2_EMIOS0_CH5_ISR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt707, OsCfg_Isr_DOWHS1_EMIOS0_CH3_ISR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt669, OsCfg_Isr_WKUP_IRQ1)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt227, OsCfg_Isr_Gpt_PIT_0_TIMER_1_ISR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt228, OsCfg_Isr_Gpt_PIT_0_TIMER_2_ISR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt668, OsCfg_Isr_WKUP_IRQ0)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt621, OsCfg_Isr_CanIsr_4_MB32To63)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt226, OsCfg_Isr_Gpt_PIT_0_TIMER_0_ISR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt622, OsCfg_Isr_CanIsr_4_MB64To95)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt388, OsCfg_Isr_Lin_Channel_4_RXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt586, OsCfg_Isr_CanIsr_1_MB64To95)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt389, OsCfg_Isr_Lin_Channel_4_TXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt620, OsCfg_Isr_CanIsr_4_MB16To31)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt584, OsCfg_Isr_CanIsr_1_MB16To31)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt585, OsCfg_Isr_CanIsr_1_MB32To63)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt582, OsCfg_Isr_CanIsr_1_MB08To11)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt583, OsCfg_Isr_CanIsr_1_MB12To15)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt580, OsCfg_Isr_CanIsr_1_MB00To03)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt581, OsCfg_Isr_CanIsr_1_MB04To07)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt380, OsCfg_Isr_Lin_Channel_1_TXI)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */
Os_Isr_Cat2Entry(Os_Isr_Core0_Interrupt381, OsCfg_Isr_Lin_Channel_1_ERR)  /* PRQA S 0605 */ /* MD_Os_Hal_Rule1.1_0605_ASM */

#define OS_STOP_SEC_CODE
#include "Os_MemMap_OsCode.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */



/**********************************************************************************************************************
 *  END OF FILE: Os_Hal_Entry_Lcfg.c
 *********************************************************************************************************************/
