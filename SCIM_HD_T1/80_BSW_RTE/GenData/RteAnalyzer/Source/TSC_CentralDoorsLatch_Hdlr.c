/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CentralDoorsLatch_Hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CentralDoorsLatch_Hdlr.h"
#include "TSC_CentralDoorsLatch_Hdlr.h"








Std_ReturnType TSC_CentralDoorsLatch_Hdlr_Rte_Read_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T *data)
{
  return Rte_Read_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(data);
}

Std_ReturnType TSC_CentralDoorsLatch_Hdlr_Rte_Read_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T *data)
{
  return Rte_Read_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(data);
}

Std_ReturnType TSC_CentralDoorsLatch_Hdlr_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Security_SwcActivation_Security(data);
}




Std_ReturnType TSC_CentralDoorsLatch_Hdlr_Rte_Write_DriverDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T data)
{
  return Rte_Write_DriverDoorLatchInternal_stat_DoorLatch_stat(data);
}

Std_ReturnType TSC_CentralDoorsLatch_Hdlr_Rte_Write_PsngDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T data)
{
  return Rte_Write_PsngDoorLatchInternal_stat_DoorLatch_stat(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_CentralDoorsLatch_Hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef, AdiPinVoltage, BatteryVoltage, FaultStatus);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* CentralDoorsLatch_Hdlr */
      /* CentralDoorsLatch_Hdlr */



