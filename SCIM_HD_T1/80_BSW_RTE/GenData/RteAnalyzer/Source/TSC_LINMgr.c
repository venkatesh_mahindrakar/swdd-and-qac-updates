/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_LINMgr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_LINMgr.h"
#include "TSC_LINMgr.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_LINMgr_Rte_IrvRead_DataServices_P1CXF_Data_P1CXF_FCI_ReadData_Irv_DID_FCI(void)
{
return Rte_IrvRead_DataServices_P1CXF_Data_P1CXF_FCI_ReadData_Irv_DID_FCI();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Boolean TSC_LINMgr_Rte_IrvRead_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig(void)
{
return Rte_IrvRead_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig();
}

void TSC_LINMgr_Rte_IrvWrite_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig(Boolean data)
{
  Rte_IrvWrite_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_LINMgr_Rte_Read_LinDiagRequestFlag_CCNADRequest(uint8 *data)
{
  return Rte_Read_LinDiagRequestFlag_CCNADRequest(data);
}

Std_ReturnType TSC_LINMgr_Rte_Read_LinDiagRequestFlag_PNSNRequest(uint8 *data)
{
  return Rte_Read_LinDiagRequestFlag_PNSNRequest(data);
}

Std_ReturnType TSC_LINMgr_Rte_Read_SwcActivation_LIN_SwcActivation_LIN(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_LIN_SwcActivation_LIN(data);
}

Std_ReturnType TSC_LINMgr_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}

Std_ReturnType TSC_LINMgr_Rte_Read_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(Boolean *data)
{
  return Rte_Read_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(data);
}

Std_ReturnType TSC_LINMgr_Rte_Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(Boolean *data)
{
  return Rte_Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(data);
}




Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type data)
{
  return Rte_Write_ComMode_LIN1_ComMode_LIN(data);
}

Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type data)
{
  return Rte_Write_ComMode_LIN2_ComMode_LIN(data);
}

Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type data)
{
  return Rte_Write_ComMode_LIN3_ComMode_LIN(data);
}

Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type data)
{
  return Rte_Write_ComMode_LIN4_ComMode_LIN(data);
}

Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type data)
{
  return Rte_Write_ComMode_LIN5_ComMode_LIN(data);
}

Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN6_ComMode_LIN(ComMode_LIN_Type data)
{
  return Rte_Write_ComMode_LIN6_ComMode_LIN(data);
}

Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN7_ComMode_LIN(ComMode_LIN_Type data)
{
  return Rte_Write_ComMode_LIN7_ComMode_LIN(data);
}

Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN8_ComMode_LIN(ComMode_LIN_Type data)
{
  return Rte_Write_ComMode_LIN8_ComMode_LIN(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_LINMgr_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(SelectParkedOrLivingPin, IsDo12VActivated, Do12VPinVoltage, BatteryVoltage, FaultStatus);
}


     /* Service calls */
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJO_88_LIN1busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BJO_88_LIN1busCIOM_BusOff_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJP_88_LIN2busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BJP_88_LIN2busCIOM_BusOff_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJQ_88_LIN3busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BJQ_88_LIN3busCIOM_BusOff_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJR_88_LIN4busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BJR_88_LIN4busCIOM_BusOff_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJS_88_LIN5busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BJS_88_LIN5busCIOM_BusOff_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN00_ace1a6ba_GetCurrentComMode(ComM_ModeType *ComMode)
{
  return Rte_Call_UR_CN_LIN00_ace1a6ba_GetCurrentComMode(ComMode);
}
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN01_4323cd84_GetCurrentComMode(ComM_ModeType *ComMode)
{
  return Rte_Call_UR_CN_LIN01_4323cd84_GetCurrentComMode(ComMode);
}
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN02_a8147687_GetCurrentComMode(ComM_ModeType *ComMode)
{
  return Rte_Call_UR_CN_LIN02_a8147687_GetCurrentComMode(ComMode);
}
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN03_47d61db9_GetCurrentComMode(ComM_ModeType *ComMode)
{
  return Rte_Call_UR_CN_LIN03_47d61db9_GetCurrentComMode(ComMode);
}
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN04_a50a06c0_GetCurrentComMode(ComM_ModeType *ComMode)
{
  return Rte_Call_UR_CN_LIN04_a50a06c0_GetCurrentComMode(ComMode);
}
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN05_4ac86dfe_GetCurrentComMode(ComM_ModeType *ComMode)
{
  return Rte_Call_UR_CN_LIN05_4ac86dfe_GetCurrentComMode(ComMode);
}
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN06_a1ffd6fd_GetCurrentComMode(ComM_ModeType *ComMode)
{
  return Rte_Call_UR_CN_LIN06_a1ffd6fd_GetCurrentComMode(ComMode);
}
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN07_4e3dbdc3_GetCurrentComMode(ComM_ModeType *ComMode)
{
  return Rte_Call_UR_CN_LIN07_4e3dbdc3_GetCurrentComMode(ComMode);
}


     /* Mode Interfaces */
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(void)
{
  return Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule();
}
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(void)
{
  return Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule();
}
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(void)
{
  return Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule();
}
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(void)
{
  return Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule();
}
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(void)
{
  return Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule();
}
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(void)
{
  return Rte_Mode_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule();
}
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(void)
{
  return Rte_Mode_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule();
}
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(void)
{
  return Rte_Mode_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule();
}




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_LINMgr_Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(void)
{
return Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI();
}
Boolean TSC_LINMgr_Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig(void)
{
return Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig();
}

void TSC_LINMgr_Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(uint8 data)
{
  Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI( data);
}
void TSC_LINMgr_Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig(Boolean data)
{
  Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig( data);
}








Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN1_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN1Schedule data)
{
  return Rte_Write_Request_LIN1_ScheduleTableRequestMode_requestedMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN2_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN2Schedule data)
{
  return Rte_Write_Request_LIN2_ScheduleTableRequestMode_requestedMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN3_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN3Schedule data)
{
  return Rte_Write_Request_LIN3_ScheduleTableRequestMode_requestedMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN4_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN4Schedule data)
{
  return Rte_Write_Request_LIN4_ScheduleTableRequestMode_requestedMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN5_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN5Schedule data)
{
  return Rte_Write_Request_LIN5_ScheduleTableRequestMode_requestedMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN6_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN6Schedule data)
{
  return Rte_Write_Request_LIN6_ScheduleTableRequestMode_requestedMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN7_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN7Schedule data)
{
  return Rte_Write_Request_LIN7_ScheduleTableRequestMode_requestedMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN8_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN8Schedule data)
{
  return Rte_Write_Request_LIN8_ScheduleTableRequestMode_requestedMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_PcbConfig_LinInterfaces_X1CX0_a_T * TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void)
{
  return (SEWS_PcbConfig_LinInterfaces_X1CX0_a_T *) Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v();
}
SEWS_LIN_topology_P1AJR_T  TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v(void)
{
  return (SEWS_LIN_topology_P1AJR_T ) Rte_Prm_P1AJR_LIN_topology_v();
}
boolean  TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v(void)
{
  return (boolean ) Rte_Prm_P1WPP_isSecurityLinActive_v();
}


     /* LINMgr */
      /* LINMgr */



