/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Spi.c
 *           Config:  SCIM_HD_T1.dpa
 *       BSW Module:  Spi
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for BSW Module <Spi>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "SchM_Spi.h"
#include "TSC_SchM_Spi.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * AsymDecryptLengthBuffer: Integer in interval [0...4294967295]
 * AxleLoad_T: Integer in interval [0...65535]
 *   Unit: [kg], Factor: 1, Offset: 0
 * Blockid_T: Integer in interval [0...15]
 *   Factor: 1, Offset: 0
 * Boolean: Boolean
 * Code32bit_T: Integer in interval [0...4294967295]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ComM_InhibitionStatusType: Integer in interval [0...255]
 * ComM_UserHandleType: Integer in interval [0...255]
 * CounterType: Integer in interval [0...255]
 * Csm_ConfigIdType: Integer in interval [0...65535]
 * Days8bit_Fact025_T: Integer in interval [0...255]
 *   Unit: [Days], Factor: 1, Offset: 0
 * Debug_PVT_DOWHS1_ReportedValue: Boolean
 * Debug_PVT_DOWHS2_ReportedValue: Boolean
 * Debug_PVT_DOWLS2_ReportedValue: Boolean
 * Debug_PVT_DOWLS3_ReportedValue: Boolean
 * Debug_PVT_SCIM_Ctrl_Generic1: Integer in interval [0...3]
 * Debug_PVT_SCIM_FlexArrayData1: Integer in interval [0...31]
 * Debug_PVT_SCIM_RD_12VDCDCFault: Boolean
 * Debug_PVT_SCIM_RD_12VDCDCVolt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_12VLivingVolt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_12VParkedVolt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI01_7: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI02_8: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI03_9: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI04_10: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI05_11: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_ADI06_12: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS1_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS2_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS3_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BHS4_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_BLS1_Volt: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_DAI1_2: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_VBAT: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WHS1_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WHS1_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WHS2_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WHS2_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WLS2_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WLS2_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_RD_WLS3_Freq: Integer in interval [0...2047]
 * Debug_PVT_SCIM_RD_WLS3_VD: Integer in interval [0...255]
 * Debug_PVT_SCIM_TSincePwrOn_Long: Integer in interval [0...65535]
 * Debug_PVT_SCIM_TSincePwrOn_Short: Integer in interval [0...255]
 * Debug_PVT_SCIM_TSinceWkUp_Short: Integer in interval [0...255]
 * Debug_PVT_ScimHwSelect_WHS1: Boolean
 * Debug_PVT_ScimHwSelect_WHS2: Boolean
 * Debug_PVT_ScimHwSelect_WLS2: Boolean
 * Debug_PVT_ScimHwSelect_WLS3: Boolean
 * Debug_PVT_ScimHw_W_Duty: Integer in interval [0...127]
 * Debug_PVT_ScimHw_W_Freq: Integer in interval [0...2047]
 * Debug_SCIM_RD_Generic1: Integer in interval [0...31]
 * Debug_SCIM_RD_Generic2: Integer in interval [0...15]
 * Debug_SCIM_RD_Generic3: Integer in interval [0...63]
 * Debug_SCIM_RD_Generic4: Integer in interval [0...255]
 * Debug_SCIM_RD_Generic5: Boolean
 * Debug_SCIM_RD_Generic6: Integer in interval [0...31]
 * Dem_DTCGroupType: Integer in interval [0...16777215]
 * Dem_DTCStatusMaskType: Integer in interval [0...255]
 * Dem_EventIdType: Integer in interval [1...65535]
 * Dem_OperationCycleIdType: Integer in interval [0...255]
 * Dem_RatioIdType: Integer in interval [1...65535]
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * Distance32bit_T: Integer in interval [0...4294967295]
 *   Unit: [m], Factor: 1, Offset: 0
 * DtcIdA_T: Integer in interval [0...65535]
 *   Factor: 1, Offset: 0
 * DtcIdB_T: Integer in interval [0...65535]
 *   Factor: 1, Offset: 0
 * EcuAdr_T: Integer in interval [0...255]
 *   Factor: 1, Offset: 0
 * EcuM_TimeType: Integer in interval [0...4294967295]
 * EcuM_UserType: Integer in interval [0...255]
 * EngineTemp_T: Integer in interval [0...255]
 *   Unit: [DegreeC], Factor: 1, Offset: -40
 * EngineTotalHoursOfOperation_T: Integer in interval [0...4294967295]
 *   Unit: [h], Factor: 1, Offset: 0
 * EventFlag_T: Boolean
 * FSP1ResponseErrorL1_T: Boolean
 * FSP1ResponseErrorL2_T: Boolean
 * FSP1ResponseErrorL3_T: Boolean
 * FSP1ResponseErrorL4_T: Boolean
 * FSP1ResponseErrorL5_T: Boolean
 * FSP2ResponseErrorL1_T: Boolean
 * FSP2ResponseErrorL2_T: Boolean
 * FSP2ResponseErrorL3_T: Boolean
 * FSP3ResponseErrorL2_T: Boolean
 * FSP4ResponseErrorL2_T: Boolean
 * FSPDiagInfo_T: Integer in interval [0...63]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * FSPIndicationCmd_T: Integer in interval [0...65535]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * FSPSwitchStatus_T: Integer in interval [0...255]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * FailTA_T: Integer in interval [0...255]
 *   Factor: 1, Offset: 0
 * FailTB_T: Integer in interval [0...255]
 *   Factor: 1, Offset: 0
 * FuelRate_T: Integer in interval [0...65535]
 *   Unit: [l_per_h], Factor: 1, Offset: 0
 * GasRate_T: Integer in interval [0...65535]
 *   Unit: [Kg_per_h], Factor: 1, Offset: 0
 * Hours8bit_T: Integer in interval [0...255]
 *   Unit: [h], Factor: 1, Offset: 0
 * Hours8bit_T: Integer in interval [0...255]
 *   Unit: [h], Factor: 1, Offset: 0
 * IOHWAB_BOOL: Boolean
 * IOHWAB_SINT8: Integer in interval [-128...127]
 * IOHWAB_UINT16: Integer in interval [0...65535]
 * IOHWAB_UINT8: Integer in interval [0...255]
 * InstantFuelEconomy_T: Integer in interval [0...65535]
 *   Unit: [km_per_l], Factor: 1, Offset: 0
 * Int8Bit_T: Integer in interval [0...255]
 * IntLghtLvlIndScaled_cmd_T: Integer in interval [0...15]
 *   Unit: [Step], Factor: 1, Offset: 0
 * Issm_IssHandleType: Integer in interval [0...255]
 * Issm_UserHandleType: Integer in interval [0...255]
 * MaintServiceID_T: Integer in interval [0...255]
 *   Unit: [ID], Factor: 1, Offset: 0
 * Minutes8bit_T: Integer in interval [0...255]
 *   Unit: [min], Factor: 1, Offset: 0
 * Minutes8bit_T: Integer in interval [0...255]
 *   Unit: [min], Factor: 1, Offset: 0
 * Months8bit_T: Integer in interval [0...255]
 *   Unit: [Months], Factor: 1, Offset: 0
 * NetworkHandleType: Integer in interval [0...255]
 * NvM_BlockIdType: Integer in interval [1...32767]
 * PGNRequest_T: Integer in interval [0...16777215]
 * Percent8bit125NegOffset_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: -125
 * Percent8bitFactor04_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: 0
 * Percent8bitNoOffset_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: 0
 * Percent8bitNoOffset_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: 0
 * PinCode_validity_time_T: Integer in interval [0...255]
 *   Unit: [h], Factor: 1, Offset: 1
 * PressureFactor8_T: Integer in interval [0...255]
 *   Unit: [kPa], Factor: 1, Offset: 0
 * RemainDistOilChange_T: Integer in interval [0...65535]
 *   Unit: [km], Factor: 1, Offset: 0
 * RemainEngineTimeOilChange_T: Integer in interval [0...65535]
 *   Unit: [h], Factor: 1, Offset: 0
 * ResponseErrorCCFW: Boolean
 * ResponseErrorDLFW_T: Boolean
 * ResponseErrorELCP1_T: Boolean
 * ResponseErrorELCP2_T: Boolean
 * ResponseErrorILCP1_T: Boolean
 * ResponseErrorILCP2_T: Boolean
 * ResponseErrorLECM2_T: Boolean
 * ResponseErrorLECMBasic_T: Boolean
 * ResponseErrorRCECS: Boolean
 * Rte_DT_EngTraceHWData_T_0: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_10: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_11: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_2: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_3: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_4: Integer in interval [0...255]
 * Rte_DT_EngTraceHWData_T_8: Integer in interval [0...65535]
 * Rte_DT_EngTraceHWData_T_9: Integer in interval [0...255]
 * SEWS_ABS_Inhibit_SwType_P1SY6_T: Integer in interval [0...255]
 * SEWS_AlarmAutoRelockRequestDuration_X1CYA_T: Integer in interval [0...255]
 * SEWS_AntMappingConfig_Gain_X1C03_T: Integer in interval [0...255]
 * SEWS_AntMappingConfig_Multi_X1CY3a_T: Integer in interval [-32768...32767]
 * SEWS_AutoAlarmReactivationTimeout_P1B2S_T: Integer in interval [0...255]
 * SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T: Integer in interval [0...255]
 * SEWS_AuxBBSw_TimeoutForReq_P1DV1_T: Integer in interval [0...255]
 * SEWS_AuxBbSw1_Logic_P1DI2_T: Integer in interval [0...255]
 * SEWS_AuxBbSw2_Logic_P1DI3_T: Integer in interval [0...255]
 * SEWS_AuxBbSw3_Logic_P1DI4_T: Integer in interval [0...255]
 * SEWS_AuxBbSw4_Logic_P1DI5_T: Integer in interval [0...255]
 * SEWS_AuxBbSw5_Logic_P1DI6_T: Integer in interval [0...255]
 * SEWS_AuxBbSw6_Logic_P1DI7_T: Integer in interval [0...255]
 * SEWS_AuxHornMaxActivationTime_X1CYZ_T: Integer in interval [0...65535]
 * SEWS_AxleConfiguration_P1B16_T: Integer in interval [0...255]
 * SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T: Integer in interval [0...255]
 * SEWS_BodybuilderAccessToAccelPedal_P1B72_T: Integer in interval [0...255]
 * SEWS_CM_Configuration_P1LGD_T: Integer in interval [0...255]
 * SEWS_CabHeightValue_P1R0P_T: Integer in interval [0...255]
 * SEWS_CityHornMaxActivationTime_X1CY0_T: Integer in interval [0...65535]
 * SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T: Integer in interval [0...255]
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * SEWS_ContainerUnlockHMIDeviceType_P1CXO_T: Integer in interval [0...255]
 * SEWS_CraneHMIDeviceType_P1CXA_T: Integer in interval [0...255]
 * SEWS_CraneSwIndicationType_P1CXC_T: Integer in interval [0...255]
 * SEWS_CrankingLockActivation_P1DS3_T: Integer in interval [0...255]
 * SEWS_DashboardLedTimeout_P1IZ4_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS01_P1V6O_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS02_P1V6P_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS02_P1V7E_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS03_P1V7F_T: Integer in interval [0...255]
 * SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T: Integer in interval [0...255]
 * SEWS_DoorAutoLockingSpeed_P1B2Q_T: Integer in interval [0...255]
 * SEWS_DoorIndicationReqDuration_P1DWP_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectMaxOperation_P1DXA_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionRestingTime_P1DW9_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionTimeWindow_P1DW8_T: Integer in interval [0...255]
 * SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T: Integer in interval [0...65535]
 * SEWS_DoorLockingFailureTimeout_X1CX9_T: Integer in interval [0...255]
 * SEWS_DriverPosition_LHD_RHD_P1ALJ_T: Integer in interval [0...255]
 * SEWS_DwmVehicleModes_P1BDU_T: Integer in interval [0...255]
 * SEWS_ECSActiveStateTimeout_P1CUE_T: Integer in interval [0...65535]
 * SEWS_ECSStandbyActivationTimeout_P1CUA_T: Integer in interval [0...65535]
 * SEWS_ECSStandbyExtendedActTimeout_P1CUB_T: Integer in interval [0...65535]
 * SEWS_ECSStopButtonHoldTimeout_P1DWI_T: Integer in interval [0...255]
 * SEWS_ECS_StandbyBlinkTime_P1GCL_T: Integer in interval [0...255]
 * SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T: Integer in interval [0...255]
 * SEWS_FCW_ConfirmTimeout_P1LGF_T: Integer in interval [0...255]
 * SEWS_FCW_LedLogic_P1LG1_T: Integer in interval [0...255]
 * SEWS_FCW_SwPushThreshold_P1LGE_T: Integer in interval [0...255]
 * SEWS_FCW_SwStuckTimeout_P1LGG_T: Integer in interval [0...255]
 * SEWS_FPBRSwitchRequestACKTime_P1LXR_T: Integer in interval [0...255]
 * SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T: Integer in interval [0...255]
 * SEWS_FSC_TimeoutThreshold_X1CZR_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN1_P1EWZ_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN2_P1EW0_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN3_P1EW1_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN4_P1EW2_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN5_P1EW3_T: Integer in interval [0...255]
 * SEWS_FS_DiagAct_ID001_ID009_P1EAA_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID010_ID019_P1EAB_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID02_ID029_P1EAC_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID030_ID039_P1EAD_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID040_ID049_P1EAE_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID050_ID059_P1EAF_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID060_ID069_P1EAG_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID070_ID079_P1EAH_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID080_ID089_P1EAI_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID100_ID109_P1EAK_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID110_ID119_P1EAL_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID120_ID129_P1EAM_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID130_ID139_P1EAN_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID140_ID149_P1EAO_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID150_ID159_P1EAP_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID170_ID179_P1EAR_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID180_ID189_P1EAS_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID190_ID199_P1EAT_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID200_ID209_P1EAU_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID210_ID219_P1EAV_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID220_ID229_P1EAW_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID230_ID239_P1EAX_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID240_ID249_P1EAY_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T: Integer in interval [0...65535]
 * SEWS_FerryFuncSwStuckedTimeout_P1EXK_T: Integer in interval [0...255]
 * SEWS_FrontAxleArrangement_P1CSH_T: Integer in interval [0...255]
 * SEWS_FrontSuspensionType_P1JBR_T: Integer in interval [0...255]
 * SEWS_FuelTypeInformation_P1M7T_T: Integer in interval [0...255]
 * SEWS_HeadwaySupport_P1BEX_T: Integer in interval [0...255]
 * SEWS_HwToleranceThreshold_X1C04_T: Integer in interval [0...255]
 * SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T: Integer in interval [0...255]
 * SEWS_IL_CtrlDeviceTypeFront_P1DKH_T: Integer in interval [0...255]
 * SEWS_IL_LockingCmdDelayOff_P1K7E_T: Integer in interval [0...255]
 * SEWS_KeyMatchingIndicationTimeout_X1CV3_T: Integer in interval [0...255]
 * SEWS_KeyfobDetectionMappingConfig_P1WIR_T: Integer in interval [0...255]
 * SEWS_KeyfobDetectionMappingSelection_P1WIP_T: Integer in interval [0...255]
 * SEWS_KeyfobEncryptCode_P1DS4_T: Integer in interval [0...255]
 * SEWS_KeyfobType_P1VKL_T: Integer in interval [0...255]
 * SEWS_KneelButtonStuckedTimeout_P1DWD_T: Integer in interval [0...255]
 * SEWS_LIN_topology_P1AJR_T: Integer in interval [0...255]
 * SEWS_LKS_SwType_P1R5P_T: Integer in interval [0...255]
 * SEWS_LNGTank1Volume_P1LX9_T: Integer in interval [0...65535]
 * SEWS_LNGTank2Volume_P1LYA_T: Integer in interval [0...65535]
 * SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T: Integer in interval [0...255]
 * SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T: Integer in interval [0...255]
 * SEWS_LockFunctionHardwareInterface_P1MXZ_T: Integer in interval [0...255]
 * SEWS_P1BWF_Level_Memorization_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Max_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1DKF_Long_press_threshold_T: Integer in interval [0...255]
 * SEWS_P1DKF_Shut_off_threshold_T: Integer in interval [0...255]
 * SEWS_P1JSY_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSY_RampStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_RampStroke_T: Integer in interval [0...255]
 * SEWS_P1QR6_Threshold_VAT_T: Integer in interval [0...255]
 * SEWS_P1QR6_Threshold_VBT_T: Integer in interval [0...255]
 * SEWS_P1QR6_Threshold_VOR_T: Integer in interval [0...255]
 * SEWS_P1V60_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V60_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V61_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V61_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V62_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V62_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V63_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V63_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V64_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V64_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V65_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V65_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V66_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V66_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V67_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V67_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V68_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V68_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V69_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V69_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6U_Threshold_OC_STB_T: Integer in interval [0...255]
 * SEWS_P1V6U_Threshold_STG_T: Integer in interval [0...255]
 * SEWS_P1V6V_Threshold_OC_STB_T: Integer in interval [0...255]
 * SEWS_P1V6V_Threshold_STG_T: Integer in interval [0...255]
 * SEWS_P1V6W_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6W_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6X_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6X_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6Y_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6Y_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6Z_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6Z_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V79_P1Interface_T: Integer in interval [0...255]
 * SEWS_P1V79_P2Interface_T: Integer in interval [0...255]
 * SEWS_P1V79_P3Interface_T: Integer in interval [0...255]
 * SEWS_P1V79_P4Interface_T: Integer in interval [0...255]
 * SEWS_P1V8F_Threshold_VAT_T: Integer in interval [0...255]
 * SEWS_P1V8F_Threshold_VBT_T: Integer in interval [0...255]
 * SEWS_P1VKG_DISPLAY_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_EMS_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_MVUC_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_TECU_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdHigh_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdLow_T: Integer in interval [0...255]
 * SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T: Integer in interval [0...255]
 * SEWS_PTO_EmergencyFilteringTimer_P1BD3_T: Integer in interval [0...255]
 * SEWS_PTO_RequestFilteringTimer_P1BD4_T: Integer in interval [0...255]
 * SEWS_PassengersSeatBeltInstalled_P1VQB_T: Integer in interval [0...255]
 * SEWS_PassengersSeatBeltSensorType_P1VYK_T: Integer in interval [0...255]
 * SEWS_PassiveEntryFunction_Type_P1VKF_T: Integer in interval [0...255]
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T: Integer in interval [0...255]
 * SEWS_Pvt_ActivateReporting_X1C14_T: Integer in interval [0...255]
 * SEWS_RAS_LEDFeedbackIndication_P1GCC_T: Integer in interval [0...255]
 * SEWS_RCECSButtonStucked_P1DWJ_T: Integer in interval [0...255]
 * SEWS_RCECSUpDownStucked_P1DWK_T: Integer in interval [0...255]
 * SEWS_RCECS_HoldCircuitTimer_P1IUS_T: Integer in interval [0...255]
 * SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T: Integer in interval [0...255]
 * SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T: Integer in interval [0...255]
 * SEWS_Slid5thWheelTimeoutForReq_P1DV9_T: Integer in interval [0...255]
 * SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T: Integer in interval [0...255]
 * SEWS_SpeedRelockingReinitThreshold_P1H55_T: Integer in interval [0...255]
 * SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T: Integer in interval [0...255]
 * SEWS_TailLiftHMIDeviceType_P1CW9_T: Integer in interval [0...255]
 * SEWS_TailLiftTimeoutForRequest_P1DWA_T: Integer in interval [0...255]
 * SEWS_TailLift_Crane_Act_P1CXB_T: Integer in interval [0...255]
 * SEWS_TheftAlarmRequestDuration_X1CY8_T: Integer in interval [0...255]
 * SEWS_WeightClassInformation_P1M7S_T: Integer in interval [0...255]
 * SEWS_WheelDifferentialLockPushButtonType_P1UG1_T: Integer in interval [0...255]
 * SEWS_X1CX4_P1Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P2Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P3Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P4Interface_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * SEWS_X1CY2_FastenedHigh_STB_T: Integer in interval [0...255]
 * SEWS_X1CY2_FastenedLow_T: Integer in interval [0...255]
 * SEWS_X1CY2_UnfastnedAndNotSeatHigh_T: Integer in interval [0...255]
 * SEWS_X1CY2_UnfastnedAndNotSeatLow_T: Integer in interval [0...255]
 * SEWS_X1CY2_UnfastnedAndSeatHigh_T: Integer in interval [0...255]
 * SEWS_X1CY2_UnfastnedAndSeatLow_T: Integer in interval [0...255]
 * SEWS_X1CY5_AntennaP1_LimitRSSI_T: Integer in interval [-32768...32767]
 * SEWS_X1CY5_AntennaP2_LimitRSSI_T: Integer in interval [-32768...32767]
 * SEWS_X1CY5_AntennaP3_LimitRSSI_T: Integer in interval [-32768...32767]
 * SEWS_X1CY5_AntennaP4_LimitRSSI_T: Integer in interval [-32768...32767]
 * SEWS_X1CY5_AntennaPi_LimitRSSI_T: Integer in interval [-32768...32767]
 * SEWS_X1CZQ_Operating_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Protecting_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Reduced_T: Integer in interval [0...255]
 * SEWS_X1CZQ_ShutdownReady_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Withstand_T: Integer in interval [0...255]
 * Seconds32bitExtra_T: Integer in interval [0...4294967295]
 *   Unit: [s], Factor: 1, Offset: 0
 * Seconds8bitFact025_T: Integer in interval [0...255]
 *   Unit: [s], Factor: 1, Offset: 0
 * ServiceDistance16BitFact5Offset_T: Integer in interval [0...65535]
 *   Unit: [km], Factor: 1, Offset: -160635
 * ServiceDistance_T: Integer in interval [0...4294967295]
 *   Unit: [m], Factor: 1, Offset: -21474836000
 * ServiceTime_T: Integer in interval [0...4294967295]
 *   Unit: [s], Factor: 1, Offset: -2147483600
 * ShortPulseMaxLength_T: Integer in interval [0...15]
 *   Unit: [ms], Factor: 1, Offset: 0
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * SpeedRpm16bit_T: Integer in interval [0...65535]
 *   Unit: [rpm], Factor: 1, Offset: 0
 * SwitchDetectionNeeded_T: Boolean
 * SymDecryptLengthBuffer: Integer in interval [0...4294967295]
 * SymEncryptLengthBuffer: Integer in interval [0...4294967295]
 * Temperature16bit_T: Integer in interval [0...65535]
 *   Unit: [DegreeC], Factor: 1, Offset: -273
 * TimeInMicrosecondsType: Integer in interval [0...4294967295]
 * TimeMinuteType_T: Integer in interval [0...63]
 *   Unit: [min], Factor: 1, Offset: 0
 * TimesetHr_T: Integer in interval [0...31]
 *   Unit: [h], Factor: 1, Offset: 0
 * TotalLngConsumed_T: Integer in interval [0...4294967295]
 *   Unit: [kg], Factor: 1, Offset: 0
 * UInt16: Integer in interval [0...65535]
 * UInt32: Integer in interval [0...4294967295]
 * UInt32: Integer in interval [0...4294967295]
 * UInt32_Length: Integer in interval [0...4294967295]
 * UInt8: Integer in interval [0...255]
 * UInt8: Integer in interval [0.0...0.0]
 * UInt8: Integer in interval [0.0...0.0]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * VGTT_EcuPwmDutycycle: Integer in interval [-128...127]
 * VGTT_EcuPwmPeriod: Integer in interval [0...65535]
 * VIN_rqst_T: Boolean
 * VehicleWeight16bit_T: Integer in interval [0...65535]
 *   Unit: [kg], Factor: 1, Offset: 0
 * Volume32BitFact001_T: Integer in interval [0...4294967295]
 *   Unit: [l], Factor: 1, Offset: 0
 * Volume32bitFact05_T: Integer in interval [0...4294967295]
 *   Unit: [l], Factor: 1, Offset: 0
 * Volume32bit_T: Integer in interval [0...4294967295]
 *   Unit: [l], Factor: 1, Offset: 0
 * VolumeValueType_T: Integer in interval [0...63]
 *   Unit: [Step], Factor: 1, Offset: 0
 * Years8bit_T: Integer in interval [0...255]
 *   Unit: [Years], Factor: 1, Offset: 1985
 * boolean: Boolean (standard type)
 * dtRef_VOID: DataReference
 * dtRef_const_VOID: DataReference
 * sint16: Integer in interval [-32768...32767] (standard type)
 * sint8: Integer in interval [-128...127] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * AcceleratorPedalStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * Ack2Bit_T: Enumeration of integer in interval [0...3] with enumerators
 * AlarmClkID_T: Enumeration of integer in interval [0...127] with enumerators
 * AlarmClkStat_T: Enumeration of integer in interval [0...3] with enumerators
 * AlarmClkType_T: Enumeration of integer in interval [0...7] with enumerators
 * AlarmStatus_stat_T: Enumeration of integer in interval [0...15] with enumerators
 * AltLoadDistribution_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * AutoRelock_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 * AutorelockingMovements_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * BBNetworkExtraSideWrknLights_T: Enumeration of integer in interval [0...7] with enumerators
 * BTStatus_T: Enumeration of integer in interval [0...3] with enumerators
 * BackToDriveReqACK_T: Enumeration of integer in interval [0...3] with enumerators
 * BackToDriveReq_T: Enumeration of integer in interval [0...3] with enumerators
 * BrakeSwitch_T: Enumeration of integer in interval [0...3] with enumerators
 * BswM_BswMRteMDG_CanBusOff: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_LIN1Schedule: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_LIN2Schedule: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_LIN3Schedule: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_LIN4Schedule: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_LIN5Schedule: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_LIN6Schedule: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_LIN7Schedule: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_LIN8Schedule: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_LINSchTableState: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_NvmWriteAllRequest: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_PvtReport_X1C14: Enumeration of integer in interval [0...255] with enumerators
 * BswM_BswMRteMDG_ResetProcess: Enumeration of integer in interval [0...255] with enumerators
 * BswM_ESH_Mode: Enumeration of integer in interval [0...255] with enumerators
 * ButtonAuth_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 * ButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 * CCIM_ACC_T: Enumeration of integer in interval [0...7] with enumerators
 * CCStates_T: Enumeration of integer in interval [0...15] with enumerators
 * CM_Status_T: Enumeration of integer in interval [0...7] with enumerators
 * CabExtraSideWorkingLight_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * CddLinTp_Status: Enumeration of integer in interval [0...255] with enumerators
 * ChangeKneelACK_T: Enumeration of integer in interval [0...3] with enumerators
 * ChangeRequest2Bit_T: Enumeration of integer in interval [0...3] with enumerators
 * ClosedOpen_T: Enumeration of integer in interval [0...3] with enumerators
 * ClutchSwitch_T: Enumeration of integer in interval [0...3] with enumerators
 * CollSituationHMICtrlRequestVM_T: Enumeration of integer in interval [0...7] with enumerators
 * ComM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 * Csm_ReturnType: Enumeration of integer in interval [0...4] with enumerators
 * Csm_ReturnType: Enumeration of integer in interval [0...255] with enumerators
 * Csm_VerifyResultType: Enumeration of integer in interval [0...1] with enumerators
 * DASSystemStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * Dcm_CommunicationModeType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_ConfirmationStatusType: Enumeration of integer in interval [0...3] with enumerators
 * Dcm_ControlDtcSettingType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_DiagnosticSessionControlType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_EcuResetType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...254] with enumerators
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_OpStatusType: Enumeration of integer in interval [0...64] with enumerators
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dcm_ProtocolType: Enumeration of integer in interval [0...254] with enumerators
 * Dcm_RequestKindType: Enumeration of integer in interval [0...2] with enumerators
 * Dcm_SecLevelType: Enumeration of integer in interval [0...28] with enumerators
 * Dcm_SesCtrlType: Enumeration of integer in interval [0...3] with enumerators
 * DeactivateActivate_T: Enumeration of integer in interval [0...3] with enumerators
 * DeactivateActivate_T: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_ADI_ReportGroup: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_ADI_ReportRequest: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_FlexDataRequest: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_LF_Trig: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_12VDCDC: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_12VLiving: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_12VParked: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_BHS1: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_BHS2: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_BHS3: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_BHS4: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_BLS1: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_DAIPullUp: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_WHS1: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_WHS2: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_WLS2: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_Ctrl_WLS3: Enumeration of integer in interval [0...3] with enumerators
 * Debug_PVT_SCIM_FlexArrayDataId: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_12VLivingFault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_12VParkedFault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_BHS1_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_BHS2_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_BHS3_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_BHS4_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_BLS1_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_VBAT_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_WHS1_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_WHS2_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_WLS2_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Debug_PVT_SCIM_RD_WLS3_Fault: Enumeration of integer in interval [0...7] with enumerators
 * Dem_DTCFormatType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DTCFormatType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DTCKindType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DTCOriginType: Enumeration of integer in interval [0...65535] with enumerators
 * Dem_DTCSeverityType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DTRControlType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DebounceResetStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_DebouncingStateType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_IndicatorStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_InitMonitorReasonType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_InitMonitorReasonType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_IumprDenomCondIdType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_IumprDenomCondStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_IumprReadinessGroupType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_MonitorStatusType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_OperationCycleStateType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_UdsStatusByteType: Enumeration of integer in interval [0...255] with enumerators
 * Dem_UdsStatusByteType: Enumeration of integer in interval [0...255] with enumerators
 * DeviceAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * DeviceInCab_stat_T: Enumeration of integer in interval [0...3] with enumerators
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 * DirectionIndicator_T: Enumeration of integer in interval [0...3] with enumerators
 * DisableEnable_T: Enumeration of integer in interval [0...3] with enumerators
 * DisengageEngage_T: Enumeration of integer in interval [0...3] with enumerators
 * DoorAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * DoorLatch_rqst_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 * DoorLatch_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * DoorLockUnlock_T: Enumeration of integer in interval [0...7] with enumerators
 * DoorLock_stat_T: Enumeration of integer in interval [0...15] with enumerators
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * Driver1TimeRelatedStates_T: Enumeration of integer in interval [0...15] with enumerators
 * DriverAuthDeviceMatching_T: Enumeration of integer in interval [0...3] with enumerators
 * DriverMemory_rqst_T: Enumeration of integer in interval [0...31] with enumerators
 * DriverSeatBeltSwitch_T: Enumeration of integer in interval [0...3] with enumerators
 * DriverTimeRelatedStates_T: Enumeration of integer in interval [0...15] with enumerators
 * DriverWorkingState_T: Enumeration of integer in interval [0...7] with enumerators
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 * DynamicCode_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * ECSStandByReq_T: Enumeration of integer in interval [0...7] with enumerators
 * ECSStandByRequest_T: Enumeration of integer in interval [0...7] with enumerators
 * ESCDriverReq_T: Enumeration of integer in interval [0...7] with enumerators
 * EcuM_BootTargetType: Enumeration of integer in interval [0...2] with enumerators
 * EcuM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 * EcuM_ShutdownCauseType: Enumeration of integer in interval [0...3] with enumerators
 * EcuM_StateType: Enumeration of integer in interval [0...144] with enumerators
 * ElectricalLoadReduction_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * EmergencyDoorsUnlock_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * EngineRetarderTorqueMode_T: Enumeration of integer in interval [0...15] with enumerators
 * EngineSpeedControlStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * EngineSpeedRequest_T: Enumeration of integer in interval [0...7] with enumerators
 * EngineStartAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * EngineStartAuth_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 * EscActionRequest_T: Enumeration of integer in interval [0...7] with enumerators
 * EvalButtonRequest_T: Enumeration of integer in interval [0...7] with enumerators
 * ExtraBBCraneStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * FPBRChangeReq_T: Enumeration of integer in interval [0...3] with enumerators
 * FPBRStatusInd_T: Enumeration of integer in interval [0...3] with enumerators
 * FWSelectedSpeedControlMode_T: Enumeration of integer in interval [0...7] with enumerators
 * FalseTrue_T: Enumeration of integer in interval [0...3] with enumerators
 * FerryFunctionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 * Freewheel_Status_Ctr_T: Enumeration of integer in interval [0...15] with enumerators
 * FrontLidLatch_cmd_T: Enumeration of integer in interval [0...7] with enumerators
 * FrontLidLatch_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 * FuelType_T: Enumeration of integer in interval [0...255] with enumerators
 * GearBoxUnlockAuth_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 * GearboxUnlockAuth_stat_decrypt_T: Enumeration of integer in interval [0...3] with enumerators
 * HandlingInformation_T: Enumeration of integer in interval [0...3] with enumerators
 * IL_ModeReq_T: Enumeration of integer in interval [0...7] with enumerators
 * IL_Mode_T: Enumeration of integer in interval [0...7] with enumerators
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 * IndicationCmd_T: Enumeration of integer in interval [0...1] with enumerators
 * Inhibit_T: Enumeration of integer in interval [0...3] with enumerators
 * InteriorLightDimming_rqst_T: Enumeration of integer in interval [0...31] with enumerators
 * Issm_IssStateType: Enumeration of integer in interval [0...2] with enumerators
 * J1939Rm_AckCode: Enumeration of integer in interval [0...3] with enumerators
 * J1939Rm_ExtIdType: Enumeration of integer in interval [0...4] with enumerators
 * KeyAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * KeyAuthentication_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 * KeyNotValid_T: Enumeration of integer in interval [0...7] with enumerators
 * KeyPosition_T: Enumeration of integer in interval [0...7] with enumerators
 * KeyfobAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * KeyfobAuth_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * KeyfobInCabLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * KeyfobInCabPresencePS_T: Enumeration of integer in interval [0...3] with enumerators
 * KeyfobLocation_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 * KeyfobOutsideLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * KeyfobPanicButton_Status_T: Enumeration of integer in interval [0...7] with enumerators
 * KeyfobPanicButton_Status_T: Enumeration of integer in interval [0...7] with enumerators
 * KneelingChangeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 * KneelingStatusHMI_T: Enumeration of integer in interval [0...3] with enumerators
 * LCSSystemStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * LKSCorrectiveSteeringStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * LKSStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * LevelAdjustmentAction_T: Enumeration of integer in interval [0...15] with enumerators
 * LevelAdjustmentAxles_T: Enumeration of integer in interval [0...7] with enumerators
 * LevelAdjustmentStroke_T: Enumeration of integer in interval [0...7] with enumerators
 * LevelChangeRequest_T: Enumeration of integer in interval [0...7] with enumerators
 * LevelControlInformation_T: Enumeration of integer in interval [0...15] with enumerators
 * LevelStrokeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 * LevelUserMemoryAction_T: Enumeration of integer in interval [0...7] with enumerators
 * LevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 * LiftAxleLiftPositionRequest_T: Enumeration of integer in interval [0...7] with enumerators
 * LiftAxlePositionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * LiftAxleUpRequestACK_T: Enumeration of integer in interval [0...7] with enumerators
 * LinDiagBusInfo: Enumeration of integer in interval [0...255] with enumerators
 * LinDiagRequest_T: Enumeration of integer in interval [0...255] with enumerators
 * LinDiagServiceStatus: Enumeration of integer in interval [0...255] with enumerators
 * Living12VPowerStability: Enumeration of integer in interval [0...255] with enumerators
 * LoadDistributionALDChangeACK_T: Enumeration of integer in interval [0...7] with enumerators
 * LoadDistributionChangeACK_T: Enumeration of integer in interval [0...7] with enumerators
 * LoadDistributionChangeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 * LoadDistributionFuncSelected_T: Enumeration of integer in interval [0...15] with enumerators
 * LoadDistributionRequestedACK_T: Enumeration of integer in interval [0...7] with enumerators
 * LoadDistributionRequested_T: Enumeration of integer in interval [0...15] with enumerators
 * LoadDistributionSelected_T: Enumeration of integer in interval [0...15] with enumerators
 * LockingIndication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * LuggageCompart_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 * MirrorFoldingRequest_T: Enumeration of integer in interval [0...7] with enumerators
 * MirrorHeat_T: Enumeration of integer in interval [0...7] with enumerators
 * NeutralPushed_T: Enumeration of integer in interval [0...3] with enumerators
 * NeutralPushed_T: Enumeration of integer in interval [0...3] with enumerators
 * NotDetected_T: Enumeration of integer in interval [0...3] with enumerators
 * NotDetected_T: Enumeration of integer in interval [0...3] with enumerators
 * NotEngagedEngaged_T: Enumeration of integer in interval [0...3] with enumerators
 * NotPresentPresent_T: Enumeration of integer in interval [0...3] with enumerators
 * NvM_RequestResultType: Enumeration of integer in interval [0...8] with enumerators
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 * OilQuality_T: Enumeration of integer in interval [0...7] with enumerators
 * OilStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * ParkHeaterTimer_cmd_T: Enumeration of integer in interval [0...7] with enumerators
 * PassengersSeatBelt_T: Enumeration of integer in interval [0...7] with enumerators
 * PassiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 * PcbPopulatedInfo_T: Enumeration of integer in interval [0...255] with enumerators
 * PinCode_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * PinCode_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * PtoState_T: Enumeration of integer in interval [0...31] with enumerators
 * PtosStatus_T: Enumeration of integer in interval [0...3] with enumerators
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 * RampLevelRequest_T: Enumeration of integer in interval [0...15] with enumerators
 * RearAxleSteeringFunctionDsbl_T: Enumeration of integer in interval [0...3] with enumerators
 * RearAxleSteeringFunctionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * ReducedSetMode_rqst_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 * Request_T: Enumeration of integer in interval [0...3] with enumerators
 * Request_T: Enumeration of integer in interval [0...3] with enumerators
 * RetarderTorqueMode_T: Enumeration of integer in interval [0...15] with enumerators
 * ReverseGearEngaged_T: Enumeration of integer in interval [0...3] with enumerators
 * ReverseWarning_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 * RideHeightFunction_T: Enumeration of integer in interval [0...15] with enumerators
 * RideHeightStorageRequest_T: Enumeration of integer in interval [0...15] with enumerators
 * RollRequest_T: Enumeration of integer in interval [0...7] with enumerators
 * RoofHatch_HMI_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 * Rte_DT_EcuHwDioCtrlArray_T_0: Enumeration of integer in interval [0...255] with enumerators
 * Rte_DT_EcuHwFaultValues_T_0: Enumeration of integer in interval [0...255] with enumerators
 * Rte_DT_InteriorLightMode_rqst_T_1: Enumeration of integer in interval [0...1] with enumerators
 * SCIM_ImmoDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 * SCIM_ImmoType_T: Enumeration of integer in interval [0...255] with enumerators
 * SCIM_PassiveDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 * SCIM_PassiveSearchCoverage_T: Enumeration of integer in interval [0...255] with enumerators
 * SEWS_P1V79_PiInterface_T: Enumeration of integer in interval [0...255] with enumerators
 * SEWS_P1VKG_APM_Check_Active_T: Enumeration of integer in interval [0...255] with enumerators
 * SEWS_PcbConfig_Adi_X1CXW_T: Enumeration of integer in interval [0...255] with enumerators
 * SEWS_PcbConfig_CanInterfaces_X1CX2_T: Enumeration of integer in interval [0...255] with enumerators
 * SEWS_PcbConfig_DOBHS_X1CXX_T: Enumeration of integer in interval [0...255] with enumerators
 * SEWS_PcbConfig_DOWHS_X1CXY_T: Enumeration of integer in interval [0...255] with enumerators
 * SEWS_PcbConfig_DOWLS_X1CXZ_T: Enumeration of integer in interval [0...255] with enumerators
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T: Enumeration of integer in interval [0...255] with enumerators
 * SEWS_X1CX4_PiInterface_T: Enumeration of integer in interval [0...255] with enumerators
 * SWSpdCtrlButtonsStatus1_T: Enumeration of integer in interval [0...15] with enumerators
 * SWSpeedControlAdjustMode_T: Enumeration of integer in interval [0...15] with enumerators
 * SeatSwivelStatus_T: Enumeration of integer in interval [0...3] with enumerators
 * SetCMOperation_T: Enumeration of integer in interval [0...7] with enumerators
 * SetFCWOperation_T: Enumeration of integer in interval [0...7] with enumerators
 * SpeedLockingInhibition_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * StopLevelChangeStatus_T: Enumeration of integer in interval [0...3] with enumerators
 * StorageAck_T: Enumeration of integer in interval [0...3] with enumerators
 * Supported_T: Enumeration of integer in interval [0...3] with enumerators
 * Synch_Unsynch_Mode_stat_T: Enumeration of integer in interval [0...7] with enumerators
 * SystemEvent_T: Enumeration of integer in interval [0...3] with enumerators
 * TachographPerformance_T: Enumeration of integer in interval [0...3] with enumerators
 * TellTaleStatus_T: Enumeration of integer in interval [0...7] with enumerators
 * TheftAlarmAct_rqst_decrypt_I: Enumeration of integer in interval [0...7] with enumerators
 * Thumbwheel_stat_T: Enumeration of integer in interval [0...31] with enumerators
 * TractionControlDriverRqst_T: Enumeration of integer in interval [0...7] with enumerators
 * TransferCaseNeutral_Req2_T: Enumeration of integer in interval [0...7] with enumerators
 * TransferCaseNeutral_T: Enumeration of integer in interval [0...3] with enumerators
 * TransferCaseNeutral_status_T: Enumeration of integer in interval [0...3] with enumerators
 * TransmissionDrivingMode_T: Enumeration of integer in interval [0...7] with enumerators
 * VEC_CryptoProxy_IdentificationState_Type: Enumeration of integer in interval [0...3] with enumerators
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 * VehicleOverspeed_T: Enumeration of integer in interval [0...3] with enumerators
 * WeightClass_T: Enumeration of integer in interval [0...15] with enumerators
 * WiredLevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 * XRSLStates_T: Enumeration of integer in interval [0...15] with enumerators
 * tVecRomTestBlockStatus: Enumeration of integer in interval [0...255] with enumerators
 *
 * Array Types:
 * ============
 * ArrayByteSize32: Array with 32 element(s) of type uint8
 * AsymDecryptDataBuffer: Array with 128 element(s) of type uint8
 * AsymDecryptDataBuffer: Array with 128 element(s) of type UInt8
 * AsymDecryptResultBuffer: Array with 128 element(s) of type UInt8
 * AsymDecryptResultBuffer: Array with 128 element(s) of type uint8
 * CryptoIdKey_T: Array with 16 element(s) of type uint8
 * Crypto_Function_serialized_T: Array with 12 element(s) of type uint8
 * Crypto_Function_serialized_T: Array with 12 element(s) of type uint8
 * DataArrayType_uint8_1: Array with 1 element(s) of type uint8
 * DataArrayType_uint8_2: Array with 2 element(s) of type uint8
 * DataArrayType_uint8_4: Array with 4 element(s) of type uint8
 * DataArrayType_uint8_5: Array with 5 element(s) of type uint8
 * DataArrayType_uint8_6: Array with 6 element(s) of type uint8
 * Dcm_Data116ByteType: Array with 116 element(s) of type UInt8
 * Dcm_Data116ByteType: Array with 116 element(s) of type uint8
 * Dcm_Data120ByteType: Array with 120 element(s) of type uint8
 * Dcm_Data120ByteType: Array with 120 element(s) of type uint8
 * Dcm_Data126ByteType: Array with 126 element(s) of type uint8
 * Dcm_Data126ByteType: Array with 126 element(s) of type uint8
 * Dcm_Data128ByteType: Array with 128 element(s) of type UInt8
 * Dcm_Data128ByteType: Array with 128 element(s) of type uint8
 * Dcm_Data12ByteType: Array with 12 element(s) of type uint8
 * Dcm_Data12ByteType: Array with 12 element(s) of type uint8
 * Dcm_Data130ByteType: Array with 130 element(s) of type uint8
 * Dcm_Data130ByteType: Array with 130 element(s) of type uint8
 * Dcm_Data16ByteType: Array with 16 element(s) of type uint8
 * Dcm_Data16ByteType: Array with 16 element(s) of type uint8
 * Dcm_Data17ByteType: Array with 17 element(s) of type uint8
 * Dcm_Data17ByteType: Array with 17 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data2000ByteType: Array with 2000 element(s) of type uint8
 * Dcm_Data221ByteType: Array with 221 element(s) of type uint8
 * Dcm_Data221ByteType: Array with 221 element(s) of type uint8
 * Dcm_Data241ByteType: Array with 241 element(s) of type uint8
 * Dcm_Data241ByteType: Array with 241 element(s) of type uint8
 * Dcm_Data256ByteType: Array with 256 element(s) of type uint8
 * Dcm_Data256ByteType: Array with 256 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 * Dcm_Data406ByteType: Array with 406 element(s) of type uint8
 * Dcm_Data406ByteType: Array with 406 element(s) of type uint8
 * Dcm_Data40ByteType: Array with 40 element(s) of type uint8
 * Dcm_Data40ByteType: Array with 40 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * Dcm_Data60ByteType: Array with 60 element(s) of type uint8
 * Dcm_Data60ByteType: Array with 60 element(s) of type uint8
 * Dcm_Data64ByteType: Array with 64 element(s) of type uint8
 * Dcm_Data64ByteType: Array with 64 element(s) of type uint8
 * Dcm_Data6ByteType: Array with 6 element(s) of type uint8
 * Dcm_Data6ByteType: Array with 6 element(s) of type uint8
 * Dcm_Data7ByteType: Array with 7 element(s) of type uint8
 * Dcm_Data7ByteType: Array with 7 element(s) of type uint8
 * Dcm_Data80ByteType: Array with 80 element(s) of type uint8
 * Dcm_Data80ByteType: Array with 80 element(s) of type uint8
 * Dcm_Data8ByteType: Array with 8 element(s) of type uint8
 * Dcm_Data8ByteType: Array with 8 element(s) of type uint8
 * Debug_PVT_SCIM_FlexArrayData: Array with 7 element(s) of type uint8
 * Dem_MaxDataValueType: Array with 6 element(s) of type uint8
 * Driver1Identification_T: Array with 14 element(s) of type uint8
 * DriversIdentifications_T: Array with 40 element(s) of type uint8
 * DrivingMode_T: Array with 3 element(s) of type uint8
 * EcuHwDioCtrlArray_T: Array with 40 element(s) of type Rte_DT_EcuHwDioCtrlArray_T_0
 * EcuHwFaultValues_T: Array with 40 element(s) of type Rte_DT_EcuHwFaultValues_T_0
 * EcuHwVoltageValues_T: Array with 40 element(s) of type VGTT_EcuPinVoltage_0V2
 * Encrypted128bit_T: Array with 16 element(s) of type uint8
 * EngTraceHWArray: Array with 10 element(s) of type EngTraceHWData_T
 * FSPIndicationCmdArray_T: Array with 8 element(s) of type DeviceIndication_T
 * FSPSwitchStatusArray_T: Array with 8 element(s) of type PushButtonStatus_T
 * FSP_Array10_8: Array with 10 element(s) of type FSP_Array8
 * FSP_Array5: Array with 5 element(s) of type uint8
 * FSP_Array8: Array with 8 element(s) of type uint8
 * FlexibleSwDisableDiagPresence_Type: Array with 5 element(s) of type uint8
 * FlexibleSwitchesinFailure_Type: Array with 24 element(s) of type FlexibleSwitchesinFailure_T
 * FspNVM_T: Array with 28 element(s) of type uint8
 * IoAsilCorePcbConfig_T: Array with 40 element(s) of type PcbPopulatedInfo_T
 * Issm_ActiveUserArrayType: Array with 2 element(s) of type uint32
 * KeyFobNVM_T: Array with 96 element(s) of type uint8
 * RandomGenerateResultBuffer: Array with 128 element(s) of type uint8
 * RandomGenerateResultBuffer: Array with 128 element(s) of type UInt8
 * RandomSeedDataBuffer: Array with 128 element(s) of type uint8
 * RandomSeedDataBuffer: Array with 128 element(s) of type UInt8
 * Rte_DT_AsymPrivateKeyType_1: Array with 128 element(s) of type UInt8
 * Rte_DT_AsymPrivateKeyType_1: Array with 128 element(s) of type uint8
 * Rte_DT_AsymPublicKeyType_1: Array with 128 element(s) of type uint8
 * Rte_DT_KeyExchangeBaseType_1: Array with 1 element(s) of type uint8
 * Rte_DT_KeyExchangePrivateType_1: Array with 1 element(s) of type uint8
 * Rte_DT_SymKeyType_1: Array with 256 element(s) of type UInt8
 * Rte_DT_SymKeyType_1: Array with 256 element(s) of type uint8
 * SEWS_AdiWakeUpConfig_P1WMD_a_T: Array with 16 element(s) of type SEWS_AdiWakeUpConfig_P1WMD_s_T
 * SEWS_AntMappingConfig_Gain_X1C03_a_T: Array with 5 element(s) of type SEWS_AntMappingConfig_Gain_X1C03_T
 * SEWS_AntMappingConfig_Multi_X1CY3_a_T: Array with 10 element(s) of type SEWS_AntMappingConfig_Multi_X1CY3a_a_T
 * SEWS_AntMappingConfig_Multi_X1CY3a_a_T: Array with 20 element(s) of type SEWS_AntMappingConfig_Multi_X1CY3a_T
 * SEWS_ChassisId_CHANO_T: Array with 16 element(s) of type uint8
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SEWS_KeyfobEncryptCode_P1DS4_a_T: Array with 24 element(s) of type SEWS_KeyfobEncryptCode_P1DS4_T
 * SEWS_PcbConfig_Adi_X1CXW_a_T: Array with 19 element(s) of type SEWS_PcbConfig_Adi_X1CXW_T
 * SEWS_PcbConfig_CanInterfaces_X1CX2_a_T: Array with 6 element(s) of type SEWS_PcbConfig_CanInterfaces_X1CX2_T
 * SEWS_PcbConfig_DOBHS_X1CXX_a_T: Array with 4 element(s) of type SEWS_PcbConfig_DOBHS_X1CXX_T
 * SEWS_PcbConfig_DOWHS_X1CXY_a_T: Array with 2 element(s) of type SEWS_PcbConfig_DOWHS_X1CXY_T
 * SEWS_PcbConfig_DOWLS_X1CXZ_a_T: Array with 3 element(s) of type SEWS_PcbConfig_DOWLS_X1CXZ_T
 * SEWS_PcbConfig_LinInterfaces_X1CX0_a_T: Array with 7 element(s) of type SEWS_PcbConfig_LinInterfaces_X1CX0_T
 * SEWS_VIN_VINNO_T: Array with 17 element(s) of type uint8
 * SEWS_X1CY5_AntennaP1_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaP1_LimitRSSI_T
 * SEWS_X1CY5_AntennaP2_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaP2_LimitRSSI_T
 * SEWS_X1CY5_AntennaP3_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaP3_LimitRSSI_T
 * SEWS_X1CY5_AntennaP4_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaP4_LimitRSSI_T
 * SEWS_X1CY5_AntennaPi_LimitRSSI_a_T: Array with 2 element(s) of type SEWS_X1CY5_AntennaPi_LimitRSSI_T
 * SignatureVerifyDataBuffer: Array with 128 element(s) of type uint8
 * SoftwareVersionSupported_T: Array with 4 element(s) of type uint8
 * SpeedControl_NVM_T: Array with 16 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 * SwitchDetectionResp_T: Array with 8 element(s) of type uint8
 * SymDecryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymDecryptDataBuffer: Array with 128 element(s) of type uint8
 * SymDecryptResultBuffer: Array with 128 element(s) of type uint8
 * SymDecryptResultBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptDataBuffer: Array with 128 element(s) of type uint8
 * SymEncryptResultBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptResultBuffer: Array with 128 element(s) of type uint8
 * VEC_CryptoProxy_UserSignal: Array with 12 element(s) of type UInt8
 * VIN_stat_T: Array with 11 element(s) of type uint8
 * VehicleIdentNumber_T: Array with 17 element(s) of type uint8
 * rkedata: Array with 16 element(s) of type uint8
 *
 * Record Types:
 * =============
 * AlmClkCurAlarm_stat_T: Record with elements
 *   ID_RE of type AlarmClkID_T
 *   SetHr_RE of type TimesetHr_T
 *   SetMin_RE of type TimeMinuteType_T
 *   Stat_RE of type AlarmClkStat_T
 *   Type_RE of type AlarmClkType_T
 * AlmClkSetCurAlm_rqst_T: Record with elements
 *   ID_RE of type AlarmClkID_T
 *   SetHr_RE of type TimesetHr_T
 *   SetMin_RE of type TimeMinuteType_T
 *   Type_RE of type AlarmClkType_T
 *   Stat_RE of type AlarmClkStat_T
 * AsymPrivateKeyType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_AsymPrivateKeyType_1
 * AsymPrivateKeyType: Record with elements
 *   length of type UInt32
 *   data of type Rte_DT_AsymPrivateKeyType_1
 * AsymPublicKeyType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_AsymPublicKeyType_1
 * ButtonStatus: Record with elements
 *   Button1ID of type uint8
 *   Button1PressCounter of type uint8
 *   Button1PressTime of type uint16
 *   Button2ID of type uint8
 *   Button2PressCounter of type uint8
 *   Button2PressTime of type uint16
 * DiagFaultStat_T: Record with elements
 *   EcuAdr_RE of type EcuAdr_T
 *   DtcIdA_RE of type DtcIdA_T
 *   FailTA_RE of type FailTA_T
 *   DtcIdB_RE of type DtcIdB_T
 *   FailTB_RE of type FailTB_T
 * EngTraceHWData_T: Record with elements
 *   Reset_Type of type Rte_DT_EngTraceHWData_T_0
 *   Timelog of type uint32
 *   TaskID of type Rte_DT_EngTraceHWData_T_2
 *   ServiceID of type Rte_DT_EngTraceHWData_T_3
 *   RunnableID of type Rte_DT_EngTraceHWData_T_4
 *   MemoryAddress of type uint32
 *   MC_RGM_FES_Reg of type uint32
 *   MC_RGM_DES_Reg of type uint32
 *   ModuleID of type Rte_DT_EngTraceHWData_T_8
 *   InstanceID of type Rte_DT_EngTraceHWData_T_9
 *   APIID of type Rte_DT_EngTraceHWData_T_10
 *   ErrorID of type Rte_DT_EngTraceHWData_T_11
 * FMS1_T: Record with elements
 *   Blockid_RE of type Blockid_T
 *   TellTaleStatus1_RE of type TellTaleStatus_T
 *   TellTaleStatus2_RE of type TellTaleStatus_T
 *   TellTaleStatus3_RE of type TellTaleStatus_T
 *   TellTaleStatus4_RE of type TellTaleStatus_T
 *   TellTaleStatus5_RE of type TellTaleStatus_T
 *   TellTaleStatus6_RE of type TellTaleStatus_T
 *   TellTaleStatus7_RE of type TellTaleStatus_T
 *   TellTaleStatus8_RE of type TellTaleStatus_T
 *   TellTaleStatus9_RE of type TellTaleStatus_T
 *   TellTaleStatus10_RE of type TellTaleStatus_T
 *   TellTaleStatus11_RE of type TellTaleStatus_T
 *   TellTaleStatus12_RE of type TellTaleStatus_T
 *   TellTaleStatus13_RE of type TellTaleStatus_T
 *   TellTaleStatus14_RE of type TellTaleStatus_T
 *   TellTaleStatus15_RE of type TellTaleStatus_T
 * FPBRMMIStat_T: Record with elements
 *   FPBRStatusInd_RE of type FPBRStatusInd_T
 *   FPBRChangeAck_RE of type Ack2Bit_T
 * FlexibleSwitchesinFailure_T: Record with elements
 *   FlexibleSwitchFailureType of type uint8
 *   FlexibleSwitchID of type uint8
 *   FlexibleSwitchPosition of type uint8
 *   FlexibleSwitchPanel of type uint8
 *   LINbus of type uint8
 * InteriorLightMode_T: Record with elements
 *   IL_Mode_RE of type IL_Mode_T
 *   EventFlag_RE of type EventFlag_T
 * InteriorLightMode_rqst_T: Record with elements
 *   IL_Mode_RE of type IL_ModeReq_T
 *   EventFlag_RE of type Rte_DT_InteriorLightMode_rqst_T_1
 * J1939Rm_ExtIdInfoType: Record with elements
 *   ExtIdType of type J1939Rm_ExtIdType
 *   ExtId1 of type uint8
 *   ExtId2 of type uint8
 *   ExtId3 of type uint8
 * KeyExchangeBaseType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_KeyExchangeBaseType_1
 * KeyExchangePrivateType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_KeyExchangePrivateType_1
 * LevelRequest_T: Record with elements
 *   FrontAxle_RE of type LevelChangeRequest_T
 *   RearAxle_RE of type LevelChangeRequest_T
 *   RollRequest_RE of type RollRequest_T
 * LfRssi: Record with elements
 *   AntennaPi of type uint16
 *   AntennaP1 of type uint16
 *   AntennaP2 of type uint16
 *   AntennaP3 of type uint16
 *   AntennaP4 of type uint16
 * MaintService_T: Record with elements
 *   ServiceDistance_RE of type ServiceDistance_T
 *   DistID_RE of type MaintServiceID_T
 *   ServiceCalendarTime_RE of type ServiceTime_T
 *   ServiceEngineTime_RE of type ServiceTime_T
 *   CalDateID_RE of type MaintServiceID_T
 *   EngTimeID_RE of type MaintServiceID_T
 * OilPrediction_T: Record with elements
 *   Status_RE of type OilStatus_T
 *   Quality_RE of type OilQuality_T
 *   RemainDist_RE of type RemainDistOilChange_T
 *   RemainTime_RE of type RemainEngineTimeOilChange_T
 * SEWS_AdiWakeUpConfig_P1WMD_s_T: Record with elements
 *   ThresholdHigh of type SEWS_P1WMD_ThresholdHigh_T
 *   ThresholdLow of type SEWS_P1WMD_ThresholdLow_T
 *   isActiveInLiving of type boolean
 *   isActiveInParked of type boolean
 * SEWS_AntMappingConfig_Single_X1CY5_s_T: Record with elements
 *   AntennaPi_LimitRSSI of type SEWS_X1CY5_AntennaPi_LimitRSSI_a_T
 *   AntennaP1_LimitRSSI of type SEWS_X1CY5_AntennaP1_LimitRSSI_a_T
 *   AntennaP2_LimitRSSI of type SEWS_X1CY5_AntennaP2_LimitRSSI_a_T
 *   AntennaP3_LimitRSSI of type SEWS_X1CY5_AntennaP3_LimitRSSI_a_T
 *   AntennaP4_LimitRSSI of type SEWS_X1CY5_AntennaP4_LimitRSSI_a_T
 * SEWS_AuxPassiveAntennasActivation_P1WIQ_s_T: Record with elements
 *   IsActiveAntenna_Pi of type boolean
 *   IsActiveAntenna_P1 of type boolean
 *   IsActiveAntenna_P2 of type boolean
 *   IsActiveAntenna_P3 of type boolean
 *   IsActiveAntenna_P4 of type boolean
 * SEWS_DAI_Installed_P1WMP_s_T: Record with elements
 *   LeftDoor of type boolean
 *   RightDoor of type boolean
 * SEWS_Diag_Act_LF_P_P1V79_s_T: Record with elements
 *   PiInterface of type SEWS_P1V79_PiInterface_T
 *   P1Interface of type SEWS_P1V79_P1Interface_T
 *   P2Interface of type SEWS_P1V79_P2Interface_T
 *   P3Interface of type SEWS_P1V79_P3Interface_T
 *   P4Interface of type SEWS_P1V79_P4Interface_T
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 * SEWS_ECS_MemSwTimings_P1BWF_s_T: Record with elements
 *   Level_Memorization_Min_Press of type SEWS_P1BWF_Level_Memorization_Min_Press_T
 *   Memory_Recall_Min_Press of type SEWS_P1BWF_Memory_Recall_Min_Press_T
 *   Memory_Recall_Max_Press of type SEWS_P1BWF_Memory_Recall_Max_Press_T
 * SEWS_FSC_VoltageThreshold_X1CZQ_s_T: Record with elements
 *   ShutdownReady of type SEWS_X1CZQ_ShutdownReady_T
 *   Reduced of type SEWS_X1CZQ_Reduced_T
 *   Operating of type SEWS_X1CZQ_Operating_T
 *   Protecting of type SEWS_X1CZQ_Protecting_T
 *   Withstand of type SEWS_X1CZQ_Withstand_T
 * SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T: Record with elements
 *   Threshold_VBT of type SEWS_P1V8F_Threshold_VBT_T
 *   Threshold_VAT of type SEWS_P1V8F_Threshold_VAT_T
 * SEWS_Fault_Config_ADI01_P1V6U_s_T: Record with elements
 *   Threshold_OC_STB of type SEWS_P1V6U_Threshold_OC_STB_T
 *   Threshold_STG of type SEWS_P1V6U_Threshold_STG_T
 * SEWS_Fault_Config_ADI02_P1V6V_s_T: Record with elements
 *   Threshold_OC_STB of type SEWS_P1V6V_Threshold_OC_STB_T
 *   Threshold_STG of type SEWS_P1V6V_Threshold_STG_T
 * SEWS_Fault_Config_ADI03_P1V6W_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6W_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6W_ContactClosed_T
 * SEWS_Fault_Config_ADI04_P1V6X_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6X_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6X_ContactClosed_T
 * SEWS_Fault_Config_ADI05_P1V6Y_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6Y_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6Y_ContactClosed_T
 * SEWS_Fault_Config_ADI06_P1V6Z_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6Z_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6Z_ContactClosed_T
 * SEWS_Fault_Config_ADI07_P1V60_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V60_ContactOpen_T
 *   ContactClosed of type SEWS_P1V60_ContactClosed_T
 * SEWS_Fault_Config_ADI08_P1V61_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V61_ContactOpen_T
 *   ContactClosed of type SEWS_P1V61_ContactClosed_T
 * SEWS_Fault_Config_ADI09_P1V62_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V62_ContactOpen_T
 *   ContactClosed of type SEWS_P1V62_ContactClosed_T
 * SEWS_Fault_Config_ADI10_P1V63_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V63_ContactOpen_T
 *   ContactClosed of type SEWS_P1V63_ContactClosed_T
 * SEWS_Fault_Config_ADI11_P1V64_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V64_ContactOpen_T
 *   ContactClosed of type SEWS_P1V64_ContactClosed_T
 * SEWS_Fault_Config_ADI12_P1V65_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V65_ContactOpen_T
 *   ContactClosed of type SEWS_P1V65_ContactClosed_T
 * SEWS_Fault_Config_ADI13_P1V66_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V66_ContactOpen_T
 *   ContactClosed of type SEWS_P1V66_ContactClosed_T
 * SEWS_Fault_Config_ADI14_P1V67_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V67_ContactOpen_T
 *   ContactClosed of type SEWS_P1V67_ContactClosed_T
 * SEWS_Fault_Config_ADI15_P1V68_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V68_ContactOpen_T
 *   ContactClosed of type SEWS_P1V68_ContactClosed_T
 * SEWS_Fault_Config_ADI16_P1V69_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V69_ContactOpen_T
 *   ContactClosed of type SEWS_P1V69_ContactClosed_T
 * SEWS_ForcedPositionStatus_Front_P1JSY_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSY_DriveStroke_T
 *   RampStroke of type SEWS_P1JSY_RampStroke_T
 * SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSZ_DriveStroke_T
 *   RampStroke of type SEWS_P1JSZ_RampStroke_T
 * SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T: Record with elements
 *   Threshold_VBT of type SEWS_P1QR6_Threshold_VBT_T
 *   Threshold_VAT of type SEWS_P1QR6_Threshold_VAT_T
 *   Threshold_VOR of type SEWS_P1QR6_Threshold_VOR_T
 * SEWS_IL_ShortLongPushThresholds_P1DKF_s_T: Record with elements
 *   Long_press_threshold of type SEWS_P1DKF_Long_press_threshold_T
 *   Shut_off_threshold of type SEWS_P1DKF_Shut_off_threshold_T
 * SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T: Record with elements
 *   Fastened of type SEWS_X1CY2_Fastened_s_T
 *   UnfastnedAndNotSeat of type SEWS_X1CY2_UnfastnedAndNotSeat_s_T
 *   UnfastnedAndSeat of type SEWS_X1CY2_UnfastnedAndSeat_s_T
 * SEWS_PcbConfig_AdiPullUp_X1CX5_s_T: Record with elements
 *   AdiPullupLiving of type boolean
 *   AdiPullupParked of type boolean
 * SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T: Record with elements
 *   PiInterface of type SEWS_X1CX4_PiInterface_T
 *   P1Interface of type SEWS_X1CX4_P1Interface_T
 *   P2Interface of type SEWS_X1CX4_P2Interface_T
 *   P3Interface of type SEWS_X1CX4_P3Interface_T
 *   P4Interface of type SEWS_X1CX4_P4Interface_T
 * SEWS_VINCheckProcessing_P1VKG_s_T: Record with elements
 *   APM_Check_Active of type SEWS_P1VKG_APM_Check_Active_T
 *   MVUC_Check_Active of type SEWS_P1VKG_MVUC_Check_Active_T
 *   EMS_Check_Active of type SEWS_P1VKG_EMS_Check_Active_T
 *   TECU_Check_Active of type SEWS_P1VKG_TECU_Check_Active_T
 *   DISPLAY_Check_Active of type SEWS_P1VKG_DISPLAY_Check_Active_T
 * SEWS_X1CY2_Fastened_s_T: Record with elements
 *   FastenedHigh_STB of type SEWS_X1CY2_FastenedHigh_STB_T
 *   FastenedLow of type SEWS_X1CY2_FastenedLow_T
 * SEWS_X1CY2_UnfastnedAndNotSeat_s_T: Record with elements
 *   UnfastnedAndNotSeatHigh of type SEWS_X1CY2_UnfastnedAndNotSeatHigh_T
 *   UnfastnedAndNotSeatLow of type SEWS_X1CY2_UnfastnedAndNotSeatLow_T
 * SEWS_X1CY2_UnfastnedAndSeat_s_T: Record with elements
 *   UnfastnedAndSeatHigh of type SEWS_X1CY2_UnfastnedAndSeatHigh_T
 *   UnfastnedAndSeatLow of type SEWS_X1CY2_UnfastnedAndSeatLow_T
 * SRS2_SNPN_T: Record with elements
 *   Byte0_RE of type Int8Bit_T
 *   Byte1_RE of type Int8Bit_T
 *   Byte2_RE of type Int8Bit_T
 *   Byte3_RE of type Int8Bit_T
 *   Byte4_RE of type Int8Bit_T
 * SRS2_SN_T: Record with elements
 *   Byte0_RE of type Int8Bit_T
 *   Byte1_RE of type Int8Bit_T
 *   Byte2_RE of type Int8Bit_T
 *   Byte3_RE of type Int8Bit_T
 * SetParkHtrTmr_rqst_T: Record with elements
 *   Timer_cmd_RE of type ParkHeaterTimer_cmd_T
 *   StartTimeHr_RE of type Hours8bit_T
 *   StartTimeMin_RE of type Minutes8bit_T
 *   DurnTimeHr_RE of type Hours8bit_T
 *   DurnTimeMin_RE of type Minutes8bit_T
 * SymKeyType: Record with elements
 *   length of type UInt32_Length
 *   data of type Rte_DT_SymKeyType_1
 * SymKeyType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_SymKeyType_1
 * VINCheckStatus_T: Record with elements
 *   isDISPLAY_CheckPassed of type boolean
 *   isAPM_CheckPassed of type boolean
 *   isVMCU_CheckPassed of type boolean
 *   isEMS_CheckPassed of type boolean
 *   isTECU_CheckPassed of type boolean
 * st_RKEdata: Record with elements
 *   rkedata of type rkedata
 *   rolling_cnt of type uint32
 *   rkecmd of type uint8
 *   v_low of type uint8
 *
 *********************************************************************************************************************/


#define SPI_START_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Schedulable Entity Name: Spi_MainFunction_Handling
 *
 *********************************************************************************************************************/

FUNC(void, SPI_CODE) Spi_MainFunction_Handling(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Spi_MainFunction_Handling
 *********************************************************************************************************************/

  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_00();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_00();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_01();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_01();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_02();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_02();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_03();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_03();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_04();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_04();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_05();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_05();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_06();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_06();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_07();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_07();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_08();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_08();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_09();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_09();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_10();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_10();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_11();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_11();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_12();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_12();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_13();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_13();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_14();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_14();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_15();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_15();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_16();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_16();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_17();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_17();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_18();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_18();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_19();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_19();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_20();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_20();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_21();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_21();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_22();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_22();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_23();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_23();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_24();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_24();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_25();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_25();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_26();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_26();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_27();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_27();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_28();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_28();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_29();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_29();
  TSC_Spi_SchM_Enter_Spi_SPI_EXCLUSIVE_AREA_30();
  TSC_Spi_SchM_Exit_Spi_SPI_EXCLUSIVE_AREA_30();

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SPI_STOP_SEC_CODE
#include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
