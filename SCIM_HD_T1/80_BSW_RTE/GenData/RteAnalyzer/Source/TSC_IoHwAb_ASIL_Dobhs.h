/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_IoHwAb_ASIL_Dobhs.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data);
Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data);
Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Read_ScimPvtControl_P_Status(uint8 *data);

/** Client server interfaces */
Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Call_EcuHwState_P_GetEcuVoltages_CS(VGTT_EcuPinVoltage_0V2 *EcuVoltageValues);
Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus);

/** Explicit inter-runnable variables */
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);

/** Calibration Component Calibration Parameters */
SEWS_HwToleranceThreshold_X1C04_T  TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v(void);
SEWS_PcbConfig_DOBHS_X1CXX_a_T * TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(void);
SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void);




