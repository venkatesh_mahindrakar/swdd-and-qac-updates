/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_LevelControl_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_BackToDriveReqACK_BackToDriveReqACK(BackToDriveReqACK_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_ChangeKneelACK_ChangeKneelACK(ChangeKneelACK_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_ECSStandByReqRCECS_ECSStandByReqRCECS(ECSStandByReq_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_ECSStandByReqWRC_ECSStandByReqWRC(ECSStandByReq_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_ECSStandbyAllowed_ECSStandbyAllowed(FalseTrue_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst(ElectricalLoadReduction_rqst_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_FPBRMMIStat_FPBRMMIStat(FPBRMMIStat_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_FPBRSwitchStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_FerryFunctionStatus_FerryFunctionStatus(FerryFunctionStatus_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK(Ack2Bit_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_FerryFunctionSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(FalseTrue_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_KneelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_KneelingStatusHMI_KneelingStatusHMI(KneelingStatusHMI_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_LoadingLevelSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_RampLevelRequestACK_RampLevelRequestACK(ChangeRequest2Bit_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_RampLevelStorageAck_RampLevelStorageAck(StorageAck_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_RideHeightStorageAck_RideHeightStorageAck(StorageAck_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_StopLevelChangeAck_StopLevelChangeStatus(StopLevelChangeStatus_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WRCAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelUserMemory_LevelUserMemory(LevelUserMemory_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WRCRollRequest_WRCRollRequest(RollRequest_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelUserMemory_WiredLevelUserMemory(WiredLevelUserMemory_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_BackToDriveReq_BackToDriveReq(BackToDriveReq_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_BlinkECSWiredLEDs_BlinkECSWiredLEDs(FalseTrue_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_ECSStandByRequest_ECSStandByRequest(ECSStandByRequest_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_ECSStandbyActive_ECSStandbyActive(FalseTrue_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_FPBRChangeReq_FPBRChangeReq(FPBRChangeReq_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_FPBR_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_FerryFunctionRequest_FerryFunctionRequest(Request_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq(ChangeRequest2Bit_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_FerryFunction_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd(InactiveActive_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_KneelDeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_KneelingChangeRequest_KneelingChangeRequest(KneelingChangeRequest_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_LevelRequest_LevelRequest(const LevelRequest_T *data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_LevelStrokeRequest_LevelStrokeRequest(LevelStrokeRequest_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_RampLevelRequest_RampLevelRequest(RampLevelRequest_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_RampLevelStorageRequest_RampLevelStorageRequest(RampLevelRequest_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_RideHeightFunctionRequest_RideHeightFunctionRequest(RideHeightFunction_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_RideHeightStorageRequest_RideHeightStorageRequest(RideHeightStorageRequest_T data);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Write_StopLevelChangeRequest_StopLevelChangeRequest(Request_T data);

/** Service interfaces */
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Call_Event_D1BUO_63_LoadingLevelSw_stuck_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Call_Event_D1CXA_63_FerryFlexibleSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Call_Event_D1CXB_63_KneelingFlexibleSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Call_Event_D1CXC_63_LoadingLevelAdjSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Call_Event_D1DOO_63_FPBRSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Call_UR_ANW_ECSStandByActive_ActivateIss(void);
Std_ReturnType TSC_LevelControl_HMICtrl_Rte_Call_UR_ANW_ECSStandByActive_DeactivateIss(void);

/** Explicit inter-runnable variables */
uint32 TSC_LevelControl_HMICtrl_Rte_IrvRead_DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_IRV_ECS_StandbyTimer(void);
void TSC_LevelControl_HMICtrl_Rte_IrvWrite_LevelControl_HMICtrl_20ms_runnable_IRV_ECS_StandbyTimer(uint32);

/** Calibration Component Calibration Parameters */
SEWS_ECSStandbyActivationTimeout_P1CUA_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1CUA_ECSStandbyActivationTimeout_v(void);
SEWS_ECSStandbyExtendedActTimeout_P1CUB_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1CUB_ECSStandbyExtendedActTimeout_v(void);
SEWS_ECSActiveStateTimeout_P1CUE_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1CUE_ECSActiveStateTimeout_v(void);
SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1CUF_LoadingLevelSwStuckedTimeout_v(void);
SEWS_KneelButtonStuckedTimeout_P1DWD_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1DWD_KneelButtonStuckedTimeout_v(void);
SEWS_FerryFuncSwStuckedTimeout_P1EXK_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1EXK_FerryFuncSwStuckedTimeout_v(void);
SEWS_ECS_StandbyBlinkTime_P1GCL_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1GCL_ECS_StandbyBlinkTime_v(void);
SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1IZ2_LoadingLevelAdjSwStuckTimeout_v(void);
SEWS_FrontSuspensionType_P1JBR_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1JBR_FrontSuspensionType_v(void);
SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1LXQ_FPBRSwitchStuckedTimeout_v(void);
SEWS_FPBRSwitchRequestACKTime_P1LXR_T  TSC_LevelControl_HMICtrl_Rte_Prm_P1LXR_FPBRSwitchRequestACKTime_v(void);
boolean  TSC_LevelControl_HMICtrl_Rte_Prm_P1A12_ADL_Sw_v(void);
boolean  TSC_LevelControl_HMICtrl_Rte_Prm_P1CT4_FerrySw_Installed_v(void);
boolean  TSC_LevelControl_HMICtrl_Rte_Prm_P1CT9_LoadingLevelSw_Installed_v(void);
boolean  TSC_LevelControl_HMICtrl_Rte_Prm_P1EXH_KneelingSwitchInstalled_v(void);
boolean  TSC_LevelControl_HMICtrl_Rte_Prm_P1IZ1_LoadingLevelAdjSwitchInstalled_v(void);
boolean  TSC_LevelControl_HMICtrl_Rte_Prm_P1LXP_FPBRSwitchInstalled_v(void);
boolean  TSC_LevelControl_HMICtrl_Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void);
boolean  TSC_LevelControl_HMICtrl_Rte_Prm_P1ALU_ECS_FullAirSystem_v(void);
boolean  TSC_LevelControl_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v(void);




