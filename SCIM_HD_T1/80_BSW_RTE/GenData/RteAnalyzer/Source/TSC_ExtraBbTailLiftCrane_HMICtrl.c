/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ExtraBbTailLiftCrane_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_ExtraBbTailLiftCrane_HMICtrl.h"
#include "TSC_ExtraBbTailLiftCrane_HMICtrl.h"








Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_CranePushButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_CranePushButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_CraneSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_CraneSwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_ExtraBBCraneStatus_ExtraBBCraneStatus(ExtraBBCraneStatus_T *data)
{
  return Rte_Read_ExtraBBCraneStatus_ExtraBBCraneStatus(data);
}

Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Living_Living(data);
}

Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_WRCCraneRequest_WRCCraneRequest(PushButtonStatus_T *data)
{
  return Rte_Read_WRCCraneRequest_WRCCraneRequest(data);
}




Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_BBCraneRequest_BBCraneRequest(OffOn_T data)
{
  return Rte_Write_BBCraneRequest_BBCraneRequest(data);
}

Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_CraneSupply_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_CraneSupply_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus(InactiveActive_T *data)
{
  return Rte_Read_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus(data);
}

Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_TailLiftPushButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_TailLiftPushButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_TailLiftSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_TailLiftSwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_WRCTailLiftRequest_WRCTailLiftRequest(PushButtonStatus_T *data)
{
  return Rte_Read_WRCTailLiftRequest_WRCTailLiftRequest(data);
}




Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_BBTailLiftRequest_BBTailLiftRequest(OffOn_T data)
{
  return Rte_Write_BBTailLiftRequest_BBTailLiftRequest(data);
}

Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_TailLift_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_TailLift_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_TailLiftHMIDeviceType_P1CW9_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CW9_TailLiftHMIDeviceType_v(void)
{
  return (SEWS_TailLiftHMIDeviceType_P1CW9_T ) Rte_Prm_P1CW9_TailLiftHMIDeviceType_v();
}
SEWS_CraneHMIDeviceType_P1CXA_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXA_CraneHMIDeviceType_v(void)
{
  return (SEWS_CraneHMIDeviceType_P1CXA_T ) Rte_Prm_P1CXA_CraneHMIDeviceType_v();
}
SEWS_TailLift_Crane_Act_P1CXB_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXB_TailLift_Crane_Act_v(void)
{
  return (SEWS_TailLift_Crane_Act_P1CXB_T ) Rte_Prm_P1CXB_TailLift_Crane_Act_v();
}
SEWS_CraneSwIndicationType_P1CXC_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXC_CraneSwIndicationType_v(void)
{
  return (SEWS_CraneSwIndicationType_P1CXC_T ) Rte_Prm_P1CXC_CraneSwIndicationType_v();
}
SEWS_TailLiftTimeoutForRequest_P1DWA_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1DWA_TailLiftTimeoutForRequest_v(void)
{
  return (SEWS_TailLiftTimeoutForRequest_P1DWA_T ) Rte_Prm_P1DWA_TailLiftTimeoutForRequest_v();
}
boolean  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
{
  return (boolean ) Rte_Prm_P1B9X_WirelessRC_Enable_v();
}


     /* ExtraBbTailLiftCrane_HMICtrl */
      /* ExtraBbTailLiftCrane_HMICtrl */



