/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  ExtraBbTailLiftCrane_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  ExtraBbTailLiftCrane_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <ExtraBbTailLiftCrane_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_CraneHMIDeviceType_P1CXA_T
 *   
 *
 * SEWS_CraneSwIndicationType_P1CXC_T
 *   
 *
 * SEWS_TailLiftHMIDeviceType_P1CW9_T
 *   
 *
 * SEWS_TailLiftTimeoutForRequest_P1DWA_T
 *   
 *
 * SEWS_TailLift_Crane_Act_P1CXB_T
 *   
 *
 *
 * Runnable Entities:
 * ==================
 * ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable
 *   
 *
 * ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable
 *   
 *
 *********************************************************************************************************************/

#include "Rte_ExtraBbTailLiftCrane_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_ExtraBbTailLiftCrane_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void ExtraBbTailLiftCrane_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_CraneHMIDeviceType_P1CXA_T: Integer in interval [0...255]
 * SEWS_CraneSwIndicationType_P1CXC_T: Integer in interval [0...255]
 * SEWS_TailLiftHMIDeviceType_P1CW9_T: Integer in interval [0...255]
 * SEWS_TailLiftTimeoutForRequest_P1DWA_T: Integer in interval [0...255]
 * SEWS_TailLift_Crane_Act_P1CXB_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * ExtraBBCraneStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   ExtraBBCraneStatus_Disable (0U)
 *   ExtraBBCraneStatus_Enable (1U)
 *   ExtraBBCraneStatus_Active (2U)
 *   ExtraBBCraneStatus_Spare (3U)
 *   ExtraBBCraneStatus_Spare_01 (4U)
 *   ExtraBBCraneStatus_Spare_02 (5U)
 *   ExtraBBCraneStatus_Error (6U)
 *   ExtraBBCraneStatus_NotAvailable (7U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_TailLiftHMIDeviceType_P1CW9_T Rte_Prm_P1CW9_TailLiftHMIDeviceType_v(void)
 *   SEWS_CraneHMIDeviceType_P1CXA_T Rte_Prm_P1CXA_CraneHMIDeviceType_v(void)
 *   SEWS_TailLift_Crane_Act_P1CXB_T Rte_Prm_P1CXB_TailLift_Crane_Act_v(void)
 *   SEWS_CraneSwIndicationType_P1CXC_T Rte_Prm_P1CXC_CraneSwIndicationType_v(void)
 *   SEWS_TailLiftTimeoutForRequest_P1DWA_T Rte_Prm_P1DWA_TailLiftTimeoutForRequest_v(void)
 *   boolean Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
 *
 *********************************************************************************************************************/


#define ExtraBbTailLiftCrane_HMICtrl_START_SEC_CODE
#include "ExtraBbTailLiftCrane_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_CranePushButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_CraneSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_ExtraBBCraneStatus_ExtraBBCraneStatus(ExtraBBCraneStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WRCCraneRequest_WRCCraneRequest(PushButtonStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BBCraneRequest_BBCraneRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_CraneSupply_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExtraBbTailLiftCrane_HMICtrl_CODE) ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbTailLiftCrane_HMICtrl_Crane_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PushButtonStatus_T Read_CranePushButtonStatus_PushButtonStatus;
  A2PosSwitchStatus_T Read_CraneSwitchStatus_A2PosSwitchStatus;
  ExtraBBCraneStatus_T Read_ExtraBBCraneStatus_ExtraBBCraneStatus;
  VehicleModeDistribution_T Read_SwcActivation_Living_Living;
  PushButtonStatus_T Read_WRCCraneRequest_WRCCraneRequest;

  SEWS_TailLiftHMIDeviceType_P1CW9_T P1CW9_TailLiftHMIDeviceType_v_data;
  SEWS_CraneHMIDeviceType_P1CXA_T P1CXA_CraneHMIDeviceType_v_data;
  SEWS_TailLift_Crane_Act_P1CXB_T P1CXB_TailLift_Crane_Act_v_data;
  SEWS_CraneSwIndicationType_P1CXC_T P1CXC_CraneSwIndicationType_v_data;
  SEWS_TailLiftTimeoutForRequest_P1DWA_T P1DWA_TailLiftTimeoutForRequest_v_data;

  boolean P1B9X_WirelessRC_Enable_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1CW9_TailLiftHMIDeviceType_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CW9_TailLiftHMIDeviceType_v();
  P1CXA_CraneHMIDeviceType_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXA_CraneHMIDeviceType_v();
  P1CXB_TailLift_Crane_Act_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXB_TailLift_Crane_Act_v();
  P1CXC_CraneSwIndicationType_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXC_CraneSwIndicationType_v();
  P1DWA_TailLiftTimeoutForRequest_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1DWA_TailLiftTimeoutForRequest_v();

  P1B9X_WirelessRC_Enable_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v();

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_CranePushButtonStatus_PushButtonStatus(&Read_CranePushButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_CraneSwitchStatus_A2PosSwitchStatus(&Read_CraneSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_ExtraBBCraneStatus_ExtraBBCraneStatus(&Read_ExtraBBCraneStatus_ExtraBBCraneStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_SwcActivation_Living_Living(&Read_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_WRCCraneRequest_WRCCraneRequest(&Read_WRCCraneRequest_WRCCraneRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_BBCraneRequest_BBCraneRequest(Rte_InitValue_BBCraneRequest_BBCraneRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_CraneSupply_DeviceIndication_DeviceIndication(Rte_InitValue_CraneSupply_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  ExtraBbTailLiftCrane_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_TailLiftPushButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_TailLiftSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_WRCTailLiftRequest_WRCTailLiftRequest(PushButtonStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BBTailLiftRequest_BBTailLiftRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_TailLift_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExtraBbTailLiftCrane_HMICtrl_CODE) ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbTailLiftCrane_HMICtrl_TailLift_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  InactiveActive_T Read_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus;
  VehicleModeDistribution_T Read_SwcActivation_Living_Living;
  PushButtonStatus_T Read_TailLiftPushButtonStatus_PushButtonStatus;
  A2PosSwitchStatus_T Read_TailLiftSwitchStatus_A2PosSwitchStatus;
  PushButtonStatus_T Read_WRCTailLiftRequest_WRCTailLiftRequest;

  SEWS_TailLiftHMIDeviceType_P1CW9_T P1CW9_TailLiftHMIDeviceType_v_data;
  SEWS_CraneHMIDeviceType_P1CXA_T P1CXA_CraneHMIDeviceType_v_data;
  SEWS_TailLift_Crane_Act_P1CXB_T P1CXB_TailLift_Crane_Act_v_data;
  SEWS_CraneSwIndicationType_P1CXC_T P1CXC_CraneSwIndicationType_v_data;
  SEWS_TailLiftTimeoutForRequest_P1DWA_T P1DWA_TailLiftTimeoutForRequest_v_data;

  boolean P1B9X_WirelessRC_Enable_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1CW9_TailLiftHMIDeviceType_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CW9_TailLiftHMIDeviceType_v();
  P1CXA_CraneHMIDeviceType_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXA_CraneHMIDeviceType_v();
  P1CXB_TailLift_Crane_Act_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXB_TailLift_Crane_Act_v();
  P1CXC_CraneSwIndicationType_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXC_CraneSwIndicationType_v();
  P1DWA_TailLiftTimeoutForRequest_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1DWA_TailLiftTimeoutForRequest_v();

  P1B9X_WirelessRC_Enable_v_data = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v();

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus(&Read_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_SwcActivation_Living_Living(&Read_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_TailLiftPushButtonStatus_PushButtonStatus(&Read_TailLiftPushButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_TailLiftSwitchStatus_A2PosSwitchStatus(&Read_TailLiftSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_WRCTailLiftRequest_WRCTailLiftRequest(&Read_WRCTailLiftRequest_WRCTailLiftRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_BBTailLiftRequest_BBTailLiftRequest(Rte_InitValue_BBTailLiftRequest_BBTailLiftRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_TailLift_DeviceIndication_DeviceIndication(Rte_InitValue_TailLift_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define ExtraBbTailLiftCrane_HMICtrl_STOP_SEC_CODE
#include "ExtraBbTailLiftCrane_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void ExtraBbTailLiftCrane_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  ExtraBBCraneStatus_T Test_ExtraBBCraneStatus_T_V_1 = ExtraBBCraneStatus_Disable;
  ExtraBBCraneStatus_T Test_ExtraBBCraneStatus_T_V_2 = ExtraBBCraneStatus_Enable;
  ExtraBBCraneStatus_T Test_ExtraBBCraneStatus_T_V_3 = ExtraBBCraneStatus_Active;
  ExtraBBCraneStatus_T Test_ExtraBBCraneStatus_T_V_4 = ExtraBBCraneStatus_Spare;
  ExtraBBCraneStatus_T Test_ExtraBBCraneStatus_T_V_5 = ExtraBBCraneStatus_Spare_01;
  ExtraBBCraneStatus_T Test_ExtraBBCraneStatus_T_V_6 = ExtraBBCraneStatus_Spare_02;
  ExtraBBCraneStatus_T Test_ExtraBBCraneStatus_T_V_7 = ExtraBBCraneStatus_Error;
  ExtraBBCraneStatus_T Test_ExtraBBCraneStatus_T_V_8 = ExtraBBCraneStatus_NotAvailable;

  InactiveActive_T Test_InactiveActive_T_V_1 = InactiveActive_Inactive;
  InactiveActive_T Test_InactiveActive_T_V_2 = InactiveActive_Active;
  InactiveActive_T Test_InactiveActive_T_V_3 = InactiveActive_Error;
  InactiveActive_T Test_InactiveActive_T_V_4 = InactiveActive_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
