/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  RoofHatch_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  RoofHatch_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <RoofHatch_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_RoofHatch_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_RoofHatch_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void RoofHatch_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * RoofHatch_HMI_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   RoofHatch_HMI_rqst_NoAction (0U)
 *   RoofHatch_HMI_rqst_Close (1U)
 *   RoofHatch_HMI_rqst_Open (2U)
 *   RoofHatch_HMI_rqst_SpareValue (3U)
 *   RoofHatch_HMI_rqst_SpareValue_01 (4U)
 *   RoofHatch_HMI_rqst_SpareValue_02 (5U)
 *   RoofHatch_HMI_rqst_ErrorIndicator (6U)
 *   RoofHatch_HMI_rqst_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T Rte_Prm_P1GCD_RoofHatch_FlexibleSwitchLogic_v(void)
 *   boolean Rte_Prm_P1CW8_RoofHatchCtrl_Act_v(void)
 *
 *********************************************************************************************************************/


#define RoofHatch_HMICtrl_START_SEC_CODE
#include "RoofHatch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoofHatch_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_RoofHatch_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst(RoofHatch_HMI_rqst_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoofHatch_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, RoofHatch_HMICtrl_CODE) RoofHatch_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoofHatch_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PushButtonStatus_T Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus;
  A3PosSwitchStatus_T Read_RoofHatch_SwitchStatus_A3PosSwitchStatus;
  VehicleModeDistribution_T Read_SwcActivation_Living_Living;

  SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T P1GCD_RoofHatch_FlexibleSwitchLogic_v_data;
  boolean P1CW8_RoofHatchCtrl_Act_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1GCD_RoofHatch_FlexibleSwitchLogic_v_data = TSC_RoofHatch_HMICtrl_Rte_Prm_P1GCD_RoofHatch_FlexibleSwitchLogic_v();
  P1CW8_RoofHatchCtrl_Act_v_data = TSC_RoofHatch_HMICtrl_Rte_Prm_P1CW8_RoofHatchCtrl_Act_v();

  fct_status = TSC_RoofHatch_HMICtrl_Rte_Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus(&Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RoofHatch_HMICtrl_Rte_Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus(&Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RoofHatch_HMICtrl_Rte_Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(&Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RoofHatch_HMICtrl_Rte_Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(&Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RoofHatch_HMICtrl_Rte_Read_RoofHatch_SwitchStatus_A3PosSwitchStatus(&Read_RoofHatch_SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RoofHatch_HMICtrl_Rte_Read_SwcActivation_Living_Living(&Read_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RoofHatch_HMICtrl_Rte_Write_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst(Rte_InitValue_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  RoofHatch_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoofHatch_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoofHatch_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, RoofHatch_HMICtrl_CODE) RoofHatch_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoofHatch_HMICtrl_Init
 *********************************************************************************************************************/

  SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T P1GCD_RoofHatch_FlexibleSwitchLogic_v_data;
  boolean P1CW8_RoofHatchCtrl_Act_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1GCD_RoofHatch_FlexibleSwitchLogic_v_data = TSC_RoofHatch_HMICtrl_Rte_Prm_P1GCD_RoofHatch_FlexibleSwitchLogic_v();
  P1CW8_RoofHatchCtrl_Act_v_data = TSC_RoofHatch_HMICtrl_Rte_Prm_P1CW8_RoofHatchCtrl_Act_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define RoofHatch_HMICtrl_STOP_SEC_CODE
#include "RoofHatch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void RoofHatch_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_1 = A3PosSwitchStatus_Middle;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_2 = A3PosSwitchStatus_Lower;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_3 = A3PosSwitchStatus_Upper;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_4 = A3PosSwitchStatus_Spare;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_5 = A3PosSwitchStatus_Spare_01;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_6 = A3PosSwitchStatus_Spare_02;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_7 = A3PosSwitchStatus_Error;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_8 = A3PosSwitchStatus_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  RoofHatch_HMI_rqst_T Test_RoofHatch_HMI_rqst_T_V_1 = RoofHatch_HMI_rqst_NoAction;
  RoofHatch_HMI_rqst_T Test_RoofHatch_HMI_rqst_T_V_2 = RoofHatch_HMI_rqst_Close;
  RoofHatch_HMI_rqst_T Test_RoofHatch_HMI_rqst_T_V_3 = RoofHatch_HMI_rqst_Open;
  RoofHatch_HMI_rqst_T Test_RoofHatch_HMI_rqst_T_V_4 = RoofHatch_HMI_rqst_SpareValue;
  RoofHatch_HMI_rqst_T Test_RoofHatch_HMI_rqst_T_V_5 = RoofHatch_HMI_rqst_SpareValue_01;
  RoofHatch_HMI_rqst_T Test_RoofHatch_HMI_rqst_T_V_6 = RoofHatch_HMI_rqst_SpareValue_02;
  RoofHatch_HMI_rqst_T Test_RoofHatch_HMI_rqst_T_V_7 = RoofHatch_HMI_rqst_ErrorIndicator;
  RoofHatch_HMI_rqst_T Test_RoofHatch_HMI_rqst_T_V_8 = RoofHatch_HMI_rqst_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
