/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  AuthenticationDevice_UICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  AuthenticationDevice_UICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <AuthenticationDevice_UICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_AuthenticationDevice_UICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_AuthenticationDevice_UICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void AuthenticationDevice_UICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * ButtonAuth_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   ButtonAuth_rqst_Idle (0U)
 *   ButtonAuth_rqst_RequestDeviceAuthentication (1U)
 *   ButtonAuth_rqst_Error (2U)
 *   ButtonAuth_rqst_NotAvailable (3U)
 * DeviceAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   DeviceAuthentication_rqst_Idle (0U)
 *   DeviceAuthentication_rqst_DeviceAuthenticationRequest (1U)
 *   DeviceAuthentication_rqst_DeviceDeauthenticationRequest (2U)
 *   DeviceAuthentication_rqst_DeviceMatching (3U)
 *   DeviceAuthentication_rqst_Spare1 (4U)
 *   DeviceAuthentication_rqst_Spare2 (5U)
 *   DeviceAuthentication_rqst_Error (6U)
 *   DeviceAuthentication_rqst_NotAvailable (7U)
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorsAjar_stat_Idle (0U)
 *   DoorsAjar_stat_BothDoorsAreClosed (1U)
 *   DoorsAjar_stat_DriverDoorIsOpen (2U)
 *   DoorsAjar_stat_PassengerDoorIsOpen (3U)
 *   DoorsAjar_stat_BothDoorsAreOpen (4U)
 *   DoorsAjar_stat_Spare (5U)
 *   DoorsAjar_stat_Error (6U)
 *   DoorsAjar_stat_NotAvailable (7U)
 * DriverAuthDeviceMatching_T: Enumeration of integer in interval [0...3] with enumerators
 *   DriverAuthDeviceMatching_Idle (0U)
 *   DriverAuthDeviceMatching_DeviceToMatchIsPresent (1U)
 *   DriverAuthDeviceMatching_Error (2U)
 *   DriverAuthDeviceMatching_NotAvailable (3U)
 * KeyPosition_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyPosition_KeyOut (0U)
 *   KeyPosition_IgnitionKeyInOffPosition (1U)
 *   KeyPosition_IgnitionKeyInAccessoryPosition (2U)
 *   KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition (3U)
 *   KeyPosition_IgnitionKeyInPreheatPosition (4U)
 *   KeyPosition_IgnitionKeyInCrankPosition (5U)
 *   KeyPosition_ErrorIndicator (6U)
 *   KeyPosition_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1T3W_VehSSButtonInstalled_v(void)
 *
 *********************************************************************************************************************/


#define AuthenticationDevice_UICtrl_START_SEC_CODE
#include "AuthenticationDevice_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AuthenticationDevice_UICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ButtonAuth_rqst_ButtonAuth_rqst(ButtonAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyPosition_KeyPosition(KeyPosition_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DeviceAuthentication_rqst_DeviceAuthentication_rqst(DeviceAuthentication_rqst_T data)
 *   Std_ReturnType Rte_Write_DriverAuthDeviceMatching_DriverAuthDeviceMatching(DriverAuthDeviceMatching_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AuthenticationDevice_UICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AuthenticationDevice_UICtrl_CODE) AuthenticationDevice_UICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AuthenticationDevice_UICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  ButtonAuth_rqst_T Read_ButtonAuth_rqst_ButtonAuth_rqst;
  DoorsAjar_stat_T Read_DoorsAjar_stat_DoorsAjar_stat;
  KeyPosition_T Read_KeyPosition_KeyPosition;
  VehicleModeDistribution_T Read_SwcActivation_Security_SwcActivation_Security;

  boolean P1T3W_VehSSButtonInstalled_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1T3W_VehSSButtonInstalled_v_data = TSC_AuthenticationDevice_UICtrl_Rte_Prm_P1T3W_VehSSButtonInstalled_v();

  fct_status = TSC_AuthenticationDevice_UICtrl_Rte_Read_ButtonAuth_rqst_ButtonAuth_rqst(&Read_ButtonAuth_rqst_ButtonAuth_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AuthenticationDevice_UICtrl_Rte_Read_DoorsAjar_stat_DoorsAjar_stat(&Read_DoorsAjar_stat_DoorsAjar_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AuthenticationDevice_UICtrl_Rte_Read_KeyPosition_KeyPosition(&Read_KeyPosition_KeyPosition);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AuthenticationDevice_UICtrl_Rte_Read_SwcActivation_Security_SwcActivation_Security(&Read_SwcActivation_Security_SwcActivation_Security);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AuthenticationDevice_UICtrl_Rte_Write_DeviceAuthentication_rqst_DeviceAuthentication_rqst(Rte_InitValue_DeviceAuthentication_rqst_DeviceAuthentication_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AuthenticationDevice_UICtrl_Rte_Write_DriverAuthDeviceMatching_DriverAuthDeviceMatching(Rte_InitValue_DriverAuthDeviceMatching_DriverAuthDeviceMatching);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  AuthenticationDevice_UICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define AuthenticationDevice_UICtrl_STOP_SEC_CODE
#include "AuthenticationDevice_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void AuthenticationDevice_UICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  ButtonAuth_rqst_T Test_ButtonAuth_rqst_T_V_1 = ButtonAuth_rqst_Idle;
  ButtonAuth_rqst_T Test_ButtonAuth_rqst_T_V_2 = ButtonAuth_rqst_RequestDeviceAuthentication;
  ButtonAuth_rqst_T Test_ButtonAuth_rqst_T_V_3 = ButtonAuth_rqst_Error;
  ButtonAuth_rqst_T Test_ButtonAuth_rqst_T_V_4 = ButtonAuth_rqst_NotAvailable;

  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_1 = DeviceAuthentication_rqst_Idle;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_2 = DeviceAuthentication_rqst_DeviceAuthenticationRequest;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_3 = DeviceAuthentication_rqst_DeviceDeauthenticationRequest;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_4 = DeviceAuthentication_rqst_DeviceMatching;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_5 = DeviceAuthentication_rqst_Spare1;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_6 = DeviceAuthentication_rqst_Spare2;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_7 = DeviceAuthentication_rqst_Error;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_8 = DeviceAuthentication_rqst_NotAvailable;

  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_1 = DoorsAjar_stat_Idle;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_2 = DoorsAjar_stat_BothDoorsAreClosed;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_3 = DoorsAjar_stat_DriverDoorIsOpen;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_4 = DoorsAjar_stat_PassengerDoorIsOpen;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_5 = DoorsAjar_stat_BothDoorsAreOpen;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_6 = DoorsAjar_stat_Spare;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_7 = DoorsAjar_stat_Error;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_8 = DoorsAjar_stat_NotAvailable;

  DriverAuthDeviceMatching_T Test_DriverAuthDeviceMatching_T_V_1 = DriverAuthDeviceMatching_Idle;
  DriverAuthDeviceMatching_T Test_DriverAuthDeviceMatching_T_V_2 = DriverAuthDeviceMatching_DeviceToMatchIsPresent;
  DriverAuthDeviceMatching_T Test_DriverAuthDeviceMatching_T_V_3 = DriverAuthDeviceMatching_Error;
  DriverAuthDeviceMatching_T Test_DriverAuthDeviceMatching_T_V_4 = DriverAuthDeviceMatching_NotAvailable;

  KeyPosition_T Test_KeyPosition_T_V_1 = KeyPosition_KeyOut;
  KeyPosition_T Test_KeyPosition_T_V_2 = KeyPosition_IgnitionKeyInOffPosition;
  KeyPosition_T Test_KeyPosition_T_V_3 = KeyPosition_IgnitionKeyInAccessoryPosition;
  KeyPosition_T Test_KeyPosition_T_V_4 = KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition;
  KeyPosition_T Test_KeyPosition_T_V_5 = KeyPosition_IgnitionKeyInPreheatPosition;
  KeyPosition_T Test_KeyPosition_T_V_6 = KeyPosition_IgnitionKeyInCrankPosition;
  KeyPosition_T Test_KeyPosition_T_V_7 = KeyPosition_ErrorIndicator;
  KeyPosition_T Test_KeyPosition_T_V_8 = KeyPosition_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
