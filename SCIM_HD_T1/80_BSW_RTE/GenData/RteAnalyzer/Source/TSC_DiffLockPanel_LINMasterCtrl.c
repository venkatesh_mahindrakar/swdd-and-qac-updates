/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DiffLockPanel_LINMasterCtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DiffLockPanel_LINMasterCtrl.h"
#include "TSC_DiffLockPanel_LINMasterCtrl.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvRead_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_DiffLockLinCtrl(void)
{
return Rte_IrvRead_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_DiffLockLinCtrl();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_DiffLockLinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_DiffLockLinCtrl( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_DiffLockLinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_DiffLockLinCtrl( data);
}





Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN4_ComMode_LIN(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
{
  return Rte_Read_DiagActiveState_isDiagActive(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_DiagInfoDLFW_DiagInfo(DiagInfo_T *data)
{
  return Rte_Read_DiagInfoDLFW_DiagInfo(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_DifflockOnOff_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_EscButtonMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_EscButtonMuddySiteDeviceInd_DeviceIndication(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_LIN_DifflockDeactivationBtn_st_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_DifflockDeactivationBtn_st_PushButtonStatus(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_LIN_DifflockMode_Wheelstatus_FreeWheel_Status(FreeWheel_Status_T *data)
{
  return Rte_Read_LIN_DifflockMode_Wheelstatus_FreeWheel_Status(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_LIN_EscButtonMuddySiteStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_EscButtonMuddySiteStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_LIN_Offroad_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_Offroad_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_Offroad_Indication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_Offroad_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_ResponseErrorDLFW_ResponseErrorDLFW(ResponseErrorDLFW_T *data)
{
  return Rte_Read_ResponseErrorDLFW_ResponseErrorDLFW(data);
}




Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_DifflockDeactivationBtn_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_DifflockDeactivationBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_DifflockMode_Wheelstatus_FreeWheel_Status(FreeWheel_Status_T data)
{
  return Rte_Write_DifflockMode_Wheelstatus_FreeWheel_Status(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_EscButtonMuddySiteStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_EscButtonMuddySiteStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_LIN_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_DifflockOnOff_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_LIN_EscButtonMuddySiteDeviceIn_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_EscButtonMuddySiteDeviceIn_DeviceIndication(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_LIN_Offroad_Indication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_Offroad_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_Offroad_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_Offroad_ButtonStatus_PushButtonStatus(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BKH_87_DLFWLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BKH_87_DLFWLink_NoResp_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_16_DLFW_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN2_16_DLFW_VBT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_17_DLFW_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN2_17_DLFW_VAT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_44_DLFW_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN2_44_DLFW_RAM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_45_DLFW_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN2_45_DLFW_FLASH_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_46_DLFW_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN2_46_DLFW_EEPROM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_94_DLFW_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN2_94_DLFW_SWFAIL_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvRead_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl(void)
{
return Rte_IrvRead_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl();
}

void TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvWrite_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl(uint8 data)
{
  Rte_IrvWrite_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl( data);
}




boolean  TSC_DiffLockPanel_LINMasterCtrl_Rte_Prm_P1B04_DLFW_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1B04_DLFW_Installed_v();
}


     /* DiffLockPanel_LINMasterCtrl */
      /* DiffLockPanel_LINMasterCtrl */



