/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  PanicFunction_Ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  PanicFunction_Ctrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <PanicFunction_Ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_PanicFunction_Ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_PanicFunction_Ctrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void PanicFunction_Ctrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Enumeration Types:
 * ==================
 * KeyfobPanicButton_Status_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobPanicButton_Status_Neutral (0U)
 *   KeyfobPanicButton_Status_DoublePress (1U)
 *   KeyfobPanicButton_Status_LongPress (2U)
 *   KeyfobPanicButton_Status_Spare_01 (3U)
 *   KeyfobPanicButton_Status_Spare_02 (4U)
 *   KeyfobPanicButton_Status_Spare_03 (5U)
 *   KeyfobPanicButton_Status_Error (6U)
 *   KeyfobPanicButton_Status_NotAvailable (7U)
 * Request_T: Enumeration of integer in interval [0...3] with enumerators
 *   Request_NotRequested (0U)
 *   Request_RequestActive (1U)
 *   Request_Error (2U)
 *   Request_NotAvailable (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void PanicFunction_Ctrl_TestDefines(void)
{
  /* Enumeration Data Types */

  KeyfobPanicButton_Status_T Test_KeyfobPanicButton_Status_T_V_1 = KeyfobPanicButton_Status_Neutral;
  KeyfobPanicButton_Status_T Test_KeyfobPanicButton_Status_T_V_2 = KeyfobPanicButton_Status_DoublePress;
  KeyfobPanicButton_Status_T Test_KeyfobPanicButton_Status_T_V_3 = KeyfobPanicButton_Status_LongPress;
  KeyfobPanicButton_Status_T Test_KeyfobPanicButton_Status_T_V_4 = KeyfobPanicButton_Status_Spare_01;
  KeyfobPanicButton_Status_T Test_KeyfobPanicButton_Status_T_V_5 = KeyfobPanicButton_Status_Spare_02;
  KeyfobPanicButton_Status_T Test_KeyfobPanicButton_Status_T_V_6 = KeyfobPanicButton_Status_Spare_03;
  KeyfobPanicButton_Status_T Test_KeyfobPanicButton_Status_T_V_7 = KeyfobPanicButton_Status_Error;
  KeyfobPanicButton_Status_T Test_KeyfobPanicButton_Status_T_V_8 = KeyfobPanicButton_Status_NotAvailable;

  Request_T Test_Request_T_V_1 = Request_NotRequested;
  Request_T Test_Request_T_V_2 = Request_RequestActive;
  Request_T Test_Request_T_V_3 = Request_Error;
  Request_T Test_Request_T_V_4 = Request_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
