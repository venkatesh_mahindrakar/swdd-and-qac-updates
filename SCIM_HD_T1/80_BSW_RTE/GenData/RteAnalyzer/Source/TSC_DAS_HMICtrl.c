/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DAS_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DAS_HMICtrl.h"
#include "TSC_DAS_HMICtrl.h"








Std_ReturnType TSC_DAS_HMICtrl_Rte_Read_DASApplicationStatus_DASApplicationStatus(DASSystemStatus_T *data)
{
  return Rte_Read_DASApplicationStatus_DASApplicationStatus(data);
}

Std_ReturnType TSC_DAS_HMICtrl_Rte_Read_DAS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_DAS_SwitchStatus_A2PosSwitchStatus(data);
}




Std_ReturnType TSC_DAS_HMICtrl_Rte_Write_DASActivation_rqst_DASActivation_rqst(OffOn_T data)
{
  return Rte_Write_DASActivation_rqst_DASActivation_rqst(data);
}

Std_ReturnType TSC_DAS_HMICtrl_Rte_Write_DAS_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_DAS_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* DAS_HMICtrl */
      /* DAS_HMICtrl */



