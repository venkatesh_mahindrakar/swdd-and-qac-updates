/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_VehicleModeDistribution.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_VehicleModeDistribution.h"
#include "TSC_VehicleModeDistribution.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_VehicleModeDistribution_Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
{
  return Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Read_Living12VPowerStability_Living12VPowerStability(Living12VPowerStability *data)
{
  return Rte_Read_Living12VPowerStability_Living12VPowerStability(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Read_VehicleMode_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleMode_VehicleMode(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Read_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(uint8 *data)
{
  return Rte_Read_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(data);
}




Std_ReturnType TSC_VehicleModeDistribution_Rte_Write_SwcActivation_Accessory_Accessory(VehicleModeDistribution_T data)
{
  return Rte_Write_SwcActivation_Accessory_Accessory(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Write_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T data)
{
  return Rte_Write_SwcActivation_EngineRun_EngineRun(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Write_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T data)
{
  return Rte_Write_SwcActivation_IgnitionOn_IgnitionOn(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Write_SwcActivation_LIN_SwcActivation_LIN(VehicleModeDistribution_T data)
{
  return Rte_Write_SwcActivation_LIN_SwcActivation_LIN(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Write_SwcActivation_Living_Living(VehicleModeDistribution_T data)
{
  return Rte_Write_SwcActivation_Living_Living(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Write_SwcActivation_Parked_Parked(VehicleModeDistribution_T data)
{
  return Rte_Write_SwcActivation_Parked_Parked(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Write_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T data)
{
  return Rte_Write_SwcActivation_Security_SwcActivation_Security(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(const uint8 *data)
{
  return Rte_Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(data);
}

Std_ReturnType TSC_VehicleModeDistribution_Rte_Write_VehicleModeInternal_VehicleMode(VehicleMode_T data)
{
  return Rte_Write_VehicleModeInternal_VehicleMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_VehicleModeDistribution_Rte_Call_UR_ANW_CIOMOperStateRedundancy_ActivateIss(void)
{
  return Rte_Call_UR_ANW_CIOMOperStateRedundancy_ActivateIss();
}
Std_ReturnType TSC_VehicleModeDistribution_Rte_Call_UR_ANW_CIOMOperStateRedundancy_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_CIOMOperStateRedundancy_DeactivateIss();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* VehicleModeDistribution */
      /* VehicleModeDistribution */



