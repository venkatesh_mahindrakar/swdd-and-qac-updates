/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ExteriorLightPanel_2_LINMastCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_DiagInfoELCP2_DiagInfo(DiagInfo_T *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_DRL_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_FogLightFront_ButtonStat_2_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_FogLightRear_ButtonStat_2_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_HeadLampUpDown_SwitchStatu_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_RearWorkProjector_BtnStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_RearWorkProjector_Indication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_ResponseErrorELCP2_ResponseErrorELCP2(ResponseErrorELCP2_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_DRL_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_FogLightFront_ButtonStatus_2_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_FogLightRear_ButtonStatus_2_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_HeadLampUpDown_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_LIN_RearWorkProjector_Indicati_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T data);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_RearWorkProjector_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);

/** Sender receiver - update flag */
boolean TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IsUpdated_LIN_LightMode_Status_2_FreeWheel_Status(void);

/** Service interfaces */
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_16_ELCP2_VBT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_17_ELCP__VAT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_44_ELCP2_RAM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_45_ELCP_FLASH_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_46_ELCP_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_49_ELCP2_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0C_87_ELCP2Link_NoResp_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest2_ActivateIss(void);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest2_DeactivateIss(void);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest2_GetIssState(Issm_IssStateType *issState);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_WLight_InputELCP_ActivateIss(void);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_WLight_InputELCP_DeactivateIss(void);
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_WLight_InputELCP_GetIssState(Issm_IssStateType *issState);

/** Explicit inter-runnable variables */
uint8 TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvRead_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP2LinCtrl(void);
void TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP2LinCtrl(uint8);
void TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP2LinCtrl(uint8);
uint8 TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvRead_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl(void);
void TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvWrite_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl(uint8);

/** Calibration Component Calibration Parameters */
boolean  TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Prm_P1VR5_ELCP2_Installed_v(void);




