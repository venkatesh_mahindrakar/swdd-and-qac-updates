/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_InCabLock_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_BunkH1LockButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_BunkH1UnlockButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_BunkH2LockButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_DashboardLockButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_Locking_Switch_stat_serialized_Crypto_Function_serialized(uint8 *data);
Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Write_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(DoorLockUnlock_T data);




