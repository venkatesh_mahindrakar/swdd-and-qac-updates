/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_TransferCase_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_TransferCase_HMICtrl.h"
#include "TSC_TransferCase_HMICtrl.h"








Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}

Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack(TransferCaseNeutral_T *data)
{
  return Rte_Read_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack(data);
}

Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_TransferCaseNeutral_SwitchStat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_TransferCaseNeutral_SwitchStat_PushButtonStatus(data);
}

Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_TransferCaseNeutral_status_TransferCaseNeutral_status(TransferCaseNeutral_status_T *data)
{
  return Rte_Read_TransferCaseNeutral_status_TransferCaseNeutral_status(data);
}

Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_TransferCasePTOEngaged_TransferCasePTOEngaged(NotEngagedEngaged_T *data)
{
  return Rte_Read_TransferCasePTOEngaged_TransferCasePTOEngaged(data);
}




Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Write_TransferCaseNeutral_DevInd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_TransferCaseNeutral_DevInd_DeviceIndication(data);
}

Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Write_TransferCaseNeutral_Req_TransferCaseNeutral_Req(TransferCaseNeutral_Req2_T data)
{
  return Rte_Write_TransferCaseNeutral_Req_TransferCaseNeutral_Req(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* TransferCase_HMICtrl */
      /* TransferCase_HMICtrl */



