/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VEC_CryptoProxyReceiverSwc.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  VEC_CryptoProxyReceiverSwc
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VEC_CryptoProxyReceiverSwc>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_ComCryptoKey_P1DLX_T
 *   
 *
 * SymDecryptDataBuffer
 *   
 *
 * SymDecryptResultBuffer
 *   
 *
 * SymEncryptDataBuffer
 *   
 *
 * SymEncryptResultBuffer
 *   
 *
 * SymKeyType
 *   
 *
 * UInt16
 *   
 *
 * UInt32
 *   
 *
 *
 * Operation Prototypes:
 * =====================
 * SymDecryptFinish of Port Interface CsmSymDecrypt
 *   
 *
 * SymDecryptStart of Port Interface CsmSymDecrypt
 *   
 *
 * SymDecryptUpdate of Port Interface CsmSymDecrypt
 *   
 *
 * SymEncryptFinish of Port Interface CsmSymEncrypt
 *   
 *
 * SymEncryptStart of Port Interface CsmSymEncrypt
 *   
 *
 * SymEncryptUpdate of Port Interface CsmSymEncrypt
 *   
 *
 *********************************************************************************************************************/

#include "Rte_VEC_CryptoProxyReceiverSwc.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_VEC_CryptoProxyReceiverSwc.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void VEC_CryptoProxyReceiverSwc_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * UInt16: Integer in interval [0...65535]
 * UInt32: Integer in interval [0...4294967295]
 * UInt32_Length: Integer in interval [0...4294967295]
 * UInt8: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * VEC_CryptoProxy_IdentificationState_Type: Enumeration of integer in interval [0...3] with enumerators
 *   VEC_CryptoProxy_Idle (0U)
 *   VEC_CryptoProxy_TransmitNewIdentification (1U)
 *   VEC_CryptoProxy_IdentificationTransmitted (2U)
 *
 * Array Types:
 * ============
 * CryptoIdKey_T: Array with 16 element(s) of type uint8
 * Encrypted128bit_T: Array with 16 element(s) of type uint8
 * Rte_DT_SymKeyType_1: Array with 256 element(s) of type UInt8
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SymDecryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymDecryptResultBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptResultBuffer: Array with 128 element(s) of type UInt8
 * VEC_CryptoProxy_UserSignal: Array with 12 element(s) of type UInt8
 *
 * Record Types:
 * =============
 * SymKeyType: Record with elements
 *   length of type UInt32_Length
 *   data of type Rte_DT_SymKeyType_1
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   UInt16 *Rte_Pim_VEC_CryptoProxy_DelayTimer(Rte_Instance self)
 *   UInt16 *Rte_Pim_VEC_CryptoProxy_ResentTimer(Rte_Instance self)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   UInt16 Rte_CData_VEC_CryptoProxyTimeFactorResentIdentification(Rte_Instance self)
 *   UInt16 Rte_CData_VEC_CryptoProxyTimeFactorTillNewIdentificationKey(Rte_Instance self)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ComCryptoKey_P1DLX_T *Rte_Prm_ComCryptoKey_P1DLX_v(Rte_Instance self)
 *     Returnvalue: SEWS_ComCryptoKey_P1DLX_T* is of type SEWS_ComCryptoKey_P1DLX_a_T
 *
 *********************************************************************************************************************/


#define VEC_CryptoProxyReceiverSwc_START_SEC_CODE
#include "VEC_CryptoProxyReceiverSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxyReceiverMainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_VEC_EncryptedSignal_EncryptedSignal(Rte_Instance self, uint8 *data)
 *     Argument data: uint8* is of type Encrypted128bit_T
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Send_VEC_CryptoIdKey_CryptoIdKey(Rte_Instance self, const uint8 *data)
 *     Argument data: uint8* is of type CryptoIdKey_T
 *   Std_ReturnType Rte_Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized(Rte_Instance self, const UInt8 *data)
 *     Argument data: UInt8* is of type VEC_CryptoProxy_UserSignal
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(Rte_Instance self)
 *   VEC_CryptoProxy_IdentificationState_Type Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(Rte_Instance self, UInt32 data)
 *   void Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(Rte_Instance self, VEC_CryptoProxy_IdentificationState_Type data)
 *   void Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_NewReceivedIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_VEC_IdentificationKeyInit_IdentificationKeyInit(Rte_Instance self, UInt32 *IdentificationKey)
 *     Synchronous Server Invocation. Timeout: None
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptFinish(Rte_Instance self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type SymEncryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK, RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptStart(Rte_Instance self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
 *     Argument InitVectorBuffer: UInt8* is of type SymEncryptDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptUpdate(Rte_Instance self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
 *     Argument plainTextBuffer: UInt8* is of type SymEncryptDataBuffer
 *     Argument cipherTextBuffer: UInt8* is of type SymEncryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK, RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *
 * Status Interfaces:
 * ==================
 *   Tx Acknowledge:
 *   ----------------
 *   Std_ReturnType Rte_Feedback_VEC_CryptoIdKey_CryptoIdKey(Rte_Instance self)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxyReceiverMainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxyReceiverSwc_CODE) VEC_CryptoProxyReceiverMainFunction(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxyReceiverMainFunction
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Encrypted128bit_T Receive_VEC_EncryptedSignal_EncryptedSignal;

  UInt16 PimVEC_CryptoProxy_DelayTimer;
  UInt16 PimVEC_CryptoProxy_ResentTimer;

  UInt16 VEC_CryptoProxyTimeFactorResentIdentification_data;
  UInt16 VEC_CryptoProxyTimeFactorTillNewIdentificationKey_data;

  SEWS_ComCryptoKey_P1DLX_a_T ComCryptoKey_P1DLX_v_data;

  CryptoIdKey_T Send_VEC_CryptoIdKey_CryptoIdKey = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  VEC_CryptoProxy_UserSignal Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized;

  UInt32 VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber;
  VEC_CryptoProxy_IdentificationState_Type VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState;

  UInt32 Call_VEC_IdentificationKeyInit_IdentificationKeyInit_IdentificationKey = 0U;

  SymEncryptResultBuffer Call_CsmSymEncrypt_SymEncryptFinish_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32_Length Call_CsmSymEncrypt_SymEncryptFinish_cipherTextLength = 0U;
  SymKeyType Call_CsmSymEncrypt_SymEncryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  SymEncryptDataBuffer Call_CsmSymEncrypt_SymEncryptStart_InitVectorBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  SymEncryptDataBuffer Call_CsmSymEncrypt_SymEncryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  SymEncryptResultBuffer Call_CsmSymEncrypt_SymEncryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32_Length Call_CsmSymEncrypt_SymEncryptUpdate_cipherTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimVEC_CryptoProxy_DelayTimer = *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_DelayTimer(self);
  *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_DelayTimer(self) = PimVEC_CryptoProxy_DelayTimer;
  PimVEC_CryptoProxy_ResentTimer = *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_ResentTimer(self);
  *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_ResentTimer(self) = PimVEC_CryptoProxy_ResentTimer;

  VEC_CryptoProxyTimeFactorResentIdentification_data = TSC_VEC_CryptoProxyReceiverSwc_Rte_CData_VEC_CryptoProxyTimeFactorResentIdentification(self);
  VEC_CryptoProxyTimeFactorTillNewIdentificationKey_data = TSC_VEC_CryptoProxyReceiverSwc_Rte_CData_VEC_CryptoProxyTimeFactorTillNewIdentificationKey(self);

  (void)memcpy(ComCryptoKey_P1DLX_v_data, TSC_VEC_CryptoProxyReceiverSwc_Rte_Prm_ComCryptoKey_P1DLX_v(self), sizeof(SEWS_ComCryptoKey_P1DLX_a_T));

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Receive_VEC_EncryptedSignal_EncryptedSignal(self, Receive_VEC_EncryptedSignal_EncryptedSignal);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Send_VEC_CryptoIdKey_CryptoIdKey(self, Send_VEC_CryptoIdKey_CryptoIdKey);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  (void)memset(&Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized, 0, sizeof(Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized));
  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized(self, Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber = TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(self);
  VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState = TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(self);

  TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(self, 4294967295U);
  TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(self, 1U);
  TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_NewReceivedIdentificationNumber(self, 0U);

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_VEC_IdentificationKeyInit_IdentificationKeyInit(self, &Call_VEC_IdentificationKeyInit_IdentificationKeyInit_IdentificationKey);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(self, Call_CsmSymEncrypt_SymEncryptFinish_cipherTextBuffer, &Call_CsmSymEncrypt_SymEncryptFinish_cipherTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(self, &Call_CsmSymEncrypt_SymEncryptStart_key, Call_CsmSymEncrypt_SymEncryptStart_InitVectorBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(self, Call_CsmSymEncrypt_SymEncryptUpdate_plainTextBuffer, 0U, Call_CsmSymEncrypt_SymEncryptUpdate_cipherTextBuffer, &Call_CsmSymEncrypt_SymEncryptUpdate_cipherTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Feedback_VEC_CryptoIdKey_CryptoIdKey(self);
  switch (fct_status)
  {
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TRANSMIT_ACK:
      fct_error = 1;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
  }

  VEC_CryptoProxyReceiverSwc_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxyReceiverReception
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <EncryptedSignal> of PortPrototype <VEC_EncryptedSignal>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_VEC_EncryptedSignal_EncryptedSignal(Rte_Instance self, uint8 *data)
 *     Argument data: uint8* is of type Encrypted128bit_T
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized(Rte_Instance self, const UInt8 *data)
 *     Argument data: UInt8* is of type VEC_CryptoProxy_UserSignal
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(Rte_Instance self)
 *   VEC_CryptoProxy_IdentificationState_Type Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(Rte_Instance self)
 *   UInt32 Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(Rte_Instance self, UInt32 data)
 *   void Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(Rte_Instance self, VEC_CryptoProxy_IdentificationState_Type data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptFinish(Rte_Instance self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type SymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK, RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptStart(Rte_Instance self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
 *     Argument InitVectorBuffer: UInt8* is of type SymDecryptDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptUpdate(Rte_Instance self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type SymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type SymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK, RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxyReceiverReception_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxyReceiverSwc_CODE) VEC_CryptoProxyReceiverReception(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxyReceiverReception
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Encrypted128bit_T Receive_VEC_EncryptedSignal_EncryptedSignal;

  UInt16 PimVEC_CryptoProxy_DelayTimer;
  UInt16 PimVEC_CryptoProxy_ResentTimer;

  UInt16 VEC_CryptoProxyTimeFactorResentIdentification_data;
  UInt16 VEC_CryptoProxyTimeFactorTillNewIdentificationKey_data;

  SEWS_ComCryptoKey_P1DLX_a_T ComCryptoKey_P1DLX_v_data;

  VEC_CryptoProxy_UserSignal Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized;

  UInt32 VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber;
  VEC_CryptoProxy_IdentificationState_Type VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState;
  UInt32 VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber;

  SymDecryptResultBuffer Call_CsmSymDecrypt_SymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32_Length Call_CsmSymDecrypt_SymDecryptFinish_plainTextLength = 0U;
  SymKeyType Call_CsmSymDecrypt_SymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  SymDecryptDataBuffer Call_CsmSymDecrypt_SymDecryptStart_InitVectorBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  SymDecryptDataBuffer Call_CsmSymDecrypt_SymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  SymDecryptResultBuffer Call_CsmSymDecrypt_SymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32_Length Call_CsmSymDecrypt_SymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimVEC_CryptoProxy_DelayTimer = *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_DelayTimer(self);
  *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_DelayTimer(self) = PimVEC_CryptoProxy_DelayTimer;
  PimVEC_CryptoProxy_ResentTimer = *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_ResentTimer(self);
  *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_ResentTimer(self) = PimVEC_CryptoProxy_ResentTimer;

  VEC_CryptoProxyTimeFactorResentIdentification_data = TSC_VEC_CryptoProxyReceiverSwc_Rte_CData_VEC_CryptoProxyTimeFactorResentIdentification(self);
  VEC_CryptoProxyTimeFactorTillNewIdentificationKey_data = TSC_VEC_CryptoProxyReceiverSwc_Rte_CData_VEC_CryptoProxyTimeFactorTillNewIdentificationKey(self);

  (void)memcpy(ComCryptoKey_P1DLX_v_data, TSC_VEC_CryptoProxyReceiverSwc_Rte_Prm_ComCryptoKey_P1DLX_v(self), sizeof(SEWS_ComCryptoKey_P1DLX_a_T));

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Receive_VEC_EncryptedSignal_EncryptedSignal(self, Receive_VEC_EncryptedSignal_EncryptedSignal);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  (void)memset(&Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized, 0, sizeof(Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized));
  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized(self, Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber = TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(self);
  VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState = TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(self);
  VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber = TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber(self);

  TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(self, 4294967295U);
  TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(self, 1U);

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(self, Call_CsmSymDecrypt_SymDecryptFinish_plainTextBuffer, &Call_CsmSymDecrypt_SymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(self, &Call_CsmSymDecrypt_SymDecryptStart_key, Call_CsmSymDecrypt_SymDecryptStart_InitVectorBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(self, Call_CsmSymDecrypt_SymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmSymDecrypt_SymDecryptUpdate_plainTextBuffer, &Call_CsmSymDecrypt_SymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxyReceiverSwc_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxyReceiverSwc_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxyReceiverSwc_CODE) VEC_CryptoProxyReceiverSwc_Init(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxyReceiverSwc_Init
 *********************************************************************************************************************/

  UInt16 PimVEC_CryptoProxy_DelayTimer;
  UInt16 PimVEC_CryptoProxy_ResentTimer;

  UInt16 VEC_CryptoProxyTimeFactorResentIdentification_data;
  UInt16 VEC_CryptoProxyTimeFactorTillNewIdentificationKey_data;

  SEWS_ComCryptoKey_P1DLX_a_T ComCryptoKey_P1DLX_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimVEC_CryptoProxy_DelayTimer = *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_DelayTimer(self);
  *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_DelayTimer(self) = PimVEC_CryptoProxy_DelayTimer;
  PimVEC_CryptoProxy_ResentTimer = *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_ResentTimer(self);
  *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_ResentTimer(self) = PimVEC_CryptoProxy_ResentTimer;

  VEC_CryptoProxyTimeFactorResentIdentification_data = TSC_VEC_CryptoProxyReceiverSwc_Rte_CData_VEC_CryptoProxyTimeFactorResentIdentification(self);
  VEC_CryptoProxyTimeFactorTillNewIdentificationKey_data = TSC_VEC_CryptoProxyReceiverSwc_Rte_CData_VEC_CryptoProxyTimeFactorTillNewIdentificationKey(self);

  (void)memcpy(ComCryptoKey_P1DLX_v_data, TSC_VEC_CryptoProxyReceiverSwc_Rte_Prm_ComCryptoKey_P1DLX_v(self), sizeof(SEWS_ComCryptoKey_P1DLX_a_T));


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VEC_CryptoProxyReceiverSwc_STOP_SEC_CODE
#include "VEC_CryptoProxyReceiverSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void VEC_CryptoProxyReceiverSwc_TestDefines(void)
{
  /* Enumeration Data Types */

  VEC_CryptoProxy_IdentificationState_Type Test_VEC_CryptoProxy_IdentificationState_Type_V_1 = VEC_CryptoProxy_Idle;
  VEC_CryptoProxy_IdentificationState_Type Test_VEC_CryptoProxy_IdentificationState_Type_V_2 = VEC_CryptoProxy_TransmitNewIdentification;
  VEC_CryptoProxy_IdentificationState_Type Test_VEC_CryptoProxy_IdentificationState_Type_V_3 = VEC_CryptoProxy_IdentificationTransmitted;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
