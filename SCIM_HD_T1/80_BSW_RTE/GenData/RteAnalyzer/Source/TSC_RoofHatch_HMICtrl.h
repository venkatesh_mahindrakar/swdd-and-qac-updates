/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_RoofHatch_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_RoofHatch_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Write_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst(RoofHatch_HMI_rqst_T data);

/** Calibration Component Calibration Parameters */
SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T  TSC_RoofHatch_HMICtrl_Rte_Prm_P1GCD_RoofHatch_FlexibleSwitchLogic_v(void);
boolean  TSC_RoofHatch_HMICtrl_Rte_Prm_P1CW8_RoofHatchCtrl_Act_v(void);




