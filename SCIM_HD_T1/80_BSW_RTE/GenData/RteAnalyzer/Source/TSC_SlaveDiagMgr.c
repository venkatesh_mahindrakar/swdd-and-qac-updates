/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SlaveDiagMgr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_SlaveDiagMgr.h"
#include "TSC_SlaveDiagMgr.h"








Std_ReturnType TSC_SlaveDiagMgr_Rte_Read_DiagInfoERAU_DiagInfo(DiagInfo_T *data)
{
  return Rte_Read_DiagInfoERAU_DiagInfo(data);
}

Std_ReturnType TSC_SlaveDiagMgr_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}








     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D72_44_EraU_DataMemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D72_44_EraU_DataMemoryFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D72_45_EraU_ProgramMemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D72_45_EraU_ProgramMemoryFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D72_46_EraU_ParameterMemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D72_46_EraU_ParameterMemoryFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D72_49_EraU_InternalElectronicFailure49_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D72_49_EraU_InternalElectronicFailure49_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D72_4A_EraU_IncorrectComponentInstalled_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D72_4A_EraU_IncorrectComponentInstalled_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D72_55_EraU_NotConfigured_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D72_55_EraU_NotConfigured_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D72_92_EraU_PerfIncorrectOperation_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D72_92_EraU_PerfIncorrectOperation_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D72_96_EraU_ComponentInternalFailure96_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D72_96_EraU_ComponentInternalFailure96_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D72_9A_EraU_CompOrSysOperatingCond_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D72_9A_EraU_CompOrSysOperatingCond_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D73_01_EraU_GpsAntennaElecFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D73_01_EraU_GpsAntennaElecFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D74_01_EraU_GsmAntennaElecFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D74_01_EraU_GsmAntennaElecFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D75_01_EraU_MicroElecFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D75_01_EraU_MicroElecFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1D76_01_EraU_SpeakerElecFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1D76_01_EraU_SpeakerElecFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SlaveDiagMgr_Rte_Call_Event_D1E8B_01_EraU_BatteryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1E8B_01_EraU_BatteryFailure_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





boolean  TSC_SlaveDiagMgr_Rte_Prm_P1M7Q_EraGlonassUnitInstalled_v(void)
{
  return (boolean ) Rte_Prm_P1M7Q_EraGlonassUnitInstalled_v();
}


     /* SlaveDiagMgr */
      /* SlaveDiagMgr */



