/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ServiceBraking_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_ServiceBraking_HMICtrl.h"
#include "TSC_ServiceBraking_HMICtrl.h"








Std_ReturnType TSC_ServiceBraking_HMICtrl_Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_ServiceBraking_HMICtrl_Rte_Read_ABSInhibitionStatus_ABSInhibitionStatus(Inhibit_T *data)
{
  return Rte_Read_ABSInhibitionStatus_ABSInhibitionStatus(data);
}

Std_ReturnType TSC_ServiceBraking_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}

Std_ReturnType TSC_ServiceBraking_HMICtrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
{
  return Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(data);
}




Std_ReturnType TSC_ServiceBraking_HMICtrl_Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest(OffOn_T data)
{
  return Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_ServiceBraking_HMICtrl_Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch(PassiveActive_T *data)
{
  return Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch(data);
}

Std_ReturnType TSC_ServiceBraking_HMICtrl_Rte_Read_HillStartAidButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_HillStartAidButtonStatus_PushButtonStatus(data);
}




Std_ReturnType TSC_ServiceBraking_HMICtrl_Rte_Write_HSADriverRequest_HSADriverRequest(InactiveActive_T data)
{
  return Rte_Write_HSADriverRequest_HSADriverRequest(data);
}

Std_ReturnType TSC_ServiceBraking_HMICtrl_Rte_Write_HillStartAid_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_HillStartAid_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_ABS_Inhibit_SwType_P1SY6_T  TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY6_ABS_Inhibit_SwType_v(void)
{
  return (SEWS_ABS_Inhibit_SwType_P1SY6_T ) Rte_Prm_P1SY6_ABS_Inhibit_SwType_v();
}
boolean  TSC_ServiceBraking_HMICtrl_Rte_Prm_P1A1R_HSA_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1A1R_HSA_Installed_v();
}
boolean  TSC_ServiceBraking_HMICtrl_Rte_Prm_P1NTV_HSA_DefaultConfig_v(void)
{
  return (boolean ) Rte_Prm_P1NTV_HSA_DefaultConfig_v();
}
boolean  TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY4_ABS_Inhibit_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1SY4_ABS_Inhibit_Installed_v();
}


     /* ServiceBraking_HMICtrl */
      /* ServiceBraking_HMICtrl */



