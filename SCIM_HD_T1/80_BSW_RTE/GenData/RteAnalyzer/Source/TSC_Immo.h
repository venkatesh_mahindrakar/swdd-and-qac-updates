/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Immo.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_Immo_Rte_Read_AddrParP1DS4_stat_dataP1DS4(SEWS_KeyfobEncryptCode_P1DS4_T *data);
Std_ReturnType TSC_Immo_Rte_Read_KeyFobNV_PR_KeyFobNV(uint8 *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_Immo_Rte_Write_KeyFobNV_PR_KeyFobNV(const uint8 *data);

/** Client server interfaces */
Std_ReturnType TSC_Immo_Rte_Call_SetupDstTelegram_SetDstTelegram(uint8 Dst_Order);
Std_ReturnType TSC_Immo_Rte_Call_TimeoutTxTelegram_CS(void);

/** Explicit inter-runnable variables */
void TSC_Immo_Rte_IrvWrite_ImmoProcessingRqst_ImmoCircuitProcessing_stopByUser(Boolean);
Boolean TSC_Immo_Rte_IrvRead_Immo_10ms_runnable_stopByUser(void);




