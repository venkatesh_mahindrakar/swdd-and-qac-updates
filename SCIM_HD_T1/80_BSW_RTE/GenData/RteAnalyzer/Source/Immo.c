/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Immo.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  Immo
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <Immo>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * SEWS_KeyfobEncryptCode_P1DS4_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_Immo.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_Immo.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void Immo_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_KeyfobEncryptCode_P1DS4_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * SCIM_ImmoDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 *   ImmoDriver_ProcessingStatus_Idle (0U)
 *   ImmoDriver_ProcessingStatus_Ongoing (1U)
 *   ImmoDriver_ProcessingStatus_LfSent (2U)
 *   ImmoDriver_ProcessingStatus_DecryptedInList (3U)
 *   ImmoDriver_ProcessingStatus_DecryptedNotInList (4U)
 *   ImmoDriver_ProcessingStatus_NotDecrypted (5U)
 *   ImmoDriver_ProcessingStatus_HwError (6U)
 * SCIM_ImmoType_T: Enumeration of integer in interval [0...255] with enumerators
 *   ImmoType_ATA5702 (0U)
 *   ImmoType_ATA5577 (1U)
 *
 * Array Types:
 * ============
 * KeyFobNVM_T: Array with 96 element(s) of type uint8
 * SEWS_KeyfobEncryptCode_P1DS4_a_T: Array with 24 element(s) of type SEWS_KeyfobEncryptCode_P1DS4_T
 *
 *********************************************************************************************************************/


#define Immo_START_SEC_CODE
#include "Immo_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ImmoProcessingRqst_GetImmoCircuitProcessingResult
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetImmoCircuitProcessingResult> of PortPrototype <ImmoProcessingRqst>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void GetImmoCircuitProcessingResult(SCIM_ImmoDriver_ProcessingStatus_T *ImmoProcessingStatus)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ImmoProcessingRqst_GetImmoCircuitProcessingResult_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Immo_CODE) GetImmoCircuitProcessingResult(P2VAR(SCIM_ImmoDriver_ProcessingStatus_T, AUTOMATIC, RTE_IMMO_APPL_VAR) ImmoProcessingStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: GetImmoCircuitProcessingResult
 *********************************************************************************************************************/

  Immo_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ImmoProcessingRqst_ImmoCircuitProcessing
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ImmoCircuitProcessing> of PortPrototype <ImmoProcessingRqst>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_ImmoProcessingRqst_ImmoCircuitProcessing_stopByUser(Boolean data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void ImmoCircuitProcessing(SCIM_ImmoType_T ImmoType, Boolean AuthRequestedbyImmo)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ImmoProcessingRqst_ImmoCircuitProcessing_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Immo_CODE) ImmoCircuitProcessing(SCIM_ImmoType_T ImmoType, Boolean AuthRequestedbyImmo) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ImmoCircuitProcessing
 *********************************************************************************************************************/

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  TSC_Immo_Rte_IrvWrite_ImmoProcessingRqst_ImmoCircuitProcessing_stopByUser(FALSE);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Immo_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AddrParP1DS4_stat_dataP1DS4(SEWS_KeyfobEncryptCode_P1DS4_T *data)
 *     Argument data: SEWS_KeyfobEncryptCode_P1DS4_T* is of type SEWS_KeyfobEncryptCode_P1DS4_a_T
 *   Std_ReturnType Rte_Read_KeyFobNV_PR_KeyFobNV(uint8 *data)
 *     Argument data: uint8* is of type KeyFobNVM_T
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_KeyFobNV_PR_KeyFobNV(const uint8 *data)
 *     Argument data: uint8* is of type KeyFobNVM_T
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Boolean Rte_IrvRead_Immo_10ms_runnable_stopByUser(void)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_SetupDstTelegram_SetDstTelegram(uint8 Dst_Order)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_TimeoutTxTelegram_CS(void)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Immo_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Immo_CODE) Immo_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Immo_10ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_KeyfobEncryptCode_P1DS4_a_T Read_AddrParP1DS4_stat_dataP1DS4;
  KeyFobNVM_T Read_KeyFobNV_PR_KeyFobNV;

  KeyFobNVM_T Write_KeyFobNV_PR_KeyFobNV;

  Boolean Immo_10ms_runnable_stopByUser;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_Immo_Rte_Read_AddrParP1DS4_stat_dataP1DS4(Read_AddrParP1DS4_stat_dataP1DS4);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Immo_Rte_Read_KeyFobNV_PR_KeyFobNV(Read_KeyFobNV_PR_KeyFobNV);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  (void)memset(&Write_KeyFobNV_PR_KeyFobNV, 0, sizeof(Write_KeyFobNV_PR_KeyFobNV));
  fct_status = TSC_Immo_Rte_Write_KeyFobNV_PR_KeyFobNV(Write_KeyFobNV_PR_KeyFobNV);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  Immo_10ms_runnable_stopByUser = TSC_Immo_Rte_IrvRead_Immo_10ms_runnable_stopByUser();

  fct_status = TSC_Immo_Rte_Call_SetupDstTelegram_SetDstTelegram(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Immo_Rte_Call_TimeoutTxTelegram_CS();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: KeyfobMatchingOperations_GetMatchingStatus
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetMatchingStatus> of PortPrototype <KeyfobMatchingOperations>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void GetMatchingStatus(uint8 *matchingStatus, uint8 *matchedKeyfobCount)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: KeyfobMatchingOperations_GetMatchingStatus_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Immo_CODE) GetMatchingStatus(P2VAR(uint8, AUTOMATIC, RTE_IMMO_APPL_VAR) matchingStatus, P2VAR(uint8, AUTOMATIC, RTE_IMMO_APPL_VAR) matchedKeyfobCount) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: GetMatchingStatus
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: KeyfobMatchingOperations_MatchKeyfobByLF
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <MatchKeyfobByLF> of PortPrototype <KeyfobMatchingOperations>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void MatchKeyfobByLF(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: KeyfobMatchingOperations_MatchKeyfobByLF_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Immo_CODE) MatchKeyfobByLF(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: MatchKeyfobByLF
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: KeyfobMatchingOperations_ReinitializeMatchingList
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReinitializeMatchingList> of PortPrototype <KeyfobMatchingOperations>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void ReinitializeMatchingList(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: KeyfobMatchingOperations_ReinitializeMatchingList_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Immo_CODE) ReinitializeMatchingList(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ReinitializeMatchingList
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Immo_STOP_SEC_CODE
#include "Immo_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void Immo_TestDefines(void)
{
  /* Enumeration Data Types */

  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_1 = ImmoDriver_ProcessingStatus_Idle;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_2 = ImmoDriver_ProcessingStatus_Ongoing;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_3 = ImmoDriver_ProcessingStatus_LfSent;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_4 = ImmoDriver_ProcessingStatus_DecryptedInList;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_5 = ImmoDriver_ProcessingStatus_DecryptedNotInList;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_6 = ImmoDriver_ProcessingStatus_NotDecrypted;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_7 = ImmoDriver_ProcessingStatus_HwError;

  SCIM_ImmoType_T Test_SCIM_ImmoType_T_V_1 = ImmoType_ATA5702;
  SCIM_ImmoType_T Test_SCIM_ImmoType_T_V_2 = ImmoType_ATA5577;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
