/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SwivelSeat_ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_SwivelSeat_ctrl.h"
#include "TSC_SwivelSeat_ctrl.h"








Std_ReturnType TSC_SwivelSeat_ctrl_Rte_Read_SeatSwivelStatus_SeatSwivelStatus(SeatSwivelStatus_T *data)
{
  return Rte_Read_SeatSwivelStatus_SeatSwivelStatus(data);
}

Std_ReturnType TSC_SwivelSeat_ctrl_Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_EngineRun_EngineRun(data);
}

Std_ReturnType TSC_SwivelSeat_ctrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
{
  return Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(data);
}




Std_ReturnType TSC_SwivelSeat_ctrl_Rte_Write_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst(InactiveActive_T data)
{
  return Rte_Write_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T  TSC_SwivelSeat_ctrl_Rte_Prm_P1DWQ_SwivelSeatCheck_SetSpeed_v(void)
{
  return (SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T ) Rte_Prm_P1DWQ_SwivelSeatCheck_SetSpeed_v();
}
boolean  TSC_SwivelSeat_ctrl_Rte_Prm_P1CUD_SwivelSeatWarning_Act_v(void)
{
  return (boolean ) Rte_Prm_P1CUD_SwivelSeatWarning_Act_v();
}


     /* SwivelSeat_ctrl */
      /* SwivelSeat_ctrl */



