/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_VOL_DIDServer.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_VOL_DIDServer_Rte_Read_AmbientAirTemperature_AmbientAirTemperature(Temperature16bit_T *data);
Std_ReturnType TSC_VOL_DIDServer_Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(Distance32bit_T *data);
Std_ReturnType TSC_VOL_DIDServer_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);

/** Client server interfaces */
Std_ReturnType TSC_VOL_DIDServer_Rte_Call_CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo);
Std_ReturnType TSC_VOL_DIDServer_Rte_Call_CddLinDiagServices_SlaveNodePnSnResp(LinDiagServiceStatus *DiagServiceStatus, uint8 *NoOfLinSlaves, uint8 *LinDiagRespPNSN);

/** Mode switches */
uint8 TSC_VOL_DIDServer_Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(void);

/** Calibration Component Calibration Parameters */
boolean  TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v(void);
boolean  TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v(void);
SEWS_ChassisId_CHANO_T * TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(void);
SEWS_VIN_VINNO_T * TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(void);




