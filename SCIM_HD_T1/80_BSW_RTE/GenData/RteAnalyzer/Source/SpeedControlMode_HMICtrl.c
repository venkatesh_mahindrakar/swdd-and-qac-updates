/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SpeedControlMode_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  SpeedControlMode_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <SpeedControlMode_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_HeadwaySupport_P1BEX_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_SpeedControlMode_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_SpeedControlMode_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void SpeedControlMode_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_HeadwaySupport_P1BEX_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * CCIM_ACC_T: Enumeration of integer in interval [0...7] with enumerators
 *   CCIM_ACC_ACCInactive (0U)
 *   CCIM_ACC_ACC1Active (1U)
 *   CCIM_ACC_ACC2Active (2U)
 *   CCIM_ACC_ACC3Active (3U)
 *   CCIM_ACC_Reserved (4U)
 *   CCIM_ACC_Reserved_01 (5U)
 *   CCIM_ACC_Error (6U)
 *   CCIM_ACC_NotAvailable (7U)
 * CCStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   CCStates_OffDisabled (0U)
 *   CCStates_Hold (1U)
 *   CCStates_Accelerate (2U)
 *   CCStates_Decelerate (3U)
 *   CCStates_Resume (4U)
 *   CCStates_Set (5U)
 *   CCStates_Driver_Override (6U)
 *   CCStates_Spare_1 (7U)
 *   CCStates_Spare_2 (8U)
 *   CCStates_Spare_3 (9U)
 *   CCStates_Spare_4 (10U)
 *   CCStates_Spare_5 (11U)
 *   CCStates_Spare_6 (12U)
 *   CCStates_Spare_7 (13U)
 *   CCStates_Error (14U)
 *   CCStates_NotAvailable (15U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DriverMemory_rqst_T: Enumeration of integer in interval [0...31] with enumerators
 *   DriverMemory_rqst_UseDefaultDriverMemory (0U)
 *   DriverMemory_rqst_UseDriverMemory1 (1U)
 *   DriverMemory_rqst_UseDriverMemory2 (2U)
 *   DriverMemory_rqst_UseDriverMemory3 (3U)
 *   DriverMemory_rqst_UseDriverMemory4 (4U)
 *   DriverMemory_rqst_UseDriverMemory5 (5U)
 *   DriverMemory_rqst_UseDriverMemory6 (6U)
 *   DriverMemory_rqst_UseDriverMemory7 (7U)
 *   DriverMemory_rqst_UseDriverMemory8 (8U)
 *   DriverMemory_rqst_UseDriverMemory9 (9U)
 *   DriverMemory_rqst_UseDriverMemory10 (10U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory1 (11U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory2 (12U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory3 (13U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory4 (14U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory5 (15U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory6 (16U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory7 (17U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory8 (18U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory9 (19U)
 *   DriverMemory_rqst_ResetAndThenUseDriverMemory10 (20U)
 *   DriverMemory_rqst_ResetAllMemThenUseDefDriverMem (21U)
 *   DriverMemory_rqst_Spare (22U)
 *   DriverMemory_rqst_Spare_01 (23U)
 *   DriverMemory_rqst_Spare_02 (24U)
 *   DriverMemory_rqst_Spare_03 (25U)
 *   DriverMemory_rqst_Spare_04 (26U)
 *   DriverMemory_rqst_Spare_05 (27U)
 *   DriverMemory_rqst_Spare_06 (28U)
 *   DriverMemory_rqst_Spare_07 (29U)
 *   DriverMemory_rqst_NotAvailable (30U)
 *   DriverMemory_rqst_Error (31U)
 * FWSelectedSpeedControlMode_T: Enumeration of integer in interval [0...7] with enumerators
 *   FWSelectedSpeedControlMode_Off (0U)
 *   FWSelectedSpeedControlMode_CruiseControl (1U)
 *   FWSelectedSpeedControlMode_AdjustableSpeedLimiter (2U)
 *   FWSelectedSpeedControlMode_AdaptiveCruiseControl (3U)
 *   FWSelectedSpeedControlMode_Spare_01 (4U)
 *   FWSelectedSpeedControlMode_Spare_02 (5U)
 *   FWSelectedSpeedControlMode_Error (6U)
 *   FWSelectedSpeedControlMode_NotAvailable (7U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * XRSLStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   XRSLStates_Off_disabled (0U)
 *   XRSLStates_Hold (1U)
 *   XRSLStates_Accelerate (2U)
 *   XRSLStates_Decelerate (3U)
 *   XRSLStates_Resume (4U)
 *   XRSLStates_Set (5U)
 *   XRSLStates_Driver_override (6U)
 *   XRSLStates_Error (14U)
 *   XRSLStates_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * SpeedControl_NVM_T: Array with 16 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HeadwaySupport_P1BEX_T Rte_Prm_P1BEX_HeadwaySupport_v(void)
 *   boolean Rte_Prm_P1B2C_CCFW_Installed_v(void)
 *   boolean Rte_Prm_P1EXP_PersonalSettingsForCC_v(void)
 *
 *********************************************************************************************************************/


#define SpeedControlMode_HMICtrl_START_SEC_CODE
#include "SpeedControlMode_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SpeedControlMode_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_CCStates_CCStates(CCStates_T *data)
 *   Std_ReturnType Rte_Read_DriverMemory_rqst_DriverMemory_rqst(DriverMemory_rqst_T *data)
 *   Std_ReturnType Rte_Read_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type SpeedControl_NVM_T
 *   Std_ReturnType Rte_Read_SpeedControlModeButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SpeedControlModeWheelStatus_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_XRSLStates_XRSLStates(XRSLStates_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ACCOrCCIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ASLIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FWSelectedACCMode_CCIM_ACC(CCIM_ACC_T data)
 *   Std_ReturnType Rte_Write_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode(FWSelectedSpeedControlMode_T data)
 *   Std_ReturnType Rte_Write_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent(OffOn_T data)
 *   Std_ReturnType Rte_Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type SpeedControl_NVM_T
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SpeedControlMode_HMICtrl_CODE) SpeedControlMode_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  CCStates_T Read_CCStates_CCStates;
  DriverMemory_rqst_T Read_DriverMemory_rqst_DriverMemory_rqst;
  SpeedControl_NVM_T Read_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I;
  PushButtonStatus_T Read_SpeedControlModeButtonStatus_PushButtonStatus;
  FreeWheel_Status_T Read_SpeedControlModeWheelStatus_FreeWheel_Status;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;
  XRSLStates_T Read_XRSLStates_XRSLStates;

  SEWS_HeadwaySupport_P1BEX_T P1BEX_HeadwaySupport_v_data;
  boolean P1B2C_CCFW_Installed_v_data;
  boolean P1EXP_PersonalSettingsForCC_v_data;

  SpeedControl_NVM_T Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1BEX_HeadwaySupport_v_data = TSC_SpeedControlMode_HMICtrl_Rte_Prm_P1BEX_HeadwaySupport_v();
  P1B2C_CCFW_Installed_v_data = TSC_SpeedControlMode_HMICtrl_Rte_Prm_P1B2C_CCFW_Installed_v();
  P1EXP_PersonalSettingsForCC_v_data = TSC_SpeedControlMode_HMICtrl_Rte_Prm_P1EXP_PersonalSettingsForCC_v();

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Read_CCStates_CCStates(&Read_CCStates_CCStates);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Read_DriverMemory_rqst_DriverMemory_rqst(&Read_DriverMemory_rqst_DriverMemory_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Read_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(Read_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Read_SpeedControlModeButtonStatus_PushButtonStatus(&Read_SpeedControlModeButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Read_SpeedControlModeWheelStatus_FreeWheel_Status(&Read_SpeedControlModeWheelStatus_FreeWheel_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Read_XRSLStates_XRSLStates(&Read_XRSLStates_XRSLStates);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Write_ACCOrCCIndication_DeviceIndication(Rte_InitValue_ACCOrCCIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Write_ASLIndication_DeviceIndication(Rte_InitValue_ASLIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Write_FWSelectedACCMode_CCIM_ACC(Rte_InitValue_FWSelectedACCMode_CCIM_ACC);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Write_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode(Rte_InitValue_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Write_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent(Rte_InitValue_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I, 0, sizeof(Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I));
  fct_status = TSC_SpeedControlMode_HMICtrl_Rte_Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  SpeedControlMode_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SpeedControlMode_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SpeedControlMode_HMICtrl_CODE) SpeedControlMode_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SpeedControlMode_HMICtrl_Init
 *********************************************************************************************************************/

  SEWS_HeadwaySupport_P1BEX_T P1BEX_HeadwaySupport_v_data;
  boolean P1B2C_CCFW_Installed_v_data;
  boolean P1EXP_PersonalSettingsForCC_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1BEX_HeadwaySupport_v_data = TSC_SpeedControlMode_HMICtrl_Rte_Prm_P1BEX_HeadwaySupport_v();
  P1B2C_CCFW_Installed_v_data = TSC_SpeedControlMode_HMICtrl_Rte_Prm_P1B2C_CCFW_Installed_v();
  P1EXP_PersonalSettingsForCC_v_data = TSC_SpeedControlMode_HMICtrl_Rte_Prm_P1EXP_PersonalSettingsForCC_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SpeedControlMode_HMICtrl_STOP_SEC_CODE
#include "SpeedControlMode_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void SpeedControlMode_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  CCIM_ACC_T Test_CCIM_ACC_T_V_1 = CCIM_ACC_ACCInactive;
  CCIM_ACC_T Test_CCIM_ACC_T_V_2 = CCIM_ACC_ACC1Active;
  CCIM_ACC_T Test_CCIM_ACC_T_V_3 = CCIM_ACC_ACC2Active;
  CCIM_ACC_T Test_CCIM_ACC_T_V_4 = CCIM_ACC_ACC3Active;
  CCIM_ACC_T Test_CCIM_ACC_T_V_5 = CCIM_ACC_Reserved;
  CCIM_ACC_T Test_CCIM_ACC_T_V_6 = CCIM_ACC_Reserved_01;
  CCIM_ACC_T Test_CCIM_ACC_T_V_7 = CCIM_ACC_Error;
  CCIM_ACC_T Test_CCIM_ACC_T_V_8 = CCIM_ACC_NotAvailable;

  CCStates_T Test_CCStates_T_V_1 = CCStates_OffDisabled;
  CCStates_T Test_CCStates_T_V_2 = CCStates_Hold;
  CCStates_T Test_CCStates_T_V_3 = CCStates_Accelerate;
  CCStates_T Test_CCStates_T_V_4 = CCStates_Decelerate;
  CCStates_T Test_CCStates_T_V_5 = CCStates_Resume;
  CCStates_T Test_CCStates_T_V_6 = CCStates_Set;
  CCStates_T Test_CCStates_T_V_7 = CCStates_Driver_Override;
  CCStates_T Test_CCStates_T_V_8 = CCStates_Spare_1;
  CCStates_T Test_CCStates_T_V_9 = CCStates_Spare_2;
  CCStates_T Test_CCStates_T_V_10 = CCStates_Spare_3;
  CCStates_T Test_CCStates_T_V_11 = CCStates_Spare_4;
  CCStates_T Test_CCStates_T_V_12 = CCStates_Spare_5;
  CCStates_T Test_CCStates_T_V_13 = CCStates_Spare_6;
  CCStates_T Test_CCStates_T_V_14 = CCStates_Spare_7;
  CCStates_T Test_CCStates_T_V_15 = CCStates_Error;
  CCStates_T Test_CCStates_T_V_16 = CCStates_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_1 = DriverMemory_rqst_UseDefaultDriverMemory;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_2 = DriverMemory_rqst_UseDriverMemory1;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_3 = DriverMemory_rqst_UseDriverMemory2;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_4 = DriverMemory_rqst_UseDriverMemory3;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_5 = DriverMemory_rqst_UseDriverMemory4;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_6 = DriverMemory_rqst_UseDriverMemory5;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_7 = DriverMemory_rqst_UseDriverMemory6;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_8 = DriverMemory_rqst_UseDriverMemory7;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_9 = DriverMemory_rqst_UseDriverMemory8;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_10 = DriverMemory_rqst_UseDriverMemory9;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_11 = DriverMemory_rqst_UseDriverMemory10;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_12 = DriverMemory_rqst_ResetAndThenUseDriverMemory1;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_13 = DriverMemory_rqst_ResetAndThenUseDriverMemory2;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_14 = DriverMemory_rqst_ResetAndThenUseDriverMemory3;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_15 = DriverMemory_rqst_ResetAndThenUseDriverMemory4;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_16 = DriverMemory_rqst_ResetAndThenUseDriverMemory5;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_17 = DriverMemory_rqst_ResetAndThenUseDriverMemory6;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_18 = DriverMemory_rqst_ResetAndThenUseDriverMemory7;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_19 = DriverMemory_rqst_ResetAndThenUseDriverMemory8;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_20 = DriverMemory_rqst_ResetAndThenUseDriverMemory9;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_21 = DriverMemory_rqst_ResetAndThenUseDriverMemory10;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_22 = DriverMemory_rqst_ResetAllMemThenUseDefDriverMem;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_23 = DriverMemory_rqst_Spare;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_24 = DriverMemory_rqst_Spare_01;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_25 = DriverMemory_rqst_Spare_02;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_26 = DriverMemory_rqst_Spare_03;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_27 = DriverMemory_rqst_Spare_04;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_28 = DriverMemory_rqst_Spare_05;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_29 = DriverMemory_rqst_Spare_06;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_30 = DriverMemory_rqst_Spare_07;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_31 = DriverMemory_rqst_NotAvailable;
  DriverMemory_rqst_T Test_DriverMemory_rqst_T_V_32 = DriverMemory_rqst_Error;

  FWSelectedSpeedControlMode_T Test_FWSelectedSpeedControlMode_T_V_1 = FWSelectedSpeedControlMode_Off;
  FWSelectedSpeedControlMode_T Test_FWSelectedSpeedControlMode_T_V_2 = FWSelectedSpeedControlMode_CruiseControl;
  FWSelectedSpeedControlMode_T Test_FWSelectedSpeedControlMode_T_V_3 = FWSelectedSpeedControlMode_AdjustableSpeedLimiter;
  FWSelectedSpeedControlMode_T Test_FWSelectedSpeedControlMode_T_V_4 = FWSelectedSpeedControlMode_AdaptiveCruiseControl;
  FWSelectedSpeedControlMode_T Test_FWSelectedSpeedControlMode_T_V_5 = FWSelectedSpeedControlMode_Spare_01;
  FWSelectedSpeedControlMode_T Test_FWSelectedSpeedControlMode_T_V_6 = FWSelectedSpeedControlMode_Spare_02;
  FWSelectedSpeedControlMode_T Test_FWSelectedSpeedControlMode_T_V_7 = FWSelectedSpeedControlMode_Error;
  FWSelectedSpeedControlMode_T Test_FWSelectedSpeedControlMode_T_V_8 = FWSelectedSpeedControlMode_NotAvailable;

  FreeWheel_Status_T Test_FreeWheel_Status_T_V_1 = FreeWheel_Status_NoMovement;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_2 = FreeWheel_Status_1StepClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_3 = FreeWheel_Status_2StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_4 = FreeWheel_Status_3StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_5 = FreeWheel_Status_4StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_6 = FreeWheel_Status_5StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_7 = FreeWheel_Status_6StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_8 = FreeWheel_Status_1StepCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_9 = FreeWheel_Status_2StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_10 = FreeWheel_Status_3StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_11 = FreeWheel_Status_4StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_12 = FreeWheel_Status_5StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_13 = FreeWheel_Status_6StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_14 = FreeWheel_Status_Spare;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_15 = FreeWheel_Status_Error;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_16 = FreeWheel_Status_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;

  XRSLStates_T Test_XRSLStates_T_V_1 = XRSLStates_Off_disabled;
  XRSLStates_T Test_XRSLStates_T_V_2 = XRSLStates_Hold;
  XRSLStates_T Test_XRSLStates_T_V_3 = XRSLStates_Accelerate;
  XRSLStates_T Test_XRSLStates_T_V_4 = XRSLStates_Decelerate;
  XRSLStates_T Test_XRSLStates_T_V_5 = XRSLStates_Resume;
  XRSLStates_T Test_XRSLStates_T_V_6 = XRSLStates_Set;
  XRSLStates_T Test_XRSLStates_T_V_7 = XRSLStates_Driver_override;
  XRSLStates_T Test_XRSLStates_T_V_8 = XRSLStates_Error;
  XRSLStates_T Test_XRSLStates_T_V_9 = XRSLStates_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
