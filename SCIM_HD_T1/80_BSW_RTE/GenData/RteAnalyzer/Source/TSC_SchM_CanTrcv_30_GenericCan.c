/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_CanTrcv_30_GenericCan.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_CanTrcv_30_GenericCan.h"
#include "TSC_SchM_CanTrcv_30_GenericCan.h"
void TSC_CanTrcv_30_GenericCan_SchM_Enter_CanTrcv_30_GenericCan_CANTRCV_30_GENERICCAN_EXCLUSIVE_AREA_0(void)
{
  SchM_Enter_CanTrcv_30_GenericCan_CANTRCV_30_GENERICCAN_EXCLUSIVE_AREA_0();
}
void TSC_CanTrcv_30_GenericCan_SchM_Exit_CanTrcv_30_GenericCan_CANTRCV_30_GENERICCAN_EXCLUSIVE_AREA_0(void)
{
  SchM_Exit_CanTrcv_30_GenericCan_CANTRCV_30_GENERICCAN_EXCLUSIVE_AREA_0();
}
