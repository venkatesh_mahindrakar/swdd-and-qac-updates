/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DriveSidePwrWindowsSelector.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Read_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Read_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Read_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Read_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Read_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Read_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Write_BunkH2PowerWinCloseDSBtn_stat_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Write_BunkH2PowerWinClosePSBtn_stat_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Write_BunkH2PowerWinOpenDSBtn_stat_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_DriveSidePwrWindowsSelector_Rte_Write_BunkH2PowerWinOpenPSBtn_stat_PushButtonStatus(PushButtonStatus_T data);

/** Calibration Component Calibration Parameters */
SEWS_DriverPosition_LHD_RHD_P1ALJ_T  TSC_DriveSidePwrWindowsSelector_Rte_Prm_P1ALJ_DriverPosition_LHD_RHD_v(void);
boolean  TSC_DriveSidePwrWindowsSelector_Rte_Prm_P1B2G_LECMH_Installed_v(void);




