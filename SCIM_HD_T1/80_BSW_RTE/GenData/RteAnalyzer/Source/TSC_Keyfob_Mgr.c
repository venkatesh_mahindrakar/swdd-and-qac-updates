/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Keyfob_Mgr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_Keyfob_Mgr.h"
#include "TSC_Keyfob_Mgr.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount(void)
{
return Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount();
}
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus(void)
{
return Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
{
  return Rte_Read_DiagActiveState_isDiagActive(data);
}

Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching(DriverAuthDeviceMatching_T *data)
{
  return Rte_Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching(data);
}

Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_KeyfobAuth_rqst_KeyfobAuth_rqst(KeyfobAuth_rqst_T *data)
{
  return Rte_Read_KeyfobAuth_rqst_KeyfobAuth_rqst(data);
}

Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst(KeyfobInCabPresencePS_T *data)
{
  return Rte_Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst(data);
}

Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst_T *data)
{
  return Rte_Read_KeyfobLocation_rqst_KeyfobLocation_rqst(data);
}

Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Security_SwcActivation_Security(data);
}




Std_ReturnType TSC_Keyfob_Mgr_Rte_Write_AddrParP1DS4_stat_dataP1DS4(const SEWS_KeyfobEncryptCode_P1DS4_T *data)
{
  return Rte_Write_AddrParP1DS4_stat_dataP1DS4(data);
}

Std_ReturnType TSC_Keyfob_Mgr_Rte_Write_KeyfobAuth_stat_KeyfobAuth_stat(KeyfobAuth_stat_T data)
{
  return Rte_Write_KeyfobAuth_stat_KeyfobAuth_stat(data);
}

Std_ReturnType TSC_Keyfob_Mgr_Rte_Write_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T data)
{
  return Rte_Write_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(data);
}

Std_ReturnType TSC_Keyfob_Mgr_Rte_Write_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(KeyfobOutsideLocation_stat_T data)
{
  return Rte_Write_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(SCIM_ImmoDriver_ProcessingStatus_T *ImmoProcessingStatus)
{
  return Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(ImmoProcessingStatus);
}
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(SCIM_ImmoType_T ImmoType, Boolean AuthRequestedbyImmo)
{
  return Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(ImmoType, AuthRequestedbyImmo);
}
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(uint8 *matchingStatus, uint8 *matchedKeyfobCount)
{
  return Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(matchingStatus, matchedKeyfobCount);
}
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF(void)
{
  return Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF();
}
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList(void)
{
  return Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList();
}
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(SCIM_PassiveDriver_ProcessingStatus_T *ProcessingStatus, KeyfobInCabLocation_stat_T *KeyfobLocationbyPassive_Incab, KeyfobOutsideLocation_stat_T *KeyfobLocationbyPassive_Outcab)
{
  return Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(ProcessingStatus, KeyfobLocationbyPassive_Incab, KeyfobLocationbyPassive_Outcab);
}
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(SCIM_PassiveSearchCoverage_T PassiveSearchCoverage, boolean SearchRequestedbyPassive)
{
  return Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(PassiveSearchCoverage, SearchRequestedbyPassive);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(void)
{
return Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF();
}
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(void)
{
return Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount();
}
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(void)
{
return Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus();
}

void TSC_Keyfob_Mgr_Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(uint8 data)
{
  Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF( data);
}
void TSC_Keyfob_Mgr_Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(uint8 data)
{
  Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount( data);
}
void TSC_Keyfob_Mgr_Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(uint8 data)
{
  Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(void)
{
return Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF();
}

void TSC_Keyfob_Mgr_Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(uint8 data)
{
  Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(void)
{
return Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF();
}

void TSC_Keyfob_Mgr_Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(uint8 data)
{
  Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(void)
{
return Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF();
}

void TSC_Keyfob_Mgr_Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(uint8 data)
{
  Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF(void)
{
return Rte_IrvRead_RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF();
}





SEWS_KeyMatchingIndicationTimeout_X1CV3_T  TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v(void)
{
  return (SEWS_KeyMatchingIndicationTimeout_X1CV3_T ) Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
}
boolean  TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v(void)
{
  return (boolean ) Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
}
boolean  TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v(void)
{
  return (boolean ) Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();
}
boolean  TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v(void)
{
  return (boolean ) Rte_Prm_P1B2U_KeyfobPresent_v();
}
SEWS_KeyfobEncryptCode_P1DS4_a_T * TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(void)
{
  return (SEWS_KeyfobEncryptCode_P1DS4_a_T *) Rte_Prm_P1DS4_KeyfobEncryptCode_v();
}
SEWS_CrankingLockActivation_P1DS3_T  TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v(void)
{
  return (SEWS_CrankingLockActivation_P1DS3_T ) Rte_Prm_P1DS3_CrankingLockActivation_v();
}
boolean  TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v(void)
{
  return (boolean ) Rte_Prm_P1C54_FactoryModeActive_v();
}


     /* Keyfob_Mgr */
      /* Keyfob_Mgr */



