/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  AxleLoadDistribution_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  AxleLoadDistribution_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <AxleLoadDistribution_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   
 *
 * SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_AxleLoadDistribution_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_AxleLoadDistribution_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void AxleLoadDistribution_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * AltLoadDistribution_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   AltLoadDistribution_rqst_Idle (0U)
 *   AltLoadDistribution_rqst_ALDOn (1U)
 *   AltLoadDistribution_rqst_ALDOff (2U)
 *   AltLoadDistribution_rqst_Reserved_1 (3U)
 *   AltLoadDistribution_rqst_Reserved_2 (4U)
 *   AltLoadDistribution_rqst_Reserved_3 (5U)
 *   AltLoadDistribution_rqst_Error (6U)
 *   AltLoadDistribution_rqst_NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 *   DualDeviceIndication_UpperOffLowerOff (0U)
 *   DualDeviceIndication_UpperOnLowerOff (1U)
 *   DualDeviceIndication_UpperBlinkLowerOff (2U)
 *   DualDeviceIndication_UpperDontCareLowerOff (3U)
 *   DualDeviceIndication_UpperOffLowerOn (4U)
 *   DualDeviceIndication_UpperOnLowerOn (5U)
 *   DualDeviceIndication_UpperBlinkLowerOn (6U)
 *   DualDeviceIndication_UpperDontCareLowerOn (7U)
 *   DualDeviceIndication_UpperOffLowerBlink (8U)
 *   DualDeviceIndication_UpperOnLowerBlink (9U)
 *   DualDeviceIndication_UpperBlinkLowerBlink (10U)
 *   DualDeviceIndication_UpperDontCareLowerBlink (11U)
 *   DualDeviceIndication_UpperOffLowerDontCare (12U)
 *   DualDeviceIndication_UpperOnLowerDontCare (13U)
 *   DualDeviceIndication_UpperBlinkLowerDontCare (14U)
 *   DualDeviceIndication_UpperDontCareLowerDontCare (15U)
 * ECSStandByRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByRequest_NoRequest (0U)
 *   ECSStandByRequest_Initiate (1U)
 *   ECSStandByRequest_StandbyRequestedRCECS (2U)
 *   ECSStandByRequest_StandbyRequestedWRC (3U)
 *   ECSStandByRequest_Reserved (4U)
 *   ECSStandByRequest_Reserved_01 (5U)
 *   ECSStandByRequest_Error (6U)
 *   ECSStandByRequest_NotAvailable (7U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * LiftAxleLiftPositionRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   LiftAxleLiftPositionRequest_Idle (0U)
 *   LiftAxleLiftPositionRequest_Down (1U)
 *   LiftAxleLiftPositionRequest_Up (2U)
 *   LiftAxleLiftPositionRequest_Reserved (3U)
 *   LiftAxleLiftPositionRequest_Reserved_01 (4U)
 *   LiftAxleLiftPositionRequest_Reserved_02 (5U)
 *   LiftAxleLiftPositionRequest_Error (6U)
 *   LiftAxleLiftPositionRequest_NotAvailable (7U)
 * LiftAxlePositionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   LiftAxlePositionStatus_Lowered (0U)
 *   LiftAxlePositionStatus_Lifted (1U)
 *   LiftAxlePositionStatus_Lowering (2U)
 *   LiftAxlePositionStatus_Lifting (3U)
 *   LiftAxlePositionStatus_Reserved (4U)
 *   LiftAxlePositionStatus_Reserved_01 (5U)
 *   LiftAxlePositionStatus_Error (6U)
 *   LiftAxlePositionStatus_NotAvailable (7U)
 * LiftAxleUpRequestACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LiftAxleUpRequestACK_NoAction (0U)
 *   LiftAxleUpRequestACK_ChangeAcknowledged (1U)
 *   LiftAxleUpRequestACK_LiftDeniedFrontOverload (2U)
 *   LiftAxleUpRequestACK_LiftDeniedRearOverload (3U)
 *   LiftAxleUpRequestACK_LiftDeniedPBrakeActive (4U)
 *   LiftAxleUpRequestACK_SystemDeniedVersatile (5U)
 *   LiftAxleUpRequestACK_Error (6U)
 *   LiftAxleUpRequestACK_NotAvailable (7U)
 * LoadDistributionALDChangeACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LoadDistributionALDChangeACK_NoAction (0U)
 *   LoadDistributionALDChangeACK_ChangeAcknowledged (1U)
 *   LoadDistributionALDChangeACK_LoadShiftDeniedOverspeed (2U)
 *   LoadDistributionALDChangeACK_Spare (3U)
 *   LoadDistributionALDChangeACK_Spare_01 (4U)
 *   LoadDistributionALDChangeACK_SystemDeniedVersatile (5U)
 *   LoadDistributionALDChangeACK_Error (6U)
 *   LoadDistributionALDChangeACK_NotAvailable (7U)
 * LoadDistributionChangeACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LoadDistributionChangeACK_NoAction (0U)
 *   LoadDistributionChangeACK_ChangeAcknowledged (1U)
 *   LoadDistributionChangeACK_LoadShiftDeniedFALIMOverload (2U)
 *   LoadDistributionChangeACK_Reserved_1 (3U)
 *   LoadDistributionChangeACK_Reserved_2 (4U)
 *   LoadDistributionChangeACK_SystemDeniedVersatile (5U)
 *   LoadDistributionChangeACK_Error (6U)
 *   LoadDistributionChangeACK_NotAvailable (7U)
 * LoadDistributionChangeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 *   LoadDistributionChangeRequest_Idle (0U)
 *   LoadDistributionChangeRequest_ChangeLoadDistribution (1U)
 *   LoadDistributionChangeRequest_Error (2U)
 *   LoadDistributionChangeRequest_NotAvailable (3U)
 * LoadDistributionFuncSelected_T: Enumeration of integer in interval [0...15] with enumerators
 *   LoadDistributionFuncSelected_NormalLoad (0U)
 *   LoadDistributionFuncSelected_OptimisedTraction (1U)
 *   LoadDistributionFuncSelected_MaximumTractionTractionHelp (2U)
 *   LoadDistributionFuncSelected_AlternativeLoad (3U)
 *   LoadDistributionFuncSelected_NoLoadDistribution (4U)
 *   LoadDistributionFuncSelected_VariableAxleLoad (5U)
 *   LoadDistributionFuncSelected_MaximumTractionStartingHelp (6U)
 *   LoadDistributionFuncSelected_SpareValue_02 (7U)
 *   LoadDistributionFuncSelected_SpareValue_03 (8U)
 *   LoadDistributionFuncSelected_SpareValue_04 (9U)
 *   LoadDistributionFuncSelected_SpareValue_05 (10U)
 *   LoadDistributionFuncSelected_SpareValue_06 (11U)
 *   LoadDistributionFuncSelected_SpareValue_07 (12U)
 *   LoadDistributionFuncSelected_SpareValue_08 (13U)
 *   LoadDistributionFuncSelected_ErrorIndicator (14U)
 *   LoadDistributionFuncSelected_NotAvaiable (15U)
 * LoadDistributionRequestedACK_T: Enumeration of integer in interval [0...7] with enumerators
 *   LoadDistributionRequestedACK_NoAction (0U)
 *   LoadDistributionRequestedACK_ChangeAcknowledged (1U)
 *   LoadDistributionRequestedACK_LoadShiftDeniedFALIMOverload (2U)
 *   LoadDistributionRequestedACK_Reserved1 (3U)
 *   LoadDistributionRequestedACK_Reserved2 (4U)
 *   LoadDistributionRequestedACK_SystemDeniedVersatile (5U)
 *   LoadDistributionRequestedACK_Error (6U)
 *   LoadDistributionRequestedACK_NotAvailable (7U)
 * LoadDistributionRequested_T: Enumeration of integer in interval [0...15] with enumerators
 *   LoadDistributionRequested_NoAction (0U)
 *   LoadDistributionRequested_DefaultLoadDistribution (1U)
 *   LoadDistributionRequested_NormalLoadDistribution (2U)
 *   LoadDistributionRequested_OptimizedTraction (3U)
 *   LoadDistributionRequested_MaximumTraction1 (4U)
 *   LoadDistributionRequested_MaximumTraction2 (5U)
 *   LoadDistributionRequested_AlternativeLoadDistribution (6U)
 *   LoadDistributionRequested_VariableLoadDistribution (7U)
 *   LoadDistributionRequested_DelpressLoadDistribution (8U)
 *   LoadDistributionRequested_Reserved1 (9U)
 *   LoadDistributionRequested_Reserved2 (10U)
 *   LoadDistributionRequested_Reserved3 (11U)
 *   LoadDistributionRequested_Reserved4 (12U)
 *   LoadDistributionRequested_Reserved5 (13U)
 *   LoadDistributionRequested_Error (14U)
 *   LoadDistributionRequested_NotAvailable (15U)
 * LoadDistributionSelected_T: Enumeration of integer in interval [0...15] with enumerators
 *   LoadDistributionSelected_NormalLoad (0U)
 *   LoadDistributionSelected_OptimisedTraction (1U)
 *   LoadDistributionSelected_StartingHelp (2U)
 *   LoadDistributionSelected_TractionHelp (3U)
 *   LoadDistributionSelected_AlternativeLoad (4U)
 *   LoadDistributionSelected_StartingHelpInterDiff (5U)
 *   LoadDistributionSelected_TractionHelpInterDiff (6U)
 *   LoadDistributionSelected_NoLoadDistribution (7U)
 *   LoadDistributionSelected_VariableAxleLoad (8U)
 *   LoadDistributionSelected_StartingHelp2 (9U)
 *   LoadDistributionSelected_TractionHelp2 (10U)
 *   LoadDistributionSelected_Reserved_03 (11U)
 *   LoadDistributionSelected_Reserved_04 (12U)
 *   LoadDistributionSelected_Reserved_05 (13U)
 *   LoadDistributionSelected_Error (14U)
 *   LoadDistributionSelected_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T Rte_Prm_P1KN5_AxleLoad_CRideLEDIndicationType_v(void)
 *   boolean Rte_Prm_P1BOS_AxleLoad_AccessoryBoggieALD_v(void)
 *   boolean Rte_Prm_P1BOV_AxleLoad_RatioALD_v(void)
 *   boolean Rte_Prm_P1BOW_AxleLoad_ArideLiftAxle_v(void)
 *   boolean Rte_Prm_P1BOX_AxleLoad_TridemFirstAxleLift_v(void)
 *   boolean Rte_Prm_P1CZ0_AxleLoad_MaxTractionTag_v(void)
 *   boolean Rte_Prm_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v(void)
 *   boolean Rte_Prm_P1CZ2_AxleLoad_RatioPusherRocker_v(void)
 *   boolean Rte_Prm_P1CZ3_AxleLoad_RatioTagRocker_v(void)
 *   boolean Rte_Prm_P1CZW_AxleLoad_OneLiftPusher_v(void)
 *   boolean Rte_Prm_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v(void)
 *   boolean Rte_Prm_P1CZY_AxleLoad_OneLiftTag_v(void)
 *   boolean Rte_Prm_P1CZZ_AxleLoad_MaxTractionPusher_v(void)
 *   boolean Rte_Prm_P1J6B_AxleLoad_AccessoryTridemALD_v(void)
 *   boolean Rte_Prm_P1J6C_AxleLoad_BoggieDualRatio_v(void)
 *   boolean Rte_Prm_P1J6D_AxleLoad_TridemDualRatio_v(void)
 *   boolean Rte_Prm_P1KN2_AxleLoad_CRideLiftAxle_v(void)
 *   boolean Rte_Prm_P1KN4_AxleLoad_CRideLEDlowerEnd_v(void)
 *   boolean Rte_Prm_P1M5B_AxleLoad_RatioRoadGripPusher_v(void)
 *   boolean Rte_Prm_P1BOY_AxleLoad_TridemSecondAxleLift_v(void)
 *
 *********************************************************************************************************************/


#define AxleLoadDistribution_HMICtrl_START_SEC_CODE
#include "AxleLoadDistribution_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AxleLoadDistribution_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_ECSStandByRequest_ECSStandByRequest(ECSStandByRequest_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1PositionStatus_LiftAxle1PositionStatus(LiftAxlePositionStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1Switch2_Status_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK(LiftAxleUpRequestACK_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK(LiftAxleUpRequestACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK(LoadDistributionALDChangeACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionChangeACK_LoadDistributionChangeACK(LoadDistributionChangeACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionFuncSelected_LoadDistributionFuncSelected(LoadDistributionFuncSelected_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionRequestedACK_LoadDistributionRequestedACK(LoadDistributionRequestedACK_T *data)
 *   Std_ReturnType Rte_Read_LoadDistributionSelected_LoadDistributionSelected(LoadDistributionSelected_T *data)
 *   Std_ReturnType Rte_Read_Ratio1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio4SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio5SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_Ratio6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_RatioALDSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_TridemALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ALD_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_AltLoadDistribution_rqst_AltLoadDistribution_rqst(AltLoadDistribution_rqst_T data)
 *   Std_ReturnType Rte_Write_BogieSwitch_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1DirectControl_LiftAxle1DirectControl(LiftAxleLiftPositionRequest_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest(LiftAxleLiftPositionRequest_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest(LiftAxleLiftPositionRequest_T data)
 *   Std_ReturnType Rte_Write_LoadDistributionChangeRequest_LoadDistributionChangeRequest(LoadDistributionChangeRequest_T data)
 *   Std_ReturnType Rte_Write_LoadDistributionRequested_LoadDistributionRequested(LoadDistributionRequested_T data)
 *   Std_ReturnType Rte_Write_MaxTract_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio4_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio5_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Ratio6_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_RatioALD_DualDeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_TridemALD_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOV_56_ALD_InvalidConfiguration_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AxleLoadDistribution_HMICtrl_CODE) AxleLoadDistribution_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  A2PosSwitchStatus_T Read_ALDSwitchStatus_A2PosSwitchStatus;
  ECSStandByRequest_T Read_ECSStandByRequest_ECSStandByRequest;
  A3PosSwitchStatus_T Read_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus;
  LiftAxlePositionStatus_T Read_LiftAxle1PositionStatus_LiftAxle1PositionStatus;
  A3PosSwitchStatus_T Read_LiftAxle1Switch2_Status_A3PosSwitchStatus;
  A3PosSwitchStatus_T Read_LiftAxle1SwitchStatus_A3PosSwitchStatus;
  A3PosSwitchStatus_T Read_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus;
  LiftAxleUpRequestACK_T Read_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK;
  A3PosSwitchStatus_T Read_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus;
  A3PosSwitchStatus_T Read_LiftAxle2SwitchStatus_A3PosSwitchStatus;
  A3PosSwitchStatus_T Read_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus;
  LiftAxleUpRequestACK_T Read_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK;
  LoadDistributionALDChangeACK_T Read_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK;
  LoadDistributionChangeACK_T Read_LoadDistributionChangeACK_LoadDistributionChangeACK;
  LoadDistributionFuncSelected_T Read_LoadDistributionFuncSelected_LoadDistributionFuncSelected;
  LoadDistributionRequestedACK_T Read_LoadDistributionRequestedACK_LoadDistributionRequestedACK;
  LoadDistributionSelected_T Read_LoadDistributionSelected_LoadDistributionSelected;
  A2PosSwitchStatus_T Read_Ratio1SwitchStatus_A2PosSwitchStatus;
  A2PosSwitchStatus_T Read_Ratio2SwitchStatus_A2PosSwitchStatus;
  A2PosSwitchStatus_T Read_Ratio3SwitchStatus_A2PosSwitchStatus;
  A3PosSwitchStatus_T Read_Ratio4SwitchStatus_A3PosSwitchStatus;
  A3PosSwitchStatus_T Read_Ratio5SwitchStatus_A3PosSwitchStatus;
  A2PosSwitchStatus_T Read_Ratio6SwitchStatus_A2PosSwitchStatus;
  A3PosSwitchStatus_T Read_RatioALDSwitchStatus_A3PosSwitchStatus;
  VehicleModeDistribution_T Read_SwcActivation_Living_Living;
  A2PosSwitchStatus_T Read_TridemALDSwitchStatus_A2PosSwitchStatus;

  SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T P1KN5_AxleLoad_CRideLEDIndicationType_v_data;
  boolean P1BOS_AxleLoad_AccessoryBoggieALD_v_data;
  boolean P1BOV_AxleLoad_RatioALD_v_data;
  boolean P1BOW_AxleLoad_ArideLiftAxle_v_data;
  boolean P1BOX_AxleLoad_TridemFirstAxleLift_v_data;
  boolean P1CZ0_AxleLoad_MaxTractionTag_v_data;
  boolean P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v_data;
  boolean P1CZ2_AxleLoad_RatioPusherRocker_v_data;
  boolean P1CZ3_AxleLoad_RatioTagRocker_v_data;
  boolean P1CZW_AxleLoad_OneLiftPusher_v_data;
  boolean P1CZX_AxleLoad_OneLiftAxleMaxTraction_v_data;
  boolean P1CZY_AxleLoad_OneLiftTag_v_data;
  boolean P1CZZ_AxleLoad_MaxTractionPusher_v_data;
  boolean P1J6B_AxleLoad_AccessoryTridemALD_v_data;
  boolean P1J6C_AxleLoad_BoggieDualRatio_v_data;
  boolean P1J6D_AxleLoad_TridemDualRatio_v_data;
  boolean P1KN2_AxleLoad_CRideLiftAxle_v_data;
  boolean P1KN4_AxleLoad_CRideLEDlowerEnd_v_data;
  boolean P1M5B_AxleLoad_RatioRoadGripPusher_v_data;

  boolean P1BOY_AxleLoad_TridemSecondAxleLift_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1KN5_AxleLoad_CRideLEDIndicationType_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1KN5_AxleLoad_CRideLEDIndicationType_v();
  P1BOS_AxleLoad_AccessoryBoggieALD_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOS_AxleLoad_AccessoryBoggieALD_v();
  P1BOV_AxleLoad_RatioALD_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOV_AxleLoad_RatioALD_v();
  P1BOW_AxleLoad_ArideLiftAxle_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOW_AxleLoad_ArideLiftAxle_v();
  P1BOX_AxleLoad_TridemFirstAxleLift_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOX_AxleLoad_TridemFirstAxleLift_v();
  P1CZ0_AxleLoad_MaxTractionTag_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ0_AxleLoad_MaxTractionTag_v();
  P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v();
  P1CZ2_AxleLoad_RatioPusherRocker_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ2_AxleLoad_RatioPusherRocker_v();
  P1CZ3_AxleLoad_RatioTagRocker_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ3_AxleLoad_RatioTagRocker_v();
  P1CZW_AxleLoad_OneLiftPusher_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZW_AxleLoad_OneLiftPusher_v();
  P1CZX_AxleLoad_OneLiftAxleMaxTraction_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v();
  P1CZY_AxleLoad_OneLiftTag_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZY_AxleLoad_OneLiftTag_v();
  P1CZZ_AxleLoad_MaxTractionPusher_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZZ_AxleLoad_MaxTractionPusher_v();
  P1J6B_AxleLoad_AccessoryTridemALD_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1J6B_AxleLoad_AccessoryTridemALD_v();
  P1J6C_AxleLoad_BoggieDualRatio_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1J6C_AxleLoad_BoggieDualRatio_v();
  P1J6D_AxleLoad_TridemDualRatio_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1J6D_AxleLoad_TridemDualRatio_v();
  P1KN2_AxleLoad_CRideLiftAxle_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1KN2_AxleLoad_CRideLiftAxle_v();
  P1KN4_AxleLoad_CRideLEDlowerEnd_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1KN4_AxleLoad_CRideLEDlowerEnd_v();
  P1M5B_AxleLoad_RatioRoadGripPusher_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1M5B_AxleLoad_RatioRoadGripPusher_v();

  P1BOY_AxleLoad_TridemSecondAxleLift_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOY_AxleLoad_TridemSecondAxleLift_v();

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_ALDSwitchStatus_A2PosSwitchStatus(&Read_ALDSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_ECSStandByRequest_ECSStandByRequest(&Read_ECSStandByRequest_ECSStandByRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(&Read_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1PositionStatus_LiftAxle1PositionStatus(&Read_LiftAxle1PositionStatus_LiftAxle1PositionStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1Switch2_Status_A3PosSwitchStatus(&Read_LiftAxle1Switch2_Status_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1SwitchStatus_A3PosSwitchStatus(&Read_LiftAxle1SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(&Read_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK(&Read_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(&Read_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle2SwitchStatus_A3PosSwitchStatus(&Read_LiftAxle2SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(&Read_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK(&Read_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK(&Read_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionChangeACK_LoadDistributionChangeACK(&Read_LoadDistributionChangeACK_LoadDistributionChangeACK);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionFuncSelected_LoadDistributionFuncSelected(&Read_LoadDistributionFuncSelected_LoadDistributionFuncSelected);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionRequestedACK_LoadDistributionRequestedACK(&Read_LoadDistributionRequestedACK_LoadDistributionRequestedACK);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionSelected_LoadDistributionSelected(&Read_LoadDistributionSelected_LoadDistributionSelected);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio1SwitchStatus_A2PosSwitchStatus(&Read_Ratio1SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio2SwitchStatus_A2PosSwitchStatus(&Read_Ratio2SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio3SwitchStatus_A2PosSwitchStatus(&Read_Ratio3SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio4SwitchStatus_A3PosSwitchStatus(&Read_Ratio4SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio5SwitchStatus_A3PosSwitchStatus(&Read_Ratio5SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio6SwitchStatus_A2PosSwitchStatus(&Read_Ratio6SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_RatioALDSwitchStatus_A3PosSwitchStatus(&Read_RatioALDSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_SwcActivation_Living_Living(&Read_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Read_TridemALDSwitchStatus_A2PosSwitchStatus(&Read_TridemALDSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_ALD_DeviceIndication_DeviceIndication(Rte_InitValue_ALD_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_AltLoadDistribution_rqst_AltLoadDistribution_rqst(Rte_InitValue_AltLoadDistribution_rqst_AltLoadDistribution_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_BogieSwitch_DeviceIndication_DualDeviceIndication(Rte_InitValue_BogieSwitch_DeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest(Rte_InitValue_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle1DirectControl_LiftAxle1DirectControl(Rte_InitValue_LiftAxle1DirectControl_LiftAxle1DirectControl);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest(Rte_InitValue_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest(Rte_InitValue_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest(Rte_InitValue_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LoadDistributionChangeRequest_LoadDistributionChangeRequest(Rte_InitValue_LoadDistributionChangeRequest_LoadDistributionChangeRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LoadDistributionRequested_LoadDistributionRequested(Rte_InitValue_LoadDistributionRequested_LoadDistributionRequested);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_MaxTract_DeviceIndication_DeviceIndication(Rte_InitValue_MaxTract_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio1_DeviceIndication_DeviceIndication(Rte_InitValue_Ratio1_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio2_DeviceIndication_DeviceIndication(Rte_InitValue_Ratio2_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio4_DeviceIndication_DualDeviceIndication(Rte_InitValue_Ratio4_DeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio5_DeviceIndication_DualDeviceIndication(Rte_InitValue_Ratio5_DeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio6_DeviceIndication_DeviceIndication(Rte_InitValue_Ratio6_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_RatioALD_DualDeviceIndication_DualDeviceIndication(Rte_InitValue_RatioALD_DualDeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Write_TridemALD_DeviceIndication_DeviceIndication(Rte_InitValue_TridemALD_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Call_Event_D1BOV_56_ALD_InvalidConfiguration_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_AxleLoadDistribution_HMICtrl_Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  AxleLoadDistribution_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AxleLoadDistribution_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, AxleLoadDistribution_HMICtrl_CODE) AxleLoadDistribution_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AxleLoadDistribution_HMICtrl_Init
 *********************************************************************************************************************/

  SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T P1KN5_AxleLoad_CRideLEDIndicationType_v_data;
  boolean P1BOS_AxleLoad_AccessoryBoggieALD_v_data;
  boolean P1BOV_AxleLoad_RatioALD_v_data;
  boolean P1BOW_AxleLoad_ArideLiftAxle_v_data;
  boolean P1BOX_AxleLoad_TridemFirstAxleLift_v_data;
  boolean P1CZ0_AxleLoad_MaxTractionTag_v_data;
  boolean P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v_data;
  boolean P1CZ2_AxleLoad_RatioPusherRocker_v_data;
  boolean P1CZ3_AxleLoad_RatioTagRocker_v_data;
  boolean P1CZW_AxleLoad_OneLiftPusher_v_data;
  boolean P1CZX_AxleLoad_OneLiftAxleMaxTraction_v_data;
  boolean P1CZY_AxleLoad_OneLiftTag_v_data;
  boolean P1CZZ_AxleLoad_MaxTractionPusher_v_data;
  boolean P1J6B_AxleLoad_AccessoryTridemALD_v_data;
  boolean P1J6C_AxleLoad_BoggieDualRatio_v_data;
  boolean P1J6D_AxleLoad_TridemDualRatio_v_data;
  boolean P1KN2_AxleLoad_CRideLiftAxle_v_data;
  boolean P1KN4_AxleLoad_CRideLEDlowerEnd_v_data;
  boolean P1M5B_AxleLoad_RatioRoadGripPusher_v_data;

  boolean P1BOY_AxleLoad_TridemSecondAxleLift_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1KN5_AxleLoad_CRideLEDIndicationType_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1KN5_AxleLoad_CRideLEDIndicationType_v();
  P1BOS_AxleLoad_AccessoryBoggieALD_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOS_AxleLoad_AccessoryBoggieALD_v();
  P1BOV_AxleLoad_RatioALD_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOV_AxleLoad_RatioALD_v();
  P1BOW_AxleLoad_ArideLiftAxle_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOW_AxleLoad_ArideLiftAxle_v();
  P1BOX_AxleLoad_TridemFirstAxleLift_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOX_AxleLoad_TridemFirstAxleLift_v();
  P1CZ0_AxleLoad_MaxTractionTag_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ0_AxleLoad_MaxTractionTag_v();
  P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v();
  P1CZ2_AxleLoad_RatioPusherRocker_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ2_AxleLoad_RatioPusherRocker_v();
  P1CZ3_AxleLoad_RatioTagRocker_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ3_AxleLoad_RatioTagRocker_v();
  P1CZW_AxleLoad_OneLiftPusher_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZW_AxleLoad_OneLiftPusher_v();
  P1CZX_AxleLoad_OneLiftAxleMaxTraction_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v();
  P1CZY_AxleLoad_OneLiftTag_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZY_AxleLoad_OneLiftTag_v();
  P1CZZ_AxleLoad_MaxTractionPusher_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZZ_AxleLoad_MaxTractionPusher_v();
  P1J6B_AxleLoad_AccessoryTridemALD_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1J6B_AxleLoad_AccessoryTridemALD_v();
  P1J6C_AxleLoad_BoggieDualRatio_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1J6C_AxleLoad_BoggieDualRatio_v();
  P1J6D_AxleLoad_TridemDualRatio_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1J6D_AxleLoad_TridemDualRatio_v();
  P1KN2_AxleLoad_CRideLiftAxle_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1KN2_AxleLoad_CRideLiftAxle_v();
  P1KN4_AxleLoad_CRideLEDlowerEnd_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1KN4_AxleLoad_CRideLEDlowerEnd_v();
  P1M5B_AxleLoad_RatioRoadGripPusher_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1M5B_AxleLoad_RatioRoadGripPusher_v();

  P1BOY_AxleLoad_TridemSecondAxleLift_v_data = TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOY_AxleLoad_TridemSecondAxleLift_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define AxleLoadDistribution_HMICtrl_STOP_SEC_CODE
#include "AxleLoadDistribution_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void AxleLoadDistribution_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_1 = A3PosSwitchStatus_Middle;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_2 = A3PosSwitchStatus_Lower;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_3 = A3PosSwitchStatus_Upper;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_4 = A3PosSwitchStatus_Spare;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_5 = A3PosSwitchStatus_Spare_01;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_6 = A3PosSwitchStatus_Spare_02;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_7 = A3PosSwitchStatus_Error;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_8 = A3PosSwitchStatus_NotAvailable;

  AltLoadDistribution_rqst_T Test_AltLoadDistribution_rqst_T_V_1 = AltLoadDistribution_rqst_Idle;
  AltLoadDistribution_rqst_T Test_AltLoadDistribution_rqst_T_V_2 = AltLoadDistribution_rqst_ALDOn;
  AltLoadDistribution_rqst_T Test_AltLoadDistribution_rqst_T_V_3 = AltLoadDistribution_rqst_ALDOff;
  AltLoadDistribution_rqst_T Test_AltLoadDistribution_rqst_T_V_4 = AltLoadDistribution_rqst_Reserved_1;
  AltLoadDistribution_rqst_T Test_AltLoadDistribution_rqst_T_V_5 = AltLoadDistribution_rqst_Reserved_2;
  AltLoadDistribution_rqst_T Test_AltLoadDistribution_rqst_T_V_6 = AltLoadDistribution_rqst_Reserved_3;
  AltLoadDistribution_rqst_T Test_AltLoadDistribution_rqst_T_V_7 = AltLoadDistribution_rqst_Error;
  AltLoadDistribution_rqst_T Test_AltLoadDistribution_rqst_T_V_8 = AltLoadDistribution_rqst_NotAvailable;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DualDeviceIndication_T Test_DualDeviceIndication_T_V_1 = DualDeviceIndication_UpperOffLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_2 = DualDeviceIndication_UpperOnLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_3 = DualDeviceIndication_UpperBlinkLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_4 = DualDeviceIndication_UpperDontCareLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_5 = DualDeviceIndication_UpperOffLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_6 = DualDeviceIndication_UpperOnLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_7 = DualDeviceIndication_UpperBlinkLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_8 = DualDeviceIndication_UpperDontCareLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_9 = DualDeviceIndication_UpperOffLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_10 = DualDeviceIndication_UpperOnLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_11 = DualDeviceIndication_UpperBlinkLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_12 = DualDeviceIndication_UpperDontCareLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_13 = DualDeviceIndication_UpperOffLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_14 = DualDeviceIndication_UpperOnLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_15 = DualDeviceIndication_UpperBlinkLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_16 = DualDeviceIndication_UpperDontCareLowerDontCare;

  ECSStandByRequest_T Test_ECSStandByRequest_T_V_1 = ECSStandByRequest_NoRequest;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_2 = ECSStandByRequest_Initiate;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_3 = ECSStandByRequest_StandbyRequestedRCECS;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_4 = ECSStandByRequest_StandbyRequestedWRC;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_5 = ECSStandByRequest_Reserved;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_6 = ECSStandByRequest_Reserved_01;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_7 = ECSStandByRequest_Error;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_8 = ECSStandByRequest_NotAvailable;

  InactiveActive_T Test_InactiveActive_T_V_1 = InactiveActive_Inactive;
  InactiveActive_T Test_InactiveActive_T_V_2 = InactiveActive_Active;
  InactiveActive_T Test_InactiveActive_T_V_3 = InactiveActive_Error;
  InactiveActive_T Test_InactiveActive_T_V_4 = InactiveActive_NotAvailable;

  LiftAxleLiftPositionRequest_T Test_LiftAxleLiftPositionRequest_T_V_1 = LiftAxleLiftPositionRequest_Idle;
  LiftAxleLiftPositionRequest_T Test_LiftAxleLiftPositionRequest_T_V_2 = LiftAxleLiftPositionRequest_Down;
  LiftAxleLiftPositionRequest_T Test_LiftAxleLiftPositionRequest_T_V_3 = LiftAxleLiftPositionRequest_Up;
  LiftAxleLiftPositionRequest_T Test_LiftAxleLiftPositionRequest_T_V_4 = LiftAxleLiftPositionRequest_Reserved;
  LiftAxleLiftPositionRequest_T Test_LiftAxleLiftPositionRequest_T_V_5 = LiftAxleLiftPositionRequest_Reserved_01;
  LiftAxleLiftPositionRequest_T Test_LiftAxleLiftPositionRequest_T_V_6 = LiftAxleLiftPositionRequest_Reserved_02;
  LiftAxleLiftPositionRequest_T Test_LiftAxleLiftPositionRequest_T_V_7 = LiftAxleLiftPositionRequest_Error;
  LiftAxleLiftPositionRequest_T Test_LiftAxleLiftPositionRequest_T_V_8 = LiftAxleLiftPositionRequest_NotAvailable;

  LiftAxlePositionStatus_T Test_LiftAxlePositionStatus_T_V_1 = LiftAxlePositionStatus_Lowered;
  LiftAxlePositionStatus_T Test_LiftAxlePositionStatus_T_V_2 = LiftAxlePositionStatus_Lifted;
  LiftAxlePositionStatus_T Test_LiftAxlePositionStatus_T_V_3 = LiftAxlePositionStatus_Lowering;
  LiftAxlePositionStatus_T Test_LiftAxlePositionStatus_T_V_4 = LiftAxlePositionStatus_Lifting;
  LiftAxlePositionStatus_T Test_LiftAxlePositionStatus_T_V_5 = LiftAxlePositionStatus_Reserved;
  LiftAxlePositionStatus_T Test_LiftAxlePositionStatus_T_V_6 = LiftAxlePositionStatus_Reserved_01;
  LiftAxlePositionStatus_T Test_LiftAxlePositionStatus_T_V_7 = LiftAxlePositionStatus_Error;
  LiftAxlePositionStatus_T Test_LiftAxlePositionStatus_T_V_8 = LiftAxlePositionStatus_NotAvailable;

  LiftAxleUpRequestACK_T Test_LiftAxleUpRequestACK_T_V_1 = LiftAxleUpRequestACK_NoAction;
  LiftAxleUpRequestACK_T Test_LiftAxleUpRequestACK_T_V_2 = LiftAxleUpRequestACK_ChangeAcknowledged;
  LiftAxleUpRequestACK_T Test_LiftAxleUpRequestACK_T_V_3 = LiftAxleUpRequestACK_LiftDeniedFrontOverload;
  LiftAxleUpRequestACK_T Test_LiftAxleUpRequestACK_T_V_4 = LiftAxleUpRequestACK_LiftDeniedRearOverload;
  LiftAxleUpRequestACK_T Test_LiftAxleUpRequestACK_T_V_5 = LiftAxleUpRequestACK_LiftDeniedPBrakeActive;
  LiftAxleUpRequestACK_T Test_LiftAxleUpRequestACK_T_V_6 = LiftAxleUpRequestACK_SystemDeniedVersatile;
  LiftAxleUpRequestACK_T Test_LiftAxleUpRequestACK_T_V_7 = LiftAxleUpRequestACK_Error;
  LiftAxleUpRequestACK_T Test_LiftAxleUpRequestACK_T_V_8 = LiftAxleUpRequestACK_NotAvailable;

  LoadDistributionALDChangeACK_T Test_LoadDistributionALDChangeACK_T_V_1 = LoadDistributionALDChangeACK_NoAction;
  LoadDistributionALDChangeACK_T Test_LoadDistributionALDChangeACK_T_V_2 = LoadDistributionALDChangeACK_ChangeAcknowledged;
  LoadDistributionALDChangeACK_T Test_LoadDistributionALDChangeACK_T_V_3 = LoadDistributionALDChangeACK_LoadShiftDeniedOverspeed;
  LoadDistributionALDChangeACK_T Test_LoadDistributionALDChangeACK_T_V_4 = LoadDistributionALDChangeACK_Spare;
  LoadDistributionALDChangeACK_T Test_LoadDistributionALDChangeACK_T_V_5 = LoadDistributionALDChangeACK_Spare_01;
  LoadDistributionALDChangeACK_T Test_LoadDistributionALDChangeACK_T_V_6 = LoadDistributionALDChangeACK_SystemDeniedVersatile;
  LoadDistributionALDChangeACK_T Test_LoadDistributionALDChangeACK_T_V_7 = LoadDistributionALDChangeACK_Error;
  LoadDistributionALDChangeACK_T Test_LoadDistributionALDChangeACK_T_V_8 = LoadDistributionALDChangeACK_NotAvailable;

  LoadDistributionChangeACK_T Test_LoadDistributionChangeACK_T_V_1 = LoadDistributionChangeACK_NoAction;
  LoadDistributionChangeACK_T Test_LoadDistributionChangeACK_T_V_2 = LoadDistributionChangeACK_ChangeAcknowledged;
  LoadDistributionChangeACK_T Test_LoadDistributionChangeACK_T_V_3 = LoadDistributionChangeACK_LoadShiftDeniedFALIMOverload;
  LoadDistributionChangeACK_T Test_LoadDistributionChangeACK_T_V_4 = LoadDistributionChangeACK_Reserved_1;
  LoadDistributionChangeACK_T Test_LoadDistributionChangeACK_T_V_5 = LoadDistributionChangeACK_Reserved_2;
  LoadDistributionChangeACK_T Test_LoadDistributionChangeACK_T_V_6 = LoadDistributionChangeACK_SystemDeniedVersatile;
  LoadDistributionChangeACK_T Test_LoadDistributionChangeACK_T_V_7 = LoadDistributionChangeACK_Error;
  LoadDistributionChangeACK_T Test_LoadDistributionChangeACK_T_V_8 = LoadDistributionChangeACK_NotAvailable;

  LoadDistributionChangeRequest_T Test_LoadDistributionChangeRequest_T_V_1 = LoadDistributionChangeRequest_Idle;
  LoadDistributionChangeRequest_T Test_LoadDistributionChangeRequest_T_V_2 = LoadDistributionChangeRequest_ChangeLoadDistribution;
  LoadDistributionChangeRequest_T Test_LoadDistributionChangeRequest_T_V_3 = LoadDistributionChangeRequest_Error;
  LoadDistributionChangeRequest_T Test_LoadDistributionChangeRequest_T_V_4 = LoadDistributionChangeRequest_NotAvailable;

  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_1 = LoadDistributionFuncSelected_NormalLoad;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_2 = LoadDistributionFuncSelected_OptimisedTraction;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_3 = LoadDistributionFuncSelected_MaximumTractionTractionHelp;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_4 = LoadDistributionFuncSelected_AlternativeLoad;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_5 = LoadDistributionFuncSelected_NoLoadDistribution;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_6 = LoadDistributionFuncSelected_VariableAxleLoad;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_7 = LoadDistributionFuncSelected_MaximumTractionStartingHelp;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_8 = LoadDistributionFuncSelected_SpareValue_02;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_9 = LoadDistributionFuncSelected_SpareValue_03;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_10 = LoadDistributionFuncSelected_SpareValue_04;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_11 = LoadDistributionFuncSelected_SpareValue_05;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_12 = LoadDistributionFuncSelected_SpareValue_06;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_13 = LoadDistributionFuncSelected_SpareValue_07;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_14 = LoadDistributionFuncSelected_SpareValue_08;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_15 = LoadDistributionFuncSelected_ErrorIndicator;
  LoadDistributionFuncSelected_T Test_LoadDistributionFuncSelected_T_V_16 = LoadDistributionFuncSelected_NotAvaiable;

  LoadDistributionRequestedACK_T Test_LoadDistributionRequestedACK_T_V_1 = LoadDistributionRequestedACK_NoAction;
  LoadDistributionRequestedACK_T Test_LoadDistributionRequestedACK_T_V_2 = LoadDistributionRequestedACK_ChangeAcknowledged;
  LoadDistributionRequestedACK_T Test_LoadDistributionRequestedACK_T_V_3 = LoadDistributionRequestedACK_LoadShiftDeniedFALIMOverload;
  LoadDistributionRequestedACK_T Test_LoadDistributionRequestedACK_T_V_4 = LoadDistributionRequestedACK_Reserved1;
  LoadDistributionRequestedACK_T Test_LoadDistributionRequestedACK_T_V_5 = LoadDistributionRequestedACK_Reserved2;
  LoadDistributionRequestedACK_T Test_LoadDistributionRequestedACK_T_V_6 = LoadDistributionRequestedACK_SystemDeniedVersatile;
  LoadDistributionRequestedACK_T Test_LoadDistributionRequestedACK_T_V_7 = LoadDistributionRequestedACK_Error;
  LoadDistributionRequestedACK_T Test_LoadDistributionRequestedACK_T_V_8 = LoadDistributionRequestedACK_NotAvailable;

  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_1 = LoadDistributionRequested_NoAction;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_2 = LoadDistributionRequested_DefaultLoadDistribution;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_3 = LoadDistributionRequested_NormalLoadDistribution;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_4 = LoadDistributionRequested_OptimizedTraction;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_5 = LoadDistributionRequested_MaximumTraction1;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_6 = LoadDistributionRequested_MaximumTraction2;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_7 = LoadDistributionRequested_AlternativeLoadDistribution;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_8 = LoadDistributionRequested_VariableLoadDistribution;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_9 = LoadDistributionRequested_DelpressLoadDistribution;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_10 = LoadDistributionRequested_Reserved1;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_11 = LoadDistributionRequested_Reserved2;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_12 = LoadDistributionRequested_Reserved3;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_13 = LoadDistributionRequested_Reserved4;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_14 = LoadDistributionRequested_Reserved5;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_15 = LoadDistributionRequested_Error;
  LoadDistributionRequested_T Test_LoadDistributionRequested_T_V_16 = LoadDistributionRequested_NotAvailable;

  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_1 = LoadDistributionSelected_NormalLoad;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_2 = LoadDistributionSelected_OptimisedTraction;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_3 = LoadDistributionSelected_StartingHelp;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_4 = LoadDistributionSelected_TractionHelp;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_5 = LoadDistributionSelected_AlternativeLoad;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_6 = LoadDistributionSelected_StartingHelpInterDiff;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_7 = LoadDistributionSelected_TractionHelpInterDiff;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_8 = LoadDistributionSelected_NoLoadDistribution;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_9 = LoadDistributionSelected_VariableAxleLoad;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_10 = LoadDistributionSelected_StartingHelp2;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_11 = LoadDistributionSelected_TractionHelp2;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_12 = LoadDistributionSelected_Reserved_03;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_13 = LoadDistributionSelected_Reserved_04;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_14 = LoadDistributionSelected_Reserved_05;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_15 = LoadDistributionSelected_Error;
  LoadDistributionSelected_T Test_LoadDistributionSelected_T_V_16 = LoadDistributionSelected_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
