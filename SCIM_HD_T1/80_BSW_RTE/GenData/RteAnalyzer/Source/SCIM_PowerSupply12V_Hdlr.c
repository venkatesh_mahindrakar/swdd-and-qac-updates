/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SCIM_PowerSupply12V_Hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  SCIM_PowerSupply12V_Hdlr
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <SCIM_PowerSupply12V_Hdlr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * IOHWAB_BOOL
 *   
 *
 * IOHWAB_UINT8
 *   
 *
 * VGTT_EcuPinFaultStatus
 *   
 *
 * VGTT_EcuPinVoltage_0V2
 *   
 *
 *********************************************************************************************************************/

#include "Rte_SCIM_PowerSupply12V_Hdlr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_SCIM_PowerSupply12V_Hdlr.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void SCIM_PowerSupply12V_Hdlr_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * IOHWAB_BOOL: Boolean
 * IOHWAB_UINT8: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * Living12VPowerStability: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Active (1U)
 *   Stable (2U)
 *   Error (3U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 *********************************************************************************************************************/


#define SCIM_PowerSupply12V_Hdlr_START_SEC_CODE
#include "SCIM_PowerSupply12V_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_PowerSupply12V_Hdlr_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Living12VResetRequest_Living12VResetRequest(Boolean *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Living12VPowerStability_Living12VPowerStability(Living12VPowerStability data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Issm_GetAllActiveIss_GetAllActiveIss(uint32 *activeIssField)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_GetAllActiveIss_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_PowerSupply12V_Hdlr_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PowerSupply12V_Hdlr_CODE) SCIM_PowerSupply12V_Hdlr_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_PowerSupply12V_Hdlr_10ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Boolean Read_Living12VResetRequest_Living12VResetRequest;
  VehicleMode_T Read_VehicleModeInternal_VehicleMode;

  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDcdc12VState_CS_DcDc12vRefVoltage = 0U;
  IOHWAB_BOOL Call_Do12VInterface_P_GetDcdc12VState_CS_IsDcDc12vActivated = FALSE;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDcdc12VState_CS_FaultStatus = 0U;
  IOHWAB_BOOL Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus = 0U;

  uint32 Call_Issm_GetAllActiveIss_GetAllActiveIss_activeIssField = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_SCIM_PowerSupply12V_Hdlr_Rte_Read_Living12VResetRequest_Living12VResetRequest(&Read_Living12VResetRequest_Living12VResetRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SCIM_PowerSupply12V_Hdlr_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SCIM_PowerSupply12V_Hdlr_Rte_Write_Living12VPowerStability_Living12VPowerStability(Rte_InitValue_Living12VPowerStability_Living12VPowerStability);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(&Call_Do12VInterface_P_GetDcdc12VState_CS_DcDc12vRefVoltage, &Call_Do12VInterface_P_GetDcdc12VState_CS_IsDcDc12vActivated, &Call_Do12VInterface_P_GetDcdc12VState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0U, &Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated, &Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Issm_GetAllActiveIss_GetAllActiveIss(&Call_Issm_GetAllActiveIss_GetAllActiveIss_activeIssField);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_GetAllActiveIss_E_NOT_OK:
      fct_error = 1;
      break;
  }

  SCIM_PowerSupply12V_Hdlr_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SCIM_PowerSupply12V_Hdlr_STOP_SEC_CODE
#include "SCIM_PowerSupply12V_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void SCIM_PowerSupply12V_Hdlr_TestDefines(void)
{
  /* Enumeration Data Types */

  IOCtrlReq_T Test_IOCtrlReq_T_V_1 = IOCtrl_AppRequest;
  IOCtrlReq_T Test_IOCtrlReq_T_V_2 = IOCtrl_DiagReturnCtrlToApp;
  IOCtrlReq_T Test_IOCtrlReq_T_V_3 = IOCtrl_DiagShortTermAdjust;

  Living12VPowerStability Test_Living12VPowerStability_V_1 = Inactive;
  Living12VPowerStability Test_Living12VPowerStability_V_2 = Active;
  Living12VPowerStability Test_Living12VPowerStability_V_3 = Stable;
  Living12VPowerStability Test_Living12VPowerStability_V_4 = Error;

  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_1 = TestNotRun;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_2 = OffState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_3 = OffState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_4 = OffState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_5 = OffState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_6 = OffState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_7 = OffState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_8 = OnState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_9 = OnState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_10 = OnState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_11 = OnState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_12 = OnState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_13 = OnState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_14 = OnState_FaultDetected_VOR;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_15 = OnState_FaultDetected_CAT;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
