/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DiagnosticComponent.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_DiagnosticComponent_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Read_DayUTC_DayUTC(Days8bit_Fact025_T *data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Read_HoursUTC_HoursUTC(Hours8bit_T *data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Read_LpModeRunTime_LpModeRunTime(uint32 *data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Read_MinutesUTC_MinutesUTC(Minutes8bit_T *data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Read_MonthUTC_MonthUTC(Months8bit_T *data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Read_SecondsUTC_SecondsUTC(Seconds8bitFact025_T *data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Read_YearUTC_YearUTC(Years8bit_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_DiagnosticComponent_Rte_Write_DiagActiveState_isDiagActive(DiagActiveState_T data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Write_DiagActiveState_isDiagActive(DiagActiveState_T data);
Std_ReturnType TSC_DiagnosticComponent_Rte_Write_LpModeRunTime_LpModeRunTime(uint32 data);

/** Sender receiver - update flag */
boolean TSC_DiagnosticComponent_Rte_IsUpdated_SecondsUTC_SecondsUTC(void);

/** Service interfaces */
Std_ReturnType TSC_DiagnosticComponent_Rte_Call_UR_ANW_Dcm_ActivateIss(void);
Std_ReturnType TSC_DiagnosticComponent_Rte_Call_UR_ANW_Dcm_DeactivateIss(void);
Std_ReturnType TSC_DiagnosticComponent_Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(Dem_OperationCycleStateType CycleState);

/** Explicit inter-runnable variables */
Days8bit_Fact025_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Day_ReadData_IrvDayUTC(void);
Hours8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Hour_ReadData_IrvHoursUTC(void);
Minutes8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Minutes_ReadData_IrvMinutesUTC(void);
Months8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Month_ReadData_IrvMonthUTC(void);
Seconds8bitFact025_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Seconds_ReadData_IrvSecondsUTC(void);
Years8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Year_ReadData_IrvYearUTC(void);
Days8bit_Fact025_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Day_ReadData_IrvDayUTC(void);
Hours8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Hour_ReadData_IrvHoursUTC(void);
Minutes8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData_IrvMinutesUTC(void);
Months8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Month_ReadData_IrvMonthUTC(void);
Seconds8bitFact025_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData_IrvSecondsUTC(void);
Years8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Year_ReadData_IrvYearUTC(void);
Days8bit_Fact025_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvDayUTC(void);
Hours8bit_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC(void);
Minutes8bit_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC(void);
Months8bit_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC(void);
Seconds8bitFact025_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC(void);
Years8bit_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvYearUTC(void);
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvDayUTC(Days8bit_Fact025_T);
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC(Hours8bit_T);
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC(Minutes8bit_T);
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC(Months8bit_T);
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC(Seconds8bitFact025_T);
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvYearUTC(Years8bit_T);




