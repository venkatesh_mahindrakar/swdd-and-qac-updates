/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  FlexibleSwitchesRouter_Ctrl_Router.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  FlexibleSwitchesRouter_Ctrl_Router
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <FlexibleSwitchesRouter_Ctrl_Router>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * Dem_EventStatusType
 *   
 *
 * Dem_InitMonitorReasonType
 *   
 *
 * SEWS_FS_DiagAct_ID001_ID009_P1EAA_T
 *   
 *
 * SEWS_FS_DiagAct_ID010_ID019_P1EAB_T
 *   
 *
 * SEWS_FS_DiagAct_ID02_ID029_P1EAC_T
 *   
 *
 * SEWS_FS_DiagAct_ID030_ID039_P1EAD_T
 *   
 *
 * SEWS_FS_DiagAct_ID040_ID049_P1EAE_T
 *   
 *
 * SEWS_FS_DiagAct_ID050_ID059_P1EAF_T
 *   
 *
 * SEWS_FS_DiagAct_ID060_ID069_P1EAG_T
 *   
 *
 * SEWS_FS_DiagAct_ID070_ID079_P1EAH_T
 *   
 *
 * SEWS_FS_DiagAct_ID080_ID089_P1EAI_T
 *   
 *
 * SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T
 *   
 *
 * SEWS_FS_DiagAct_ID100_ID109_P1EAK_T
 *   
 *
 * SEWS_FS_DiagAct_ID110_ID119_P1EAL_T
 *   
 *
 * SEWS_FS_DiagAct_ID120_ID129_P1EAM_T
 *   
 *
 * SEWS_FS_DiagAct_ID130_ID139_P1EAN_T
 *   
 *
 * SEWS_FS_DiagAct_ID140_ID149_P1EAO_T
 *   
 *
 * SEWS_FS_DiagAct_ID150_ID159_P1EAP_T
 *   
 *
 * SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T
 *   
 *
 * SEWS_FS_DiagAct_ID170_ID179_P1EAR_T
 *   
 *
 * SEWS_FS_DiagAct_ID180_ID189_P1EAS_T
 *   
 *
 * SEWS_FS_DiagAct_ID190_ID199_P1EAT_T
 *   
 *
 * SEWS_FS_DiagAct_ID200_ID209_P1EAU_T
 *   
 *
 * SEWS_FS_DiagAct_ID210_ID219_P1EAV_T
 *   
 *
 * SEWS_FS_DiagAct_ID220_ID229_P1EAW_T
 *   
 *
 * SEWS_FS_DiagAct_ID230_ID239_P1EAX_T
 *   
 *
 * SEWS_FS_DiagAct_ID240_ID249_P1EAY_T
 *   
 *
 * SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T
 *   
 *
 * SEWS_LIN_topology_P1AJR_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_FlexibleSwitchesRouter_Ctrl_Router.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_FlexibleSwitchesRouter_Ctrl_Router.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void FlexibleSwitchesRouter_Ctrl_Router_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_FS_DiagAct_ID001_ID009_P1EAA_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID010_ID019_P1EAB_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID02_ID029_P1EAC_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID030_ID039_P1EAD_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID040_ID049_P1EAE_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID050_ID059_P1EAF_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID060_ID069_P1EAG_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID070_ID079_P1EAH_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID080_ID089_P1EAI_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID100_ID109_P1EAK_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID110_ID119_P1EAL_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID120_ID129_P1EAM_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID130_ID139_P1EAN_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID140_ID149_P1EAO_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID150_ID159_P1EAP_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID170_ID179_P1EAR_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID180_ID189_P1EAS_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID190_ID199_P1EAT_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID200_ID209_P1EAU_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID210_ID219_P1EAV_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID220_ID229_P1EAW_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID230_ID239_P1EAX_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID240_ID249_P1EAY_T: Integer in interval [0...65535]
 * SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T: Integer in interval [0...65535]
 * SEWS_LIN_topology_P1AJR_T: Integer in interval [0...255]
 * SwitchDetectionNeeded_T: Boolean
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * Dem_InitMonitorReasonType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_INIT_MONITOR_CLEAR (1U)
 *   DEM_INIT_MONITOR_RESTART (2U)
 *   DEM_INIT_MONITOR_REENABLED (3U)
 *   DEM_INIT_MONITOR_STORAGE_REENABLED (4U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 *   DualDeviceIndication_UpperOffLowerOff (0U)
 *   DualDeviceIndication_UpperOnLowerOff (1U)
 *   DualDeviceIndication_UpperBlinkLowerOff (2U)
 *   DualDeviceIndication_UpperDontCareLowerOff (3U)
 *   DualDeviceIndication_UpperOffLowerOn (4U)
 *   DualDeviceIndication_UpperOnLowerOn (5U)
 *   DualDeviceIndication_UpperBlinkLowerOn (6U)
 *   DualDeviceIndication_UpperDontCareLowerOn (7U)
 *   DualDeviceIndication_UpperOffLowerBlink (8U)
 *   DualDeviceIndication_UpperOnLowerBlink (9U)
 *   DualDeviceIndication_UpperBlinkLowerBlink (10U)
 *   DualDeviceIndication_UpperDontCareLowerBlink (11U)
 *   DualDeviceIndication_UpperOffLowerDontCare (12U)
 *   DualDeviceIndication_UpperOnLowerDontCare (13U)
 *   DualDeviceIndication_UpperBlinkLowerDontCare (14U)
 *   DualDeviceIndication_UpperDontCareLowerDontCare (15U)
 * IndicationCmd_T: Enumeration of integer in interval [0...1] with enumerators
 *   IndicationCmd_OFFLEDNotActivated (0U)
 *   IndicationCmd_ONLEDActivated (1U)
 * NeutralPushed_T: Enumeration of integer in interval [0...3] with enumerators
 *   NeutralPushed_Neutral (0U)
 *   NeutralPushed_Pushed (1U)
 *   NeutralPushed_Error (2U)
 *   NeutralPushed_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data120ByteType: Array with 120 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * Dcm_Data8ByteType: Array with 8 element(s) of type uint8
 * FSPIndicationCmdArray_T: Array with 8 element(s) of type DeviceIndication_T
 * FSPSwitchStatusArray_T: Array with 8 element(s) of type PushButtonStatus_T
 * FSP_Array10_8: Array with 10 element(s) of type FSP_Array8
 * FSP_Array8: Array with 8 element(s) of type uint8
 * FlexibleSwDisableDiagPresence_Type: Array with 5 element(s) of type uint8
 * FlexibleSwitchesinFailure_Type: Array with 24 element(s) of type FlexibleSwitchesinFailure_T
 * SwitchDetectionResp_T: Array with 8 element(s) of type uint8
 *
 * Record Types:
 * =============
 * FlexibleSwitchesinFailure_T: Record with elements
 *   FlexibleSwitchFailureType of type uint8
 *   FlexibleSwitchID of type uint8
 *   FlexibleSwitchPosition of type uint8
 *   FlexibleSwitchPanel of type uint8
 *   LINbus of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   uint8 *Rte_Pim_Pim_P1DCT_Info(void)
 *   uint8 *Rte_Pim_Pim_FlexibleSwDisableDiagPresence(void)
 *     Returnvalue: uint8* is of type FlexibleSwDisableDiagPresence_Type
 *   FlexibleSwitchesinFailure_T *Rte_Pim_Pim_FlexibleSwFailureData(void)
 *     Returnvalue: FlexibleSwitchesinFailure_T* is of type FlexibleSwitchesinFailure_Type
 *   uint8 *Rte_Pim_Pim_FlexibleSwStatus(void)
 *     Returnvalue: uint8* is of type FSP_Array10_8
 *   uint8 *Rte_Pim_Pim_TableOfDetectedId(void)
 *     Returnvalue: uint8* is of type FSP_Array10_8
 *
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_FS_DiagAct_ID001_ID009_P1EAA_T Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v(void)
 *   SEWS_FS_DiagAct_ID010_ID019_P1EAB_T Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v(void)
 *   SEWS_FS_DiagAct_ID02_ID029_P1EAC_T Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v(void)
 *   SEWS_FS_DiagAct_ID030_ID039_P1EAD_T Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v(void)
 *   SEWS_FS_DiagAct_ID040_ID049_P1EAE_T Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v(void)
 *   SEWS_FS_DiagAct_ID050_ID059_P1EAF_T Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v(void)
 *   SEWS_FS_DiagAct_ID060_ID069_P1EAG_T Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v(void)
 *   SEWS_FS_DiagAct_ID070_ID079_P1EAH_T Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v(void)
 *   SEWS_FS_DiagAct_ID080_ID089_P1EAI_T Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v(void)
 *   SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v(void)
 *   SEWS_FS_DiagAct_ID100_ID109_P1EAK_T Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v(void)
 *   SEWS_FS_DiagAct_ID110_ID119_P1EAL_T Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v(void)
 *   SEWS_FS_DiagAct_ID120_ID129_P1EAM_T Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v(void)
 *   SEWS_FS_DiagAct_ID130_ID139_P1EAN_T Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v(void)
 *   SEWS_FS_DiagAct_ID140_ID149_P1EAO_T Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v(void)
 *   SEWS_FS_DiagAct_ID150_ID159_P1EAP_T Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v(void)
 *   SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v(void)
 *   SEWS_FS_DiagAct_ID170_ID179_P1EAR_T Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v(void)
 *   SEWS_FS_DiagAct_ID180_ID189_P1EAS_T Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v(void)
 *   SEWS_FS_DiagAct_ID190_ID199_P1EAT_T Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v(void)
 *   SEWS_FS_DiagAct_ID200_ID209_P1EAU_T Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v(void)
 *   SEWS_FS_DiagAct_ID210_ID219_P1EAV_T Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v(void)
 *   SEWS_FS_DiagAct_ID220_ID229_P1EAW_T Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v(void)
 *   SEWS_FS_DiagAct_ID230_ID239_P1EAX_T Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v(void)
 *   SEWS_FS_DiagAct_ID240_ID249_P1EAY_T Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v(void)
 *   SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v(void)
 *   SEWS_LIN_topology_P1AJR_T Rte_Prm_P1AJR_LIN_topology_v(void)
 *   boolean Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v(void)
 *
 *********************************************************************************************************************/


#define FlexibleSwitchesRouter_Ctrl_Router_START_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_Router_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <InitMonitorForEvent> of PortPrototype <CBInitEvt_D1BUL_31_FlexSwEEPROM>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOX_4A_FS_NotSupportedID_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOY_4A_FS_NotSupportedDoubleID_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BUL_31_FlexSwEEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BUL_95_FlexSwConfigFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CallbackInitMonitorForEvent_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_Event_D1BOX_4A_FS_NotSupportedID_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_Event_D1BOY_4A_FS_NotSupportedDoubleID_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_Event_D1BUL_31_FlexSwEEPROM_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_Event_D1BUL_95_FlexSwConfigFailure_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  FlexibleSwitchesRouter_Ctrl_Router_TestDefines();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <InitMonitorForEvent> of PortPrototype <CBInitEvt_D1BUL_95_FlexSwConfigFailure>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CallbackInitMonitorForEvent_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW0_Data_P1BW0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BW0_Data_P1BW0_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW1_Data_P1BW1_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BW1_Data_P1BW1_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW3_Data_P1BW3_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BW3_Data_P1BW3_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW4_Data_P1BW4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BW4_Data_P1BW4_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW5_Data_P1BW5_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BW5_Data_P1BW5_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW6_Data_P1BW6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BW6_Data_P1BW6_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW8_Data_P1BW8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BW8_Data_P1BW8_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BW9_Data_P1BW9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BW9_Data_P1BW9_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWI_Data_P1BWI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWI_Data_P1BWI_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWJ_Data_P1BWJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWJ_Data_P1BWJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWL_Data_P1BWL_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWL_Data_P1BWL_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWM_Data_P1BWM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWM_Data_P1BWM_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWN_Data_P1BWN_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWN_Data_P1BWN_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWO_Data_P1BWO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWO_Data_P1BWO_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWQ_Data_P1BWQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWQ_Data_P1BWQ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWR_Data_P1BWR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWR_Data_P1BWR_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWV_Data_P1BWV_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWV_Data_P1BWV_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BWX_Data_P1BWX_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BWX_Data_P1BWX_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BXD_Data_P1BXD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BXD_Data_P1BXD_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1BXF_Data_P1BXF_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1BXF_Data_P1BXF_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DCT_Data_P1DCT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1DCT_Data_P1DCT_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data120ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1GCM_Data_P1GCM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1GCM_Data_P1GCM_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ILR_Data_P1ILR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData (returns application error)
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  return RTE_E_DataServices_P1ILR_Data_P1ILR_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FlexibleSwitchesRouter_Ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ABSInhibit_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ABS_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ALD_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ASROff_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch1_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch2_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch3_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch4_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch5_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_AuxBbSwitch6_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_BeaconSRocker_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Beacon_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_BogieSwitch_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_BrakeBlending_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_CabTilt_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_CabWorkingLight_DevInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ChildLock_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ConstructionSwitch_DeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ContUnlock_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_CraneSupply_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DAS_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DoorLockSwitch_DeviceIndic_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ESCOff_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ESC_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EngineTmpryStopDisableDevInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EquipmentLightInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchEnableDeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ExtraSideMarkers_DeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FPBR_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP2SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP3SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP4SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP5SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP6SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP7SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP8SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP9SwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FSP_BSwitchStatus_FSPSwitchStatusArray(PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Read_FerryFunction_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FifthWheelLightInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FrontDiffLock_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FrontFog_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FrtAxleHydro_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_HillStartAid_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_InhibRegeneration_DeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_KneelDeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LCS_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LEDVega_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LKSCS_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_MaxTract_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_MirrorHeatingDeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_PTO1_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_PTO2_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_PTO3_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_PTO4_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ParkingHeater_IndicationCmd_IndicationCmd(IndicationCmd_T *data)
 *   Std_ReturnType Rte_Read_PloughtLights_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio1_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio2_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio4_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio5_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Ratio6_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RatioALD_DualDeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearAxleSteeringDeviceInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearDiffLockSwap_DeviceIndicat_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearDiffLock_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearFog_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ReducedSetMode_DevInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RegenerationIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Regeneration_DeviceInd_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ReverseWarningInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RrAutoDifflck_DeviceIndication_RrAutoDifflck_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RrAxlDifflck_DeviceIndication_RrAxlDifflck_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RrWhl1Difflck_DeviceIndication_RrWhl1Difflck_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RrWhl2Difflck_DeviceIndication_RrWhl2Difflck_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_SideReverseLightInd_cmd_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_SideWheelLightInd_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_Slid5thWheel_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_SpecialLightSituation_ind_SpecialLightSituation_ind(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_LIN_SwcActivation_LIN(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded1_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded2_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded3_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded4_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded5_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded6_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded7_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded8_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeeded9_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionNeededB_SwitchDetectionNeeded(SwitchDetectionNeeded_T *data)
 *   Std_ReturnType Rte_Read_SwitchDetectionResp1_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp2_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp3_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp4_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp5_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp6_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp7_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp8_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionResp9_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_SwitchDetectionRespB_SwitchDetectionResp(uint8 *data)
 *     Argument data: uint8* is of type SwitchDetectionResp_T
 *   Std_ReturnType Rte_Read_TCS_DualDeviceIndication_DualDeviceIndication(DualDeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_TailLift_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_TemporaryRSLDeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_TransferCaseNeutral_DevInd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_TridemALD_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_WarmUp_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_WorkingLight_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
 *   boolean Rte_IsUpdated_SwitchDetectionResp1_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp2_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp3_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp4_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp5_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp6_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp7_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp8_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionResp9_SwitchDetectionResp(void)
 *   boolean Rte_IsUpdated_SwitchDetectionRespB_SwitchDetectionResp(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ABSInhibitSwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ABSSwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_AEBS_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ASROffButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_AdjustFrontBeamInclination_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch4SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch5SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_AuxSwitch6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_BeaconSRocker_DeviceEvent_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Beacon_DeviceEven_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BlackOutConvoyModeSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_BrakeBlendingButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkB1ParkHeaterBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkB1ParkHeaterTempSw_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_CabTilt_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_CabWorkLight_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_Construction_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ContUnlockSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_CranePushButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_CraneSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_DAS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_DRLandAHSinhib_stat_DRLandAHSinhib_stat(NeutralPushed_T data)
 *   Std_ReturnType Rte_Write_DashboardLockButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ESCOff_SwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ESCReducedOff_Switch_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_EconomyPowerSwitch_status_EconomyPowerSwitch_status(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EngineTmpryStopButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EquipmentLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EscSwitchEnableStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EscSwitchIncDecStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_EscSwitchMuddySiteStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_EscSwitchResumeStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ExtraMainbeam_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ExtraSideMarkers_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FCW_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FPBRSwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP2IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP3IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP4IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP5IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP6IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP7IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP8IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP9IndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FSP_BIndicationCmd_FSPIndicationCmdArray(const DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Write_FerryFunctionSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FifthWheelLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FlexSwitchChildLockButton_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightFront_ButtonStatus_3_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightRear_ButtonStatus_3_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FrtAxleHydro_ButtonPush_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_HillStartAidButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_InhibRegenerationSwitch_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_InhibRegeneration_ButtonStat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtActvnBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtNightModeBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_IntLghtNightModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_KneelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LCS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LEDVega_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_LKSCS_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LKS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1Switch2_Status_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_LoadingLevelSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_MirrorFoldingSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_MirrorHeatingSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_PloughLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_PloughtLightsPushButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_Pto1SwitchStatus_Pto1SwitchStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto2SwitchStatus_Pto2SwitchStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto3SwitchStatus_Pto3SwitchStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto4SwitchStatus_Pto4SwitchStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_Ratio1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio4SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio5SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Ratio6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RatioALDSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RearAxleDiffLock_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_RearAxleSteering_DeviceEvent_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RearDifflockSwap_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ReducedSetModeButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_Regeneration2PosSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RegenerationPushButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_RegenerationSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ReverseGearWarningBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_ReverseGearWarningSw_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RoofHatch_SwitchStatus_1_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RoofHatch_SwitchStatus_2_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_RoofSignLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_RrAutoDifflockSwitch_stat_RrAutoDifflockSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_RrItrAxlDifflckSwitch_stat_RrItrAxlDifflckSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_RrItrWhl1DifflckSwitch_stat_RrItrWhl1DifflckSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_RrItrWhl2DifflckSwitch_stat_RrItrWhl2DifflckSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_SideReverseLight_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_SideReverseLight_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_Slid5thWheelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_SpotlightFront_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_SpotlightRoof_DeviceEvent_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_StatTrailerBrakeSwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_StaticCornerLight_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TailLiftPushButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_TailLiftSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TemporaryRSLSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ThreePosDifflockSwitch_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_ThreePosFrDiffLockSwitch_stat_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TractionControlSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TransferCaseNeutral_SwitchStat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_TridemALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_TwoPosDifflockSwitch_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data)
 *   Std_ReturnType Rte_Write_WarmUp_SwitchStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_WorkLight_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(Boolean data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_ASLight_InputFSP_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ASLight_InputFSP_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AlarmSetUnset1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AlarmSetUnset1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_BlackoutConvoyMode_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_BlackoutConvoyMode_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_CabTiltSwitchRequest_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_CabTiltSwitchRequest_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_FlexibleSwitchDetection_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_FlexibleSwitchDetection_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_InteriorLightsRqst1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_InteriorLightsRqst1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_RoofHatchRequest1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_RoofHatchRequest1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_WLight_InputFSP_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_WLight_InputFSP_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchesRouter_Ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FlexibleSwitchesRouter_Ctrl_Router_CODE) FlexibleSwitchesRouter_Ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchesRouter_Ctrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  DeviceIndication_T Read_ABSInhibit_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_ABS_Indication_DeviceIndication;
  DeviceIndication_T Read_ALD_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_ASROff_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_AuxBbSwitch1_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_AuxBbSwitch2_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_AuxBbSwitch3_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_AuxBbSwitch4_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_AuxBbSwitch5_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_AuxBbSwitch6_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_BeaconSRocker_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_Beacon_DeviceIndication_DeviceIndication;
  DualDeviceIndication_T Read_BogieSwitch_DeviceIndication_DualDeviceIndication;
  DeviceIndication_T Read_BrakeBlending_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_CabTilt_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_CabWorkingLight_DevInd_DeviceIndication;
  DeviceIndication_T Read_ChildLock_DeviceIndication_DeviceIndication;
  ComMode_LIN_Type Read_ComMode_LIN1_ComMode_LIN;
  ComMode_LIN_Type Read_ComMode_LIN2_ComMode_LIN;
  ComMode_LIN_Type Read_ComMode_LIN3_ComMode_LIN;
  ComMode_LIN_Type Read_ComMode_LIN4_ComMode_LIN;
  ComMode_LIN_Type Read_ComMode_LIN5_ComMode_LIN;
  DeviceIndication_T Read_ConstructionSwitch_DeviceInd_DeviceIndication;
  DeviceIndication_T Read_ContUnlock_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_CraneSupply_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_DAS_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_DoorAutoFuncInd_cmd_DeviceIndication;
  DeviceIndication_T Read_DoorLockSwitch_DeviceIndic_DeviceIndication;
  DeviceIndication_T Read_ESCOff_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_ESC_Indication_DeviceIndication;
  DeviceIndication_T Read_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd;
  DeviceIndication_T Read_EngineTmpryStopDisableDevInd_DeviceIndication;
  DeviceIndication_T Read_EquipmentLightInd_cmd_DeviceIndication;
  DeviceIndication_T Read_EscSwitchEnableDeviceInd_DeviceIndication;
  DeviceIndication_T Read_EscSwitchMuddySiteDeviceInd_DeviceIndication;
  DeviceIndication_T Read_ExtraSideMarkers_DeviceInd_DeviceIndication;
  DeviceIndication_T Read_FCW_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_FPBR_DeviceIndication_DeviceIndication;
  FSPSwitchStatusArray_T Read_FSP1SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Read_FSP2SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Read_FSP3SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Read_FSP4SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Read_FSP5SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Read_FSP6SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Read_FSP7SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Read_FSP8SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Read_FSP9SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Read_FSP_BSwitchStatus_FSPSwitchStatusArray;
  DeviceIndication_T Read_FerryFunction_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_FifthWheelLightInd_cmd_DeviceIndication;
  DeviceIndication_T Read_FrontDiffLock_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_FrontFog_Indication_DeviceIndication;
  DeviceIndication_T Read_FrtAxleHydro_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_HillStartAid_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_InhibRegeneration_DeviceInd_DeviceIndication;
  DeviceIndication_T Read_KneelDeviceIndication_DeviceIndication;
  DeviceIndication_T Read_LCS_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_LEDVega_DeviceIndication_DeviceIndication;
  DualDeviceIndication_T Read_LKSCS_DeviceIndication_DualDeviceIndication;
  DeviceIndication_T Read_LKS_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_MaxTract_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_MirrorHeatingDeviceIndication_DeviceIndication;
  DeviceIndication_T Read_PTO1_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_PTO2_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_PTO3_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_PTO4_DeviceIndication_DeviceIndication;
  IndicationCmd_T Read_ParkingHeater_IndicationCmd_IndicationCmd;
  DeviceIndication_T Read_PloughtLights_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_Ratio1_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_Ratio2_DeviceIndication_DeviceIndication;
  DualDeviceIndication_T Read_Ratio4_DeviceIndication_DualDeviceIndication;
  DualDeviceIndication_T Read_Ratio5_DeviceIndication_DualDeviceIndication;
  DeviceIndication_T Read_Ratio6_DeviceIndication_DeviceIndication;
  DualDeviceIndication_T Read_RatioALD_DualDeviceIndication_DualDeviceIndication;
  DeviceIndication_T Read_RearAxleSteeringDeviceInd_DeviceIndication;
  DeviceIndication_T Read_RearDiffLockSwap_DeviceIndicat_DeviceIndication;
  DeviceIndication_T Read_RearDiffLock_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_RearFog_Indication_DeviceIndication;
  DeviceIndication_T Read_ReducedSetMode_DevInd_DeviceIndication;
  DeviceIndication_T Read_RegenerationIndication_DeviceIndication;
  DualDeviceIndication_T Read_Regeneration_DeviceInd_DualDeviceIndication;
  DeviceIndication_T Read_ReverseWarningInd_cmd_DeviceIndication;
  DeviceIndication_T Read_RrAutoDifflck_DeviceIndication_RrAutoDifflck_DeviceIndication;
  DeviceIndication_T Read_RrAxlDifflck_DeviceIndication_RrAxlDifflck_DeviceIndication;
  DeviceIndication_T Read_RrWhl1Difflck_DeviceIndication_RrWhl1Difflck_DeviceIndication;
  DeviceIndication_T Read_RrWhl2Difflck_DeviceIndication_RrWhl2Difflck_DeviceIndication;
  DualDeviceIndication_T Read_SideReverseLightInd_cmd_DualDeviceIndication;
  DeviceIndication_T Read_SideWheelLightInd_cmd_DeviceIndication;
  DeviceIndication_T Read_Slid5thWheel_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_SpecialLightSituation_ind_SpecialLightSituation_ind;
  VehicleModeDistribution_T Read_SwcActivation_LIN_SwcActivation_LIN;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeeded1_SwitchDetectionNeeded;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeeded2_SwitchDetectionNeeded;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeeded3_SwitchDetectionNeeded;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeeded4_SwitchDetectionNeeded;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeeded5_SwitchDetectionNeeded;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeeded6_SwitchDetectionNeeded;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeeded7_SwitchDetectionNeeded;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeeded8_SwitchDetectionNeeded;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeeded9_SwitchDetectionNeeded;
  SwitchDetectionNeeded_T Read_SwitchDetectionNeededB_SwitchDetectionNeeded;
  SwitchDetectionResp_T Read_SwitchDetectionResp1_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionResp1_SwitchDetectionResp;
  SwitchDetectionResp_T Read_SwitchDetectionResp2_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionResp2_SwitchDetectionResp;
  SwitchDetectionResp_T Read_SwitchDetectionResp3_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionResp3_SwitchDetectionResp;
  SwitchDetectionResp_T Read_SwitchDetectionResp4_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionResp4_SwitchDetectionResp;
  SwitchDetectionResp_T Read_SwitchDetectionResp5_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionResp5_SwitchDetectionResp;
  SwitchDetectionResp_T Read_SwitchDetectionResp6_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionResp6_SwitchDetectionResp;
  SwitchDetectionResp_T Read_SwitchDetectionResp7_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionResp7_SwitchDetectionResp;
  SwitchDetectionResp_T Read_SwitchDetectionResp8_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionResp8_SwitchDetectionResp;
  SwitchDetectionResp_T Read_SwitchDetectionResp9_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionResp9_SwitchDetectionResp;
  SwitchDetectionResp_T Read_SwitchDetectionRespB_SwitchDetectionResp;
  boolean IsUpdated_SwitchDetectionRespB_SwitchDetectionResp;
  DualDeviceIndication_T Read_TCS_DualDeviceIndication_DualDeviceIndication;
  DeviceIndication_T Read_TailLift_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_TemporaryRSLDeviceIndication_DeviceIndication;
  DeviceIndication_T Read_TransferCaseNeutral_DevInd_DeviceIndication;
  DeviceIndication_T Read_TridemALD_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_WarmUp_DeviceIndication_DeviceIndication;
  DeviceIndication_T Read_WorkingLight_DeviceIndication_DeviceIndication;

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  FSPIndicationCmdArray_T Write_FSP1IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Write_FSP2IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Write_FSP3IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Write_FSP4IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Write_FSP5IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Write_FSP6IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Write_FSP7IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Write_FSP8IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Write_FSP9IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Write_FSP_BIndicationCmd_FSPIndicationCmdArray;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();

  IsUpdated_SwitchDetectionResp1_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionResp1_SwitchDetectionResp();
  IsUpdated_SwitchDetectionResp2_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionResp2_SwitchDetectionResp();
  IsUpdated_SwitchDetectionResp3_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionResp3_SwitchDetectionResp();
  IsUpdated_SwitchDetectionResp4_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionResp4_SwitchDetectionResp();
  IsUpdated_SwitchDetectionResp5_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionResp5_SwitchDetectionResp();
  IsUpdated_SwitchDetectionResp6_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionResp6_SwitchDetectionResp();
  IsUpdated_SwitchDetectionResp7_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionResp7_SwitchDetectionResp();
  IsUpdated_SwitchDetectionResp8_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionResp8_SwitchDetectionResp();
  IsUpdated_SwitchDetectionResp9_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionResp9_SwitchDetectionResp();
  IsUpdated_SwitchDetectionRespB_SwitchDetectionResp = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_IsUpdated_SwitchDetectionRespB_SwitchDetectionResp();

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ABSInhibit_DeviceIndication_DeviceIndication(&Read_ABSInhibit_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ABS_Indication_DeviceIndication(&Read_ABS_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ALD_DeviceIndication_DeviceIndication(&Read_ALD_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ASROff_DeviceIndication_DeviceIndication(&Read_ASROff_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_AuxBbSwitch1_DeviceIndication_DeviceIndication(&Read_AuxBbSwitch1_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_AuxBbSwitch2_DeviceIndication_DeviceIndication(&Read_AuxBbSwitch2_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_AuxBbSwitch3_DeviceIndication_DeviceIndication(&Read_AuxBbSwitch3_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_AuxBbSwitch4_DeviceIndication_DeviceIndication(&Read_AuxBbSwitch4_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_AuxBbSwitch5_DeviceIndication_DeviceIndication(&Read_AuxBbSwitch5_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_AuxBbSwitch6_DeviceIndication_DeviceIndication(&Read_AuxBbSwitch6_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_BeaconSRocker_DeviceIndication_DeviceIndication(&Read_BeaconSRocker_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_Beacon_DeviceIndication_DeviceIndication(&Read_Beacon_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_BogieSwitch_DeviceIndication_DualDeviceIndication(&Read_BogieSwitch_DeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_BrakeBlending_DeviceIndication_DeviceIndication(&Read_BrakeBlending_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_CabTilt_DeviceIndication_DeviceIndication(&Read_CabTilt_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_CabWorkingLight_DevInd_DeviceIndication(&Read_CabWorkingLight_DevInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ChildLock_DeviceIndication_DeviceIndication(&Read_ChildLock_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ComMode_LIN1_ComMode_LIN(&Read_ComMode_LIN1_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ComMode_LIN2_ComMode_LIN(&Read_ComMode_LIN2_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ComMode_LIN3_ComMode_LIN(&Read_ComMode_LIN3_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ComMode_LIN4_ComMode_LIN(&Read_ComMode_LIN4_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ComMode_LIN5_ComMode_LIN(&Read_ComMode_LIN5_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ConstructionSwitch_DeviceInd_DeviceIndication(&Read_ConstructionSwitch_DeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ContUnlock_DeviceIndication_DeviceIndication(&Read_ContUnlock_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_CraneSupply_DeviceIndication_DeviceIndication(&Read_CraneSupply_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_DAS_DeviceIndication_DeviceIndication(&Read_DAS_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_DoorAutoFuncInd_cmd_DeviceIndication(&Read_DoorAutoFuncInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_DoorLockSwitch_DeviceIndic_DeviceIndication(&Read_DoorLockSwitch_DeviceIndic_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ESCOff_DeviceIndication_DeviceIndication(&Read_ESCOff_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ESC_Indication_DeviceIndication(&Read_ESC_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd(&Read_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_EngineTmpryStopDisableDevInd_DeviceIndication(&Read_EngineTmpryStopDisableDevInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_EquipmentLightInd_cmd_DeviceIndication(&Read_EquipmentLightInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_EscSwitchEnableDeviceInd_DeviceIndication(&Read_EscSwitchEnableDeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_EscSwitchMuddySiteDeviceInd_DeviceIndication(&Read_EscSwitchMuddySiteDeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ExtraSideMarkers_DeviceInd_DeviceIndication(&Read_ExtraSideMarkers_DeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FCW_DeviceIndication_DeviceIndication(&Read_FCW_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FPBR_DeviceIndication_DeviceIndication(&Read_FPBR_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP1SwitchStatus_FSPSwitchStatusArray(Read_FSP1SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP2SwitchStatus_FSPSwitchStatusArray(Read_FSP2SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP3SwitchStatus_FSPSwitchStatusArray(Read_FSP3SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP4SwitchStatus_FSPSwitchStatusArray(Read_FSP4SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP5SwitchStatus_FSPSwitchStatusArray(Read_FSP5SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP6SwitchStatus_FSPSwitchStatusArray(Read_FSP6SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP7SwitchStatus_FSPSwitchStatusArray(Read_FSP7SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP8SwitchStatus_FSPSwitchStatusArray(Read_FSP8SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP9SwitchStatus_FSPSwitchStatusArray(Read_FSP9SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FSP_BSwitchStatus_FSPSwitchStatusArray(Read_FSP_BSwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FerryFunction_DeviceIndication_DeviceIndication(&Read_FerryFunction_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FifthWheelLightInd_cmd_DeviceIndication(&Read_FifthWheelLightInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FrontDiffLock_DeviceIndication_DeviceIndication(&Read_FrontDiffLock_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FrontFog_Indication_DeviceIndication(&Read_FrontFog_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_FrtAxleHydro_DeviceIndication_DeviceIndication(&Read_FrtAxleHydro_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_HillStartAid_DeviceIndication_DeviceIndication(&Read_HillStartAid_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_InhibRegeneration_DeviceInd_DeviceIndication(&Read_InhibRegeneration_DeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_KneelDeviceIndication_DeviceIndication(&Read_KneelDeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_LCS_DeviceIndication_DeviceIndication(&Read_LCS_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_LEDVega_DeviceIndication_DeviceIndication(&Read_LEDVega_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_LKSCS_DeviceIndication_DualDeviceIndication(&Read_LKSCS_DeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_LKS_DeviceIndication_DeviceIndication(&Read_LKS_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_MaxTract_DeviceIndication_DeviceIndication(&Read_MaxTract_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_MirrorHeatingDeviceIndication_DeviceIndication(&Read_MirrorHeatingDeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_PTO1_DeviceIndication_DeviceIndication(&Read_PTO1_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_PTO2_DeviceIndication_DeviceIndication(&Read_PTO2_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_PTO3_DeviceIndication_DeviceIndication(&Read_PTO3_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_PTO4_DeviceIndication_DeviceIndication(&Read_PTO4_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ParkingHeater_IndicationCmd_IndicationCmd(&Read_ParkingHeater_IndicationCmd_IndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_PloughtLights_DeviceIndication_DeviceIndication(&Read_PloughtLights_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_Ratio1_DeviceIndication_DeviceIndication(&Read_Ratio1_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_Ratio2_DeviceIndication_DeviceIndication(&Read_Ratio2_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_Ratio4_DeviceIndication_DualDeviceIndication(&Read_Ratio4_DeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_Ratio5_DeviceIndication_DualDeviceIndication(&Read_Ratio5_DeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_Ratio6_DeviceIndication_DeviceIndication(&Read_Ratio6_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RatioALD_DualDeviceIndication_DualDeviceIndication(&Read_RatioALD_DualDeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RearAxleSteeringDeviceInd_DeviceIndication(&Read_RearAxleSteeringDeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RearDiffLockSwap_DeviceIndicat_DeviceIndication(&Read_RearDiffLockSwap_DeviceIndicat_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RearDiffLock_DeviceIndication_DeviceIndication(&Read_RearDiffLock_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RearFog_Indication_DeviceIndication(&Read_RearFog_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ReducedSetMode_DevInd_DeviceIndication(&Read_ReducedSetMode_DevInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RegenerationIndication_DeviceIndication(&Read_RegenerationIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_Regeneration_DeviceInd_DualDeviceIndication(&Read_Regeneration_DeviceInd_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_ReverseWarningInd_cmd_DeviceIndication(&Read_ReverseWarningInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RrAutoDifflck_DeviceIndication_RrAutoDifflck_DeviceIndication(&Read_RrAutoDifflck_DeviceIndication_RrAutoDifflck_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RrAxlDifflck_DeviceIndication_RrAxlDifflck_DeviceIndication(&Read_RrAxlDifflck_DeviceIndication_RrAxlDifflck_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RrWhl1Difflck_DeviceIndication_RrWhl1Difflck_DeviceIndication(&Read_RrWhl1Difflck_DeviceIndication_RrWhl1Difflck_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_RrWhl2Difflck_DeviceIndication_RrWhl2Difflck_DeviceIndication(&Read_RrWhl2Difflck_DeviceIndication_RrWhl2Difflck_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SideReverseLightInd_cmd_DualDeviceIndication(&Read_SideReverseLightInd_cmd_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SideWheelLightInd_cmd_DeviceIndication(&Read_SideWheelLightInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_Slid5thWheel_DeviceIndication_DeviceIndication(&Read_Slid5thWheel_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SpecialLightSituation_ind_SpecialLightSituation_ind(&Read_SpecialLightSituation_ind_SpecialLightSituation_ind);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwcActivation_LIN_SwcActivation_LIN(&Read_SwcActivation_LIN_SwcActivation_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeeded1_SwitchDetectionNeeded(&Read_SwitchDetectionNeeded1_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeeded2_SwitchDetectionNeeded(&Read_SwitchDetectionNeeded2_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeeded3_SwitchDetectionNeeded(&Read_SwitchDetectionNeeded3_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeeded4_SwitchDetectionNeeded(&Read_SwitchDetectionNeeded4_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeeded5_SwitchDetectionNeeded(&Read_SwitchDetectionNeeded5_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeeded6_SwitchDetectionNeeded(&Read_SwitchDetectionNeeded6_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeeded7_SwitchDetectionNeeded(&Read_SwitchDetectionNeeded7_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeeded8_SwitchDetectionNeeded(&Read_SwitchDetectionNeeded8_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeeded9_SwitchDetectionNeeded(&Read_SwitchDetectionNeeded9_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionNeededB_SwitchDetectionNeeded(&Read_SwitchDetectionNeededB_SwitchDetectionNeeded);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionResp1_SwitchDetectionResp(Read_SwitchDetectionResp1_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionResp2_SwitchDetectionResp(Read_SwitchDetectionResp2_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionResp3_SwitchDetectionResp(Read_SwitchDetectionResp3_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionResp4_SwitchDetectionResp(Read_SwitchDetectionResp4_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionResp5_SwitchDetectionResp(Read_SwitchDetectionResp5_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionResp6_SwitchDetectionResp(Read_SwitchDetectionResp6_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionResp7_SwitchDetectionResp(Read_SwitchDetectionResp7_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionResp8_SwitchDetectionResp(Read_SwitchDetectionResp8_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionResp9_SwitchDetectionResp(Read_SwitchDetectionResp9_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_SwitchDetectionRespB_SwitchDetectionResp(Read_SwitchDetectionRespB_SwitchDetectionResp);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_TCS_DualDeviceIndication_DualDeviceIndication(&Read_TCS_DualDeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_TailLift_DeviceIndication_DeviceIndication(&Read_TailLift_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_TemporaryRSLDeviceIndication_DeviceIndication(&Read_TemporaryRSLDeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_TransferCaseNeutral_DevInd_DeviceIndication(&Read_TransferCaseNeutral_DevInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_TridemALD_DeviceIndication_DeviceIndication(&Read_TridemALD_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_WarmUp_DeviceIndication_DeviceIndication(&Read_WarmUp_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Read_WorkingLight_DeviceIndication_DeviceIndication(&Read_WorkingLight_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ABSInhibitSwitchStatus_PushButtonStatus(Rte_InitValue_ABSInhibitSwitchStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ABSSwitchStatus_PushButtonStatus(Rte_InitValue_ABSSwitchStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_AEBS_ButtonStatus_PushButtonStatus(Rte_InitValue_AEBS_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ALDSwitchStatus_A2PosSwitchStatus(Rte_InitValue_ALDSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ASROffButtonStatus_PushButtonStatus(Rte_InitValue_ASROffButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_AdjustFrontBeamInclination_A3PosSwitchStatus(Rte_InitValue_AdjustFrontBeamInclination_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(Rte_InitValue_AlternativeDriveLevelSw_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_AuxSwitch1SwitchStatus_A2PosSwitchStatus(Rte_InitValue_AuxSwitch1SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_AuxSwitch2SwitchStatus_A2PosSwitchStatus(Rte_InitValue_AuxSwitch2SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_AuxSwitch3SwitchStatus_A2PosSwitchStatus(Rte_InitValue_AuxSwitch3SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_AuxSwitch4SwitchStatus_A2PosSwitchStatus(Rte_InitValue_AuxSwitch4SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_AuxSwitch5SwitchStatus_A2PosSwitchStatus(Rte_InitValue_AuxSwitch5SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_AuxSwitch6SwitchStatus_A2PosSwitchStatus(Rte_InitValue_AuxSwitch6SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_BeaconSRocker_DeviceEvent_A2PosSwitchStatus(Rte_InitValue_BeaconSRocker_DeviceEvent_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Beacon_DeviceEven_PushButtonStatus(Rte_InitValue_Beacon_DeviceEven_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_BlackOutConvoyModeSwitchStatus_A3PosSwitchStatus(Rte_InitValue_BlackOutConvoyModeSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_BrakeBlendingButtonStatus_PushButtonStatus(Rte_InitValue_BrakeBlendingButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_BunkB1ParkHeaterBtn_stat_PushButtonStatus(Rte_InitValue_BunkB1ParkHeaterBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_BunkB1ParkHeaterTempSw_stat_A3PosSwitchStatus(Rte_InitValue_BunkB1ParkHeaterTempSw_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_CabTilt_SwitchStatus_A2PosSwitchStatus(Rte_InitValue_CabTilt_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_CabWorkLight_ButtonStatus_PushButtonStatus(Rte_InitValue_CabWorkLight_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Construction_SwitchStatus_A2PosSwitchStatus(Rte_InitValue_Construction_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ContUnlockSwitchStatus_A2PosSwitchStatus(Rte_InitValue_ContUnlockSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_CranePushButtonStatus_PushButtonStatus(Rte_InitValue_CranePushButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_CraneSwitchStatus_A2PosSwitchStatus(Rte_InitValue_CraneSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_DAS_SwitchStatus_A2PosSwitchStatus(Rte_InitValue_DAS_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_DRLandAHSinhib_stat_DRLandAHSinhib_stat(Rte_InitValue_DRLandAHSinhib_stat_DRLandAHSinhib_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_DashboardLockButtonStatus_PushButtonStatus(Rte_InitValue_DashboardLockButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ESCOff_SwitchStatus_PushButtonStatus(Rte_InitValue_ESCOff_SwitchStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ESCReducedOff_Switch_A2PosSwitchStatus(Rte_InitValue_ESCReducedOff_Switch_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_EconomyPowerSwitch_status_EconomyPowerSwitch_status(Rte_InitValue_EconomyPowerSwitch_status_EconomyPowerSwitch_status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_EngineTmpryStopButtonStatus_PushButtonStatus(Rte_InitValue_EngineTmpryStopButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_EquipmentLight_DeviceEvent_PushButtonStatus(Rte_InitValue_EquipmentLight_DeviceEvent_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_EscSwitchEnableStatus_PushButtonStatus(Rte_InitValue_EscSwitchEnableStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_EscSwitchIncDecStatus_A3PosSwitchStatus(Rte_InitValue_EscSwitchIncDecStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_EscSwitchMuddySiteStatus_PushButtonStatus(Rte_InitValue_EscSwitchMuddySiteStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_EscSwitchResumeStatus_PushButtonStatus(Rte_InitValue_EscSwitchResumeStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ExtraMainbeam_SwitchStatus_A2PosSwitchStatus(Rte_InitValue_ExtraMainbeam_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ExtraSideMarkers_DeviceEvent_PushButtonStatus(Rte_InitValue_ExtraSideMarkers_DeviceEvent_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FCW_SwitchStatus_A2PosSwitchStatus(Rte_InitValue_FCW_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FPBRSwitchStatus_PushButtonStatus(Rte_InitValue_FPBRSwitchStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP1IndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP1IndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP1IndicationCmd_FSPIndicationCmdArray(Write_FSP1IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP2IndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP2IndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP2IndicationCmd_FSPIndicationCmdArray(Write_FSP2IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP3IndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP3IndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP3IndicationCmd_FSPIndicationCmdArray(Write_FSP3IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP4IndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP4IndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP4IndicationCmd_FSPIndicationCmdArray(Write_FSP4IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP5IndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP5IndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP5IndicationCmd_FSPIndicationCmdArray(Write_FSP5IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP6IndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP6IndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP6IndicationCmd_FSPIndicationCmdArray(Write_FSP6IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP7IndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP7IndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP7IndicationCmd_FSPIndicationCmdArray(Write_FSP7IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP8IndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP8IndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP8IndicationCmd_FSPIndicationCmdArray(Write_FSP8IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP9IndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP9IndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP9IndicationCmd_FSPIndicationCmdArray(Write_FSP9IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP_BIndicationCmd_FSPIndicationCmdArray, 0, sizeof(Write_FSP_BIndicationCmd_FSPIndicationCmdArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FSP_BIndicationCmd_FSPIndicationCmdArray(Write_FSP_BIndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FerryFunctionSwitchStatus_A2PosSwitchStatus(Rte_InitValue_FerryFunctionSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FifthWheelLight_DeviceEvent_PushButtonStatus(Rte_InitValue_FifthWheelLight_DeviceEvent_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FlexSwitchChildLockButton_stat_A3PosSwitchStatus(Rte_InitValue_FlexSwitchChildLockButton_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus(Rte_InitValue_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus(Rte_InitValue_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FogLightFront_ButtonStatus_3_PushButtonStatus(Rte_InitValue_FogLightFront_ButtonStatus_3_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FogLightRear_ButtonStatus_3_PushButtonStatus(Rte_InitValue_FogLightRear_ButtonStatus_3_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_FrtAxleHydro_ButtonPush_PushButtonStatus(Rte_InitValue_FrtAxleHydro_ButtonPush_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_HillStartAidButtonStatus_PushButtonStatus(Rte_InitValue_HillStartAidButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_InhibRegenerationSwitch_stat_A2PosSwitchStatus(Rte_InitValue_InhibRegenerationSwitch_stat_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_InhibRegeneration_ButtonStat_PushButtonStatus(Rte_InitValue_InhibRegeneration_ButtonStat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_IntLghtActvnBtn_stat_A2PosSwitchStatus(Rte_InitValue_IntLghtActvnBtn_stat_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(Rte_InitValue_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(Rte_InitValue_IntLghtMaxModeFlxSw2_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_IntLghtNightModeBtn_stat_A2PosSwitchStatus(Rte_InitValue_IntLghtNightModeBtn_stat_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_IntLghtNightModeFlxSw2_stat_PushButtonStatus(Rte_InitValue_IntLghtNightModeFlxSw2_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_KneelSwitchStatus_A2PosSwitchStatus(Rte_InitValue_KneelSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LCS_SwitchStatus_A2PosSwitchStatus(Rte_InitValue_LCS_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LEDVega_DeviceEvent_PushButtonStatus(Rte_InitValue_LEDVega_DeviceEvent_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LKSCS_SwitchStatus_A3PosSwitchStatus(Rte_InitValue_LKSCS_SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LKS_SwitchStatus_A2PosSwitchStatus(Rte_InitValue_LKS_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(Rte_InitValue_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LiftAxle1Switch2_Status_A3PosSwitchStatus(Rte_InitValue_LiftAxle1Switch2_Status_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LiftAxle1SwitchStatus_A3PosSwitchStatus(Rte_InitValue_LiftAxle1SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(Rte_InitValue_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(Rte_InitValue_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LiftAxle2SwitchStatus_A3PosSwitchStatus(Rte_InitValue_LiftAxle2SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(Rte_InitValue_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(Rte_InitValue_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_LoadingLevelSwitchStatus_A3PosSwitchStatus(Rte_InitValue_LoadingLevelSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_MirrorFoldingSwitchStatus_A3PosSwitchStatus(Rte_InitValue_MirrorFoldingSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_MirrorHeatingSwitchStatus_A2PosSwitchStatus(Rte_InitValue_MirrorHeatingSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_PloughLight_DeviceEvent_PushButtonStatus(Rte_InitValue_PloughLight_DeviceEvent_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_PloughtLightsPushButtonStatus_PushButtonStatus(Rte_InitValue_PloughtLightsPushButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Pto1SwitchStatus_Pto1SwitchStatus(Rte_InitValue_Pto1SwitchStatus_Pto1SwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Pto2SwitchStatus_Pto2SwitchStatus(Rte_InitValue_Pto2SwitchStatus_Pto2SwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Pto3SwitchStatus_Pto3SwitchStatus(Rte_InitValue_Pto3SwitchStatus_Pto3SwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Pto4SwitchStatus_Pto4SwitchStatus(Rte_InitValue_Pto4SwitchStatus_Pto4SwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Ratio1SwitchStatus_A2PosSwitchStatus(Rte_InitValue_Ratio1SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Ratio2SwitchStatus_A2PosSwitchStatus(Rte_InitValue_Ratio2SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Ratio3SwitchStatus_A2PosSwitchStatus(Rte_InitValue_Ratio3SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Ratio4SwitchStatus_A3PosSwitchStatus(Rte_InitValue_Ratio4SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Ratio5SwitchStatus_A3PosSwitchStatus(Rte_InitValue_Ratio5SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Ratio6SwitchStatus_A2PosSwitchStatus(Rte_InitValue_Ratio6SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RatioALDSwitchStatus_A3PosSwitchStatus(Rte_InitValue_RatioALDSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RearAxleDiffLock_ButtonStatus_PushButtonStatus(Rte_InitValue_RearAxleDiffLock_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RearAxleSteering_DeviceEvent_A2PosSwitchStatus(Rte_InitValue_RearAxleSteering_DeviceEvent_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RearDifflockSwap_ButtonStatus_PushButtonStatus(Rte_InitValue_RearDifflockSwap_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ReducedSetModeButtonStatus_PushButtonStatus(Rte_InitValue_ReducedSetModeButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Regeneration2PosSwitchStatus_A2PosSwitchStatus(Rte_InitValue_Regeneration2PosSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RegenerationPushButtonStatus_PushButtonStatus(Rte_InitValue_RegenerationPushButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RegenerationSwitchStatus_A3PosSwitchStatus(Rte_InitValue_RegenerationSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ReverseGearWarningBtn_stat_PushButtonStatus(Rte_InitValue_ReverseGearWarningBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ReverseGearWarningSw_stat_A2PosSwitchStatus(Rte_InitValue_ReverseGearWarningSw_stat_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RoofHatch_SwitchStatus_1_A3PosSwitchStatus(Rte_InitValue_RoofHatch_SwitchStatus_1_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RoofHatch_SwitchStatus_2_A3PosSwitchStatus(Rte_InitValue_RoofHatch_SwitchStatus_2_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RoofSignLight_DeviceEvent_PushButtonStatus(Rte_InitValue_RoofSignLight_DeviceEvent_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RrAutoDifflockSwitch_stat_RrAutoDifflockSwitch_stat(Rte_InitValue_RrAutoDifflockSwitch_stat_RrAutoDifflockSwitch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RrItrAxlDifflckSwitch_stat_RrItrAxlDifflckSwitch_stat(Rte_InitValue_RrItrAxlDifflckSwitch_stat_RrItrAxlDifflckSwitch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RrItrWhl1DifflckSwitch_stat_RrItrWhl1DifflckSwitch_stat(Rte_InitValue_RrItrWhl1DifflckSwitch_stat_RrItrWhl1DifflckSwitch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_RrItrWhl2DifflckSwitch_stat_RrItrWhl2DifflckSwitch_stat(Rte_InitValue_RrItrWhl2DifflckSwitch_stat_RrItrWhl2DifflckSwitch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_SideReverseLight_ButtonStatus_PushButtonStatus(Rte_InitValue_SideReverseLight_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_SideReverseLight_SwitchStatus_A3PosSwitchStatus(Rte_InitValue_SideReverseLight_SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_Slid5thWheelSwitchStatus_A2PosSwitchStatus(Rte_InitValue_Slid5thWheelSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_SpotlightFront_DeviceEvent_PushButtonStatus(Rte_InitValue_SpotlightFront_DeviceEvent_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_SpotlightRoof_DeviceEvent_PushButtonStatus(Rte_InitValue_SpotlightRoof_DeviceEvent_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_StatTrailerBrakeSwitchStatus_PushButtonStatus(Rte_InitValue_StatTrailerBrakeSwitchStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_StaticCornerLight_SwitchStatus_A2PosSwitchStatus(Rte_InitValue_StaticCornerLight_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_TailLiftPushButtonStatus_PushButtonStatus(Rte_InitValue_TailLiftPushButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_TailLiftSwitchStatus_A2PosSwitchStatus(Rte_InitValue_TailLiftSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_TemporaryRSLSwitchStatus_A2PosSwitchStatus(Rte_InitValue_TemporaryRSLSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ThreePosDifflockSwitch_stat_A3PosSwitchStatus(Rte_InitValue_ThreePosDifflockSwitch_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_ThreePosFrDiffLockSwitch_stat_A3PosSwitchStatus(Rte_InitValue_ThreePosFrDiffLockSwitch_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_TractionControlSwitchStatus_A3PosSwitchStatus(Rte_InitValue_TractionControlSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_TransferCaseNeutral_SwitchStat_PushButtonStatus(Rte_InitValue_TransferCaseNeutral_SwitchStat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_TridemALDSwitchStatus_A2PosSwitchStatus(Rte_InitValue_TridemALDSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_TwoPosDifflockSwitch_stat_A2PosSwitchStatus(Rte_InitValue_TwoPosDifflockSwitch_stat_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_WarmUp_SwitchStatus_PushButtonStatus(Rte_InitValue_WarmUp_SwitchStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_WorkLight_ButtonStatus_PushButtonStatus(Rte_InitValue_WorkLight_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Write_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(Rte_InitValue_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_ASLight_InputFSP_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_ASLight_InputFSP_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_AlarmSetUnset1_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_AlarmSetUnset1_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_BlackoutConvoyMode_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_BlackoutConvoyMode_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_CabTiltSwitchRequest_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_CabTiltSwitchRequest_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_FlexibleSwitchDetection_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_FlexibleSwitchDetection_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_InteriorLightsRqst1_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_InteriorLightsRqst1_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_LockControlCabRqst1_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_LockControlCabRqst1_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_PHActMaintainLiving2_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_PHActMaintainLiving2_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_RoofHatchRequest1_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_RoofHatchRequest1_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_WLight_InputFSP_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Call_UR_ANW_WLight_InputFSP_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FlexibleSwitchesRouter_Ctrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchesRouter_Ctrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FlexibleSwitchesRouter_Ctrl_Router_CODE) FlexibleSwitchesRouter_Ctrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchesRouter_Ctrl_Init
 *********************************************************************************************************************/

  uint8 PimPim_P1DCT_Info;
  FlexibleSwDisableDiagPresence_Type PimPim_FlexibleSwDisableDiagPresence;
  FlexibleSwitchesinFailure_Type PimPim_FlexibleSwFailureData;
  FSP_Array10_8 PimPim_FlexibleSwStatus;
  FSP_Array10_8 PimPim_TableOfDetectedId;

  SEWS_FS_DiagAct_ID001_ID009_P1EAA_T P1EAA_FS_DiagAct_ID001_ID009_v_data;
  SEWS_FS_DiagAct_ID010_ID019_P1EAB_T P1EAB_FS_DiagAct_ID010_ID019_v_data;
  SEWS_FS_DiagAct_ID02_ID029_P1EAC_T P1EAC_FS_DiagAct_ID02_ID029_v_data;
  SEWS_FS_DiagAct_ID030_ID039_P1EAD_T P1EAD_FS_DiagAct_ID030_ID039_v_data;
  SEWS_FS_DiagAct_ID040_ID049_P1EAE_T P1EAE_FS_DiagAct_ID040_ID049_v_data;
  SEWS_FS_DiagAct_ID050_ID059_P1EAF_T P1EAF_FS_DiagAct_ID050_ID059_v_data;
  SEWS_FS_DiagAct_ID060_ID069_P1EAG_T P1EAG_FS_DiagAct_ID060_ID069_v_data;
  SEWS_FS_DiagAct_ID070_ID079_P1EAH_T P1EAH_FS_DiagAct_ID070_ID079_v_data;
  SEWS_FS_DiagAct_ID080_ID089_P1EAI_T P1EAI_FS_DiagAct_ID080_ID089_v_data;
  SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T P1EAJ_FS_DiagAct_ID090_ID099_v_data;
  SEWS_FS_DiagAct_ID100_ID109_P1EAK_T P1EAK_FS_DiagAct_ID100_ID109_v_data;
  SEWS_FS_DiagAct_ID110_ID119_P1EAL_T P1EAL_FS_DiagAct_ID110_ID119_v_data;
  SEWS_FS_DiagAct_ID120_ID129_P1EAM_T P1EAM_FS_DiagAct_ID120_ID129_v_data;
  SEWS_FS_DiagAct_ID130_ID139_P1EAN_T P1EAN_FS_DiagAct_ID130_ID139_v_data;
  SEWS_FS_DiagAct_ID140_ID149_P1EAO_T P1EAO_FS_DiagAct_ID140_ID149_v_data;
  SEWS_FS_DiagAct_ID150_ID159_P1EAP_T P1EAP_FS_DiagAct_ID150_ID159_v_data;
  SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T P1EAQ_FS_DiagAct_ID160_ID169_v_data;
  SEWS_FS_DiagAct_ID170_ID179_P1EAR_T P1EAR_FS_DiagAct_ID170_ID179_v_data;
  SEWS_FS_DiagAct_ID180_ID189_P1EAS_T P1EAS_FS_DiagAct_ID180_ID189_v_data;
  SEWS_FS_DiagAct_ID190_ID199_P1EAT_T P1EAT_FS_DiagAct_ID190_ID199_v_data;
  SEWS_FS_DiagAct_ID200_ID209_P1EAU_T P1EAU_FS_DiagAct_ID200_ID209_v_data;
  SEWS_FS_DiagAct_ID210_ID219_P1EAV_T P1EAV_FS_DiagAct_ID210_ID219_v_data;
  SEWS_FS_DiagAct_ID220_ID229_P1EAW_T P1EAW_FS_DiagAct_ID220_ID229_v_data;
  SEWS_FS_DiagAct_ID230_ID239_P1EAX_T P1EAX_FS_DiagAct_ID230_ID239_v_data;
  SEWS_FS_DiagAct_ID240_ID249_P1EAY_T P1EAY_FS_DiagAct_ID240_ID249_v_data;
  SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T P1EAZ_FS_DiagAct_ID250_ID254_v_data;
  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1BWZ_DoubleRoofHatchSwConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimPim_P1DCT_Info = *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info();
  *TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_P1DCT_Info() = PimPim_P1DCT_Info;

  (void)memcpy(PimPim_FlexibleSwDisableDiagPresence, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), sizeof(FlexibleSwDisableDiagPresence_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwDisableDiagPresence(), PimPim_FlexibleSwDisableDiagPresence, sizeof(FlexibleSwDisableDiagPresence_Type));


  (void)memcpy(PimPim_FlexibleSwFailureData, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), sizeof(FlexibleSwitchesinFailure_Type));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwFailureData(), PimPim_FlexibleSwFailureData, sizeof(FlexibleSwitchesinFailure_Type));


  (void)memcpy(PimPim_FlexibleSwStatus, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_FlexibleSwStatus(), PimPim_FlexibleSwStatus, sizeof(FSP_Array10_8));


  (void)memcpy(PimPim_TableOfDetectedId, TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), sizeof(FSP_Array10_8));
  (void)memcpy(TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Pim_Pim_TableOfDetectedId(), PimPim_TableOfDetectedId, sizeof(FSP_Array10_8));


  P1EAA_FS_DiagAct_ID001_ID009_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v();
  P1EAB_FS_DiagAct_ID010_ID019_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v();
  P1EAC_FS_DiagAct_ID02_ID029_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v();
  P1EAD_FS_DiagAct_ID030_ID039_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v();
  P1EAE_FS_DiagAct_ID040_ID049_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v();
  P1EAF_FS_DiagAct_ID050_ID059_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v();
  P1EAG_FS_DiagAct_ID060_ID069_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v();
  P1EAH_FS_DiagAct_ID070_ID079_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v();
  P1EAI_FS_DiagAct_ID080_ID089_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v();
  P1EAJ_FS_DiagAct_ID090_ID099_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v();
  P1EAK_FS_DiagAct_ID100_ID109_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v();
  P1EAL_FS_DiagAct_ID110_ID119_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v();
  P1EAM_FS_DiagAct_ID120_ID129_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v();
  P1EAN_FS_DiagAct_ID130_ID139_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v();
  P1EAO_FS_DiagAct_ID140_ID149_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v();
  P1EAP_FS_DiagAct_ID150_ID159_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v();
  P1EAQ_FS_DiagAct_ID160_ID169_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v();
  P1EAR_FS_DiagAct_ID170_ID179_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v();
  P1EAS_FS_DiagAct_ID180_ID189_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v();
  P1EAT_FS_DiagAct_ID190_ID199_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v();
  P1EAU_FS_DiagAct_ID200_ID209_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v();
  P1EAV_FS_DiagAct_ID210_ID219_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v();
  P1EAW_FS_DiagAct_ID220_ID229_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v();
  P1EAX_FS_DiagAct_ID230_ID239_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v();
  P1EAY_FS_DiagAct_ID240_ID249_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v();
  P1EAZ_FS_DiagAct_ID250_ID254_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v();
  P1AJR_LIN_topology_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1AJR_LIN_topology_v();
  P1BWZ_DoubleRoofHatchSwConfig_v_data = TSC_FlexibleSwitchesRouter_Ctrl_Router_Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define FlexibleSwitchesRouter_Ctrl_Router_STOP_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_Router_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void FlexibleSwitchesRouter_Ctrl_Router_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_1 = A3PosSwitchStatus_Middle;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_2 = A3PosSwitchStatus_Lower;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_3 = A3PosSwitchStatus_Upper;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_4 = A3PosSwitchStatus_Spare;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_5 = A3PosSwitchStatus_Spare_01;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_6 = A3PosSwitchStatus_Spare_02;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_7 = A3PosSwitchStatus_Error;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_8 = A3PosSwitchStatus_NotAvailable;

  ComMode_LIN_Type Test_ComMode_LIN_Type_V_1 = Inactive;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_2 = Diagnostic;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_3 = SwitchDetection;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_4 = ApplicationMonitoring;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_5 = Calibration;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_6 = Spare1;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_7 = Error;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_8 = NotAvailable;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_1 = DEM_INIT_MONITOR_CLEAR;
  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_2 = DEM_INIT_MONITOR_RESTART;
  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_3 = DEM_INIT_MONITOR_REENABLED;
  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_4 = DEM_INIT_MONITOR_STORAGE_REENABLED;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DualDeviceIndication_T Test_DualDeviceIndication_T_V_1 = DualDeviceIndication_UpperOffLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_2 = DualDeviceIndication_UpperOnLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_3 = DualDeviceIndication_UpperBlinkLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_4 = DualDeviceIndication_UpperDontCareLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_5 = DualDeviceIndication_UpperOffLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_6 = DualDeviceIndication_UpperOnLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_7 = DualDeviceIndication_UpperBlinkLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_8 = DualDeviceIndication_UpperDontCareLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_9 = DualDeviceIndication_UpperOffLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_10 = DualDeviceIndication_UpperOnLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_11 = DualDeviceIndication_UpperBlinkLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_12 = DualDeviceIndication_UpperDontCareLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_13 = DualDeviceIndication_UpperOffLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_14 = DualDeviceIndication_UpperOnLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_15 = DualDeviceIndication_UpperBlinkLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_16 = DualDeviceIndication_UpperDontCareLowerDontCare;

  IndicationCmd_T Test_IndicationCmd_T_V_1 = IndicationCmd_OFFLEDNotActivated;
  IndicationCmd_T Test_IndicationCmd_T_V_2 = IndicationCmd_ONLEDActivated;

  NeutralPushed_T Test_NeutralPushed_T_V_1 = NeutralPushed_Neutral;
  NeutralPushed_T Test_NeutralPushed_T_V_2 = NeutralPushed_Pushed;
  NeutralPushed_T Test_NeutralPushed_T_V_3 = NeutralPushed_Error;
  NeutralPushed_T Test_NeutralPushed_T_V_4 = NeutralPushed_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_IV_1 = InvalidValue_PushButtonStatus_T;
  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
