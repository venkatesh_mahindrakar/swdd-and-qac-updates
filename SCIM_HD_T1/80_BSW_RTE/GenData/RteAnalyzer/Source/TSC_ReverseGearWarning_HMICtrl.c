/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ReverseGearWarning_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_ReverseGearWarning_HMICtrl.h"
#include "TSC_ReverseGearWarning_HMICtrl.h"








Std_ReturnType TSC_ReverseGearWarning_HMICtrl_Rte_Read_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(uint8 *data)
{
  return Rte_Read_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(data);
}

Std_ReturnType TSC_ReverseGearWarning_HMICtrl_Rte_Read_ReverseGearWarningBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_ReverseGearWarningBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_ReverseGearWarning_HMICtrl_Rte_Read_ReverseGearWarningSw_stat_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_ReverseGearWarningSw_stat_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_ReverseGearWarning_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}




Std_ReturnType TSC_ReverseGearWarning_HMICtrl_Rte_Write_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(const uint8 *data)
{
  return Rte_Write_RGW_HMICtrl_NVM_I_RGW_HMICtrl_NVM_I(data);
}

Std_ReturnType TSC_ReverseGearWarning_HMICtrl_Rte_Write_ReverseWarningInd_cmd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_ReverseWarningInd_cmd_DeviceIndication(data);
}

Std_ReturnType TSC_ReverseGearWarning_HMICtrl_Rte_Write_ReverseWarning_rqst_ReverseWarning_rqst(ReverseWarning_rqst_T data)
{
  return Rte_Write_ReverseWarning_rqst_ReverseWarning_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





boolean  TSC_ReverseGearWarning_HMICtrl_Rte_Prm_P1BXH_ReverseWarning_SwType_v(void)
{
  return (boolean ) Rte_Prm_P1BXH_ReverseWarning_SwType_v();
}
boolean  TSC_ReverseGearWarning_HMICtrl_Rte_Prm_P1AJJ_ReverseWarning_Act_v(void)
{
  return (boolean ) Rte_Prm_P1AJJ_ReverseWarning_Act_v();
}


     /* ReverseGearWarning_HMICtrl */
      /* ReverseGearWarning_HMICtrl */



