/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VehicleModeDistribution.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  VehicleModeDistribution
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VehicleModeDistribution>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   
 *
 *********************************************************************************************************************/

#include "Rte_VehicleModeDistribution.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_VehicleModeDistribution.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void VehicleModeDistribution_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * Living12VPowerStability: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Active (1U)
 *   Stable (2U)
 *   Error (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


#define VehicleModeDistribution_START_SEC_CODE
#include "VehicleModeDistribution_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  VehicleModeDistribution_TestDefines();

  return RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  return RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  return RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1QXJ_Data_P1QXJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_DataServices_P1QXJ_Data_P1QXJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1RG1_Data_P1RG1_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData (returns application error)
 *********************************************************************************************************************/

  return RTE_E_DataServices_P1RG1_Data_P1RG1_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VehicleModeDistribution_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
 *   Std_ReturnType Rte_Read_Living12VPowerStability_Living12VPowerStability(Living12VPowerStability *data)
 *   Std_ReturnType Rte_Read_VehicleMode_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_SwcActivation_Accessory_Accessory(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_LIN_SwcActivation_LIN(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_Living_Living(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_Parked_Parked(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T data)
 *   Std_ReturnType Rte_Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_VehicleModeInternal_VehicleMode(VehicleMode_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_CIOMOperStateRedundancy_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_CIOMOperStateRedundancy_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleModeDistribution_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VehicleModeDistribution_CODE) VehicleModeDistribution_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleModeDistribution_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Fsc_OperationalMode_T Read_Fsc_OperationalMode_P_Fsc_OperationalMode;
  Living12VPowerStability Read_Living12VPowerStability_Living12VPowerStability;
  VehicleMode_T Read_VehicleMode_VehicleMode;
  StandardNVM_T Read_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I;

  StandardNVM_T Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VehicleModeDistribution_Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&Read_Fsc_OperationalMode_P_Fsc_OperationalMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Read_Living12VPowerStability_Living12VPowerStability(&Read_Living12VPowerStability_Living12VPowerStability);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Read_VehicleMode_VehicleMode(&Read_VehicleMode_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Read_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(Read_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Write_SwcActivation_Accessory_Accessory(Rte_InitValue_SwcActivation_Accessory_Accessory);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Write_SwcActivation_EngineRun_EngineRun(Rte_InitValue_SwcActivation_EngineRun_EngineRun);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Write_SwcActivation_IgnitionOn_IgnitionOn(Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Write_SwcActivation_LIN_SwcActivation_LIN(Rte_InitValue_SwcActivation_LIN_SwcActivation_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Write_SwcActivation_Living_Living(Rte_InitValue_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Write_SwcActivation_Parked_Parked(Rte_InitValue_SwcActivation_Parked_Parked);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Write_SwcActivation_Security_SwcActivation_Security(Rte_InitValue_SwcActivation_Security_SwcActivation_Security);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I, 0, sizeof(Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I));
  fct_status = TSC_VehicleModeDistribution_Rte_Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Write_VehicleModeInternal_VehicleMode(Rte_InitValue_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Call_UR_ANW_CIOMOperStateRedundancy_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleModeDistribution_Rte_Call_UR_ANW_CIOMOperStateRedundancy_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VehicleModeDistribution_STOP_SEC_CODE
#include "VehicleModeDistribution_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void VehicleModeDistribution_TestDefines(void)
{
  /* Enumeration Data Types */

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_1 = FSC_ShutdownReady;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_2 = FSC_Reduced_12vDcDcLimit;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_3 = FSC_Reduced;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_4 = FSC_Operating;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_5 = FSC_Protecting;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_6 = FSC_Withstand;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_7 = FSC_NotAvailable;

  Living12VPowerStability Test_Living12VPowerStability_V_1 = Inactive;
  Living12VPowerStability Test_Living12VPowerStability_V_2 = Active;
  Living12VPowerStability Test_Living12VPowerStability_V_3 = Stable;
  Living12VPowerStability Test_Living12VPowerStability_V_4 = Error;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
