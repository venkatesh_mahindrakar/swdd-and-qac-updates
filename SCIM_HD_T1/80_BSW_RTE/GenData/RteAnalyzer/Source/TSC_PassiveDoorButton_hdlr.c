/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_PassiveDoorButton_hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_PassiveDoorButton_hdlr.h"
#include "TSC_PassiveDoorButton_hdlr.h"








Std_ReturnType TSC_PassiveDoorButton_hdlr_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Security_SwcActivation_Security(data);
}




Std_ReturnType TSC_PassiveDoorButton_hdlr_Rte_Write_LeftDoorButton_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_LeftDoorButton_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_PassiveDoorButton_hdlr_Rte_Write_RightDoorButton_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_RightDoorButton_stat_PushButtonStatus(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_PassiveDoorButton_hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef, AdiPinVoltage, BatteryVoltage, FaultStatus);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* PassiveDoorButton_hdlr */
      /* PassiveDoorButton_hdlr */



