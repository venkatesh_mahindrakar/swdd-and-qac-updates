/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DoubleSwitch_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DoubleSwitch_HMICtrl.h"
#include "TSC_DoubleSwitch_HMICtrl.h"








Std_ReturnType TSC_DoubleSwitch_HMICtrl_Rte_Read_RoofHatch_SwitchStatus_1_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
{
  return Rte_Read_RoofHatch_SwitchStatus_1_A3PosSwitchStatus(data);
}

Std_ReturnType TSC_DoubleSwitch_HMICtrl_Rte_Read_RoofHatch_SwitchStatus_2_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
{
  return Rte_Read_RoofHatch_SwitchStatus_2_A3PosSwitchStatus(data);
}

Std_ReturnType TSC_DoubleSwitch_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Living_Living(data);
}




Std_ReturnType TSC_DoubleSwitch_HMICtrl_Rte_Write_SwitchStatus_combined_A3PosSwitchStatus(A3PosSwitchStatus_T data)
{
  return Rte_Write_SwitchStatus_combined_A3PosSwitchStatus(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* DoubleSwitch_HMICtrl */
      /* DoubleSwitch_HMICtrl */



