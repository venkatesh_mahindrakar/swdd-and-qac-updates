/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_IoHwAb_ASIL_Core.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit write services */
Std_ReturnType TSC_IoHwAb_ASIL_Core_Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T data);
Std_ReturnType TSC_IoHwAb_ASIL_Core_Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T data);

/** Explicit inter-runnable variables */
void TSC_IoHwAb_ASIL_Core_Rte_IrvRead_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated(PcbPopulatedInfo_T *data);
void TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data);
void TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data);
void TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated(PcbPopulatedInfo_T *data);
void TSC_IoHwAb_ASIL_Core_Rte_IrvRead_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data);
void TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_FSCMode(IOHWAB_UINT8);
void TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvBatteryFaultStatus(VGTT_EcuPinFaultStatus);
void TSC_IoHwAb_ASIL_Core_Rte_IrvRead_EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data);
VGTT_EcuPinFaultStatus TSC_IoHwAb_ASIL_Core_Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvBatteryFaultStatus(void);
void TSC_IoHwAb_ASIL_Core_Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data);

/** Calibration Component Calibration Parameters */
SEWS_PcbConfig_DoorAccessIf_X1CX3_T  TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(void);
SEWS_FSC_TimeoutThreshold_X1CZR_T  TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZR_FSC_TimeoutThreshold_v(void);
SEWS_PcbConfig_Adi_X1CXW_a_T * TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXW_PcbConfig_Adi_v(void);
SEWS_PcbConfig_DOBHS_X1CXX_a_T * TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(void);
SEWS_PcbConfig_DOWHS_X1CXY_a_T * TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(void);
SEWS_PcbConfig_DOWLS_X1CXZ_a_T * TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(void);
SEWS_PcbConfig_AdiPullUp_X1CX5_s_T * TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(void);
SEWS_FSC_VoltageThreshold_X1CZQ_s_T * TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZQ_FSC_VoltageThreshold_v(void);
SEWS_Diag_Act_DOWHS01_P1V6O_T  TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v(void);
SEWS_Diag_Act_DOWHS02_P1V6P_T  TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v(void);
SEWS_Diag_Act_DOWLS02_P1V7E_T  TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v(void);
SEWS_Diag_Act_DOWLS03_P1V7F_T  TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v(void);
SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T * TSC_IoHwAb_ASIL_Core_Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v(void);




