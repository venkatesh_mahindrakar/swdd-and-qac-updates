/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DiagnosticMonitor_Appl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Calibration Component Calibration Parameters */
SEWS_LIN_topology_P1AJR_T  TSC_DiagnosticMonitor_Appl_Rte_Prm_P1AJR_LIN_topology_v(void);
boolean  TSC_DiagnosticMonitor_Appl_Rte_Prm_P1DXX_FMSgateway_Act_v(void);
SEWS_KeyfobEncryptCode_P1DS4_a_T * TSC_DiagnosticMonitor_Appl_Rte_Prm_P1DS4_KeyfobEncryptCode_v(void);
boolean  TSC_DiagnosticMonitor_Appl_Rte_Prm_P1AW6_IsEcuAvailableDDM_v(void);
boolean  TSC_DiagnosticMonitor_Appl_Rte_Prm_P1AWY_IsEcuAvailableAlarm_v(void);
boolean  TSC_DiagnosticMonitor_Appl_Rte_Prm_P1AXE_IsEcuAvailablePDM_v(void);
boolean  TSC_DiagnosticMonitor_Appl_Rte_Prm_P1TTA_GearBoxLockActivation_v(void);
SEWS_CrankingLockActivation_P1DS3_T  TSC_DiagnosticMonitor_Appl_Rte_Prm_P1DS3_CrankingLockActivation_v(void);
boolean  TSC_DiagnosticMonitor_Appl_Rte_Prm_P1C54_FactoryModeActive_v(void);
SEWS_ComCryptoKey_P1DLX_a_T * TSC_DiagnosticMonitor_Appl_Rte_Prm_P1DLX_ComCryptoKey_v(void);




