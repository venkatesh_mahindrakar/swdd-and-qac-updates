/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SpeedControlFreeWheel_LINMasCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_ACCOrCCIndication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_ASLIndication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_DiagInfoCCFW_DiagInfo(DiagInfo_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LIN_FCWPushButton_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LIN_LKSPushButton_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LIN_SpeedControlModeButtonStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LIN_SpeedControlModeWheelStat_FreeWheel_Status(FreeWheel_Status_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_ResponseErrorCCFW_ResponseErrorCCFW(ResponseErrorCCFW *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_FCWPushButton_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LIN_ACCOrCCIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LIN_ASLIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LIN_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LIN_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LKSPushButton_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_SpeedControlModeButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_SpeedControlModeWheelStatus_FreeWheel_Status(FreeWheel_Status_T data);

/** Service interfaces */
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_ResetEventStatus(void);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_16_CCFW_VBT_ResetEventStatus(void);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_16_CCFW_VBT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_17_CCFW_VAT_ResetEventStatus(void);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_17_CCFW_VAT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_44_CCFW_RAM_ResetEventStatus(void);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_44_CCFW_RAM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_45_CCFW_FLASH_ResetEventStatus(void);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_45_CCFW_FLASH_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_46_CCFW_EEPROM_ResetEventStatus(void);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_46_CCFW_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_ResetEventStatus(void);
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus);

/** Explicit inter-runnable variables */
void TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState_Irv_IOCTL_CCFWLinCtrl(uint8);
uint8 TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvRead_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_CCFWLinCtrl(void);
void TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_CCFWLinCtrl(uint8);
void TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_CCFWLinCtrl(uint8);
uint8 TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvRead_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl(void);
void TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvWrite_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl(uint8);

/** Calibration Component Calibration Parameters */
boolean  TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Prm_P1B2C_CCFW_Installed_v(void);




