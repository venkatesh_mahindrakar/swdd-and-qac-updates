/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_WiredControlBox_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_WiredControlBox_HMICtrl.h"
#include "TSC_WiredControlBox_HMICtrl.h"








Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_AdjustButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_AdjustButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_BackButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BackButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs(FalseTrue_T *data)
{
  return Rte_Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(FalseTrue_T *data)
{
  return Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_LevelControlInformation_LevelControlInformation(LevelControlInformation_T *data)
{
  return Rte_Read_LevelControlInformation_LevelControlInformation(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_MemButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_MemButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_RampLevelRequest_RampLevelRequest(RampLevelRequest_T *data)
{
  return Rte_Read_RampLevelRequest_RampLevelRequest(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_SelectButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_SelectButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_StopButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_StopButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Living_Living(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_WRDownButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
{
  return Rte_Read_WRDownButtonStatus_EvalButtonRequest(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Read_WRUpButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
{
  return Rte_Read_WRUpButtonStatus_EvalButtonRequest(data);
}




Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_Adjust_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_Adjust_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_Down_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_Down_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_ECSStandByReqRCECS_ECSStandByReqRCECS(ECSStandByReq_T data)
{
  return Rte_Write_ECSStandByReqRCECS_ECSStandByReqRCECS(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_M1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_M1_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_M2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_M2_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_M3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_M3_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_ShortPulseMaxLength_ShortPulseMaxLength(ShortPulseMaxLength_T data)
{
  return Rte_Write_ShortPulseMaxLength_ShortPulseMaxLength(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_Up_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_Up_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T data)
{
  return Rte_Write_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T data)
{
  return Rte_Write_WiredLevelAdjustmentAction_LevelAdjustmentAction(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T data)
{
  return Rte_Write_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T data)
{
  return Rte_Write_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelUserMemory_WiredLevelUserMemory(WiredLevelUserMemory_T data)
{
  return Rte_Write_WiredLevelUserMemory_WiredLevelUserMemory(data);
}

Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T data)
{
  return Rte_Write_WiredLevelUserMemoryAction_LevelUserMemoryAction(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_WiredControlBox_HMICtrl_Rte_Call_Event_D1BOI_63_RCECS_ButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BOI_63_RCECS_ButtonStuck_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_ECSStopButtonHoldTimeout_P1DWI_T  TSC_WiredControlBox_HMICtrl_Rte_Prm_P1DWI_ECSStopButtonHoldTimeout_v(void)
{
  return (SEWS_ECSStopButtonHoldTimeout_P1DWI_T ) Rte_Prm_P1DWI_ECSStopButtonHoldTimeout_v();
}
SEWS_RCECSButtonStucked_P1DWJ_T  TSC_WiredControlBox_HMICtrl_Rte_Prm_P1DWJ_RCECSButtonStucked_v(void)
{
  return (SEWS_RCECSButtonStucked_P1DWJ_T ) Rte_Prm_P1DWJ_RCECSButtonStucked_v();
}
SEWS_RCECSUpDownStucked_P1DWK_T  TSC_WiredControlBox_HMICtrl_Rte_Prm_P1DWK_RCECSUpDownStucked_v(void)
{
  return (SEWS_RCECSUpDownStucked_P1DWK_T ) Rte_Prm_P1DWK_RCECSUpDownStucked_v();
}
SEWS_RCECS_HoldCircuitTimer_P1IUS_T  TSC_WiredControlBox_HMICtrl_Rte_Prm_P1IUS_RCECS_HoldCircuitTimer_v(void)
{
  return (SEWS_RCECS_HoldCircuitTimer_P1IUS_T ) Rte_Prm_P1IUS_RCECS_HoldCircuitTimer_v();
}
SEWS_ECS_MemSwTimings_P1BWF_s_T * TSC_WiredControlBox_HMICtrl_Rte_Prm_P1BWF_ECS_MemSwTimings_v(void)
{
  return (SEWS_ECS_MemSwTimings_P1BWF_s_T *) Rte_Prm_P1BWF_ECS_MemSwTimings_v();
}
SEWS_ForcedPositionStatus_Front_P1JSY_s_T * TSC_WiredControlBox_HMICtrl_Rte_Prm_P1JSY_ForcedPositionStatus_Front_v(void)
{
  return (SEWS_ForcedPositionStatus_Front_P1JSY_s_T *) Rte_Prm_P1JSY_ForcedPositionStatus_Front_v();
}
SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T * TSC_WiredControlBox_HMICtrl_Rte_Prm_P1JSZ_ForcedPositionStatus_Rear_v(void)
{
  return (SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T *) Rte_Prm_P1JSZ_ForcedPositionStatus_Rear_v();
}
boolean  TSC_WiredControlBox_HMICtrl_Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void)
{
  return (boolean ) Rte_Prm_P1ALT_ECS_PartialAirSystem_v();
}
boolean  TSC_WiredControlBox_HMICtrl_Rte_Prm_P1ALU_ECS_FullAirSystem_v(void)
{
  return (boolean ) Rte_Prm_P1ALU_ECS_FullAirSystem_v();
}


     /* WiredControlBox_HMICtrl */
      /* WiredControlBox_HMICtrl */



