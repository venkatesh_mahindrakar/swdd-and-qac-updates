/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_EconomyPower_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_EconomyPower_HMICtrl.h"
#include "TSC_EconomyPower_HMICtrl.h"








Std_ReturnType TSC_EconomyPower_HMICtrl_Rte_Read_DrivingMode_DrivingMode(uint8 *data)
{
  return Rte_Read_DrivingMode_DrivingMode(data);
}

Std_ReturnType TSC_EconomyPower_HMICtrl_Rte_Read_EconomyPowerSwitch_status_EconomyPowerSwitch_status(PushButtonStatus_T *data)
{
  return Rte_Read_EconomyPowerSwitch_status_EconomyPowerSwitch_status(data);
}

Std_ReturnType TSC_EconomyPower_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}

Std_ReturnType TSC_EconomyPower_HMICtrl_Rte_Read_TransmissionDrivingMode_TransmissionDrivingMode(TransmissionDrivingMode_T *data)
{
  return Rte_Read_TransmissionDrivingMode_TransmissionDrivingMode(data);
}




Std_ReturnType TSC_EconomyPower_HMICtrl_Rte_Write_EcoBalancedSwitch_stat_EcoBalancedSwitch_stat(OffOn_T data)
{
  return Rte_Write_EcoBalancedSwitch_stat_EcoBalancedSwitch_stat(data);
}

Std_ReturnType TSC_EconomyPower_HMICtrl_Rte_Write_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd(DeviceIndication_T data)
{
  return Rte_Write_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





boolean  TSC_EconomyPower_HMICtrl_Rte_Prm_P1VQ0_FuelEcoOffButton_v(void)
{
  return (boolean ) Rte_Prm_P1VQ0_FuelEcoOffButton_v();
}


     /* EconomyPower_HMICtrl */
      /* EconomyPower_HMICtrl */



