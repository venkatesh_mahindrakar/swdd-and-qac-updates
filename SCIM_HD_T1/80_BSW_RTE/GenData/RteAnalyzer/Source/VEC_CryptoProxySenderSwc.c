/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VEC_CryptoProxySenderSwc.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  VEC_CryptoProxySenderSwc
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VEC_CryptoProxySenderSwc>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * SEWS_ComCryptoKey_P1DLX_T
 *   
 *
 * SymDecryptDataBuffer
 *   
 *
 * SymDecryptResultBuffer
 *   
 *
 * SymEncryptDataBuffer
 *   
 *
 * SymEncryptResultBuffer
 *   
 *
 * SymKeyType
 *   
 *
 * UInt16
 *   
 *
 * UInt32
 *   
 *
 *
 * Operation Prototypes:
 * =====================
 * SymDecryptFinish of Port Interface CsmSymDecrypt
 *   
 *
 * SymDecryptStart of Port Interface CsmSymDecrypt
 *   
 *
 * SymDecryptUpdate of Port Interface CsmSymDecrypt
 *   
 *
 * SymEncryptFinish of Port Interface CsmSymEncrypt
 *   
 *
 * SymEncryptStart of Port Interface CsmSymEncrypt
 *   
 *
 * SymEncryptUpdate of Port Interface CsmSymEncrypt
 *   
 *
 *********************************************************************************************************************/

#include "Rte_VEC_CryptoProxySenderSwc.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_VEC_CryptoProxySenderSwc.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * UInt16: Integer in interval [0...65535]
 * UInt32: Integer in interval [0...4294967295]
 * UInt32_Length: Integer in interval [0...4294967295]
 * UInt8: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Array Types:
 * ============
 * CryptoIdKey_T: Array with 16 element(s) of type uint8
 * Encrypted128bit_T: Array with 16 element(s) of type uint8
 * Rte_DT_SymKeyType_1: Array with 256 element(s) of type UInt8
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SymDecryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymDecryptResultBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptDataBuffer: Array with 128 element(s) of type UInt8
 * SymEncryptResultBuffer: Array with 128 element(s) of type UInt8
 * VEC_CryptoProxy_UserSignal: Array with 12 element(s) of type UInt8
 *
 * Record Types:
 * =============
 * SymKeyType: Record with elements
 *   length of type UInt32_Length
 *   data of type Rte_DT_SymKeyType_1
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Per-Instance Memory:
 * ====================
 *   UInt16 *Rte_Pim_VEC_CryptoProxy_CycleTimer(Rte_Instance self)
 *
 * Calibration Parameters:
 * =======================
 *   SW-C local Calibration Parameters:
 *   ----------------------------------
 *   UInt16 Rte_CData_VEC_CryptoProxyCycleFactor(Rte_Instance self)
 *   UInt16 Rte_CData_VEC_CryptoProxyCycleOffset(Rte_Instance self)
 *
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ComCryptoKey_P1DLX_T *Rte_Prm_ComCryptoKey_P1DLX_v(Rte_Instance self)
 *     Returnvalue: SEWS_ComCryptoKey_P1DLX_T* is of type SEWS_ComCryptoKey_P1DLX_a_T
 *
 *********************************************************************************************************************/


#define VEC_CryptoProxySenderSwc_START_SEC_CODE
#include "VEC_CryptoProxySenderSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderConfirmation
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataSendCompletedEvent for DataElementPrototype <EncryptedSignal> of PortPrototype <VEC_EncryptedSignal>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderConfirmation_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderConfirmation(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderConfirmation
 *********************************************************************************************************************/

  UInt16 PimVEC_CryptoProxy_CycleTimer;

  UInt16 VEC_CryptoProxyCycleFactor_data;
  UInt16 VEC_CryptoProxyCycleOffset_data;

  SEWS_ComCryptoKey_P1DLX_a_T ComCryptoKey_P1DLX_v_data;

  UInt32 VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimVEC_CryptoProxy_CycleTimer = *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(self);
  *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(self) = PimVEC_CryptoProxy_CycleTimer;

  VEC_CryptoProxyCycleFactor_data = TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(self);
  VEC_CryptoProxyCycleOffset_data = TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(self);

  (void)memcpy(ComCryptoKey_P1DLX_v_data, TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(self), sizeof(SEWS_ComCryptoKey_P1DLX_a_T));

  VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber = TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(self);

  TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(self, 4294967295U);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderMainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(Rte_Instance self, uint8 *data)
 *     Argument data: uint8* is of type CryptoIdKey_T
 *   Std_ReturnType Rte_Read_CryptoTrigger_CryptoTrigger(Rte_Instance self, Boolean *data)
 *   Std_ReturnType Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(Rte_Instance self, UInt8 *data)
 *     Argument data: UInt8* is of type VEC_CryptoProxy_UserSignal
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Send_VEC_EncryptedSignal_EncryptedSignal(Rte_Instance self, const uint8 *data)
 *     Argument data: uint8* is of type Encrypted128bit_T
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptFinish(Rte_Instance self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type SymEncryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK, RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptStart(Rte_Instance self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
 *     Argument InitVectorBuffer: UInt8* is of type SymEncryptDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSymEncrypt_SymEncryptUpdate(Rte_Instance self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
 *     Argument plainTextBuffer: UInt8* is of type SymEncryptDataBuffer
 *     Argument cipherTextBuffer: UInt8* is of type SymEncryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymEncrypt_CSM_E_BUSY, RTE_E_CsmSymEncrypt_CSM_E_NOT_OK, RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *
 * Status Interfaces:
 * ==================
 *   Tx Acknowledge:
 *   ----------------
 *   Std_ReturnType Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(Rte_Instance self)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderMainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderMainFunction(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderMainFunction
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  CryptoIdKey_T Receive_VEC_CryptoIdKey_CryptoIdKey;
  Boolean Read_CryptoTrigger_CryptoTrigger;
  VEC_CryptoProxy_UserSignal Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized;

  UInt16 PimVEC_CryptoProxy_CycleTimer;

  UInt16 VEC_CryptoProxyCycleFactor_data;
  UInt16 VEC_CryptoProxyCycleOffset_data;

  SEWS_ComCryptoKey_P1DLX_a_T ComCryptoKey_P1DLX_v_data;

  Encrypted128bit_T Send_VEC_EncryptedSignal_EncryptedSignal = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  UInt32 VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber;

  SymEncryptResultBuffer Call_CsmSymEncrypt_SymEncryptFinish_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32_Length Call_CsmSymEncrypt_SymEncryptFinish_cipherTextLength = 0U;
  SymKeyType Call_CsmSymEncrypt_SymEncryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  SymEncryptDataBuffer Call_CsmSymEncrypt_SymEncryptStart_InitVectorBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  SymEncryptDataBuffer Call_CsmSymEncrypt_SymEncryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  SymEncryptResultBuffer Call_CsmSymEncrypt_SymEncryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32_Length Call_CsmSymEncrypt_SymEncryptUpdate_cipherTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimVEC_CryptoProxy_CycleTimer = *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(self);
  *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(self) = PimVEC_CryptoProxy_CycleTimer;

  VEC_CryptoProxyCycleFactor_data = TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(self);
  VEC_CryptoProxyCycleOffset_data = TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(self);

  (void)memcpy(ComCryptoKey_P1DLX_v_data, TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(self), sizeof(SEWS_ComCryptoKey_P1DLX_a_T));

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(self, Receive_VEC_CryptoIdKey_CryptoIdKey);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Read_CryptoTrigger_CryptoTrigger(self, &Read_CryptoTrigger_CryptoTrigger);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(self, Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Send_VEC_EncryptedSignal_EncryptedSignal(self, Send_VEC_EncryptedSignal_EncryptedSignal);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber = TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(self);

  TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(self, 4294967295U);

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(self, Call_CsmSymEncrypt_SymEncryptFinish_cipherTextBuffer, &Call_CsmSymEncrypt_SymEncryptFinish_cipherTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(self, &Call_CsmSymEncrypt_SymEncryptStart_key, Call_CsmSymEncrypt_SymEncryptStart_InitVectorBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(self, Call_CsmSymEncrypt_SymEncryptUpdate_plainTextBuffer, 0U, Call_CsmSymEncrypt_SymEncryptUpdate_cipherTextBuffer, &Call_CsmSymEncrypt_SymEncryptUpdate_cipherTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(self);
  switch (fct_status)
  {
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TRANSMIT_ACK:
      fct_error = 1;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderReception
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <CryptoIdKey> of PortPrototype <VEC_CryptoIdKey>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(Rte_Instance self, uint8 *data)
 *     Argument data: uint8* is of type CryptoIdKey_T
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   UInt32 Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(Rte_Instance self)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(Rte_Instance self, UInt32 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptFinish(Rte_Instance self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type SymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK, RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptStart(Rte_Instance self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
 *     Argument InitVectorBuffer: UInt8* is of type SymDecryptDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmSymDecrypt_SymDecryptUpdate(Rte_Instance self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type SymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type SymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmSymDecrypt_CSM_E_BUSY, RTE_E_CsmSymDecrypt_CSM_E_NOT_OK, RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderReception_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderReception(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderReception
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  CryptoIdKey_T Receive_VEC_CryptoIdKey_CryptoIdKey;

  UInt16 PimVEC_CryptoProxy_CycleTimer;

  UInt16 VEC_CryptoProxyCycleFactor_data;
  UInt16 VEC_CryptoProxyCycleOffset_data;

  SEWS_ComCryptoKey_P1DLX_a_T ComCryptoKey_P1DLX_v_data;

  UInt32 VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber;

  SymDecryptResultBuffer Call_CsmSymDecrypt_SymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32_Length Call_CsmSymDecrypt_SymDecryptFinish_plainTextLength = 0U;
  SymKeyType Call_CsmSymDecrypt_SymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  SymDecryptDataBuffer Call_CsmSymDecrypt_SymDecryptStart_InitVectorBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  SymDecryptDataBuffer Call_CsmSymDecrypt_SymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  SymDecryptResultBuffer Call_CsmSymDecrypt_SymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32_Length Call_CsmSymDecrypt_SymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimVEC_CryptoProxy_CycleTimer = *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(self);
  *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(self) = PimVEC_CryptoProxy_CycleTimer;

  VEC_CryptoProxyCycleFactor_data = TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(self);
  VEC_CryptoProxyCycleOffset_data = TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(self);

  (void)memcpy(ComCryptoKey_P1DLX_v_data, TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(self), sizeof(SEWS_ComCryptoKey_P1DLX_a_T));

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(self, Receive_VEC_CryptoIdKey_CryptoIdKey);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber = TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(self);

  TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(self, 4294967295U);

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(self, Call_CsmSymDecrypt_SymDecryptFinish_plainTextBuffer, &Call_CsmSymDecrypt_SymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(self, &Call_CsmSymDecrypt_SymDecryptStart_key, Call_CsmSymDecrypt_SymDecryptStart_InitVectorBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(self, Call_CsmSymDecrypt_SymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmSymDecrypt_SymDecryptUpdate_plainTextBuffer, &Call_CsmSymDecrypt_SymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_CryptoProxySenderSwc_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderSwc_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_CryptoProxySenderSwc_CODE) VEC_CryptoProxySenderSwc_Init(Rte_Instance self) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_CryptoProxySenderSwc_Init
 *********************************************************************************************************************/

  UInt16 PimVEC_CryptoProxy_CycleTimer;

  UInt16 VEC_CryptoProxyCycleFactor_data;
  UInt16 VEC_CryptoProxyCycleOffset_data;

  SEWS_ComCryptoKey_P1DLX_a_T ComCryptoKey_P1DLX_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  PimVEC_CryptoProxy_CycleTimer = *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(self);
  *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(self) = PimVEC_CryptoProxy_CycleTimer;

  VEC_CryptoProxyCycleFactor_data = TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(self);
  VEC_CryptoProxyCycleOffset_data = TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(self);

  (void)memcpy(ComCryptoKey_P1DLX_v_data, TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(self), sizeof(SEWS_ComCryptoKey_P1DLX_a_T));


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VEC_CryptoProxySenderSwc_STOP_SEC_CODE
#include "VEC_CryptoProxySenderSwc_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
