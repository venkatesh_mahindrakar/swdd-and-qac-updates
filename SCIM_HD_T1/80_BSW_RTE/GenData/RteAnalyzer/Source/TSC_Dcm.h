/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Dcm.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Service interfaces */
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_CHANO_Data_CHANO_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1AFR_Data_P1AFR_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1AFS_Data_P1AFS_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1AFT_Data_P1AFT_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1ALA_Data_P1ALA_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1ALA_Data_P1ALA_ReadDataLength(Dcm_OpStatusType OpStatus, uint16 *DataLength);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1ALB_Data_P1ALB_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1ALP_Data_P1ALP_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1ALP_Data_P1ALP_ReadDataLength(uint16 *DataLength);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1ALQ_Data_P1ALQ_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1ALQ_Data_P1ALQ_ReadDataLength(uint16 *DataLength);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1B0T_Data_P1B0T_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1B1O_Data_P1B1O_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BW0_Data_P1BW0_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BW1_Data_P1BW1_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BW3_Data_P1BW3_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BW4_Data_P1BW4_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BW5_Data_P1BW5_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BW6_Data_P1BW6_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BW8_Data_P1BW8_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BW9_Data_P1BW9_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWI_Data_P1BWI_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWJ_Data_P1BWJ_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWL_Data_P1BWL_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWM_Data_P1BWM_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWN_Data_P1BWN_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWO_Data_P1BWO_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWQ_Data_P1BWQ_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWR_Data_P1BWR_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWV_Data_P1BWV_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BWX_Data_P1BWX_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BXD_Data_P1BXD_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1BXF_Data_P1BXF_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1C1R_Data_P1C1R_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1CXF_Data_P1CXF_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1DCT_Data_P1DCT_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1DCU_Data_P1DCU_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1DIH_Data_P1DIH_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1DJ8_Data_P1DJ8_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1DS3_Data_P1DS3_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1DVZ_Data_P1DVZ_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EIJ_Data_P1EIJ_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EIJ_Data_P1EIJ_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EIJ_Data_P1EIJ_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EIJ_Data_P1EIJ_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOQ_Data_P1EOQ_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOQ_Data_P1EOQ_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOQ_Data_P1EOQ_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOQ_Data_P1EOQ_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOR_Data_P1EOR_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOR_Data_P1EOR_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOR_Data_P1EOR_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOR_Data_P1EOR_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOS_Data_P1EOS_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOS_Data_P1EOS_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOS_Data_P1EOS_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOS_Data_P1EOS_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOT_Data_P1EOT_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOT_Data_P1EOT_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOT_Data_P1EOT_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOT_Data_P1EOT_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOU_Data_P1EOU_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOU_Data_P1EOU_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOU_Data_P1EOU_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOU_Data_P1EOU_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOV_Data_P1EOV_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOV_Data_P1EOV_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOV_Data_P1EOV_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOV_Data_P1EOV_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOW_Data_P1EOW_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOW_Data_P1EOW_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOW_Data_P1EOW_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1EOW_Data_P1EOW_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1FDL_Data_P1FDL_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1FM6_Data_P1FM6_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1GCM_Data_P1GCM_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1ILR_Data_P1ILR_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1KAO_Data_P1KAO_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1KAO_Data_P1KAO_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1OLT_Data_P1OLT_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1Q82_Data_P1Q82_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1QXI_Data_P1QXI_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1QXJ_Data_P1QXJ_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1QXM_Data_P1QXM_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1QXP_Data_P1QXP_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1QXR_Data_P1QXR_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1QXU_Data_P1QXU_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1RG1_Data_P1RG1_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VKH_Data_P1VKH_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VKK_Data_P1VKK_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VQ1_Data_P1VQ1_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VQ2_Data_P1VQ2_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VQ3_Data_P1VQ3_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VQ4_Data_P1VQ4_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VQ5_Data_P1VQ5_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VQ6_Data_P1VQ6_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VQ7_Data_P1VQ7_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VQ8_Data_P1VQ8_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VQ9_Data_P1VQ9_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR0_Data_P1VR0_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR0_Data_P1VR0_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR0_Data_P1VR0_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR0_Data_P1VR0_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR4_Data_P1VR4_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR4_Data_P1VR4_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR4_Data_P1VR4_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR4_Data_P1VR4_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR6_Data_P1VR6_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR6_Data_P1VR6_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR6_Data_P1VR6_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR6_Data_P1VR6_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR7_Data_P1VR7_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR7_Data_P1VR7_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR7_Data_P1VR7_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR7_Data_P1VR7_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR8_Data_P1VR8_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR8_Data_P1VR8_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR8_Data_P1VR8_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR8_Data_P1VR8_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR9_Data_P1VR9_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR9_Data_P1VR9_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR9_Data_P1VR9_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VR9_Data_P1VR9_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRA_Data_P1VRA_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRB_Data_P1VRB_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRC_Data_P1VRC_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRD_Data_P1VRD_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRE_Data_P1VRE_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRF_Data_P1VRF_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRG_Data_P1VRG_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRH_Data_P1VRH_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRI_Data_P1VRI_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRJ_Data_P1VRJ_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRK_Data_P1VRK_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRL_Data_P1VRL_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRM_Data_P1VRM_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRN_Data_P1VRN_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRO_Data_P1VRO_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRS_Data_P1VRS_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRT_Data_P1VRT_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRU_Data_P1VRU_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRV_Data_P1VRV_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRW_Data_P1VRW_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRZ_Data_P1VRZ_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRZ_Data_P1VRZ_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRZ_Data_P1VRZ_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VRZ_Data_P1VRZ_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSB_Data_P1VSB_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSB_Data_P1VSB_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSB_Data_P1VSB_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSB_Data_P1VSB_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSC_Data_P1VSC_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSC_Data_P1VSC_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSC_Data_P1VSC_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSC_Data_P1VSC_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSD_Data_P1VSD_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSD_Data_P1VSD_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSD_Data_P1VSD_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSD_Data_P1VSD_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSG_Data_P1VSG_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSG_Data_P1VSG_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSG_Data_P1VSG_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_P1VSG_Data_P1VSG_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_VINNO_Data_VINNO_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1C12_Data_X1C12_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1C12_Data_X1C12_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1C13_Data_X1C13_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1C13_Data_X1C13_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1C1U_Data_X1C1U_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1C1U_Data_X1C1U_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1C1Z_Data_X1C1Z_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1CV5_Data_X1CV5_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1CV7_Data_X1CV7_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1CV7_Data_X1CV7_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1CV7_Data_X1CV7_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1CV7_Data_X1CV7_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1CY4_Data_X1CY4_ReadData(uint8 *Data);
Std_ReturnType TSC_Dcm_Rte_Call_DataServices_X1CY4_Data_X1CY4_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_R1AAA_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_R1AAA_Start(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_R1AAA_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_R1AAI_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_R1AAI_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_R1AAI_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_R1AAJ_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_R1AAJ_Start(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_R1AAJ_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_Y1ABD_Start(const uint8 *In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_RoutineServices_Y1ABE_Start(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_01_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_01_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_07_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_07_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_09_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_09_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_0B_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_0B_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_0D_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_0D_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_0F_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_0F_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_11_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_11_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_15_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_15_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_17_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_17_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_1B_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_1B_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_29_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_29_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_2B_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_2B_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_2D_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_2D_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_2F_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_2F_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_31_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_31_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_33_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_33_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_37_CompareKey(const uint8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_SecurityAccess_SA_Seed_37_GetSeed(Dcm_OpStatusType OpStatus, uint8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode);
Std_ReturnType TSC_Dcm_Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation(uint8 SID, uint8 ReqType, uint16 SourceAddress, Dcm_ConfirmationStatusType ConfirmationStatus);
Std_ReturnType TSC_Dcm_Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication(uint8 SID, const uint8 *RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, Dcm_NegativeResponseCodeType *ErrorCode);

/** Mode switches */
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54(Dcm_CommunicationModeType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmControlDtcSetting_DcmControlDtcSetting(Dcm_ControlDtcSettingType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(Dcm_DiagnosticSessionControlType mode);
Std_ReturnType TSC_Dcm_Rte_Switch_DcmEcuReset_DcmEcuReset(Dcm_EcuResetType mode);

/** Mode switch acknowledgement */
Std_ReturnType TSC_Dcm_Rte_SwitchAck_DcmEcuReset_DcmEcuReset(void);




