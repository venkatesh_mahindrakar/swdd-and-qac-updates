/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  PassengersSeatBelt_hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  PassengersSeatBelt_hdlr
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <PassengersSeatBelt_hdlr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   
 *
 * IOHWAB_UINT8
 *   
 *
 * SEWS_PassengersSeatBeltInstalled_P1VQB_T
 *   
 *
 * SEWS_PassengersSeatBeltSensorType_P1VYK_T
 *   
 *
 * SEWS_X1CY1_DigitalBiLevelHigh_T
 *   
 *
 * SEWS_X1CY1_DigitalBiLevelLow_T
 *   
 *
 * SEWS_X1CY2_FastenedHigh_STB_T
 *   
 *
 * SEWS_X1CY2_FastenedLow_T
 *   
 *
 * SEWS_X1CY2_UnfastnedAndNotSeatHigh_T
 *   
 *
 * SEWS_X1CY2_UnfastnedAndNotSeatLow_T
 *   
 *
 * SEWS_X1CY2_UnfastnedAndSeatHigh_T
 *   
 *
 * SEWS_X1CY2_UnfastnedAndSeatLow_T
 *   
 *
 * VGTT_EcuPinFaultStatus
 *   
 *
 * VGTT_EcuPinVoltage_0V2
 *   
 *
 *********************************************************************************************************************/

#include "Rte_PassengersSeatBelt_hdlr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_PassengersSeatBelt_hdlr.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void PassengersSeatBelt_hdlr_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_PassengersSeatBeltInstalled_P1VQB_T: Integer in interval [0...255]
 * SEWS_PassengersSeatBeltSensorType_P1VYK_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * SEWS_X1CY2_FastenedHigh_STB_T: Integer in interval [0...255]
 * SEWS_X1CY2_FastenedLow_T: Integer in interval [0...255]
 * SEWS_X1CY2_UnfastnedAndNotSeatHigh_T: Integer in interval [0...255]
 * SEWS_X1CY2_UnfastnedAndNotSeatLow_T: Integer in interval [0...255]
 * SEWS_X1CY2_UnfastnedAndSeatHigh_T: Integer in interval [0...255]
 * SEWS_X1CY2_UnfastnedAndSeatLow_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * PassengersSeatBelt_T: Enumeration of integer in interval [0...7] with enumerators
 *   PassengersSeatBelt_AtLeastOnePassengerSeatedNotFastened (0U)
 *   PassengersSeatBelt_PassengersFastened (1U)
 *   PassengersSeatBelt_PassengersNotSeated (2U)
 *   PassengersSeatBelt_Spare1 (3U)
 *   PassengersSeatBelt_Spare2 (4U)
 *   PassengersSeatBelt_Spare3 (5U)
 *   PassengersSeatBelt_Error (6U)
 *   PassengersSeatBelt_NotAvailable (7U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Record Types:
 * =============
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 * SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T: Record with elements
 *   Fastened of type SEWS_X1CY2_Fastened_s_T
 *   UnfastnedAndNotSeat of type SEWS_X1CY2_UnfastnedAndNotSeat_s_T
 *   UnfastnedAndSeat of type SEWS_X1CY2_UnfastnedAndSeat_s_T
 * SEWS_X1CY2_Fastened_s_T: Record with elements
 *   FastenedHigh_STB of type SEWS_X1CY2_FastenedHigh_STB_T
 *   FastenedLow of type SEWS_X1CY2_FastenedLow_T
 * SEWS_X1CY2_UnfastnedAndNotSeat_s_T: Record with elements
 *   UnfastnedAndNotSeatHigh of type SEWS_X1CY2_UnfastnedAndNotSeatHigh_T
 *   UnfastnedAndNotSeatLow of type SEWS_X1CY2_UnfastnedAndNotSeatLow_T
 * SEWS_X1CY2_UnfastnedAndSeat_s_T: Record with elements
 *   UnfastnedAndSeatHigh of type SEWS_X1CY2_UnfastnedAndSeatHigh_T
 *   UnfastnedAndSeatLow of type SEWS_X1CY2_UnfastnedAndSeatLow_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
 *   SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T *Rte_Prm_X1CY2_PassengersSeatBeltVoltageLevels_v(void)
 *   SEWS_PassengersSeatBeltInstalled_P1VQB_T Rte_Prm_P1VQB_PassengersSeatBeltInstalled_v(void)
 *   SEWS_PassengersSeatBeltSensorType_P1VYK_T Rte_Prm_P1VYK_PassengersSeatBeltSensorType_v(void)
 *
 *********************************************************************************************************************/


#define PassengersSeatBelt_hdlr_START_SEC_CODE
#include "PassengersSeatBelt_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PassengersSeatBelt_hdlr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PassengersSeatBelt_PassengersSeatBelt(PassengersSeatBelt_T data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1F0O_1E_ResistOutOfRange_SecondPsgrSeat_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FZ9_1E_ResistOutOfRange_FirstPsgrSeat_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PassengersSeatBelt_hdlr_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PassengersSeatBelt_hdlr_CODE) PassengersSeatBelt_hdlr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PassengersSeatBelt_hdlr_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;

  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;
  SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T X1CY2_PassengersSeatBeltVoltageLevels_v_data;

  SEWS_PassengersSeatBeltInstalled_P1VQB_T P1VQB_PassengersSeatBeltInstalled_v_data;
  SEWS_PassengersSeatBeltSensorType_P1VYK_T P1VYK_PassengersSeatBeltSensorType_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_PassengersSeatBelt_hdlr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
  X1CY2_PassengersSeatBeltVoltageLevels_v_data = *TSC_PassengersSeatBelt_hdlr_Rte_Prm_X1CY2_PassengersSeatBeltVoltageLevels_v();

  P1VQB_PassengersSeatBeltInstalled_v_data = TSC_PassengersSeatBelt_hdlr_Rte_Prm_P1VQB_PassengersSeatBeltInstalled_v();
  P1VYK_PassengersSeatBeltSensorType_v_data = TSC_PassengersSeatBelt_hdlr_Rte_Prm_P1VYK_PassengersSeatBeltSensorType_v();

  fct_status = TSC_PassengersSeatBelt_hdlr_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PassengersSeatBelt_hdlr_Rte_Write_PassengersSeatBelt_PassengersSeatBelt(Rte_InitValue_PassengersSeatBelt_PassengersSeatBelt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_PassengersSeatBelt_hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PassengersSeatBelt_hdlr_Rte_Call_Event_D1F0O_1E_ResistOutOfRange_SecondPsgrSeat_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PassengersSeatBelt_hdlr_Rte_Call_Event_D1FZ9_1E_ResistOutOfRange_FirstPsgrSeat_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  PassengersSeatBelt_hdlr_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define PassengersSeatBelt_hdlr_STOP_SEC_CODE
#include "PassengersSeatBelt_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void PassengersSeatBelt_hdlr_TestDefines(void)
{
  /* Enumeration Data Types */

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  PassengersSeatBelt_T Test_PassengersSeatBelt_T_V_1 = PassengersSeatBelt_AtLeastOnePassengerSeatedNotFastened;
  PassengersSeatBelt_T Test_PassengersSeatBelt_T_V_2 = PassengersSeatBelt_PassengersFastened;
  PassengersSeatBelt_T Test_PassengersSeatBelt_T_V_3 = PassengersSeatBelt_PassengersNotSeated;
  PassengersSeatBelt_T Test_PassengersSeatBelt_T_V_4 = PassengersSeatBelt_Spare1;
  PassengersSeatBelt_T Test_PassengersSeatBelt_T_V_5 = PassengersSeatBelt_Spare2;
  PassengersSeatBelt_T Test_PassengersSeatBelt_T_V_6 = PassengersSeatBelt_Spare3;
  PassengersSeatBelt_T Test_PassengersSeatBelt_T_V_7 = PassengersSeatBelt_Error;
  PassengersSeatBelt_T Test_PassengersSeatBelt_T_V_8 = PassengersSeatBelt_NotAvailable;

  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_1 = TestNotRun;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_2 = OffState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_3 = OffState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_4 = OffState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_5 = OffState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_6 = OffState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_7 = OffState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_8 = OnState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_9 = OnState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_10 = OnState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_11 = OnState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_12 = OnState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_13 = OnState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_14 = OnState_FaultDetected_VOR;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_15 = OnState_FaultDetected_CAT;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
