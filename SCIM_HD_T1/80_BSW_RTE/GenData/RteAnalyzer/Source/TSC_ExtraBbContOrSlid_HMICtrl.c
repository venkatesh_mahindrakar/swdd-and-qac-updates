/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ExtraBbContOrSlid_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_ExtraBbContOrSlid_HMICtrl.h"
#include "TSC_ExtraBbContOrSlid_HMICtrl.h"








Std_ReturnType TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_ContUnlockSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_ContUnlockSwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus(InactiveActive_T *data)
{
  return Rte_Read_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus(data);
}

Std_ReturnType TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus(InactiveActive_T *data)
{
  return Rte_Read_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus(data);
}

Std_ReturnType TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_Slid5thWheelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_Slid5thWheelSwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}




Std_ReturnType TSC_ExtraBbContOrSlid_HMICtrl_Rte_Write_BBContainerUnlockRequest_BBContainerUnlockRequest(OffOn_T data)
{
  return Rte_Write_BBContainerUnlockRequest_BBContainerUnlockRequest(data);
}

Std_ReturnType TSC_ExtraBbContOrSlid_HMICtrl_Rte_Write_BBSlidable5thWheelRequest_BBSlidable5thWheelRequest(OffOn_T data)
{
  return Rte_Write_BBSlidable5thWheelRequest_BBSlidable5thWheelRequest(data);
}

Std_ReturnType TSC_ExtraBbContOrSlid_HMICtrl_Rte_Write_ContUnlock_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_ContUnlock_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_ExtraBbContOrSlid_HMICtrl_Rte_Write_Slid5thWheel_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_Slid5thWheel_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_ContainerUnlockHMIDeviceType_P1CXO_T  TSC_ExtraBbContOrSlid_HMICtrl_Rte_Prm_P1CXO_ContainerUnlockHMIDeviceType_v(void)
{
  return (SEWS_ContainerUnlockHMIDeviceType_P1CXO_T ) Rte_Prm_P1CXO_ContainerUnlockHMIDeviceType_v();
}
SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T  TSC_ExtraBbContOrSlid_HMICtrl_Rte_Prm_P1CXP_Slidable5thWheelHMIDeviceType_v(void)
{
  return (SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T ) Rte_Prm_P1CXP_Slidable5thWheelHMIDeviceType_v();
}
SEWS_Slid5thWheelTimeoutForReq_P1DV9_T  TSC_ExtraBbContOrSlid_HMICtrl_Rte_Prm_P1DV9_Slid5thWheelTimeoutForReq_v(void)
{
  return (SEWS_Slid5thWheelTimeoutForReq_P1DV9_T ) Rte_Prm_P1DV9_Slid5thWheelTimeoutForReq_v();
}


     /* ExtraBbContOrSlid_HMICtrl */
      /* ExtraBbContOrSlid_HMICtrl */



