/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_FlexibleSwitchesRouter_Ctrl_LINMstr.h"
#include "TSC_FlexibleSwitchesRouter_Ctrl_LINMstr.h"















     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BN8_44_FSP_MemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN8_44_FSP_MemoryFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(uint8 *data)
{
Rte_IrvRead_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD( data);
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(uint8 *data)
{
Rte_IrvRead_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure( data);
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl(void)
{
return Rte_IrvRead_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_FspLinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_FspLinCtrl( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_FspLinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_FspLinCtrl( data);
}





Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN1_ComMode_LIN(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN2_ComMode_LIN(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN3_ComMode_LIN(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN4_ComMode_LIN(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN5_ComMode_LIN(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
{
  return Rte_Read_DiagActiveState_isDiagActive(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL1_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP1DiagInfoL1_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP1DiagInfoL2_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL3_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP1DiagInfoL3_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL4_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP1DiagInfoL4_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL5_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP1DiagInfoL5_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP1IndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL1_FSP1ResponseErrorL1(FSP1ResponseErrorL1_T *data)
{
  return Rte_Read_FSP1ResponseErrorL1_FSP1ResponseErrorL1(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL2_FSP1ResponseErrorL2(FSP1ResponseErrorL2_T *data)
{
  return Rte_Read_FSP1ResponseErrorL2_FSP1ResponseErrorL2(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL3_FSP1ResponseErrorL3(FSP1ResponseErrorL3_T *data)
{
  return Rte_Read_FSP1ResponseErrorL3_FSP1ResponseErrorL3(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL4_FSP1ResponseErrorL4(FSP1ResponseErrorL4_T *data)
{
  return Rte_Read_FSP1ResponseErrorL4_FSP1ResponseErrorL4(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL5_FSP1ResponseErrorL5(FSP1ResponseErrorL5_T *data)
{
  return Rte_Read_FSP1ResponseErrorL5_FSP1ResponseErrorL5(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL1_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP1SwitchStatusL1_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP1SwitchStatusL2_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL3_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP1SwitchStatusL3_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL4_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP1SwitchStatusL4_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL5_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP1SwitchStatusL5_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2DiagInfoL1_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP2DiagInfoL1_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP2DiagInfoL2_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2DiagInfoL3_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP2DiagInfoL3_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP2IndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2ResponseErrorL1_FSP2ResponseErrorL1(FSP2ResponseErrorL1_T *data)
{
  return Rte_Read_FSP2ResponseErrorL1_FSP2ResponseErrorL1(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2ResponseErrorL2_FSP2ResponseErrorL2(FSP2ResponseErrorL2_T *data)
{
  return Rte_Read_FSP2ResponseErrorL2_FSP2ResponseErrorL2(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2ResponseErrorL3_FSP2ResponseErrorL3(FSP2ResponseErrorL3_T *data)
{
  return Rte_Read_FSP2ResponseErrorL3_FSP2ResponseErrorL3(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2SwitchStatusL1_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP2SwitchStatusL1_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP2SwitchStatusL2_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2SwitchStatusL3_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP2SwitchStatusL3_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP3DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP3DiagInfoL2_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP3IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP3IndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP3ResponseErrorL2_FSP3ResponseErrorL2(FSP3ResponseErrorL2_T *data)
{
  return Rte_Read_FSP3ResponseErrorL2_FSP3ResponseErrorL2(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP3SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP3SwitchStatusL2_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP4DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
{
  return Rte_Read_FSP4DiagInfoL2_FSPDiagInfo(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP4IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP4IndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP4ResponseErrorL2_FSP4ResponseErrorL2(FSP4ResponseErrorL2_T *data)
{
  return Rte_Read_FSP4ResponseErrorL2_FSP4ResponseErrorL2(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP4SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
{
  return Rte_Read_FSP4SwitchStatusL2_FSPSwitchStatus(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP5IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP5IndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP6IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP6IndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP7IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP7IndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP8IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP8IndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP9IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP9IndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP_BIndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
{
  return Rte_Read_FSP_BIndicationCmd_FSPIndicationCmdArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FspNV_PR_FspNV(uint8 *data)
{
  return Rte_Read_FspNV_PR_FspNV(data);
}




Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP1IndicationCmdL1_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP1IndicationCmdL2_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP1IndicationCmdL3_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL4_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP1IndicationCmdL4_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL5_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP1IndicationCmdL5_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP1SwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP2IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP2IndicationCmdL1_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP2IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP2IndicationCmdL2_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP2IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP2IndicationCmdL3_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP2SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP2SwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP3IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP3IndicationCmdL2_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP3SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP3SwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP4IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
{
  return Rte_Write_FSP4IndicationCmdL2_FSPIndicationCmd(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP4SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP4SwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP5SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP5SwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP6SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP6SwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP7SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP7SwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP8SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP8SwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP9SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP9SwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP_BSwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
{
  return Rte_Write_FSP_BSwitchStatus_FSPSwitchStatusArray(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FspNV_PR_FspNV(const uint8 *data)
{
  return Rte_Write_FspNV_PR_FspNV(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_Living12VResetRequest_Living12VResetRequest(Boolean data)
{
  return Rte_Write_Living12VResetRequest_Living12VResetRequest(data);
}

Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(Boolean data)
{
  return Rte_Write_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(void)
{
return Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl();
}
void TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(uint8 *data)
{
Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD( data);
}
void TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(uint8 *data)
{
Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure( data);
}

void TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(uint8 data)
{
  Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl( data);
}
void TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(uint8 *data)
{
  Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD( data);
}
void TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(uint8 *data)
{
  Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure( data);
}












     /* Client Server Interfaces: */
Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
{
  return Rte_Call_CddLinDiagServices_FSPAssignReq(LinBusInfo, RequestType);
}
Std_ReturnType TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
{
  return Rte_Call_CddLinDiagServices_FSPAssignResp(pDiagServiceStatus, pAvailableFSPCount, pFspErrorStatus, pFspNvData);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_FSPConfigSettingsLIN2_P1EW0_T  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v(void)
{
  return (SEWS_FSPConfigSettingsLIN2_P1EW0_T ) Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
}
SEWS_FSPConfigSettingsLIN3_P1EW1_T  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v(void)
{
  return (SEWS_FSPConfigSettingsLIN3_P1EW1_T ) Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
}
SEWS_FSPConfigSettingsLIN4_P1EW2_T  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v(void)
{
  return (SEWS_FSPConfigSettingsLIN4_P1EW2_T ) Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
}
SEWS_FSPConfigSettingsLIN5_P1EW3_T  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v(void)
{
  return (SEWS_FSPConfigSettingsLIN5_P1EW3_T ) Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
}
SEWS_FSPConfigSettingsLIN1_P1EWZ_T  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v(void)
{
  return (SEWS_FSPConfigSettingsLIN1_P1EWZ_T ) Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();
}


     /* FlexibleSwitchesRouter_Ctrl_LINMstr */
      /* FlexibleSwitchesRouter_Ctrl_LINMstr */



