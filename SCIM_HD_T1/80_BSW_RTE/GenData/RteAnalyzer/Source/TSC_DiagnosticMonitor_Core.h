/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DiagnosticMonitor_Core.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(const EngTraceHWData_T *data);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(const EngTraceHWData_T *data);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(const EngTraceHWData_T *data);

/** Service interfaces */
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_44_Core_RamFailure_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_46_Core_NvramFailure_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventFailed(boolean *EventFailed);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte);
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_SetEventStatus(Dem_EventStatusType EventStatus);




