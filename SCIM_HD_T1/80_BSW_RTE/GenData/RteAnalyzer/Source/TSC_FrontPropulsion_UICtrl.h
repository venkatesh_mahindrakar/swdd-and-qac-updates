/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_FrontPropulsion_UICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_FrontPropulsion_UICtrl_Rte_Read_FrtAxleHydro_ButtonPush_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_FrontPropulsion_UICtrl_Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_FrontPropulsion_UICtrl_Rte_Write_FrtAxleHydroActive_rqst_FrtAxleHydroActive_rqst(InactiveActive_T data);

/** Calibration Component Calibration Parameters */
boolean  TSC_FrontPropulsion_UICtrl_Rte_Prm_P1SDA_OptitrackSystemInstalled_v(void);




