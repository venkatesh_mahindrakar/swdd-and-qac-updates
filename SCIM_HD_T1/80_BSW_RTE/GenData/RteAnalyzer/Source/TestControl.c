/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TestControl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Project specific file that implements a Test Suite Component Instance
 *********************************************************************************************************************/

/* PRQA S 0777 EOF */ /* MD_MSR_5.1_777 */

#include "string.h"
#include "Std_Types.h"
#include "Rte_Cbk.h" /* PRQA S 0828, 0883 */ /* MD_MSR_1.1_828, MD_Rte_0883 */
#include "Rte_Main.h"


void TSC_BswCallbacks()
{
    uint8 buffer[320];
    (void)memset(buffer, 0, 320);
    /**********************************************************************************************************************
     * COM Callbacks for Rx Indication
     *********************************************************************************************************************/
    Rte_COMCbk_ABSInhibit_DeviceIndication_oHMIIOM_BB2_07P_oBackbone2_ee1461e1_Rx();
    Rte_COMCbk_ABSInhibitionStatus_ISig_3_oEBS_BB1_02P_oBackbone1J1939_14d96a66_Rx();
    Rte_COMCbk_ACCEnableRqst_ISig_4_oHMIIOM_BB2_01P_oBackbone2_fe5931dd_Rx();
    Rte_COMCbk_ASRHillHolderSwitch_ISig_3_oEBC1_X_EBS_oBackbone1J1939_ae8948e3_Rx();
    Rte_COMCbk_AcceleratorPedalPosition1_ISig_3_oVMCU_BB1_01P_oBackbone1J1939_7025a200_Rx();
    Rte_COMCbk_AcceleratorPedalStatus_ISig_4_oVMCU_BB2_52P_oBackbone2_c6b3ef9e_Rx();
    Rte_COMCbk_ActualDrvlnRetdrPercentTorque_ISig_3_oERC1_X_RECU_oBackbone1J1939_b2bc1e25_Rx();
    Rte_COMCbk_ActualEnginePercentTorque_ISig_3_oEEC1_X_EMS_oBackbone1J1939_cfb19d75_Rx();
    Rte_COMCbk_ActualEngineRetarderPercentTrq_ISig_3_oERC1_X_EMSRet_oBackbone1J1939_d87209ba_Rx();
    Rte_COMCbk_AmbientAirTemperature_ISig_3_oAMB_X_VMCU_oBackbone1J1939_a102c408_Rx();
    Rte_COMCbk_AudioSystemStatus_ISig_4_oHMIIOM_BB2_07P_oBackbone2_4800fc7f_Rx();
    Rte_COMCbk_AudioVolumeIndicationCmd_ISig_4_oHMIIOM_BB2_07P_oBackbone2_88a309c1_Rx();
    Rte_COMCbk_AutorelockingMovements_stat_oAlarm_Sec_02P_oSecuritySubnet_c816460c_Rx();
    Rte_COMCbk_AuxSwitchBbLoad1_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_68beafdf_Rx();
    Rte_COMCbk_AuxSwitchBbLoad2_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_42021f57_Rx();
    Rte_COMCbk_AuxSwitchBbLoad3_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_ed468d10_Rx();
    Rte_COMCbk_AuxSwitchBbLoad4_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_177b7e47_Rx();
    Rte_COMCbk_AuxSwitchBbLoad5_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_b83fec00_Rx();
    Rte_COMCbk_AuxSwitchBbLoad6_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_92835c88_Rx();
    Rte_COMCbk_BBNetwBeaconLight_stat_oBBM_BB2_02P_oBackbone2_0af6e9b5_Rx();
    Rte_COMCbk_BBNetwBodyOrCabWrknLight_stat_oBBM_BB2_02P_oBackbone2_1a41cb1a_Rx();
    Rte_COMCbk_BBNetwWrknLightChassis_stat_oBBM_BB2_02P_oBackbone2_d6b6577a_Rx();
    Rte_COMCbk_BTStatus_oHMIIOM_BB2_07P_oBackbone2_2504e67b_Rx();
    Rte_COMCbk_BackToDriveReqACK_ISig_4_oVMCU_BB2_01P_oBackbone2_220da73b_Rx();
    Rte_COMCbk_BodyOrCabWorkingLightFdbk_stat_ISig_4_oVMCU_BB2_08P_oBackbone2_6dee2288_Rx();
    Rte_COMCbk_BrakeBlending_DeviceIndication_oHMIIOM_BB2_07P_oBackbone2_22c13ed8_Rx();
    Rte_COMCbk_BrakeSwitch_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_f9976b4d_Rx();
    Rte_COMCbk_BunkH1IntLghtActvnBtn_stat_oLECM1_Cab_02P_oCabSubnet_20e1888b_Rx();
    Rte_COMCbk_BunkH1IntLghtDirAccsDnBtn_stat_oLECM1_Cab_02P_oCabSubnet_9b517646_Rx();
    Rte_COMCbk_BunkH1IntLghtDirAccsUpBtn_stat_oLECM1_Cab_02P_oCabSubnet_a6085fb2_Rx();
    Rte_COMCbk_BunkH1LockButtonStatus_oLECM1_Cab_02P_oCabSubnet_493c9af1_Rx();
    Rte_COMCbk_BunkH1RoofhatchCloseBtn_Stat_oLECM1_Cab_02P_oCabSubnet_fa799afe_Rx();
    Rte_COMCbk_BunkH1RoofhatchOpenBtn_Stat_oLECM1_Cab_02P_oCabSubnet_fde56247_Rx();
    Rte_COMCbk_BunkH1UnlockButtonStatus_oLECM1_Cab_02P_oCabSubnet_99f14132_Rx();
    Rte_COMCbk_ButtonAuth_rqst_oVMCU_BB2_01P_oBackbone2_f5cc0ebb_Rx();
    Rte_COMCbk_CCActive_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_143627fe_Rx();
    Rte_COMCbk_CCEnableRequest_ISig_4_oHMIIOM_BB2_03P_oBackbone2_53582b1d_Rx();
    Rte_COMCbk_CCStates_ISig_4_oVMCU_BB2_52P_oBackbone2_62c12527_Rx();
    Rte_COMCbk_CM_Status_ISig_4_oDACU_BB2_02P_oBackbone2_1c558e89_Rx();
    Rte_COMCbk_CabBeaconLightFeedback_Status_ISig_4_oVMCU_BB2_08P_oBackbone2_6f6463b5_Rx();
    Rte_COMCbk_CabFrontSpotFeedback_Status_oVMCU_BB2_08P_oBackbone2_d9f3ed72_Rx();
    Rte_COMCbk_CabRoofSignLightFeedback_stat_oVMCU_BB2_08P_oBackbone2_350ebaea_Rx();
    Rte_COMCbk_CabRoofSpotFeedback_Status_oVMCU_BB2_08P_oBackbone2_ae9c92bc_Rx();
    Rte_COMCbk_CabTrailerBodyLgthnFdbk_stat_oVMCU_BB2_08P_oBackbone2_8622cf32_Rx();
    Rte_COMCbk_CatalystTankLevel_ISig_3_oACM_BB1_01P_oBackbone1J1939_036544ac_Rx();
    Rte_COMCbk_ChangeKneelACK_ISig_4_oVMCU_BB2_20P_oBackbone2_9adade9e_Rx();
    Rte_COMCbk_ClutchSwitch_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_29916b2c_Rx();
    Rte_COMCbk_CtaBB_Horn_rqst_oBBM_BB2_01P_oBackbone2_6037b38a_Rx();
    Rte_COMCbk_DASApplicationStatus_ISig_4_oDACU_BB2_02P_oBackbone2_f1591292_Rx();
    Rte_COMCbk_DaytimeRunningLight_Indication_oVMCU_BB2_03P_oBackbone2_24b27437_Rx();
    Rte_COMCbk_Driver1Identification_ISig_3_oPropTCO2_X_TACHO_oBackbone1J1939_c8e2845e_Rx();
    Rte_COMCbk_Driver1TimeRelatedStates_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_2d2093d1_Rx();
    Rte_COMCbk_Driver1WorkingState_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_edae4574_Rx();
    Rte_COMCbk_Driver2TimeRelatedStates_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_4b2446e1_Rx();
    Rte_COMCbk_Driver2WorkingState_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_ba8c6926_Rx();
    Rte_COMCbk_DriverCardDriver1_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_800ed341_Rx();
    Rte_COMCbk_DriverCardDriver2_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_cde6d326_Rx();
    Rte_COMCbk_DriverDoorAjar_stat_ISig_10_oDDM_Sec_01P_oSecuritySubnet_81afb630_Rx();
    Rte_COMCbk_DriverDoorKeyCylTurned_stat_oDDM_Sec_04S_oSecuritySubnet_c22d1da5_Rx();
    Rte_COMCbk_DriverDoorLatch_stat_oDDM_Sec_01P_oSecuritySubnet_68da2bcf_Rx();
    Rte_COMCbk_DriverMemory_rqst_ISig_4_oHMIIOM_BB2_08P_oBackbone2_a1568d13_Rx();
    Rte_COMCbk_DriversIdentifications_oDI_X_TACHO_oBackbone1J1939_ebdf0af1_Rx();
    Rte_COMCbk_DrivingLightPlus_Indication_oVMCU_BB2_03P_oBackbone2_a98387bf_Rx();
    Rte_COMCbk_DrivingLight_Indication_oVMCU_BB2_03P_oBackbone2_db0b239c_Rx();
    Rte_COMCbk_DynamicCode_rqst_oHMIIOM_BB2_09P_oBackbone2_3c032dbe_Rx();
    Rte_COMCbk_ECSStandByReqWRC_oWRCS_Cab_01P_oCabSubnet_e31f608b_Rx();
    Rte_COMCbk_ECSStandbyAllowed_ISig_4_oVMCU_BB2_20P_oBackbone2_38626bd2_Rx();
    Rte_COMCbk_ECU1VIN_stat_oVMCU_BB2_31S_oBackbone2_fdb2f9bb_Rx();
    Rte_COMCbk_ECU2VIN_stat_ISig_4_oVMCU_BB2_32S_oBackbone2_10789d9f_Rx();
    Rte_COMCbk_ECU3VIN_stat_oHMIIOM_BB2_20S_oBackbone2_67971553_Rx();
    Rte_COMCbk_ECU4VIN_stat_oEMS_BB2_09S_oBackbone2_520ec3f7_Rx();
    Rte_COMCbk_ECU5VIN_stat_oTECU_BB2_06S_oBackbone2_3a7c6b09_Rx();
    Rte_COMCbk_ElectricalLoadReduction_rqst_ISig_4_oVMCU_BB2_52P_oBackbone2_8e6be74b_Rx();
    Rte_COMCbk_EmergencyDoorsUnlock_rqst_oSRS_Cab_01P_oCabSubnet_12455702_Rx();
    Rte_COMCbk_EngineCoolantTemp_stat_ISig_3_oET1_X_EMS_oBackbone1J1939_01eda871_Rx();
    Rte_COMCbk_EngineFuelRate_ISig_3_oLFE_X_EMS_oBackbone1J1939_dd8e08ba_Rx();
    Rte_COMCbk_EngineGasRate_oEMS_BB2_05P_oBackbone2_96ec243f_Rx();
    Rte_COMCbk_EnginePercentLoadAtCurrentSpd_ISig_3_oEEC2_X_EMS_oBackbone1J1939_2e4dbdde_Rx();
    Rte_COMCbk_EngineRetarderTorqueMode_ISig_3_oERC1_X_EMSRet_oBackbone1J1939_7b2fe6f4_Rx();
    Rte_COMCbk_EngineRunningTime_ISig_4_oEMS_BB2_08P_oBackbone2_50a8aaa0_Rx();
    Rte_COMCbk_EngineSpeedControlStatus_ISig_4_oVMCU_BB2_04P_oBackbone2_c5c1a7aa_Rx();
    Rte_COMCbk_EngineSpeed_ISig_3_oEEC1_X_EMS_oBackbone1J1939_56ecedf7_Rx();
    Rte_COMCbk_EngineStartAuth_rqst_oEMS_BB2_01P_oBackbone2_285ce89b_Rx();
    Rte_COMCbk_EngineTotalFuelConsumed_ISig_4_oEMS_BB2_04P_oBackbone2_ab6106b8_Rx();
    Rte_COMCbk_EngineTotalLngConsumed_ISig_4_oEMS_BB2_13P_oBackbone2_cfe79fd2_Rx();
    Rte_COMCbk_ExtraAxleSteeringFunctionStat_ISig_4_oVMCU_BB2_08P_oBackbone2_e7241d0d_Rx();
    Rte_COMCbk_ExtraBBContainerUnlockStatus_oVMCU_BB2_54P_oBackbone2_0f9800fc_Rx();
    Rte_COMCbk_ExtraBBCraneStatus_ISig_4_oVMCU_BB2_54P_oBackbone2_bd0882a3_Rx();
    Rte_COMCbk_ExtraBBSlidable5thWheelStatus_oVMCU_BB2_54P_oBackbone2_723450b2_Rx();
    Rte_COMCbk_ExtraBBTailLiftStatus_ISig_4_oVMCU_BB2_54P_oBackbone2_0401d03c_Rx();
    Rte_COMCbk_ExtraSideMarkers_FunctStat_oVMCU_BB2_73P_oBackbone2_34f556e9_Rx();
    Rte_COMCbk_FCW_Status_ISig_4_oDACU_BB2_02P_oBackbone2_da223606_Rx();
    Rte_COMCbk_FSP1ResponseErrorL1_oFSP1_Frame_L1_oLIN00_b9e1fc6c_Rx();
    Rte_COMCbk_FSP1ResponseErrorL2_oFSP1_Frame_L2_oLIN01_368cf224_Rx();
    Rte_COMCbk_FSP1ResponseErrorL3_oFSP1_Frame_L3_oLIN02_f85c49d4_Rx();
    Rte_COMCbk_FSP1ResponseErrorL4_oFSP1_Frame_L4_oLIN03_f327e8f5_Rx();
    Rte_COMCbk_FSP1ResponseErrorL5_oFSP1_Frame_L5_oLIN04_3a9a971c_Rx();
    Rte_COMCbk_FSP1SwitchStatusL1_oFSP1_Frame_L1_oLIN00_e78b81b2_Rx();
    Rte_COMCbk_FSP1SwitchStatusL2_oFSP1_Frame_L2_oLIN01_68e68ffa_Rx();
    Rte_COMCbk_FSP1SwitchStatusL3_oFSP1_Frame_L3_oLIN02_a636340a_Rx();
    Rte_COMCbk_FSP1SwitchStatusL4_oFSP1_Frame_L4_oLIN03_ad4d952b_Rx();
    Rte_COMCbk_FSP1SwitchStatusL5_oFSP1_Frame_L5_oLIN04_64f0eac2_Rx();
    Rte_COMCbk_FSP2ResponseErrorL1_oFSP2_Frame_L1_oLIN00_a793a78f_Rx();
    Rte_COMCbk_FSP2ResponseErrorL2_oFSP2_Frame_L2_oLIN01_28fea9c7_Rx();
    Rte_COMCbk_FSP2ResponseErrorL3_oFSP2_Frame_L3_oLIN02_e62e1237_Rx();
    Rte_COMCbk_FSP2SwitchStatusL1_oFSP2_Frame_L1_oLIN00_ee70e556_Rx();
    Rte_COMCbk_FSP2SwitchStatusL2_oFSP2_Frame_L2_oLIN01_611deb1e_Rx();
    Rte_COMCbk_FSP2SwitchStatusL3_oFSP2_Frame_L3_oLIN02_afcd50ee_Rx();
    Rte_COMCbk_FSP3ResponseErrorL2_oFSP3_Frame_L2_oLIN01_22d09f66_Rx();
    Rte_COMCbk_FSP3SwitchStatusL2_oFSP3_Frame_L2_oLIN01_664b3742_Rx();
    Rte_COMCbk_FSP4ResponseErrorL2_oFSP4_Frame_L2_oLIN01_141a1e01_Rx();
    Rte_COMCbk_FSP4SwitchStatusL2_oFSP4_Frame_L2_oLIN01_72eb22d6_Rx();
    Rte_COMCbk_FerryFunctionStatus_ISig_4_oVMCU_BB2_20P_oBackbone2_5e04e3f4_Rx();
    Rte_COMCbk_FerryFunctionSwitchChangeACK_ISig_4_oVMCU_BB2_01P_oBackbone2_71f009e5_Rx();
    Rte_COMCbk_FrontFog_Indication_oVMCU_BB2_03P_oBackbone2_92113a12_Rx();
    Rte_COMCbk_FrtAxleHydroActive_Status_oVMCU_BB2_20P_oBackbone2_280ed8ff_Rx();
    Rte_COMCbk_FuelLevel_ISig_3_oVMCU_BB1_03P_oBackbone1J1939_0f23d9f2_Rx();
    Rte_COMCbk_GearBoxUnlockAuth_rqst_oTECU_BB2_01P_oBackbone2_d45ce9c0_Rx();
    Rte_COMCbk_GrossCombinationVehicleWeight_ISig_3_oCVW_X_EBS_oBackbone1J1939_81d6bcd8_Rx();
    Rte_COMCbk_HandlingInformation_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_8cdf696e_Rx();
    Rte_COMCbk_HeightAdjustmentAllowed_ISig_4_oVMCU_BB2_02P_oBackbone2_3a236070_Rx();
    Rte_COMCbk_HighResEngineTotalFuelUsed_ISig_3_oHRLFC_X_EMS_oBackbone1J1939_9a2f5b32_Rx();
    Rte_COMCbk_InstantaneousFuelEconomy_ISig_3_oLFE_X_EMS_oBackbone1J1939_a1c216ba_Rx();
    Rte_COMCbk_InteriorLightLevelInd_cmd_oVMCU_BB2_03P_oBackbone2_3f259764_Rx();
    Rte_COMCbk_KeyAuthentication_rqst_oVMCU_BB2_04P_oBackbone2_c5ca7983_Rx();
    Rte_COMCbk_KeyID_DriverDoorLatch_rqst_oDDM_Sec_05S_oSecuritySubnet_99c5da4c_Rx();
    Rte_COMCbk_KeyID_EncryptedGBUnlockAuth_oTECU_BB2_05S_oBackbone2_1d0ec920_Rx();
    Rte_COMCbk_KeyID_EngineStartAuth_oEMS_BB2_11S_oBackbone2_7b40f91d_Rx();
    Rte_COMCbk_KeyID_KeyAuthEncrypted_oVMCU_BB2_34S_oBackbone2_c663ac17_Rx();
    Rte_COMCbk_KeyID_LuggageCompartment_stat_oAlarm_Sec_07S_oSecuritySubnet_8ed452d8_Rx();
    Rte_COMCbk_KeyID_PassengerDoorLatch_rqst_oPDM_Sec_03S_oSecuritySubnet_09c03245_Rx();
    Rte_COMCbk_KeyID_ReducedSetMode_oAlarm_Sec_03S_oSecuritySubnet_0a371acd_Rx();
    Rte_COMCbk_KeyID_TheftAlarmActivation_oAlarm_Sec_06S_oSecuritySubnet_1e0daa05_Rx();
    Rte_COMCbk_KeyPosition_ISig_4_oVMCU_BB2_01P_oBackbone2_666d9433_Rx();
    Rte_COMCbk_KeyfobInCabPresencePS_rqst_oVMCU_BB2_82P_oBackbone2_c182d016_Rx();
    Rte_COMCbk_KneelingStatusHMI_ISig_4_oVMCU_BB2_20P_oBackbone2_755dcb67_Rx();
    Rte_COMCbk_LCSSystemStatus_oDACU_BB2_02P_oBackbone2_a47552fc_Rx();
    Rte_COMCbk_LIN_BunkH2PowerWinCloseDSBtn_s_oLECM2toCIOM_FR2_L1_oLIN00_9fa2715d_Rx();
    Rte_COMCbk_LIN_BunkH2PowerWinClosePSBtn_s_oLECM2toCIOM_FR2_L1_oLIN00_281d2c26_Rx();
    Rte_COMCbk_LIN_BunkH2PowerWinOpenDSBtn_st_oLECM2toCIOM_FR2_L1_oLIN00_0df6d397_Rx();
    Rte_COMCbk_LIN_BunkH2PowerWinOpenPSBtn_st_oLECM2toCIOM_FR2_L1_oLIN00_ca96c47e_Rx();
    Rte_COMCbk_LIN_IntLghtDimmingLvlFreeWhl_s_oILCP2toCIOM_L4_oLIN03_9d9afdd7_Rx();
    Rte_COMCbk_LIN_IntLghtModeSelrFreeWheel_s_oILCP1toCIOM_L1_oLIN00_98c2e509_Rx();
    Rte_COMCbk_LIN_LightMode_Status_1_oELCP1toCIOM_L4_oLIN03_c62c6da2_Rx();
    Rte_COMCbk_LIN_LightMode_Status_2_oELCP2toCIOM_L4_oLIN03_cda89f0e_Rx();
    Rte_COMCbk_LKSApplicationStatus_ISig_4_oDACU_BB2_02P_oBackbone2_a1929cc2_Rx();
    Rte_COMCbk_LKSCorrectiveSteeringStatus_ISig_4_oDACU_BB2_02P_oBackbone2_5bf2e494_Rx();
    Rte_COMCbk_LevelControlInformation_ISig_4_oVMCU_BB2_02P_oBackbone2_c3136e85_Rx();
    Rte_COMCbk_LiftAxle1PositionStatus_ISig_4_oVMCU_BB2_02P_oBackbone2_8f87d670_Rx();
    Rte_COMCbk_LiftAxle1UpRequestACK_ISig_4_oVMCU_BB2_02P_oBackbone2_fa0577fd_Rx();
    Rte_COMCbk_LiftAxle2UpRequestACK_ISig_4_oVMCU_BB2_02P_oBackbone2_c5ce0968_Rx();
    Rte_COMCbk_LngTank1RemainingGasVolume_ISig_3_oACM_BB1_01P_oBackbone1J1939_8a2851cd_Rx();
    Rte_COMCbk_LngTank2RemainingGasVolume_ISig_3_oACM_BB1_01P_oBackbone1J1939_ec2c84fd_Rx();
    Rte_COMCbk_LoadDistributionALDChangeACK_ISig_4_oVMCU_BB2_54P_oBackbone2_0286104b_Rx();
    Rte_COMCbk_LoadDistributionChangeACK_ISig_4_oVMCU_BB2_02P_oBackbone2_3f2946ee_Rx();
    Rte_COMCbk_LoadDistributionFuncSelected_ISig_4_oVMCU_BB2_02P_oBackbone2_e30d445f_Rx();
    Rte_COMCbk_LoadDistributionRequestedACK_ISig_4_oVMCU_BB2_52P_oBackbone2_27f055a3_Rx();
    Rte_COMCbk_LoadDistributionSelected_ISig_4_oVMCU_BB2_02P_oBackbone2_91087134_Rx();
    Rte_COMCbk_Locking_Switch_stat_oDDM_Sec_03S_oSecuritySubnet_5089aa26_Rx();
    Rte_COMCbk_MinutesUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_b5b3e478_Rx();
    Rte_COMCbk_MirrorHeat_rqst_ddm_oDDM_Sec_01P_oSecuritySubnet_f2c6892d_Rx();
    Rte_COMCbk_MirrorHeatingMode_ISig_4_oVMCU_BB2_03P_oBackbone2_93dcd786_Rx();
    Rte_COMCbk_ParkingLight_Indication_oVMCU_BB2_03P_oBackbone2_e64048d9_Rx();
    Rte_COMCbk_PassengerDoorAjar_stat_ISig_10_oPDM_Sec_01P_oSecuritySubnet_15fc3894_Rx();
    Rte_COMCbk_PassengerDoorKeyCylTurned_stat_oPDM_Sec_04S_oSecuritySubnet_567e9301_Rx();
    Rte_COMCbk_PassengerDoorLatch_stat_oPDM_Sec_01P_oSecuritySubnet_7ad7cbe3_Rx();
    Rte_COMCbk_PhoneButtonIndication_cmd_oHMIIOM_BB2_07P_oBackbone2_1c062854_Rx();
    Rte_COMCbk_PinCodeEntered_value_oHMIIOM_BB2_09P_oBackbone2_7f89cada_Rx();
    Rte_COMCbk_PloughLampModeStatus_ISig_4_oBBM_BB2_01P_oBackbone2_ce1fdf4f_Rx();
    Rte_COMCbk_PredeliveryModeIndication_cmd_ISig_4_oVMCU_BB2_04P_oBackbone2_eefcbaa0_Rx();
    Rte_COMCbk_Pto1Status_ISig_4_oVMCU_BB2_07P_oBackbone2_c7aca240_Rx();
    Rte_COMCbk_Pto2Indication_oVMCU_BB2_07P_oBackbone2_26780e74_Rx();
    Rte_COMCbk_Pto2Status_ISig_4_oVMCU_BB2_07P_oBackbone2_1437bebb_Rx();
    Rte_COMCbk_Pto4Status_ISig_4_oVMCU_BB2_07P_oBackbone2_6870810c_Rx();
    Rte_COMCbk_PtosStatus_ISig_4_oVMCU_BB2_07P_oBackbone2_9464094a_Rx();
    Rte_COMCbk_RSLMgrRptTRSLEnabledOpStat_oVMCU_BB2_08P_oBackbone2_748e4c57_Rx();
    Rte_COMCbk_RampLevelRequestACK_ISig_4_oVMCU_BB2_07P_oBackbone2_a790dd8f_Rx();
    Rte_COMCbk_RampLevelStorageAck_ISig_4_oVMCU_BB2_02P_oBackbone2_4026e627_Rx();
    Rte_COMCbk_RearAxleSteeringFunctionStatus_oEBS_BB1_02P_oBackbone1J1939_101ad651_Rx();
    Rte_COMCbk_RearFog_Indication_oVMCU_BB2_03P_oBackbone2_fa31dca4_Rx();
    Rte_COMCbk_RegenerationIndication_oHMIIOM_BB2_24P_oBackbone2_777f5711_Rx();
    Rte_COMCbk_Regeneration_DeviceInd_oHMIIOM_BB2_24P_oBackbone2_86c1e4d1_Rx();
    Rte_COMCbk_ResponseErrorCCFW_oCCFWtoCIOM_L4_oLIN03_38ff59f8_Rx();
    Rte_COMCbk_ResponseErrorDLFW_oDLFWtoCIOM_L4_oLIN03_a0bc7d9c_Rx();
    Rte_COMCbk_ResponseErrorELCP1_oELCP1toCIOM_L4_oLIN03_a9bf457f_Rx();
    Rte_COMCbk_ResponseErrorELCP2_oELCP2toCIOM_L4_oLIN03_a23bb7d3_Rx();
    Rte_COMCbk_ResponseErrorILCP1_oILCP1toCIOM_L1_oLIN00_44250bf0_Rx();
    Rte_COMCbk_ResponseErrorILCP2_oILCP2toCIOM_L4_oLIN03_9e48a682_Rx();
    Rte_COMCbk_ResponseErrorLECM2_oLECM2toCIOM_FR1_L1_oLIN00_ed8050df_Rx();
    Rte_COMCbk_ResponseErrorLECMBasic_oLECMBasic2CIOM_L1_oLIN00_9a0b7e5e_Rx();
    Rte_COMCbk_ResponseErrorRCECS_oRCECStoCIOM_L5_oLIN04_07d94fb4_Rx();
    Rte_COMCbk_RetarderTorqueMode_ISig_3_oERC1_X_RECU_oBackbone1J1939_68c292d8_Rx();
    Rte_COMCbk_ReverseGearEngaged_ISig_4_oVMCU_BB2_01P_oBackbone2_ea2f14fc_Rx();
    Rte_COMCbk_RideHeightStorageAck_ISig_4_oVMCU_BB2_02P_oBackbone2_af65dfcb_Rx();
    Rte_COMCbk_SG_AlmClkCurAlarm_stat_sg_oHMIIOM_BB2_10P_oBackbone2_16a10c0a_Rx();
    Rte_COMCbk_SG_BnkH1IntLghtMMenu_stat_sg_oLECM1_Cab_02P_oCabSubnet_72949da5_Rx();
    Rte_COMCbk_SG_FMS1_sg_ISig_3_oFMS1_X_HMIIOM_oBackbone1J1939_a2ca12fb_Rx();
    Rte_COMCbk_SG_FPBRMMIStat_sg_ISig_4_oVMCU_BB2_74P_oBackbone2_6ccb74d8_Rx();
    Rte_COMCbk_SG_IntLightMode_CoreRqst_sg_oVMCU_BB2_03P_oBackbone2_fb45020d_Rx();
    Rte_COMCbk_SG_LIN_BunkH2PHTi_rqs_sg_oLECM2toCIOM_FR3_L1_oLIN00_0bf5fe80_Rx();
    Rte_COMCbk_SG_MaintService_sg_ISig_4_18f78124_oVMCU_BB2_57P_oBackbone2_6378e077_Rx();
    Rte_COMCbk_SG_OilPrediction_sg_oEMS_BB2_06P_oBackbone2_f16954dd_Rx();
    Rte_COMCbk_SWSpdCtrlButtonsStatus1_oHMIIOM_BB2_01P_oBackbone2_f235f469_Rx();
    Rte_COMCbk_SWSpeedControlAdjustMode_oHMIIOM_BB2_01P_oBackbone2_4c33da86_Rx();
    Rte_COMCbk_SecondsUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_5a9bc5ac_Rx();
    Rte_COMCbk_ServiceBrakeAirPrsCircuit1_ISig_3_oAIR1_X_VMCU_oBackbone1J1939_e8caeb17_Rx();
    Rte_COMCbk_ServiceBrakeAirPrsCircuit2_ISig_3_oAIR1_X_VMCU_oBackbone1J1939_b2abd477_Rx();
    Rte_COMCbk_SpecialLightSituation_ind_oVMCU_BB2_52P_oBackbone2_bb95932a_Rx();
    Rte_COMCbk_SpeedLockingInhibition_stat_oHMIIOM_BB2_08P_oBackbone2_0bfbe2ed_Rx();
    Rte_COMCbk_StopLevelChangeAck_ISig_4_oVMCU_BB2_02P_oBackbone2_5eeeb44a_Rx();
    Rte_COMCbk_SwitchDetectionResp1L1_oFSP1_SwitchDetResp_L1_oLIN00_e8a163d5_Rx();
    Rte_COMCbk_SwitchDetectionResp1L2_oFSP1_SwitchDetResp_L2_oLIN01_02016dd3_Rx();
    Rte_COMCbk_SwitchDetectionResp1L3_oFSP1_SwitchDetResp_L3_oLIN02_5945d426_Rx();
    Rte_COMCbk_SwitchDetectionResp1L4_oFSP1_SwitchDetResp_L4_oLIN03_0c30779e_Rx();
    Rte_COMCbk_SwitchDetectionResp1L5_oFSP1_SwitchDetResp_L5_oLIN04_50190a72_Rx();
    Rte_COMCbk_SwitchDetectionResp2L1_oFSP2_SwitchDetResp_L1_oLIN00_865b6493_Rx();
    Rte_COMCbk_SwitchDetectionResp2L2_oFSP2_SwitchDetResp_L2_oLIN01_6cfb6a95_Rx();
    Rte_COMCbk_SwitchDetectionResp2L3_oFSP2_SwitchDetResp_L3_oLIN02_37bfd360_Rx();
    Rte_COMCbk_SwitchDetectionResp3L2_oFSP3_SwitchDetResp_L2_oLIN01_ff826a68_Rx();
    Rte_COMCbk_SwitchDetectionResp4L2_oFSP4_SwitchDetResp_L2_oLIN01_b10f6419_Rx();
    Rte_COMCbk_Synch_Unsynch_Mode_stat_oHMIIOM_BB2_08P_oBackbone2_8a425ab2_Rx();
    Rte_COMCbk_SystemEvent_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_5a5c5e53_Rx();
    Rte_COMCbk_TachographPerformance_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_1bf17036_Rx();
    Rte_COMCbk_TachographVehicleSpeed_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_78a7f516_Rx();
    Rte_COMCbk_TotalVehicleDistanceHighRes_ISig_3_oVDHR_X_VMCU_oBackbone1J1939_6b802705_Rx();
    Rte_COMCbk_TrailerBodyLampDI_stat_oVMCU_BB2_80P_oBackbone2_47992f30_Rx();
    Rte_COMCbk_TrailerBodyLampFdbk_stat_oVMCU_BB2_80P_oBackbone2_fa358fbc_Rx();
    Rte_COMCbk_TransferCaseNeutral_Ack_oVMCU_BB2_73P_oBackbone2_3029afb8_Rx();
    Rte_COMCbk_TransferCaseNeutral_status_oVMCU_BB2_29P_oBackbone2_0936e4a4_Rx();
    Rte_COMCbk_TransferCasePTOEngaged_ISig_4_oVMCU_BB2_29P_oBackbone2_9e9887d1_Rx();
    Rte_COMCbk_VehFrontAxle1CalibratedLoad_ISig_4_oHMIIOM_BB2_11P_oBackbone2_b768cee7_Rx();
    Rte_COMCbk_VehRearAxle1CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_6732f367_Rx();
    Rte_COMCbk_VehRearAxle2CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_2dab8413_Rx();
    Rte_COMCbk_VehRearAxle3CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_a2f3ab00_Rx();
    Rte_COMCbk_VehicleMode_ISig_3_oVMCU_BB1_01P_oBackbone1J1939_997c94c6_Rx();
    Rte_COMCbk_VehicleMotion_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_c4c069be_Rx();
    Rte_COMCbk_VehicleOverspeed_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_b3d80892_Rx();
    Rte_COMCbk_WRC5thWheelRequest_oWRCS_Cab_02P_oCabSubnet_6e07aa44_Rx();
    Rte_COMCbk_WRCAirSuspensionStopRequest_oWRCS_Cab_01P_oCabSubnet_18c85d80_Rx();
    Rte_COMCbk_WRCAux1Request_oWRCS_Cab_01P_oCabSubnet_3d62fac1_Rx();
    Rte_COMCbk_WRCAux2Request_oWRCS_Cab_01P_oCabSubnet_66754bd4_Rx();
    Rte_COMCbk_WRCAux3Request_oWRCS_Cab_01P_oCabSubnet_5087db27_Rx();
    Rte_COMCbk_WRCAux4Request_oWRCS_Cab_01P_oCabSubnet_d05a29fe_Rx();
    Rte_COMCbk_WRCAux5Request_oWRCS_Cab_01P_oCabSubnet_e6a8b90d_Rx();
    Rte_COMCbk_WRCAux6Request_oWRCS_Cab_01P_oCabSubnet_bdbf0818_Rx();
    Rte_COMCbk_WRCBeaconRequest_oWRCS_Cab_02P_oCabSubnet_7ba25fea_Rx();
    Rte_COMCbk_WRCCabBodyWLightsRqst_oWRCS_Cab_02P_oCabSubnet_b2a0874a_Rx();
    Rte_COMCbk_WRCCraneRequest_oWRCS_Cab_01P_oCabSubnet_95c4579e_Rx();
    Rte_COMCbk_WRCLevelAdjustmentAction_oWRCS_Cab_01P_oCabSubnet_810159ff_Rx();
    Rte_COMCbk_WRCLevelAdjustmentAxles_oWRCS_Cab_01P_oCabSubnet_b1b2524c_Rx();
    Rte_COMCbk_WRCLevelAdjustmentStroke_oWRCS_Cab_01P_oCabSubnet_e59ce5a1_Rx();
    Rte_COMCbk_WRCLevelUserMemoryAction_oWRCS_Cab_02P_oCabSubnet_42c76a61_Rx();
    Rte_COMCbk_WRCLevelUserMemory_oWRCS_Cab_02P_oCabSubnet_d79fc958_Rx();
    Rte_COMCbk_WRCLockButtonStatus_oWRCS_Cab_01P_oCabSubnet_762ddecb_Rx();
    Rte_COMCbk_WRCRollRequest_oWRCS_Cab_01P_oCabSubnet_47562199_Rx();
    Rte_COMCbk_WRCTailLiftRequest_oWRCS_Cab_01P_oCabSubnet_a1fd4993_Rx();
    Rte_COMCbk_WRCUnlockButtonStatus_oWRCS_Cab_01P_oCabSubnet_fd05435c_Rx();
    Rte_COMCbk_WheelBasedVehicleSpeed_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_e3ff6d4c_Rx();
    Rte_COMCbk_WrcEngSpdCtrlDecreaseButtStat_oWRCS_Cab_02P_oCabSubnet_f90375a0_Rx();
    Rte_COMCbk_WrcEngSpdCtrlEnableStatus_oWRCS_Cab_02P_oCabSubnet_c15f6325_Rx();
    Rte_COMCbk_WrcEngSpdCtrlIncreaseButtStat_oWRCS_Cab_02P_oCabSubnet_7a1f067c_Rx();
    Rte_COMCbk_WrcEngSpdCtrlResumeButtonStat_oWRCS_Cab_02P_oCabSubnet_09c53237_Rx();
    Rte_COMCbk_WrcPto1ButtonStatus_oWRCS_Cab_01P_oCabSubnet_9b7a6ac6_Rx();
    Rte_COMCbk_WrcPto2ButtonStatus_oWRCS_Cab_01P_oCabSubnet_d6926aa1_Rx();
    Rte_COMCbk_WrcPto3ButtonStatus_oWRCS_Cab_01P_oCabSubnet_5b1a9743_Rx();
    Rte_COMCbk_WrcPto4ButtonStatus_oWRCS_Cab_01P_oCabSubnet_4d426a6f_Rx();
    Rte_COMCbk_XRSLStates_oVMCU_BB2_51P_oBackbone2_06570c1b_Rx();

    /**********************************************************************************************************************
     * COM Callbacks for Tx Confirmation
     *********************************************************************************************************************/
    Rte_COMCbkTAck_DriverDoorLatch_rqst_oCIOM_Sec_12S_oSecuritySubnet_bb5d6202_Tx();
    Rte_COMCbkTAck_EncryptedGBUnlockAuth_stat_oCIOM_BB2_30S_oBackbone2_a01fb2a3_Tx();
    Rte_COMCbkTAck_EngineStartAuth_stat_oCIOM_BB2_13S_oBackbone2_6b05f3d8_Tx();
    Rte_COMCbkTAck_KeyAuthEncrypted_stat_oCIOM_BB2_12S_oBackbone2_f8744b4b_Tx();
    Rte_COMCbkTAck_KeyID_DrivDoorKeyCylinderStat_oCIOM_Sec_09S_oSecuritySubnet_d3beca19_Tx();
    Rte_COMCbkTAck_KeyID_Locking_Switch_stat_oCIOM_Sec_08S_oSecuritySubnet_39feba2e_Tx();
    Rte_COMCbkTAck_KeyID_PassDoorKeyCylinderStat_oCIOM_Sec_11S_oSecuritySubnet_67a2f229_Tx();
    Rte_COMCbkTAck_LuggageCompartment_stat_oCIOM_Sec_05S_oSecuritySubnet_64873c5a_Tx();
    Rte_COMCbkTAck_PassengerDoorLatch_rqst_oCIOM_Sec_10S_oSecuritySubnet_91cb4d26_Tx();
    Rte_COMCbkTAck_ReducedSetMode_rqst_oCIOM_Sec_06S_oSecuritySubnet_a3c01e34_Tx();
    Rte_COMCbkTAck_TheftAlarmActivation_rqst_oCIOM_Sec_07S_oSecuritySubnet_cc025a3d_Tx();
    /**********************************************************************************************************************
     * COM Callbacks for Tx Error and Tx Timeout Notification
     *********************************************************************************************************************/
    Rte_COMCbkTErr_DriverDoorLatch_rqst_oCIOM_Sec_12S_oSecuritySubnet_bb5d6202_Tx();
    Rte_COMCbkTErr_EncryptedGBUnlockAuth_stat_oCIOM_BB2_30S_oBackbone2_a01fb2a3_Tx();
    Rte_COMCbkTErr_EngineStartAuth_stat_oCIOM_BB2_13S_oBackbone2_6b05f3d8_Tx();
    Rte_COMCbkTErr_KeyAuthEncrypted_stat_oCIOM_BB2_12S_oBackbone2_f8744b4b_Tx();
    Rte_COMCbkTErr_KeyID_DrivDoorKeyCylinderStat_oCIOM_Sec_09S_oSecuritySubnet_d3beca19_Tx();
    Rte_COMCbkTErr_KeyID_Locking_Switch_stat_oCIOM_Sec_08S_oSecuritySubnet_39feba2e_Tx();
    Rte_COMCbkTErr_KeyID_PassDoorKeyCylinderStat_oCIOM_Sec_11S_oSecuritySubnet_67a2f229_Tx();
    Rte_COMCbkTErr_LuggageCompartment_stat_oCIOM_Sec_05S_oSecuritySubnet_64873c5a_Tx();
    Rte_COMCbkTErr_PassengerDoorLatch_rqst_oCIOM_Sec_10S_oSecuritySubnet_91cb4d26_Tx();
    Rte_COMCbkTErr_ReducedSetMode_rqst_oCIOM_Sec_06S_oSecuritySubnet_a3c01e34_Tx();
    Rte_COMCbkTErr_TheftAlarmActivation_rqst_oCIOM_Sec_07S_oSecuritySubnet_cc025a3d_Tx();
    Rte_COMCbkTxTOut_DriverDoorLatch_rqst_oCIOM_Sec_12S_oSecuritySubnet_bb5d6202_Tx();
    Rte_COMCbkTxTOut_EncryptedGBUnlockAuth_stat_oCIOM_BB2_30S_oBackbone2_a01fb2a3_Tx();
    Rte_COMCbkTxTOut_EngineStartAuth_stat_oCIOM_BB2_13S_oBackbone2_6b05f3d8_Tx();
    Rte_COMCbkTxTOut_KeyAuthEncrypted_stat_oCIOM_BB2_12S_oBackbone2_f8744b4b_Tx();
    Rte_COMCbkTxTOut_KeyID_DrivDoorKeyCylinderStat_oCIOM_Sec_09S_oSecuritySubnet_d3beca19_Tx();
    Rte_COMCbkTxTOut_KeyID_Locking_Switch_stat_oCIOM_Sec_08S_oSecuritySubnet_39feba2e_Tx();
    Rte_COMCbkTxTOut_KeyID_PassDoorKeyCylinderStat_oCIOM_Sec_11S_oSecuritySubnet_67a2f229_Tx();
    Rte_COMCbkTxTOut_LuggageCompartment_stat_oCIOM_Sec_05S_oSecuritySubnet_64873c5a_Tx();
    Rte_COMCbkTxTOut_PassengerDoorLatch_rqst_oCIOM_Sec_10S_oSecuritySubnet_91cb4d26_Tx();
    Rte_COMCbkTxTOut_ReducedSetMode_rqst_oCIOM_Sec_06S_oSecuritySubnet_a3c01e34_Tx();
    Rte_COMCbkTxTOut_TheftAlarmActivation_rqst_oCIOM_Sec_07S_oSecuritySubnet_cc025a3d_Tx();

    /**********************************************************************************************************************
     * COM Callbacks for Rx Timeout Notification
     *********************************************************************************************************************/
    Rte_COMCbkRxTOut_ABSInhibit_DeviceIndication_oHMIIOM_BB2_07P_oBackbone2_ee1461e1_Rx();
    Rte_COMCbkRxTOut_ABSInhibitionStatus_ISig_3_oEBS_BB1_02P_oBackbone1J1939_14d96a66_Rx();
    Rte_COMCbkRxTOut_ACCEnableRqst_ISig_4_oHMIIOM_BB2_01P_oBackbone2_fe5931dd_Rx();
    Rte_COMCbkRxTOut_ASRHillHolderSwitch_ISig_3_oEBC1_X_EBS_oBackbone1J1939_ae8948e3_Rx();
    Rte_COMCbkRxTOut_AcceleratorPedalPosition1_ISig_3_oVMCU_BB1_01P_oBackbone1J1939_7025a200_Rx();
    Rte_COMCbkRxTOut_AcceleratorPedalStatus_ISig_4_oVMCU_BB2_52P_oBackbone2_c6b3ef9e_Rx();
    Rte_COMCbkRxTOut_ActualDrvlnRetdrPercentTorque_ISig_3_oERC1_X_RECU_oBackbone1J1939_b2bc1e25_Rx();
    Rte_COMCbkRxTOut_ActualEnginePercentTorque_ISig_3_oEEC1_X_EMS_oBackbone1J1939_cfb19d75_Rx();
    Rte_COMCbkRxTOut_ActualEngineRetarderPercentTrq_ISig_3_oERC1_X_EMSRet_oBackbone1J1939_d87209ba_Rx();
    Rte_COMCbkRxTOut_AmbientAirTemperature_ISig_3_oAMB_X_VMCU_oBackbone1J1939_a102c408_Rx();
    Rte_COMCbkRxTOut_AudioSystemStatus_ISig_4_oHMIIOM_BB2_07P_oBackbone2_4800fc7f_Rx();
    Rte_COMCbkRxTOut_AudioVolumeIndicationCmd_ISig_4_oHMIIOM_BB2_07P_oBackbone2_88a309c1_Rx();
    Rte_COMCbkRxTOut_AutorelockingMovements_stat_oAlarm_Sec_02P_oSecuritySubnet_c816460c_Rx();
    Rte_COMCbkRxTOut_AuxSwitchBbLoad1_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_68beafdf_Rx();
    Rte_COMCbkRxTOut_AuxSwitchBbLoad2_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_42021f57_Rx();
    Rte_COMCbkRxTOut_AuxSwitchBbLoad3_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_ed468d10_Rx();
    Rte_COMCbkRxTOut_AuxSwitchBbLoad4_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_177b7e47_Rx();
    Rte_COMCbkRxTOut_AuxSwitchBbLoad5_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_b83fec00_Rx();
    Rte_COMCbkRxTOut_AuxSwitchBbLoad6_Status_ISig_4_oVMCU_BB2_53P_oBackbone2_92835c88_Rx();
    Rte_COMCbkRxTOut_BBNetwBeaconLight_stat_oBBM_BB2_02P_oBackbone2_0af6e9b5_Rx();
    Rte_COMCbkRxTOut_BBNetwBodyOrCabWrknLight_stat_oBBM_BB2_02P_oBackbone2_1a41cb1a_Rx();
    Rte_COMCbkRxTOut_BBNetwWrknLightChassis_stat_oBBM_BB2_02P_oBackbone2_d6b6577a_Rx();
    Rte_COMCbkRxTOut_BTStatus_oHMIIOM_BB2_07P_oBackbone2_2504e67b_Rx();
    Rte_COMCbkRxTOut_BackToDriveReqACK_ISig_4_oVMCU_BB2_01P_oBackbone2_220da73b_Rx();
    Rte_COMCbkRxTOut_BodyOrCabWorkingLightFdbk_stat_ISig_4_oVMCU_BB2_08P_oBackbone2_6dee2288_Rx();
    Rte_COMCbkRxTOut_BrakeBlending_DeviceIndication_oHMIIOM_BB2_07P_oBackbone2_22c13ed8_Rx();
    Rte_COMCbkRxTOut_BrakeSwitch_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_f9976b4d_Rx();
    Rte_COMCbkRxTOut_BunkH1IntLghtActvnBtn_stat_oLECM1_Cab_02P_oCabSubnet_20e1888b_Rx();
    Rte_COMCbkRxTOut_BunkH1IntLghtDirAccsDnBtn_stat_oLECM1_Cab_02P_oCabSubnet_9b517646_Rx();
    Rte_COMCbkRxTOut_BunkH1IntLghtDirAccsUpBtn_stat_oLECM1_Cab_02P_oCabSubnet_a6085fb2_Rx();
    Rte_COMCbkRxTOut_BunkH1LockButtonStatus_oLECM1_Cab_02P_oCabSubnet_493c9af1_Rx();
    Rte_COMCbkRxTOut_BunkH1RoofhatchCloseBtn_Stat_oLECM1_Cab_02P_oCabSubnet_fa799afe_Rx();
    Rte_COMCbkRxTOut_BunkH1RoofhatchOpenBtn_Stat_oLECM1_Cab_02P_oCabSubnet_fde56247_Rx();
    Rte_COMCbkRxTOut_BunkH1UnlockButtonStatus_oLECM1_Cab_02P_oCabSubnet_99f14132_Rx();
    Rte_COMCbkRxTOut_ButtonAuth_rqst_oVMCU_BB2_01P_oBackbone2_f5cc0ebb_Rx();
    Rte_COMCbkRxTOut_CCActive_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_143627fe_Rx();
    Rte_COMCbkRxTOut_CCEnableRequest_ISig_4_oHMIIOM_BB2_03P_oBackbone2_53582b1d_Rx();
    Rte_COMCbkRxTOut_CCStates_ISig_4_oVMCU_BB2_52P_oBackbone2_62c12527_Rx();
    Rte_COMCbkRxTOut_CM_Status_ISig_4_oDACU_BB2_02P_oBackbone2_1c558e89_Rx();
    Rte_COMCbkRxTOut_CabBeaconLightFeedback_Status_ISig_4_oVMCU_BB2_08P_oBackbone2_6f6463b5_Rx();
    Rte_COMCbkRxTOut_CabFrontSpotFeedback_Status_oVMCU_BB2_08P_oBackbone2_d9f3ed72_Rx();
    Rte_COMCbkRxTOut_CabRoofSignLightFeedback_stat_oVMCU_BB2_08P_oBackbone2_350ebaea_Rx();
    Rte_COMCbkRxTOut_CabRoofSpotFeedback_Status_oVMCU_BB2_08P_oBackbone2_ae9c92bc_Rx();
    Rte_COMCbkRxTOut_CabTrailerBodyLgthnFdbk_stat_oVMCU_BB2_08P_oBackbone2_8622cf32_Rx();
    Rte_COMCbkRxTOut_CatalystTankLevel_ISig_3_oACM_BB1_01P_oBackbone1J1939_036544ac_Rx();
    Rte_COMCbkRxTOut_ChangeKneelACK_ISig_4_oVMCU_BB2_20P_oBackbone2_9adade9e_Rx();
    Rte_COMCbkRxTOut_ClutchSwitch_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_29916b2c_Rx();
    Rte_COMCbkRxTOut_CtaBB_Horn_rqst_oBBM_BB2_01P_oBackbone2_6037b38a_Rx();
    Rte_COMCbkRxTOut_DASApplicationStatus_ISig_4_oDACU_BB2_02P_oBackbone2_f1591292_Rx();
    Rte_COMCbkRxTOut_DaytimeRunningLight_Indication_oVMCU_BB2_03P_oBackbone2_24b27437_Rx();
    Rte_COMCbkRxTOut_Driver1TimeRelatedStates_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_2d2093d1_Rx();
    Rte_COMCbkRxTOut_Driver1WorkingState_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_edae4574_Rx();
    Rte_COMCbkRxTOut_Driver2TimeRelatedStates_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_4b2446e1_Rx();
    Rte_COMCbkRxTOut_Driver2WorkingState_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_ba8c6926_Rx();
    Rte_COMCbkRxTOut_DriverCardDriver1_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_800ed341_Rx();
    Rte_COMCbkRxTOut_DriverCardDriver2_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_cde6d326_Rx();
    Rte_COMCbkRxTOut_DriverDoorAjar_stat_ISig_10_oDDM_Sec_01P_oSecuritySubnet_81afb630_Rx();
    Rte_COMCbkRxTOut_DriverDoorLatch_stat_oDDM_Sec_01P_oSecuritySubnet_68da2bcf_Rx();
    Rte_COMCbkRxTOut_DriverMemory_rqst_ISig_4_oHMIIOM_BB2_08P_oBackbone2_a1568d13_Rx();
    Rte_COMCbkRxTOut_DrivingLightPlus_Indication_oVMCU_BB2_03P_oBackbone2_a98387bf_Rx();
    Rte_COMCbkRxTOut_DrivingLight_Indication_oVMCU_BB2_03P_oBackbone2_db0b239c_Rx();
    Rte_COMCbkRxTOut_DynamicCode_rqst_oHMIIOM_BB2_09P_oBackbone2_3c032dbe_Rx();
    Rte_COMCbkRxTOut_ECSStandByReqWRC_oWRCS_Cab_01P_oCabSubnet_e31f608b_Rx();
    Rte_COMCbkRxTOut_ECSStandbyAllowed_ISig_4_oVMCU_BB2_20P_oBackbone2_38626bd2_Rx();
    Rte_COMCbkRxTOut_ElectricalLoadReduction_rqst_ISig_4_oVMCU_BB2_52P_oBackbone2_8e6be74b_Rx();
    Rte_COMCbkRxTOut_EmergencyDoorsUnlock_rqst_oSRS_Cab_01P_oCabSubnet_12455702_Rx();
    Rte_COMCbkRxTOut_EngineCoolantTemp_stat_ISig_3_oET1_X_EMS_oBackbone1J1939_01eda871_Rx();
    Rte_COMCbkRxTOut_EngineFuelRate_ISig_3_oLFE_X_EMS_oBackbone1J1939_dd8e08ba_Rx();
    Rte_COMCbkRxTOut_EngineGasRate_oEMS_BB2_05P_oBackbone2_96ec243f_Rx();
    Rte_COMCbkRxTOut_EnginePercentLoadAtCurrentSpd_ISig_3_oEEC2_X_EMS_oBackbone1J1939_2e4dbdde_Rx();
    Rte_COMCbkRxTOut_EngineRetarderTorqueMode_ISig_3_oERC1_X_EMSRet_oBackbone1J1939_7b2fe6f4_Rx();
    Rte_COMCbkRxTOut_EngineRunningTime_ISig_4_oEMS_BB2_08P_oBackbone2_50a8aaa0_Rx();
    Rte_COMCbkRxTOut_EngineSpeedControlStatus_ISig_4_oVMCU_BB2_04P_oBackbone2_c5c1a7aa_Rx();
    Rte_COMCbkRxTOut_EngineSpeed_ISig_3_oEEC1_X_EMS_oBackbone1J1939_56ecedf7_Rx();
    Rte_COMCbkRxTOut_EngineStartAuth_rqst_oEMS_BB2_01P_oBackbone2_285ce89b_Rx();
    Rte_COMCbkRxTOut_EngineTotalFuelConsumed_ISig_4_oEMS_BB2_04P_oBackbone2_ab6106b8_Rx();
    Rte_COMCbkRxTOut_EngineTotalLngConsumed_ISig_4_oEMS_BB2_13P_oBackbone2_cfe79fd2_Rx();
    Rte_COMCbkRxTOut_ExtraAxleSteeringFunctionStat_ISig_4_oVMCU_BB2_08P_oBackbone2_e7241d0d_Rx();
    Rte_COMCbkRxTOut_ExtraBBContainerUnlockStatus_oVMCU_BB2_54P_oBackbone2_0f9800fc_Rx();
    Rte_COMCbkRxTOut_ExtraBBCraneStatus_ISig_4_oVMCU_BB2_54P_oBackbone2_bd0882a3_Rx();
    Rte_COMCbkRxTOut_ExtraBBSlidable5thWheelStatus_oVMCU_BB2_54P_oBackbone2_723450b2_Rx();
    Rte_COMCbkRxTOut_ExtraBBTailLiftStatus_ISig_4_oVMCU_BB2_54P_oBackbone2_0401d03c_Rx();
    Rte_COMCbkRxTOut_ExtraSideMarkers_FunctStat_oVMCU_BB2_73P_oBackbone2_34f556e9_Rx();
    Rte_COMCbkRxTOut_FCW_Status_ISig_4_oDACU_BB2_02P_oBackbone2_da223606_Rx();
    Rte_COMCbkRxTOut_FSP1ResponseErrorL1_oFSP1_Frame_L1_oLIN00_b9e1fc6c_Rx();
    Rte_COMCbkRxTOut_FSP1ResponseErrorL2_oFSP1_Frame_L2_oLIN01_368cf224_Rx();
    Rte_COMCbkRxTOut_FSP1ResponseErrorL3_oFSP1_Frame_L3_oLIN02_f85c49d4_Rx();
    Rte_COMCbkRxTOut_FSP1ResponseErrorL4_oFSP1_Frame_L4_oLIN03_f327e8f5_Rx();
    Rte_COMCbkRxTOut_FSP1ResponseErrorL5_oFSP1_Frame_L5_oLIN04_3a9a971c_Rx();
    Rte_COMCbkRxTOut_FSP1SwitchStatusL1_oFSP1_Frame_L1_oLIN00_e78b81b2_Rx();
    Rte_COMCbkRxTOut_FSP1SwitchStatusL2_oFSP1_Frame_L2_oLIN01_68e68ffa_Rx();
    Rte_COMCbkRxTOut_FSP1SwitchStatusL3_oFSP1_Frame_L3_oLIN02_a636340a_Rx();
    Rte_COMCbkRxTOut_FSP1SwitchStatusL4_oFSP1_Frame_L4_oLIN03_ad4d952b_Rx();
    Rte_COMCbkRxTOut_FSP1SwitchStatusL5_oFSP1_Frame_L5_oLIN04_64f0eac2_Rx();
    Rte_COMCbkRxTOut_FSP2ResponseErrorL1_oFSP2_Frame_L1_oLIN00_a793a78f_Rx();
    Rte_COMCbkRxTOut_FSP2ResponseErrorL2_oFSP2_Frame_L2_oLIN01_28fea9c7_Rx();
    Rte_COMCbkRxTOut_FSP2ResponseErrorL3_oFSP2_Frame_L3_oLIN02_e62e1237_Rx();
    Rte_COMCbkRxTOut_FSP2SwitchStatusL1_oFSP2_Frame_L1_oLIN00_ee70e556_Rx();
    Rte_COMCbkRxTOut_FSP2SwitchStatusL2_oFSP2_Frame_L2_oLIN01_611deb1e_Rx();
    Rte_COMCbkRxTOut_FSP2SwitchStatusL3_oFSP2_Frame_L3_oLIN02_afcd50ee_Rx();
    Rte_COMCbkRxTOut_FSP3ResponseErrorL2_oFSP3_Frame_L2_oLIN01_22d09f66_Rx();
    Rte_COMCbkRxTOut_FSP3SwitchStatusL2_oFSP3_Frame_L2_oLIN01_664b3742_Rx();
    Rte_COMCbkRxTOut_FSP4ResponseErrorL2_oFSP4_Frame_L2_oLIN01_141a1e01_Rx();
    Rte_COMCbkRxTOut_FSP4SwitchStatusL2_oFSP4_Frame_L2_oLIN01_72eb22d6_Rx();
    Rte_COMCbkRxTOut_FerryFunctionStatus_ISig_4_oVMCU_BB2_20P_oBackbone2_5e04e3f4_Rx();
    Rte_COMCbkRxTOut_FerryFunctionSwitchChangeACK_ISig_4_oVMCU_BB2_01P_oBackbone2_71f009e5_Rx();
    Rte_COMCbkRxTOut_FrontFog_Indication_oVMCU_BB2_03P_oBackbone2_92113a12_Rx();
    Rte_COMCbkRxTOut_FrtAxleHydroActive_Status_oVMCU_BB2_20P_oBackbone2_280ed8ff_Rx();
    Rte_COMCbkRxTOut_FuelLevel_ISig_3_oVMCU_BB1_03P_oBackbone1J1939_0f23d9f2_Rx();
    Rte_COMCbkRxTOut_GearBoxUnlockAuth_rqst_oTECU_BB2_01P_oBackbone2_d45ce9c0_Rx();
    Rte_COMCbkRxTOut_GrossCombinationVehicleWeight_ISig_3_oCVW_X_EBS_oBackbone1J1939_81d6bcd8_Rx();
    Rte_COMCbkRxTOut_HandlingInformation_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_8cdf696e_Rx();
    Rte_COMCbkRxTOut_HeightAdjustmentAllowed_ISig_4_oVMCU_BB2_02P_oBackbone2_3a236070_Rx();
    Rte_COMCbkRxTOut_HighResEngineTotalFuelUsed_ISig_3_oHRLFC_X_EMS_oBackbone1J1939_9a2f5b32_Rx();
    Rte_COMCbkRxTOut_InstantaneousFuelEconomy_ISig_3_oLFE_X_EMS_oBackbone1J1939_a1c216ba_Rx();
    Rte_COMCbkRxTOut_InteriorLightLevelInd_cmd_oVMCU_BB2_03P_oBackbone2_3f259764_Rx();
    Rte_COMCbkRxTOut_KeyAuthentication_rqst_oVMCU_BB2_04P_oBackbone2_c5ca7983_Rx();
    Rte_COMCbkRxTOut_KeyPosition_ISig_4_oVMCU_BB2_01P_oBackbone2_666d9433_Rx();
    Rte_COMCbkRxTOut_KeyfobInCabPresencePS_rqst_oVMCU_BB2_82P_oBackbone2_c182d016_Rx();
    Rte_COMCbkRxTOut_KneelingStatusHMI_ISig_4_oVMCU_BB2_20P_oBackbone2_755dcb67_Rx();
    Rte_COMCbkRxTOut_LCSSystemStatus_oDACU_BB2_02P_oBackbone2_a47552fc_Rx();
    Rte_COMCbkRxTOut_LIN_BunkH2PowerWinCloseDSBtn_s_oLECM2toCIOM_FR2_L1_oLIN00_9fa2715d_Rx();
    Rte_COMCbkRxTOut_LIN_BunkH2PowerWinClosePSBtn_s_oLECM2toCIOM_FR2_L1_oLIN00_281d2c26_Rx();
    Rte_COMCbkRxTOut_LIN_BunkH2PowerWinOpenDSBtn_st_oLECM2toCIOM_FR2_L1_oLIN00_0df6d397_Rx();
    Rte_COMCbkRxTOut_LIN_BunkH2PowerWinOpenPSBtn_st_oLECM2toCIOM_FR2_L1_oLIN00_ca96c47e_Rx();
    Rte_COMCbkRxTOut_LKSApplicationStatus_ISig_4_oDACU_BB2_02P_oBackbone2_a1929cc2_Rx();
    Rte_COMCbkRxTOut_LKSCorrectiveSteeringStatus_ISig_4_oDACU_BB2_02P_oBackbone2_5bf2e494_Rx();
    Rte_COMCbkRxTOut_LevelControlInformation_ISig_4_oVMCU_BB2_02P_oBackbone2_c3136e85_Rx();
    Rte_COMCbkRxTOut_LiftAxle1PositionStatus_ISig_4_oVMCU_BB2_02P_oBackbone2_8f87d670_Rx();
    Rte_COMCbkRxTOut_LiftAxle1UpRequestACK_ISig_4_oVMCU_BB2_02P_oBackbone2_fa0577fd_Rx();
    Rte_COMCbkRxTOut_LiftAxle2UpRequestACK_ISig_4_oVMCU_BB2_02P_oBackbone2_c5ce0968_Rx();
    Rte_COMCbkRxTOut_LngTank1RemainingGasVolume_ISig_3_oACM_BB1_01P_oBackbone1J1939_8a2851cd_Rx();
    Rte_COMCbkRxTOut_LngTank2RemainingGasVolume_ISig_3_oACM_BB1_01P_oBackbone1J1939_ec2c84fd_Rx();
    Rte_COMCbkRxTOut_LoadDistributionALDChangeACK_ISig_4_oVMCU_BB2_54P_oBackbone2_0286104b_Rx();
    Rte_COMCbkRxTOut_LoadDistributionChangeACK_ISig_4_oVMCU_BB2_02P_oBackbone2_3f2946ee_Rx();
    Rte_COMCbkRxTOut_LoadDistributionFuncSelected_ISig_4_oVMCU_BB2_02P_oBackbone2_e30d445f_Rx();
    Rte_COMCbkRxTOut_LoadDistributionRequestedACK_ISig_4_oVMCU_BB2_52P_oBackbone2_27f055a3_Rx();
    Rte_COMCbkRxTOut_LoadDistributionSelected_ISig_4_oVMCU_BB2_02P_oBackbone2_91087134_Rx();
    Rte_COMCbkRxTOut_MinutesUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_b5b3e478_Rx();
    Rte_COMCbkRxTOut_MirrorHeat_rqst_ddm_oDDM_Sec_01P_oSecuritySubnet_f2c6892d_Rx();
    Rte_COMCbkRxTOut_MirrorHeatingMode_ISig_4_oVMCU_BB2_03P_oBackbone2_93dcd786_Rx();
    Rte_COMCbkRxTOut_ParkingLight_Indication_oVMCU_BB2_03P_oBackbone2_e64048d9_Rx();
    Rte_COMCbkRxTOut_PassengerDoorAjar_stat_ISig_10_oPDM_Sec_01P_oSecuritySubnet_15fc3894_Rx();
    Rte_COMCbkRxTOut_PassengerDoorLatch_stat_oPDM_Sec_01P_oSecuritySubnet_7ad7cbe3_Rx();
    Rte_COMCbkRxTOut_PhoneButtonIndication_cmd_oHMIIOM_BB2_07P_oBackbone2_1c062854_Rx();
    Rte_COMCbkRxTOut_PinCodeEntered_value_oHMIIOM_BB2_09P_oBackbone2_7f89cada_Rx();
    Rte_COMCbkRxTOut_PloughLampModeStatus_ISig_4_oBBM_BB2_01P_oBackbone2_ce1fdf4f_Rx();
    Rte_COMCbkRxTOut_PredeliveryModeIndication_cmd_ISig_4_oVMCU_BB2_04P_oBackbone2_eefcbaa0_Rx();
    Rte_COMCbkRxTOut_Pto1Status_ISig_4_oVMCU_BB2_07P_oBackbone2_c7aca240_Rx();
    Rte_COMCbkRxTOut_Pto2Indication_oVMCU_BB2_07P_oBackbone2_26780e74_Rx();
    Rte_COMCbkRxTOut_Pto2Status_ISig_4_oVMCU_BB2_07P_oBackbone2_1437bebb_Rx();
    Rte_COMCbkRxTOut_Pto4Status_ISig_4_oVMCU_BB2_07P_oBackbone2_6870810c_Rx();
    Rte_COMCbkRxTOut_PtosStatus_ISig_4_oVMCU_BB2_07P_oBackbone2_9464094a_Rx();
    Rte_COMCbkRxTOut_RSLMgrRptTRSLEnabledOpStat_oVMCU_BB2_08P_oBackbone2_748e4c57_Rx();
    Rte_COMCbkRxTOut_RampLevelRequestACK_ISig_4_oVMCU_BB2_07P_oBackbone2_a790dd8f_Rx();
    Rte_COMCbkRxTOut_RampLevelStorageAck_ISig_4_oVMCU_BB2_02P_oBackbone2_4026e627_Rx();
    Rte_COMCbkRxTOut_RearAxleSteeringFunctionStatus_oEBS_BB1_02P_oBackbone1J1939_101ad651_Rx();
    Rte_COMCbkRxTOut_RearFog_Indication_oVMCU_BB2_03P_oBackbone2_fa31dca4_Rx();
    Rte_COMCbkRxTOut_RegenerationIndication_oHMIIOM_BB2_24P_oBackbone2_777f5711_Rx();
    Rte_COMCbkRxTOut_Regeneration_DeviceInd_oHMIIOM_BB2_24P_oBackbone2_86c1e4d1_Rx();
    Rte_COMCbkRxTOut_ResponseErrorCCFW_oCCFWtoCIOM_L4_oLIN03_38ff59f8_Rx();
    Rte_COMCbkRxTOut_ResponseErrorDLFW_oDLFWtoCIOM_L4_oLIN03_a0bc7d9c_Rx();
    Rte_COMCbkRxTOut_ResponseErrorELCP1_oELCP1toCIOM_L4_oLIN03_a9bf457f_Rx();
    Rte_COMCbkRxTOut_ResponseErrorELCP2_oELCP2toCIOM_L4_oLIN03_a23bb7d3_Rx();
    Rte_COMCbkRxTOut_ResponseErrorILCP1_oILCP1toCIOM_L1_oLIN00_44250bf0_Rx();
    Rte_COMCbkRxTOut_ResponseErrorILCP2_oILCP2toCIOM_L4_oLIN03_9e48a682_Rx();
    Rte_COMCbkRxTOut_ResponseErrorLECM2_oLECM2toCIOM_FR1_L1_oLIN00_ed8050df_Rx();
    Rte_COMCbkRxTOut_ResponseErrorLECMBasic_oLECMBasic2CIOM_L1_oLIN00_9a0b7e5e_Rx();
    Rte_COMCbkRxTOut_ResponseErrorRCECS_oRCECStoCIOM_L5_oLIN04_07d94fb4_Rx();
    Rte_COMCbkRxTOut_RetarderTorqueMode_ISig_3_oERC1_X_RECU_oBackbone1J1939_68c292d8_Rx();
    Rte_COMCbkRxTOut_ReverseGearEngaged_ISig_4_oVMCU_BB2_01P_oBackbone2_ea2f14fc_Rx();
    Rte_COMCbkRxTOut_RideHeightStorageAck_ISig_4_oVMCU_BB2_02P_oBackbone2_af65dfcb_Rx();
    Rte_COMCbkRxTOut_SG_AlmClkCurAlarm_stat_sg_oHMIIOM_BB2_10P_oBackbone2_16a10c0a_Rx();
    Rte_COMCbkRxTOut_SG_BnkH1IntLghtMMenu_stat_sg_oLECM1_Cab_02P_oCabSubnet_72949da5_Rx();
    Rte_COMCbkRxTOut_SG_FMS1_sg_ISig_3_oFMS1_X_HMIIOM_oBackbone1J1939_a2ca12fb_Rx();
    Rte_COMCbkRxTOut_SG_FPBRMMIStat_sg_ISig_4_oVMCU_BB2_74P_oBackbone2_6ccb74d8_Rx();
    Rte_COMCbkRxTOut_SG_IntLightMode_CoreRqst_sg_oVMCU_BB2_03P_oBackbone2_fb45020d_Rx();
    Rte_COMCbkRxTOut_SG_LIN_BunkH2PHTi_rqs_sg_oLECM2toCIOM_FR3_L1_oLIN00_0bf5fe80_Rx();
    Rte_COMCbkRxTOut_SG_OilPrediction_sg_oEMS_BB2_06P_oBackbone2_f16954dd_Rx();
    Rte_COMCbkRxTOut_SWSpdCtrlButtonsStatus1_oHMIIOM_BB2_01P_oBackbone2_f235f469_Rx();
    Rte_COMCbkRxTOut_SWSpeedControlAdjustMode_oHMIIOM_BB2_01P_oBackbone2_4c33da86_Rx();
    Rte_COMCbkRxTOut_SecondsUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_5a9bc5ac_Rx();
    Rte_COMCbkRxTOut_ServiceBrakeAirPrsCircuit1_ISig_3_oAIR1_X_VMCU_oBackbone1J1939_e8caeb17_Rx();
    Rte_COMCbkRxTOut_ServiceBrakeAirPrsCircuit2_ISig_3_oAIR1_X_VMCU_oBackbone1J1939_b2abd477_Rx();
    Rte_COMCbkRxTOut_SpecialLightSituation_ind_oVMCU_BB2_52P_oBackbone2_bb95932a_Rx();
    Rte_COMCbkRxTOut_SpeedLockingInhibition_stat_oHMIIOM_BB2_08P_oBackbone2_0bfbe2ed_Rx();
    Rte_COMCbkRxTOut_StopLevelChangeAck_ISig_4_oVMCU_BB2_02P_oBackbone2_5eeeb44a_Rx();
    Rte_COMCbkRxTOut_Synch_Unsynch_Mode_stat_oHMIIOM_BB2_08P_oBackbone2_8a425ab2_Rx();
    Rte_COMCbkRxTOut_SystemEvent_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_5a5c5e53_Rx();
    Rte_COMCbkRxTOut_TachographPerformance_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_1bf17036_Rx();
    Rte_COMCbkRxTOut_TachographVehicleSpeed_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_78a7f516_Rx();
    Rte_COMCbkRxTOut_TotalVehicleDistanceHighRes_ISig_3_oVDHR_X_VMCU_oBackbone1J1939_6b802705_Rx();
    Rte_COMCbkRxTOut_TrailerBodyLampDI_stat_oVMCU_BB2_80P_oBackbone2_47992f30_Rx();
    Rte_COMCbkRxTOut_TrailerBodyLampFdbk_stat_oVMCU_BB2_80P_oBackbone2_fa358fbc_Rx();
    Rte_COMCbkRxTOut_TransferCaseNeutral_Ack_oVMCU_BB2_73P_oBackbone2_3029afb8_Rx();
    Rte_COMCbkRxTOut_TransferCaseNeutral_status_oVMCU_BB2_29P_oBackbone2_0936e4a4_Rx();
    Rte_COMCbkRxTOut_TransferCasePTOEngaged_ISig_4_oVMCU_BB2_29P_oBackbone2_9e9887d1_Rx();
    Rte_COMCbkRxTOut_VehFrontAxle1CalibratedLoad_ISig_4_oHMIIOM_BB2_11P_oBackbone2_b768cee7_Rx();
    Rte_COMCbkRxTOut_VehRearAxle1CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_6732f367_Rx();
    Rte_COMCbkRxTOut_VehRearAxle2CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_2dab8413_Rx();
    Rte_COMCbkRxTOut_VehRearAxle3CalibratedLoad_ISig_4_oHMIIOM_BB2_12P_oBackbone2_a2f3ab00_Rx();
    Rte_COMCbkRxTOut_VehicleMode_ISig_3_oVMCU_BB1_01P_oBackbone1J1939_997c94c6_Rx();
    Rte_COMCbkRxTOut_VehicleMotion_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_c4c069be_Rx();
    Rte_COMCbkRxTOut_VehicleOverspeed_ISig_3_oTCO1_X_TACHO_oBackbone1J1939_b3d80892_Rx();
    Rte_COMCbkRxTOut_WRC5thWheelRequest_oWRCS_Cab_02P_oCabSubnet_6e07aa44_Rx();
    Rte_COMCbkRxTOut_WRCAirSuspensionStopRequest_oWRCS_Cab_01P_oCabSubnet_18c85d80_Rx();
    Rte_COMCbkRxTOut_WRCAux1Request_oWRCS_Cab_01P_oCabSubnet_3d62fac1_Rx();
    Rte_COMCbkRxTOut_WRCAux2Request_oWRCS_Cab_01P_oCabSubnet_66754bd4_Rx();
    Rte_COMCbkRxTOut_WRCAux3Request_oWRCS_Cab_01P_oCabSubnet_5087db27_Rx();
    Rte_COMCbkRxTOut_WRCAux4Request_oWRCS_Cab_01P_oCabSubnet_d05a29fe_Rx();
    Rte_COMCbkRxTOut_WRCAux5Request_oWRCS_Cab_01P_oCabSubnet_e6a8b90d_Rx();
    Rte_COMCbkRxTOut_WRCAux6Request_oWRCS_Cab_01P_oCabSubnet_bdbf0818_Rx();
    Rte_COMCbkRxTOut_WRCBeaconRequest_oWRCS_Cab_02P_oCabSubnet_7ba25fea_Rx();
    Rte_COMCbkRxTOut_WRCCabBodyWLightsRqst_oWRCS_Cab_02P_oCabSubnet_b2a0874a_Rx();
    Rte_COMCbkRxTOut_WRCCraneRequest_oWRCS_Cab_01P_oCabSubnet_95c4579e_Rx();
    Rte_COMCbkRxTOut_WRCLevelAdjustmentAction_oWRCS_Cab_01P_oCabSubnet_810159ff_Rx();
    Rte_COMCbkRxTOut_WRCLevelAdjustmentAxles_oWRCS_Cab_01P_oCabSubnet_b1b2524c_Rx();
    Rte_COMCbkRxTOut_WRCLevelAdjustmentStroke_oWRCS_Cab_01P_oCabSubnet_e59ce5a1_Rx();
    Rte_COMCbkRxTOut_WRCLevelUserMemoryAction_oWRCS_Cab_02P_oCabSubnet_42c76a61_Rx();
    Rte_COMCbkRxTOut_WRCLevelUserMemory_oWRCS_Cab_02P_oCabSubnet_d79fc958_Rx();
    Rte_COMCbkRxTOut_WRCLockButtonStatus_oWRCS_Cab_01P_oCabSubnet_762ddecb_Rx();
    Rte_COMCbkRxTOut_WRCRollRequest_oWRCS_Cab_01P_oCabSubnet_47562199_Rx();
    Rte_COMCbkRxTOut_WRCTailLiftRequest_oWRCS_Cab_01P_oCabSubnet_a1fd4993_Rx();
    Rte_COMCbkRxTOut_WRCUnlockButtonStatus_oWRCS_Cab_01P_oCabSubnet_fd05435c_Rx();
    Rte_COMCbkRxTOut_WheelBasedVehicleSpeed_ISig_3_oCCVS_X_VMCU_oBackbone1J1939_e3ff6d4c_Rx();
    Rte_COMCbkRxTOut_WrcEngSpdCtrlDecreaseButtStat_oWRCS_Cab_02P_oCabSubnet_f90375a0_Rx();
    Rte_COMCbkRxTOut_WrcEngSpdCtrlEnableStatus_oWRCS_Cab_02P_oCabSubnet_c15f6325_Rx();
    Rte_COMCbkRxTOut_WrcEngSpdCtrlIncreaseButtStat_oWRCS_Cab_02P_oCabSubnet_7a1f067c_Rx();
    Rte_COMCbkRxTOut_WrcEngSpdCtrlResumeButtonStat_oWRCS_Cab_02P_oCabSubnet_09c53237_Rx();
    Rte_COMCbkRxTOut_WrcPto1ButtonStatus_oWRCS_Cab_01P_oCabSubnet_9b7a6ac6_Rx();
    Rte_COMCbkRxTOut_WrcPto2ButtonStatus_oWRCS_Cab_01P_oCabSubnet_d6926aa1_Rx();
    Rte_COMCbkRxTOut_WrcPto3ButtonStatus_oWRCS_Cab_01P_oCabSubnet_5b1a9743_Rx();
    Rte_COMCbkRxTOut_WrcPto4ButtonStatus_oWRCS_Cab_01P_oCabSubnet_4d426a6f_Rx();
    Rte_COMCbkRxTOut_XRSLStates_oVMCU_BB2_51P_oBackbone2_06570c1b_Rx();

    /**********************************************************************************************************************
     * NvM-Callback for synchronous copying of the mirror buffer to and from the NvM
     *********************************************************************************************************************/
    (void)Rte_SetMirror_Application_Data_NVM_DriverAuth2_Ctrl_NVM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_DriverAuth2_Ctrl_NVM(buffer);
    (void)Rte_NvMNotifyJobFinished_Application_Data_NVM_DriverAuth2_Ctrl_NVM(0, 0);

    (void)Rte_SetMirror_Application_Data_NVM_EngTraceHW_NvM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_EngTraceHW_NvM(buffer);
    (void)Rte_NvMNotifyJobFinished_Application_Data_NVM_EngTraceHW_NvM(0, 0);

    (void)Rte_SetMirror_Application_Data_NVM_InteriorLights_HMICtrl_NVM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_InteriorLights_HMICtrl_NVM(buffer);

    (void)Rte_SetMirror_Application_Data_NVM_MUT_UICtrl_Difflock_NVM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_MUT_UICtrl_Difflock_NVM(buffer);
    (void)Rte_NvMNotifyJobFinished_Application_Data_NVM_MUT_UICtrl_Difflock_NVM(0, 0);

    (void)Rte_SetMirror_Application_Data_NVM_MUT_UICtrl_Traction_NVM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_MUT_UICtrl_Traction_NVM(buffer);
    (void)Rte_NvMNotifyJobFinished_Application_Data_NVM_MUT_UICtrl_Traction_NVM(0, 0);

    (void)Rte_SetMirror_Application_Data_NVM_PinCode_ctrl_NVM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_PinCode_ctrl_NVM(buffer);
    (void)Rte_NvMNotifyJobFinished_Application_Data_NVM_PinCode_ctrl_NVM(0, 0);

    (void)Rte_SetMirror_Application_Data_NVM_RGW_HMICtrl_NVM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_RGW_HMICtrl_NVM(buffer);
    (void)Rte_NvMNotifyJobFinished_Application_Data_NVM_RGW_HMICtrl_NVM(0, 0);

    (void)Rte_SetMirror_Application_Data_NVM_SCM_HMICtrl_NVM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_SCM_HMICtrl_NVM(buffer);
    (void)Rte_NvMNotifyJobFinished_Application_Data_NVM_SCM_HMICtrl_NVM(0, 0);

    (void)Rte_SetMirror_Application_Data_NVM_VehicleAccess_Ctrl_NVM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_VehicleAccess_Ctrl_NVM(buffer);
    (void)Rte_NvMNotifyJobFinished_Application_Data_NVM_VehicleAccess_Ctrl_NVM(0, 0);

    (void)Rte_SetMirror_Application_Data_NVM_VehicleModeDistribution_NVM(buffer);
    (void)Rte_GetMirror_Application_Data_NVM_VehicleModeDistribution_NVM(buffer);
    (void)Rte_NvMNotifyJobFinished_Application_Data_NVM_VehicleModeDistribution_NVM(0, 0);

    (void)Rte_SetMirror_Calibration_Data_NVM_FSP_NVM(buffer);
    (void)Rte_GetMirror_Calibration_Data_NVM_FSP_NVM(buffer);
    (void)Rte_NvMNotifyJobFinished_Calibration_Data_NVM_FSP_NVM(0, 0);

    (void)Rte_SetMirror_Keyfob_Radio_Com_NVM_KeyFobNVBlock(buffer);
    (void)Rte_GetMirror_Keyfob_Radio_Com_NVM_KeyFobNVBlock(buffer);
    (void)Rte_NvMNotifyJobFinished_Keyfob_Radio_Com_NVM_KeyFobNVBlock(0, 0);


} /* PRQA S 6050 */ /* MD_MSR_STCAL */


/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_0883:  MISRA rule: 19.15
     Reason:     AUTOSAR SWS Memory Mapping requires inclusion of MemMap.h multiple times in a file in order to
                 select appropriate #pragma directives.
     Risk:       MemMap.h is provided by the integrator, hence many risks may occur, caused by wrong implementation of this file.
     Prevention: The integrator strictly has to adhere to the definitions of the AUTOSAR SWS Memory Mapping. Extensions to
                 the file not described in the SWS may not be put into MemMap.h. This has to be verified by code inspection.

*/
