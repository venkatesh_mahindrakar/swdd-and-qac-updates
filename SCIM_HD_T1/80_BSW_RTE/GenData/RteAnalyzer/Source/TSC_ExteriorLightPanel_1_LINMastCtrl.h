/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ExteriorLightPanel_1_LINMastCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DaytimeRunningLight_Indication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DiagInfoELCP1_DiagInfo(DiagInfo_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DrivingLightPlus_Indication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DrivingLight_Indication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_FrontFog_Indication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_BackLightDimming_Status_Thumbwheel_stat(Thumbwheel_stat_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat(Thumbwheel_stat_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_ParkingLight_Indication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_RearFog_Indication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_ResponseErrorELCP1_ResponseErrorELCP1(ResponseErrorELCP1_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_BackLightDimming_Status_Thumbwheel_stat(Thumbwheel_stat_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_BlackPanelMode_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_FogLightFront_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_FogLightRear_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DaytimeRunningLight_Indica_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DrivingLightPlus_Indicatio_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DrivingLight_Indication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_FrontFog_Indication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_ParkingLight_Indication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_RearFog_Indication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LevelingThumbwheel_stat_Thumbwheel_stat(Thumbwheel_stat_T data);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T data);

/** Sender receiver - update flag */
boolean TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status(void);

/** Service interfaces */
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_44_ELCP_RAM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_DimmingAdjustment1_ActivateIss(void);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_DimmingAdjustment1_DeactivateIss(void);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest1_ActivateIss(void);
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest1_DeactivateIss(void);

/** Explicit inter-runnable variables */
uint8 TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvRead_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl(void);
void TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP1LinCtrl(uint8);
void TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP1LinCtrl(uint8);
uint8 TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvRead_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(void);
void TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(uint8);

/** Calibration Component Calibration Parameters */
boolean  TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Prm_P1VR3_ELCP1_Installed_v(void);




