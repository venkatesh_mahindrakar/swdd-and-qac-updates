/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Horn_ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_Horn_ctrl_Rte_Read_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst(OffOn_T *data);
Std_ReturnType TSC_Horn_ctrl_Rte_Read_CityHorn_HMI_rqst_CityHorn_HMI_rqst(OffOn_T *data);
Std_ReturnType TSC_Horn_ctrl_Rte_Read_CtaBB_Horn_rqst_CtaBB_Horn_rqst(OffOn_T *data);
Std_ReturnType TSC_Horn_ctrl_Rte_Read_PanicAlarmFromKeyfob_rqst_PanicAlarmFromKeyfob_rqst(Request_T *data);
Std_ReturnType TSC_Horn_ctrl_Rte_Read_SwcActivation_Parked_Parked(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_Horn_ctrl_Rte_Write_AuxiliaryHorn_cmd_AuxiliaryHorn_cmd(OffOn_T data);
Std_ReturnType TSC_Horn_ctrl_Rte_Write_CityHorn_cmd_CityHorn_cmd(OffOn_T data);

/** Calibration Component Calibration Parameters */
boolean  TSC_Horn_ctrl_Rte_Prm_P1A1T_CityHorn_Act_v(void);




