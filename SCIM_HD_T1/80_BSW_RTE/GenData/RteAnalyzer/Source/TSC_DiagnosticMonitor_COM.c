/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DiagnosticMonitor_COM.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DiagnosticMonitor_COM.h"
#include "TSC_DiagnosticMonitor_COM.h"















     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DiagnosticMonitor_COM_Rte_Call_Event_D1A6D_88_BB2Net_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1A6D_88_BB2Net_ComFail_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_COM_Rte_Call_Event_D1A6E_88_BB1Net_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1A6E_88_BB1Net_ComFail_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_COM_Rte_Call_Event_D1A6F_88_CabNet_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1A6F_88_CabNet_ComFail_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_COM_Rte_Call_Event_D1A6H_88_SecurityNet_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1A6H_88_SecurityNet_ComFail_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_COM_Rte_Call_Event_D1A6L_88_FMSNet_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1A6L_88_FMSNet_ComFail_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_COM_Rte_Call_Event_D1E4Q_88_CAN6_ComFail_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1E4Q_88_CAN6_ComFail_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */
uint8 TSC_DiagnosticMonitor_COM_Rte_Mode_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
{
  return Rte_Mode_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
}
uint8 TSC_DiagnosticMonitor_COM_Rte_Mode_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
{
  return Rte_Mode_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
}
uint8 TSC_DiagnosticMonitor_COM_Rte_Mode_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
{
  return Rte_Mode_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
}
uint8 TSC_DiagnosticMonitor_COM_Rte_Mode_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
{
  return Rte_Mode_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
}
uint8 TSC_DiagnosticMonitor_COM_Rte_Mode_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
{
  return Rte_Mode_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
}
uint8 TSC_DiagnosticMonitor_COM_Rte_Mode_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void)
{
  return Rte_Mode_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff();
}




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





boolean  TSC_DiagnosticMonitor_COM_Rte_Prm_P1DXX_FMSgateway_Act_v(void)
{
  return (boolean ) Rte_Prm_P1DXX_FMSgateway_Act_v();
}
boolean  TSC_DiagnosticMonitor_COM_Rte_Prm_P1V8I_BB2_active_v(void)
{
  return (boolean ) Rte_Prm_P1V8I_BB2_active_v();
}
boolean  TSC_DiagnosticMonitor_COM_Rte_Prm_P1V8J_BB1_active_v(void)
{
  return (boolean ) Rte_Prm_P1V8J_BB1_active_v();
}
boolean  TSC_DiagnosticMonitor_COM_Rte_Prm_P1V8K_CabSubnet_active_v(void)
{
  return (boolean ) Rte_Prm_P1V8K_CabSubnet_active_v();
}
boolean  TSC_DiagnosticMonitor_COM_Rte_Prm_P1V8L_SecuritySubnet_active_v(void)
{
  return (boolean ) Rte_Prm_P1V8L_SecuritySubnet_active_v();
}
boolean  TSC_DiagnosticMonitor_COM_Rte_Prm_P1V8M_CAN6_active_v(void)
{
  return (boolean ) Rte_Prm_P1V8M_CAN6_active_v();
}


     /* DiagnosticMonitor_COM */
      /* DiagnosticMonitor_COM */



