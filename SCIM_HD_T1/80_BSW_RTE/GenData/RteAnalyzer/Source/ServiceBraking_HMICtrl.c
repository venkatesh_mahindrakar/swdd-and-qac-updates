/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  ServiceBraking_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  ServiceBraking_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <ServiceBraking_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_ABS_Inhibit_SwType_P1SY6_T
 *   
 *
 * Speed16bit_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_ServiceBraking_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_ServiceBraking_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void ServiceBraking_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ABS_Inhibit_SwType_P1SY6_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * Inhibit_T: Enumeration of integer in interval [0...3] with enumerators
 *   Inhibit_NoInhibit (0U)
 *   Inhibit_InhibitActive (1U)
 *   Inhibit_Error (2U)
 *   Inhibit_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PassiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   PassiveActive_Passive (0U)
 *   PassiveActive_Active (1U)
 *   PassiveActive_Error (2U)
 *   PassiveActive_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ABS_Inhibit_SwType_P1SY6_T Rte_Prm_P1SY6_ABS_Inhibit_SwType_v(void)
 *   boolean Rte_Prm_P1A1R_HSA_Installed_v(void)
 *   boolean Rte_Prm_P1NTV_HSA_DefaultConfig_v(void)
 *   boolean Rte_Prm_P1SY4_ABS_Inhibit_Installed_v(void)
 *
 *********************************************************************************************************************/


#define ServiceBraking_HMICtrl_START_SEC_CODE
#include "ServiceBraking_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SB_ABS_Inhibit_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ABSInhibitionStatus_ABSInhibitionStatus(Inhibit_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest(OffOn_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SB_ABS_Inhibit_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ServiceBraking_HMICtrl_CODE) SB_ABS_Inhibit_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SB_ABS_Inhibit_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PushButtonStatus_T Read_ABSInhibitSwitchStatus_PushButtonStatus;
  Inhibit_T Read_ABSInhibitionStatus_ABSInhibitionStatus;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;
  Speed16bit_T Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed;

  SEWS_ABS_Inhibit_SwType_P1SY6_T P1SY6_ABS_Inhibit_SwType_v_data;
  boolean P1A1R_HSA_Installed_v_data;
  boolean P1NTV_HSA_DefaultConfig_v_data;
  boolean P1SY4_ABS_Inhibit_Installed_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1SY6_ABS_Inhibit_SwType_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY6_ABS_Inhibit_SwType_v();
  P1A1R_HSA_Installed_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1A1R_HSA_Installed_v();
  P1NTV_HSA_DefaultConfig_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1NTV_HSA_DefaultConfig_v();
  P1SY4_ABS_Inhibit_Installed_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY4_ABS_Inhibit_Installed_v();

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus(&Read_ABSInhibitSwitchStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_ABSInhibitionStatus_ABSInhibitionStatus(&Read_ABSInhibitionStatus_ABSInhibitionStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(&Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest(Rte_InitValue_ABSInhibitionRequest_ABSInhibitionRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  ServiceBraking_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SB_HSA_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch(PassiveActive_T *data)
 *   Std_ReturnType Rte_Read_HillStartAidButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_HSADriverRequest_HSADriverRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_HillStartAid_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SB_HSA_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ServiceBraking_HMICtrl_CODE) SB_HSA_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SB_HSA_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PassiveActive_T Read_ASRHillHolderSwitch_ASRHillHolderSwitch;
  PushButtonStatus_T Read_HillStartAidButtonStatus_PushButtonStatus;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;

  SEWS_ABS_Inhibit_SwType_P1SY6_T P1SY6_ABS_Inhibit_SwType_v_data;
  boolean P1A1R_HSA_Installed_v_data;
  boolean P1NTV_HSA_DefaultConfig_v_data;
  boolean P1SY4_ABS_Inhibit_Installed_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1SY6_ABS_Inhibit_SwType_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY6_ABS_Inhibit_SwType_v();
  P1A1R_HSA_Installed_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1A1R_HSA_Installed_v();
  P1NTV_HSA_DefaultConfig_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1NTV_HSA_DefaultConfig_v();
  P1SY4_ABS_Inhibit_Installed_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY4_ABS_Inhibit_Installed_v();

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch(&Read_ASRHillHolderSwitch_ASRHillHolderSwitch);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_HillStartAidButtonStatus_PushButtonStatus(&Read_HillStartAidButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Write_HSADriverRequest_HSADriverRequest(Rte_InitValue_HSADriverRequest_HSADriverRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Write_HillStartAid_DeviceIndication_DeviceIndication(Rte_InitValue_HillStartAid_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ServiceBraking_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ABSInhibitionStatus_ABSInhibitionStatus(Inhibit_T *data)
 *   Std_ReturnType Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch(PassiveActive_T *data)
 *   Std_ReturnType Rte_Read_HillStartAidButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_HSADriverRequest_HSADriverRequest(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_HillStartAid_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ServiceBraking_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ServiceBraking_HMICtrl_CODE) ServiceBraking_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ServiceBraking_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PushButtonStatus_T Read_ABSInhibitSwitchStatus_PushButtonStatus;
  Inhibit_T Read_ABSInhibitionStatus_ABSInhibitionStatus;
  PassiveActive_T Read_ASRHillHolderSwitch_ASRHillHolderSwitch;
  PushButtonStatus_T Read_HillStartAidButtonStatus_PushButtonStatus;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;
  Speed16bit_T Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed;

  SEWS_ABS_Inhibit_SwType_P1SY6_T P1SY6_ABS_Inhibit_SwType_v_data;
  boolean P1A1R_HSA_Installed_v_data;
  boolean P1NTV_HSA_DefaultConfig_v_data;
  boolean P1SY4_ABS_Inhibit_Installed_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1SY6_ABS_Inhibit_SwType_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY6_ABS_Inhibit_SwType_v();
  P1A1R_HSA_Installed_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1A1R_HSA_Installed_v();
  P1NTV_HSA_DefaultConfig_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1NTV_HSA_DefaultConfig_v();
  P1SY4_ABS_Inhibit_Installed_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY4_ABS_Inhibit_Installed_v();

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_ABSInhibitSwitchStatus_PushButtonStatus(&Read_ABSInhibitSwitchStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_ABSInhibitionStatus_ABSInhibitionStatus(&Read_ABSInhibitionStatus_ABSInhibitionStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_ASRHillHolderSwitch_ASRHillHolderSwitch(&Read_ASRHillHolderSwitch_ASRHillHolderSwitch);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_HillStartAidButtonStatus_PushButtonStatus(&Read_HillStartAidButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(&Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Write_ABSInhibitionRequest_ABSInhibitionRequest(Rte_InitValue_ABSInhibitionRequest_ABSInhibitionRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Write_HSADriverRequest_HSADriverRequest(Rte_InitValue_HSADriverRequest_HSADriverRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ServiceBraking_HMICtrl_Rte_Write_HillStartAid_DeviceIndication_DeviceIndication(Rte_InitValue_HillStartAid_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ServiceBraking_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ServiceBraking_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ServiceBraking_HMICtrl_CODE) ServiceBraking_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ServiceBraking_HMICtrl_Init
 *********************************************************************************************************************/

  SEWS_ABS_Inhibit_SwType_P1SY6_T P1SY6_ABS_Inhibit_SwType_v_data;
  boolean P1A1R_HSA_Installed_v_data;
  boolean P1NTV_HSA_DefaultConfig_v_data;
  boolean P1SY4_ABS_Inhibit_Installed_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1SY6_ABS_Inhibit_SwType_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY6_ABS_Inhibit_SwType_v();
  P1A1R_HSA_Installed_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1A1R_HSA_Installed_v();
  P1NTV_HSA_DefaultConfig_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1NTV_HSA_DefaultConfig_v();
  P1SY4_ABS_Inhibit_Installed_v_data = TSC_ServiceBraking_HMICtrl_Rte_Prm_P1SY4_ABS_Inhibit_Installed_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define ServiceBraking_HMICtrl_STOP_SEC_CODE
#include "ServiceBraking_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void ServiceBraking_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  InactiveActive_T Test_InactiveActive_T_V_1 = InactiveActive_Inactive;
  InactiveActive_T Test_InactiveActive_T_V_2 = InactiveActive_Active;
  InactiveActive_T Test_InactiveActive_T_V_3 = InactiveActive_Error;
  InactiveActive_T Test_InactiveActive_T_V_4 = InactiveActive_NotAvailable;

  Inhibit_T Test_Inhibit_T_V_1 = Inhibit_NoInhibit;
  Inhibit_T Test_Inhibit_T_V_2 = Inhibit_InhibitActive;
  Inhibit_T Test_Inhibit_T_V_3 = Inhibit_Error;
  Inhibit_T Test_Inhibit_T_V_4 = Inhibit_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  PassiveActive_T Test_PassiveActive_T_V_1 = PassiveActive_Passive;
  PassiveActive_T Test_PassiveActive_T_V_2 = PassiveActive_Active;
  PassiveActive_T Test_PassiveActive_T_V_3 = PassiveActive_Error;
  PassiveActive_T Test_PassiveActive_T_V_4 = PassiveActive_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
