/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_LKS_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_LKS_HMICtrl.h"
#include "TSC_LKS_HMICtrl.h"








Std_ReturnType TSC_LKS_HMICtrl_Rte_Read_LKSApplicationStatus_LKSApplicationStatus(LKSStatus_T *data)
{
  return Rte_Read_LKSApplicationStatus_LKSApplicationStatus(data);
}

Std_ReturnType TSC_LKS_HMICtrl_Rte_Read_LKSCS_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
{
  return Rte_Read_LKSCS_SwitchStatus_A3PosSwitchStatus(data);
}

Std_ReturnType TSC_LKS_HMICtrl_Rte_Read_LKSCorrectiveSteeringStatus_LKSCorrectiveSteeringStatus(LKSCorrectiveSteeringStatus_T *data)
{
  return Rte_Read_LKSCorrectiveSteeringStatus_LKSCorrectiveSteeringStatus(data);
}

Std_ReturnType TSC_LKS_HMICtrl_Rte_Read_LKSPushButton_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LKSPushButton_PushButtonStatus(data);
}

Std_ReturnType TSC_LKS_HMICtrl_Rte_Read_LKS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_LKS_SwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_LKS_HMICtrl_Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_EngineRun_EngineRun(data);
}




Std_ReturnType TSC_LKS_HMICtrl_Rte_Write_LKSCSEnableSwitch_rqst_LKSEnableSwitch_rqst(OffOn_T data)
{
  return Rte_Write_LKSCSEnableSwitch_rqst_LKSEnableSwitch_rqst(data);
}

Std_ReturnType TSC_LKS_HMICtrl_Rte_Write_LKSCS_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
{
  return Rte_Write_LKSCS_DeviceIndication_DualDeviceIndication(data);
}

Std_ReturnType TSC_LKS_HMICtrl_Rte_Write_LKSEnableSwitch_rqst_LKSEnableSwitch_rqst(OffOn_T data)
{
  return Rte_Write_LKSEnableSwitch_rqst_LKSEnableSwitch_rqst(data);
}

Std_ReturnType TSC_LKS_HMICtrl_Rte_Write_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LKS_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_LKS_SwType_P1R5P_T  TSC_LKS_HMICtrl_Rte_Prm_P1R5P_LKS_SwType_v(void)
{
  return (SEWS_LKS_SwType_P1R5P_T ) Rte_Prm_P1R5P_LKS_SwType_v();
}
boolean  TSC_LKS_HMICtrl_Rte_Prm_P1BKI_LKS_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1BKI_LKS_Installed_v();
}
boolean  TSC_LKS_HMICtrl_Rte_Prm_P1NQD_LKS_SwIndicationType_v(void)
{
  return (boolean ) Rte_Prm_P1NQD_LKS_SwIndicationType_v();
}


     /* LKS_HMICtrl */
      /* LKS_HMICtrl */



