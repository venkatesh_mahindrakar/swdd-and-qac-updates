/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_AuxiliaryBbSwitch_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_AuxiliaryBbSwitch_HMICtrl.h"
#include "TSC_AuxiliaryBbSwitch_HMICtrl.h"








Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitch1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_AuxSwitch1SwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitch2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_AuxSwitch2SwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitch3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_AuxSwitch3SwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitch4SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_AuxSwitch4SwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitch5SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_AuxSwitch5SwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitch6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_AuxSwitch6SwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitchBbLoad1_Status_AuxSwitchBbLoad1_Status(InactiveActive_T *data)
{
  return Rte_Read_AuxSwitchBbLoad1_Status_AuxSwitchBbLoad1_Status(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitchBbLoad2_Status_AuxSwitchBbLoad2_Status(InactiveActive_T *data)
{
  return Rte_Read_AuxSwitchBbLoad2_Status_AuxSwitchBbLoad2_Status(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitchBbLoad3_Status_AuxSwitchBbLoad3_Status(InactiveActive_T *data)
{
  return Rte_Read_AuxSwitchBbLoad3_Status_AuxSwitchBbLoad3_Status(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitchBbLoad4_Status_AuxSwitchBbLoad4_Status(InactiveActive_T *data)
{
  return Rte_Read_AuxSwitchBbLoad4_Status_AuxSwitchBbLoad4_Status(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitchBbLoad5_Status_AuxSwitchBbLoad5_Status(InactiveActive_T *data)
{
  return Rte_Read_AuxSwitchBbLoad5_Status_AuxSwitchBbLoad5_Status(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_AuxSwitchBbLoad6_Status_AuxSwitchBbLoad6_Status(InactiveActive_T *data)
{
  return Rte_Read_AuxSwitchBbLoad6_Status_AuxSwitchBbLoad6_Status(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Living_Living(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_WRCAux1Request_WRCAuxRequest(OffOn_T *data)
{
  return Rte_Read_WRCAux1Request_WRCAuxRequest(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_WRCAux2Request_WRCAuxRequest(OffOn_T *data)
{
  return Rte_Read_WRCAux2Request_WRCAuxRequest(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_WRCAux3Request_WRCAuxRequest(OffOn_T *data)
{
  return Rte_Read_WRCAux3Request_WRCAuxRequest(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_WRCAux4Request_WRCAuxRequest(OffOn_T *data)
{
  return Rte_Read_WRCAux4Request_WRCAuxRequest(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_WRCAux5Request_WRCAuxRequest(OffOn_T *data)
{
  return Rte_Read_WRCAux5Request_WRCAuxRequest(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Read_WRCAux6Request_WRCAuxRequest(OffOn_T *data)
{
  return Rte_Read_WRCAux6Request_WRCAuxRequest(data);
}




Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxBbSwitch1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_AuxBbSwitch1_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxBbSwitch2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_AuxBbSwitch2_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxBbSwitch3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_AuxBbSwitch3_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxBbSwitch4_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_AuxBbSwitch4_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxBbSwitch5_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_AuxBbSwitch5_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxBbSwitch6_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_AuxBbSwitch6_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxSwitchBbLoad1_Request_AuxSwitchBbLoad1_Request(OffOn_T data)
{
  return Rte_Write_AuxSwitchBbLoad1_Request_AuxSwitchBbLoad1_Request(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxSwitchBbLoad2_Request_AuxSwitchBbLoad2_Request(OffOn_T data)
{
  return Rte_Write_AuxSwitchBbLoad2_Request_AuxSwitchBbLoad2_Request(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxSwitchBbLoad3_Request_AuxSwitchBbLoad3_Request(OffOn_T data)
{
  return Rte_Write_AuxSwitchBbLoad3_Request_AuxSwitchBbLoad3_Request(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxSwitchBbLoad4_Request_AuxSwitchBbLoad4_Request(OffOn_T data)
{
  return Rte_Write_AuxSwitchBbLoad4_Request_AuxSwitchBbLoad4_Request(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxSwitchBbLoad5_Request_AuxSwitchBbLoad5_Request(OffOn_T data)
{
  return Rte_Write_AuxSwitchBbLoad5_Request_AuxSwitchBbLoad5_Request(data);
}

Std_ReturnType TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Write_AuxSwitchBbLoad6_Request_AuxSwitchBbLoad6_Request(OffOn_T data)
{
  return Rte_Write_AuxSwitchBbLoad6_Request_AuxSwitchBbLoad6_Request(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_AuxBBSw_TimeoutForReq_P1DV1_T  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DV1_AuxBBSw_TimeoutForReq_v(void)
{
  return (SEWS_AuxBBSw_TimeoutForReq_P1DV1_T ) Rte_Prm_P1DV1_AuxBBSw_TimeoutForReq_v();
}
SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1M93_AuxBBLoadStat_MaxInitTime_v(void)
{
  return (SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T ) Rte_Prm_P1M93_AuxBBLoadStat_MaxInitTime_v();
}
boolean  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DI0_AuxBBSw5_Act_v(void)
{
  return (boolean ) Rte_Prm_P1DI0_AuxBBSw5_Act_v();
}
boolean  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DI1_AuxBbSw6_Act_v(void)
{
  return (boolean ) Rte_Prm_P1DI1_AuxBbSw6_Act_v();
}
boolean  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DIW_AuxBbSw1_Act_v(void)
{
  return (boolean ) Rte_Prm_P1DIW_AuxBbSw1_Act_v();
}
boolean  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DIX_AuxBbSw2_Act_v(void)
{
  return (boolean ) Rte_Prm_P1DIX_AuxBbSw2_Act_v();
}
boolean  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DIY_AuxBbSw3_Act_v(void)
{
  return (boolean ) Rte_Prm_P1DIY_AuxBbSw3_Act_v();
}
boolean  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DIZ_AuxBbSw4_Act_v(void)
{
  return (boolean ) Rte_Prm_P1DIZ_AuxBbSw4_Act_v();
}
SEWS_AuxBbSw1_Logic_P1DI2_T  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DI2_AuxBbSw1_Logic_v(void)
{
  return (SEWS_AuxBbSw1_Logic_P1DI2_T ) Rte_Prm_P1DI2_AuxBbSw1_Logic_v();
}
SEWS_AuxBbSw2_Logic_P1DI3_T  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DI3_AuxBbSw2_Logic_v(void)
{
  return (SEWS_AuxBbSw2_Logic_P1DI3_T ) Rte_Prm_P1DI3_AuxBbSw2_Logic_v();
}
SEWS_AuxBbSw3_Logic_P1DI4_T  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DI4_AuxBbSw3_Logic_v(void)
{
  return (SEWS_AuxBbSw3_Logic_P1DI4_T ) Rte_Prm_P1DI4_AuxBbSw3_Logic_v();
}
SEWS_AuxBbSw4_Logic_P1DI5_T  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DI5_AuxBbSw4_Logic_v(void)
{
  return (SEWS_AuxBbSw4_Logic_P1DI5_T ) Rte_Prm_P1DI5_AuxBbSw4_Logic_v();
}
SEWS_AuxBbSw5_Logic_P1DI6_T  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DI6_AuxBbSw5_Logic_v(void)
{
  return (SEWS_AuxBbSw5_Logic_P1DI6_T ) Rte_Prm_P1DI6_AuxBbSw5_Logic_v();
}
SEWS_AuxBbSw6_Logic_P1DI7_T  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1DI7_AuxBbSw6_Logic_v(void)
{
  return (SEWS_AuxBbSw6_Logic_P1DI7_T ) Rte_Prm_P1DI7_AuxBbSw6_Logic_v();
}
boolean  TSC_AuxiliaryBbSwitch_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
{
  return (boolean ) Rte_Prm_P1B9X_WirelessRC_Enable_v();
}


     /* AuxiliaryBbSwitch_HMICtrl */
      /* AuxiliaryBbSwitch_HMICtrl */



