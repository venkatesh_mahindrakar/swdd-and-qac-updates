/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  LINMgr.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  LINMgr
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <LINMgr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * BswM_BswMRteMDG_LIN1Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN2Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN3Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN4Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN5Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN6Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN7Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN8Schedule
 *   
 *
 * ComM_ModeType
 *   
 *
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 * Dem_EventStatusType
 *   
 *
 * IOHWAB_BOOL
 *   
 *
 * IOHWAB_UINT8
 *   
 *
 * Issm_IssHandleType
 *   
 *
 * SEWS_LIN_topology_P1AJR_T
 *   
 *
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *   
 *
 * VGTT_EcuPinFaultStatus
 *   
 *
 * VGTT_EcuPinVoltage_0V2
 *   
 *
 *********************************************************************************************************************/

#include "Rte_LINMgr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_LINMgr.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void LINMgr_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * IOHWAB_BOOL: Boolean
 * IOHWAB_UINT8: Integer in interval [0...255]
 * Issm_IssHandleType: Integer in interval [0...255]
 * SEWS_LIN_topology_P1AJR_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BswM_BswMRteMDG_LIN1Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN1_Table1 (1U)
 *   LIN1_Table2 (2U)
 *   LIN1_Table_E (3U)
 *   LIN1_MasterReq_SlaveResp_Table1 (4U)
 *   LIN1_MasterReq_SlaveResp_Table2 (5U)
 *   LIN1_NULL (0U)
 *   LIN1_MasterReq_SlaveResp (6U)
 * BswM_BswMRteMDG_LIN2Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN2_NULL (0U)
 *   LIN2_TABLE0 (1U)
 *   LIN2_TABLE_E (2U)
 *   LIN2_MasterReq_SlaveResp_TABLE0 (3U)
 *   LIN2_MasterReq_SlaveResp (4U)
 * BswM_BswMRteMDG_LIN3Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN3_NULL (0U)
 *   LIN3_TABLE1 (1U)
 *   LIN3_TABLE2 (2U)
 *   LIN3_TABLE_E (3U)
 *   LIN3_MasterReq_SlaveResp_Table1 (4U)
 *   LIN3_MasterReq_SlaveResp_Table2 (5U)
 *   LIN3_MasterReq_SlaveResp (6U)
 * BswM_BswMRteMDG_LIN4Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN4_NULL (0U)
 *   LIN4_MasterReq_SlaveResp_Table1 (4U)
 *   LIN4_TABLE1 (1U)
 *   LIN4_TABLE2 (2U)
 *   LIN4_TABLE_E (3U)
 *   LIN4_MasterReq_SlaveResp_Table2 (5U)
 *   LIN4_MasterReq_SlaveResp (6U)
 * BswM_BswMRteMDG_LIN5Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN5_NULL (0U)
 *   LIN5_TABLE1 (1U)
 *   LIN5_MasterReq_SlaveResp_Table1 (4U)
 *   LIN5_MasterReq_SlaveResp_Table2 (5U)
 *   LIN5_MasterReq_SlaveResp (6U)
 *   LIN5_TABLE2 (2U)
 *   LIN5_TABLE_E (3U)
 * BswM_BswMRteMDG_LIN6Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN6_NULL (0U)
 *   LIN6_TABLE0 (1U)
 *   LIN6_MasterReq_SlaveResp_Table0 (3U)
 *   LIN6_MasterReq_SlaveResp (2U)
 * BswM_BswMRteMDG_LIN7Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN7_NULL (0U)
 *   LIN7_TABLE0 (1U)
 *   LIN7_MasterReq_SlaveResp_Table0 (3U)
 *   LIN7_MasterReq_SlaveResp (2U)
 * BswM_BswMRteMDG_LIN8Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN8_NULL (0U)
 *   LIN8_TABLE0 (1U)
 *   LIN8_MasterReq_SlaveResp_Table0 (3U)
 *   LIN8_MasterReq_SlaveResp (2U)
 * ComM_ModeType: Enumeration of integer in interval [0...3] with enumerators
 *   COMM_NO_COMMUNICATION (0U)
 *   COMM_SILENT_COMMUNICATION (1U)
 *   COMM_FULL_COMMUNICATION (2U)
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated (0U)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated (1U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * SEWS_PcbConfig_LinInterfaces_X1CX0_a_T: Array with 7 element(s) of type SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T *Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_LinInterfaces_X1CX0_T* is of type SEWS_PcbConfig_LinInterfaces_X1CX0_a_T
 *   SEWS_LIN_topology_P1AJR_T Rte_Prm_P1AJR_LIN_topology_v(void)
 *   boolean Rte_Prm_P1WPP_isSecurityLinActive_v(void)
 *
 *********************************************************************************************************************/


#define LINMgr_START_SEC_CODE
#include "LINMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1CXF_Data_P1CXF_FCI_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1CXF_Data_P1CXF_FCI>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1CXF_Data_P1CXF_FCI_ReadData_Irv_DID_FCI(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1CXF_Data_P1CXF_FCI_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1CXF_Data_P1CXF_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1CXF_Data_P1CXF_FCI_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LINMgr_CODE) DataServices_P1CXF_Data_P1CXF_FCI_ReadData(P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1CXF_Data_P1CXF_FCI_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  uint8 DataServices_P1CXF_Data_P1CXF_FCI_ReadData_Irv_DID_FCI;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  DataServices_P1CXF_Data_P1CXF_FCI_ReadData_Irv_DID_FCI = TSC_LINMgr_Rte_IrvRead_DataServices_P1CXF_Data_P1CXF_FCI_ReadData_Irv_DID_FCI();

  LINMgr_TestDefines();

  return RTE_E_DataServices_P1CXF_Data_P1CXF_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Issm_IssStateChange_Issm_IssActivation
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Issm_IssActivation> of PortPrototype <Issm_IssStateChange>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void Issm_IssStateChange_Issm_IssActivation(Issm_IssHandleType issID)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_IssStateChange_Issm_IssActivation_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) Issm_IssStateChange_Issm_IssActivation(Issm_IssHandleType issID) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_IssStateChange_Issm_IssActivation
 *********************************************************************************************************************/

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Issm_IssStateChange_Issm_IssDeactivation
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Issm_IssDeactivation> of PortPrototype <Issm_IssStateChange>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void Issm_IssStateChange_Issm_IssDeactivation(Issm_IssHandleType issID)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_IssStateChange_Issm_IssDeactivation_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) Issm_IssStateChange_Issm_IssDeactivation(Issm_IssHandleType issID) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_IssStateChange_Issm_IssDeactivation
 *********************************************************************************************************************/

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_R1AAJ_FlexibleSwitchDetection>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults (returns application error)
 *********************************************************************************************************************/

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  return RTE_E_RoutineServices_R1AAJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAJ_FlexibleSwitchDetection_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_R1AAJ_FlexibleSwitchDetection>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   Boolean Rte_IrvRead_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig(Boolean data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAJ_FlexibleSwitchDetection_Start(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_Start (returns application error)
 *********************************************************************************************************************/

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  Boolean RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig = TSC_LINMgr_Rte_IrvRead_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig();

  TSC_LINMgr_Rte_IrvWrite_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig(FALSE);

  return RTE_E_RoutineServices_R1AAJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_R1AAJ_FlexibleSwitchDetection>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAJ_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LINMgr_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop (returns application error)
 *********************************************************************************************************************/

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  return RTE_E_RoutineServices_R1AAJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_LinDiagRequestFlag_CCNADRequest(uint8 *data)
 *   Std_ReturnType Rte_Read_LinDiagRequestFlag_PNSNRequest(uint8 *data)
 *   Std_ReturnType Rte_Read_SwcActivation_LIN_SwcActivation_LIN(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(Boolean *data)
 *   Std_ReturnType Rte_Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(Boolean *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN6_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN7_ComMode_LIN(ComMode_LIN_Type data)
 *   Std_ReturnType Rte_Write_ComMode_LIN8_ComMode_LIN(ComMode_LIN_Type data)
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN1Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN1Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN2Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN2Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN3Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN3Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN4Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN4Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN5Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN5Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN6Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0
 *   - RTE_TRANSITION_BswMRteMDG_LIN6Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN7Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0
 *   - RTE_TRANSITION_BswMRteMDG_LIN7Schedule
 *   uint8 Rte_Mode_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN8Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0
 *   - RTE_TRANSITION_BswMRteMDG_LIN8Schedule
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(void)
 *   Boolean Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(uint8 data)
 *   void Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig(Boolean data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BJO_88_LIN1busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BJP_88_LIN2busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BJQ_88_LIN3busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BJR_88_LIN4busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BJS_88_LIN5busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN00_ace1a6ba_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN01_4323cd84_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN02_a8147687_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN03_47d61db9_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN04_a50a06c0_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN05_4ac86dfe_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN06_a1ffd6fd_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_CN_LIN07_4e3dbdc3_GetCurrentComMode(ComM_ModeType *ComMode)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_ComM_UserRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  uint8 Read_LinDiagRequestFlag_CCNADRequest;
  uint8 Read_LinDiagRequestFlag_PNSNRequest;
  VehicleModeDistribution_T Read_SwcActivation_LIN_SwcActivation_LIN;
  VehicleMode_T Read_VehicleModeInternal_VehicleMode;
  Boolean Read_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted;
  Boolean Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type;

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  Rte_ModeType_BswMRteMDG_LIN1Schedule Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule;
  Rte_ModeType_BswMRteMDG_LIN2Schedule Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule;
  Rte_ModeType_BswMRteMDG_LIN3Schedule Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule;
  Rte_ModeType_BswMRteMDG_LIN4Schedule Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule;
  Rte_ModeType_BswMRteMDG_LIN5Schedule Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule;
  Rte_ModeType_BswMRteMDG_LIN6Schedule Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule;
  Rte_ModeType_BswMRteMDG_LIN7Schedule Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule;
  Rte_ModeType_BswMRteMDG_LIN8Schedule Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule;

  uint8 SCIM_LINMgr_20ms_runnable_Irv_DID_FCI;
  Boolean SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig;

  IOHWAB_BOOL Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus = 0U;

  ComM_ModeType Call_UR_CN_LIN00_ace1a6ba_GetCurrentComMode_ComMode = 0U;
  ComM_ModeType Call_UR_CN_LIN01_4323cd84_GetCurrentComMode_ComMode = 0U;
  ComM_ModeType Call_UR_CN_LIN02_a8147687_GetCurrentComMode_ComMode = 0U;
  ComM_ModeType Call_UR_CN_LIN03_47d61db9_GetCurrentComMode_ComMode = 0U;
  ComM_ModeType Call_UR_CN_LIN04_a50a06c0_GetCurrentComMode_ComMode = 0U;
  ComM_ModeType Call_UR_CN_LIN05_4ac86dfe_GetCurrentComMode_ComMode = 0U;
  ComM_ModeType Call_UR_CN_LIN06_a1ffd6fd_GetCurrentComMode_ComMode = 0U;
  ComM_ModeType Call_UR_CN_LIN07_4e3dbdc3_GetCurrentComMode_ComMode = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  fct_status = TSC_LINMgr_Rte_Read_LinDiagRequestFlag_CCNADRequest(&Read_LinDiagRequestFlag_CCNADRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Read_LinDiagRequestFlag_PNSNRequest(&Read_LinDiagRequestFlag_PNSNRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Read_SwcActivation_LIN_SwcActivation_LIN(&Read_SwcActivation_LIN_SwcActivation_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Read_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(&Read_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(&Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Write_ComMode_LIN1_ComMode_LIN(Rte_InitValue_ComMode_LIN1_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Write_ComMode_LIN2_ComMode_LIN(Rte_InitValue_ComMode_LIN2_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Write_ComMode_LIN3_ComMode_LIN(Rte_InitValue_ComMode_LIN3_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Write_ComMode_LIN4_ComMode_LIN(Rte_InitValue_ComMode_LIN4_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Write_ComMode_LIN5_ComMode_LIN(Rte_InitValue_ComMode_LIN5_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Write_ComMode_LIN6_ComMode_LIN(Rte_InitValue_ComMode_LIN6_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Write_ComMode_LIN7_ComMode_LIN(Rte_InitValue_ComMode_LIN7_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Write_ComMode_LIN8_ComMode_LIN(Rte_InitValue_ComMode_LIN8_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule();
  Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule = TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule();
  Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule();
  Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule();
  Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule();
  Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule = TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule();
  Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule = TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule();
  Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule = TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule();

  SCIM_LINMgr_20ms_runnable_Irv_DID_FCI = TSC_LINMgr_Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI();
  SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig = TSC_LINMgr_Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig();

  TSC_LINMgr_Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(0U);
  TSC_LINMgr_Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig(FALSE);

  fct_status = TSC_LINMgr_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0U, &Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated, &Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_Event_D1BJO_88_LIN1busCIOM_BusOff_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_Event_D1BJP_88_LIN2busCIOM_BusOff_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_Event_D1BJQ_88_LIN3busCIOM_BusOff_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_Event_D1BJR_88_LIN4busCIOM_BusOff_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_Event_D1BJS_88_LIN5busCIOM_BusOff_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_UR_CN_LIN00_ace1a6ba_GetCurrentComMode(&Call_UR_CN_LIN00_ace1a6ba_GetCurrentComMode_ComMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_UR_CN_LIN01_4323cd84_GetCurrentComMode(&Call_UR_CN_LIN01_4323cd84_GetCurrentComMode_ComMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_UR_CN_LIN02_a8147687_GetCurrentComMode(&Call_UR_CN_LIN02_a8147687_GetCurrentComMode_ComMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_UR_CN_LIN03_47d61db9_GetCurrentComMode(&Call_UR_CN_LIN03_47d61db9_GetCurrentComMode_ComMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_UR_CN_LIN04_a50a06c0_GetCurrentComMode(&Call_UR_CN_LIN04_a50a06c0_GetCurrentComMode_ComMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_UR_CN_LIN05_4ac86dfe_GetCurrentComMode(&Call_UR_CN_LIN05_4ac86dfe_GetCurrentComMode_ComMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_UR_CN_LIN06_a1ffd6fd_GetCurrentComMode(&Call_UR_CN_LIN06_a1ffd6fd_GetCurrentComMode_ComMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LINMgr_Rte_Call_UR_CN_LIN07_4e3dbdc3_GetCurrentComMode(&Call_UR_CN_LIN07_4e3dbdc3_GetCurrentComMode_ComMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_ComM_UserRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN1SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN1SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN1_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN1Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN1SchEndNotif_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN1SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN1SchEndNotif
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  BswM_BswMRteMDG_LIN1Schedule Write_Request_LIN1_ScheduleTableRequestMode_requestedMode;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  (void)memset(&Write_Request_LIN1_ScheduleTableRequestMode_requestedMode, 0, sizeof(Write_Request_LIN1_ScheduleTableRequestMode_requestedMode));
  fct_status = TSC_LINMgr_Rte_Write_Request_LIN1_ScheduleTableRequestMode_requestedMode(Write_Request_LIN1_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN2SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN2SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN2_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN2Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN2SchEndNotif_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN2SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN2SchEndNotif
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  BswM_BswMRteMDG_LIN2Schedule Write_Request_LIN2_ScheduleTableRequestMode_requestedMode;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  (void)memset(&Write_Request_LIN2_ScheduleTableRequestMode_requestedMode, 0, sizeof(Write_Request_LIN2_ScheduleTableRequestMode_requestedMode));
  fct_status = TSC_LINMgr_Rte_Write_Request_LIN2_ScheduleTableRequestMode_requestedMode(Write_Request_LIN2_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN3SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN3SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN3_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN3Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN3SchEndNotif_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN3SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN3SchEndNotif
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  BswM_BswMRteMDG_LIN3Schedule Write_Request_LIN3_ScheduleTableRequestMode_requestedMode;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  (void)memset(&Write_Request_LIN3_ScheduleTableRequestMode_requestedMode, 0, sizeof(Write_Request_LIN3_ScheduleTableRequestMode_requestedMode));
  fct_status = TSC_LINMgr_Rte_Write_Request_LIN3_ScheduleTableRequestMode_requestedMode(Write_Request_LIN3_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN4SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN4SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN4_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN4Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN4SchEndNotif_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN4SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN4SchEndNotif
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  BswM_BswMRteMDG_LIN4Schedule Write_Request_LIN4_ScheduleTableRequestMode_requestedMode;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  (void)memset(&Write_Request_LIN4_ScheduleTableRequestMode_requestedMode, 0, sizeof(Write_Request_LIN4_ScheduleTableRequestMode_requestedMode));
  fct_status = TSC_LINMgr_Rte_Write_Request_LIN4_ScheduleTableRequestMode_requestedMode(Write_Request_LIN4_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN5SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN5SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN5_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN5Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN5SchEndNotif_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN5SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN5SchEndNotif
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  BswM_BswMRteMDG_LIN5Schedule Write_Request_LIN5_ScheduleTableRequestMode_requestedMode;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  (void)memset(&Write_Request_LIN5_ScheduleTableRequestMode_requestedMode, 0, sizeof(Write_Request_LIN5_ScheduleTableRequestMode_requestedMode));
  fct_status = TSC_LINMgr_Rte_Write_Request_LIN5_ScheduleTableRequestMode_requestedMode(Write_Request_LIN5_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN6SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN6SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN6_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN6Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN6SchEndNotif_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN6SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN6SchEndNotif
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  fct_status = TSC_LINMgr_Rte_Write_Request_LIN6_ScheduleTableRequestMode_requestedMode(Rte_InitValue_Request_LIN6_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN7SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN7SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN7_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN7Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN7SchEndNotif_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN7SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN7SchEndNotif
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  fct_status = TSC_LINMgr_Rte_Write_Request_LIN7_ScheduleTableRequestMode_requestedMode(Rte_InitValue_Request_LIN7_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SCIM_LINMgr_LIN8SchEndNotif
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN8SchTableState>
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Request_LIN8_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN8Schedule data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN8SchEndNotif_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LINMgr_CODE) SCIM_LINMgr_LIN8SchEndNotif(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SCIM_LINMgr_LIN8SchEndNotif
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));

  P1AJR_LIN_topology_v_data = TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  fct_status = TSC_LINMgr_Rte_Write_Request_LIN8_ScheduleTableRequestMode_requestedMode(Rte_InitValue_Request_LIN8_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define LINMgr_STOP_SEC_CODE
#include "LINMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void LINMgr_TestDefines(void)
{
  /* Enumeration Data Types */

  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_1 = LIN1_Table1;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_2 = LIN1_Table2;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_3 = LIN1_Table_E;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_4 = LIN1_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_5 = LIN1_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_6 = LIN1_NULL;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_7 = LIN1_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_1 = LIN2_NULL;
  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_2 = LIN2_TABLE0;
  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_3 = LIN2_TABLE_E;
  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_4 = LIN2_MasterReq_SlaveResp_TABLE0;
  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_5 = LIN2_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_1 = LIN3_NULL;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_2 = LIN3_TABLE1;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_3 = LIN3_TABLE2;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_4 = LIN3_TABLE_E;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_5 = LIN3_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_6 = LIN3_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_7 = LIN3_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_1 = LIN4_NULL;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_2 = LIN4_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_3 = LIN4_TABLE1;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_4 = LIN4_TABLE2;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_5 = LIN4_TABLE_E;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_6 = LIN4_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_7 = LIN4_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_1 = LIN5_NULL;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_2 = LIN5_TABLE1;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_3 = LIN5_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_4 = LIN5_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_5 = LIN5_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_6 = LIN5_TABLE2;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_7 = LIN5_TABLE_E;

  BswM_BswMRteMDG_LIN6Schedule Test_BswM_BswMRteMDG_LIN6Schedule_V_1 = LIN6_NULL;
  BswM_BswMRteMDG_LIN6Schedule Test_BswM_BswMRteMDG_LIN6Schedule_V_2 = LIN6_TABLE0;
  BswM_BswMRteMDG_LIN6Schedule Test_BswM_BswMRteMDG_LIN6Schedule_V_3 = LIN6_MasterReq_SlaveResp_Table0;
  BswM_BswMRteMDG_LIN6Schedule Test_BswM_BswMRteMDG_LIN6Schedule_V_4 = LIN6_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN7Schedule Test_BswM_BswMRteMDG_LIN7Schedule_V_1 = LIN7_NULL;
  BswM_BswMRteMDG_LIN7Schedule Test_BswM_BswMRteMDG_LIN7Schedule_V_2 = LIN7_TABLE0;
  BswM_BswMRteMDG_LIN7Schedule Test_BswM_BswMRteMDG_LIN7Schedule_V_3 = LIN7_MasterReq_SlaveResp_Table0;
  BswM_BswMRteMDG_LIN7Schedule Test_BswM_BswMRteMDG_LIN7Schedule_V_4 = LIN7_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN8Schedule Test_BswM_BswMRteMDG_LIN8Schedule_V_1 = LIN8_NULL;
  BswM_BswMRteMDG_LIN8Schedule Test_BswM_BswMRteMDG_LIN8Schedule_V_2 = LIN8_TABLE0;
  BswM_BswMRteMDG_LIN8Schedule Test_BswM_BswMRteMDG_LIN8Schedule_V_3 = LIN8_MasterReq_SlaveResp_Table0;
  BswM_BswMRteMDG_LIN8Schedule Test_BswM_BswMRteMDG_LIN8Schedule_V_4 = LIN8_MasterReq_SlaveResp;

  ComM_ModeType Test_ComM_ModeType_V_1 = COMM_NO_COMMUNICATION;
  ComM_ModeType Test_ComM_ModeType_V_2 = COMM_SILENT_COMMUNICATION;
  ComM_ModeType Test_ComM_ModeType_V_3 = COMM_FULL_COMMUNICATION;

  ComMode_LIN_Type Test_ComMode_LIN_Type_V_1 = Inactive;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_2 = Diagnostic;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_3 = SwitchDetection;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_4 = ApplicationMonitoring;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_5 = Calibration;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_6 = Spare1;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_7 = Error;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_8 = NotAvailable;

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  SEWS_PcbConfig_LinInterfaces_X1CX0_T Test_SEWS_PcbConfig_LinInterfaces_X1CX0_T_V_1 = SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated;
  SEWS_PcbConfig_LinInterfaces_X1CX0_T Test_SEWS_PcbConfig_LinInterfaces_X1CX0_T_V_2 = SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated;

  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_1 = TestNotRun;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_2 = OffState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_3 = OffState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_4 = OffState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_5 = OffState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_6 = OffState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_7 = OffState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_8 = OnState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_9 = OnState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_10 = OnState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_11 = OnState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_12 = OnState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_13 = OnState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_14 = OnState_FaultDetected_VOR;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_15 = OnState_FaultDetected_CAT;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;

  /* Modes */

  uint8 Test_BswMRteMDG_LIN1Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E;
  uint8 Test_BswMRteMDG_LIN1Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN1Schedule;

  uint8 Test_BswMRteMDG_LIN2Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN2Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0;
  uint8 Test_BswMRteMDG_LIN2Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL;
  uint8 Test_BswMRteMDG_LIN2Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0;
  uint8 Test_BswMRteMDG_LIN2Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E;
  uint8 Test_BswMRteMDG_LIN2Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN2Schedule;

  uint8 Test_BswMRteMDG_LIN3Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E;
  uint8 Test_BswMRteMDG_LIN3Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN3Schedule;

  uint8 Test_BswMRteMDG_LIN4Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E;
  uint8 Test_BswMRteMDG_LIN4Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN4Schedule;

  uint8 Test_BswMRteMDG_LIN5Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E;
  uint8 Test_BswMRteMDG_LIN5Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN5Schedule;

  uint8 Test_BswMRteMDG_LIN6Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN6Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0;
  uint8 Test_BswMRteMDG_LIN6Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL;
  uint8 Test_BswMRteMDG_LIN6Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0;
  uint8 Test_BswMRteMDG_LIN6Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN6Schedule;

  uint8 Test_BswMRteMDG_LIN7Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN7Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0;
  uint8 Test_BswMRteMDG_LIN7Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL;
  uint8 Test_BswMRteMDG_LIN7Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0;
  uint8 Test_BswMRteMDG_LIN7Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN7Schedule;

  uint8 Test_BswMRteMDG_LIN8Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN8Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0;
  uint8 Test_BswMRteMDG_LIN8Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL;
  uint8 Test_BswMRteMDG_LIN8Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0;
  uint8 Test_BswMRteMDG_LIN8Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN8Schedule;

  uint8 Test_BswMRteMDG_LINSchTableState_MV_1 = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  uint8 Test_BswMRteMDG_LINSchTableState_MV_2 = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT;
  uint8 Test_BswMRteMDG_LINSchTableState_MV_3 = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  uint8 Test_BswMRteMDG_LINSchTableState_TV = RTE_TRANSITION_BswMRteMDG_LINSchTableState;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
