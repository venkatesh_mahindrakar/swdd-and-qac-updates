/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  LKS_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  LKS_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <LKS_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_LKS_SwType_P1R5P_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_LKS_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_LKS_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void LKS_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_LKS_SwType_P1R5P_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 *   DualDeviceIndication_UpperOffLowerOff (0U)
 *   DualDeviceIndication_UpperOnLowerOff (1U)
 *   DualDeviceIndication_UpperBlinkLowerOff (2U)
 *   DualDeviceIndication_UpperDontCareLowerOff (3U)
 *   DualDeviceIndication_UpperOffLowerOn (4U)
 *   DualDeviceIndication_UpperOnLowerOn (5U)
 *   DualDeviceIndication_UpperBlinkLowerOn (6U)
 *   DualDeviceIndication_UpperDontCareLowerOn (7U)
 *   DualDeviceIndication_UpperOffLowerBlink (8U)
 *   DualDeviceIndication_UpperOnLowerBlink (9U)
 *   DualDeviceIndication_UpperBlinkLowerBlink (10U)
 *   DualDeviceIndication_UpperDontCareLowerBlink (11U)
 *   DualDeviceIndication_UpperOffLowerDontCare (12U)
 *   DualDeviceIndication_UpperOnLowerDontCare (13U)
 *   DualDeviceIndication_UpperBlinkLowerDontCare (14U)
 *   DualDeviceIndication_UpperDontCareLowerDontCare (15U)
 * LKSCorrectiveSteeringStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   LKSCorrectiveSteeringStatus_Off (0U)
 *   LKSCorrectiveSteeringStatus_Available (1U)
 *   LKSCorrectiveSteeringStatus_Steering (2U)
 *   LKSCorrectiveSteeringStatus_DriverTakeOverRqst (3U)
 *   LKSCorrectiveSteeringStatus_TempUnavailable (4U)
 *   LKSCorrectiveSteeringStatus_Spare2 (5U)
 *   LKSCorrectiveSteeringStatus_Error (6U)
 *   LKSCorrectiveSteeringStatus_NotAvailable (7U)
 * LKSStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   LKSStatus_OFF (0U)
 *   LKSStatus_INACTIVE (1U)
 *   LKSStatus_ACTIVE (2U)
 *   LKSStatus_Spare (3U)
 *   LKSStatus_Spare_01 (4U)
 *   LKSStatus_Spare_02 (5U)
 *   LKSStatus_Error (6U)
 *   LKSStatus_NotAvailable (7U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_LKS_SwType_P1R5P_T Rte_Prm_P1R5P_LKS_SwType_v(void)
 *   boolean Rte_Prm_P1BKI_LKS_Installed_v(void)
 *   boolean Rte_Prm_P1NQD_LKS_SwIndicationType_v(void)
 *
 *********************************************************************************************************************/


#define LKS_HMICtrl_START_SEC_CODE
#include "LKS_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LKS_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_LKSApplicationStatus_LKSApplicationStatus(LKSStatus_T *data)
 *   Std_ReturnType Rte_Read_LKSCS_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LKSCorrectiveSteeringStatus_LKSCorrectiveSteeringStatus(LKSCorrectiveSteeringStatus_T *data)
 *   Std_ReturnType Rte_Read_LKSPushButton_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LKS_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LKSCSEnableSwitch_rqst_LKSEnableSwitch_rqst(OffOn_T data)
 *   Std_ReturnType Rte_Write_LKSCS_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LKSEnableSwitch_rqst_LKSEnableSwitch_rqst(OffOn_T data)
 *   Std_ReturnType Rte_Write_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LKS_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LKS_HMICtrl_CODE) LKS_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LKS_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  LKSStatus_T Read_LKSApplicationStatus_LKSApplicationStatus;
  A3PosSwitchStatus_T Read_LKSCS_SwitchStatus_A3PosSwitchStatus;
  LKSCorrectiveSteeringStatus_T Read_LKSCorrectiveSteeringStatus_LKSCorrectiveSteeringStatus;
  PushButtonStatus_T Read_LKSPushButton_PushButtonStatus;
  A2PosSwitchStatus_T Read_LKS_SwitchStatus_A2PosSwitchStatus;
  VehicleModeDistribution_T Read_SwcActivation_EngineRun_EngineRun;

  SEWS_LKS_SwType_P1R5P_T P1R5P_LKS_SwType_v_data;
  boolean P1BKI_LKS_Installed_v_data;
  boolean P1NQD_LKS_SwIndicationType_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1R5P_LKS_SwType_v_data = TSC_LKS_HMICtrl_Rte_Prm_P1R5P_LKS_SwType_v();
  P1BKI_LKS_Installed_v_data = TSC_LKS_HMICtrl_Rte_Prm_P1BKI_LKS_Installed_v();
  P1NQD_LKS_SwIndicationType_v_data = TSC_LKS_HMICtrl_Rte_Prm_P1NQD_LKS_SwIndicationType_v();

  fct_status = TSC_LKS_HMICtrl_Rte_Read_LKSApplicationStatus_LKSApplicationStatus(&Read_LKSApplicationStatus_LKSApplicationStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LKS_HMICtrl_Rte_Read_LKSCS_SwitchStatus_A3PosSwitchStatus(&Read_LKSCS_SwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LKS_HMICtrl_Rte_Read_LKSCorrectiveSteeringStatus_LKSCorrectiveSteeringStatus(&Read_LKSCorrectiveSteeringStatus_LKSCorrectiveSteeringStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LKS_HMICtrl_Rte_Read_LKSPushButton_PushButtonStatus(&Read_LKSPushButton_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LKS_HMICtrl_Rte_Read_LKS_SwitchStatus_A2PosSwitchStatus(&Read_LKS_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LKS_HMICtrl_Rte_Read_SwcActivation_EngineRun_EngineRun(&Read_SwcActivation_EngineRun_EngineRun);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LKS_HMICtrl_Rte_Write_LKSCSEnableSwitch_rqst_LKSEnableSwitch_rqst(Rte_InitValue_LKSCSEnableSwitch_rqst_LKSEnableSwitch_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LKS_HMICtrl_Rte_Write_LKSCS_DeviceIndication_DualDeviceIndication(Rte_InitValue_LKSCS_DeviceIndication_DualDeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LKS_HMICtrl_Rte_Write_LKSEnableSwitch_rqst_LKSEnableSwitch_rqst(Rte_InitValue_LKSEnableSwitch_rqst_LKSEnableSwitch_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LKS_HMICtrl_Rte_Write_LKS_DeviceIndication_DeviceIndication(Rte_InitValue_LKS_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  LKS_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define LKS_HMICtrl_STOP_SEC_CODE
#include "LKS_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void LKS_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_1 = A3PosSwitchStatus_Middle;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_2 = A3PosSwitchStatus_Lower;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_3 = A3PosSwitchStatus_Upper;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_4 = A3PosSwitchStatus_Spare;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_5 = A3PosSwitchStatus_Spare_01;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_6 = A3PosSwitchStatus_Spare_02;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_7 = A3PosSwitchStatus_Error;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_8 = A3PosSwitchStatus_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DualDeviceIndication_T Test_DualDeviceIndication_T_V_1 = DualDeviceIndication_UpperOffLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_2 = DualDeviceIndication_UpperOnLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_3 = DualDeviceIndication_UpperBlinkLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_4 = DualDeviceIndication_UpperDontCareLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_5 = DualDeviceIndication_UpperOffLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_6 = DualDeviceIndication_UpperOnLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_7 = DualDeviceIndication_UpperBlinkLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_8 = DualDeviceIndication_UpperDontCareLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_9 = DualDeviceIndication_UpperOffLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_10 = DualDeviceIndication_UpperOnLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_11 = DualDeviceIndication_UpperBlinkLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_12 = DualDeviceIndication_UpperDontCareLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_13 = DualDeviceIndication_UpperOffLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_14 = DualDeviceIndication_UpperOnLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_15 = DualDeviceIndication_UpperBlinkLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_16 = DualDeviceIndication_UpperDontCareLowerDontCare;

  LKSCorrectiveSteeringStatus_T Test_LKSCorrectiveSteeringStatus_T_V_1 = LKSCorrectiveSteeringStatus_Off;
  LKSCorrectiveSteeringStatus_T Test_LKSCorrectiveSteeringStatus_T_V_2 = LKSCorrectiveSteeringStatus_Available;
  LKSCorrectiveSteeringStatus_T Test_LKSCorrectiveSteeringStatus_T_V_3 = LKSCorrectiveSteeringStatus_Steering;
  LKSCorrectiveSteeringStatus_T Test_LKSCorrectiveSteeringStatus_T_V_4 = LKSCorrectiveSteeringStatus_DriverTakeOverRqst;
  LKSCorrectiveSteeringStatus_T Test_LKSCorrectiveSteeringStatus_T_V_5 = LKSCorrectiveSteeringStatus_TempUnavailable;
  LKSCorrectiveSteeringStatus_T Test_LKSCorrectiveSteeringStatus_T_V_6 = LKSCorrectiveSteeringStatus_Spare2;
  LKSCorrectiveSteeringStatus_T Test_LKSCorrectiveSteeringStatus_T_V_7 = LKSCorrectiveSteeringStatus_Error;
  LKSCorrectiveSteeringStatus_T Test_LKSCorrectiveSteeringStatus_T_V_8 = LKSCorrectiveSteeringStatus_NotAvailable;

  LKSStatus_T Test_LKSStatus_T_V_1 = LKSStatus_OFF;
  LKSStatus_T Test_LKSStatus_T_V_2 = LKSStatus_INACTIVE;
  LKSStatus_T Test_LKSStatus_T_V_3 = LKSStatus_ACTIVE;
  LKSStatus_T Test_LKSStatus_T_V_4 = LKSStatus_Spare;
  LKSStatus_T Test_LKSStatus_T_V_5 = LKSStatus_Spare_01;
  LKSStatus_T Test_LKSStatus_T_V_6 = LKSStatus_Spare_02;
  LKSStatus_T Test_LKSStatus_T_V_7 = LKSStatus_Error;
  LKSStatus_T Test_LKSStatus_T_V_8 = LKSStatus_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
