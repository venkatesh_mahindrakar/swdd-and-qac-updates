/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Issm.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  Issm
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <Issm>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Issm_IssHandleType
 *   
 *
 * Issm_IssStateType
 *   
 *
 * Issm_UserHandleType
 *   
 *
 *********************************************************************************************************************/

#include "Rte_Issm.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_Issm.h"
#include "SchM_Issm.h"
#include "TSC_SchM_Issm.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void Issm_TestDefines(void);

typedef P2FUNC(Std_ReturnType, RTE_CODE, FncPtrType)(void); /* PRQA S 3448 */ /* MD_Rte_TestCode */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Issm_IssHandleType: Integer in interval [0...255]
 * Issm_UserHandleType: Integer in interval [0...255]
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Issm_IssStateType: Enumeration of integer in interval [0...2] with enumerators
 *   ISSM_STATE_INACTIVE (0U)
 *   ISSM_STATE_PENDING (1U)
 *   ISSM_STATE_ACTIVE (2U)
 *
 * Array Types:
 * ============
 * Issm_ActiveUserArrayType: Array with 2 element(s) of type uint32
 *
 *********************************************************************************************************************/


#define Issm_START_SEC_CODE
#include "Issm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ActivateIss
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ASLight_InputFSP>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_AlarmSetUnset1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_AlarmSetUnset2>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_AlarmTriggers3>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ApproachLights1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_AudioRadio2>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_AudioRadio3>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_AuxHornRequest>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_BlackoutConvoyMode>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_CIOMOperStateRedundancy>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_CabTiltSwitchRequest>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_CityHornRequest>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ClimPHTimerSettings3>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_Dcm>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_DimmingAdjustment1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_DimmingAdjustment2>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_DimmingAdjustment3>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_DimmingAdjustment4>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_DoorOpeningLights5>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ECSStandByActive>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ECSStandByTrigger1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ExteriorLightsRequest1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ExteriorLightsRequest2>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ExtraBBAuxiliarySwitches1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ExtraBBTailLiftFSP2>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_FlexibleSwitchDetection>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ImmobilizerPINCode>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_InteriorLightsRqst1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_KeyfobPowerControl>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_LockControlActDeactivation>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_LockControlCabRqst1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_LockControlCabRqst2>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_LockControlCabRqst2_Pending>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_LockControlKeyfobRqst>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_OtherInteriorLights2>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_OtherInteriorLights3>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_OtherInteriorLights4>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_OtherInteriorLights5>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_PHActMaintainLiving2>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_PHActMaintainLiving5>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_PHActMaintainLiving6>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_PanicAlarmFromKeyfob>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_ParkBrakeAlert5>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_PowerWindowsActivate1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_PowerWindowsActivate3>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_RoofHatchRequest1>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_RoofHatchRequest3>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_SCIM>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_VMChangeOnDoorsOpening>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_WLight_InputELCP>
 *   - triggered by server invocation for OperationPrototype <ActivateIss> of PortPrototype <UR_ANW_WLight_InputFSP>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Issm_ActivateIss(void)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ActivateIss_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Issm_CODE) Issm_ActivateIss(Issm_UserHandleType parg0) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_ActivateIss (returns application error)
 *********************************************************************************************************************/

  Issm_TestDefines();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DeactivateIss
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ASLight_InputFSP>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_AlarmSetUnset1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_AlarmSetUnset2>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_AlarmTriggers3>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ApproachLights1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_AudioRadio2>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_AudioRadio3>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_AuxHornRequest>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_BlackoutConvoyMode>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_CIOMOperStateRedundancy>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_CabTiltSwitchRequest>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_CityHornRequest>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ClimPHTimerSettings3>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_Dcm>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_DimmingAdjustment1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_DimmingAdjustment2>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_DimmingAdjustment3>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_DimmingAdjustment4>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_DoorOpeningLights5>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ECSStandByActive>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ECSStandByTrigger1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ExteriorLightsRequest1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ExteriorLightsRequest2>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ExtraBBAuxiliarySwitches1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ExtraBBTailLiftFSP2>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_FlexibleSwitchDetection>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ImmobilizerPINCode>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_InteriorLightsRqst1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_KeyfobPowerControl>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_LockControlActDeactivation>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_LockControlCabRqst1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_LockControlCabRqst2>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_LockControlCabRqst2_Pending>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_LockControlKeyfobRqst>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_OtherInteriorLights2>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_OtherInteriorLights3>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_OtherInteriorLights4>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_OtherInteriorLights5>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_PHActMaintainLiving2>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_PHActMaintainLiving5>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_PHActMaintainLiving6>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_PanicAlarmFromKeyfob>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_ParkBrakeAlert5>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_PowerWindowsActivate1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_PowerWindowsActivate3>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_RoofHatchRequest1>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_RoofHatchRequest3>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_SCIM>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_VMChangeOnDoorsOpening>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_WLight_InputELCP>
 *   - triggered by server invocation for OperationPrototype <DeactivateIss> of PortPrototype <UR_ANW_WLight_InputFSP>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Issm_DeactivateIss(void)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DeactivateIss_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Issm_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_DeactivateIss (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetAllActiveIss
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetAllActiveIss> of PortPrototype <Issm_GetAllActiveIss>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Issm_GetAllActiveIss(uint32 *activeIssField)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Issm_GetAllActiveIss_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetAllActiveIss_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Issm_CODE) Issm_GetAllActiveIss(P2VAR(uint32, AUTOMATIC, RTE_ISSM_APPL_VAR) activeIssField) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_GetAllActiveIss (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetAllActiveUsers
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetAllActiveUsers> of PortPrototype <Issm_GetAllActiveUsers>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Issm_GetAllActiveUsers(uint32 *activeUsersField)
 *     Argument activeUsersField: uint32* is of type Issm_ActiveUserArrayType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Issm_GetAllActiveUsers_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetAllActiveUsers_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Issm_CODE) Issm_GetAllActiveUsers(P2VAR(uint32, AUTOMATIC, RTE_ISSM_APPL_VAR) activeUsersField) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_GetAllActiveUsers (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: GetIssState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ASLight_InputFSP>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_AlarmSetUnset1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_AlarmSetUnset2>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_AlarmTriggers3>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ApproachLights1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_AudioRadio2>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_AudioRadio3>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_AuxHornRequest>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_BlackoutConvoyMode>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_CIOMOperStateRedundancy>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_CabTiltSwitchRequest>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_CityHornRequest>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ClimPHTimerSettings3>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_Dcm>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_DimmingAdjustment1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_DimmingAdjustment2>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_DimmingAdjustment3>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_DimmingAdjustment4>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_DoorOpeningLights5>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ECSStandByActive>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ECSStandByTrigger1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ExteriorLightsRequest1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ExteriorLightsRequest2>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ExtraBBAuxiliarySwitches1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ExtraBBTailLiftFSP2>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_FlexibleSwitchDetection>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ImmobilizerPINCode>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_InteriorLightsRqst1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_KeyfobPowerControl>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_LockControlActDeactivation>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_LockControlCabRqst1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_LockControlCabRqst2>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_LockControlCabRqst2_Pending>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_LockControlKeyfobRqst>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_OtherInteriorLights2>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_OtherInteriorLights3>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_OtherInteriorLights4>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_OtherInteriorLights5>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_PHActMaintainLiving2>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_PHActMaintainLiving5>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_PHActMaintainLiving6>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_PanicAlarmFromKeyfob>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_ParkBrakeAlert5>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_PowerWindowsActivate1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_PowerWindowsActivate3>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_RoofHatchRequest1>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_RoofHatchRequest3>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_SCIM>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_VMChangeOnDoorsOpening>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_WLight_InputELCP>
 *   - triggered by server invocation for OperationPrototype <GetIssState> of PortPrototype <UR_ANW_WLight_InputFSP>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Issm_GetIssState(Issm_IssStateType *issState)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: GetIssState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Issm_CODE) Issm_GetIssState(Issm_UserHandleType parg0, P2VAR(Issm_IssStateType, AUTOMATIC, RTE_ISSM_APPL_VAR) issState) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_GetIssState (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Issm_MainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Issm_IssStateChange_Issm_IssActivation(Issm_IssHandleType issID)
 *     Synchronous Service Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_Issm_IssStateChange_Issm_IssDeactivation(Issm_IssHandleType issID)
 *     Synchronous Service Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_MainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Issm_CODE) Issm_MainFunction(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Issm_MainFunction
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType Issm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Issm_FctPtr = (FncPtrType)TSC_Issm_Rte_Call_Issm_IssStateChange_Issm_IssActivation; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Issm_Rte_Call_Issm_IssStateChange_Issm_IssActivation(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType Issm_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    Issm_FctPtr = (FncPtrType)TSC_Issm_Rte_Call_Issm_IssStateChange_Issm_IssDeactivation; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_Issm_Rte_Call_Issm_IssStateChange_Issm_IssDeactivation(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  TSC_Issm_SchM_Enter_Issm_ISSM_EXCLUSIVE_AREA_0();
  TSC_Issm_SchM_Exit_Issm_ISSM_EXCLUSIVE_AREA_0();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Issm_STOP_SEC_CODE
#include "Issm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void Issm_TestDefines(void)
{
  /* Enumeration Data Types */

  Issm_IssStateType Test_Issm_IssStateType_V_1 = ISSM_STATE_INACTIVE;
  Issm_IssStateType Test_Issm_IssStateType_V_2 = ISSM_STATE_PENDING;
  Issm_IssStateType Test_Issm_IssStateType_V_3 = ISSM_STATE_ACTIVE;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
