/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_VEC_SecurityAccess.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Service interfaces */
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomSeed_RandomSeedFinish(void);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomSeed_RandomSeedStart(void);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomSeed_RandomSeedUpdate(const UInt8 *seedBuffer, UInt32 seedLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength);
Std_ReturnType TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength);

/** Exclusive Areas */
void TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea(void);
void TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea(void);




