/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Keyfob_UICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Read_PredeliveryModeIndication_cmd_PredeliveryModeIndication_cmd(DeactivateActivate_T *data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_ApproachLightButton_Statu_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobLockBtn_DoublePress_KeyfobLockBtn_DoublePress(NotDetected_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobPanicButton_Status_KeyfobPanicButton_Status(KeyfobPanicButton_Status_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobSuperLockButton_Sta_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_MainSwitchButton_Status_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_ApproachLightButton_Statu_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobLockBtn_DoublePress_KeyfobLockBtn_DoublePress(NotDetected_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobPanicButton_Status_KeyfobPanicButton_Status(KeyfobPanicButton_Status_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobSuperLockButton_Sta_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_MainSwitchButton_Status_PushButtonStatus(PushButtonStatus_T data);

/** Client server interfaces */
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_KeyfobButtonStatus_GetKeyfobButtonStatus(uint8 *BatteryStatus, ButtonStatus *ButtonStatus);

/** Service interfaces */
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_Event_D1BUK_16_KeyfobLowBattery_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_Event_D1BUK_63_KeyfobButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_ApproachLights1_ActivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_ApproachLights1_DeactivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_ApproachLights1_GetIssState(Issm_IssStateType *issState);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_KeyfobPowerControl_ActivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_KeyfobPowerControl_DeactivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_KeyfobPowerControl_GetIssState(Issm_IssStateType *issState);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_LockControlKeyfobRqst_ActivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_LockControlKeyfobRqst_DeactivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_LockControlKeyfobRqst_GetIssState(Issm_IssStateType *issState);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PanicAlarmFromKeyfob_ActivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PanicAlarmFromKeyfob_DeactivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PanicAlarmFromKeyfob_GetIssState(Issm_IssStateType *issState);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PowerWindowsActivate1_ActivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PowerWindowsActivate1_DeactivateIss(void);
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PowerWindowsActivate1_GetIssState(Issm_IssStateType *issState);

/** Explicit inter-runnable variables */
void TSC_Keyfob_UICtrl_Rte_IrvRead_DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData_Irv_KeyfobStatus_ReadData(uint8 *data);
void TSC_Keyfob_UICtrl_Rte_IrvWrite_Keyfob_UICtrl_20ms_runnable_Irv_KeyfobStatus_ReadData(uint8 *data);

/** Calibration Component Calibration Parameters */
SEWS_KeyfobType_P1VKL_T  TSC_Keyfob_UICtrl_Rte_Prm_P1VKL_KeyfobType_v(void);
boolean  TSC_Keyfob_UICtrl_Rte_Prm_P1Y1C_SuperlockInhibition_v(void);
boolean  TSC_Keyfob_UICtrl_Rte_Prm_P1C54_FactoryModeActive_v(void);




