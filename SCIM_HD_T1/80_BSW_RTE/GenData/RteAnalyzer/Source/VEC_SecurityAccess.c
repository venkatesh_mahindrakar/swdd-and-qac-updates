/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VEC_SecurityAccess.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  VEC_SecurityAccess
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VEC_SecurityAccess>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * AsymDecryptDataBuffer
 *   
 *
 * AsymDecryptResultBuffer
 *   
 *
 * AsymPrivateKeyType
 *   
 *
 * Csm_ReturnType
 *   
 *
 * RandomGenerateResultBuffer
 *   
 *
 * RandomSeedDataBuffer
 *   
 *
 *
 * Operation Prototypes:
 * =====================
 * AsymDecryptFinish of Port Interface CsmAsymDecrypt
 *   
 *
 * AsymDecryptStart of Port Interface CsmAsymDecrypt
 *   
 *
 * AsymDecryptUpdate of Port Interface CsmAsymDecrypt
 *   
 *
 * RandomGenerate of Port Interface CsmRandomGenerate
 *   
 *
 * RandomSeedFinish of Port Interface CsmRandomSeed
 *   
 *
 * RandomSeedStart of Port Interface CsmRandomSeed
 *   
 *
 * RandomSeedUpdate of Port Interface CsmRandomSeed
 *   
 *
 *********************************************************************************************************************/

#include "Rte_VEC_SecurityAccess.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_VEC_SecurityAccess.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void VEC_SecurityAccess_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * UInt32: Integer in interval [0...4294967295]
 * UInt8: Integer in interval [0.0...0.0]
 *
 * Enumeration Types:
 * ==================
 * Csm_ReturnType: Enumeration of integer in interval [0...255] with enumerators
 *   CSM_E_OK (0U)
 *   CSM_E_NOT_OK (1U)
 *   CSM_E_BUSY (2U)
 *   CSM_E_SMALL_BUFFER (3U)
 *   CSM_E_ENTROPY_EXHAUSTION (4U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 *
 * Array Types:
 * ============
 * AsymDecryptDataBuffer: Array with 128 element(s) of type UInt8
 * AsymDecryptResultBuffer: Array with 128 element(s) of type UInt8
 * Dcm_Data116ByteType: Array with 116 element(s) of type UInt8
 * Dcm_Data128ByteType: Array with 128 element(s) of type UInt8
 * RandomGenerateResultBuffer: Array with 128 element(s) of type UInt8
 * RandomSeedDataBuffer: Array with 128 element(s) of type UInt8
 * Rte_DT_AsymPrivateKeyType_1: Array with 128 element(s) of type UInt8
 *
 * Record Types:
 * =============
 * AsymPrivateKeyType: Record with elements
 *   length of type UInt32
 *   data of type Rte_DT_AsymPrivateKeyType_1
 *
 *********************************************************************************************************************/


#define VEC_SecurityAccess_START_SEC_CODE
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_CallbackNotificationAsymDecrypt
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <JobFinished> of PortPrototype <CsmCallbackAsymDecrypt>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_CallbackNotificationAsymDecrypt(Csm_ReturnType retVal)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmCallback_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationAsymDecrypt_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CallbackNotificationAsymDecrypt(Csm_ReturnType retVal) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationAsymDecrypt (returns application error)
 *********************************************************************************************************************/

  VEC_SecurityAccess_TestDefines();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_CallbackNotificationRandomGenerate
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <JobFinished> of PortPrototype <CsmCallbackRandomGenerate>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_CallbackNotificationRandomGenerate(Csm_ReturnType retVal)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmCallback_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationRandomGenerate_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CallbackNotificationRandomGenerate(Csm_ReturnType retVal) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationRandomGenerate (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_CallbackNotificationRandomSeed
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <JobFinished> of PortPrototype <CsmCallbackRandomSeed>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_CallbackNotificationRandomSeed(Csm_ReturnType retVal)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmCallback_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationRandomSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CallbackNotificationRandomSeed(Csm_ReturnType retVal) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_CallbackNotificationRandomSeed (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_SecurityAccess_CODE) VEC_SecurityAccess_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_Init
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_MainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmRandomSeed_RandomSeedFinish(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomSeed_CSM_E_BUSY, RTE_E_CsmRandomSeed_CSM_E_NOT_OK, RTE_E_CsmRandomSeed_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmRandomSeed_RandomSeedStart(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomSeed_CSM_E_BUSY, RTE_E_CsmRandomSeed_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmRandomSeed_RandomSeedUpdate(const UInt8 *seedBuffer, UInt32 seedLength)
 *     Argument seedBuffer: UInt8* is of type RandomSeedDataBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomSeed_CSM_E_BUSY, RTE_E_CsmRandomSeed_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_MainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VEC_SecurityAccess_CODE) VEC_SecurityAccess_MainFunction(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_MainFunction
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;
  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  RandomSeedDataBuffer Call_CsmRandomSeed_RandomSeedUpdate_seedBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomSeed_RandomSeedFinish();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomSeed_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomSeed_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomSeed_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomSeed_RandomSeedStart();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomSeed_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomSeed_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomSeed_RandomSeedUpdate(Call_CsmRandomSeed_RandomSeedUpdate_seedBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomSeed_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomSeed_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_ProvideEntropy
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ProvideEntropy> of PortPrototype <VEC_SecurityAccess_ProvideEntropy>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_ProvideEntropy(const UInt8 *seedPtr, UInt32 seedLength)
 *     Argument seedPtr: UInt8* is of type RandomSeedDataBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_VEC_SecurityAccess_ProvideEntropy_E_BUSY
 *   RTE_E_VEC_SecurityAccess_ProvideEntropy_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_ProvideEntropy_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_ProvideEntropy(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) seedPtr, UInt32 seedLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_ProvideEntropy (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_01_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_01>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_01_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_01_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_01_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_01_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_01_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_01>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_01_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_01_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_01_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_01_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_03_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_03>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_03_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_03_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_03_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_03_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_03_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_03>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_03_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_03_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_03_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_03_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_05_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_05>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_05_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_05_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_05_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_05_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_05_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_05>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_05_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_05_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_05_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_05_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_07_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_07>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_07_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_07_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_07_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_07_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_07_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_07>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_07_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_07_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_07_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_07_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_09_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_09>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_09_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_09_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_09_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_09_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_09_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_09>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_09_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_09_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_09_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_09_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0B_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_0B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0B_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0B_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0B_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0B_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_0B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0B_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0B_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0B_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0D_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_0D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0D_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0D_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0D_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0D_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_0D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0D_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0D_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0D_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0F_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_0F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0F_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0F_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0F_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_0F_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_0F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_0F_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0F_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_0F_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_11_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_11>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_11_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_11_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_11_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_11_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_11_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_11>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_11_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_11_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_11_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_11_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_13_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_13>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_13_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_13_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_13_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_13_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_13_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_13>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_13_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_13_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_13_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_13_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_15_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_15>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_15_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_15_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_15_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_15_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_15_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_15>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_15_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_15_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_15_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_15_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_17_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_17>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_17_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_17_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_17_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_17_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_17_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_17>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_17_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_17_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_17_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_17_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_19_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_19>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_19_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_19_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_19_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_19_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_19_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_19>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_19_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_19_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_19_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_19_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1B_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_1B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1B_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1B_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1B_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1B_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_1B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1B_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1B_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1B_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1D_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_1D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1D_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1D_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1D_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1D_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_1D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1D_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1D_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1D_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1F_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_1F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1F_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1F_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1F_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_1F_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_1F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_1F_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1F_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_1F_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_21_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_21>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_21_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_21_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_21_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_21_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_21_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_21>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_21_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_21_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_21_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_21_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_23_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_23>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_23_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_23_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_23_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_23_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_23_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_23>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_23_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_23_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_23_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_23_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_25_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_25>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_25_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_25_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_25_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_25_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_25_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_25>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_25_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_25_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_25_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_25_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_27_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_27>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_27_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_27_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_27_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_27_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_27_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_27>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_27_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_27_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_27_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_27_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_29_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_29>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_29_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_29_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_29_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_29_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_29_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_29>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_29_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_29_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_29_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_29_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2B_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_2B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2B_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2B_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2B_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2B_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_2B>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2B_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2B_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2B_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2D_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_2D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2D_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2D_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2D_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2D_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_2D>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2D_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2D_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2D_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2F_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_2F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2F_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2F_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2F_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_2F_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_2F>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_2F_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2F_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_2F_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_31_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_31>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_31_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_31_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_31_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_31_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_31_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_31>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_31_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_31_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_31_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_31_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_33_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_33>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_33_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_33_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_33_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_33_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_33_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_33>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_33_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_33_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_33_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_33_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_35_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_35>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_35_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_35_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_35_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_35_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_35_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_35>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_35_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_35_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_35_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_35_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_37_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_37>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_37_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_37_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_37_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_37_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_37_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_37>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_37_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_37_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_37_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_37_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_SecurityAccess_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_39_CompareKey
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <CompareKey> of PortPrototype <SecurityAccess_SA_Seed_39>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptStart(const AsymPrivateKeyType *key)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   Std_ReturnType Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(const UInt8 *cipherTextBuffer, UInt32 cipherTextLength, UInt8 *plainTextBuffer, UInt32 *plainTextLength)
 *     Argument cipherTextBuffer: UInt8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: UInt8* is of type AsymDecryptResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmAsymDecrypt_CSM_E_BUSY, RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK, RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_39_CompareKey(const UInt8 *Key, Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Key: UInt8* is of type Dcm_Data128ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_39_CompareKey_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_39_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_39_CompareKey (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength = 0U;
  AsymPrivateKeyType Call_CsmAsymDecrypt_AsymDecryptStart_key = {
  0U, {0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U}
};
  AsymDecryptDataBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  AsymDecryptResultBuffer Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  UInt32 Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptFinish_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptStart(&Call_CsmAsymDecrypt_AsymDecryptStart_key);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(Call_CsmAsymDecrypt_AsymDecryptUpdate_cipherTextBuffer, 0U, Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextBuffer, &Call_CsmAsymDecrypt_AsymDecryptUpdate_plainTextLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK:
      fct_error = 1;
      break;
    case RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VEC_SecurityAccess_SA_Seed_39_GetSeed
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetSeed> of PortPrototype <SecurityAccess_SA_Seed_39>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_CsmRandomGenerate_RandomGenerate(UInt8 *resultBuffer, UInt32 resultLength)
 *     Argument resultBuffer: UInt8* is of type RandomGenerateResultBuffer
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_CsmRandomGenerate_CSM_E_BUSY, RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION, RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 * Exclusive Area Access:
 * ======================
 *   void Rte_Enter_ExclusiveArea(void)
 *   void Rte_Exit_ExclusiveArea(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VEC_SecurityAccess_SA_Seed_39_GetSeed(Dcm_OpStatusType OpStatus, UInt8 *Seed, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Seed: UInt8* is of type Dcm_Data116ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_SecurityAccess_DCM_E_PENDING
 *   RTE_E_SecurityAccess_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_39_GetSeed_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_39_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VEC_SecurityAccess_SA_Seed_39_GetSeed (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RandomGenerateResultBuffer Call_CsmRandomGenerate_RandomGenerate_resultBuffer = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_VEC_SecurityAccess_Rte_Call_CsmRandomGenerate_RandomGenerate(Call_CsmRandomGenerate_RandomGenerate_resultBuffer, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_BUSY:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION:
      fct_error = 1;
      break;
    case RTE_E_CsmRandomGenerate_CSM_E_NOT_OK:
      fct_error = 1;
      break;
  }

  TSC_VEC_SecurityAccess_Rte_Enter_ExclusiveArea();
  TSC_VEC_SecurityAccess_Rte_Exit_ExclusiveArea();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VEC_SecurityAccess_STOP_SEC_CODE
#include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void VEC_SecurityAccess_TestDefines(void)
{
  /* Enumeration Data Types */

  Csm_ReturnType Test_Csm_ReturnType_V_1 = CSM_E_OK;
  Csm_ReturnType Test_Csm_ReturnType_V_2 = CSM_E_NOT_OK;
  Csm_ReturnType Test_Csm_ReturnType_V_3 = CSM_E_BUSY;
  Csm_ReturnType Test_Csm_ReturnType_V_4 = CSM_E_SMALL_BUFFER;
  Csm_ReturnType Test_Csm_ReturnType_V_5 = CSM_E_ENTROPY_EXHAUSTION;

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
