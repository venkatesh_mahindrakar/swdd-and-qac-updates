/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_InCabLock_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_InCabLock_HMICtrl.h"
#include "TSC_InCabLock_HMICtrl.h"








Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_BunkH1LockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH1LockButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_BunkH1UnlockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH1UnlockButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_BunkH2LockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH2LockButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_DashboardLockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_DashboardLockButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_Locking_Switch_stat_serialized_Crypto_Function_serialized(uint8 *data)
{
  return Rte_Read_Locking_Switch_stat_serialized_Crypto_Function_serialized(data);
}

Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Living_Living(data);
}




Std_ReturnType TSC_InCabLock_HMICtrl_Rte_Write_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(DoorLockUnlock_T data)
{
  return Rte_Write_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* InCabLock_HMICtrl */
      /* InCabLock_HMICtrl */



