/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Cdd_LinTpHandling.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  Cdd_LinTpHandling
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <Cdd_LinTpHandling>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_Cdd_LinTpHandling.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_Cdd_LinTpHandling.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void Cdd_LinTpHandling_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * CddLinTp_Status: Enumeration of integer in interval [0...255] with enumerators
 *   CDD_LIN_NOT_OK (11U)
 *   CDD_LIN_TX_OK (0U)
 *   CDD_LIN_TX_BUSY (1U)
 *   CDD_LIN_TX_HEADER_ERROR (2U)
 *   CDD_LIN_TX_ERROR (3U)
 *   CDD_LIN_RX_OK (4U)
 *   CDD_LIN_RX_BUSY (5U)
 *   CDD_LIN_RX_ERROR (6U)
 *   CDD_LIN_RX_NO_RESPONSE (7U)
 *   CDD_LIN_NONE (9U)
 *
 *********************************************************************************************************************/


#define Cdd_LinTpHandling_START_SEC_CODE
#include "Cdd_LinTpHandling_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpClearRequest
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ClearRequest> of PortPrototype <CddLinClearRequest>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void Cdd_LinTpClearRequest(void)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpClearRequest_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpClearRequest(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpClearRequest
 *********************************************************************************************************************/

  Cdd_LinTpHandling_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin1EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN1SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN1Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN1Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin1EndofNotification_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin1EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin1EndofNotification
 *********************************************************************************************************************/

  Rte_ModeType_BswMRteMDG_LIN1Schedule Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin2EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN2SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN2Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN2Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin2EndofNotification_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin2EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin2EndofNotification
 *********************************************************************************************************************/

  Rte_ModeType_BswMRteMDG_LIN2Schedule Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule = TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin3EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN3SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN3Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN3Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin3EndofNotification_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin3EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin3EndofNotification
 *********************************************************************************************************************/

  Rte_ModeType_BswMRteMDG_LIN3Schedule Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin4EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN4SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN4Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN4Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin4EndofNotification_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin4EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin4EndofNotification
 *********************************************************************************************************************/

  Rte_ModeType_BswMRteMDG_LIN4Schedule Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpLin5EndofNotification
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on entering of Mode <LIN_SchTable_EndOfNotification> of ModeDeclarationGroupPrototype <BswM_MDGP_BswMRteMDG_LINSchTableState> of PortPrototype <Switch_BswMSP_LIN5SchTableState>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(void)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN5Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN5Schedule
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin5EndofNotification_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin5EndofNotification(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpLin5EndofNotification
 *********************************************************************************************************************/

  Rte_ModeType_BswMRteMDG_LIN5Schedule Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpMainfunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinRxHandling_ReceiveIndication(uint8 TxId, uint8 SubServiceId, uint8 *RxData, uint8 Length, CddLinTp_Status Status)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpMainfunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpMainfunction(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpMainfunction
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  uint8 Call_CddLinRxHandling_ReceiveIndication_RxData = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_Cdd_LinTpHandling_Rte_Call_CddLinRxHandling_ReceiveIndication(0U, 0U, &Call_CddLinRxHandling_ReceiveIndication_RxData, 0U, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Cdd_LinTpTransmit
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Transmit> of PortPrototype <CddLinTxHandling>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   void Cdd_LinTpTransmit(uint8 TxId, uint8 *TxData, uint8 Length)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpTransmit_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpTransmit(uint8 TxId, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINTPHANDLING_APPL_VAR) TxData, uint8 Length) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Cdd_LinTpTransmit
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Cdd_LinTpHandling_STOP_SEC_CODE
#include "Cdd_LinTpHandling_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void Cdd_LinTpHandling_TestDefines(void)
{
  /* Enumeration Data Types */

  CddLinTp_Status Test_CddLinTp_Status_V_1 = CDD_LIN_NOT_OK;
  CddLinTp_Status Test_CddLinTp_Status_V_2 = CDD_LIN_TX_OK;
  CddLinTp_Status Test_CddLinTp_Status_V_3 = CDD_LIN_TX_BUSY;
  CddLinTp_Status Test_CddLinTp_Status_V_4 = CDD_LIN_TX_HEADER_ERROR;
  CddLinTp_Status Test_CddLinTp_Status_V_5 = CDD_LIN_TX_ERROR;
  CddLinTp_Status Test_CddLinTp_Status_V_6 = CDD_LIN_RX_OK;
  CddLinTp_Status Test_CddLinTp_Status_V_7 = CDD_LIN_RX_BUSY;
  CddLinTp_Status Test_CddLinTp_Status_V_8 = CDD_LIN_RX_ERROR;
  CddLinTp_Status Test_CddLinTp_Status_V_9 = CDD_LIN_RX_NO_RESPONSE;
  CddLinTp_Status Test_CddLinTp_Status_V_10 = CDD_LIN_NONE;

  /* Modes */

  uint8 Test_BswMRteMDG_LIN1Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2;
  uint8 Test_BswMRteMDG_LIN1Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E;
  uint8 Test_BswMRteMDG_LIN1Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN1Schedule;

  uint8 Test_BswMRteMDG_LIN2Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN2Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0;
  uint8 Test_BswMRteMDG_LIN2Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL;
  uint8 Test_BswMRteMDG_LIN2Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0;
  uint8 Test_BswMRteMDG_LIN2Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E;
  uint8 Test_BswMRteMDG_LIN2Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN2Schedule;

  uint8 Test_BswMRteMDG_LIN3Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2;
  uint8 Test_BswMRteMDG_LIN3Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E;
  uint8 Test_BswMRteMDG_LIN3Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN3Schedule;

  uint8 Test_BswMRteMDG_LIN4Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2;
  uint8 Test_BswMRteMDG_LIN4Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E;
  uint8 Test_BswMRteMDG_LIN4Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN4Schedule;

  uint8 Test_BswMRteMDG_LIN5Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2;
  uint8 Test_BswMRteMDG_LIN5Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E;
  uint8 Test_BswMRteMDG_LIN5Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN5Schedule;

  uint8 Test_BswMRteMDG_LINSchTableState_MV_1 = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  uint8 Test_BswMRteMDG_LINSchTableState_MV_2 = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT;
  uint8 Test_BswMRteMDG_LINSchTableState_MV_3 = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  uint8 Test_BswMRteMDG_LINSchTableState_TV = RTE_TRANSITION_BswMRteMDG_LINSchTableState;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
