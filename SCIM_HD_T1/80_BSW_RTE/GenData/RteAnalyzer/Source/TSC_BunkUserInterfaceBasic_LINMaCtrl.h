/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_BunkUserInterfaceBasic_LINMaCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_DiagInfoLECMBasic_DiagInfo(DiagInfo_T *data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_LIN_BunkBAudioOnOff_ButtonStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_LIN_BunkBIntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_LIN_BunkBParkHeater_ButtonStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_LIN_BunkBTempDec_ButtonStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_LIN_BunkBTempInc_ButtonStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_LIN_BunkBVolumeDown_ButtonStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_LIN_BunkBVolumeUp_ButtonStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Read_ResponseErrorLECMBasic_ResponseErrorLECMBasic(ResponseErrorLECMBasic_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Write_BunkBAudioOnOff_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Write_BunkBIntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Write_BunkBParkHeater_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Write_BunkBTempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Write_BunkBTempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Write_BunkBVolumeDown_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Write_BunkBVolumeUp_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);

/** Service interfaces */
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_Event_D1BKG_87_LECMLowLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_Event_D1BOG_16_LECMLow_VBT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_Event_D1BOG_17_LECMLow_VAT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_Event_D1BOG_46_LECMLow_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_Event_D1BOG_94_LECMLow_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_UR_ANW_AudioRadio3_ActivateIss(void);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_UR_ANW_AudioRadio3_DeactivateIss(void);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_UR_ANW_OtherInteriorLights5_ActivateIss(void);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_UR_ANW_OtherInteriorLights5_DeactivateIss(void);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_UR_ANW_PHActMaintainLiving5_ActivateIss(void);
Std_ReturnType TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Call_UR_ANW_PHActMaintainLiving5_DeactivateIss(void);

/** Calibration Component Calibration Parameters */
boolean  TSC_BunkUserInterfaceBasic_LINMaCtrl_Rte_Prm_P1CAQ_LECML_Installed_v(void);




