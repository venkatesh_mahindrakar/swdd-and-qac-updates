/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_PowerTakeOff_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_PowerTakeOff_HMICtrl.h"
#include "TSC_PowerTakeOff_HMICtrl.h"








Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto1Indication_Pto1Indication(Request_T *data)
{
  return Rte_Read_Pto1Indication_Pto1Indication(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto1Status_Pto1Status(InactiveActive_T *data)
{
  return Rte_Read_Pto1Status_Pto1Status(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto1SwitchStatus_Pto1SwitchStatus(OffOn_T *data)
{
  return Rte_Read_Pto1SwitchStatus_Pto1SwitchStatus(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto2Indication_Pto2Indication(Request_T *data)
{
  return Rte_Read_Pto2Indication_Pto2Indication(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto2Status_Pto2Status(InactiveActive_T *data)
{
  return Rte_Read_Pto2Status_Pto2Status(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto2SwitchStatus_Pto2SwitchStatus(OffOn_T *data)
{
  return Rte_Read_Pto2SwitchStatus_Pto2SwitchStatus(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto3Indication_Pto3Indication(Request_T *data)
{
  return Rte_Read_Pto3Indication_Pto3Indication(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto3Status_Pto3Status(InactiveActive_T *data)
{
  return Rte_Read_Pto3Status_Pto3Status(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto3SwitchStatus_Pto3SwitchStatus(OffOn_T *data)
{
  return Rte_Read_Pto3SwitchStatus_Pto3SwitchStatus(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto4Indication_Pto4Indication(Request_T *data)
{
  return Rte_Read_Pto4Indication_Pto4Indication(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto4Status_Pto4Status(InactiveActive_T *data)
{
  return Rte_Read_Pto4Status_Pto4Status(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto4SwitchStatus_Pto4SwitchStatus(OffOn_T *data)
{
  return Rte_Read_Pto4SwitchStatus_Pto4SwitchStatus(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_WrcPto1ButtonStatus_WrcPto1ButtonStatus(ButtonStatus_T *data)
{
  return Rte_Read_WrcPto1ButtonStatus_WrcPto1ButtonStatus(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_WrcPto2ButtonStatus_WrcPto2ButtonStatus(ButtonStatus_T *data)
{
  return Rte_Read_WrcPto2ButtonStatus_WrcPto2ButtonStatus(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_WrcPto3ButtonStatus_WrcPto3ButtonStatus(ButtonStatus_T *data)
{
  return Rte_Read_WrcPto3ButtonStatus_WrcPto3ButtonStatus(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Read_WrcPto4ButtonStatus_WrcPto4ButtonStatus(ButtonStatus_T *data)
{
  return Rte_Read_WrcPto4ButtonStatus_WrcPto4ButtonStatus(data);
}




Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Write_PTO1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_PTO1_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Write_PTO2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_PTO2_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Write_PTO3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_PTO3_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Write_PTO4_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_PTO4_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Write_Pto1CabRequest_Pto1CabRequest(OffOn_T data)
{
  return Rte_Write_Pto1CabRequest_Pto1CabRequest(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Write_Pto2CabRequest_Pto2CabRequest(OffOn_T data)
{
  return Rte_Write_Pto2CabRequest_Pto2CabRequest(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Write_Pto3CabRequest_Pto3CabRequest(OffOn_T data)
{
  return Rte_Write_Pto3CabRequest_Pto3CabRequest(data);
}

Std_ReturnType TSC_PowerTakeOff_HMICtrl_Rte_Write_Pto4CabRequest_Pto4CabRequest(OffOn_T data)
{
  return Rte_Write_Pto4CabRequest_Pto4CabRequest(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T  TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BD2_PTO_EmergencyDeactivationTimer_v(void)
{
  return (SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T ) Rte_Prm_P1BD2_PTO_EmergencyDeactivationTimer_v();
}
SEWS_PTO_EmergencyFilteringTimer_P1BD3_T  TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BD3_PTO_EmergencyFilteringTimer_v(void)
{
  return (SEWS_PTO_EmergencyFilteringTimer_P1BD3_T ) Rte_Prm_P1BD3_PTO_EmergencyFilteringTimer_v();
}
SEWS_PTO_RequestFilteringTimer_P1BD4_T  TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BD4_PTO_RequestFilteringTimer_v(void)
{
  return (SEWS_PTO_RequestFilteringTimer_P1BD4_T ) Rte_Prm_P1BD4_PTO_RequestFilteringTimer_v();
}
boolean  TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1AJL_PTO1_Act_v(void)
{
  return (boolean ) Rte_Prm_P1AJL_PTO1_Act_v();
}
boolean  TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1AJM_PTO2_Act_v(void)
{
  return (boolean ) Rte_Prm_P1AJM_PTO2_Act_v();
}
boolean  TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
{
  return (boolean ) Rte_Prm_P1B9X_WirelessRC_Enable_v();
}
boolean  TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BBG_PTO3_Act_v(void)
{
  return (boolean ) Rte_Prm_P1BBG_PTO3_Act_v();
}
boolean  TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BBH_PTO4_Act_v(void)
{
  return (boolean ) Rte_Prm_P1BBH_PTO4_Act_v();
}


     /* PowerTakeOff_HMICtrl */
      /* PowerTakeOff_HMICtrl */



