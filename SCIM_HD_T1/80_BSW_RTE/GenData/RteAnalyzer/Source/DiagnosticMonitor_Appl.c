/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  DiagnosticMonitor_Appl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  DiagnosticMonitor_Appl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <DiagnosticMonitor_Appl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_ComCryptoKey_P1DLX_T
 *   
 *
 * SEWS_CrankingLockActivation_P1DS3_T
 *   
 *
 * SEWS_KeyfobEncryptCode_P1DS4_T
 *   
 *
 * SEWS_LIN_topology_P1AJR_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_DiagnosticMonitor_Appl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_DiagnosticMonitor_Appl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ComCryptoKey_P1DLX_T: Integer in interval [0...255]
 * SEWS_CrankingLockActivation_P1DS3_T: Integer in interval [0...255]
 * SEWS_KeyfobEncryptCode_P1DS4_T: Integer in interval [0...255]
 * SEWS_LIN_topology_P1AJR_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * SEWS_ComCryptoKey_P1DLX_a_T: Array with 16 element(s) of type SEWS_ComCryptoKey_P1DLX_T
 * SEWS_KeyfobEncryptCode_P1DS4_a_T: Array with 24 element(s) of type SEWS_KeyfobEncryptCode_P1DS4_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_LIN_topology_P1AJR_T Rte_Prm_P1AJR_LIN_topology_v(void)
 *   boolean Rte_Prm_P1DXX_FMSgateway_Act_v(void)
 *   SEWS_KeyfobEncryptCode_P1DS4_T *Rte_Prm_P1DS4_KeyfobEncryptCode_v(void)
 *     Returnvalue: SEWS_KeyfobEncryptCode_P1DS4_T* is of type SEWS_KeyfobEncryptCode_P1DS4_a_T
 *   boolean Rte_Prm_P1AW6_IsEcuAvailableDDM_v(void)
 *   boolean Rte_Prm_P1AWY_IsEcuAvailableAlarm_v(void)
 *   boolean Rte_Prm_P1AXE_IsEcuAvailablePDM_v(void)
 *   boolean Rte_Prm_P1TTA_GearBoxLockActivation_v(void)
 *   SEWS_CrankingLockActivation_P1DS3_T Rte_Prm_P1DS3_CrankingLockActivation_v(void)
 *   boolean Rte_Prm_P1C54_FactoryModeActive_v(void)
 *   SEWS_ComCryptoKey_P1DLX_T *Rte_Prm_P1DLX_ComCryptoKey_v(void)
 *     Returnvalue: SEWS_ComCryptoKey_P1DLX_T* is of type SEWS_ComCryptoKey_P1DLX_a_T
 *
 *********************************************************************************************************************/


#define DiagnosticMonitor_Appl_START_SEC_CODE
#include "DiagnosticMonitor_Appl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1QXU_Data_P1QXU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_Appl_CODE) DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_APPL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_LIN_topology_P1AJR_T P1AJR_LIN_topology_v_data;
  boolean P1DXX_FMSgateway_Act_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  boolean P1AW6_IsEcuAvailableDDM_v_data;
  boolean P1AWY_IsEcuAvailableAlarm_v_data;
  boolean P1AXE_IsEcuAvailablePDM_v_data;

  boolean P1TTA_GearBoxLockActivation_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ComCryptoKey_P1DLX_a_T P1DLX_ComCryptoKey_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1AJR_LIN_topology_v_data = TSC_DiagnosticMonitor_Appl_Rte_Prm_P1AJR_LIN_topology_v();
  P1DXX_FMSgateway_Act_v_data = TSC_DiagnosticMonitor_Appl_Rte_Prm_P1DXX_FMSgateway_Act_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_DiagnosticMonitor_Appl_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1AW6_IsEcuAvailableDDM_v_data = TSC_DiagnosticMonitor_Appl_Rte_Prm_P1AW6_IsEcuAvailableDDM_v();
  P1AWY_IsEcuAvailableAlarm_v_data = TSC_DiagnosticMonitor_Appl_Rte_Prm_P1AWY_IsEcuAvailableAlarm_v();
  P1AXE_IsEcuAvailablePDM_v_data = TSC_DiagnosticMonitor_Appl_Rte_Prm_P1AXE_IsEcuAvailablePDM_v();

  P1TTA_GearBoxLockActivation_v_data = TSC_DiagnosticMonitor_Appl_Rte_Prm_P1TTA_GearBoxLockActivation_v();

  P1DS3_CrankingLockActivation_v_data = TSC_DiagnosticMonitor_Appl_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_DiagnosticMonitor_Appl_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(P1DLX_ComCryptoKey_v_data, TSC_DiagnosticMonitor_Appl_Rte_Prm_P1DLX_ComCryptoKey_v(), sizeof(SEWS_ComCryptoKey_P1DLX_a_T));

  return RTE_E_DataServices_P1QXU_Data_P1QXU_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define DiagnosticMonitor_Appl_STOP_SEC_CODE
#include "DiagnosticMonitor_Appl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
