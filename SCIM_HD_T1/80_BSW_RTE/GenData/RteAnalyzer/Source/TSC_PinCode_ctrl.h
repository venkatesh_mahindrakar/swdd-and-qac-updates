/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_PinCode_ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_PinCode_ctrl_Rte_Read_DynamicCode_rqst_DynamicCode_rqst(DynamicCode_rqst_T *data);
Std_ReturnType TSC_PinCode_ctrl_Rte_Read_PinCodeEntered_value_PinCodeEntered_value(Code32bit_T *data);
Std_ReturnType TSC_PinCode_ctrl_Rte_Read_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I(uint8 *data);
Std_ReturnType TSC_PinCode_ctrl_Rte_Read_PinCode_rqst_PinCode_rqst(PinCode_rqst_T *data);
Std_ReturnType TSC_PinCode_ctrl_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_PinCode_ctrl_Rte_Write_DynamicCode_value_DynamicCode_value(Code32bit_T data);
Std_ReturnType TSC_PinCode_ctrl_Rte_Write_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I(const uint8 *data);
Std_ReturnType TSC_PinCode_ctrl_Rte_Write_PinCode_stat_PinCode_stat(PinCode_stat_T data);
Std_ReturnType TSC_PinCode_ctrl_Rte_Write_PinCode_validity_time_PinCode_validity_time(PinCode_validity_time_T data);




