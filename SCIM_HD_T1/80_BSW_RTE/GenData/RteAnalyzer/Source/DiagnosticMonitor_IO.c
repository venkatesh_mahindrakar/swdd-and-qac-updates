/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  DiagnosticMonitor_IO.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  DiagnosticMonitor_IO
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <DiagnosticMonitor_IO>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dem_EventStatusType
 *   
 *
 * IOHWAB_BOOL
 *   
 *
 * IOHWAB_SINT8
 *   
 *
 * IOHWAB_UINT16
 *   
 *
 * IOHWAB_UINT8
 *   
 *
 * SEWS_Diag_Act_DOWHS01_P1V6O_T
 *   
 *
 * SEWS_Diag_Act_DOWHS02_P1V6P_T
 *   
 *
 * SEWS_Diag_Act_DOWLS02_P1V7E_T
 *   
 *
 * SEWS_Diag_Act_DOWLS03_P1V7F_T
 *   
 *
 * SEWS_HwToleranceThreshold_X1C04_T
 *   
 *
 * SEWS_P1V79_P1Interface_T
 *   
 *
 * SEWS_P1V79_P2Interface_T
 *   
 *
 * SEWS_P1V79_P3Interface_T
 *   
 *
 * SEWS_P1V79_P4Interface_T
 *   
 *
 * SEWS_P1V79_PiInterface_T
 *   
 *
 * VGTT_EcuPinFaultStatus
 *   
 *
 * VGTT_EcuPinVoltage_0V2
 *   
 *
 * VGTT_EcuPwmDutycycle
 *   
 *
 * VGTT_EcuPwmPeriod
 *   
 *
 *********************************************************************************************************************/

#include "Rte_DiagnosticMonitor_IO.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_DiagnosticMonitor_IO.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void DiagnosticMonitor_IO_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_BOOL: Boolean
 * IOHWAB_SINT8: Integer in interval [-128...127]
 * IOHWAB_UINT16: Integer in interval [0...65535]
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS01_P1V6O_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS02_P1V6P_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS02_P1V7E_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS03_P1V7F_T: Integer in interval [0...255]
 * SEWS_HwToleranceThreshold_X1C04_T: Integer in interval [0...255]
 * SEWS_P1V79_P1Interface_T: Integer in interval [0...255]
 * SEWS_P1V79_P2Interface_T: Integer in interval [0...255]
 * SEWS_P1V79_P3Interface_T: Integer in interval [0...255]
 * SEWS_P1V79_P4Interface_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * VGTT_EcuPwmDutycycle: Integer in interval [-128...127]
 * VGTT_EcuPwmPeriod: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * sint8: Integer in interval [-128...127] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * SEWS_P1V79_PiInterface_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_P1V79_PiInterface_T_Deactivation (0U)
 *   SEWS_P1V79_PiInterface_T_Activation (1U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 *
 * Array Types:
 * ============
 * DataArrayType_uint8_5: Array with 5 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 *
 * Record Types:
 * =============
 * SEWS_Diag_Act_LF_P_P1V79_s_T: Record with elements
 *   PiInterface of type SEWS_P1V79_PiInterface_T
 *   P1Interface of type SEWS_P1V79_P1Interface_T
 *   P2Interface of type SEWS_P1V79_P2Interface_T
 *   P3Interface of type SEWS_P1V79_P3Interface_T
 *   P4Interface of type SEWS_P1V79_P4Interface_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HwToleranceThreshold_X1C04_T Rte_Prm_X1C04_HwToleranceThreshold_v(void)
 *   boolean Rte_Prm_X1C05_DtcActivationVbatEnable_v(void)
 *   SEWS_Diag_Act_DOWHS01_P1V6O_T Rte_Prm_P1V6O_Diag_Act_DOWHS01_v(void)
 *   SEWS_Diag_Act_DOWHS02_P1V6P_T Rte_Prm_P1V6P_Diag_Act_DOWHS02_v(void)
 *   SEWS_Diag_Act_DOWLS02_P1V7E_T Rte_Prm_P1V7E_Diag_Act_DOWLS02_v(void)
 *   SEWS_Diag_Act_DOWLS03_P1V7F_T Rte_Prm_P1V7F_Diag_Act_DOWLS03_v(void)
 *   boolean Rte_Prm_P1V6I_Diag_Act_AO12_P_v(void)
 *   boolean Rte_Prm_P1V6K_Diag_Act_AO12_L_v(void)
 *   boolean Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v(void)
 *   boolean Rte_Prm_P1V6R_Diag_Act_DOBHS02_v(void)
 *   boolean Rte_Prm_P1V6S_Diag_Act_DOBHS03_v(void)
 *   boolean Rte_Prm_P1V6T_Diag_Act_DOBHS04_v(void)
 *   boolean Rte_Prm_P1V7D_Diag_Act_DOBLS01_v(void)
 *   boolean Rte_Prm_P1V7G_Diag_Act_DAI01_v(void)
 *   boolean Rte_Prm_P1V7H_Diag_Act_DAI02_v(void)
 *   boolean Rte_Prm_P1V8E_Diag_Act_12VDCDC_v(void)
 *   SEWS_Diag_Act_LF_P_P1V79_s_T *Rte_Prm_P1V79_Diag_Act_LF_P_v(void)
 *
 *********************************************************************************************************************/


#define DiagnosticMonitor_IO_START_SEC_CODE
#include "DiagnosticMonitor_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1QXI_Data_P1QXI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0U, &Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated, &Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  DiagnosticMonitor_IO_TestDefines();

  return RTE_E_DataServices_P1QXI_Data_P1QXI_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ1_Data_P1VQ1_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0U, &Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated, &Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VQ1_Data_P1VQ1_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ2_Data_P1VQ2_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0U, &Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated, &Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VQ2_Data_P1VQ2_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ3_Data_P1VQ3_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowhsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowhsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VQ3_Data_P1VQ3_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ4_Data_P1VQ4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowhsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowhsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VQ4_Data_P1VQ4_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ5_Data_P1VQ5_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(0U, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VQ5_Data_P1VQ5_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ6_Data_P1VQ6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(0U, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VQ6_Data_P1VQ6_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ7_Data_P1VQ7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(0U, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VQ7_Data_P1VQ7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ8_Data_P1VQ8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(0U, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VQ8_Data_P1VQ8_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VQ9_Data_P1VQ9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VQ9_Data_P1VQ9_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowhsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowhsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowhsInterface_P_SetDowActive_CS(0U, 0U, 0U, 0, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowhsInterface_P_SetDowActive_CS(0U, 0U, 0U, 0, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowhsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowhsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowhsInterface_P_SetDowActive_CS(0U, 0U, 0U, 0, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowhsInterface_P_SetDowActive_CS(0U, 0U, 0U, 0, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(0U, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(0U, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(0U, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(0U, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRA_Data_P1VRA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRA_Data_P1VRA_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRB_Data_P1VRB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRB_Data_P1VRB_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRC_Data_P1VRC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRC_Data_P1VRC_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRD_Data_P1VRD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRD_Data_P1VRD_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRE_Data_P1VRE_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRE_Data_P1VRE_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRF_Data_P1VRF_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRF_Data_P1VRF_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRG_Data_P1VRG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRG_Data_P1VRG_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRH_Data_P1VRH_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRH_Data_P1VRH_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRI_Data_P1VRI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRI_Data_P1VRI_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRJ_Data_P1VRJ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRJ_Data_P1VRJ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRK_Data_P1VRK_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRK_Data_P1VRK_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRL_Data_P1VRL_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRL_Data_P1VRL_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRM_Data_P1VRM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRM_Data_P1VRM_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRN_Data_P1VRN_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRN_Data_P1VRN_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRO_Data_P1VRO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRO_Data_P1VRO_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRS_Data_P1VRS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(&Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_IsDoActivated, &Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_DoPinVoltage, &Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_BatteryVoltage, &Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRS_Data_P1VRS_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRT_Data_P1VRT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowlsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowlsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRT_Data_P1VRT_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRU_Data_P1VRU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowlsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowlsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRU_Data_P1VRU_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRV_Data_P1VRV_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Strong = FALSE;
  IOHWAB_BOOL Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Weak = FALSE;
  IOHWAB_BOOL Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_DAI = FALSE;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetPullUpState_CS(&Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Strong, &Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Weak, &Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_DAI);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRV_Data_P1VRV_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRW_Data_P1VRW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Strong = FALSE;
  IOHWAB_BOOL Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Weak = FALSE;
  IOHWAB_BOOL Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_DAI = FALSE;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetPullUpState_CS(&Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Strong, &Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Weak, &Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_DAI);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRW_Data_P1VRW_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0U, &Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated, &Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(&Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_IsDoActivated, &Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_DoPinVoltage, &Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_BatteryVoltage, &Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowlsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowlsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowlsInterface_P_SetDowActive_CS(0U, 0U, 0U, 0, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowlsInterface_P_SetDowActive_CS(0U, 0U, 0U, 0, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowlsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowlsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowlsInterface_P_SetDowActive_CS(0U, 0U, 0U, 0, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowlsInterface_P_SetDowActive_CS(0U, 0U, 0U, 0, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  IOHWAB_BOOL Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0U, &Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated, &Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV5_Data_X1CV5_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDcdc12VState_CS_DcDc12vRefVoltage = 0U;
  IOHWAB_BOOL Call_Do12VInterface_P_GetDcdc12VState_CS_IsDcDc12vActivated = FALSE;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDcdc12VState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(&Call_Do12VInterface_P_GetDcdc12VState_CS_DcDc12vRefVoltage, &Call_Do12VInterface_P_GetDcdc12VState_CS_IsDcDc12vActivated, &Call_Do12VInterface_P_GetDcdc12VState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_X1CV5_Data_X1CV5_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  return RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDcdc12VState_CS_DcDc12vRefVoltage = 0U;
  IOHWAB_BOOL Call_Do12VInterface_P_GetDcdc12VState_CS_IsDcDc12vActivated = FALSE;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDcdc12VState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(&Call_Do12VInterface_P_GetDcdc12VState_CS_DcDc12vRefVoltage, &Call_Do12VInterface_P_GetDcdc12VState_CS_IsDcDc12vActivated, &Call_Do12VInterface_P_GetDcdc12VState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DiagnosticMonitor_IO_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DiagMonitor_IO_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *   Std_ReturnType Rte_Call_LFAntennaDiagnostic_P_GetDiagnosticResult(uint8 *ValidFlag, uint8 *STGStatus, uint8 *STBStatus, uint8 *OCStatus, uint8 *OTStatus)
 *     Argument STGStatus: uint8* is of type DataArrayType_uint8_5
 *     Argument STBStatus: uint8* is of type DataArrayType_uint8_5
 *     Argument OCStatus: uint8* is of type DataArrayType_uint8_5
 *     Argument OTStatus: uint8* is of type DataArrayType_uint8_5
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_VbatInterface_I_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_VbatInterface_I_AdcInFailure, RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1AD0_1C_DcDc12v_VOR_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_29_ECU_SignalInvalid_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD0_2F_ECU_SignalErratic_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD9_16_PWR24V_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD9_17_PWR24V_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1AD9_1C_PWR24V_VOR_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_11_DOWHS1_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_12_DOWHS1_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_13_DOWHS1_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_16_DOWHS1_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_17_DOWHS1_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E0K_38_DOWHS1_PWM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E10_11_ADI4_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E10_13_ADI4_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E11_11_ADI5_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E11_13_ADI5_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E12_11_ADI6_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E12_13_ADI6_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E13_11_ADI7_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E13_13_ADI7_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E14_11_ADI8_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E14_13_ADI8_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E15_11_ADI9_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E15_13_ADI9_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E16_11_ADI10_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E16_13_ADI10_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E17_11_ADI11_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E17_13_ADI11_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E18_11_ADI12_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E18_13_ADI12_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E19_11_ADI13_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E19_13_ADI13_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_11_AO12L_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_12_AO12L_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_16_AO12L_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_17_AO12L_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Q_19_AO12L_CAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_11_AO12P_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_12_AO12P_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_16_AO12P_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_17_AO12P_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1R_19_AO12P_CAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_11_DOWHS2_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_12_DOWHS2_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_13_DOWHS2_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_16_DOWHS2_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_17_DOWHS2_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1S_38_DOWHS2_PWM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_11_DOBHS1_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_12_DOBHS1_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_13_DOBHS1_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_16_DOBHS1_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1T_17_DOBHS1_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_11_DOBHS2_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_12_DOBHS2_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_13_DOBHS2_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_16_DOBHS2_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1U_17_DOBHS2_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_11_DOBHS3_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_12_DOBHS3_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_13_DOBHS3_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_16_DOBHS3_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1V_17_DOBHS3_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_11_DOBHS4_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_12_DOBHS4_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_13_DOBHS4_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_16_DOBHS4_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1W_17_DOBHS4_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1X_11_ADI1_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1X_13_ADI1_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Y_11_ADI2_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Y_13_ADI2_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Z_11_ADI3_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E1Z_13_ADI3_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2A_11_ADI14_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2A_13_ADI14_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2B_11_ADI15_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2B_13_ADI15_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2C_11_ADI16_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2C_13_ADI16_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2G_12_DOBLS1_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2G_14_DOBLS1_STGOC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2G_16_DOBLS1_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2G_17_DOBLS1_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_12_DOWLS2_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_14_DOWLS2_STGOC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_16_DOWLS2_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_17_DOWLS2_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2H_38_DOWLS2_PWM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_12_DOWLS3_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_14_DOWLS3_STGOC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_16_DOWLS3_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_17_DOWLS3_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2I_38_DOWLS3_PWM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2L_29_DAI1_SI_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1E2M_29_DAI2_SI_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1A_11_ADI17_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1A_13_ADI17_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1B_11_ADI18_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1B_13_ADI18_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1C_11_ADI19_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1F1C_13_ADI19_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM3_11_LFPi_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM3_12_LFPi_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM3_13_LFPi_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM3_98_LFPi_OverTemperature_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM4_11_LFP4_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM4_12_LFP4_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM4_13_LFP4_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM5_11_LFP1_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM5_12_LFP1_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM5_13_LFP1_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM6_11_LFP2_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM6_12_LFP2_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM6_13_LFP2_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM7_11_LFP3_STG_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM7_12_LFP3_STB_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1FM7_13_LFP3_OC_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagMonitor_IO_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DiagnosticMonitor_IO_CODE) DiagMonitor_IO_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DiagMonitor_IO_10ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  boolean X1C05_DtcActivationVbatEnable_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1V6I_Diag_Act_AO12_P_v_data;
  boolean P1V6K_Diag_Act_AO12_L_v_data;
  boolean P1V6Q_Diag_Act_DOBHS01_v_data;
  boolean P1V6R_Diag_Act_DOBHS02_v_data;
  boolean P1V6S_Diag_Act_DOBHS03_v_data;
  boolean P1V6T_Diag_Act_DOBHS04_v_data;
  boolean P1V7D_Diag_Act_DOBLS01_v_data;
  boolean P1V7G_Diag_Act_DAI01_v_data;
  boolean P1V7H_Diag_Act_DAI02_v_data;
  boolean P1V8E_Diag_Act_12VDCDC_v_data;
  SEWS_Diag_Act_LF_P_P1V79_s_T P1V79_Diag_Act_LF_P_v_data;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;
  IOHWAB_BOOL Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Strong = FALSE;
  IOHWAB_BOOL Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Weak = FALSE;
  IOHWAB_BOOL Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_DAI = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDcdc12VState_CS_DcDc12vRefVoltage = 0U;
  IOHWAB_BOOL Call_Do12VInterface_P_GetDcdc12VState_CS_IsDcDc12vActivated = FALSE;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDcdc12VState_CS_FaultStatus = 0U;
  IOHWAB_BOOL Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus = 0U;
  IOHWAB_BOOL Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus = 0U;
  IOHWAB_BOOL Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_FaultStatus = 0U;
  IOHWAB_BOOL Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowhsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;
  IOHWAB_BOOL Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated = FALSE;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage = 0U;
  VGTT_EcuPwmDutycycle Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle = 0;
  VGTT_EcuPwmPeriod Call_DowlsInterface_P_GetDoPinStateOne_CS_Period = 0U;
  VGTT_EcuPinFaultStatus Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus = 0U;
  uint8 Call_LFAntennaDiagnostic_P_GetDiagnosticResult_ValidFlag = 0U;
  DataArrayType_uint8_5 Call_LFAntennaDiagnostic_P_GetDiagnosticResult_STGStatus = {
  0U, 0U, 0U, 0U, 0U
};
  DataArrayType_uint8_5 Call_LFAntennaDiagnostic_P_GetDiagnosticResult_STBStatus = {
  0U, 0U, 0U, 0U, 0U
};
  DataArrayType_uint8_5 Call_LFAntennaDiagnostic_P_GetDiagnosticResult_OCStatus = {
  0U, 0U, 0U, 0U, 0U
};
  DataArrayType_uint8_5 Call_LFAntennaDiagnostic_P_GetDiagnosticResult_OTStatus = {
  0U, 0U, 0U, 0U, 0U
};
  VGTT_EcuPinVoltage_0V2 Call_VbatInterface_I_GetVbatVoltage_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_VbatInterface_I_GetVbatVoltage_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1C05_DtcActivationVbatEnable_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_X1C05_DtcActivationVbatEnable_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1V6I_Diag_Act_AO12_P_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6I_Diag_Act_AO12_P_v();
  P1V6K_Diag_Act_AO12_L_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6K_Diag_Act_AO12_L_v();
  P1V6Q_Diag_Act_DOBHS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6Q_Diag_Act_DOBHS01_v();
  P1V6R_Diag_Act_DOBHS02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6R_Diag_Act_DOBHS02_v();
  P1V6S_Diag_Act_DOBHS03_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6S_Diag_Act_DOBHS03_v();
  P1V6T_Diag_Act_DOBHS04_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V6T_Diag_Act_DOBHS04_v();
  P1V7D_Diag_Act_DOBLS01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7D_Diag_Act_DOBLS01_v();
  P1V7G_Diag_Act_DAI01_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7G_Diag_Act_DAI01_v();
  P1V7H_Diag_Act_DAI02_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V7H_Diag_Act_DAI02_v();
  P1V8E_Diag_Act_12VDCDC_v_data = TSC_DiagnosticMonitor_IO_Rte_Prm_P1V8E_Diag_Act_12VDCDC_v();
  P1V79_Diag_Act_LF_P_v_data = *TSC_DiagnosticMonitor_IO_Rte_Prm_P1V79_Diag_Act_LF_P_v();

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_AdiInterface_P_GetPullUpState_CS(&Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Strong, &Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_Weak, &Call_AdiInterface_P_GetPullUpState_CS_isPullUpActive_DAI);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(&Call_Do12VInterface_P_GetDcdc12VState_CS_DcDc12vRefVoltage, &Call_Do12VInterface_P_GetDcdc12VState_CS_IsDcDc12vActivated, &Call_Do12VInterface_P_GetDcdc12VState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(0U, &Call_Do12VInterface_P_GetDo12VPinsState_CS_IsDo12VActivated, &Call_Do12VInterface_P_GetDo12VPinsState_CS_Do12VPinVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_BatteryVoltage, &Call_Do12VInterface_P_GetDo12VPinsState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Do12VInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(0U, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_isDioActivated, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_DoPinVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_BatteryVoltage, &Call_DobhsDiagInterface_P_GetDobhsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(&Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_IsDoActivated, &Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_DoPinVoltage, &Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_BatteryVoltage, &Call_DoblsCtrlInterface_P_GetDoblsPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowhsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowhsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowhsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(0U, &Call_DowlsInterface_P_GetDoPinStateOne_CS_IsDoActivated, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DoPinVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_BatteryVoltage, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DutyCycle, &Call_DowlsInterface_P_GetDoPinStateOne_CS_Period, &Call_DowlsInterface_P_GetDoPinStateOne_CS_DiagStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DowxsInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_LFAntennaDiagnostic_P_GetDiagnosticResult(&Call_LFAntennaDiagnostic_P_GetDiagnosticResult_ValidFlag, Call_LFAntennaDiagnostic_P_GetDiagnosticResult_STGStatus, Call_LFAntennaDiagnostic_P_GetDiagnosticResult_STBStatus, Call_LFAntennaDiagnostic_P_GetDiagnosticResult_OCStatus, Call_LFAntennaDiagnostic_P_GetDiagnosticResult_OTStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_VbatInterface_I_GetVbatVoltage_CS(&Call_VbatInterface_I_GetVbatVoltage_CS_BatteryVoltage, &Call_VbatInterface_I_GetVbatVoltage_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_VbatInterface_I_AdcInFailure:
      fct_error = 1;
      break;
    case RTE_E_VbatInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1AD0_1C_DcDc12v_VOR_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1AD0_29_ECU_SignalInvalid_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1AD0_2F_ECU_SignalErratic_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1AD9_16_PWR24V_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1AD9_17_PWR24V_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1AD9_1C_PWR24V_VOR_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E0K_11_DOWHS1_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E0K_12_DOWHS1_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E0K_13_DOWHS1_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E0K_16_DOWHS1_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E0K_17_DOWHS1_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E0K_38_DOWHS1_PWM_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E10_11_ADI4_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E10_13_ADI4_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E11_11_ADI5_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E11_13_ADI5_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E12_11_ADI6_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E12_13_ADI6_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E13_11_ADI7_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E13_13_ADI7_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E14_11_ADI8_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E14_13_ADI8_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E15_11_ADI9_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E15_13_ADI9_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E16_11_ADI10_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E16_13_ADI10_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E17_11_ADI11_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E17_13_ADI11_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E18_11_ADI12_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E18_13_ADI12_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E19_11_ADI13_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E19_13_ADI13_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1Q_11_AO12L_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1Q_12_AO12L_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1Q_16_AO12L_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1Q_17_AO12L_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1Q_19_AO12L_CAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1R_11_AO12P_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1R_12_AO12P_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1R_16_AO12P_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1R_17_AO12P_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1R_19_AO12P_CAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1S_11_DOWHS2_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1S_12_DOWHS2_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1S_13_DOWHS2_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1S_16_DOWHS2_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1S_17_DOWHS2_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1S_38_DOWHS2_PWM_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1T_11_DOBHS1_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1T_12_DOBHS1_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1T_13_DOBHS1_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1T_16_DOBHS1_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1T_17_DOBHS1_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1U_11_DOBHS2_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1U_12_DOBHS2_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1U_13_DOBHS2_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1U_16_DOBHS2_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1U_17_DOBHS2_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1V_11_DOBHS3_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1V_12_DOBHS3_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1V_13_DOBHS3_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1V_16_DOBHS3_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1V_17_DOBHS3_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1W_11_DOBHS4_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1W_12_DOBHS4_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1W_13_DOBHS4_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1W_16_DOBHS4_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1W_17_DOBHS4_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1X_11_ADI1_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1X_13_ADI1_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1Y_11_ADI2_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1Y_13_ADI2_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1Z_11_ADI3_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E1Z_13_ADI3_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2A_11_ADI14_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2A_13_ADI14_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2B_11_ADI15_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2B_13_ADI15_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2C_11_ADI16_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2C_13_ADI16_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2G_12_DOBLS1_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2G_14_DOBLS1_STGOC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2G_16_DOBLS1_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2G_17_DOBLS1_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2H_12_DOWLS2_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2H_14_DOWLS2_STGOC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2H_16_DOWLS2_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2H_17_DOWLS2_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2H_38_DOWLS2_PWM_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2I_12_DOWLS3_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2I_14_DOWLS3_STGOC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2I_16_DOWLS3_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2I_17_DOWLS3_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2I_38_DOWLS3_PWM_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2L_29_DAI1_SI_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1E2M_29_DAI2_SI_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1F1A_11_ADI17_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1F1A_13_ADI17_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1F1B_11_ADI18_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1F1B_13_ADI18_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1F1C_11_ADI19_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1F1C_13_ADI19_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM3_11_LFPi_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM3_12_LFPi_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM3_13_LFPi_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM3_98_LFPi_OverTemperature_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM4_11_LFP4_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM4_12_LFP4_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM4_13_LFP4_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM5_11_LFP1_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM5_12_LFP1_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM5_13_LFP1_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM6_11_LFP2_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM6_12_LFP2_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM6_13_LFP2_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM7_11_LFP3_STG_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM7_12_LFP3_STB_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DiagnosticMonitor_IO_Rte_Call_Event_D1FM7_13_LFP3_OC_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define DiagnosticMonitor_IO_STOP_SEC_CODE
#include "DiagnosticMonitor_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void DiagnosticMonitor_IO_TestDefines(void)
{
  /* Enumeration Data Types */

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  IOCtrlReq_T Test_IOCtrlReq_T_V_1 = IOCtrl_AppRequest;
  IOCtrlReq_T Test_IOCtrlReq_T_V_2 = IOCtrl_DiagReturnCtrlToApp;
  IOCtrlReq_T Test_IOCtrlReq_T_V_3 = IOCtrl_DiagShortTermAdjust;

  SEWS_P1V79_PiInterface_T Test_SEWS_P1V79_PiInterface_T_V_1 = SEWS_P1V79_PiInterface_T_Deactivation;
  SEWS_P1V79_PiInterface_T Test_SEWS_P1V79_PiInterface_T_V_2 = SEWS_P1V79_PiInterface_T_Activation;

  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_1 = TestNotRun;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_2 = OffState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_3 = OffState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_4 = OffState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_5 = OffState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_6 = OffState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_7 = OffState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_8 = OnState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_9 = OnState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_10 = OnState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_11 = OnState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_12 = OnState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_13 = OnState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_14 = OnState_FaultDetected_VOR;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_15 = OnState_FaultDetected_CAT;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
