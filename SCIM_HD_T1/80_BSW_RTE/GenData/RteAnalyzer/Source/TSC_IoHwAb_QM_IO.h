/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_IoHwAb_QM_IO.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_IoHwAb_QM_IO_Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data);
Std_ReturnType TSC_IoHwAb_QM_IO_Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data);
Std_ReturnType TSC_IoHwAb_QM_IO_Rte_Read_ScimPvtControl_P_Status(uint8 *data);
Std_ReturnType TSC_IoHwAb_QM_IO_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);

/** Client server interfaces */
Std_ReturnType TSC_IoHwAb_QM_IO_Rte_Call_EcuHwState_P_GetEcuVoltages_CS(VGTT_EcuPinVoltage_0V2 *EcuVoltageValues);
Std_ReturnType TSC_IoHwAb_QM_IO_Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus);

/** Explicit inter-runnable variables */
void TSC_IoHwAb_QM_IO_Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data);
void TSC_IoHwAb_QM_IO_Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data);

/** Calibration Component Calibration Parameters */
SEWS_HwToleranceThreshold_X1C04_T  TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v(void);
SEWS_PcbConfig_DoorAccessIf_X1CX3_T  TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(void);
SEWS_PcbConfig_LinInterfaces_X1CX0_a_T * TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void);
SEWS_PcbConfig_CanInterfaces_X1CX2_a_T * TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(void);
SEWS_PcbConfig_Adi_X1CXW_a_T * TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(void);
SEWS_PcbConfig_DOWHS_X1CXY_a_T * TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(void);
SEWS_PcbConfig_DOWLS_X1CXZ_a_T * TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(void);
SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v(void);
SEWS_PcbConfig_AdiPullUp_X1CX5_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(void);
SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void);
SEWS_Diag_Act_DOWHS01_P1V6O_T  TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v(void);
SEWS_Diag_Act_DOWHS02_P1V6P_T  TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v(void);
SEWS_Diag_Act_DOWLS02_P1V7E_T  TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v(void);
SEWS_Diag_Act_DOWLS03_P1V7F_T  TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v(void);
boolean  TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v(void);
boolean  TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v(void);
boolean  TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v(void);
boolean  TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v(void);
SEWS_AdiWakeUpConfig_P1WMD_a_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(void);
SEWS_Fault_Config_ADI07_P1V60_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v(void);
SEWS_Fault_Config_ADI08_P1V61_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v(void);
SEWS_Fault_Config_ADI09_P1V62_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v(void);
SEWS_Fault_Config_ADI10_P1V63_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v(void);
SEWS_Fault_Config_ADI11_P1V64_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v(void);
SEWS_Fault_Config_ADI12_P1V65_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v(void);
SEWS_Fault_Config_ADI13_P1V66_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v(void);
SEWS_Fault_Config_ADI14_P1V67_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v(void);
SEWS_Fault_Config_ADI15_P1V68_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v(void);
SEWS_Fault_Config_ADI16_P1V69_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v(void);
SEWS_Fault_Config_ADI01_P1V6U_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v(void);
SEWS_Fault_Config_ADI02_P1V6V_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v(void);
SEWS_Fault_Config_ADI03_P1V6W_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v(void);
SEWS_Fault_Config_ADI04_P1V6X_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v(void);
SEWS_Fault_Config_ADI05_P1V6Y_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v(void);
SEWS_Fault_Config_ADI06_P1V6Z_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v(void);
SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v(void);
SEWS_DAI_Installed_P1WMP_s_T * TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v(void);
boolean  TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v(void);




