/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  EngineSpeedControl_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  EngineSpeedControl_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <EngineSpeedControl_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_BodybuilderAccessToAccelPedal_P1B72_T
 *   
 *
 * SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T
 *   
 *
 * Speed16bit_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_EngineSpeedControl_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_EngineSpeedControl_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void EngineSpeedControl_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_BodybuilderAccessToAccelPedal_P1B72_T: Integer in interval [0...255]
 * SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * AcceleratorPedalStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   AcceleratorPedalStatus_SecPedNotActPrimPedAct (0U)
 *   AcceleratorPedalStatus_SecPedActPrimPedNotAct (1U)
 *   AcceleratorPedalStatus_PrimAndSecPedalInhibited (2U)
 *   AcceleratorPedalStatus_Spare1 (3U)
 *   AcceleratorPedalStatus_Spare2 (4U)
 *   AcceleratorPedalStatus_Spare3 (5U)
 *   AcceleratorPedalStatus_Error (6U)
 *   AcceleratorPedalStatus_NotAvailable (7U)
 * ButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   ButtonStatus_Idle (0U)
 *   ButtonStatus_Pressed (1U)
 *   ButtonStatus_Error (2U)
 *   ButtonStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DisableEnable_T: Enumeration of integer in interval [0...3] with enumerators
 *   DisableEnable_Disable (0U)
 *   DisableEnable_Enable (1U)
 *   DisableEnable_Error (2U)
 *   DisableEnable_NotAvailable (3U)
 * EngineSpeedControlStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineSpeedControlStatus_Inactive (0U)
 *   EngineSpeedControlStatus_ActiveManual (1U)
 *   EngineSpeedControlStatus_ActiveExternalSource (2U)
 *   EngineSpeedControlStatus_CrossCountryEngineSpeedControlActive (3U)
 *   EngineSpeedControlStatus_AutoPtoMin_AutoNeutral (4U)
 *   EngineSpeedControlStatus_Spare_01 (5U)
 *   EngineSpeedControlStatus_Error (6U)
 *   EngineSpeedControlStatus_NotAvailable (7U)
 * EngineSpeedRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineSpeedRequest_NoRequest (0U)
 *   EngineSpeedRequest_Off (1U)
 *   EngineSpeedRequest_Activate (2U)
 *   EngineSpeedRequest_Increase (3U)
 *   EngineSpeedRequest_Decrease (4U)
 *   EngineSpeedRequest_Spare (5U)
 *   EngineSpeedRequest_Error (6U)
 *   EngineSpeedRequest_NotAvailable (7U)
 * EscActionRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   EscActionRequest_NoRequest (0U)
 *   EscActionRequest_Resume (1U)
 *   EscActionRequest_Increase (2U)
 *   EscActionRequest_Decrease (3U)
 *   EscActionRequest_Set (4U)
 *   EscActionRequest_Spare (5U)
 *   EscActionRequest_Error (6U)
 *   EscActionRequest_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * SWSpdCtrlButtonsStatus1_T: Enumeration of integer in interval [0...15] with enumerators
 *   SWSpdCtrlButtonsStatus1_None (0U)
 *   SWSpdCtrlButtonsStatus1_PauseOff (1U)
 *   SWSpdCtrlButtonsStatus1_Resume (2U)
 *   SWSpdCtrlButtonsStatus1_Increase (3U)
 *   SWSpdCtrlButtonsStatus1_Decrease (4U)
 *   SWSpdCtrlButtonsStatus1_Enter (5U)
 *   SWSpdCtrlButtonsStatus1_CC (6U)
 *   SWSpdCtrlButtonsStatus1_Overspeed (7U)
 *   SWSpdCtrlButtonsStatus1_ACC (8U)
 *   SWSpdCtrlButtonsStatus1_TimeGap (9U)
 *   SWSpdCtrlButtonsStatus1_Spare (10U)
 *   SWSpdCtrlButtonsStatus1_Spare_01 (11U)
 *   SWSpdCtrlButtonsStatus1_Spare_02 (12U)
 *   SWSpdCtrlButtonsStatus1_Spare_03 (13U)
 *   SWSpdCtrlButtonsStatus1_Error (14U)
 *   SWSpdCtrlButtonsStatus1_NotAvaillable (15U)
 * SWSpeedControlAdjustMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   SWSpeedControlAdjustMode_AdjustSpeed (0U)
 *   SWSpeedControlAdjustMode_AdjustOverspeed (1U)
 *   SWSpeedControlAdjustMode_AdjustTimeGap (2U)
 *   SWSpeedControlAdjustMode_Spare (3U)
 *   SWSpeedControlAdjustMode_Spare_01 (4U)
 *   SWSpeedControlAdjustMode_Spare_02 (5U)
 *   SWSpeedControlAdjustMode_Spare_03 (6U)
 *   SWSpeedControlAdjustMode_Spare_04 (7U)
 *   SWSpeedControlAdjustMode_Spare_05 (8U)
 *   SWSpeedControlAdjustMode_Spare_06 (9U)
 *   SWSpeedControlAdjustMode_Spare_07 (10U)
 *   SWSpeedControlAdjustMode_Spare_08 (11U)
 *   SWSpeedControlAdjustMode_Spare_09 (12U)
 *   SWSpeedControlAdjustMode_Spare_10 (13U)
 *   SWSpeedControlAdjustMode_Error (14U)
 *   SWSpeedControlAdjustMode_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T Rte_Prm_P1IZ3_ESC_InhibitionByPrimaryPedal_v(void)
 *   boolean Rte_Prm_P1B0X_EngineSpeedControlSw_v(void)
 *   SEWS_BodybuilderAccessToAccelPedal_P1B72_T Rte_Prm_P1B72_BodybuilderAccessToAccelPedal_v(void)
 *   boolean Rte_Prm_P1B0W_CrossCountryCC_Act_v(void)
 *
 *********************************************************************************************************************/


#define EngineSpeedControl_HMICtrl_START_SEC_CODE
#include "EngineSpeedControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: EngineSpeedControl_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ACCEnableRqst_ACCEnableRqst(DisableEnable_T *data)
 *   Std_ReturnType Rte_Read_AcceleratorPedalStatus_AcceleratorPedalStatus(AcceleratorPedalStatus_T *data)
 *   Std_ReturnType Rte_Read_CCEnableRequest_CCEnableRequest(DisableEnable_T *data)
 *   Std_ReturnType Rte_Read_EngineSpeedControlStatus_EngineSpeedControlStatus(EngineSpeedControlStatus_T *data)
 *   Std_ReturnType Rte_Read_EscButtonMuddySiteStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchEnableStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchIncDecStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchMuddySiteStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_EscSwitchResumeStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1(SWSpdCtrlButtonsStatus1_T *data)
 *   Std_ReturnType Rte_Read_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode(SWSpeedControlAdjustMode_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *   Std_ReturnType Rte_Read_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus(DisableEnable_T *data)
 *   Std_ReturnType Rte_Read_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat(ButtonStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AdjustRequestForIdle_AdjustRequestForIdle(EngineSpeedRequest_T data)
 *   Std_ReturnType Rte_Write_EscButtonMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_EscCabActionRequest_EscCabActionRequest(EscActionRequest_T data)
 *   Std_ReturnType Rte_Write_EscCabEnable_EscCabEnable(DisableEnable_T data)
 *   Std_ReturnType Rte_Write_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst(DisableEnable_T data)
 *   Std_ReturnType Rte_Write_EscSwitchEnableDeviceInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_EscSwitchMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: EngineSpeedControl_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, EngineSpeedControl_HMICtrl_CODE) EngineSpeedControl_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: EngineSpeedControl_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  DisableEnable_T Read_ACCEnableRqst_ACCEnableRqst;
  AcceleratorPedalStatus_T Read_AcceleratorPedalStatus_AcceleratorPedalStatus;
  DisableEnable_T Read_CCEnableRequest_CCEnableRequest;
  EngineSpeedControlStatus_T Read_EngineSpeedControlStatus_EngineSpeedControlStatus;
  PushButtonStatus_T Read_EscButtonMuddySiteStatus_PushButtonStatus;
  PushButtonStatus_T Read_EscSwitchEnableStatus_PushButtonStatus;
  A3PosSwitchStatus_T Read_EscSwitchIncDecStatus_A3PosSwitchStatus;
  PushButtonStatus_T Read_EscSwitchMuddySiteStatus_PushButtonStatus;
  PushButtonStatus_T Read_EscSwitchResumeStatus_PushButtonStatus;
  SWSpdCtrlButtonsStatus1_T Read_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1;
  SWSpeedControlAdjustMode_T Read_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;
  Speed16bit_T Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed;
  ButtonStatus_T Read_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat;
  DisableEnable_T Read_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus;
  ButtonStatus_T Read_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat;
  ButtonStatus_T Read_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat;

  SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T P1IZ3_ESC_InhibitionByPrimaryPedal_v_data;
  boolean P1B0X_EngineSpeedControlSw_v_data;

  SEWS_BodybuilderAccessToAccelPedal_P1B72_T P1B72_BodybuilderAccessToAccelPedal_v_data;
  boolean P1B0W_CrossCountryCC_Act_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1IZ3_ESC_InhibitionByPrimaryPedal_v_data = TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1IZ3_ESC_InhibitionByPrimaryPedal_v();
  P1B0X_EngineSpeedControlSw_v_data = TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1B0X_EngineSpeedControlSw_v();

  P1B72_BodybuilderAccessToAccelPedal_v_data = TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1B72_BodybuilderAccessToAccelPedal_v();
  P1B0W_CrossCountryCC_Act_v_data = TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1B0W_CrossCountryCC_Act_v();

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_ACCEnableRqst_ACCEnableRqst(&Read_ACCEnableRqst_ACCEnableRqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_AcceleratorPedalStatus_AcceleratorPedalStatus(&Read_AcceleratorPedalStatus_AcceleratorPedalStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_CCEnableRequest_CCEnableRequest(&Read_CCEnableRequest_CCEnableRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_EngineSpeedControlStatus_EngineSpeedControlStatus(&Read_EngineSpeedControlStatus_EngineSpeedControlStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscButtonMuddySiteStatus_PushButtonStatus(&Read_EscButtonMuddySiteStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscSwitchEnableStatus_PushButtonStatus(&Read_EscSwitchEnableStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscSwitchIncDecStatus_A3PosSwitchStatus(&Read_EscSwitchIncDecStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscSwitchMuddySiteStatus_PushButtonStatus(&Read_EscSwitchMuddySiteStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscSwitchResumeStatus_PushButtonStatus(&Read_EscSwitchResumeStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1(&Read_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode(&Read_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(&Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat(&Read_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus(&Read_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat(&Read_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Read_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat(&Read_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Write_AdjustRequestForIdle_AdjustRequestForIdle(Rte_InitValue_AdjustRequestForIdle_AdjustRequestForIdle);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscButtonMuddySiteDeviceInd_DeviceIndication(Rte_InitValue_EscButtonMuddySiteDeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscCabActionRequest_EscCabActionRequest(Rte_InitValue_EscCabActionRequest_EscCabActionRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscCabEnable_EscCabEnable(Rte_InitValue_EscCabEnable_EscCabEnable);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst(Rte_InitValue_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscSwitchEnableDeviceInd_DeviceIndication(Rte_InitValue_EscSwitchEnableDeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscSwitchMuddySiteDeviceInd_DeviceIndication(Rte_InitValue_EscSwitchMuddySiteDeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  EngineSpeedControl_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: EngineSpeedControl_HMICtrl_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: EngineSpeedControl_HMICtrl_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, EngineSpeedControl_HMICtrl_CODE) EngineSpeedControl_HMICtrl_init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: EngineSpeedControl_HMICtrl_init
 *********************************************************************************************************************/

  SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T P1IZ3_ESC_InhibitionByPrimaryPedal_v_data;
  boolean P1B0X_EngineSpeedControlSw_v_data;

  SEWS_BodybuilderAccessToAccelPedal_P1B72_T P1B72_BodybuilderAccessToAccelPedal_v_data;
  boolean P1B0W_CrossCountryCC_Act_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1IZ3_ESC_InhibitionByPrimaryPedal_v_data = TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1IZ3_ESC_InhibitionByPrimaryPedal_v();
  P1B0X_EngineSpeedControlSw_v_data = TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1B0X_EngineSpeedControlSw_v();

  P1B72_BodybuilderAccessToAccelPedal_v_data = TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1B72_BodybuilderAccessToAccelPedal_v();
  P1B0W_CrossCountryCC_Act_v_data = TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1B0W_CrossCountryCC_Act_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define EngineSpeedControl_HMICtrl_STOP_SEC_CODE
#include "EngineSpeedControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void EngineSpeedControl_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_1 = A3PosSwitchStatus_Middle;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_2 = A3PosSwitchStatus_Lower;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_3 = A3PosSwitchStatus_Upper;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_4 = A3PosSwitchStatus_Spare;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_5 = A3PosSwitchStatus_Spare_01;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_6 = A3PosSwitchStatus_Spare_02;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_7 = A3PosSwitchStatus_Error;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_8 = A3PosSwitchStatus_NotAvailable;

  AcceleratorPedalStatus_T Test_AcceleratorPedalStatus_T_V_1 = AcceleratorPedalStatus_SecPedNotActPrimPedAct;
  AcceleratorPedalStatus_T Test_AcceleratorPedalStatus_T_V_2 = AcceleratorPedalStatus_SecPedActPrimPedNotAct;
  AcceleratorPedalStatus_T Test_AcceleratorPedalStatus_T_V_3 = AcceleratorPedalStatus_PrimAndSecPedalInhibited;
  AcceleratorPedalStatus_T Test_AcceleratorPedalStatus_T_V_4 = AcceleratorPedalStatus_Spare1;
  AcceleratorPedalStatus_T Test_AcceleratorPedalStatus_T_V_5 = AcceleratorPedalStatus_Spare2;
  AcceleratorPedalStatus_T Test_AcceleratorPedalStatus_T_V_6 = AcceleratorPedalStatus_Spare3;
  AcceleratorPedalStatus_T Test_AcceleratorPedalStatus_T_V_7 = AcceleratorPedalStatus_Error;
  AcceleratorPedalStatus_T Test_AcceleratorPedalStatus_T_V_8 = AcceleratorPedalStatus_NotAvailable;

  ButtonStatus_T Test_ButtonStatus_T_V_1 = ButtonStatus_Idle;
  ButtonStatus_T Test_ButtonStatus_T_V_2 = ButtonStatus_Pressed;
  ButtonStatus_T Test_ButtonStatus_T_V_3 = ButtonStatus_Error;
  ButtonStatus_T Test_ButtonStatus_T_V_4 = ButtonStatus_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DisableEnable_T Test_DisableEnable_T_V_1 = DisableEnable_Disable;
  DisableEnable_T Test_DisableEnable_T_V_2 = DisableEnable_Enable;
  DisableEnable_T Test_DisableEnable_T_V_3 = DisableEnable_Error;
  DisableEnable_T Test_DisableEnable_T_V_4 = DisableEnable_NotAvailable;

  EngineSpeedControlStatus_T Test_EngineSpeedControlStatus_T_V_1 = EngineSpeedControlStatus_Inactive;
  EngineSpeedControlStatus_T Test_EngineSpeedControlStatus_T_V_2 = EngineSpeedControlStatus_ActiveManual;
  EngineSpeedControlStatus_T Test_EngineSpeedControlStatus_T_V_3 = EngineSpeedControlStatus_ActiveExternalSource;
  EngineSpeedControlStatus_T Test_EngineSpeedControlStatus_T_V_4 = EngineSpeedControlStatus_CrossCountryEngineSpeedControlActive;
  EngineSpeedControlStatus_T Test_EngineSpeedControlStatus_T_V_5 = EngineSpeedControlStatus_AutoPtoMin_AutoNeutral;
  EngineSpeedControlStatus_T Test_EngineSpeedControlStatus_T_V_6 = EngineSpeedControlStatus_Spare_01;
  EngineSpeedControlStatus_T Test_EngineSpeedControlStatus_T_V_7 = EngineSpeedControlStatus_Error;
  EngineSpeedControlStatus_T Test_EngineSpeedControlStatus_T_V_8 = EngineSpeedControlStatus_NotAvailable;

  EngineSpeedRequest_T Test_EngineSpeedRequest_T_V_1 = EngineSpeedRequest_NoRequest;
  EngineSpeedRequest_T Test_EngineSpeedRequest_T_V_2 = EngineSpeedRequest_Off;
  EngineSpeedRequest_T Test_EngineSpeedRequest_T_V_3 = EngineSpeedRequest_Activate;
  EngineSpeedRequest_T Test_EngineSpeedRequest_T_V_4 = EngineSpeedRequest_Increase;
  EngineSpeedRequest_T Test_EngineSpeedRequest_T_V_5 = EngineSpeedRequest_Decrease;
  EngineSpeedRequest_T Test_EngineSpeedRequest_T_V_6 = EngineSpeedRequest_Spare;
  EngineSpeedRequest_T Test_EngineSpeedRequest_T_V_7 = EngineSpeedRequest_Error;
  EngineSpeedRequest_T Test_EngineSpeedRequest_T_V_8 = EngineSpeedRequest_NotAvailable;

  EscActionRequest_T Test_EscActionRequest_T_V_1 = EscActionRequest_NoRequest;
  EscActionRequest_T Test_EscActionRequest_T_V_2 = EscActionRequest_Resume;
  EscActionRequest_T Test_EscActionRequest_T_V_3 = EscActionRequest_Increase;
  EscActionRequest_T Test_EscActionRequest_T_V_4 = EscActionRequest_Decrease;
  EscActionRequest_T Test_EscActionRequest_T_V_5 = EscActionRequest_Set;
  EscActionRequest_T Test_EscActionRequest_T_V_6 = EscActionRequest_Spare;
  EscActionRequest_T Test_EscActionRequest_T_V_7 = EscActionRequest_Error;
  EscActionRequest_T Test_EscActionRequest_T_V_8 = EscActionRequest_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_1 = SWSpdCtrlButtonsStatus1_None;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_2 = SWSpdCtrlButtonsStatus1_PauseOff;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_3 = SWSpdCtrlButtonsStatus1_Resume;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_4 = SWSpdCtrlButtonsStatus1_Increase;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_5 = SWSpdCtrlButtonsStatus1_Decrease;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_6 = SWSpdCtrlButtonsStatus1_Enter;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_7 = SWSpdCtrlButtonsStatus1_CC;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_8 = SWSpdCtrlButtonsStatus1_Overspeed;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_9 = SWSpdCtrlButtonsStatus1_ACC;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_10 = SWSpdCtrlButtonsStatus1_TimeGap;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_11 = SWSpdCtrlButtonsStatus1_Spare;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_12 = SWSpdCtrlButtonsStatus1_Spare_01;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_13 = SWSpdCtrlButtonsStatus1_Spare_02;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_14 = SWSpdCtrlButtonsStatus1_Spare_03;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_15 = SWSpdCtrlButtonsStatus1_Error;
  SWSpdCtrlButtonsStatus1_T Test_SWSpdCtrlButtonsStatus1_T_V_16 = SWSpdCtrlButtonsStatus1_NotAvaillable;

  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_1 = SWSpeedControlAdjustMode_AdjustSpeed;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_2 = SWSpeedControlAdjustMode_AdjustOverspeed;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_3 = SWSpeedControlAdjustMode_AdjustTimeGap;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_4 = SWSpeedControlAdjustMode_Spare;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_5 = SWSpeedControlAdjustMode_Spare_01;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_6 = SWSpeedControlAdjustMode_Spare_02;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_7 = SWSpeedControlAdjustMode_Spare_03;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_8 = SWSpeedControlAdjustMode_Spare_04;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_9 = SWSpeedControlAdjustMode_Spare_05;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_10 = SWSpeedControlAdjustMode_Spare_06;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_11 = SWSpeedControlAdjustMode_Spare_07;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_12 = SWSpeedControlAdjustMode_Spare_08;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_13 = SWSpeedControlAdjustMode_Spare_09;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_14 = SWSpeedControlAdjustMode_Spare_10;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_15 = SWSpeedControlAdjustMode_Error;
  SWSpeedControlAdjustMode_T Test_SWSpeedControlAdjustMode_T_V_16 = SWSpeedControlAdjustMode_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
