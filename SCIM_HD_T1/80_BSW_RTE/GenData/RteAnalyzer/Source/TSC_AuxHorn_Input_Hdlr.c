/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_AuxHorn_Input_Hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_AuxHorn_Input_Hdlr.h"
#include "TSC_AuxHorn_Input_Hdlr.h"








Std_ReturnType TSC_AuxHorn_Input_Hdlr_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Living_Living(data);
}




Std_ReturnType TSC_AuxHorn_Input_Hdlr_Rte_Write_AH_PushButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_AH_PushButtonStatus_PushButtonStatus(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_AuxHorn_Input_Hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef, AdiPinVoltage, BatteryVoltage, FaultStatus);
}


     /* Service calls */
Std_ReturnType TSC_AuxHorn_Input_Hdlr_Rte_Call_Event_D1A8N_11_AuxHorn_STG_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1A8N_11_AuxHorn_STG_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_AuxHorn_Input_Hdlr_Rte_Call_UR_ANW_AuxHornRequest_ActivateIss(void)
{
  return Rte_Call_UR_ANW_AuxHornRequest_ActivateIss();
}
Std_ReturnType TSC_AuxHorn_Input_Hdlr_Rte_Call_UR_ANW_AuxHornRequest_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_AuxHornRequest_DeactivateIss();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_AuxHornMaxActivationTime_X1CYZ_T  TSC_AuxHorn_Input_Hdlr_Rte_Prm_X1CYZ_AuxHornMaxActivationTime_v(void)
{
  return (SEWS_AuxHornMaxActivationTime_X1CYZ_T ) Rte_Prm_X1CYZ_AuxHornMaxActivationTime_v();
}
SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_AuxHorn_Input_Hdlr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
{
  return (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *) Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
}


     /* AuxHorn_Input_Hdlr */
      /* AuxHorn_Input_Hdlr */



