/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DiagnosticComponent.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DiagnosticComponent.h"
#include "TSC_DiagnosticComponent.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Days8bit_Fact025_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Day_ReadData_IrvDayUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_First_Day_ReadData_IrvDayUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Hours8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Hour_ReadData_IrvHoursUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_First_Hour_ReadData_IrvHoursUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Minutes8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Minutes_ReadData_IrvMinutesUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_First_Minutes_ReadData_IrvMinutesUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Months8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Month_ReadData_IrvMonthUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_First_Month_ReadData_IrvMonthUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Seconds8bitFact025_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Seconds_ReadData_IrvSecondsUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_First_Seconds_ReadData_IrvSecondsUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Years8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_First_Year_ReadData_IrvYearUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_First_Year_ReadData_IrvYearUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Days8bit_Fact025_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Day_ReadData_IrvDayUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Day_ReadData_IrvDayUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Hours8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Hour_ReadData_IrvHoursUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Hour_ReadData_IrvHoursUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Minutes8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData_IrvMinutesUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData_IrvMinutesUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Months8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Month_ReadData_IrvMonthUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Month_ReadData_IrvMonthUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Seconds8bitFact025_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData_IrvSecondsUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData_IrvSecondsUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Years8bit_T TSC_DiagnosticComponent_Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Year_ReadData_IrvYearUTC(void)
{
return Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Year_ReadData_IrvYearUTC();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_DiagnosticComponent_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_DiagnosticComponent_Rte_Write_DiagActiveState_isDiagActive(DiagActiveState_T data)
{
  return Rte_Write_DiagActiveState_isDiagActive(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DiagnosticComponent_Rte_Call_UR_ANW_Dcm_ActivateIss(void)
{
  return Rte_Call_UR_ANW_Dcm_ActivateIss();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DiagnosticComponent_Rte_Call_UR_ANW_Dcm_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_Dcm_DeactivateIss();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DiagnosticComponent_Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(Dem_OperationCycleStateType CycleState)
{
  return Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(CycleState);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_DiagnosticComponent_Rte_Read_DayUTC_DayUTC(Days8bit_Fact025_T *data)
{
  return Rte_Read_DayUTC_DayUTC(data);
}

Std_ReturnType TSC_DiagnosticComponent_Rte_Read_HoursUTC_HoursUTC(Hours8bit_T *data)
{
  return Rte_Read_HoursUTC_HoursUTC(data);
}

Std_ReturnType TSC_DiagnosticComponent_Rte_Read_LpModeRunTime_LpModeRunTime(uint32 *data)
{
  return Rte_Read_LpModeRunTime_LpModeRunTime(data);
}

Std_ReturnType TSC_DiagnosticComponent_Rte_Read_MinutesUTC_MinutesUTC(Minutes8bit_T *data)
{
  return Rte_Read_MinutesUTC_MinutesUTC(data);
}

Std_ReturnType TSC_DiagnosticComponent_Rte_Read_MonthUTC_MonthUTC(Months8bit_T *data)
{
  return Rte_Read_MonthUTC_MonthUTC(data);
}

Std_ReturnType TSC_DiagnosticComponent_Rte_Read_SecondsUTC_SecondsUTC(Seconds8bitFact025_T *data)
{
  return Rte_Read_SecondsUTC_SecondsUTC(data);
}

Std_ReturnType TSC_DiagnosticComponent_Rte_Read_YearUTC_YearUTC(Years8bit_T *data)
{
  return Rte_Read_YearUTC_YearUTC(data);
}


boolean TSC_DiagnosticComponent_Rte_IsUpdated_SecondsUTC_SecondsUTC(void)
{
  return Rte_IsUpdated_SecondsUTC_SecondsUTC();
}



Std_ReturnType TSC_DiagnosticComponent_Rte_Write_LpModeRunTime_LpModeRunTime(uint32 data)
{
  return Rte_Write_LpModeRunTime_LpModeRunTime(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
Days8bit_Fact025_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvDayUTC(void)
{
return Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvDayUTC();
}
Hours8bit_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC(void)
{
return Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC();
}
Minutes8bit_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC(void)
{
return Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC();
}
Months8bit_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC(void)
{
return Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC();
}
Seconds8bitFact025_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC(void)
{
return Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC();
}
Years8bit_T TSC_DiagnosticComponent_Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvYearUTC(void)
{
return Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvYearUTC();
}

void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvDayUTC(Days8bit_Fact025_T data)
{
  Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvDayUTC( data);
}
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC(Hours8bit_T data)
{
  Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC( data);
}
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC(Minutes8bit_T data)
{
  Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC( data);
}
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC(Months8bit_T data)
{
  Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC( data);
}
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC(Seconds8bitFact025_T data)
{
  Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC( data);
}
void TSC_DiagnosticComponent_Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvYearUTC(Years8bit_T data)
{
  Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvYearUTC( data);
}






     /* DiagnosticComponent */
      /* DiagnosticComponent */



