/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SCIM_PVTPT_IO.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_SCIM_PVTPT_IO.h"
#include "TSC_SCIM_PVTPT_IO.h"








Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VDCDC(Debug_PVT_SCIM_Ctrl_12VDCDC *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VDCDC(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VLiving(Debug_PVT_SCIM_Ctrl_12VLiving *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VLiving(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VParked(Debug_PVT_SCIM_Ctrl_12VParked *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_12VParked(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS1(Debug_PVT_SCIM_Ctrl_BHS1 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS1(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS2(Debug_PVT_SCIM_Ctrl_BHS2 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS2(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS3(Debug_PVT_SCIM_Ctrl_BHS3 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS3(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS4(Debug_PVT_SCIM_Ctrl_BHS4 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BHS4(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BLS1(Debug_PVT_SCIM_Ctrl_BLS1 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_BLS1(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_DAIPullUp(Debug_PVT_SCIM_Ctrl_DAIPullUp *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_DAIPullUp(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_Generic1(Debug_PVT_SCIM_Ctrl_Generic1 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_Generic1(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS1(Debug_PVT_SCIM_Ctrl_WHS1 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS1(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS2(Debug_PVT_SCIM_Ctrl_WHS2 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WHS2(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS2(Debug_PVT_SCIM_Ctrl_WLS2 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS2(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS3(Debug_PVT_SCIM_Ctrl_WLS3 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_SCIM_Ctrl_WLS3(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS1(Debug_PVT_ScimHwSelect_WHS1 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS1(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS2(Debug_PVT_ScimHwSelect_WHS2 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WHS2(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS2(Debug_PVT_ScimHwSelect_WLS2 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS2(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS3(Debug_PVT_ScimHwSelect_WLS3 *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHwSelect_WLS3(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Duty(Debug_PVT_ScimHw_W_Duty *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Duty(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Freq(Debug_PVT_ScimHw_W_Freq *data)
{
  return Rte_Read_Debug_PVT_CtrlGroup_Debug_PVT_ScimHw_W_Freq(data);
}




Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_ScimPvtControl_P_Status(uint8 data)
{
  return Rte_Write_ScimPvtControl_P_Status(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_AdiInterface_P_SetPullUp_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp, IOHWAB_BOOL ActivateWeakPullUp, IOHWAB_BOOL ActivateDAIPullUp)
{
  return Rte_Call_AdiInterface_P_SetPullUp_CS(IOCtrlReqType, ActivateStrongPullUp, ActivateWeakPullUp, ActivateDAIPullUp);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
{
  return Rte_Call_DowhsInterface_P_SetDowActive_CS(IOCtrlReqType, OutputId, Period, DutyCycle, Activation);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
{
  return Rte_Call_DowlsInterface_P_SetDowActive_CS(IOCtrlReqType, OutputId, Period, DutyCycle, Activation);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Read_Debug_PVT_ADI_ReportRequest_Debug_PVT_ADI_ReportRequest(Debug_PVT_ADI_ReportRequest *data)
{
  return Rte_Read_Debug_PVT_ADI_ReportRequest_Debug_PVT_ADI_ReportRequest(data);
}




Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_ADI_ReportGroup(Debug_PVT_ADI_ReportGroup data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_ADI_ReportGroup(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue(Debug_PVT_DOWHS1_ReportedValue data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS1_ReportedValue(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue(Debug_PVT_DOWHS2_ReportedValue data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWHS2_ReportedValue(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue(Debug_PVT_DOWLS2_ReportedValue data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS2_ReportedValue(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue(Debug_PVT_DOWLS3_ReportedValue data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_DOWLS3_ReportedValue(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCFault(Debug_PVT_SCIM_RD_12VDCDCFault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCFault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCVolt(Debug_PVT_SCIM_RD_12VDCDCVolt data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VDCDCVolt(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingFault(Debug_PVT_SCIM_RD_12VLivingFault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingFault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingVolt(Debug_PVT_SCIM_RD_12VLivingVolt data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VLivingVolt(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedFault(Debug_PVT_SCIM_RD_12VParkedFault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedFault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedVolt(Debug_PVT_SCIM_RD_12VParkedVolt data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_12VParkedVolt(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7(Debug_PVT_SCIM_RD_ADI01_7 data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI01_7(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8(Debug_PVT_SCIM_RD_ADI02_8 data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI02_8(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9(Debug_PVT_SCIM_RD_ADI03_9 data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI03_9(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10(Debug_PVT_SCIM_RD_ADI04_10 data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI04_10(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11(Debug_PVT_SCIM_RD_ADI05_11 data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI05_11(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12(Debug_PVT_SCIM_RD_ADI06_12 data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_ADI06_12(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Fault(Debug_PVT_SCIM_RD_BHS1_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Volt(Debug_PVT_SCIM_RD_BHS1_Volt data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS1_Volt(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Fault(Debug_PVT_SCIM_RD_BHS2_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Volt(Debug_PVT_SCIM_RD_BHS2_Volt data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS2_Volt(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Fault(Debug_PVT_SCIM_RD_BHS3_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Volt(Debug_PVT_SCIM_RD_BHS3_Volt data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS3_Volt(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Fault(Debug_PVT_SCIM_RD_BHS4_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Volt(Debug_PVT_SCIM_RD_BHS4_Volt data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BHS4_Volt(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Fault(Debug_PVT_SCIM_RD_BLS1_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Volt(Debug_PVT_SCIM_RD_BLS1_Volt data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_BLS1_Volt(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2(Debug_PVT_SCIM_RD_DAI1_2 data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_DAI1_2(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT(Debug_PVT_SCIM_RD_VBAT data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT_Fault(Debug_PVT_SCIM_RD_VBAT_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_VBAT_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Fault(Debug_PVT_SCIM_RD_WHS1_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Freq(Debug_PVT_SCIM_RD_WHS1_Freq data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_Freq(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD(Debug_PVT_SCIM_RD_WHS1_VD data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS1_VD(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Fault(Debug_PVT_SCIM_RD_WHS2_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Freq(Debug_PVT_SCIM_RD_WHS2_Freq data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_Freq(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD(Debug_PVT_SCIM_RD_WHS2_VD data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WHS2_VD(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Fault(Debug_PVT_SCIM_RD_WLS2_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Freq(Debug_PVT_SCIM_RD_WLS2_Freq data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_Freq(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD(Debug_PVT_SCIM_RD_WLS2_VD data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS2_VD(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Fault(Debug_PVT_SCIM_RD_WLS3_Fault data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Fault(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Freq(Debug_PVT_SCIM_RD_WLS3_Freq data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_Freq(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD(Debug_PVT_SCIM_RD_WLS3_VD data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_RD_WLS3_VD(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Long(Debug_PVT_SCIM_TSincePwrOn_Long data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Long(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Short(Debug_PVT_SCIM_TSincePwrOn_Short data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSincePwrOn_Short(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSinceWkUp_Short(Debug_PVT_SCIM_TSinceWkUp_Short data)
{
  return Rte_Write_Debug_PVT_ReportGroup_Debug_PVT_SCIM_TSinceWkUp_Short(data);
}

Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Write_Request_SwcModeRequest_PvtReportCtrl_requestedMode(BswM_BswMRteMDG_PvtReport_X1C14 data)
{
  return Rte_Write_Request_SwcModeRequest_PvtReportCtrl_requestedMode(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef, AdiPinVoltage, BatteryVoltage, FaultStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
{
  return Rte_Call_AdiInterface_P_GetPullUpState_CS(isPullUpActive_Strong, isPullUpActive_Weak, isPullUpActive_DAI);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(DcDc12vRefVoltage, IsDcDc12vActivated, FaultStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(SelectParkedOrLivingPin, IsDo12VActivated, Do12VPinVoltage, BatteryVoltage, FaultStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DobhsCtrlInterface_P_1_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_DobhsCtrlInterface_P_1_GetDobhsPinState_CS(IsDoActivated, DoPinVoltage, BatteryVoltage, FaultStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DobhsCtrlInterface_P_2_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_DobhsCtrlInterface_P_2_GetDobhsPinState_CS(IsDoActivated, DoPinVoltage, BatteryVoltage, FaultStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DobhsCtrlInterface_P_3_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_DobhsCtrlInterface_P_3_GetDobhsPinState_CS(IsDoActivated, DoPinVoltage, BatteryVoltage, FaultStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DobhsCtrlInterface_P_4_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_DobhsCtrlInterface_P_4_GetDobhsPinState_CS(IsDoActivated, DoPinVoltage, BatteryVoltage, FaultStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_DobhsDiagInterface_P_GetDobhsPinState_CS(DoPinRef, isDioActivated, DoPinVoltage, BatteryVoltage, FaultStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_DoblsCtrlInterface_P_GetDoblsPinState_CS(IsDoActivated, DoPinVoltage, BatteryVoltage, FaultStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
{
  return Rte_Call_DowhsInterface_P_GetDoPinStateOne_CS(DoPinRef, IsDoActivated, DoPinVoltage, BatteryVoltage, DutyCycle, Period, DiagStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
{
  return Rte_Call_DowlsInterface_P_GetDoPinStateOne_CS(DoPinRef, IsDoActivated, DoPinVoltage, BatteryVoltage, DutyCycle, Period, DiagStatus);
}
Std_ReturnType TSC_SCIM_PVTPT_IO_Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_VbatInterface_P_GetVbatVoltage_CS(BatteryVoltage, FaultStatus);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_Pvt_ActivateReporting_X1C14_T  TSC_SCIM_PVTPT_IO_Rte_Prm_X1C14_Pvt_ActivateReporting_v(void)
{
  return (SEWS_Pvt_ActivateReporting_X1C14_T ) Rte_Prm_X1C14_Pvt_ActivateReporting_v();
}


     /* SCIM_PVTPT_IO */
      /* SCIM_PVTPT_IO */



