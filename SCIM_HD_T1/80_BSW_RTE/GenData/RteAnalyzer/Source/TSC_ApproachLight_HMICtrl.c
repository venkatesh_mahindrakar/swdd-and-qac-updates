/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ApproachLight_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_ApproachLight_HMICtrl.h"
#include "TSC_ApproachLight_HMICtrl.h"








Std_ReturnType TSC_ApproachLight_HMICtrl_Rte_Read_ApproachLightButton_Status_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_ApproachLightButton_Status_PushButtonStatus(data);
}

Std_ReturnType TSC_ApproachLight_HMICtrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}




Std_ReturnType TSC_ApproachLight_HMICtrl_Rte_Write_ApproachLight_HMI_rqst_ApproachLight_HMI_rqst(InactiveActive_T data)
{
  return Rte_Write_ApproachLight_HMI_rqst_ApproachLight_HMI_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* ApproachLight_HMICtrl */
      /* ApproachLight_HMICtrl */



