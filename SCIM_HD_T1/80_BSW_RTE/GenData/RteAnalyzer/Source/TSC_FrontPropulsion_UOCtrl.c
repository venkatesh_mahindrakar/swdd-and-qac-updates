/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_FrontPropulsion_UOCtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_FrontPropulsion_UOCtrl.h"
#include "TSC_FrontPropulsion_UOCtrl.h"








Std_ReturnType TSC_FrontPropulsion_UOCtrl_Rte_Read_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status(DeactivateActivate_T *data)
{
  return Rte_Read_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status(data);
}

Std_ReturnType TSC_FrontPropulsion_UOCtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}




Std_ReturnType TSC_FrontPropulsion_UOCtrl_Rte_Write_FrtAxleHydro_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_FrtAxleHydro_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





boolean  TSC_FrontPropulsion_UOCtrl_Rte_Prm_P1SDA_OptitrackSystemInstalled_v(void)
{
  return (boolean ) Rte_Prm_P1SDA_OptitrackSystemInstalled_v();
}


     /* FrontPropulsion_UOCtrl */
      /* FrontPropulsion_UOCtrl */



