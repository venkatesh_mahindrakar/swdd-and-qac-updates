/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SwivelSeat_ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  SwivelSeat_ctrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <SwivelSeat_ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T
 *   
 *
 * Speed16bit_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_SwivelSeat_ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_SwivelSeat_ctrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void SwivelSeat_ctrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * SeatSwivelStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   SeatSwivelStatus_SeatIsEmptyOrSeatHaveaCorrectPosition (0U)
 *   SeatSwivelStatus_SeatHaveAnIncorrectPosition (1U)
 *   SeatSwivelStatus_Error (2U)
 *   SeatSwivelStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T Rte_Prm_P1DWQ_SwivelSeatCheck_SetSpeed_v(void)
 *   boolean Rte_Prm_P1CUD_SwivelSeatWarning_Act_v(void)
 *
 *********************************************************************************************************************/


#define SwivelSeat_ctrl_START_SEC_CODE
#include "SwivelSeat_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SwivelSeat_ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_SeatSwivelStatus_SeatSwivelStatus(SeatSwivelStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst(InactiveActive_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SwivelSeat_ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SwivelSeat_ctrl_CODE) SwivelSeat_ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: SwivelSeat_ctrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SeatSwivelStatus_T Read_SeatSwivelStatus_SeatSwivelStatus;
  VehicleModeDistribution_T Read_SwcActivation_EngineRun_EngineRun;
  Speed16bit_T Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed;

  SEWS_SwivelSeatCheck_SetSpeed_P1DWQ_T P1DWQ_SwivelSeatCheck_SetSpeed_v_data;

  boolean P1CUD_SwivelSeatWarning_Act_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1DWQ_SwivelSeatCheck_SetSpeed_v_data = TSC_SwivelSeat_ctrl_Rte_Prm_P1DWQ_SwivelSeatCheck_SetSpeed_v();

  P1CUD_SwivelSeatWarning_Act_v_data = TSC_SwivelSeat_ctrl_Rte_Prm_P1CUD_SwivelSeatWarning_Act_v();

  fct_status = TSC_SwivelSeat_ctrl_Rte_Read_SeatSwivelStatus_SeatSwivelStatus(&Read_SeatSwivelStatus_SeatSwivelStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SwivelSeat_ctrl_Rte_Read_SwcActivation_EngineRun_EngineRun(&Read_SwcActivation_EngineRun_EngineRun);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SwivelSeat_ctrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(&Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SwivelSeat_ctrl_Rte_Write_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst(Rte_InitValue_SeatSwivelWarning_rqst_SeatSwivelWarning_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  SwivelSeat_ctrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SwivelSeat_ctrl_STOP_SEC_CODE
#include "SwivelSeat_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void SwivelSeat_ctrl_TestDefines(void)
{
  /* Enumeration Data Types */

  InactiveActive_T Test_InactiveActive_T_V_1 = InactiveActive_Inactive;
  InactiveActive_T Test_InactiveActive_T_V_2 = InactiveActive_Active;
  InactiveActive_T Test_InactiveActive_T_V_3 = InactiveActive_Error;
  InactiveActive_T Test_InactiveActive_T_V_4 = InactiveActive_NotAvailable;

  SeatSwivelStatus_T Test_SeatSwivelStatus_T_V_1 = SeatSwivelStatus_SeatIsEmptyOrSeatHaveaCorrectPosition;
  SeatSwivelStatus_T Test_SeatSwivelStatus_T_V_2 = SeatSwivelStatus_SeatHaveAnIncorrectPosition;
  SeatSwivelStatus_T Test_SeatSwivelStatus_T_V_3 = SeatSwivelStatus_Error;
  SeatSwivelStatus_T Test_SeatSwivelStatus_T_V_4 = SeatSwivelStatus_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
