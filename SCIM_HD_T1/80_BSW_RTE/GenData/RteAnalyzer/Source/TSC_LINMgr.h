/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_LINMgr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_LINMgr_Rte_Read_LinDiagRequestFlag_CCNADRequest(uint8 *data);
Std_ReturnType TSC_LINMgr_Rte_Read_LinDiagRequestFlag_PNSNRequest(uint8 *data);
Std_ReturnType TSC_LINMgr_Rte_Read_SwcActivation_LIN_SwcActivation_LIN(VehicleModeDistribution_T *data);
Std_ReturnType TSC_LINMgr_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);
Std_ReturnType TSC_LINMgr_Rte_Read_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(Boolean *data);
Std_ReturnType TSC_LINMgr_Rte_Read_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(Boolean *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type data);
Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type data);
Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type data);
Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type data);
Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type data);
Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN6_ComMode_LIN(ComMode_LIN_Type data);
Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN7_ComMode_LIN(ComMode_LIN_Type data);
Std_ReturnType TSC_LINMgr_Rte_Write_ComMode_LIN8_ComMode_LIN(ComMode_LIN_Type data);
Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN1_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN1Schedule data);
Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN2_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN2Schedule data);
Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN3_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN3Schedule data);
Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN4_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN4Schedule data);
Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN5_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN5Schedule data);
Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN6_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN6Schedule data);
Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN7_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN7Schedule data);
Std_ReturnType TSC_LINMgr_Rte_Write_Request_LIN8_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN8Schedule data);

/** Client server interfaces */
Std_ReturnType TSC_LINMgr_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus);

/** Service interfaces */
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJO_88_LIN1busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJP_88_LIN2busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJQ_88_LIN3busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJR_88_LIN4busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LINMgr_Rte_Call_Event_D1BJS_88_LIN5busCIOM_BusOff_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN00_ace1a6ba_GetCurrentComMode(ComM_ModeType *ComMode);
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN01_4323cd84_GetCurrentComMode(ComM_ModeType *ComMode);
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN02_a8147687_GetCurrentComMode(ComM_ModeType *ComMode);
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN03_47d61db9_GetCurrentComMode(ComM_ModeType *ComMode);
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN04_a50a06c0_GetCurrentComMode(ComM_ModeType *ComMode);
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN05_4ac86dfe_GetCurrentComMode(ComM_ModeType *ComMode);
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN06_a1ffd6fd_GetCurrentComMode(ComM_ModeType *ComMode);
Std_ReturnType TSC_LINMgr_Rte_Call_UR_CN_LIN07_4e3dbdc3_GetCurrentComMode(ComM_ModeType *ComMode);

/** Mode switches */
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(void);
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(void);
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(void);
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(void);
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(void);
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(void);
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(void);
uint8 TSC_LINMgr_Rte_Mode_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(void);

/** Explicit inter-runnable variables */
uint8 TSC_LINMgr_Rte_IrvRead_DataServices_P1CXF_Data_P1CXF_FCI_ReadData_Irv_DID_FCI(void);
Boolean TSC_LINMgr_Rte_IrvRead_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig(void);
void TSC_LINMgr_Rte_IrvWrite_RoutineServices_R1AAJ_FlexibleSwitchDetection_Start_Irv_RID_SwitchDetectionTrig(Boolean);
uint8 TSC_LINMgr_Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(void);
Boolean TSC_LINMgr_Rte_IrvRead_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig(void);
void TSC_LINMgr_Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_DID_FCI(uint8);
void TSC_LINMgr_Rte_IrvWrite_SCIM_LINMgr_20ms_runnable_Irv_RID_SwitchDetectionTrig(Boolean);

/** Calibration Component Calibration Parameters */
SEWS_PcbConfig_LinInterfaces_X1CX0_a_T * TSC_LINMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void);
SEWS_LIN_topology_P1AJR_T  TSC_LINMgr_Rte_Prm_P1AJR_LIN_topology_v(void);
boolean  TSC_LINMgr_Rte_Prm_P1WPP_isSecurityLinActive_v(void);




