/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_Rtm.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_Rtm.h"
#include "TSC_SchM_Rtm.h"
void TSC_Rtm_SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_0(void)
{
  SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_0();
}
void TSC_Rtm_SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_0(void)
{
  SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_0();
}
void TSC_Rtm_SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_1(void)
{
  SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_1();
}
void TSC_Rtm_SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_1(void)
{
  SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_1();
}
void TSC_Rtm_SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_2(void)
{
  SchM_Enter_Rtm_RTM_EXCLUSIVE_AREA_2();
}
void TSC_Rtm_SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_2(void)
{
  SchM_Exit_Rtm_RTM_EXCLUSIVE_AREA_2();
}
