/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DemSatellite_0.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DemSatellite_0.h"
#include "TSC_DemSatellite_0.h"















     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataClass_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataClass_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_DemDataElementClass_StartApplication_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_DemDataElementClass_StartApplication_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1AFR_Data_P1AFR_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1AFR_Data_P1AFR_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1AFS_Data_P1AFS_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1AFS_Data_P1AFS_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1AFT_Data_P1AFT_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1AFT_Data_P1AFT_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1CXF_Data_P1CXF_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1CXF_Data_P1CXF_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1DCT_Data_P1DCT_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1DCT_Data_P1DCT_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1DCU_Data_P1DCU_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1DCU_Data_P1DCU_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1QXI_Data_P1QXI_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1QXI_Data_P1QXI_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1QXJ_Data_P1QXJ_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1QXJ_Data_P1QXJ_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1QXM_Data_P1QXM_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1QXM_Data_P1QXM_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1QXP_Data_P1QXP_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1QXP_Data_P1QXP_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_P1QXR_Data_P1QXR_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_P1QXR_Data_P1QXR_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_First_Day_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_First_Day_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_First_Hour_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_First_Hour_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_First_Minutes_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_First_Minutes_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_First_Month_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_First_Month_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_First_Seconds_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_First_Seconds_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_First_Year_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_First_Year_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Day_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_Latest_Day_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Hour_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_Latest_Hour_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Month_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_Latest_Month_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData(Data);
}
Std_ReturnType TSC_DemSatellite_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Year_ReadData(uint8 *Data)
{
  return Rte_Call_CBReadData_UTCTimeStamp_Latest_Year_ReadData(Data);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* DemSatellite_0 */
      /* DemSatellite_0 */



