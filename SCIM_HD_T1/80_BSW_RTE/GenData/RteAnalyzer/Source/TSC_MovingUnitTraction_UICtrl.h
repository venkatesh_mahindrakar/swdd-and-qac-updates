/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_MovingUnitTraction_UICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_DifflockDeactivationBtn_stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_DifflockMode_Wheelstatus_FreeWheel_Status(FreeWheel_Status_T *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(uint8 *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_RearAxleDiffLock_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_ASROffButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_Construction_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status(DeactivateActivate_T *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(uint8 *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_Offroad_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_AutoFrontWheelDriveDrvReq_AutoFrontWheelDriveDrvReq(DisengageEngage_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_FrAxleDiffLockActvnDriverRqst_FrAxleDiffLockActvnDriverRqst(DisengageEngage_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(const uint8 *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_RearAxleDiffLockActvnDrvrReq_RearAxleDiffLockActvnDrvrReq(DisengageEngage_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_RearDiffLock_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_RrInterAxlDiffLockActvnDrvrReq_RrInterAxlDiffLockActvnDrvrReq(DisengageEngage_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_SplitBoxDifflockActvnDrvrRqst_TransferBDifflockActvnDrvrRqst(DisengageEngage_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_ASROff_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_ConstructionSwitch_DeviceInd_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_ConstructionSwitch_stat_ConstructionSwitch_stat(OffOn_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(const uint8 *data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_Offroad_Indication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Write_TractionControlDriverRqst_TractionControlDriverRqst(TractionControlDriverRqst_T data);

/** Service interfaces */
Std_ReturnType TSC_MovingUnitTraction_UICtrl_Rte_Call_Event_D1BOW_63_TractionSw_Stuck_SetEventStatus(Dem_EventStatusType EventStatus);

/** Calibration Component Calibration Parameters */
SEWS_AxleConfiguration_P1B16_T  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1B16_AxleConfiguration_v(void);
SEWS_FrontAxleArrangement_P1CSH_T  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1CSH_FrontAxleArrangement_v(void);
SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v(void);
SEWS_WheelDifferentialLockPushButtonType_P1UG1_T  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1UG1_WheelDifferentialLockPushButtonType_v(void);
SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v(void);
boolean  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1B03_RearWheelDiffLockPushSw_v(void);
boolean  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1B04_DLFW_Installed_v(void);
boolean  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1CUC_ConstructionSw_Act_v(void);
boolean  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1F7J_ASROffRoadFullVersion_v(void);
boolean  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1FNW_ASROffButtonInstalled_v(void);
boolean  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1NQA_AutomaticFrontWheelDrive_Act_v(void);
boolean  TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1SDA_OptitrackSystemInstalled_v(void);




