/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Keyfob_UICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_Keyfob_UICtrl.h"
#include "TSC_Keyfob_UICtrl.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_Keyfob_UICtrl_Rte_IrvRead_DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData_Irv_KeyfobStatus_ReadData(uint8 *data)
{
Rte_IrvRead_DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData_Irv_KeyfobStatus_ReadData( data);
}






Std_ReturnType TSC_Keyfob_UICtrl_Rte_Read_PredeliveryModeIndication_cmd_PredeliveryModeIndication_cmd(DeactivateActivate_T *data)
{
  return Rte_Read_PredeliveryModeIndication_cmd_PredeliveryModeIndication_cmd(data);
}

Std_ReturnType TSC_Keyfob_UICtrl_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Security_SwcActivation_Security(data);
}

Std_ReturnType TSC_Keyfob_UICtrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}




Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_ApproachLightButton_Statu_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_ApproachLightButton_Statu_PushButtonStatus(data);
}

Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobLockBtn_DoublePress_KeyfobLockBtn_DoublePress(NotDetected_T data)
{
  return Rte_Write_KeyfobLockBtn_DoublePress_KeyfobLockBtn_DoublePress(data);
}

Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_KeyfobLockButton_Status_PushButtonStatus(data);
}

Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobPanicButton_Status_KeyfobPanicButton_Status(KeyfobPanicButton_Status_T data)
{
  return Rte_Write_KeyfobPanicButton_Status_KeyfobPanicButton_Status(data);
}

Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobSuperLockButton_Sta_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_KeyfobSuperLockButton_Sta_PushButtonStatus(data);
}

Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_KeyfobUnlockButton_Status_PushButtonStatus(data);
}

Std_ReturnType TSC_Keyfob_UICtrl_Rte_Write_MainSwitchButton_Status_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_MainSwitchButton_Status_PushButtonStatus(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_KeyfobButtonStatus_GetKeyfobButtonStatus(uint8 *BatteryStatus, ButtonStatus *ButtonStatus)
{
  return Rte_Call_KeyfobButtonStatus_GetKeyfobButtonStatus(BatteryStatus, ButtonStatus);
}


     /* Service calls */
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_Event_D1BUK_16_KeyfobLowBattery_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BUK_16_KeyfobLowBattery_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_Event_D1BUK_63_KeyfobButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BUK_63_KeyfobButtonStuck_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_ApproachLights1_ActivateIss(void)
{
  return Rte_Call_UR_ANW_ApproachLights1_ActivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_ApproachLights1_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_ApproachLights1_DeactivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_ApproachLights1_GetIssState(Issm_IssStateType *issState)
{
  return Rte_Call_UR_ANW_ApproachLights1_GetIssState(issState);
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_KeyfobPowerControl_ActivateIss(void)
{
  return Rte_Call_UR_ANW_KeyfobPowerControl_ActivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_KeyfobPowerControl_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_KeyfobPowerControl_DeactivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_KeyfobPowerControl_GetIssState(Issm_IssStateType *issState)
{
  return Rte_Call_UR_ANW_KeyfobPowerControl_GetIssState(issState);
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_LockControlKeyfobRqst_ActivateIss(void)
{
  return Rte_Call_UR_ANW_LockControlKeyfobRqst_ActivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_LockControlKeyfobRqst_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_LockControlKeyfobRqst_DeactivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_LockControlKeyfobRqst_GetIssState(Issm_IssStateType *issState)
{
  return Rte_Call_UR_ANW_LockControlKeyfobRqst_GetIssState(issState);
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PanicAlarmFromKeyfob_ActivateIss(void)
{
  return Rte_Call_UR_ANW_PanicAlarmFromKeyfob_ActivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PanicAlarmFromKeyfob_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_PanicAlarmFromKeyfob_DeactivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PanicAlarmFromKeyfob_GetIssState(Issm_IssStateType *issState)
{
  return Rte_Call_UR_ANW_PanicAlarmFromKeyfob_GetIssState(issState);
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PowerWindowsActivate1_ActivateIss(void)
{
  return Rte_Call_UR_ANW_PowerWindowsActivate1_ActivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PowerWindowsActivate1_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_PowerWindowsActivate1_DeactivateIss();
}
Std_ReturnType TSC_Keyfob_UICtrl_Rte_Call_UR_ANW_PowerWindowsActivate1_GetIssState(Issm_IssStateType *issState)
{
  return Rte_Call_UR_ANW_PowerWindowsActivate1_GetIssState(issState);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_Keyfob_UICtrl_Rte_IrvWrite_Keyfob_UICtrl_20ms_runnable_Irv_KeyfobStatus_ReadData(uint8 *data)
{
  Rte_IrvWrite_Keyfob_UICtrl_20ms_runnable_Irv_KeyfobStatus_ReadData( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_KeyfobType_P1VKL_T  TSC_Keyfob_UICtrl_Rte_Prm_P1VKL_KeyfobType_v(void)
{
  return (SEWS_KeyfobType_P1VKL_T ) Rte_Prm_P1VKL_KeyfobType_v();
}
boolean  TSC_Keyfob_UICtrl_Rte_Prm_P1Y1C_SuperlockInhibition_v(void)
{
  return (boolean ) Rte_Prm_P1Y1C_SuperlockInhibition_v();
}
boolean  TSC_Keyfob_UICtrl_Rte_Prm_P1C54_FactoryModeActive_v(void)
{
  return (boolean ) Rte_Prm_P1C54_FactoryModeActive_v();
}


     /* Keyfob_UICtrl */
      /* Keyfob_UICtrl */



