/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VOL_DIDServer.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  VOL_DIDServer
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VOL_DIDServer>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_OpStatusType
 *   
 *
 * Distance32bit_T
 *   
 *
 * Temperature16bit_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_VOL_DIDServer.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_VOL_DIDServer.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void VOL_DIDServer_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Distance32bit_T: Integer in interval [0...4294967295]
 *   Unit: [m], Factor: 1, Offset: 0
 * Temperature16bit_T: Integer in interval [0...65535]
 *   Unit: [DegreeC], Factor: 1, Offset: -273
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * LinDiagBusInfo: Enumeration of integer in interval [0...255] with enumerators
 *   None (0U)
 *   LinDiag_BUS1 (1U)
 *   LinDiag_BUS2 (2U)
 *   LinDiag_BUS3 (3U)
 *   LinDiag_BUS4 (4U)
 *   LinDiag_BUS5 (5U)
 *   LinDiag_SpareBUS1 (6U)
 *   LinDiag_SpareBUS2 (7U)
 *   LinDiag_ALL_BUSSES (8U)
 * LinDiagServiceStatus: Enumeration of integer in interval [0...255] with enumerators
 *   LinDiagService_None (0U)
 *   LinDiagService_Pending (1U)
 *   LinDiagService_Completed (2U)
 *   LinDiagService_Error (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data16ByteType: Array with 16 element(s) of type uint8
 * Dcm_Data17ByteType: Array with 17 element(s) of type uint8
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data221ByteType: Array with 221 element(s) of type uint8
 * Dcm_Data241ByteType: Array with 241 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data406ByteType: Array with 406 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * Dcm_Data64ByteType: Array with 64 element(s) of type uint8
 * Dcm_Data8ByteType: Array with 8 element(s) of type uint8
 * SEWS_ChassisId_CHANO_T: Array with 16 element(s) of type uint8
 * SEWS_VIN_VINNO_T: Array with 17 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v(void)
 *   boolean Rte_Prm_P1C54_FactoryModeActive_v(void)
 *   uint8 *Rte_Prm_CHANO_ChassisId_v(void)
 *     Returnvalue: uint8* is of type SEWS_ChassisId_CHANO_T
 *   uint8 *Rte_Prm_VINNO_VIN_v(void)
 *     Returnvalue: uint8* is of type SEWS_VIN_VINNO_T
 *
 *********************************************************************************************************************/


#define VOL_DIDServer_START_SEC_CODE
#include "VOL_DIDServer_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_CHANO_Data_CHANO_ChassisId_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_CHANO_Data_CHANO_ChassisId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_CHANO_Data_CHANO_ChassisId_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data16ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_CHANO_Data_CHANO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_CHANO_Data_CHANO_ChassisId_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_CHANO_Data_CHANO_ChassisId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_CHANO_Data_CHANO_ChassisId_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  VOL_DIDServer_TestDefines();

  return RTE_E_DataServices_CHANO_Data_CHANO_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1AFR_Data_P1AFR_OutdoorTemperature>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AmbientAirTemperature_AmbientAirTemperature(Temperature16bit_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1AFR_Data_P1AFR_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Temperature16bit_T Read_AmbientAirTemperature_AmbientAirTemperature;

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  fct_status = TSC_VOL_DIDServer_Rte_Read_AmbientAirTemperature_AmbientAirTemperature(&Read_AmbientAirTemperature_AmbientAirTemperature);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1AFR_Data_P1AFR_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1AFS_Data_P1AFS_Odometer_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1AFS_Data_P1AFS_Odometer>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(Distance32bit_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1AFS_Data_P1AFS_Odometer_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data4ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1AFS_Data_P1AFS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFS_Data_P1AFS_Odometer_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFS_Data_P1AFS_Odometer_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFS_Data_P1AFS_Odometer_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Distance32bit_T Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes;

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  fct_status = TSC_VOL_DIDServer_Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(&Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1AFS_Data_P1AFS_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1AFT_Data_P1AFT_VehicleMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1AFT_Data_P1AFT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  VehicleMode_T Read_VehicleModeInternal_VehicleMode;

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  fct_status = TSC_VOL_DIDServer_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1AFT_Data_P1AFT_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData(Dcm_OpStatusType OpStatus, uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data406ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING
 *   RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadDataLength> of PortPrototype <DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_SlaveNodePnSnResp(LinDiagServiceStatus *DiagServiceStatus, uint8 *NoOfLinSlaves, uint8 *LinDiagRespPNSN)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength(Dcm_OpStatusType OpStatus, uint16 *DataLength)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING
 *   RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength(Dcm_OpStatusType OpStatus, P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  LinDiagServiceStatus Call_CddLinDiagServices_SlaveNodePnSnResp_DiagServiceStatus = 0U;
  uint8 Call_CddLinDiagServices_SlaveNodePnSnResp_NoOfLinSlaves = 0U;
  uint8 Call_CddLinDiagServices_SlaveNodePnSnResp_LinDiagRespPNSN = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  fct_status = TSC_VOL_DIDServer_Rte_Call_CddLinDiagServices_SlaveNodePnSnReq(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING:
      fct_error = 1;
      break;
    case RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VOL_DIDServer_Rte_Call_CddLinDiagServices_SlaveNodePnSnResp(&Call_CddLinDiagServices_SlaveNodePnSnResp_DiagServiceStatus, &Call_CddLinDiagServices_SlaveNodePnSnResp_NoOfLinSlaves, &Call_CddLinDiagServices_SlaveNodePnSnResp_LinDiagRespPNSN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING:
      fct_error = 1;
      break;
    case RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK:
      fct_error = 1;
      break;
  }

  return RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data8ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALB_Data_P1ALB_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_P1ALB_Data_P1ALB_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ALP_Data_P1ALP_ApplicationDataId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data241ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALP_Data_P1ALP_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_P1ALP_Data_P1ALP_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadDataLength> of PortPrototype <DataServices_P1ALP_Data_P1ALP_ApplicationDataId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength(uint16 *DataLength)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALP_Data_P1ALP_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength(P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_P1ALP_Data_P1ALP_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data241ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALQ_Data_P1ALQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_P1ALQ_Data_P1ALQ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadDataLength> of PortPrototype <DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength(uint16 *DataLength)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1ALQ_Data_P1ALQ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength(P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_P1ALQ_Data_P1ALQ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1B1O_Data_P1B1O_BootSWIdentifier>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data221ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1B1O_Data_P1B1O_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_P1B1O_Data_P1B1O_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId>
 *
 **********************************************************************************************************************
 *
 * Mode Interfaces:
 * ================
 *   uint8 Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(void)
 *   Modes of Rte_ModeType_DcmDiagnosticSessionControl:
 *   - RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION
 *   - RTE_MODE_DcmDiagnosticSessionControl_EXTENDED_SESSION
 *   - RTE_MODE_DcmDiagnosticSessionControl_PROGRAMMING_SESSION
 *   - RTE_TRANSITION_DcmDiagnosticSessionControl
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DIH_Data_P1DIH_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  Rte_ModeType_DcmDiagnosticSessionControl DcmDiagnosticSessionControl_DcmDiagnosticSessionControl;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  DcmDiagnosticSessionControl_DcmDiagnosticSessionControl = TSC_VOL_DIDServer_Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl();

  return RTE_E_DataServices_P1DIH_Data_P1DIH_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1OLT_Data_P1OLT_BuildVersionInfo>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data64ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1OLT_Data_P1OLT_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_P1OLT_Data_P1OLT_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data64ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1Q82_Data_P1Q82_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_P1Q82_Data_P1Q82_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_VINNO_Data_VINNO_VIN_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_VINNO_Data_VINNO_VIN>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_VINNO_Data_VINNO_VIN_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data17ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_VINNO_Data_VINNO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_VINNO_Data_VINNO_VIN_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_VINNO_Data_VINNO_VIN_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_VINNO_Data_VINNO_VIN_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean X1CJT_EnableCustomDemCfgCrc_v_data;

  boolean P1C54_FactoryModeActive_v_data;
  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;
  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CJT_EnableCustomDemCfgCrc_v_data = TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();

  P1C54_FactoryModeActive_v_data = TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v();
  (void)memcpy(CHANO_ChassisId_v_data, TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));
  (void)memcpy(VINNO_VIN_v_data, TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  return RTE_E_DataServices_VINNO_Data_VINNO_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VOL_DIDServer_STOP_SEC_CODE
#include "VOL_DIDServer_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void VOL_DIDServer_TestDefines(void)
{
  /* Enumeration Data Types */

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  LinDiagBusInfo Test_LinDiagBusInfo_V_1 = None;
  LinDiagBusInfo Test_LinDiagBusInfo_V_2 = LinDiag_BUS1;
  LinDiagBusInfo Test_LinDiagBusInfo_V_3 = LinDiag_BUS2;
  LinDiagBusInfo Test_LinDiagBusInfo_V_4 = LinDiag_BUS3;
  LinDiagBusInfo Test_LinDiagBusInfo_V_5 = LinDiag_BUS4;
  LinDiagBusInfo Test_LinDiagBusInfo_V_6 = LinDiag_BUS5;
  LinDiagBusInfo Test_LinDiagBusInfo_V_7 = LinDiag_SpareBUS1;
  LinDiagBusInfo Test_LinDiagBusInfo_V_8 = LinDiag_SpareBUS2;
  LinDiagBusInfo Test_LinDiagBusInfo_V_9 = LinDiag_ALL_BUSSES;

  LinDiagServiceStatus Test_LinDiagServiceStatus_V_1 = LinDiagService_None;
  LinDiagServiceStatus Test_LinDiagServiceStatus_V_2 = LinDiagService_Pending;
  LinDiagServiceStatus Test_LinDiagServiceStatus_V_3 = LinDiagService_Completed;
  LinDiagServiceStatus Test_LinDiagServiceStatus_V_4 = LinDiagService_Error;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;

  /* Modes */

  uint8 Test_DcmDiagnosticSessionControl_MV_1 = RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION;
  uint8 Test_DcmDiagnosticSessionControl_MV_2 = RTE_MODE_DcmDiagnosticSessionControl_PROGRAMMING_SESSION;
  uint8 Test_DcmDiagnosticSessionControl_MV_3 = RTE_MODE_DcmDiagnosticSessionControl_EXTENDED_SESSION;
  uint8 Test_DcmDiagnosticSessionControl_TV = RTE_TRANSITION_DcmDiagnosticSessionControl;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
