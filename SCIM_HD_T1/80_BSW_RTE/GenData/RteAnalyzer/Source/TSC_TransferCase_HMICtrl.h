/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_TransferCase_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data);
Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack(TransferCaseNeutral_T *data);
Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_TransferCaseNeutral_SwitchStat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_TransferCaseNeutral_status_TransferCaseNeutral_status(TransferCaseNeutral_status_T *data);
Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Read_TransferCasePTOEngaged_TransferCasePTOEngaged(NotEngagedEngaged_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Write_TransferCaseNeutral_DevInd_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_TransferCase_HMICtrl_Rte_Write_TransferCaseNeutral_Req_TransferCaseNeutral_Req(TransferCaseNeutral_Req2_T data);




