/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_AuthenticationDevice_UICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_AuthenticationDevice_UICtrl_Rte_Read_ButtonAuth_rqst_ButtonAuth_rqst(ButtonAuth_rqst_T *data);
Std_ReturnType TSC_AuthenticationDevice_UICtrl_Rte_Read_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T *data);
Std_ReturnType TSC_AuthenticationDevice_UICtrl_Rte_Read_KeyPosition_KeyPosition(KeyPosition_T *data);
Std_ReturnType TSC_AuthenticationDevice_UICtrl_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_AuthenticationDevice_UICtrl_Rte_Write_DeviceAuthentication_rqst_DeviceAuthentication_rqst(DeviceAuthentication_rqst_T data);
Std_ReturnType TSC_AuthenticationDevice_UICtrl_Rte_Write_DriverAuthDeviceMatching_DriverAuthDeviceMatching(DriverAuthDeviceMatching_T data);

/** Calibration Component Calibration Parameters */
boolean  TSC_AuthenticationDevice_UICtrl_Rte_Prm_P1T3W_VehSSButtonInstalled_v(void);




