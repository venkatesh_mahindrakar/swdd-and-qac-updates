/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TheftAlarm_HMI2_ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  TheftAlarm_HMI2_ctrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <TheftAlarm_HMI2_ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * SEWS_TheftAlarmRequestDuration_X1CY8_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_TheftAlarm_HMI2_ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_TheftAlarm_HMI2_ctrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void TheftAlarm_HMI2_ctrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_TheftAlarmRequestDuration_X1CY8_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * AlarmStatus_stat_T: Enumeration of integer in interval [0...15] with enumerators
 *   AlarmStatus_stat_Idle (0U)
 *   AlarmStatus_stat_SetMode (1U)
 *   AlarmStatus_stat_ReduceSetMode (2U)
 *   AlarmStatus_stat_UnsetMode (3U)
 *   AlarmStatus_stat_PanicMode (4U)
 *   AlarmStatus_stat_AlarmMode (5U)
 *   AlarmStatus_stat_ServiceMode (6U)
 *   AlarmStatus_stat_TamperMode (7U)
 *   AlarmStatus_stat_Spare_01 (8U)
 *   AlarmStatus_stat_Spare_02 (9U)
 *   AlarmStatus_stat_Spare_03 (10U)
 *   AlarmStatus_stat_Spare_04 (11U)
 *   AlarmStatus_stat_Spare_05 (12U)
 *   AlarmStatus_stat_Spare_06 (13U)
 *   AlarmStatus_stat_Error (14U)
 *   AlarmStatus_stat_NotAvailable (15U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * KeyAuthentication_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyAuthentication_stat_decrypt_KeyNotAuthenticated (0U)
 *   KeyAuthentication_stat_decrypt_KeyAuthenticated (1U)
 *   KeyAuthentication_stat_decrypt_Spare1 (2U)
 *   KeyAuthentication_stat_decrypt_Spare2 (3U)
 *   KeyAuthentication_stat_decrypt_Spare3 (4U)
 *   KeyAuthentication_stat_decrypt_Spare4 (5U)
 *   KeyAuthentication_stat_decrypt_Error (6U)
 *   KeyAuthentication_stat_decrypt_NotAvailable (7U)
 * KeyPosition_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyPosition_KeyOut (0U)
 *   KeyPosition_IgnitionKeyInOffPosition (1U)
 *   KeyPosition_IgnitionKeyInAccessoryPosition (2U)
 *   KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition (3U)
 *   KeyPosition_IgnitionKeyInPreheatPosition (4U)
 *   KeyPosition_IgnitionKeyInCrankPosition (5U)
 *   KeyPosition_ErrorIndicator (6U)
 *   KeyPosition_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * ReducedSetMode_rqst_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   ReducedSetMode_rqst_decrypt_Idle (0U)
 *   ReducedSetMode_rqst_decrypt_ReducedSetModeRequested (1U)
 *   ReducedSetMode_rqst_decrypt_ReducedSetModeNOT_Requested (2U)
 *   ReducedSetMode_rqst_decrypt_Spare1 (3U)
 *   ReducedSetMode_rqst_decrypt_Spare2 (4U)
 *   ReducedSetMode_rqst_decrypt_Spare3 (5U)
 *   ReducedSetMode_rqst_decrypt_Error (6U)
 *   ReducedSetMode_rqst_decrypt_NotAvailable (7U)
 * TheftAlarmAct_rqst_decrypt_I: Enumeration of integer in interval [0...7] with enumerators
 *   TheftAlarmAct_rqst_decrypt_Idle (0U)
 *   TheftAlarmAct_rqst_decrypt_TheftAlarmActReqstFromKeyfob (1U)
 *   TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromKeyfob (2U)
 *   TheftAlarmAct_rqst_decrypt_TheftAlarmActivationRequestFromOtherSource (3U)
 *   TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromOtherSource (4U)
 *   TheftAlarmAct_rqst_decrypt_Spare (5U)
 *   TheftAlarmAct_rqst_decrypt_Error (6U)
 *   TheftAlarmAct_rqst_decrypt_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Crypto_Function_serialized_T: Array with 12 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_TheftAlarmRequestDuration_X1CY8_T Rte_Prm_X1CY8_TheftAlarmRequestDuration_v(void)
 *   boolean Rte_Prm_P1B2T_AlarmInstalled_v(void)
 *
 *********************************************************************************************************************/


#define TheftAlarm_HMI2_ctrl_START_SEC_CODE
#include "TheftAlarm_HMI2_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: TheftAlarm_HMI2_ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AlarmStatus_stat_AlarmStatus_stat(AlarmStatus_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(KeyAuthentication_stat_decrypt_T *data)
 *   Std_ReturnType Rte_Read_KeyPosition_KeyPosition(KeyPosition_T *data)
 *   Std_ReturnType Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobSuperLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_ReducedSetModeButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Parked_Parked(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ReducedSetMode_DevInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ReducedSetMode_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt(ReducedSetMode_rqst_decrypt_T data)
 *   Std_ReturnType Rte_Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt(TheftAlarmAct_rqst_decrypt_I data)
 *   Std_ReturnType Rte_Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: TheftAlarm_HMI2_ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, TheftAlarm_HMI2_ctrl_CODE) TheftAlarm_HMI2_ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: TheftAlarm_HMI2_ctrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AlarmStatus_stat_T Read_AlarmStatus_stat_AlarmStatus_stat;
  KeyAuthentication_stat_decrypt_T Read_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt;
  KeyPosition_T Read_KeyPosition_KeyPosition;
  PushButtonStatus_T Read_KeyfobLockButton_Status_PushButtonStatus;
  PushButtonStatus_T Read_KeyfobSuperLockButton_Status_PushButtonStatus;
  PushButtonStatus_T Read_KeyfobUnlockButton_Status_PushButtonStatus;
  PushButtonStatus_T Read_ReducedSetModeButtonStatus_PushButtonStatus;
  VehicleModeDistribution_T Read_SwcActivation_Parked_Parked;
  VehicleMode_T Read_VehicleModeInternal_VehicleMode;

  SEWS_TheftAlarmRequestDuration_X1CY8_T X1CY8_TheftAlarmRequestDuration_v_data;

  boolean P1B2T_AlarmInstalled_v_data;

  Crypto_Function_serialized_T Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized;
  Crypto_Function_serialized_T Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CY8_TheftAlarmRequestDuration_v_data = TSC_TheftAlarm_HMI2_ctrl_Rte_Prm_X1CY8_TheftAlarmRequestDuration_v();

  P1B2T_AlarmInstalled_v_data = TSC_TheftAlarm_HMI2_ctrl_Rte_Prm_P1B2T_AlarmInstalled_v();

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Read_AlarmStatus_stat_AlarmStatus_stat(&Read_AlarmStatus_stat_AlarmStatus_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(&Read_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyPosition_KeyPosition(&Read_KeyPosition_KeyPosition);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyfobLockButton_Status_PushButtonStatus(&Read_KeyfobLockButton_Status_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyfobSuperLockButton_Status_PushButtonStatus(&Read_KeyfobSuperLockButton_Status_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(&Read_KeyfobUnlockButton_Status_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Read_ReducedSetModeButtonStatus_PushButtonStatus(&Read_ReducedSetModeButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Read_SwcActivation_Parked_Parked(&Read_SwcActivation_Parked_Parked);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_DevInd_DeviceIndication(Rte_InitValue_ReducedSetMode_DevInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_rqst_CryptTrig_CryptoTrigger(Rte_InitValue_ReducedSetMode_rqst_CryptTrig_CryptoTrigger);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt(Rte_InitValue_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized, 0, sizeof(Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized));
  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized(Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Write_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger(Rte_InitValue_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Write_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt(Rte_InitValue_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized, 0, sizeof(Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized));
  fct_status = TSC_TheftAlarm_HMI2_ctrl_Rte_Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized(Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  TheftAlarm_HMI2_ctrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define TheftAlarm_HMI2_ctrl_STOP_SEC_CODE
#include "TheftAlarm_HMI2_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void TheftAlarm_HMI2_ctrl_TestDefines(void)
{
  /* Enumeration Data Types */

  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_1 = AlarmStatus_stat_Idle;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_2 = AlarmStatus_stat_SetMode;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_3 = AlarmStatus_stat_ReduceSetMode;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_4 = AlarmStatus_stat_UnsetMode;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_5 = AlarmStatus_stat_PanicMode;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_6 = AlarmStatus_stat_AlarmMode;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_7 = AlarmStatus_stat_ServiceMode;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_8 = AlarmStatus_stat_TamperMode;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_9 = AlarmStatus_stat_Spare_01;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_10 = AlarmStatus_stat_Spare_02;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_11 = AlarmStatus_stat_Spare_03;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_12 = AlarmStatus_stat_Spare_04;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_13 = AlarmStatus_stat_Spare_05;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_14 = AlarmStatus_stat_Spare_06;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_15 = AlarmStatus_stat_Error;
  AlarmStatus_stat_T Test_AlarmStatus_stat_T_V_16 = AlarmStatus_stat_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_1 = KeyAuthentication_stat_decrypt_KeyNotAuthenticated;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_2 = KeyAuthentication_stat_decrypt_KeyAuthenticated;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_3 = KeyAuthentication_stat_decrypt_Spare1;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_4 = KeyAuthentication_stat_decrypt_Spare2;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_5 = KeyAuthentication_stat_decrypt_Spare3;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_6 = KeyAuthentication_stat_decrypt_Spare4;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_7 = KeyAuthentication_stat_decrypt_Error;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_8 = KeyAuthentication_stat_decrypt_NotAvailable;

  KeyPosition_T Test_KeyPosition_T_V_1 = KeyPosition_KeyOut;
  KeyPosition_T Test_KeyPosition_T_V_2 = KeyPosition_IgnitionKeyInOffPosition;
  KeyPosition_T Test_KeyPosition_T_V_3 = KeyPosition_IgnitionKeyInAccessoryPosition;
  KeyPosition_T Test_KeyPosition_T_V_4 = KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition;
  KeyPosition_T Test_KeyPosition_T_V_5 = KeyPosition_IgnitionKeyInPreheatPosition;
  KeyPosition_T Test_KeyPosition_T_V_6 = KeyPosition_IgnitionKeyInCrankPosition;
  KeyPosition_T Test_KeyPosition_T_V_7 = KeyPosition_ErrorIndicator;
  KeyPosition_T Test_KeyPosition_T_V_8 = KeyPosition_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  ReducedSetMode_rqst_decrypt_T Test_ReducedSetMode_rqst_decrypt_T_V_1 = ReducedSetMode_rqst_decrypt_Idle;
  ReducedSetMode_rqst_decrypt_T Test_ReducedSetMode_rqst_decrypt_T_V_2 = ReducedSetMode_rqst_decrypt_ReducedSetModeRequested;
  ReducedSetMode_rqst_decrypt_T Test_ReducedSetMode_rqst_decrypt_T_V_3 = ReducedSetMode_rqst_decrypt_ReducedSetModeNOT_Requested;
  ReducedSetMode_rqst_decrypt_T Test_ReducedSetMode_rqst_decrypt_T_V_4 = ReducedSetMode_rqst_decrypt_Spare1;
  ReducedSetMode_rqst_decrypt_T Test_ReducedSetMode_rqst_decrypt_T_V_5 = ReducedSetMode_rqst_decrypt_Spare2;
  ReducedSetMode_rqst_decrypt_T Test_ReducedSetMode_rqst_decrypt_T_V_6 = ReducedSetMode_rqst_decrypt_Spare3;
  ReducedSetMode_rqst_decrypt_T Test_ReducedSetMode_rqst_decrypt_T_V_7 = ReducedSetMode_rqst_decrypt_Error;
  ReducedSetMode_rqst_decrypt_T Test_ReducedSetMode_rqst_decrypt_T_V_8 = ReducedSetMode_rqst_decrypt_NotAvailable;

  TheftAlarmAct_rqst_decrypt_I Test_TheftAlarmAct_rqst_decrypt_I_V_1 = TheftAlarmAct_rqst_decrypt_Idle;
  TheftAlarmAct_rqst_decrypt_I Test_TheftAlarmAct_rqst_decrypt_I_V_2 = TheftAlarmAct_rqst_decrypt_TheftAlarmActReqstFromKeyfob;
  TheftAlarmAct_rqst_decrypt_I Test_TheftAlarmAct_rqst_decrypt_I_V_3 = TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromKeyfob;
  TheftAlarmAct_rqst_decrypt_I Test_TheftAlarmAct_rqst_decrypt_I_V_4 = TheftAlarmAct_rqst_decrypt_TheftAlarmActivationRequestFromOtherSource;
  TheftAlarmAct_rqst_decrypt_I Test_TheftAlarmAct_rqst_decrypt_I_V_5 = TheftAlarmAct_rqst_decrypt_TheftAlarmDeactReqstFromOtherSource;
  TheftAlarmAct_rqst_decrypt_I Test_TheftAlarmAct_rqst_decrypt_I_V_6 = TheftAlarmAct_rqst_decrypt_Spare;
  TheftAlarmAct_rqst_decrypt_I Test_TheftAlarmAct_rqst_decrypt_I_V_7 = TheftAlarmAct_rqst_decrypt_Error;
  TheftAlarmAct_rqst_decrypt_I Test_TheftAlarmAct_rqst_decrypt_I_V_8 = TheftAlarmAct_rqst_decrypt_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
