/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Csm.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  Csm
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <Csm>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * AsymDecryptDataBuffer
 *   
 *
 * AsymDecryptLengthBuffer
 *   
 *
 * AsymDecryptResultBuffer
 *   
 *
 * AsymPrivateKeyType
 *   
 *
 * AsymPublicKeyType
 *   
 *
 * Csm_ConfigIdType
 *   
 *
 * Csm_ReturnType
 *   
 *
 * Csm_VerifyResultType
 *   
 *
 * KeyExchangeBaseType
 *   
 *
 * KeyExchangePrivateType
 *   
 *
 * RandomGenerateResultBuffer
 *   
 *
 * RandomSeedDataBuffer
 *   
 *
 * SignatureVerifyDataBuffer
 *   
 *
 * SymDecryptDataBuffer
 *   
 *
 * SymDecryptLengthBuffer
 *   
 *
 * SymDecryptResultBuffer
 *   
 *
 * SymEncryptDataBuffer
 *   
 *
 * SymEncryptLengthBuffer
 *   
 *
 * SymEncryptResultBuffer
 *   
 *
 * SymKeyType
 *   
 *
 *
 * Operation Prototypes:
 * =====================
 * AsymDecryptFinish of Port Interface CsmAsymDecrypt
 *   
 *
 * AsymDecryptStart of Port Interface CsmAsymDecrypt
 *   
 *
 * AsymDecryptUpdate of Port Interface CsmAsymDecrypt
 *   
 *
 * RandomGenerate of Port Interface CsmRandomGenerate
 *   
 *
 * RandomSeedFinish of Port Interface CsmRandomSeed
 *   
 *
 * RandomSeedStart of Port Interface CsmRandomSeed
 *   
 *
 * RandomSeedUpdate of Port Interface CsmRandomSeed
 *   
 *
 * SignatureVerifyFinish of Port Interface CsmSignatureVerify
 *   
 *
 * SignatureVerifyStart of Port Interface CsmSignatureVerify
 *   
 *
 * SignatureVerifyUpdate of Port Interface CsmSignatureVerify
 *   
 *
 * SymDecryptFinish of Port Interface CsmSymDecrypt
 *   
 *
 * SymDecryptStart of Port Interface CsmSymDecrypt
 *   
 *
 * SymDecryptUpdate of Port Interface CsmSymDecrypt
 *   
 *
 * SymEncryptFinish of Port Interface CsmSymEncrypt
 *   
 *
 * SymEncryptStart of Port Interface CsmSymEncrypt
 *   
 *
 * SymEncryptUpdate of Port Interface CsmSymEncrypt
 *   
 *
 *********************************************************************************************************************/

#include "Rte_Csm.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_Csm.h"
#include "SchM_Csm.h"
#include "TSC_SchM_Csm.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void Csm_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * AsymDecryptLengthBuffer: Integer in interval [0...4294967295]
 * Csm_ConfigIdType: Integer in interval [0...65535]
 * SymDecryptLengthBuffer: Integer in interval [0...4294967295]
 * SymEncryptLengthBuffer: Integer in interval [0...4294967295]
 * uint16: Integer in interval [0...65535] (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Csm_ReturnType: Enumeration of integer in interval [0...4] with enumerators
 *   CSM_E_OK (0U)
 *   CSM_E_NOT_OK (1U)
 *   CSM_E_BUSY (2U)
 *   CSM_E_SMALL_BUFFER (3U)
 *   CSM_E_ENTROPY_EXHAUSTION (4U)
 * Csm_VerifyResultType: Enumeration of integer in interval [0...1] with enumerators
 *   CSM_E_VER_OK (0U)
 *   CSM_E_VER_NOT_OK (1U)
 *
 * Array Types:
 * ============
 * AsymDecryptDataBuffer: Array with 128 element(s) of type uint8
 * AsymDecryptResultBuffer: Array with 128 element(s) of type uint8
 * RandomGenerateResultBuffer: Array with 128 element(s) of type uint8
 * RandomSeedDataBuffer: Array with 128 element(s) of type uint8
 * Rte_DT_AsymPrivateKeyType_1: Array with 128 element(s) of type uint8
 * Rte_DT_AsymPublicKeyType_1: Array with 128 element(s) of type uint8
 * Rte_DT_KeyExchangeBaseType_1: Array with 1 element(s) of type uint8
 * Rte_DT_KeyExchangePrivateType_1: Array with 1 element(s) of type uint8
 * Rte_DT_SymKeyType_1: Array with 256 element(s) of type uint8
 * SignatureVerifyDataBuffer: Array with 128 element(s) of type uint8
 * SymDecryptDataBuffer: Array with 128 element(s) of type uint8
 * SymDecryptResultBuffer: Array with 128 element(s) of type uint8
 * SymEncryptDataBuffer: Array with 128 element(s) of type uint8
 * SymEncryptResultBuffer: Array with 128 element(s) of type uint8
 *
 * Record Types:
 * =============
 * AsymPrivateKeyType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_AsymPrivateKeyType_1
 * AsymPublicKeyType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_AsymPublicKeyType_1
 * KeyExchangeBaseType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_KeyExchangeBaseType_1
 * KeyExchangePrivateType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_KeyExchangePrivateType_1
 * SymKeyType: Record with elements
 *   length of type uint32
 *   data of type Rte_DT_SymKeyType_1
 *
 *********************************************************************************************************************/


#define Csm_START_SEC_CODE
#include "Csm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AsymDecryptFinish
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <AsymDecryptFinish> of PortPrototype <CsmAsymDecryptConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_AsymDecryptFinish(uint8 *plainTextBuffer, AsymDecryptLengthBuffer *plainTextLength)
 *     Argument plainTextBuffer: uint8* is of type AsymDecryptResultBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmAsymDecrypt_CSM_E_BUSY
 *   RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AsymDecryptFinish_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_AsymDecryptFinish(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_AsymDecryptFinish (returns application error)
 *********************************************************************************************************************/

  Csm_TestDefines();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AsymDecryptStart
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <AsymDecryptStart> of PortPrototype <CsmAsymDecryptConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_AsymDecryptStart(const AsymPrivateKeyType *key)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmAsymDecrypt_CSM_E_BUSY
 *   RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AsymDecryptStart_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_AsymDecryptStart(Csm_ConfigIdType parg0, P2CONST(AsymPrivateKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_AsymDecryptStart (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AsymDecryptUpdate
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <AsymDecryptUpdate> of PortPrototype <CsmAsymDecryptConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_AsymDecryptUpdate(const uint8 *cipherTextBuffer, uint32 cipherTextLength, uint8 *plainTextBuffer, AsymDecryptLengthBuffer *plainTextLength)
 *     Argument cipherTextBuffer: uint8* is of type AsymDecryptDataBuffer
 *     Argument plainTextBuffer: uint8* is of type AsymDecryptResultBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmAsymDecrypt_CSM_E_BUSY
 *   RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK
 *   RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AsymDecryptUpdate_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_AsymDecryptUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) cipherTextBuffer, uint32 cipherTextLength, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_AsymDecryptUpdate (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Csm_MainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_MainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Csm_CODE) Csm_MainFunction(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_MainFunction
 *********************************************************************************************************************/



/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RandomGenerate
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RandomGenerate> of PortPrototype <CsmRandomGenerateConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_RandomGenerate(uint8 *resultBuffer, uint32 resultLength)
 *     Argument resultBuffer: uint8* is of type RandomGenerateResultBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmRandomGenerate_CSM_E_BUSY
 *   RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION
 *   RTE_E_CsmRandomGenerate_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RandomGenerate_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_RandomGenerate(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer, uint32 resultLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_RandomGenerate (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RandomSeedFinish
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RandomSeedFinish> of PortPrototype <CsmRandomSeedConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_RandomSeedFinish(void)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmRandomSeed_CSM_E_BUSY
 *   RTE_E_CsmRandomSeed_CSM_E_NOT_OK
 *   RTE_E_CsmRandomSeed_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RandomSeedFinish_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_RandomSeedFinish(Csm_ConfigIdType parg0) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_RandomSeedFinish (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RandomSeedStart
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RandomSeedStart> of PortPrototype <CsmRandomSeedConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_RandomSeedStart(void)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmRandomSeed_CSM_E_BUSY
 *   RTE_E_CsmRandomSeed_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RandomSeedStart_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_RandomSeedStart(Csm_ConfigIdType parg0) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_RandomSeedStart (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RandomSeedUpdate
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RandomSeedUpdate> of PortPrototype <CsmRandomSeedConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_RandomSeedUpdate(const uint8 *seedBuffer, uint32 seedLength)
 *     Argument seedBuffer: uint8* is of type RandomSeedDataBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmRandomSeed_CSM_E_BUSY
 *   RTE_E_CsmRandomSeed_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RandomSeedUpdate_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_RandomSeedUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) seedBuffer, uint32 seedLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_RandomSeedUpdate (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SignatureVerifyFinish
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SignatureVerifyFinish> of PortPrototype <CsmSignatureVerifyConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_SignatureVerifyFinish(const uint8 *signatureBuffer, uint32 signatureLength, Csm_VerifyResultType *resultBuffer)
 *     Argument signatureBuffer: uint8* is of type SignatureVerifyDataBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmSignatureVerify_CSM_E_BUSY
 *   RTE_E_CsmSignatureVerify_CSM_E_NOT_OK
 *   RTE_E_CsmSignatureVerify_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SignatureVerifyFinish_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_SignatureVerifyFinish(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) signatureBuffer, uint32 signatureLength, P2VAR(Csm_VerifyResultType, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_SignatureVerifyFinish (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SignatureVerifyStart
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SignatureVerifyStart> of PortPrototype <CsmSignatureVerifyConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_SignatureVerifyStart(const AsymPublicKeyType *key)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmSignatureVerify_CSM_E_BUSY
 *   RTE_E_CsmSignatureVerify_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SignatureVerifyStart_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_SignatureVerifyStart(Csm_ConfigIdType parg0, P2CONST(AsymPublicKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_SignatureVerifyStart (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SignatureVerifyUpdate
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SignatureVerifyUpdate> of PortPrototype <CsmSignatureVerifyConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_SignatureVerifyUpdate(const uint8 *dataBuffer, uint32 dataLength)
 *     Argument dataBuffer: uint8* is of type SignatureVerifyDataBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmSignatureVerify_CSM_E_BUSY
 *   RTE_E_CsmSignatureVerify_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SignatureVerifyUpdate_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_SignatureVerifyUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) dataBuffer, uint32 dataLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_SignatureVerifyUpdate (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SymDecryptFinish
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SymDecryptFinish> of PortPrototype <CsmSymDecryptConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_SymDecryptFinish(uint8 *plainTextBuffer, SymDecryptLengthBuffer *plainTextLength)
 *     Argument plainTextBuffer: uint8* is of type SymDecryptResultBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmSymDecrypt_CSM_E_BUSY
 *   RTE_E_CsmSymDecrypt_CSM_E_NOT_OK
 *   RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SymDecryptFinish_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_SymDecryptFinish(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(SymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_SymDecryptFinish (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SymDecryptStart
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SymDecryptStart> of PortPrototype <CsmSymDecryptConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_SymDecryptStart(const SymKeyType *key, const uint8 *InitVectorBuffer, uint32 InitVectorLength)
 *     Argument InitVectorBuffer: uint8* is of type SymDecryptDataBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmSymDecrypt_CSM_E_BUSY
 *   RTE_E_CsmSymDecrypt_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SymDecryptStart_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_SymDecryptStart(Csm_ConfigIdType parg0, P2CONST(SymKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) InitVectorBuffer, uint32 InitVectorLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_SymDecryptStart (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SymDecryptUpdate
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SymDecryptUpdate> of PortPrototype <CsmSymDecryptConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_SymDecryptUpdate(const uint8 *cipherTextBuffer, uint32 cipherTextLength, uint8 *plainTextBuffer, SymDecryptLengthBuffer *plainTextLength)
 *     Argument cipherTextBuffer: uint8* is of type SymDecryptDataBuffer
 *     Argument plainTextBuffer: uint8* is of type SymDecryptResultBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmSymDecrypt_CSM_E_BUSY
 *   RTE_E_CsmSymDecrypt_CSM_E_NOT_OK
 *   RTE_E_CsmSymDecrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SymDecryptUpdate_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_SymDecryptUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) cipherTextBuffer, uint32 cipherTextLength, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(SymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_SymDecryptUpdate (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SymEncryptFinish
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SymEncryptFinish> of PortPrototype <CsmSymEncryptConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_SymEncryptFinish(uint8 *cipherTextBuffer, SymEncryptLengthBuffer *cipherTextLength)
 *     Argument cipherTextBuffer: uint8* is of type SymEncryptResultBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmSymEncrypt_CSM_E_BUSY
 *   RTE_E_CsmSymEncrypt_CSM_E_NOT_OK
 *   RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SymEncryptFinish_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_SymEncryptFinish(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextBuffer, P2VAR(SymEncryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_SymEncryptFinish (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SymEncryptStart
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SymEncryptStart> of PortPrototype <CsmSymEncryptConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_SymEncryptStart(const SymKeyType *key, const uint8 *InitVectorBuffer, uint32 InitVectorLength)
 *     Argument InitVectorBuffer: uint8* is of type SymEncryptDataBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmSymEncrypt_CSM_E_BUSY
 *   RTE_E_CsmSymEncrypt_CSM_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SymEncryptStart_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_SymEncryptStart(Csm_ConfigIdType parg0, P2CONST(SymKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) InitVectorBuffer, uint32 InitVectorLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_SymEncryptStart (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: SymEncryptUpdate
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SymEncryptUpdate> of PortPrototype <CsmSymEncryptConfig>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Csm_SymEncryptUpdate(const uint8 *plainTextBuffer, uint32 plainTextLength, uint8 *cipherTextBuffer, SymEncryptLengthBuffer *cipherTextLength)
 *     Argument plainTextBuffer: uint8* is of type SymEncryptDataBuffer
 *     Argument cipherTextBuffer: uint8* is of type SymEncryptResultBuffer
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CsmSymEncrypt_CSM_E_BUSY
 *   RTE_E_CsmSymEncrypt_CSM_E_NOT_OK
 *   RTE_E_CsmSymEncrypt_CSM_E_SMALL_BUFFER
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: SymEncryptUpdate_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Csm_CODE) Csm_SymEncryptUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) plainTextBuffer, uint32 plainTextLength, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextBuffer, P2VAR(SymEncryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) cipherTextLength) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Csm_SymEncryptUpdate (returns application error)
 *********************************************************************************************************************/

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Csm_STOP_SEC_CODE
#include "Csm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void Csm_TestDefines(void)
{
  /* Enumeration Data Types */

  Csm_ReturnType Test_Csm_ReturnType_V_1 = CSM_E_OK;
  Csm_ReturnType Test_Csm_ReturnType_V_2 = CSM_E_NOT_OK;
  Csm_ReturnType Test_Csm_ReturnType_V_3 = CSM_E_BUSY;
  Csm_ReturnType Test_Csm_ReturnType_V_4 = CSM_E_SMALL_BUFFER;
  Csm_ReturnType Test_Csm_ReturnType_V_5 = CSM_E_ENTROPY_EXHAUSTION;

  Csm_VerifyResultType Test_Csm_VerifyResultType_V_1 = CSM_E_VER_OK;
  Csm_VerifyResultType Test_Csm_VerifyResultType_V_2 = CSM_E_VER_NOT_OK;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
