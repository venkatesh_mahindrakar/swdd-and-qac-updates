/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ExteriorLightPanels_Freewheel_Gw.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Read_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T *data);
Std_ReturnType TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Read_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T *data);
Std_ReturnType TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Write_LightMode_Status_Ctr_1_Freewheel_Status_Ctr(Freewheel_Status_Ctr_T data);
Std_ReturnType TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Write_LightMode_Status_Ctr_2_Freewheel_Status_Ctr(Freewheel_Status_Ctr_T data);

/** Sender receiver - update flag */
boolean TSC_ExteriorLightPanels_Freewheel_Gw_Rte_IsUpdated_LightMode_Status_1_FreeWheel_Status(void);
boolean TSC_ExteriorLightPanels_Freewheel_Gw_Rte_IsUpdated_LightMode_Status_2_FreeWheel_Status(void);




