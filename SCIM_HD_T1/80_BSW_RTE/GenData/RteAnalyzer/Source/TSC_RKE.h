/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_RKE.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Client server interfaces */
Std_ReturnType TSC_RKE_Rte_Call_DiagForceDriveAntenastatus_CS(void);
Std_ReturnType TSC_RKE_Rte_Call_GenerateTelegramEncryptionKey_CS(void);
Std_ReturnType TSC_RKE_Rte_Call_DiagForceDriveAntenastatus_CS(void);
Std_ReturnType TSC_RKE_Rte_Call_GenerateTelegramEncryptionKey_CS(void);
Std_ReturnType TSC_RKE_Rte_Call_RkeData_SetRcvRkeData(st_RKEdata *RcvRKEDataonRFIC);




