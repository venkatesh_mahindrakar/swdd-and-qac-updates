/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SCIM_PowerSupply12V_Hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_SCIM_PowerSupply12V_Hdlr.h"
#include "TSC_SCIM_PowerSupply12V_Hdlr.h"








Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Read_Living12VResetRequest_Living12VResetRequest(Boolean *data)
{
  return Rte_Read_Living12VResetRequest_Living12VResetRequest(data);
}

Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}




Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Write_Living12VPowerStability_Living12VPowerStability(Living12VPowerStability data)
{
  return Rte_Write_Living12VPowerStability_Living12VPowerStability(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(DcDc12vRefVoltage, IsDcDc12vActivated, FaultStatus);
}
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(SelectParkedOrLivingPin, IsDo12VActivated, Do12VPinVoltage, BatteryVoltage, FaultStatus);
}
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReqType, Activation);
}
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
{
  return Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReqType, Activation);
}


     /* Service calls */
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Issm_GetAllActiveIss_GetAllActiveIss(uint32 *activeIssField)
{
  return Rte_Call_Issm_GetAllActiveIss_GetAllActiveIss(activeIssField);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* SCIM_PowerSupply12V_Hdlr */
      /* SCIM_PowerSupply12V_Hdlr */



