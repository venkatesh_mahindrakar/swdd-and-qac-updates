/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_RearAxleSteering_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_RearAxleSteering_HMICtrl_Rte_Read_ExtraAxleSteeringFunctionStat_RearAxleSteeringFunctionStatus(RearAxleSteeringFunctionStatus_T *data);
Std_ReturnType TSC_RearAxleSteering_HMICtrl_Rte_Read_RearAxleSteeringFunctionStatus_RearAxleSteeringFunctionStatus(RearAxleSteeringFunctionStatus_T *data);
Std_ReturnType TSC_RearAxleSteering_HMICtrl_Rte_Read_RearAxleSteering_DeviceEvent_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_RearAxleSteering_HMICtrl_Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_RearAxleSteering_HMICtrl_Rte_Write_RearAxleSteeringDeviceInd_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_RearAxleSteering_HMICtrl_Rte_Write_RearAxleSteeringFunctionDsbl_RearAxleSteeringFunctionDsbl(RearAxleSteeringFunctionDsbl_T data);

/** Calibration Component Calibration Parameters */
SEWS_RAS_LEDFeedbackIndication_P1GCC_T  TSC_RearAxleSteering_HMICtrl_Rte_Prm_P1GCC_RAS_LEDFeedbackIndication_v(void);
boolean  TSC_RearAxleSteering_HMICtrl_Rte_Prm_P1GBT_RAS_SwitchInstalled_v(void);
boolean  TSC_RearAxleSteering_HMICtrl_Rte_Prm_P1RRH_RAS_ToggleButton_v(void);




