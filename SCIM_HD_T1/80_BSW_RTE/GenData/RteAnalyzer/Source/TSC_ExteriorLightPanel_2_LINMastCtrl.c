/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ExteriorLightPanel_2_LINMastCtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_ExteriorLightPanel_2_LINMastCtrl.h"
#include "TSC_ExteriorLightPanel_2_LINMastCtrl.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvRead_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP2LinCtrl(void)
{
return Rte_IrvRead_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP2LinCtrl();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP2LinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP2LinCtrl( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP2LinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP2LinCtrl( data);
}





Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN4_ComMode_LIN(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
{
  return Rte_Read_DiagActiveState_isDiagActive(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_DiagInfoELCP2_DiagInfo(DiagInfo_T *data)
{
  return Rte_Read_DiagInfoELCP2_DiagInfo(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_DRL_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_DRL_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_FogLightFront_ButtonStat_2_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_FogLightFront_ButtonStat_2_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_FogLightRear_ButtonStat_2_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_FogLightRear_ButtonStat_2_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_HeadLampUpDown_SwitchStatu_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
{
  return Rte_Read_LIN_HeadLampUpDown_SwitchStatu_A3PosSwitchStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T *data)
{
  return Rte_Read_LIN_LightMode_Status_2_FreeWheel_Status(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_RearWorkProjector_BtnStat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_RearWorkProjector_BtnStat_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_RearWorkProjector_Indication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_RearWorkProjector_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Read_ResponseErrorELCP2_ResponseErrorELCP2(ResponseErrorELCP2_T *data)
{
  return Rte_Read_ResponseErrorELCP2_ResponseErrorELCP2(data);
}


boolean TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IsUpdated_LIN_LightMode_Status_2_FreeWheel_Status(void)
{
  return Rte_IsUpdated_LIN_LightMode_Status_2_FreeWheel_Status();
}



Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_DRL_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_DRL_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_FogLightFront_ButtonStatus_2_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_FogLightFront_ButtonStatus_2_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_FogLightRear_ButtonStatus_2_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_FogLightRear_ButtonStatus_2_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_HeadLampUpDown_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data)
{
  return Rte_Write_HeadLampUpDown_SwitchStatus_A3PosSwitchStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_LIN_RearWorkProjector_Indicati_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_RearWorkProjector_Indicati_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T data)
{
  return Rte_Write_LightMode_Status_2_FreeWheel_Status(data);
}

Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Write_RearWorkProjector_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_RearWorkProjector_ButtonStatus_PushButtonStatus(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_16_ELCP2_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F0B_16_ELCP2_VBT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_17_ELCP__VAT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F0B_17_ELCP__VAT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_44_ELCP2_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F0B_44_ELCP2_RAM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_45_ELCP_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F0B_45_ELCP_FLASH_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_46_ELCP_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F0B_46_ELCP_EEPROM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_49_ELCP2_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F0B_49_ELCP2_HWFAIL_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F0B_94_ELCP2_SWFAIL_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F0C_87_ELCP2Link_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F0C_87_ELCP2Link_NoResp_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest2_ActivateIss(void)
{
  return Rte_Call_UR_ANW_ExteriorLightsRequest2_ActivateIss();
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest2_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_ExteriorLightsRequest2_DeactivateIss();
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest2_GetIssState(Issm_IssStateType *issState)
{
  return Rte_Call_UR_ANW_ExteriorLightsRequest2_GetIssState(issState);
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_WLight_InputELCP_ActivateIss(void)
{
  return Rte_Call_UR_ANW_WLight_InputELCP_ActivateIss();
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_WLight_InputELCP_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_WLight_InputELCP_DeactivateIss();
}
Std_ReturnType TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_WLight_InputELCP_GetIssState(Issm_IssStateType *issState)
{
  return Rte_Call_UR_ANW_WLight_InputELCP_GetIssState(issState);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvRead_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl(void)
{
return Rte_IrvRead_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl();
}

void TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_IrvWrite_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl(uint8 data)
{
  Rte_IrvWrite_ExteriorLightPanel_2_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP2LinCtrl( data);
}




boolean  TSC_ExteriorLightPanel_2_LINMastCtrl_Rte_Prm_P1VR5_ELCP2_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1VR5_ELCP2_Installed_v();
}


     /* ExteriorLightPanel_2_LINMastCtrl */
      /* ExteriorLightPanel_2_LINMastCtrl */



