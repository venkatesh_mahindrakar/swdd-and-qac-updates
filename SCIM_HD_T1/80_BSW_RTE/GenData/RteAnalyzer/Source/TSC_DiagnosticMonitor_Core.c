/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DiagnosticMonitor_Core.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DiagnosticMonitor_Core.h"
#include "TSC_DiagnosticMonitor_Core.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(EngTraceHWData_T *data)
{
  return Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */









Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(const EngTraceHWData_T *data)
{
  return Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_44_Core_RamFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1AD0_44_Core_RamFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_46_Core_NvramFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1AD0_46_Core_NvramFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventStatus(Dem_UdsStatusByteType *UDSStatusByte)
{
  return Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventStatus(UDSStatusByte);
}
Std_ReturnType TSC_DiagnosticMonitor_Core_Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* DiagnosticMonitor_Core */
      /* DiagnosticMonitor_Core */



