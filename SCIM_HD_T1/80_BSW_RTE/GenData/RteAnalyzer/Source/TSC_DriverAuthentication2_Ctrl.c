/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DriverAuthentication2_Ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_DriverAuthentication2_Ctrl.h"
#include "TSC_DriverAuthentication2_Ctrl.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_DriverAuthentication2_Ctrl_Rte_IrvRead_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus(VINCheckStatus_T *data)
{
Rte_IrvRead_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus( data);
}





Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU1VIN_stat_VIN_stat(uint8 *data)
{
  return Rte_Receive_ECU1VIN_stat_VIN_stat(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU2VIN_stat_VIN_stat(uint8 *data)
{
  return Rte_Receive_ECU2VIN_stat_VIN_stat(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU3VIN_stat_VIN_stat(uint8 *data)
{
  return Rte_Receive_ECU3VIN_stat_VIN_stat(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU4VIN_stat_VIN_stat(uint8 *data)
{
  return Rte_Receive_ECU4VIN_stat_VIN_stat(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU5VIN_stat_VIN_stat(uint8 *data)
{
  return Rte_Receive_ECU5VIN_stat_VIN_stat(data);
}


Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst(DeviceAuthentication_rqst_T *data)
{
  return Rte_Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T *data)
{
  return Rte_Read_DoorsAjar_stat_DoorsAjar_stat(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(uint8 *data)
{
  return Rte_Read_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_EngineStartAuth_rqst_EngineStartAuth_rqst(EngineStartAuth_rqst_T *data)
{
  return Rte_Read_EngineStartAuth_rqst_EngineStartAuth_rqst(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst(GearBoxUnlockAuth_rqst_T *data)
{
  return Rte_Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_KeyAuthentication_rqst_KeyAuthentication_rqst(KeyAuthentication_rqst_T *data)
{
  return Rte_Read_KeyAuthentication_rqst_KeyAuthentication_rqst(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_KeyfobAuth_stat_KeyfobAuth_stat(KeyfobAuth_stat_T *data)
{
  return Rte_Read_KeyfobAuth_stat_KeyfobAuth_stat(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_PinCode_stat_PinCode_stat(PinCode_stat_T *data)
{
  return Rte_Read_PinCode_stat_PinCode_stat(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Security_SwcActivation_Security(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}




Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_DeviceInCab_stat_DeviceInCab_stat(DeviceInCab_stat_T data)
{
  return Rte_Write_DeviceInCab_stat_DeviceInCab_stat(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(const uint8 *data)
{
  return Rte_Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_EngineStartAuth_st_serialized_Crypto_Function_serialized(const uint8 *data)
{
  return Rte_Write_EngineStartAuth_st_serialized_Crypto_Function_serialized(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_EngineStartAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
{
  return Rte_Write_EngineStartAuth_stat_CryptTrig_CryptoTrigger(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt(EngineStartAuth_stat_decrypt_T data)
{
  return Rte_Write_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt(GearboxUnlockAuth_stat_decrypt_T data)
{
  return Rte_Write_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
{
  return Rte_Write_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized(const uint8 *data)
{
  return Rte_Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
{
  return Rte_Write_KeyAuth_stat_CryptTrig_CryptoTrigger(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyAuth_stat_serialized_Crypto_Function_serialized(const uint8 *data)
{
  return Rte_Write_KeyAuth_stat_serialized_Crypto_Function_serialized(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(KeyAuthentication_stat_decrypt_T data)
{
  return Rte_Write_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyNotValid_KeyNotValid(KeyNotValid_T data)
{
  return Rte_Write_KeyNotValid_KeyNotValid(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyfobAuth_rqst_KeyfobAuth_rqst(KeyfobAuth_rqst_T data)
{
  return Rte_Write_KeyfobAuth_rqst_KeyfobAuth_rqst(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_PinCode_rqst_PinCode_rqst(PinCode_rqst_T data)
{
  return Rte_Write_PinCode_rqst_PinCode_rqst(data);
}

Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Write_VIN_rqst_VIN_rqst(VIN_rqst_T data)
{
  return Rte_Write_VIN_rqst_VIN_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Call_UR_ANW_ImmobilizerPINCode_ActivateIss(void)
{
  return Rte_Call_UR_ANW_ImmobilizerPINCode_ActivateIss();
}
Std_ReturnType TSC_DriverAuthentication2_Ctrl_Rte_Call_UR_ANW_ImmobilizerPINCode_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_ImmobilizerPINCode_DeactivateIss();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_DriverAuthentication2_Ctrl_Rte_IrvWrite_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus(VINCheckStatus_T *data)
{
  Rte_IrvWrite_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus( data);
}




boolean  TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1VKI_PassiveStart_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1VKI_PassiveStart_Installed_v();
}
SEWS_VINCheckProcessing_P1VKG_s_T * TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1VKG_VINCheckProcessing_v(void)
{
  return (SEWS_VINCheckProcessing_P1VKG_s_T *) Rte_Prm_P1VKG_VINCheckProcessing_v();
}
boolean  TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1TTA_GearBoxLockActivation_v(void)
{
  return (boolean ) Rte_Prm_P1TTA_GearBoxLockActivation_v();
}
SEWS_CrankingLockActivation_P1DS3_T  TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1DS3_CrankingLockActivation_v(void)
{
  return (SEWS_CrankingLockActivation_P1DS3_T ) Rte_Prm_P1DS3_CrankingLockActivation_v();
}
SEWS_ChassisId_CHANO_T * TSC_DriverAuthentication2_Ctrl_Rte_Prm_CHANO_ChassisId_v(void)
{
  return (SEWS_ChassisId_CHANO_T *) Rte_Prm_CHANO_ChassisId_v();
}


     /* DriverAuthentication2_Ctrl */
      /* DriverAuthentication2_Ctrl */



