/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_VehicleAccess_Ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_AutorelockingMovements_stat_AutorelockingMovements_stat(AutorelockingMovements_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_DriverDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_DriverDoorAjar_stat_DoorAjar_stat(DoorAjar_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_DriverDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_DriverDoorLatch_stat_DoorLatch_stat(DoorLatch_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized(uint8 *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst(EmergencyDoorsUnlock_rqst_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_FrontLidLatch_stat_FrontLidLatch_stat(FrontLidLatch_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(DoorLockUnlock_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(KeyfobOutsideLocation_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobSuperLockButton_Sta_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_LeftDoorButton_stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_PassengerDoorAjar_stat_DoorAjar_stat(DoorAjar_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_PassengerDoorLatch_stat_DoorLatch_stat(DoorLatch_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized(uint8 *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_PsngDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_PsngrDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_RightDoorButton_stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat(SpeedLockingInhibition_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat(Synch_Unsynch_Mode_stat_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(uint8 *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_WRCLockButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_WRCUnlockButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_AutoRelock_rqst_AutoRelock_rqst(AutoRelock_rqst_T data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_DashboardLockSwitch_Devic_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_DoorLock_stat_DoorLock_stat(DoorLock_stat_T data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger(Boolean data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized(const uint8 *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_FrontLidLatch_cmd_FrontLidLatch_cmd(FrontLidLatch_cmd_T data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst_T data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_LockingIndication_rqst_LockingIndication_rqst(LockingIndication_rqst_T data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger(Boolean data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized(const uint8 *data);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(const uint8 *data);

/** Service interfaces */
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlActDeactivation_ActivateIss(void);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlActDeactivation_DeactivateIss(void);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlActDeactivation_GetIssState(Issm_IssStateType *issState);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlCabRqst2_Pending_ActivateIss(void);
Std_ReturnType TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlCabRqst2_Pending_DeactivateIss(void);

/** Calibration Component Calibration Parameters */
SEWS_DoorLockingFailureTimeout_X1CX9_T  TSC_VehicleAccess_Ctrl_Rte_Prm_X1CX9_DoorLockingFailureTimeout_v(void);
SEWS_AlarmAutoRelockRequestDuration_X1CYA_T  TSC_VehicleAccess_Ctrl_Rte_Prm_X1CYA_AlarmAutoRelockRequestDuration_v(void);
SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1O8Q_DoorLockHazardIndicationRqstDelay_v(void);
SEWS_DoorAutoLockingSpeed_P1B2Q_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1B2Q_DoorAutoLockingSpeed_v(void);
SEWS_AutoAlarmReactivationTimeout_P1B2S_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1B2S_AutoAlarmReactivationTimeout_v(void);
SEWS_DoorLatchProtectionTimeWindow_P1DW8_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1DW8_DoorLatchProtectionTimeWindow_v(void);
SEWS_DoorLatchProtectionRestingTime_P1DW9_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1DW9_DoorLatchProtectionRestingTime_v(void);
SEWS_DoorIndicationReqDuration_P1DWP_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1DWP_DoorIndicationReqDuration_v(void);
SEWS_DoorLatchProtectMaxOperation_P1DXA_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1DXA_DoorLatchProtectMaxOperation_v(void);
SEWS_SpeedRelockingReinitThreshold_P1H55_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1H55_SpeedRelockingReinitThreshold_v(void);
SEWS_DashboardLedTimeout_P1IZ4_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1IZ4_DashboardLedTimeout_v(void);
SEWS_LockFunctionHardwareInterface_P1MXZ_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1MXZ_LockFunctionHardwareInterface_v(void);
SEWS_PassiveEntryFunction_Type_P1VKF_T  TSC_VehicleAccess_Ctrl_Rte_Prm_P1VKF_PassiveEntryFunction_Type_v(void);
boolean  TSC_VehicleAccess_Ctrl_Rte_Prm_P1NE9_KeyInsertDetection_Enabled_v(void);
boolean  TSC_VehicleAccess_Ctrl_Rte_Prm_P1NQE_LockModeHandling_v(void);
boolean  TSC_VehicleAccess_Ctrl_Rte_Prm_P1VKI_PassiveStart_Installed_v(void);




