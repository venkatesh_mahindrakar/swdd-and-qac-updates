/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SpeedControlMode_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Read_CCStates_CCStates(CCStates_T *data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Read_DriverMemory_rqst_DriverMemory_rqst(DriverMemory_rqst_T *data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Read_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(uint8 *data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Read_SpeedControlModeButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Read_SpeedControlModeWheelStatus_FreeWheel_Status(FreeWheel_Status_T *data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Read_XRSLStates_XRSLStates(XRSLStates_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Write_ACCOrCCIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Write_ASLIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Write_FWSelectedACCMode_CCIM_ACC(CCIM_ACC_T data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Write_FWSelectedSpeedControlMode_FWSelectedSpeedControlMode(FWSelectedSpeedControlMode_T data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Write_FWSpeedControlEndStopEvent_FWSpeedControlEndStopEvent(OffOn_T data);
Std_ReturnType TSC_SpeedControlMode_HMICtrl_Rte_Write_SCM_HMICtrl_NVM_I_SCM_HMICtrl_NVM_I(const uint8 *data);

/** Calibration Component Calibration Parameters */
SEWS_HeadwaySupport_P1BEX_T  TSC_SpeedControlMode_HMICtrl_Rte_Prm_P1BEX_HeadwaySupport_v(void);
boolean  TSC_SpeedControlMode_HMICtrl_Rte_Prm_P1B2C_CCFW_Installed_v(void);
boolean  TSC_SpeedControlMode_HMICtrl_Rte_Prm_P1EXP_PersonalSettingsForCC_v(void);




