/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_PassengersSeatBelt_hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_PassengersSeatBelt_hdlr.h"
#include "TSC_PassengersSeatBelt_hdlr.h"








Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}




Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Write_PassengersSeatBelt_PassengersSeatBelt(PassengersSeatBelt_T data)
{
  return Rte_Write_PassengersSeatBelt_PassengersSeatBelt(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef, AdiPinVoltage, BatteryVoltage, FaultStatus);
}


     /* Service calls */
Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Call_Event_D1F0O_1E_ResistOutOfRange_SecondPsgrSeat_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F0O_1E_ResistOutOfRange_SecondPsgrSeat_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Call_Event_D1FZ9_1E_ResistOutOfRange_FirstPsgrSeat_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1FZ9_1E_ResistOutOfRange_FirstPsgrSeat_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_PassengersSeatBelt_hdlr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
{
  return (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *) Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
}
SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T * TSC_PassengersSeatBelt_hdlr_Rte_Prm_X1CY2_PassengersSeatBeltVoltageLevels_v(void)
{
  return (SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T *) Rte_Prm_X1CY2_PassengersSeatBeltVoltageLevels_v();
}
SEWS_PassengersSeatBeltInstalled_P1VQB_T  TSC_PassengersSeatBelt_hdlr_Rte_Prm_P1VQB_PassengersSeatBeltInstalled_v(void)
{
  return (SEWS_PassengersSeatBeltInstalled_P1VQB_T ) Rte_Prm_P1VQB_PassengersSeatBeltInstalled_v();
}
SEWS_PassengersSeatBeltSensorType_P1VYK_T  TSC_PassengersSeatBelt_hdlr_Rte_Prm_P1VYK_PassengersSeatBeltSensorType_v(void)
{
  return (SEWS_PassengersSeatBeltSensorType_P1VYK_T ) Rte_Prm_P1VYK_PassengersSeatBeltSensorType_v();
}


     /* PassengersSeatBelt_hdlr */
      /* PassengersSeatBelt_hdlr */



