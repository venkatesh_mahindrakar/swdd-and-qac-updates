/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ExtraBbTailLiftCrane_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_CranePushButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_CraneSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_ExtraBBCraneStatus_ExtraBBCraneStatus(ExtraBBCraneStatus_T *data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_WRCCraneRequest_WRCCraneRequest(PushButtonStatus_T *data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_ExtraBBTailLiftStatus_ExtraBBTailLiftStatus(InactiveActive_T *data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_TailLiftPushButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_TailLiftSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Read_WRCTailLiftRequest_WRCTailLiftRequest(PushButtonStatus_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_BBCraneRequest_BBCraneRequest(OffOn_T data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_CraneSupply_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_BBTailLiftRequest_BBTailLiftRequest(OffOn_T data);
Std_ReturnType TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Write_TailLift_DeviceIndication_DeviceIndication(DeviceIndication_T data);

/** Calibration Component Calibration Parameters */
SEWS_TailLiftHMIDeviceType_P1CW9_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CW9_TailLiftHMIDeviceType_v(void);
SEWS_CraneHMIDeviceType_P1CXA_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXA_CraneHMIDeviceType_v(void);
SEWS_TailLift_Crane_Act_P1CXB_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXB_TailLift_Crane_Act_v(void);
SEWS_CraneSwIndicationType_P1CXC_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1CXC_CraneSwIndicationType_v(void);
SEWS_TailLiftTimeoutForRequest_P1DWA_T  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1DWA_TailLiftTimeoutForRequest_v(void);
boolean  TSC_ExtraBbTailLiftCrane_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v(void);




