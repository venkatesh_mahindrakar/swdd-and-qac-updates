/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SwivelSeatSwitch_hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_SwivelSeatSwitch_hdlr.h"
#include "TSC_SwivelSeatSwitch_hdlr.h"








Std_ReturnType TSC_SwivelSeatSwitch_hdlr_Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_EngineRun_EngineRun(data);
}




Std_ReturnType TSC_SwivelSeatSwitch_hdlr_Rte_Write_SeatSwivelStatus_SeatSwivelStatus(SeatSwivelStatus_T data)
{
  return Rte_Write_SeatSwivelStatus_SeatSwivelStatus(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_SwivelSeatSwitch_hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef, AdiPinVoltage, BatteryVoltage, FaultStatus);
}


     /* Service calls */
Std_ReturnType TSC_SwivelSeatSwitch_hdlr_Rte_Call_Event_D1BUP_12_SwivelSeatSwitch_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BUP_12_SwivelSeatSwitch_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_SwivelSeatSwitch_hdlr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
{
  return (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *) Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
}
boolean  TSC_SwivelSeatSwitch_hdlr_Rte_Prm_P1CUD_SwivelSeatWarning_Act_v(void)
{
  return (boolean ) Rte_Prm_P1CUD_SwivelSeatWarning_Act_v();
}


     /* SwivelSeatSwitch_hdlr */
      /* SwivelSeatSwitch_hdlr */



