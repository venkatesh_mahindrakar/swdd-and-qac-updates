/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_MirrorHeating_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_MirrorHeating_HMICtrl_Rte_Read_MirrorHeat_rqst_ddm_MirrorHeat_rqst(MirrorHeat_T *data);
Std_ReturnType TSC_MirrorHeating_HMICtrl_Rte_Read_MirrorHeatingMode_MirrorHeatingMode(MirrorHeat_T *data);
Std_ReturnType TSC_MirrorHeating_HMICtrl_Rte_Read_MirrorHeatingSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_MirrorHeating_HMICtrl_Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_MirrorHeating_HMICtrl_Rte_Write_MirrorHeat_rqst_MirrorHeat_rqst(MirrorHeat_T data);
Std_ReturnType TSC_MirrorHeating_HMICtrl_Rte_Write_MirrorHeatingDeviceIndication_DeviceIndication(DeviceIndication_T data);




