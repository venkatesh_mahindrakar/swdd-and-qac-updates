/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  ExteriorLightPanels_Freewheel_Gw.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  ExteriorLightPanels_Freewheel_Gw
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <ExteriorLightPanels_Freewheel_Gw>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_ExteriorLightPanels_Freewheel_Gw.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_ExteriorLightPanels_Freewheel_Gw.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void ExteriorLightPanels_Freewheel_Gw_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Enumeration Types:
 * ==================
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * Freewheel_Status_Ctr_T: Enumeration of integer in interval [0...15] with enumerators
 *   Freewheel_Status_Ctr_NoMovement (0U)
 *   Freewheel_Status_Ctr_Position1 (1U)
 *   Freewheel_Status_Ctr_Position2 (2U)
 *   Freewheel_Status_Ctr_Position3 (3U)
 *   Freewheel_Status_Ctr_Position4 (4U)
 *   Freewheel_Status_Ctr_Position5 (5U)
 *   Freewheel_Status_Ctr_Position6 (6U)
 *   Freewheel_Status_Ctr_Position7 (7U)
 *   Freewheel_Status_Ctr_Position8 (8U)
 *   Freewheel_Status_Ctr_Position9 (9U)
 *   Freewheel_Status_Ctr_Position10 (10U)
 *   Freewheel_Status_Ctr_Position11 (11U)
 *   Freewheel_Status_Ctr_Position12 (12U)
 *   Freewheel_Status_Ctr_Position13 (13U)
 *   Freewheel_Status_Ctr_Error (14U)
 *   Freewheel_Status_Ctr_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


#define ExteriorLightPanels_Freewheel_Gw_START_SEC_CODE
#include "ExteriorLightPanels_Freewheel_Gw_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExteriorLightPanels_Freewheel_Gw_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_LightMode_Status_2_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   boolean Rte_IsUpdated_LightMode_Status_1_FreeWheel_Status(void)
 *   boolean Rte_IsUpdated_LightMode_Status_2_FreeWheel_Status(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LightMode_Status_Ctr_1_Freewheel_Status_Ctr(Freewheel_Status_Ctr_T data)
 *   Std_ReturnType Rte_Write_LightMode_Status_Ctr_2_Freewheel_Status_Ctr(Freewheel_Status_Ctr_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanels_Freewheel_Gw_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExteriorLightPanels_Freewheel_Gw_CODE) ExteriorLightPanels_Freewheel_Gw_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanels_Freewheel_Gw_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  FreeWheel_Status_T Read_LightMode_Status_1_FreeWheel_Status;
  boolean IsUpdated_LightMode_Status_1_FreeWheel_Status;
  FreeWheel_Status_T Read_LightMode_Status_2_FreeWheel_Status;
  boolean IsUpdated_LightMode_Status_2_FreeWheel_Status;
  VehicleModeDistribution_T Read_SwcActivation_Living_Living;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  IsUpdated_LightMode_Status_1_FreeWheel_Status = TSC_ExteriorLightPanels_Freewheel_Gw_Rte_IsUpdated_LightMode_Status_1_FreeWheel_Status();
  IsUpdated_LightMode_Status_2_FreeWheel_Status = TSC_ExteriorLightPanels_Freewheel_Gw_Rte_IsUpdated_LightMode_Status_2_FreeWheel_Status();

  fct_status = TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Read_LightMode_Status_1_FreeWheel_Status(&Read_LightMode_Status_1_FreeWheel_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Read_LightMode_Status_2_FreeWheel_Status(&Read_LightMode_Status_2_FreeWheel_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Read_SwcActivation_Living_Living(&Read_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Write_LightMode_Status_Ctr_1_Freewheel_Status_Ctr(Rte_InitValue_LightMode_Status_Ctr_1_Freewheel_Status_Ctr);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanels_Freewheel_Gw_Rte_Write_LightMode_Status_Ctr_2_Freewheel_Status_Ctr(Rte_InitValue_LightMode_Status_Ctr_2_Freewheel_Status_Ctr);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  ExteriorLightPanels_Freewheel_Gw_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define ExteriorLightPanels_Freewheel_Gw_STOP_SEC_CODE
#include "ExteriorLightPanels_Freewheel_Gw_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void ExteriorLightPanels_Freewheel_Gw_TestDefines(void)
{
  /* Enumeration Data Types */

  FreeWheel_Status_T Test_FreeWheel_Status_T_V_1 = FreeWheel_Status_NoMovement;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_2 = FreeWheel_Status_1StepClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_3 = FreeWheel_Status_2StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_4 = FreeWheel_Status_3StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_5 = FreeWheel_Status_4StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_6 = FreeWheel_Status_5StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_7 = FreeWheel_Status_6StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_8 = FreeWheel_Status_1StepCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_9 = FreeWheel_Status_2StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_10 = FreeWheel_Status_3StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_11 = FreeWheel_Status_4StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_12 = FreeWheel_Status_5StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_13 = FreeWheel_Status_6StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_14 = FreeWheel_Status_Spare;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_15 = FreeWheel_Status_Error;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_16 = FreeWheel_Status_NotAvailable;

  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_1 = Freewheel_Status_Ctr_NoMovement;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_2 = Freewheel_Status_Ctr_Position1;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_3 = Freewheel_Status_Ctr_Position2;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_4 = Freewheel_Status_Ctr_Position3;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_5 = Freewheel_Status_Ctr_Position4;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_6 = Freewheel_Status_Ctr_Position5;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_7 = Freewheel_Status_Ctr_Position6;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_8 = Freewheel_Status_Ctr_Position7;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_9 = Freewheel_Status_Ctr_Position8;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_10 = Freewheel_Status_Ctr_Position9;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_11 = Freewheel_Status_Ctr_Position10;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_12 = Freewheel_Status_Ctr_Position11;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_13 = Freewheel_Status_Ctr_Position12;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_14 = Freewheel_Status_Ctr_Position13;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_15 = Freewheel_Status_Ctr_Error;
  Freewheel_Status_Ctr_T Test_Freewheel_Status_Ctr_T_V_16 = Freewheel_Status_Ctr_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
