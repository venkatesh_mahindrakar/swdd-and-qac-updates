/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SpeedControlFreeWheel_LINMasCtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_SpeedControlFreeWheel_LINMasCtrl.h"
#include "TSC_SpeedControlFreeWheel_LINMasCtrl.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState_Irv_IOCTL_CCFWLinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState_Irv_IOCTL_CCFWLinCtrl( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvRead_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_CCFWLinCtrl(void)
{
return Rte_IrvRead_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_CCFWLinCtrl();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_CCFWLinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_CCFWLinCtrl( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_CCFWLinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_CCFWLinCtrl( data);
}





Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_ACCOrCCIndication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_ACCOrCCIndication_DeviceIndication(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_ASLIndication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_ASLIndication_DeviceIndication(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN4_ComMode_LIN(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
{
  return Rte_Read_DiagActiveState_isDiagActive(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_DiagInfoCCFW_DiagInfo(DiagInfo_T *data)
{
  return Rte_Read_DiagInfoCCFW_DiagInfo(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_FCW_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LIN_FCWPushButton_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_FCWPushButton_PushButtonStatus(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LIN_LKSPushButton_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_LKSPushButton_PushButtonStatus(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LIN_SpeedControlModeButtonStat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_SpeedControlModeButtonStat_PushButtonStatus(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LIN_SpeedControlModeWheelStat_FreeWheel_Status(FreeWheel_Status_T *data)
{
  return Rte_Read_LIN_SpeedControlModeWheelStat_FreeWheel_Status(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_LKS_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Read_ResponseErrorCCFW_ResponseErrorCCFW(ResponseErrorCCFW *data)
{
  return Rte_Read_ResponseErrorCCFW_ResponseErrorCCFW(data);
}




Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_FCWPushButton_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_FCWPushButton_PushButtonStatus(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LIN_ACCOrCCIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_ACCOrCCIndication_DeviceIndication(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LIN_ASLIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_ASLIndication_DeviceIndication(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LIN_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_FCW_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LIN_LKS_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_LKS_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_LKSPushButton_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_LKSPushButton_PushButtonStatus(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_SpeedControlModeButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_SpeedControlModeButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Write_SpeedControlModeWheelStatus_FreeWheel_Status(FreeWheel_Status_T data)
{
  return Rte_Write_SpeedControlModeWheelStatus_FreeWheel_Status(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_ResetEventStatus(void)
{
  return Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_ResetEventStatus();
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BKB_87_CCFWLink_NoResp_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_16_CCFW_VBT_ResetEventStatus(void)
{
  return Rte_Call_Event_D1BN1_16_CCFW_VBT_ResetEventStatus();
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_16_CCFW_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN1_16_CCFW_VBT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_17_CCFW_VAT_ResetEventStatus(void)
{
  return Rte_Call_Event_D1BN1_17_CCFW_VAT_ResetEventStatus();
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_17_CCFW_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN1_17_CCFW_VAT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_44_CCFW_RAM_ResetEventStatus(void)
{
  return Rte_Call_Event_D1BN1_44_CCFW_RAM_ResetEventStatus();
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_44_CCFW_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN1_44_CCFW_RAM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_45_CCFW_FLASH_ResetEventStatus(void)
{
  return Rte_Call_Event_D1BN1_45_CCFW_FLASH_ResetEventStatus();
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_45_CCFW_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN1_45_CCFW_FLASH_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_46_CCFW_EEPROM_ResetEventStatus(void)
{
  return Rte_Call_Event_D1BN1_46_CCFW_EEPROM_ResetEventStatus();
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_46_CCFW_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN1_46_CCFW_EEPROM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_ResetEventStatus(void)
{
  return Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_ResetEventStatus();
}
Std_ReturnType TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN1_94_CCFW_SWFAIL_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvRead_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl(void)
{
return Rte_IrvRead_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl();
}

void TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_IrvWrite_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl(uint8 data)
{
  Rte_IrvWrite_SpeedControlFreeWheel_LINMasCtrl_20ms_runnable_Irv_IOCTL_CCFWLinCtrl( data);
}




boolean  TSC_SpeedControlFreeWheel_LINMasCtrl_Rte_Prm_P1B2C_CCFW_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1B2C_CCFW_Installed_v();
}


     /* SpeedControlFreeWheel_LINMasCtrl */
      /* SpeedControlFreeWheel_LINMasCtrl */



