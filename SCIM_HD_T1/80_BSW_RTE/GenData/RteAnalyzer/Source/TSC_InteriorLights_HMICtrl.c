/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_InteriorLights_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_InteriorLights_HMICtrl.h"
#include "TSC_InteriorLights_HMICtrl.h"








Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_BnkH1IntLghtMMenu_stat_InteriorLightMode(InteriorLightMode_T *data)
{
  return Rte_Read_BnkH1IntLghtMMenu_stat_InteriorLightMode(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_BunkBIntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkBIntLightActvnBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_BunkH1IntLghtActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH1IntLghtActvnBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_BunkH2IntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH2IntLightActvnBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_DoorAutoFuncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_DoorAutoFuncBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtActvnBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_IntLghtActvnBtn_stat_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(FreeWheel_Status_T *data)
{
  return Rte_Read_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
{
  return Rte_Read_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(FreeWheel_Status_T *data)
{
  return Rte_Read_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtNightModeBtn2_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_IntLghtNightModeBtn2_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtNightModeBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_IntLghtNightModeBtn_stat_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtNightModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_IntLghtNightModeFlxSw2_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_IntLightMode_CoreRqst_InteriorLightMode_rqst(InteriorLightMode_rqst_T *data)
{
  return Rte_Read_IntLightMode_CoreRqst_InteriorLightMode_rqst(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd(Percent8bitNoOffset_T *data)
{
  return Rte_Read_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(uint8 *data)
{
  return Rte_Read_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_KeyfobLockButton_Status_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Living_Living(data);
}


boolean TSC_InteriorLights_HMICtrl_Rte_IsUpdated_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(void)
{
  return Rte_IsUpdated_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status();
}



Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_DoorAutoFuncInd_cmd_DeviceIndication(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_DoorAutoFunction_rqst_DoorAutoFunction_rqst(OffOn_T data)
{
  return Rte_Write_DoorAutoFunction_rqst_DoorAutoFunction_rqst(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T data)
{
  return Rte_Write_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_IntLghtModeInd_cmd_InteriorLightMode(const InteriorLightMode_T *data)
{
  return Rte_Write_IntLghtModeInd_cmd_InteriorLightMode(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_IntLghtOffModeInd_cmd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_IntLghtOffModeInd_cmd_DeviceIndication(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_IntLightMaxModeInd_cmd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_IntLightMaxModeInd_cmd_DeviceIndication(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_IntLightNightModeInd_cmd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_IntLightNightModeInd_cmd_DeviceIndication(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_IntLightRestingModeInd_cmd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_IntLightRestingModeInd_cmd_DeviceIndication(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_InteriorLightDimming_rqst_InteriorLightDimming_rqst(InteriorLightDimming_rqst_T data)
{
  return Rte_Write_InteriorLightDimming_rqst_InteriorLightDimming_rqst(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_InteriorLightMode_rqst_InteriorLightMode_rqst(const InteriorLightMode_rqst_T *data)
{
  return Rte_Write_InteriorLightMode_rqst_InteriorLightMode_rqst(data);
}

Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Write_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(const uint8 *data)
{
  return Rte_Write_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss(void)
{
  return Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss();
}
Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss();
}
Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_DimmingAdjustment2_GetIssState(Issm_IssStateType *issState)
{
  return Rte_Call_UR_ANW_DimmingAdjustment2_GetIssState(issState);
}
Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss(void)
{
  return Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss();
}
Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss();
}
Std_ReturnType TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_OtherInteriorLights3_GetIssState(Issm_IssStateType *issState)
{
  return Rte_Call_UR_ANW_OtherInteriorLights3_GetIssState(issState);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_IL_LockingCmdDelayOff_P1K7E_T  TSC_InteriorLights_HMICtrl_Rte_Prm_P1K7E_IL_LockingCmdDelayOff_v(void)
{
  return (SEWS_IL_LockingCmdDelayOff_P1K7E_T ) Rte_Prm_P1K7E_IL_LockingCmdDelayOff_v();
}
SEWS_IL_ShortLongPushThresholds_P1DKF_s_T * TSC_InteriorLights_HMICtrl_Rte_Prm_P1DKF_IL_ShortLongPushThresholds_v(void)
{
  return (SEWS_IL_ShortLongPushThresholds_P1DKF_s_T *) Rte_Prm_P1DKF_IL_ShortLongPushThresholds_v();
}
SEWS_IL_CtrlDeviceTypeFront_P1DKH_T  TSC_InteriorLights_HMICtrl_Rte_Prm_P1DKH_IL_CtrlDeviceTypeFront_v(void)
{
  return (SEWS_IL_CtrlDeviceTypeFront_P1DKH_T ) Rte_Prm_P1DKH_IL_CtrlDeviceTypeFront_v();
}
SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T  TSC_InteriorLights_HMICtrl_Rte_Prm_P1DKI_IL_CtrlDeviceTypeBunk_v(void)
{
  return (SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T ) Rte_Prm_P1DKI_IL_CtrlDeviceTypeBunk_v();
}


     /* InteriorLights_HMICtrl */
      /* InteriorLights_HMICtrl */



