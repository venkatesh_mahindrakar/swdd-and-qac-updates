/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Horn_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_Horn_HMICtrl.h"
#include "TSC_Horn_HMICtrl.h"








Std_ReturnType TSC_Horn_HMICtrl_Rte_Read_AH_PushButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_AH_PushButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_Horn_HMICtrl_Rte_Read_CH_PushButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_CH_PushButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_Horn_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Living_Living(data);
}




Std_ReturnType TSC_Horn_HMICtrl_Rte_Write_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst(OffOn_T data)
{
  return Rte_Write_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst(data);
}

Std_ReturnType TSC_Horn_HMICtrl_Rte_Write_CityHorn_HMI_rqst_CityHorn_HMI_rqst(OffOn_T data)
{
  return Rte_Write_CityHorn_HMI_rqst_CityHorn_HMI_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





boolean  TSC_Horn_HMICtrl_Rte_Prm_P1A1T_CityHorn_Act_v(void)
{
  return (boolean ) Rte_Prm_P1A1T_CityHorn_Act_v();
}
boolean  TSC_Horn_HMICtrl_Rte_Prm_P1A1U_AuxiliaryHorn_Act_v(void)
{
  return (boolean ) Rte_Prm_P1A1U_AuxiliaryHorn_Act_v();
}
boolean  TSC_Horn_HMICtrl_Rte_Prm_P1A1V_HornLivingMode_Act_v(void)
{
  return (boolean ) Rte_Prm_P1A1V_HornLivingMode_Act_v();
}


     /* Horn_HMICtrl */
      /* Horn_HMICtrl */



