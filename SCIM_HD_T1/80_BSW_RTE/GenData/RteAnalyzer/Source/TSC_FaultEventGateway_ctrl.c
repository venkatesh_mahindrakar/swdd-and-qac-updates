/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_FaultEventGateway_ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_FaultEventGateway_ctrl.h"
#include "TSC_FaultEventGateway_ctrl.h"








Std_ReturnType TSC_FaultEventGateway_ctrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}




Std_ReturnType TSC_FaultEventGateway_ctrl_Rte_Write_DiagFaultStat_DiagFaultStat(const DiagFaultStat_T *data)
{
  return Rte_Write_DiagFaultStat_DiagFaultStat(data);
}

Std_ReturnType TSC_FaultEventGateway_ctrl_Rte_Write_ModeRequest_NvmWriteAllRequest_requestedMode(BswM_BswMRteMDG_NvmWriteAllRequest data)
{
  return Rte_Write_ModeRequest_NvmWriteAllRequest_requestedMode(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_FaultEventGateway_ctrl_Rte_Call_DemServices_SynchronizeNvData(void)
{
  return Rte_Call_DemServices_SynchronizeNvData();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_FaultEventGateway_ctrl_Rte_Call_Event_D1BR9_68_Notification_LimitReached_GetDTCOfEvent(Dem_DTCFormatType DTCFormat, uint32 *DTCOfEvent)
{
  return Rte_Call_Event_D1BR9_68_Notification_LimitReached_GetDTCOfEvent(DTCFormat, DTCOfEvent);
}
Std_ReturnType TSC_FaultEventGateway_ctrl_Rte_Call_Event_D1BR9_68_Notification_LimitReached_GetEventFailed(boolean *EventFailed)
{
  return Rte_Call_Event_D1BR9_68_Notification_LimitReached_GetEventFailed(EventFailed);
}
Std_ReturnType TSC_FaultEventGateway_ctrl_Rte_Call_Event_D1BR9_68_Notification_LimitReached_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BR9_68_Notification_LimitReached_SetEventStatus(EventStatus);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_DwmVehicleModes_P1BDU_T  TSC_FaultEventGateway_ctrl_Rte_Prm_P1BDU_DwmVehicleModes_v(void)
{
  return (SEWS_DwmVehicleModes_P1BDU_T ) Rte_Prm_P1BDU_DwmVehicleModes_v();
}


     /* FaultEventGateway_ctrl */
      /* FaultEventGateway_ctrl */



