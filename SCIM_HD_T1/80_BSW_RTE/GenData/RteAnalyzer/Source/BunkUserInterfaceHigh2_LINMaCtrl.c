/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  BunkUserInterfaceHigh2_LINMaCtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  BunkUserInterfaceHigh2_LINMaCtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <BunkUserInterfaceHigh2_LINMaCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   
 *
 * Hours8bit_T
 *   
 *
 * IntLghtLvlIndScaled_cmd_T
 *   
 *
 * Minutes8bit_T
 *   
 *
 * TimesetHr_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_BunkUserInterfaceHigh2_LINMaCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_BunkUserInterfaceHigh2_LINMaCtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void BunkUserInterfaceHigh2_LINMaCtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * EventFlag_T: Boolean
 * Hours8bit_T: Integer in interval [0...255]
 *   Unit: [h], Factor: 1, Offset: 0
 * IntLghtLvlIndScaled_cmd_T: Integer in interval [0...15]
 *   Unit: [Step], Factor: 1, Offset: 0
 * Minutes8bit_T: Integer in interval [0...255]
 *   Unit: [min], Factor: 1, Offset: 0
 * ResponseErrorLECM2_T: Boolean
 * TimeMinuteType_T: Integer in interval [0...63]
 *   Unit: [min], Factor: 1, Offset: 0
 * TimesetHr_T: Integer in interval [0...31]
 *   Unit: [h], Factor: 1, Offset: 0
 * VolumeValueType_T: Integer in interval [0...63]
 *   Unit: [Step], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * AlarmClkID_T: Enumeration of integer in interval [0...127] with enumerators
 *   AlarmClkID_NotUsed (0U)
 *   AlarmClkID_Alarm1 (1U)
 *   AlarmClkID_Alarm2 (2U)
 *   AlarmClkID_Alarm3 (3U)
 *   AlarmClkID_Alarm4 (4U)
 *   AlarmClkID_Alarm5 (5U)
 *   AlarmClkID_Alarm6 (6U)
 *   AlarmClkID_Alarm7 (7U)
 *   AlarmClkID_Alarm8 (8U)
 *   AlarmClkID_Alarm9 (9U)
 *   AlarmClkID_Alarm10 (10U)
 *   AlarmClkID_Error (126U)
 *   AlarmClkID_NotAvailable (127U)
 * AlarmClkStat_T: Enumeration of integer in interval [0...3] with enumerators
 *   AlarmClkStat_Inactive (0U)
 *   AlarmClkStat_Active (1U)
 *   AlarmClkStat_NoUsed (2U)
 *   AlarmClkStat_Spare (3U)
 * AlarmClkType_T: Enumeration of integer in interval [0...7] with enumerators
 *   AlarmClkType_NoAudibleNotification (0U)
 *   AlarmClkType_Buzzer (1U)
 *   AlarmClkType_Radio (2U)
 *   AlarmClkType_Reserved (3U)
 *   AlarmClkType_Reserved_01 (4U)
 *   AlarmClkType_Reserved_02 (5U)
 *   AlarmClkType_Error (6U)
 *   AlarmClkType_NotAvailable (7U)
 * BTStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   BTStatus_BTOff (0U)
 *   BTStatus_BTOnAndNoDeviceConnected (1U)
 *   BTStatus_BTOnAndDeviceConnected (2U)
 *   BTStatus_NotAvailable (3U)
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * IL_Mode_T: Enumeration of integer in interval [0...7] with enumerators
 *   IL_Mode_OFF (0U)
 *   IL_Mode_NightDriving (1U)
 *   IL_Mode_Resting (2U)
 *   IL_Mode_Max (3U)
 *   IL_Mode_spare_1 (4U)
 *   IL_Mode_spare_2 (5U)
 *   IL_Mode_ErrorIndicator (6U)
 *   IL_Mode_NotAvailable (7U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * ParkHeaterTimer_cmd_T: Enumeration of integer in interval [0...7] with enumerators
 *   ParkHeaterTimer_cmd_NoAction (0U)
 *   ParkHeaterTimer_cmd_TimerEnable (1U)
 *   ParkHeaterTimer_cmd_TimerDisable (2U)
 *   ParkHeaterTimer_cmd_Spare (3U)
 *   ParkHeaterTimer_cmd_Spare_01 (4U)
 *   ParkHeaterTimer_cmd_Spare_02 (5U)
 *   ParkHeaterTimer_cmd_Error (6U)
 *   ParkHeaterTimer_cmd_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 *
 * Record Types:
 * =============
 * AlmClkCurAlarm_stat_T: Record with elements
 *   ID_RE of type AlarmClkID_T
 *   SetHr_RE of type TimesetHr_T
 *   SetMin_RE of type TimeMinuteType_T
 *   Stat_RE of type AlarmClkStat_T
 *   Type_RE of type AlarmClkType_T
 * AlmClkSetCurAlm_rqst_T: Record with elements
 *   ID_RE of type AlarmClkID_T
 *   SetHr_RE of type TimesetHr_T
 *   SetMin_RE of type TimeMinuteType_T
 *   Type_RE of type AlarmClkType_T
 *   Stat_RE of type AlarmClkStat_T
 * InteriorLightMode_T: Record with elements
 *   IL_Mode_RE of type IL_Mode_T
 *   EventFlag_RE of type EventFlag_T
 * SetParkHtrTmr_rqst_T: Record with elements
 *   Timer_cmd_RE of type ParkHeaterTimer_cmd_T
 *   StartTimeHr_RE of type Hours8bit_T
 *   StartTimeMin_RE of type Minutes8bit_T
 *   DurnTimeHr_RE of type Hours8bit_T
 *   DurnTimeMin_RE of type Minutes8bit_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1B2G_LECMH_Installed_v(void)
 *
 *********************************************************************************************************************/


#define BunkUserInterfaceHigh2_LINMaCtrl_START_SEC_CODE
#include "BunkUserInterfaceHigh2_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(AlmClkCurAlarm_stat_T *data)
 *   Std_ReturnType Rte_Read_AudioSystemStatus_AudioSystemStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_AudioVolumeIndicationCmd_VolumeValueType(VolumeValueType_T *data)
 *   Std_ReturnType Rte_Read_BTStatus_BTStatus(BTStatus_T *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagInfoLECM2_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T *data)
 *   Std_ReturnType Rte_Read_IntLghtModeInd_cmd_InteriorLightMode(InteriorLightMode_T *data)
 *   Std_ReturnType Rte_Read_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(AlmClkSetCurAlm_rqst_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2LockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst(SetParkHtrTmr_rqst_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_PhoneButtonIndication_cmd_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorLECM2_ResponseErrorLECM2(ResponseErrorLECM2_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(const AlmClkSetCurAlm_rqst_T *data)
 *   Std_ReturnType Rte_Write_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2Fade_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2IntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2LockButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OnOFF_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst(const SetParkHtrTmr_rqst_T *data)
 *   Std_ReturnType Rte_Write_BunkH2ParkHeater_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2Phone_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2TempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2TempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2VolumeDown_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2VolumeUp_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(const AlmClkCurAlarm_stat_T *data)
 *   Std_ReturnType Rte_Write_LIN_AudioSystemStatus_AudioSystemStatus(OffOn_T data)
 *   Std_ReturnType Rte_Write_LIN_AudioVolumeIndicationCmd_VolumeValueType(VolumeValueType_T data)
 *   Std_ReturnType Rte_Write_LIN_BTStatus_BTStatus(BTStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T data)
 *   Std_ReturnType Rte_Write_LIN_IntLghtModeInd_cmd_InteriorLightMode(const InteriorLightMode_T *data)
 *   Std_ReturnType Rte_Write_LIN_PhoneButtonIndication_cmd_DeviceIndication(DeviceIndication_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKF_87_LECMHighLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_16_LECMHigh_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_17_LECMHigh_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_44_LECMHigh_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_45_LECMHigh_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_46_LECMHigh_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_49_LECMHigh_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BOH_94_LECMHigh_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AudioRadio2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_AudioRadio2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ClimPHTimerSettings3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ClimPHTimerSettings3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights4_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights4_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving6_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PHActMaintainLiving6_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PowerWindowsActivate3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_PowerWindowsActivate3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_RoofHatchRequest3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_RoofHatchRequest3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BunkUserInterfaceHigh2_LINMaCtrl_CODE) BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BunkUserInterfaceHigh2_LINMaCtrl_10ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AlmClkCurAlarm_stat_T Read_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat;
  OffOn_T Read_AudioSystemStatus_AudioSystemStatus;
  VolumeValueType_T Read_AudioVolumeIndicationCmd_VolumeValueType;
  BTStatus_T Read_BTStatus_BTStatus;
  ComMode_LIN_Type Read_ComMode_LIN1_ComMode_LIN;
  DiagInfo_T Read_DiagInfoLECM2_DiagInfo;
  IntLghtLvlIndScaled_cmd_T Read_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd;
  InteriorLightMode_T Read_IntLghtModeInd_cmd_InteriorLightMode;
  AlmClkSetCurAlm_rqst_T Read_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst;
  PushButtonStatus_T Read_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2LockButtonStatus_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus;
  SetParkHtrTmr_rqst_T Read_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst;
  PushButtonStatus_T Read_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus;
  PushButtonStatus_T Read_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus;
  DeviceIndication_T Read_PhoneButtonIndication_cmd_DeviceIndication;
  ResponseErrorLECM2_T Read_ResponseErrorLECM2_ResponseErrorLECM2;

  boolean P1B2G_LECMH_Installed_v_data;

  AlmClkSetCurAlm_rqst_T Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst;
  SetParkHtrTmr_rqst_T Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst;
  AlmClkCurAlarm_stat_T Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat;
  InteriorLightMode_T Write_LIN_IntLghtModeInd_cmd_InteriorLightMode;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1B2G_LECMH_Installed_v_data = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Prm_P1B2G_LECMH_Installed_v();

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(&Read_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_AudioSystemStatus_AudioSystemStatus(&Read_AudioSystemStatus_AudioSystemStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_AudioVolumeIndicationCmd_VolumeValueType(&Read_AudioVolumeIndicationCmd_VolumeValueType);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_BTStatus_BTStatus(&Read_BTStatus_BTStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_ComMode_LIN1_ComMode_LIN(&Read_ComMode_LIN1_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_DiagInfoLECM2_DiagInfo(&Read_DiagInfoLECM2_DiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(&Read_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_IntLghtModeInd_cmd_InteriorLightMode(&Read_IntLghtModeInd_cmd_InteriorLightMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(&Read_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus(&Read_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus(&Read_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus(&Read_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus(&Read_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus(&Read_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2LockButtonStatus_PushButtonStatus(&Read_LIN_BunkH2LockButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus(&Read_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst(&Read_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus(&Read_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus(&Read_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus(&Read_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus(&Read_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus(&Read_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus(&Read_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus(&Read_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus(&Read_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus(&Read_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus(&Read_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus(&Read_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus(&Read_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_PhoneButtonIndication_cmd_DeviceIndication(&Read_PhoneButtonIndication_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_ResponseErrorLECM2_ResponseErrorLECM2(&Read_ResponseErrorLECM2_ResponseErrorLECM2);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  (void)memset(&Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst, 0, sizeof(Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst));
  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(&Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2Fade_ButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2Fade_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2IntLightActvnBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2IntLightActvnBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2LockButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2LockButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OnOFF_ButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2OnOFF_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst, 0, sizeof(Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst));
  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst(&Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2ParkHeater_ButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2ParkHeater_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2Phone_ButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2Phone_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(Rte_InitValue_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(Rte_InitValue_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2TempDec_ButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2TempDec_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2TempInc_ButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2TempInc_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2VolumeDown_ButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2VolumeDown_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2VolumeUp_ButtonStatus_PushButtonStatus(Rte_InitValue_BunkH2VolumeUp_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat, 0, sizeof(Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat));
  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(&Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_AudioSystemStatus_AudioSystemStatus(Rte_InitValue_LIN_AudioSystemStatus_AudioSystemStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_AudioVolumeIndicationCmd_VolumeValueType(Rte_InitValue_LIN_AudioVolumeIndicationCmd_VolumeValueType);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_BTStatus_BTStatus(Rte_InitValue_LIN_BTStatus_BTStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(Rte_InitValue_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_LIN_IntLghtModeInd_cmd_InteriorLightMode, 0, sizeof(Write_LIN_IntLghtModeInd_cmd_InteriorLightMode));
  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_IntLghtModeInd_cmd_InteriorLightMode(&Write_LIN_IntLghtModeInd_cmd_InteriorLightMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_PhoneButtonIndication_cmd_DeviceIndication(Rte_InitValue_LIN_PhoneButtonIndication_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BKF_87_LECMHighLink_NoResp_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_16_LECMHigh_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_17_LECMHigh_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_44_LECMHigh_RAM_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_45_LECMHigh_FLASH_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_46_LECMHigh_EEPROM_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_49_LECMHigh_HWFAIL_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_94_LECMHigh_SWFAIL_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_AudioRadio2_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_AudioRadio2_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_ClimPHTimerSettings3_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_ClimPHTimerSettings3_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_LockControlCabRqst2_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_LockControlCabRqst2_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_OtherInteriorLights4_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_OtherInteriorLights4_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_PHActMaintainLiving6_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_PHActMaintainLiving6_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_PowerWindowsActivate3_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_PowerWindowsActivate3_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_RoofHatchRequest3_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_RoofHatchRequest3_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  BunkUserInterfaceHigh2_LINMaCtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define BunkUserInterfaceHigh2_LINMaCtrl_STOP_SEC_CODE
#include "BunkUserInterfaceHigh2_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void BunkUserInterfaceHigh2_LINMaCtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  AlarmClkID_T Test_AlarmClkID_T_V_1 = AlarmClkID_NotUsed;
  AlarmClkID_T Test_AlarmClkID_T_V_2 = AlarmClkID_Alarm1;
  AlarmClkID_T Test_AlarmClkID_T_V_3 = AlarmClkID_Alarm2;
  AlarmClkID_T Test_AlarmClkID_T_V_4 = AlarmClkID_Alarm3;
  AlarmClkID_T Test_AlarmClkID_T_V_5 = AlarmClkID_Alarm4;
  AlarmClkID_T Test_AlarmClkID_T_V_6 = AlarmClkID_Alarm5;
  AlarmClkID_T Test_AlarmClkID_T_V_7 = AlarmClkID_Alarm6;
  AlarmClkID_T Test_AlarmClkID_T_V_8 = AlarmClkID_Alarm7;
  AlarmClkID_T Test_AlarmClkID_T_V_9 = AlarmClkID_Alarm8;
  AlarmClkID_T Test_AlarmClkID_T_V_10 = AlarmClkID_Alarm9;
  AlarmClkID_T Test_AlarmClkID_T_V_11 = AlarmClkID_Alarm10;
  AlarmClkID_T Test_AlarmClkID_T_V_12 = AlarmClkID_Error;
  AlarmClkID_T Test_AlarmClkID_T_V_13 = AlarmClkID_NotAvailable;

  AlarmClkStat_T Test_AlarmClkStat_T_V_1 = AlarmClkStat_Inactive;
  AlarmClkStat_T Test_AlarmClkStat_T_V_2 = AlarmClkStat_Active;
  AlarmClkStat_T Test_AlarmClkStat_T_V_3 = AlarmClkStat_NoUsed;
  AlarmClkStat_T Test_AlarmClkStat_T_V_4 = AlarmClkStat_Spare;

  AlarmClkType_T Test_AlarmClkType_T_V_1 = AlarmClkType_NoAudibleNotification;
  AlarmClkType_T Test_AlarmClkType_T_V_2 = AlarmClkType_Buzzer;
  AlarmClkType_T Test_AlarmClkType_T_V_3 = AlarmClkType_Radio;
  AlarmClkType_T Test_AlarmClkType_T_V_4 = AlarmClkType_Reserved;
  AlarmClkType_T Test_AlarmClkType_T_V_5 = AlarmClkType_Reserved_01;
  AlarmClkType_T Test_AlarmClkType_T_V_6 = AlarmClkType_Reserved_02;
  AlarmClkType_T Test_AlarmClkType_T_V_7 = AlarmClkType_Error;
  AlarmClkType_T Test_AlarmClkType_T_V_8 = AlarmClkType_NotAvailable;

  BTStatus_T Test_BTStatus_T_V_1 = BTStatus_BTOff;
  BTStatus_T Test_BTStatus_T_V_2 = BTStatus_BTOnAndNoDeviceConnected;
  BTStatus_T Test_BTStatus_T_V_3 = BTStatus_BTOnAndDeviceConnected;
  BTStatus_T Test_BTStatus_T_V_4 = BTStatus_NotAvailable;

  ComMode_LIN_Type Test_ComMode_LIN_Type_V_1 = Inactive;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_2 = Diagnostic;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_3 = SwitchDetection;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_4 = ApplicationMonitoring;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_5 = Calibration;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_6 = Spare1;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_7 = Error;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_8 = NotAvailable;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  IL_Mode_T Test_IL_Mode_T_V_1 = IL_Mode_OFF;
  IL_Mode_T Test_IL_Mode_T_V_2 = IL_Mode_NightDriving;
  IL_Mode_T Test_IL_Mode_T_V_3 = IL_Mode_Resting;
  IL_Mode_T Test_IL_Mode_T_V_4 = IL_Mode_Max;
  IL_Mode_T Test_IL_Mode_T_V_5 = IL_Mode_spare_1;
  IL_Mode_T Test_IL_Mode_T_V_6 = IL_Mode_spare_2;
  IL_Mode_T Test_IL_Mode_T_V_7 = IL_Mode_ErrorIndicator;
  IL_Mode_T Test_IL_Mode_T_V_8 = IL_Mode_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  ParkHeaterTimer_cmd_T Test_ParkHeaterTimer_cmd_T_V_1 = ParkHeaterTimer_cmd_NoAction;
  ParkHeaterTimer_cmd_T Test_ParkHeaterTimer_cmd_T_V_2 = ParkHeaterTimer_cmd_TimerEnable;
  ParkHeaterTimer_cmd_T Test_ParkHeaterTimer_cmd_T_V_3 = ParkHeaterTimer_cmd_TimerDisable;
  ParkHeaterTimer_cmd_T Test_ParkHeaterTimer_cmd_T_V_4 = ParkHeaterTimer_cmd_Spare;
  ParkHeaterTimer_cmd_T Test_ParkHeaterTimer_cmd_T_V_5 = ParkHeaterTimer_cmd_Spare_01;
  ParkHeaterTimer_cmd_T Test_ParkHeaterTimer_cmd_T_V_6 = ParkHeaterTimer_cmd_Spare_02;
  ParkHeaterTimer_cmd_T Test_ParkHeaterTimer_cmd_T_V_7 = ParkHeaterTimer_cmd_Error;
  ParkHeaterTimer_cmd_T Test_ParkHeaterTimer_cmd_T_V_8 = ParkHeaterTimer_cmd_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
