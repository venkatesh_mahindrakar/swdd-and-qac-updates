/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  FMSGateway.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  FMSGateway
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <FMSGateway>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * AxleLoad_T
 *   
 *
 * Blockid_T
 *   
 *
 * Distance32bit_T
 *   
 *
 * EngineTemp_T
 *   
 *
 * FuelRate_T
 *   
 *
 * GasRate_T
 *   
 *
 * InstantFuelEconomy_T
 *   
 *
 * Int8Bit_T
 *   
 *
 * Percent8bit125NegOffset_T
 *   
 *
 * Percent8bitFactor04_T
 *   
 *
 * Percent8bitNoOffset_T
 *   
 *
 * PressureFactor8_T
 *   
 *
 * RemainDistOilChange_T
 *   
 *
 * RemainEngineTimeOilChange_T
 *   
 *
 * SEWS_CabHeightValue_P1R0P_T
 *   
 *
 * SEWS_FuelTypeInformation_P1M7T_T
 *   
 *
 * SEWS_LNGTank1Volume_P1LX9_T
 *   
 *
 * SEWS_LNGTank2Volume_P1LYA_T
 *   
 *
 * SEWS_WeightClassInformation_P1M7S_T
 *   
 *
 * Seconds32bitExtra_T
 *   
 *
 * ServiceDistance16BitFact5Offset_T
 *   
 *
 * ServiceDistance_T
 *   
 *
 * ServiceTime_T
 *   
 *
 * Speed16bit_T
 *   
 *
 * SpeedRpm16bit_T
 *   
 *
 * Temperature16bit_T
 *   
 *
 * TotalLngConsumed_T
 *   
 *
 * VehicleWeight16bit_T
 *   
 *
 * Volume32BitFact001_T
 *   
 *
 * Volume32bitFact05_T
 *   
 *
 * Volume32bit_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_FMSGateway.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_FMSGateway.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void FMSGateway_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * AxleLoad_T: Integer in interval [0...65535]
 *   Unit: [kg], Factor: 1, Offset: 0
 * Blockid_T: Integer in interval [0...15]
 *   Factor: 1, Offset: 0
 * Distance32bit_T: Integer in interval [0...4294967295]
 *   Unit: [m], Factor: 1, Offset: 0
 * EngineTemp_T: Integer in interval [0...255]
 *   Unit: [DegreeC], Factor: 1, Offset: -40
 * EngineTotalHoursOfOperation_T: Integer in interval [0...4294967295]
 *   Unit: [h], Factor: 1, Offset: 0
 * FuelRate_T: Integer in interval [0...65535]
 *   Unit: [l_per_h], Factor: 1, Offset: 0
 * GasRate_T: Integer in interval [0...65535]
 *   Unit: [Kg_per_h], Factor: 1, Offset: 0
 * InstantFuelEconomy_T: Integer in interval [0...65535]
 *   Unit: [km_per_l], Factor: 1, Offset: 0
 * Int8Bit_T: Integer in interval [0...255]
 * MaintServiceID_T: Integer in interval [0...255]
 *   Unit: [ID], Factor: 1, Offset: 0
 * PGNRequest_T: Integer in interval [0...16777215]
 * Percent8bit125NegOffset_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: -125
 * Percent8bitFactor04_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: 0
 * Percent8bitNoOffset_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: 0
 * PressureFactor8_T: Integer in interval [0...255]
 *   Unit: [kPa], Factor: 1, Offset: 0
 * RemainDistOilChange_T: Integer in interval [0...65535]
 *   Unit: [km], Factor: 1, Offset: 0
 * RemainEngineTimeOilChange_T: Integer in interval [0...65535]
 *   Unit: [h], Factor: 1, Offset: 0
 * SEWS_CabHeightValue_P1R0P_T: Integer in interval [0...255]
 * SEWS_FuelTypeInformation_P1M7T_T: Integer in interval [0...255]
 * SEWS_LNGTank1Volume_P1LX9_T: Integer in interval [0...65535]
 * SEWS_LNGTank2Volume_P1LYA_T: Integer in interval [0...65535]
 * SEWS_WeightClassInformation_P1M7S_T: Integer in interval [0...255]
 * Seconds32bitExtra_T: Integer in interval [0...4294967295]
 *   Unit: [s], Factor: 1, Offset: 0
 * ServiceDistance16BitFact5Offset_T: Integer in interval [0...65535]
 *   Unit: [km], Factor: 1, Offset: -160635
 * ServiceDistance_T: Integer in interval [0...4294967295]
 *   Unit: [m], Factor: 1, Offset: -21474836000
 * ServiceTime_T: Integer in interval [0...4294967295]
 *   Unit: [s], Factor: 1, Offset: -2147483600
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * SpeedRpm16bit_T: Integer in interval [0...65535]
 *   Unit: [rpm], Factor: 1, Offset: 0
 * Temperature16bit_T: Integer in interval [0...65535]
 *   Unit: [DegreeC], Factor: 1, Offset: -273
 * TotalLngConsumed_T: Integer in interval [0...4294967295]
 *   Unit: [kg], Factor: 1, Offset: 0
 * VehicleWeight16bit_T: Integer in interval [0...65535]
 *   Unit: [kg], Factor: 1, Offset: 0
 * Volume32BitFact001_T: Integer in interval [0...4294967295]
 *   Unit: [l], Factor: 1, Offset: 0
 * Volume32bitFact05_T: Integer in interval [0...4294967295]
 *   Unit: [l], Factor: 1, Offset: 0
 * Volume32bit_T: Integer in interval [0...4294967295]
 *   Unit: [l], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BrakeSwitch_T: Enumeration of integer in interval [0...3] with enumerators
 *   BrakeSwitch_BrakePedalReleased (0U)
 *   BrakeSwitch_BrakePedalDepressed (1U)
 *   BrakeSwitch_Error (2U)
 *   BrakeSwitch_NotAvailable (3U)
 * ClutchSwitch_T: Enumeration of integer in interval [0...3] with enumerators
 *   ClutchSwitch_ClutchPedalReleased (0U)
 *   ClutchSwitch_ClutchPedalDepressed (1U)
 *   ClutchSwitch_Error (2U)
 *   ClutchSwitch_NotAvailable (3U)
 * DirectionIndicator_T: Enumeration of integer in interval [0...3] with enumerators
 *   DirectionIndicator_Forward (0U)
 *   DirectionIndicator_Reverse (1U)
 *   DirectionIndicator_Error (2U)
 *   DirectionIndicator_NotAvailable (3U)
 * Driver1TimeRelatedStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   Driver1TimeRelatedStates_NormalNoLimitsReached (0U)
 *   Driver1TimeRelatedStates_15minBefore4h30min (1U)
 *   Driver1TimeRelatedStates_4h30minReached (2U)
 *   Driver1TimeRelatedStates_DailyDrivingTimePreWarning (3U)
 *   Driver1TimeRelatedStates_DailyDrivingTimeWarning (4U)
 *   Driver1TimeRelatedStates_DailyWeeklyRestPreWarning (5U)
 *   Driver1TimeRelatedStates_DailyWeeklyRestWarning (6U)
 *   Driver1TimeRelatedStates_WeeklyDrivingTimePreWarning (7U)
 *   Driver1TimeRelatedStates_WeeklyDrivingTimeWarning (8U)
 *   Driver1TimeRelatedStates_2WeekDrivingTimePreWarning (9U)
 *   Driver1TimeRelatedStates_2WeekDrivingTimeWarning (10U)
 *   Driver1TimeRelatedStates_Driver1CardExpiryWarning (11U)
 *   Driver1TimeRelatedStates_NextMandatoryDriver1CardDownload (12U)
 *   Driver1TimeRelatedStates_Other (13U)
 *   Driver1TimeRelatedStates_Error (14U)
 *   Driver1TimeRelatedStates_NotAvailable (15U)
 * DriverTimeRelatedStates_T: Enumeration of integer in interval [0...15] with enumerators
 *   DriverTimeRelatedStates_NormalNoLimitsReached (0U)
 *   DriverTimeRelatedStates_Limit1_15minBefore4h30min (1U)
 *   DriverTimeRelatedStates_Limit2_4h30minReached (2U)
 *   DriverTimeRelatedStates_Limit3_15MinutesBefore9h (3U)
 *   DriverTimeRelatedStates_Limit4_9hReached (4U)
 *   DriverTimeRelatedStates_Limit5_15MinutesBefore16h (5U)
 *   DriverTimeRelatedStates_Limit6_16hReached (6U)
 *   DriverTimeRelatedStates_Reserved (7U)
 *   DriverTimeRelatedStates_Reserved_01 (8U)
 *   DriverTimeRelatedStates_Reserved_02 (9U)
 *   DriverTimeRelatedStates_Reserved_03 (10U)
 *   DriverTimeRelatedStates_Reserved_04 (11U)
 *   DriverTimeRelatedStates_Reserved_05 (12U)
 *   DriverTimeRelatedStates_Other (13U)
 *   DriverTimeRelatedStates_Error (14U)
 *   DriverTimeRelatedStates_NotAvailable (15U)
 * DriverWorkingState_T: Enumeration of integer in interval [0...7] with enumerators
 *   DriverWorkingState_RestSleeping (0U)
 *   DriverWorkingState_DriverAvailableShortBreak (1U)
 *   DriverWorkingState_WorkLoadingUnloadingWorkingInAnOffice (2U)
 *   DriverWorkingState_DriveBehindWheel (3U)
 *   DriverWorkingState_Reserved (4U)
 *   DriverWorkingState_Reserved_01 (5U)
 *   DriverWorkingState_ErrorIndicator (6U)
 *   DriverWorkingState_NotAvailable (7U)
 * EngineRetarderTorqueMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   EngineRetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode (0U)
 *   EngineRetarderTorqueMode_AcceleratorPedalOperatorSelection (1U)
 *   EngineRetarderTorqueMode_CruiseControl (2U)
 *   EngineRetarderTorqueMode_PTOGovernor (3U)
 *   EngineRetarderTorqueMode_RoadSpeedGovernor (4U)
 *   EngineRetarderTorqueMode_ASRControl (5U)
 *   EngineRetarderTorqueMode_TransmissionControl (6U)
 *   EngineRetarderTorqueMode_ABSControl (7U)
 *   EngineRetarderTorqueMode_TorqueLimiting (8U)
 *   EngineRetarderTorqueMode_HighSpeedGovernor (9U)
 *   EngineRetarderTorqueMode_BrakingSystem (10U)
 *   EngineRetarderTorqueMode_RemoteAccelerator (11U)
 *   EngineRetarderTorqueMode_ServiceProcedure (12U)
 *   EngineRetarderTorqueMode_NotDefined (13U)
 *   EngineRetarderTorqueMode_Other (14U)
 *   EngineRetarderTorqueMode_NotAvailable (15U)
 * FuelType_T: Enumeration of integer in interval [0...255] with enumerators
 *   FuelType_NotAvailable_NONE (0U)
 *   FuelType_GasolinePetrol_GAS (1U)
 *   FuelType_Methanol_METH (2U)
 *   FuelType_Ethanol_ETH (3U)
 *   FuelType_Diesel_DSL (4U)
 *   FuelType_LiquefiedPetroleumGas_LPG (5U)
 *   FuelType_CompressedNaturalGas_CNG (6U)
 *   FuelType_Propane_PROP (7U)
 *   FuelType_BatteryElectric_ELEC (8U)
 *   FuelType_BifuelVehicleGasoline_BI_GAS (9U)
 *   FuelType_BifuelVehicleMethanol_BI_METH (10U)
 *   FuelType_BifuelVehicleEthanol_BI_ETH (11U)
 *   FuelType_BifuelVehicleLPG_BI_LPG (12U)
 *   FuelType_BifuelVehicleCNG_BI_CNG (13U)
 *   FuelType_BifuelVehiclePropane_BI_PROP (14U)
 *   FuelType_BifuelVehicleBattery_BI_ELEC (15U)
 *   FuelType_BifuelVehicleBatteryCombustion_BI_MIX (16U)
 *   FuelType_HybridVehicleGasoline_HYB_GAS (17U)
 *   FuelType_HybridVehicleEthanol_HYB_ETH (18U)
 *   FuelType_HybridVehicleDiesel_HYB_DSL (19U)
 *   FuelType_HybridVehicleBattery_HYB_ELEC (20U)
 *   FuelType_HybridVehicleBatteryAndCombustion_HYB_MIX (21U)
 *   FuelType_HybridVehicleRegenerationMode_HYB_REG (22U)
 *   FuelType_NaturalGas_NG (23U)
 *   FuelType_BifuelVehicleNG_BI_NG (24U)
 *   FuelType_BifuelDiesel (25U)
 *   FuelType_NaturalGasCompressedOrLiquefied (26U)
 *   FuelType_DualFuel_DieselAndCNG (27U)
 *   FuelType_DualFuel_DieselAndLNG (28U)
 *   FuelType_Error (254U)
 *   FuelType_NotAvailable (255U)
 * HandlingInformation_T: Enumeration of integer in interval [0...3] with enumerators
 *   HandlingInformation_NoHandlingInformation (0U)
 *   HandlingInformation_HandlingInformation (1U)
 *   HandlingInformation_Error (2U)
 *   HandlingInformation_NotAvailable (3U)
 * NotDetected_T: Enumeration of integer in interval [0...3] with enumerators
 *   NotDetected_NotDetected (0U)
 *   NotDetected_Detected (1U)
 *   NotDetected_Error (2U)
 *   NotDetected_NotAvaliable (3U)
 * NotPresentPresent_T: Enumeration of integer in interval [0...3] with enumerators
 *   NotPresentPresent_NotPresent (0U)
 *   NotPresentPresent_Present (1U)
 *   NotPresentPresent_ErrorIndicator (2U)
 *   NotPresentPresent_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * OilQuality_T: Enumeration of integer in interval [0...7] with enumerators
 *   OilQuality_NonVDS_VDS4WithFixedInterval (0U)
 *   OilQuality_VDSVDS3WithFixedInterval (1U)
 *   OilQuality_VDS5 (2U)
 *   OilQuality_VDS3 (3U)
 *   OilQuality_VDS4 (4U)
 *   OilQuality_NoSelectionMade (5U)
 *   OilQuality_Error (6U)
 *   OilQuality_NotAvailable (7U)
 * OilStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   OilStatus_NotActive (0U)
 *   OilStatus_ServiceOverDue (1U)
 *   OilStatus_ServiceDue (2U)
 *   OilStatus_ServiceNearDue (3U)
 *   OilStatus_Ok (4U)
 *   OilStatus_Spare (5U)
 *   OilStatus_Error (6U)
 *   OilStatus_NotAvailable (7U)
 * PtoState_T: Enumeration of integer in interval [0...31] with enumerators
 *   PtoState_OffDisabled (0U)
 *   PtoState_Hold (1U)
 *   PtoState_RemoteHold (2U)
 *   PtoState_Standby (3U)
 *   PtoState_RemoteStandby (4U)
 *   PtoState_Set (5U)
 *   PtoState_DecelerateCoast (6U)
 *   PtoState_Resume (7U)
 *   PtoState_Accelerate (8U)
 *   PtoState_AcceleratorOverride (9U)
 *   PtoState_PreprogramSetSpeed1 (10U)
 *   PtoState_PreprogramSetSpeed2 (11U)
 *   PtoState_PreprogramSetSpeed3 (12U)
 *   PtoState_PreprogramSetSpeed4 (13U)
 *   PtoState_PreprogramSetSpeed5 (14U)
 *   PtoState_PreprogramSetSpeed6 (15U)
 *   PtoState_PreprogramSetSpeed7 (16U)
 *   PtoState_PreprogramSetSpeed8 (17U)
 *   PtoState_PTOSetSpeedMemory1 (18U)
 *   PtoState_PTOSetSpeedMemory2 (19U)
 *   PtoState_NotDefined (20U)
 *   PtoState_NotDefined01 (21U)
 *   PtoState_NotDefined02 (22U)
 *   PtoState_NotDefined03 (23U)
 *   PtoState_NotDefined04 (24U)
 *   PtoState_NotDefined05 (25U)
 *   PtoState_NotDefined06 (26U)
 *   PtoState_NotDefined07 (27U)
 *   PtoState_NotDefined08 (28U)
 *   PtoState_NotDefined09 (29U)
 *   PtoState_NotDefined10 (30U)
 *   PtoState_NotAvailable (31U)
 * PtosStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PtosStatus_NoPTOEngaged (0U)
 *   PtosStatus_OneOrMorePTOEngaged (1U)
 *   PtosStatus_Error (2U)
 *   PtosStatus_NotAvailable (3U)
 * RetarderTorqueMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   RetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode (0U)
 *   RetarderTorqueMode_AcceleratorPedalOperatorSelection (1U)
 *   RetarderTorqueMode_CruiseControl (2U)
 *   RetarderTorqueMode_PTOGovernor (3U)
 *   RetarderTorqueMode_RoadSpeedGovernor (4U)
 *   RetarderTorqueMode_ASRControl (5U)
 *   RetarderTorqueMode_TransmissionControl (6U)
 *   RetarderTorqueMode_ABSControl (7U)
 *   RetarderTorqueMode_TorqueLimiting (8U)
 *   RetarderTorqueMode_HighSpeedGovernor (9U)
 *   RetarderTorqueMode_BrakingSystem (10U)
 *   RetarderTorqueMode_RemoteAccelerator (11U)
 *   RetarderTorqueMode_ServiceProcedure (12U)
 *   RetarderTorqueMode_NotDefined (13U)
 *   RetarderTorqueMode_Other (14U)
 *   RetarderTorqueMode_NotAvailable (15U)
 * ReverseGearEngaged_T: Enumeration of integer in interval [0...3] with enumerators
 *   ReverseGearEngaged_ReverseGearNotEngaged (0U)
 *   ReverseGearEngaged_ReverseGearEngaged (1U)
 *   ReverseGearEngaged_Error (2U)
 *   ReverseGearEngaged_NotAvailable (3U)
 * Supported_T: Enumeration of integer in interval [0...3] with enumerators
 *   Supported_NotSupported (0U)
 *   Supported_Supported (1U)
 *   Supported_Reserved (2U)
 *   Supported_DontCare (3U)
 * SystemEvent_T: Enumeration of integer in interval [0...3] with enumerators
 *   SystemEvent_NoTachographEvent (0U)
 *   SystemEvent_TachographEvent (1U)
 *   SystemEvent_Error (2U)
 *   SystemEvent_NotAvailable (3U)
 * TachographPerformance_T: Enumeration of integer in interval [0...3] with enumerators
 *   TachographPerformance_NormalPerformance (0U)
 *   TachographPerformance_PerformanceAnalysis (1U)
 *   TachographPerformance_Error (2U)
 *   TachographPerformance_NotAvailable (3U)
 * TellTaleStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   TellTaleStatus_Off (0U)
 *   TellTaleStatus_Red (1U)
 *   TellTaleStatus_Yellow (2U)
 *   TellTaleStatus_Info (3U)
 *   TellTaleStatus_Reserved1 (4U)
 *   TellTaleStatus_Reserved2 (5U)
 *   TellTaleStatus_Reserved3 (6U)
 *   TellTaleStatus_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleOverspeed_T: Enumeration of integer in interval [0...3] with enumerators
 *   VehicleOverspeed_NoOverspeed (0U)
 *   VehicleOverspeed_Overspeed (1U)
 *   VehicleOverspeed_PreOverspeed (2U)
 *   VehicleOverspeed_NotAvailable (3U)
 * WeightClass_T: Enumeration of integer in interval [0...15] with enumerators
 *   WeightClass_Spare0 (0U)
 *   WeightClass_PassengerVehicleClassM1 (1U)
 *   WeightClass_PassengerVehicleClassM2 (2U)
 *   WeightClass_PassengerVehicleClassM3 (3U)
 *   WeightClass_LightCommercialVehiclesClassN1 (4U)
 *   WeightClass_HeavyDutyVehiclesClassN2 (5U)
 *   WeightClass_HeavyDutyVehiclesClassN3 (6U)
 *   WeightClass_Spare1 (7U)
 *   WeightClass_Spare2 (8U)
 *   WeightClass_Spare3 (9U)
 *   WeightClass_Spare4 (10U)
 *   WeightClass_Spare5 (11U)
 *   WeightClass_Spare6 (12U)
 *   WeightClass_Spare7 (13U)
 *   WeightClass_Error (14U)
 *   WeightClass_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Driver1Identification_T: Array with 14 element(s) of type uint8
 * DriversIdentifications_T: Array with 40 element(s) of type uint8
 * SEWS_VIN_VINNO_T: Array with 17 element(s) of type uint8
 * SoftwareVersionSupported_T: Array with 4 element(s) of type uint8
 * VehicleIdentNumber_T: Array with 17 element(s) of type uint8
 *
 * Record Types:
 * =============
 * FMS1_T: Record with elements
 *   Blockid_RE of type Blockid_T
 *   TellTaleStatus1_RE of type TellTaleStatus_T
 *   TellTaleStatus2_RE of type TellTaleStatus_T
 *   TellTaleStatus3_RE of type TellTaleStatus_T
 *   TellTaleStatus4_RE of type TellTaleStatus_T
 *   TellTaleStatus5_RE of type TellTaleStatus_T
 *   TellTaleStatus6_RE of type TellTaleStatus_T
 *   TellTaleStatus7_RE of type TellTaleStatus_T
 *   TellTaleStatus8_RE of type TellTaleStatus_T
 *   TellTaleStatus9_RE of type TellTaleStatus_T
 *   TellTaleStatus10_RE of type TellTaleStatus_T
 *   TellTaleStatus11_RE of type TellTaleStatus_T
 *   TellTaleStatus12_RE of type TellTaleStatus_T
 *   TellTaleStatus13_RE of type TellTaleStatus_T
 *   TellTaleStatus14_RE of type TellTaleStatus_T
 *   TellTaleStatus15_RE of type TellTaleStatus_T
 * MaintService_T: Record with elements
 *   ServiceDistance_RE of type ServiceDistance_T
 *   DistID_RE of type MaintServiceID_T
 *   ServiceCalendarTime_RE of type ServiceTime_T
 *   ServiceEngineTime_RE of type ServiceTime_T
 *   CalDateID_RE of type MaintServiceID_T
 *   EngTimeID_RE of type MaintServiceID_T
 * OilPrediction_T: Record with elements
 *   Status_RE of type OilStatus_T
 *   Quality_RE of type OilQuality_T
 *   RemainDist_RE of type RemainDistOilChange_T
 *   RemainTime_RE of type RemainEngineTimeOilChange_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_LNGTank1Volume_P1LX9_T Rte_Prm_P1LX9_LNGTank1Volume_v(void)
 *   SEWS_LNGTank2Volume_P1LYA_T Rte_Prm_P1LYA_LNGTank2Volume_v(void)
 *   SEWS_WeightClassInformation_P1M7S_T Rte_Prm_P1M7S_WeightClassInformation_v(void)
 *   SEWS_FuelTypeInformation_P1M7T_T Rte_Prm_P1M7T_FuelTypeInformation_v(void)
 *   SEWS_CabHeightValue_P1R0P_T Rte_Prm_P1R0P_CabHeightValue_v(void)
 *   boolean Rte_Prm_P1DXX_FMSgateway_Act_v(void)
 *   boolean Rte_Prm_P1LX8_EngineFuelTypeLNG_v(void)
 *   boolean Rte_Prm_P1M7Q_EraGlonassUnitInstalled_v(void)
 *   uint8 *Rte_Prm_VINNO_VIN_v(void)
 *     Returnvalue: uint8* is of type SEWS_VIN_VINNO_T
 *
 *********************************************************************************************************************/


#define FMSGateway_START_SEC_CODE
#include "FMSGateway_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FMSGateway_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_Driver1Identification_Driver1Identification(uint8 *data)
 *     Argument data: uint8* is of type Driver1Identification_T
 *   Std_ReturnType Rte_Receive_DriversIdentifications_DriversIdentifications(uint8 *data, uint16* length)
 *     Argument data: uint8* is of type DriversIdentifications_T
 *   Std_ReturnType Rte_Receive_MaintService_MaintService(MaintService_T *data)
 *   Std_ReturnType Rte_Read_AcceleratorPedalPosition1_AcceleratorPedalPosition1(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque(Percent8bit125NegOffset_T *data)
 *   Std_ReturnType Rte_Read_ActualEnginePercentTorque_ActualEnginePercentTorque(Percent8bit125NegOffset_T *data)
 *   Std_ReturnType Rte_Read_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq(Percent8bit125NegOffset_T *data)
 *   Std_ReturnType Rte_Read_AmbientAirTemperature_AmbientAirTemperature(Temperature16bit_T *data)
 *   Std_ReturnType Rte_Read_BrakeSwitch_BrakeSwitch(BrakeSwitch_T *data)
 *   Std_ReturnType Rte_Read_CCActive_CCActive(OffOn_T *data)
 *   Std_ReturnType Rte_Read_CatalystTankLevel_CatalystTankLevel(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_ClutchSwitch_ClutchSwitch(ClutchSwitch_T *data)
 *   Std_ReturnType Rte_Read_Driver1TimeRelatedStates_Driver1TimeRelatedStates(Driver1TimeRelatedStates_T *data)
 *   Std_ReturnType Rte_Read_Driver1WorkingState_Driver1WorkingState(DriverWorkingState_T *data)
 *   Std_ReturnType Rte_Read_Driver2TimeRelatedStates_Driver2TimeRelatedStates(DriverTimeRelatedStates_T *data)
 *   Std_ReturnType Rte_Read_Driver2WorkingState_Driver2WorkingState(DriverWorkingState_T *data)
 *   Std_ReturnType Rte_Read_DriverCardDriver1_DriverCardDriver1(NotPresentPresent_T *data)
 *   Std_ReturnType Rte_Read_DriverCardDriver2_DriverCardDriver2(NotPresentPresent_T *data)
 *   Std_ReturnType Rte_Read_EngineCoolantTemp_stat_EngineCoolantTemp_stat(EngineTemp_T *data)
 *   Std_ReturnType Rte_Read_EngineFuelRate_EngineFuelRate(FuelRate_T *data)
 *   Std_ReturnType Rte_Read_EngineGasRate_EngineGasRate(GasRate_T *data)
 *   Std_ReturnType Rte_Read_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd(Percent8bitNoOffset_T *data)
 *   Std_ReturnType Rte_Read_EngineRetarderTorqueMode_EngineRetarderTorqueMode(EngineRetarderTorqueMode_T *data)
 *   Std_ReturnType Rte_Read_EngineRunningTime_EngineRunningTime(Seconds32bitExtra_T *data)
 *   Std_ReturnType Rte_Read_EngineSpeed_EngineSpeed(SpeedRpm16bit_T *data)
 *   Std_ReturnType Rte_Read_EngineTotalFuelConsumed_EngineTotalFuelConsumed(Volume32bit_T *data)
 *   Std_ReturnType Rte_Read_EngineTotalLngConsumed_EngineTotalLngConsumed(TotalLngConsumed_T *data)
 *   Std_ReturnType Rte_Read_FMS1_FMS1(FMS1_T *data)
 *   Std_ReturnType Rte_Read_FuelLevel_FuelLevel(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight(VehicleWeight16bit_T *data)
 *   Std_ReturnType Rte_Read_HandlingInformation_HandlingInformation(HandlingInformation_T *data)
 *   Std_ReturnType Rte_Read_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(Volume32BitFact001_T *data)
 *   Std_ReturnType Rte_Read_InstantaneousFuelEconomy_FMSInstantFuelEconomy(InstantFuelEconomy_T *data)
 *   Std_ReturnType Rte_Read_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume(Percent8bitFactor04_T *data)
 *   Std_ReturnType Rte_Read_OilPrediction_OilPrediction(OilPrediction_T *data)
 *   Std_ReturnType Rte_Read_PtosStatus_PtosStatus(PtosStatus_T *data)
 *   Std_ReturnType Rte_Read_RetarderTorqueMode_RetarderTorqueMode(RetarderTorqueMode_T *data)
 *   Std_ReturnType Rte_Read_ReverseGearEngaged_ReverseGearEngaged(ReverseGearEngaged_T *data)
 *   Std_ReturnType Rte_Read_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1(PressureFactor8_T *data)
 *   Std_ReturnType Rte_Read_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2(PressureFactor8_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_SystemEvent_SystemEvent(SystemEvent_T *data)
 *   Std_ReturnType Rte_Read_TachographPerformance_TachographPerformance(TachographPerformance_T *data)
 *   Std_ReturnType Rte_Read_TachographVehicleSpeed_TachographVehicleSpeed(Speed16bit_T *data)
 *   Std_ReturnType Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(Distance32bit_T *data)
 *   Std_ReturnType Rte_Read_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data)
 *   Std_ReturnType Rte_Read_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data)
 *   Std_ReturnType Rte_Read_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data)
 *   Std_ReturnType Rte_Read_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data)
 *   Std_ReturnType Rte_Read_VehicleMotion_VehicleMotion(NotDetected_T *data)
 *   Std_ReturnType Rte_Read_VehicleOverspeed_VehicleOverspeed(VehicleOverspeed_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Send_FMSDriversIdentifications_DriversIdentifications(const uint8 *data, uint16 length)
 *     Argument data: uint8* is of type DriversIdentifications_T
 *   Std_ReturnType Rte_Send_FMSVehicleIdentNumber_VehicleIdentNumber(const uint8 *data)
 *     Argument data: uint8* is of type VehicleIdentNumber_T
 *   Std_ReturnType Rte_Write_AcceleratorPedalPosition1_fms_AcceleratorPedalPosition1(Percent8bitFactor04_T data)
 *   Std_ReturnType Rte_Write_ActualDrvlnRetdrPctTrq_fms_ActualDrvlnRetdrPercentTorque(Percent8bit125NegOffset_T data)
 *   Std_ReturnType Rte_Write_ActualEngRetdrPercentTrq_fms_ActualEngineRetarderPercentTrq(Percent8bit125NegOffset_T data)
 *   Std_ReturnType Rte_Write_ActualEnginePercentTorque_fms_ActualEnginePercentTorque(Percent8bit125NegOffset_T data)
 *   Std_ReturnType Rte_Write_AmbientAirTemperature_fms_AmbientAirTemperature(Temperature16bit_T data)
 *   Std_ReturnType Rte_Write_BrakeSwitch_fms_BrakeSwitch(BrakeSwitch_T data)
 *   Std_ReturnType Rte_Write_CCActive_fms_CCActive(OffOn_T data)
 *   Std_ReturnType Rte_Write_CatalystTankLevel_fms_CatalystTankLevel(Percent8bitFactor04_T data)
 *   Std_ReturnType Rte_Write_ClutchSwitch_fms_ClutchSwitch(ClutchSwitch_T data)
 *   Std_ReturnType Rte_Write_DirectionIndicator_fms_DirectionIndicator(DirectionIndicator_T data)
 *   Std_ReturnType Rte_Write_Driver1TimeRelatedStates_fms_Driver1TimeRelatedStates(Driver1TimeRelatedStates_T data)
 *   Std_ReturnType Rte_Write_Driver1WorkingState_fms_Driver1WorkingState(DriverWorkingState_T data)
 *   Std_ReturnType Rte_Write_Driver2TimeRelatedStates_fms_Driver2TimeRelatedStates(DriverTimeRelatedStates_T data)
 *   Std_ReturnType Rte_Write_Driver2WorkingState_fms_Driver2WorkingState(DriverWorkingState_T data)
 *   Std_ReturnType Rte_Write_DriverCardDriver1_fms_DriverCardDriver1(NotPresentPresent_T data)
 *   Std_ReturnType Rte_Write_DriverCardDriver2_fms_DriverCardDriver2(NotPresentPresent_T data)
 *   Std_ReturnType Rte_Write_EngineCoolantTemp_stat_fms_EngineCoolantTemp_stat(EngineTemp_T data)
 *   Std_ReturnType Rte_Write_EngineFuelRate_fms_EngineFuelRate(FuelRate_T data)
 *   Std_ReturnType Rte_Write_EnginePctLoadAtCurrentSpd_fms_EnginePercentLoadAtCurrentSpd(Percent8bitNoOffset_T data)
 *   Std_ReturnType Rte_Write_EngineRetarderTorqueMode_fms_EngineRetarderTorqueMode(EngineRetarderTorqueMode_T data)
 *   Std_ReturnType Rte_Write_EngineSpeed_fms_EngineSpeed(SpeedRpm16bit_T data)
 *   Std_ReturnType Rte_Write_FMS1_fms_FMS1(const FMS1_T *data)
 *   Std_ReturnType Rte_Write_FMSAtLeastOnePTOEngaged_AtLeastOnePTOEngaged(OffOn_T data)
 *   Std_ReturnType Rte_Write_FMSAxleLocation_AxleLocation(Int8Bit_T data)
 *   Std_ReturnType Rte_Write_FMSAxleWeight_AxleWeight(AxleLoad_T data)
 *   Std_ReturnType Rte_Write_FMSDiagnosisSupported_FMSDiagnosisSupported(Supported_T data)
 *   Std_ReturnType Rte_Write_FMSEngineTotHoursOfOp_EngineTotalHoursOfOperation(EngineTotalHoursOfOperation_T data)
 *   Std_ReturnType Rte_Write_FMSEngineTotalFuelUsed_FMSEngineTotalFuelUsed(Volume32bitFact05_T data)
 *   Std_ReturnType Rte_Write_FMSHiResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(Volume32BitFact001_T data)
 *   Std_ReturnType Rte_Write_FMSInstantFuelEconomy_FMSInstantFuelEconomy(InstantFuelEconomy_T data)
 *   Std_ReturnType Rte_Write_FMSPtoState_PtoState(PtoState_T data)
 *   Std_ReturnType Rte_Write_FMSRequestsSupported_FMSRequestsSupported(Supported_T data)
 *   Std_ReturnType Rte_Write_FMSServiceDistance_FMSServiceDistance(ServiceDistance16BitFact5Offset_T data)
 *   Std_ReturnType Rte_Write_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported(const uint8 *data)
 *     Argument data: uint8* is of type SoftwareVersionSupported_T
 *   Std_ReturnType Rte_Write_FuelLevel_fms_FuelLevel(Percent8bitFactor04_T data)
 *   Std_ReturnType Rte_Write_FuelType_FuelType(FuelType_T data)
 *   Std_ReturnType Rte_Write_GrossCombVehicleWeight_fms_GrossCombinationVehicleWeight(VehicleWeight16bit_T data)
 *   Std_ReturnType Rte_Write_HandlingInformation_fms_HandlingInformation(HandlingInformation_T data)
 *   Std_ReturnType Rte_Write_PGNReq_Tacho_CIOM_PGNRequest(PGNRequest_T data)
 *   Std_ReturnType Rte_Write_RetarderTorqueMode_fms_RetarderTorqueMode(RetarderTorqueMode_T data)
 *   Std_ReturnType Rte_Write_ServiceBrakeAirPrsCircuit1_fms_ServiceBrakeAirPrsCircuit1(PressureFactor8_T data)
 *   Std_ReturnType Rte_Write_ServiceBrakeAirPrsCircuit2_fms_ServiceBrakeAirPrsCircuit2(PressureFactor8_T data)
 *   Std_ReturnType Rte_Write_SystemEvent_fms_SystemEvent(SystemEvent_T data)
 *   Std_ReturnType Rte_Write_TachographPerformance_fms_TachographPerformance(TachographPerformance_T data)
 *   Std_ReturnType Rte_Write_TachographVehicleSpeed_fms_TachographVehicleSpeed(Speed16bit_T data)
 *   Std_ReturnType Rte_Write_TotalVehicleDistanceHiRes_fms_TotalVehicleDistanceHighRes(Distance32bit_T data)
 *   Std_ReturnType Rte_Write_VehicleMotion_fms_VehicleMotion(NotDetected_T data)
 *   Std_ReturnType Rte_Write_VehicleOverspeed_fms_VehicleOverspeed(VehicleOverspeed_T data)
 *   Std_ReturnType Rte_Write_WeightClass_WeightClass(WeightClass_T data)
 *   Std_ReturnType Rte_Write_WheelBasedVehicleSpeed_fms_WheelBasedVehicleSpeed(Speed16bit_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FMSGateway_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FMSGateway_CODE) FMSGateway_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FMSGateway_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Driver1Identification_T Receive_Driver1Identification_Driver1Identification;
  DriversIdentifications_T Receive_DriversIdentifications_DriversIdentifications;
  uint16 Receive_DriversIdentifications_DriversIdentifications_length;
  MaintService_T Receive_MaintService_MaintService;
  Percent8bitFactor04_T Read_AcceleratorPedalPosition1_AcceleratorPedalPosition1;
  Percent8bit125NegOffset_T Read_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque;
  Percent8bit125NegOffset_T Read_ActualEnginePercentTorque_ActualEnginePercentTorque;
  Percent8bit125NegOffset_T Read_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq;
  Temperature16bit_T Read_AmbientAirTemperature_AmbientAirTemperature;
  BrakeSwitch_T Read_BrakeSwitch_BrakeSwitch;
  OffOn_T Read_CCActive_CCActive;
  Percent8bitFactor04_T Read_CatalystTankLevel_CatalystTankLevel;
  ClutchSwitch_T Read_ClutchSwitch_ClutchSwitch;
  Driver1TimeRelatedStates_T Read_Driver1TimeRelatedStates_Driver1TimeRelatedStates;
  DriverWorkingState_T Read_Driver1WorkingState_Driver1WorkingState;
  DriverTimeRelatedStates_T Read_Driver2TimeRelatedStates_Driver2TimeRelatedStates;
  DriverWorkingState_T Read_Driver2WorkingState_Driver2WorkingState;
  NotPresentPresent_T Read_DriverCardDriver1_DriverCardDriver1;
  NotPresentPresent_T Read_DriverCardDriver2_DriverCardDriver2;
  EngineTemp_T Read_EngineCoolantTemp_stat_EngineCoolantTemp_stat;
  FuelRate_T Read_EngineFuelRate_EngineFuelRate;
  GasRate_T Read_EngineGasRate_EngineGasRate;
  Percent8bitNoOffset_T Read_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd;
  EngineRetarderTorqueMode_T Read_EngineRetarderTorqueMode_EngineRetarderTorqueMode;
  Seconds32bitExtra_T Read_EngineRunningTime_EngineRunningTime;
  SpeedRpm16bit_T Read_EngineSpeed_EngineSpeed;
  Volume32bit_T Read_EngineTotalFuelConsumed_EngineTotalFuelConsumed;
  TotalLngConsumed_T Read_EngineTotalLngConsumed_EngineTotalLngConsumed;
  FMS1_T Read_FMS1_FMS1;
  Percent8bitFactor04_T Read_FuelLevel_FuelLevel;
  VehicleWeight16bit_T Read_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight;
  HandlingInformation_T Read_HandlingInformation_HandlingInformation;
  Volume32BitFact001_T Read_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed;
  InstantFuelEconomy_T Read_InstantaneousFuelEconomy_FMSInstantFuelEconomy;
  Percent8bitFactor04_T Read_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume;
  Percent8bitFactor04_T Read_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume;
  OilPrediction_T Read_OilPrediction_OilPrediction;
  PtosStatus_T Read_PtosStatus_PtosStatus;
  RetarderTorqueMode_T Read_RetarderTorqueMode_RetarderTorqueMode;
  ReverseGearEngaged_T Read_ReverseGearEngaged_ReverseGearEngaged;
  PressureFactor8_T Read_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1;
  PressureFactor8_T Read_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;
  SystemEvent_T Read_SystemEvent_SystemEvent;
  TachographPerformance_T Read_TachographPerformance_TachographPerformance;
  Speed16bit_T Read_TachographVehicleSpeed_TachographVehicleSpeed;
  Distance32bit_T Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes;
  AxleLoad_T Read_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad;
  AxleLoad_T Read_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad;
  AxleLoad_T Read_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad;
  AxleLoad_T Read_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad;
  NotDetected_T Read_VehicleMotion_VehicleMotion;
  VehicleOverspeed_T Read_VehicleOverspeed_VehicleOverspeed;
  Speed16bit_T Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed;

  SEWS_LNGTank1Volume_P1LX9_T P1LX9_LNGTank1Volume_v_data;
  SEWS_LNGTank2Volume_P1LYA_T P1LYA_LNGTank2Volume_v_data;
  SEWS_WeightClassInformation_P1M7S_T P1M7S_WeightClassInformation_v_data;
  SEWS_FuelTypeInformation_P1M7T_T P1M7T_FuelTypeInformation_v_data;
  SEWS_CabHeightValue_P1R0P_T P1R0P_CabHeightValue_v_data;
  boolean P1DXX_FMSgateway_Act_v_data;
  boolean P1LX8_EngineFuelTypeLNG_v_data;
  boolean P1M7Q_EraGlonassUnitInstalled_v_data;

  SEWS_VIN_VINNO_T VINNO_VIN_v_data;

  DriversIdentifications_T Send_FMSDriversIdentifications_DriversIdentifications = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  VehicleIdentNumber_T Send_FMSVehicleIdentNumber_VehicleIdentNumber = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};

  FMS1_T Write_FMS1_fms_FMS1;
  SoftwareVersionSupported_T Write_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1LX9_LNGTank1Volume_v_data = TSC_FMSGateway_Rte_Prm_P1LX9_LNGTank1Volume_v();
  P1LYA_LNGTank2Volume_v_data = TSC_FMSGateway_Rte_Prm_P1LYA_LNGTank2Volume_v();
  P1M7S_WeightClassInformation_v_data = TSC_FMSGateway_Rte_Prm_P1M7S_WeightClassInformation_v();
  P1M7T_FuelTypeInformation_v_data = TSC_FMSGateway_Rte_Prm_P1M7T_FuelTypeInformation_v();
  P1R0P_CabHeightValue_v_data = TSC_FMSGateway_Rte_Prm_P1R0P_CabHeightValue_v();
  P1DXX_FMSgateway_Act_v_data = TSC_FMSGateway_Rte_Prm_P1DXX_FMSgateway_Act_v();
  P1LX8_EngineFuelTypeLNG_v_data = TSC_FMSGateway_Rte_Prm_P1LX8_EngineFuelTypeLNG_v();
  P1M7Q_EraGlonassUnitInstalled_v_data = TSC_FMSGateway_Rte_Prm_P1M7Q_EraGlonassUnitInstalled_v();

  (void)memcpy(VINNO_VIN_v_data, TSC_FMSGateway_Rte_Prm_VINNO_VIN_v(), sizeof(SEWS_VIN_VINNO_T));

  fct_status = TSC_FMSGateway_Rte_Receive_Driver1Identification_Driver1Identification(Receive_Driver1Identification_Driver1Identification);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Receive_DriversIdentifications_DriversIdentifications(Receive_DriversIdentifications_DriversIdentifications, &Receive_DriversIdentifications_DriversIdentifications_length);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Receive_MaintService_MaintService(&Receive_MaintService_MaintService);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_AcceleratorPedalPosition1_AcceleratorPedalPosition1(&Read_AcceleratorPedalPosition1_AcceleratorPedalPosition1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque(&Read_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_ActualEnginePercentTorque_ActualEnginePercentTorque(&Read_ActualEnginePercentTorque_ActualEnginePercentTorque);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq(&Read_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_AmbientAirTemperature_AmbientAirTemperature(&Read_AmbientAirTemperature_AmbientAirTemperature);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_BrakeSwitch_BrakeSwitch(&Read_BrakeSwitch_BrakeSwitch);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_CCActive_CCActive(&Read_CCActive_CCActive);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_CatalystTankLevel_CatalystTankLevel(&Read_CatalystTankLevel_CatalystTankLevel);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_ClutchSwitch_ClutchSwitch(&Read_ClutchSwitch_ClutchSwitch);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_Driver1TimeRelatedStates_Driver1TimeRelatedStates(&Read_Driver1TimeRelatedStates_Driver1TimeRelatedStates);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_Driver1WorkingState_Driver1WorkingState(&Read_Driver1WorkingState_Driver1WorkingState);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_Driver2TimeRelatedStates_Driver2TimeRelatedStates(&Read_Driver2TimeRelatedStates_Driver2TimeRelatedStates);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_Driver2WorkingState_Driver2WorkingState(&Read_Driver2WorkingState_Driver2WorkingState);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_DriverCardDriver1_DriverCardDriver1(&Read_DriverCardDriver1_DriverCardDriver1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_DriverCardDriver2_DriverCardDriver2(&Read_DriverCardDriver2_DriverCardDriver2);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_EngineCoolantTemp_stat_EngineCoolantTemp_stat(&Read_EngineCoolantTemp_stat_EngineCoolantTemp_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_EngineFuelRate_EngineFuelRate(&Read_EngineFuelRate_EngineFuelRate);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_EngineGasRate_EngineGasRate(&Read_EngineGasRate_EngineGasRate);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd(&Read_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_EngineRetarderTorqueMode_EngineRetarderTorqueMode(&Read_EngineRetarderTorqueMode_EngineRetarderTorqueMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_EngineRunningTime_EngineRunningTime(&Read_EngineRunningTime_EngineRunningTime);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_EngineSpeed_EngineSpeed(&Read_EngineSpeed_EngineSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_EngineTotalFuelConsumed_EngineTotalFuelConsumed(&Read_EngineTotalFuelConsumed_EngineTotalFuelConsumed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_EngineTotalLngConsumed_EngineTotalLngConsumed(&Read_EngineTotalLngConsumed_EngineTotalLngConsumed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_FMS1_FMS1(&Read_FMS1_FMS1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_FuelLevel_FuelLevel(&Read_FuelLevel_FuelLevel);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight(&Read_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_HandlingInformation_HandlingInformation(&Read_HandlingInformation_HandlingInformation);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(&Read_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_InstantaneousFuelEconomy_FMSInstantFuelEconomy(&Read_InstantaneousFuelEconomy_FMSInstantFuelEconomy);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume(&Read_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume(&Read_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_OilPrediction_OilPrediction(&Read_OilPrediction_OilPrediction);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_PtosStatus_PtosStatus(&Read_PtosStatus_PtosStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_RetarderTorqueMode_RetarderTorqueMode(&Read_RetarderTorqueMode_RetarderTorqueMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_ReverseGearEngaged_ReverseGearEngaged(&Read_ReverseGearEngaged_ReverseGearEngaged);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1(&Read_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2(&Read_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_SystemEvent_SystemEvent(&Read_SystemEvent_SystemEvent);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_TachographPerformance_TachographPerformance(&Read_TachographPerformance_TachographPerformance);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_TachographVehicleSpeed_TachographVehicleSpeed(&Read_TachographVehicleSpeed_TachographVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(&Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad(&Read_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad(&Read_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad(&Read_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad(&Read_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_VehicleMotion_VehicleMotion(&Read_VehicleMotion_VehicleMotion);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_VehicleOverspeed_VehicleOverspeed(&Read_VehicleOverspeed_VehicleOverspeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(&Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Send_FMSDriversIdentifications_DriversIdentifications(Send_FMSDriversIdentifications_DriversIdentifications, sizeof(Send_FMSDriversIdentifications_DriversIdentifications));
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Send_FMSVehicleIdentNumber_VehicleIdentNumber(Send_FMSVehicleIdentNumber_VehicleIdentNumber);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_AcceleratorPedalPosition1_fms_AcceleratorPedalPosition1(Rte_InitValue_AcceleratorPedalPosition1_fms_AcceleratorPedalPosition1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_ActualDrvlnRetdrPctTrq_fms_ActualDrvlnRetdrPercentTorque(Rte_InitValue_ActualDrvlnRetdrPctTrq_fms_ActualDrvlnRetdrPercentTorque);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_ActualEngRetdrPercentTrq_fms_ActualEngineRetarderPercentTrq(Rte_InitValue_ActualEngRetdrPercentTrq_fms_ActualEngineRetarderPercentTrq);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_ActualEnginePercentTorque_fms_ActualEnginePercentTorque(Rte_InitValue_ActualEnginePercentTorque_fms_ActualEnginePercentTorque);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_AmbientAirTemperature_fms_AmbientAirTemperature(Rte_InitValue_AmbientAirTemperature_fms_AmbientAirTemperature);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_BrakeSwitch_fms_BrakeSwitch(Rte_InitValue_BrakeSwitch_fms_BrakeSwitch);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_CCActive_fms_CCActive(Rte_InitValue_CCActive_fms_CCActive);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_CatalystTankLevel_fms_CatalystTankLevel(Rte_InitValue_CatalystTankLevel_fms_CatalystTankLevel);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_ClutchSwitch_fms_ClutchSwitch(Rte_InitValue_ClutchSwitch_fms_ClutchSwitch);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_DirectionIndicator_fms_DirectionIndicator(Rte_InitValue_DirectionIndicator_fms_DirectionIndicator);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_Driver1TimeRelatedStates_fms_Driver1TimeRelatedStates(Rte_InitValue_Driver1TimeRelatedStates_fms_Driver1TimeRelatedStates);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_Driver1WorkingState_fms_Driver1WorkingState(Rte_InitValue_Driver1WorkingState_fms_Driver1WorkingState);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_Driver2TimeRelatedStates_fms_Driver2TimeRelatedStates(Rte_InitValue_Driver2TimeRelatedStates_fms_Driver2TimeRelatedStates);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_Driver2WorkingState_fms_Driver2WorkingState(Rte_InitValue_Driver2WorkingState_fms_Driver2WorkingState);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_DriverCardDriver1_fms_DriverCardDriver1(Rte_InitValue_DriverCardDriver1_fms_DriverCardDriver1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_DriverCardDriver2_fms_DriverCardDriver2(Rte_InitValue_DriverCardDriver2_fms_DriverCardDriver2);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_EngineCoolantTemp_stat_fms_EngineCoolantTemp_stat(Rte_InitValue_EngineCoolantTemp_stat_fms_EngineCoolantTemp_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_EngineFuelRate_fms_EngineFuelRate(Rte_InitValue_EngineFuelRate_fms_EngineFuelRate);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_EnginePctLoadAtCurrentSpd_fms_EnginePercentLoadAtCurrentSpd(Rte_InitValue_EnginePctLoadAtCurrentSpd_fms_EnginePercentLoadAtCurrentSpd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_EngineRetarderTorqueMode_fms_EngineRetarderTorqueMode(Rte_InitValue_EngineRetarderTorqueMode_fms_EngineRetarderTorqueMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_EngineSpeed_fms_EngineSpeed(Rte_InitValue_EngineSpeed_fms_EngineSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FMS1_fms_FMS1, 0, sizeof(Write_FMS1_fms_FMS1));
  fct_status = TSC_FMSGateway_Rte_Write_FMS1_fms_FMS1(&Write_FMS1_fms_FMS1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSAtLeastOnePTOEngaged_AtLeastOnePTOEngaged(Rte_InitValue_FMSAtLeastOnePTOEngaged_AtLeastOnePTOEngaged);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSAxleLocation_AxleLocation(Rte_InitValue_FMSAxleLocation_AxleLocation);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSAxleWeight_AxleWeight(Rte_InitValue_FMSAxleWeight_AxleWeight);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSDiagnosisSupported_FMSDiagnosisSupported(Rte_InitValue_FMSDiagnosisSupported_FMSDiagnosisSupported);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSEngineTotHoursOfOp_EngineTotalHoursOfOperation(Rte_InitValue_FMSEngineTotHoursOfOp_EngineTotalHoursOfOperation);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSEngineTotalFuelUsed_FMSEngineTotalFuelUsed(Rte_InitValue_FMSEngineTotalFuelUsed_FMSEngineTotalFuelUsed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSHiResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(Rte_InitValue_FMSHiResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSInstantFuelEconomy_FMSInstantFuelEconomy(Rte_InitValue_FMSInstantFuelEconomy_FMSInstantFuelEconomy);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSPtoState_PtoState(Rte_InitValue_FMSPtoState_PtoState);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSRequestsSupported_FMSRequestsSupported(Rte_InitValue_FMSRequestsSupported_FMSRequestsSupported);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FMSServiceDistance_FMSServiceDistance(Rte_InitValue_FMSServiceDistance_FMSServiceDistance);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported, 0, sizeof(Write_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported));
  fct_status = TSC_FMSGateway_Rte_Write_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported(Write_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FuelLevel_fms_FuelLevel(Rte_InitValue_FuelLevel_fms_FuelLevel);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_FuelType_FuelType(Rte_InitValue_FuelType_FuelType);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_GrossCombVehicleWeight_fms_GrossCombinationVehicleWeight(Rte_InitValue_GrossCombVehicleWeight_fms_GrossCombinationVehicleWeight);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_HandlingInformation_fms_HandlingInformation(Rte_InitValue_HandlingInformation_fms_HandlingInformation);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_PGNReq_Tacho_CIOM_PGNRequest(Rte_InitValue_PGNReq_Tacho_CIOM_PGNRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_RetarderTorqueMode_fms_RetarderTorqueMode(Rte_InitValue_RetarderTorqueMode_fms_RetarderTorqueMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_ServiceBrakeAirPrsCircuit1_fms_ServiceBrakeAirPrsCircuit1(Rte_InitValue_ServiceBrakeAirPrsCircuit1_fms_ServiceBrakeAirPrsCircuit1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_ServiceBrakeAirPrsCircuit2_fms_ServiceBrakeAirPrsCircuit2(Rte_InitValue_ServiceBrakeAirPrsCircuit2_fms_ServiceBrakeAirPrsCircuit2);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_SystemEvent_fms_SystemEvent(Rte_InitValue_SystemEvent_fms_SystemEvent);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_TachographPerformance_fms_TachographPerformance(Rte_InitValue_TachographPerformance_fms_TachographPerformance);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_TachographVehicleSpeed_fms_TachographVehicleSpeed(Rte_InitValue_TachographVehicleSpeed_fms_TachographVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_TotalVehicleDistanceHiRes_fms_TotalVehicleDistanceHighRes(Rte_InitValue_TotalVehicleDistanceHiRes_fms_TotalVehicleDistanceHighRes);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_VehicleMotion_fms_VehicleMotion(Rte_InitValue_VehicleMotion_fms_VehicleMotion);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_VehicleOverspeed_fms_VehicleOverspeed(Rte_InitValue_VehicleOverspeed_fms_VehicleOverspeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_WeightClass_WeightClass(Rte_InitValue_WeightClass_WeightClass);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FMSGateway_Rte_Write_WheelBasedVehicleSpeed_fms_WheelBasedVehicleSpeed(Rte_InitValue_WheelBasedVehicleSpeed_fms_WheelBasedVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  FMSGateway_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define FMSGateway_STOP_SEC_CODE
#include "FMSGateway_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void FMSGateway_TestDefines(void)
{
  /* Enumeration Data Types */

  BrakeSwitch_T Test_BrakeSwitch_T_V_1 = BrakeSwitch_BrakePedalReleased;
  BrakeSwitch_T Test_BrakeSwitch_T_V_2 = BrakeSwitch_BrakePedalDepressed;
  BrakeSwitch_T Test_BrakeSwitch_T_V_3 = BrakeSwitch_Error;
  BrakeSwitch_T Test_BrakeSwitch_T_V_4 = BrakeSwitch_NotAvailable;

  ClutchSwitch_T Test_ClutchSwitch_T_V_1 = ClutchSwitch_ClutchPedalReleased;
  ClutchSwitch_T Test_ClutchSwitch_T_V_2 = ClutchSwitch_ClutchPedalDepressed;
  ClutchSwitch_T Test_ClutchSwitch_T_V_3 = ClutchSwitch_Error;
  ClutchSwitch_T Test_ClutchSwitch_T_V_4 = ClutchSwitch_NotAvailable;

  DirectionIndicator_T Test_DirectionIndicator_T_V_1 = DirectionIndicator_Forward;
  DirectionIndicator_T Test_DirectionIndicator_T_V_2 = DirectionIndicator_Reverse;
  DirectionIndicator_T Test_DirectionIndicator_T_V_3 = DirectionIndicator_Error;
  DirectionIndicator_T Test_DirectionIndicator_T_V_4 = DirectionIndicator_NotAvailable;

  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_1 = Driver1TimeRelatedStates_NormalNoLimitsReached;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_2 = Driver1TimeRelatedStates_15minBefore4h30min;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_3 = Driver1TimeRelatedStates_4h30minReached;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_4 = Driver1TimeRelatedStates_DailyDrivingTimePreWarning;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_5 = Driver1TimeRelatedStates_DailyDrivingTimeWarning;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_6 = Driver1TimeRelatedStates_DailyWeeklyRestPreWarning;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_7 = Driver1TimeRelatedStates_DailyWeeklyRestWarning;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_8 = Driver1TimeRelatedStates_WeeklyDrivingTimePreWarning;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_9 = Driver1TimeRelatedStates_WeeklyDrivingTimeWarning;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_10 = Driver1TimeRelatedStates_2WeekDrivingTimePreWarning;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_11 = Driver1TimeRelatedStates_2WeekDrivingTimeWarning;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_12 = Driver1TimeRelatedStates_Driver1CardExpiryWarning;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_13 = Driver1TimeRelatedStates_NextMandatoryDriver1CardDownload;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_14 = Driver1TimeRelatedStates_Other;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_15 = Driver1TimeRelatedStates_Error;
  Driver1TimeRelatedStates_T Test_Driver1TimeRelatedStates_T_V_16 = Driver1TimeRelatedStates_NotAvailable;

  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_1 = DriverTimeRelatedStates_NormalNoLimitsReached;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_2 = DriverTimeRelatedStates_Limit1_15minBefore4h30min;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_3 = DriverTimeRelatedStates_Limit2_4h30minReached;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_4 = DriverTimeRelatedStates_Limit3_15MinutesBefore9h;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_5 = DriverTimeRelatedStates_Limit4_9hReached;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_6 = DriverTimeRelatedStates_Limit5_15MinutesBefore16h;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_7 = DriverTimeRelatedStates_Limit6_16hReached;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_8 = DriverTimeRelatedStates_Reserved;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_9 = DriverTimeRelatedStates_Reserved_01;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_10 = DriverTimeRelatedStates_Reserved_02;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_11 = DriverTimeRelatedStates_Reserved_03;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_12 = DriverTimeRelatedStates_Reserved_04;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_13 = DriverTimeRelatedStates_Reserved_05;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_14 = DriverTimeRelatedStates_Other;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_15 = DriverTimeRelatedStates_Error;
  DriverTimeRelatedStates_T Test_DriverTimeRelatedStates_T_V_16 = DriverTimeRelatedStates_NotAvailable;

  DriverWorkingState_T Test_DriverWorkingState_T_V_1 = DriverWorkingState_RestSleeping;
  DriverWorkingState_T Test_DriverWorkingState_T_V_2 = DriverWorkingState_DriverAvailableShortBreak;
  DriverWorkingState_T Test_DriverWorkingState_T_V_3 = DriverWorkingState_WorkLoadingUnloadingWorkingInAnOffice;
  DriverWorkingState_T Test_DriverWorkingState_T_V_4 = DriverWorkingState_DriveBehindWheel;
  DriverWorkingState_T Test_DriverWorkingState_T_V_5 = DriverWorkingState_Reserved;
  DriverWorkingState_T Test_DriverWorkingState_T_V_6 = DriverWorkingState_Reserved_01;
  DriverWorkingState_T Test_DriverWorkingState_T_V_7 = DriverWorkingState_ErrorIndicator;
  DriverWorkingState_T Test_DriverWorkingState_T_V_8 = DriverWorkingState_NotAvailable;

  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_1 = EngineRetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_2 = EngineRetarderTorqueMode_AcceleratorPedalOperatorSelection;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_3 = EngineRetarderTorqueMode_CruiseControl;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_4 = EngineRetarderTorqueMode_PTOGovernor;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_5 = EngineRetarderTorqueMode_RoadSpeedGovernor;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_6 = EngineRetarderTorqueMode_ASRControl;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_7 = EngineRetarderTorqueMode_TransmissionControl;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_8 = EngineRetarderTorqueMode_ABSControl;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_9 = EngineRetarderTorqueMode_TorqueLimiting;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_10 = EngineRetarderTorqueMode_HighSpeedGovernor;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_11 = EngineRetarderTorqueMode_BrakingSystem;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_12 = EngineRetarderTorqueMode_RemoteAccelerator;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_13 = EngineRetarderTorqueMode_ServiceProcedure;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_14 = EngineRetarderTorqueMode_NotDefined;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_15 = EngineRetarderTorqueMode_Other;
  EngineRetarderTorqueMode_T Test_EngineRetarderTorqueMode_T_V_16 = EngineRetarderTorqueMode_NotAvailable;

  FuelType_T Test_FuelType_T_V_1 = FuelType_NotAvailable_NONE;
  FuelType_T Test_FuelType_T_V_2 = FuelType_GasolinePetrol_GAS;
  FuelType_T Test_FuelType_T_V_3 = FuelType_Methanol_METH;
  FuelType_T Test_FuelType_T_V_4 = FuelType_Ethanol_ETH;
  FuelType_T Test_FuelType_T_V_5 = FuelType_Diesel_DSL;
  FuelType_T Test_FuelType_T_V_6 = FuelType_LiquefiedPetroleumGas_LPG;
  FuelType_T Test_FuelType_T_V_7 = FuelType_CompressedNaturalGas_CNG;
  FuelType_T Test_FuelType_T_V_8 = FuelType_Propane_PROP;
  FuelType_T Test_FuelType_T_V_9 = FuelType_BatteryElectric_ELEC;
  FuelType_T Test_FuelType_T_V_10 = FuelType_BifuelVehicleGasoline_BI_GAS;
  FuelType_T Test_FuelType_T_V_11 = FuelType_BifuelVehicleMethanol_BI_METH;
  FuelType_T Test_FuelType_T_V_12 = FuelType_BifuelVehicleEthanol_BI_ETH;
  FuelType_T Test_FuelType_T_V_13 = FuelType_BifuelVehicleLPG_BI_LPG;
  FuelType_T Test_FuelType_T_V_14 = FuelType_BifuelVehicleCNG_BI_CNG;
  FuelType_T Test_FuelType_T_V_15 = FuelType_BifuelVehiclePropane_BI_PROP;
  FuelType_T Test_FuelType_T_V_16 = FuelType_BifuelVehicleBattery_BI_ELEC;
  FuelType_T Test_FuelType_T_V_17 = FuelType_BifuelVehicleBatteryCombustion_BI_MIX;
  FuelType_T Test_FuelType_T_V_18 = FuelType_HybridVehicleGasoline_HYB_GAS;
  FuelType_T Test_FuelType_T_V_19 = FuelType_HybridVehicleEthanol_HYB_ETH;
  FuelType_T Test_FuelType_T_V_20 = FuelType_HybridVehicleDiesel_HYB_DSL;
  FuelType_T Test_FuelType_T_V_21 = FuelType_HybridVehicleBattery_HYB_ELEC;
  FuelType_T Test_FuelType_T_V_22 = FuelType_HybridVehicleBatteryAndCombustion_HYB_MIX;
  FuelType_T Test_FuelType_T_V_23 = FuelType_HybridVehicleRegenerationMode_HYB_REG;
  FuelType_T Test_FuelType_T_V_24 = FuelType_NaturalGas_NG;
  FuelType_T Test_FuelType_T_V_25 = FuelType_BifuelVehicleNG_BI_NG;
  FuelType_T Test_FuelType_T_V_26 = FuelType_BifuelDiesel;
  FuelType_T Test_FuelType_T_V_27 = FuelType_NaturalGasCompressedOrLiquefied;
  FuelType_T Test_FuelType_T_V_28 = FuelType_DualFuel_DieselAndCNG;
  FuelType_T Test_FuelType_T_V_29 = FuelType_DualFuel_DieselAndLNG;
  FuelType_T Test_FuelType_T_V_30 = FuelType_Error;
  FuelType_T Test_FuelType_T_V_31 = FuelType_NotAvailable;

  HandlingInformation_T Test_HandlingInformation_T_V_1 = HandlingInformation_NoHandlingInformation;
  HandlingInformation_T Test_HandlingInformation_T_V_2 = HandlingInformation_HandlingInformation;
  HandlingInformation_T Test_HandlingInformation_T_V_3 = HandlingInformation_Error;
  HandlingInformation_T Test_HandlingInformation_T_V_4 = HandlingInformation_NotAvailable;

  NotDetected_T Test_NotDetected_T_V_1 = NotDetected_NotDetected;
  NotDetected_T Test_NotDetected_T_V_2 = NotDetected_Detected;
  NotDetected_T Test_NotDetected_T_V_3 = NotDetected_Error;
  NotDetected_T Test_NotDetected_T_V_4 = NotDetected_NotAvaliable;

  NotPresentPresent_T Test_NotPresentPresent_T_V_1 = NotPresentPresent_NotPresent;
  NotPresentPresent_T Test_NotPresentPresent_T_V_2 = NotPresentPresent_Present;
  NotPresentPresent_T Test_NotPresentPresent_T_V_3 = NotPresentPresent_ErrorIndicator;
  NotPresentPresent_T Test_NotPresentPresent_T_V_4 = NotPresentPresent_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  OilQuality_T Test_OilQuality_T_V_1 = OilQuality_NonVDS_VDS4WithFixedInterval;
  OilQuality_T Test_OilQuality_T_V_2 = OilQuality_VDSVDS3WithFixedInterval;
  OilQuality_T Test_OilQuality_T_V_3 = OilQuality_VDS5;
  OilQuality_T Test_OilQuality_T_V_4 = OilQuality_VDS3;
  OilQuality_T Test_OilQuality_T_V_5 = OilQuality_VDS4;
  OilQuality_T Test_OilQuality_T_V_6 = OilQuality_NoSelectionMade;
  OilQuality_T Test_OilQuality_T_V_7 = OilQuality_Error;
  OilQuality_T Test_OilQuality_T_V_8 = OilQuality_NotAvailable;

  OilStatus_T Test_OilStatus_T_V_1 = OilStatus_NotActive;
  OilStatus_T Test_OilStatus_T_V_2 = OilStatus_ServiceOverDue;
  OilStatus_T Test_OilStatus_T_V_3 = OilStatus_ServiceDue;
  OilStatus_T Test_OilStatus_T_V_4 = OilStatus_ServiceNearDue;
  OilStatus_T Test_OilStatus_T_V_5 = OilStatus_Ok;
  OilStatus_T Test_OilStatus_T_V_6 = OilStatus_Spare;
  OilStatus_T Test_OilStatus_T_V_7 = OilStatus_Error;
  OilStatus_T Test_OilStatus_T_V_8 = OilStatus_NotAvailable;

  PtoState_T Test_PtoState_T_V_1 = PtoState_OffDisabled;
  PtoState_T Test_PtoState_T_V_2 = PtoState_Hold;
  PtoState_T Test_PtoState_T_V_3 = PtoState_RemoteHold;
  PtoState_T Test_PtoState_T_V_4 = PtoState_Standby;
  PtoState_T Test_PtoState_T_V_5 = PtoState_RemoteStandby;
  PtoState_T Test_PtoState_T_V_6 = PtoState_Set;
  PtoState_T Test_PtoState_T_V_7 = PtoState_DecelerateCoast;
  PtoState_T Test_PtoState_T_V_8 = PtoState_Resume;
  PtoState_T Test_PtoState_T_V_9 = PtoState_Accelerate;
  PtoState_T Test_PtoState_T_V_10 = PtoState_AcceleratorOverride;
  PtoState_T Test_PtoState_T_V_11 = PtoState_PreprogramSetSpeed1;
  PtoState_T Test_PtoState_T_V_12 = PtoState_PreprogramSetSpeed2;
  PtoState_T Test_PtoState_T_V_13 = PtoState_PreprogramSetSpeed3;
  PtoState_T Test_PtoState_T_V_14 = PtoState_PreprogramSetSpeed4;
  PtoState_T Test_PtoState_T_V_15 = PtoState_PreprogramSetSpeed5;
  PtoState_T Test_PtoState_T_V_16 = PtoState_PreprogramSetSpeed6;
  PtoState_T Test_PtoState_T_V_17 = PtoState_PreprogramSetSpeed7;
  PtoState_T Test_PtoState_T_V_18 = PtoState_PreprogramSetSpeed8;
  PtoState_T Test_PtoState_T_V_19 = PtoState_PTOSetSpeedMemory1;
  PtoState_T Test_PtoState_T_V_20 = PtoState_PTOSetSpeedMemory2;
  PtoState_T Test_PtoState_T_V_21 = PtoState_NotDefined;
  PtoState_T Test_PtoState_T_V_22 = PtoState_NotDefined01;
  PtoState_T Test_PtoState_T_V_23 = PtoState_NotDefined02;
  PtoState_T Test_PtoState_T_V_24 = PtoState_NotDefined03;
  PtoState_T Test_PtoState_T_V_25 = PtoState_NotDefined04;
  PtoState_T Test_PtoState_T_V_26 = PtoState_NotDefined05;
  PtoState_T Test_PtoState_T_V_27 = PtoState_NotDefined06;
  PtoState_T Test_PtoState_T_V_28 = PtoState_NotDefined07;
  PtoState_T Test_PtoState_T_V_29 = PtoState_NotDefined08;
  PtoState_T Test_PtoState_T_V_30 = PtoState_NotDefined09;
  PtoState_T Test_PtoState_T_V_31 = PtoState_NotDefined10;
  PtoState_T Test_PtoState_T_V_32 = PtoState_NotAvailable;

  PtosStatus_T Test_PtosStatus_T_V_1 = PtosStatus_NoPTOEngaged;
  PtosStatus_T Test_PtosStatus_T_V_2 = PtosStatus_OneOrMorePTOEngaged;
  PtosStatus_T Test_PtosStatus_T_V_3 = PtosStatus_Error;
  PtosStatus_T Test_PtosStatus_T_V_4 = PtosStatus_NotAvailable;

  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_1 = RetarderTorqueMode_LowIdleGovernorNoRequestDefaultMode;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_2 = RetarderTorqueMode_AcceleratorPedalOperatorSelection;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_3 = RetarderTorqueMode_CruiseControl;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_4 = RetarderTorqueMode_PTOGovernor;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_5 = RetarderTorqueMode_RoadSpeedGovernor;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_6 = RetarderTorqueMode_ASRControl;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_7 = RetarderTorqueMode_TransmissionControl;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_8 = RetarderTorqueMode_ABSControl;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_9 = RetarderTorqueMode_TorqueLimiting;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_10 = RetarderTorqueMode_HighSpeedGovernor;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_11 = RetarderTorqueMode_BrakingSystem;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_12 = RetarderTorqueMode_RemoteAccelerator;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_13 = RetarderTorqueMode_ServiceProcedure;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_14 = RetarderTorqueMode_NotDefined;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_15 = RetarderTorqueMode_Other;
  RetarderTorqueMode_T Test_RetarderTorqueMode_T_V_16 = RetarderTorqueMode_NotAvailable;

  ReverseGearEngaged_T Test_ReverseGearEngaged_T_V_1 = ReverseGearEngaged_ReverseGearNotEngaged;
  ReverseGearEngaged_T Test_ReverseGearEngaged_T_V_2 = ReverseGearEngaged_ReverseGearEngaged;
  ReverseGearEngaged_T Test_ReverseGearEngaged_T_V_3 = ReverseGearEngaged_Error;
  ReverseGearEngaged_T Test_ReverseGearEngaged_T_V_4 = ReverseGearEngaged_NotAvailable;

  Supported_T Test_Supported_T_V_1 = Supported_NotSupported;
  Supported_T Test_Supported_T_V_2 = Supported_Supported;
  Supported_T Test_Supported_T_V_3 = Supported_Reserved;
  Supported_T Test_Supported_T_V_4 = Supported_DontCare;

  SystemEvent_T Test_SystemEvent_T_V_1 = SystemEvent_NoTachographEvent;
  SystemEvent_T Test_SystemEvent_T_V_2 = SystemEvent_TachographEvent;
  SystemEvent_T Test_SystemEvent_T_V_3 = SystemEvent_Error;
  SystemEvent_T Test_SystemEvent_T_V_4 = SystemEvent_NotAvailable;

  TachographPerformance_T Test_TachographPerformance_T_V_1 = TachographPerformance_NormalPerformance;
  TachographPerformance_T Test_TachographPerformance_T_V_2 = TachographPerformance_PerformanceAnalysis;
  TachographPerformance_T Test_TachographPerformance_T_V_3 = TachographPerformance_Error;
  TachographPerformance_T Test_TachographPerformance_T_V_4 = TachographPerformance_NotAvailable;

  TellTaleStatus_T Test_TellTaleStatus_T_V_1 = TellTaleStatus_Off;
  TellTaleStatus_T Test_TellTaleStatus_T_V_2 = TellTaleStatus_Red;
  TellTaleStatus_T Test_TellTaleStatus_T_V_3 = TellTaleStatus_Yellow;
  TellTaleStatus_T Test_TellTaleStatus_T_V_4 = TellTaleStatus_Info;
  TellTaleStatus_T Test_TellTaleStatus_T_V_5 = TellTaleStatus_Reserved1;
  TellTaleStatus_T Test_TellTaleStatus_T_V_6 = TellTaleStatus_Reserved2;
  TellTaleStatus_T Test_TellTaleStatus_T_V_7 = TellTaleStatus_Reserved3;
  TellTaleStatus_T Test_TellTaleStatus_T_V_8 = TellTaleStatus_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;

  VehicleOverspeed_T Test_VehicleOverspeed_T_V_1 = VehicleOverspeed_NoOverspeed;
  VehicleOverspeed_T Test_VehicleOverspeed_T_V_2 = VehicleOverspeed_Overspeed;
  VehicleOverspeed_T Test_VehicleOverspeed_T_V_3 = VehicleOverspeed_PreOverspeed;
  VehicleOverspeed_T Test_VehicleOverspeed_T_V_4 = VehicleOverspeed_NotAvailable;

  WeightClass_T Test_WeightClass_T_V_1 = WeightClass_Spare0;
  WeightClass_T Test_WeightClass_T_V_2 = WeightClass_PassengerVehicleClassM1;
  WeightClass_T Test_WeightClass_T_V_3 = WeightClass_PassengerVehicleClassM2;
  WeightClass_T Test_WeightClass_T_V_4 = WeightClass_PassengerVehicleClassM3;
  WeightClass_T Test_WeightClass_T_V_5 = WeightClass_LightCommercialVehiclesClassN1;
  WeightClass_T Test_WeightClass_T_V_6 = WeightClass_HeavyDutyVehiclesClassN2;
  WeightClass_T Test_WeightClass_T_V_7 = WeightClass_HeavyDutyVehiclesClassN3;
  WeightClass_T Test_WeightClass_T_V_8 = WeightClass_Spare1;
  WeightClass_T Test_WeightClass_T_V_9 = WeightClass_Spare2;
  WeightClass_T Test_WeightClass_T_V_10 = WeightClass_Spare3;
  WeightClass_T Test_WeightClass_T_V_11 = WeightClass_Spare4;
  WeightClass_T Test_WeightClass_T_V_12 = WeightClass_Spare5;
  WeightClass_T Test_WeightClass_T_V_13 = WeightClass_Spare6;
  WeightClass_T Test_WeightClass_T_V_14 = WeightClass_Spare7;
  WeightClass_T Test_WeightClass_T_V_15 = WeightClass_Error;
  WeightClass_T Test_WeightClass_T_V_16 = WeightClass_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
