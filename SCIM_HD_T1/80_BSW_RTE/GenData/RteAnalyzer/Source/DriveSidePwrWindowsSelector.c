/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  DriveSidePwrWindowsSelector.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  DriveSidePwrWindowsSelector
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <DriveSidePwrWindowsSelector>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_DriverPosition_LHD_RHD_P1ALJ_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_DriveSidePwrWindowsSelector.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_DriveSidePwrWindowsSelector.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void DriveSidePwrWindowsSelector_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_DriverPosition_LHD_RHD_P1ALJ_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_DriverPosition_LHD_RHD_P1ALJ_T Rte_Prm_P1ALJ_DriverPosition_LHD_RHD_v(void)
 *   boolean Rte_Prm_P1B2G_LECMH_Installed_v(void)
 *
 *********************************************************************************************************************/


#define DriveSidePwrWindowsSelector_START_SEC_CODE
#include "DriveSidePwrWindowsSelector_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DriveSidePwrWindowsSelector_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BunkH2PowerWinCloseDSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2PowerWinClosePSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2PowerWinOpenDSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_BunkH2PowerWinOpenPSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DriveSidePwrWindowsSelector_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DriveSidePwrWindowsSelector_CODE) DriveSidePwrWindowsSelector_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DriveSidePwrWindowsSelector_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PushButtonStatus_T Read_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus;
  A3PosSwitchStatus_T Read_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus;
  A3PosSwitchStatus_T Read_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus;
  VehicleModeDistribution_T Read_SwcActivation_Living_Living;

  SEWS_DriverPosition_LHD_RHD_P1ALJ_T P1ALJ_DriverPosition_LHD_RHD_v_data;
  boolean P1B2G_LECMH_Installed_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1ALJ_DriverPosition_LHD_RHD_v_data = TSC_DriveSidePwrWindowsSelector_Rte_Prm_P1ALJ_DriverPosition_LHD_RHD_v();
  P1B2G_LECMH_Installed_v_data = TSC_DriveSidePwrWindowsSelector_Rte_Prm_P1B2G_LECMH_Installed_v();

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Read_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(&Read_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Read_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(&Read_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Read_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(&Read_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Read_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(&Read_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Read_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus(&Read_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Read_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus(&Read_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Read_SwcActivation_Living_Living(&Read_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Write_BunkH2PowerWinCloseDSBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2PowerWinCloseDSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Write_BunkH2PowerWinClosePSBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2PowerWinClosePSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Write_BunkH2PowerWinOpenDSBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2PowerWinOpenDSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriveSidePwrWindowsSelector_Rte_Write_BunkH2PowerWinOpenPSBtn_stat_PushButtonStatus(Rte_InitValue_BunkH2PowerWinOpenPSBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  DriveSidePwrWindowsSelector_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define DriveSidePwrWindowsSelector_STOP_SEC_CODE
#include "DriveSidePwrWindowsSelector_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void DriveSidePwrWindowsSelector_TestDefines(void)
{
  /* Enumeration Data Types */

  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_1 = A3PosSwitchStatus_Middle;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_2 = A3PosSwitchStatus_Lower;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_3 = A3PosSwitchStatus_Upper;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_4 = A3PosSwitchStatus_Spare;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_5 = A3PosSwitchStatus_Spare_01;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_6 = A3PosSwitchStatus_Spare_02;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_7 = A3PosSwitchStatus_Error;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_8 = A3PosSwitchStatus_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
