/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ExtraLighting_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_ExtraLighting_HMICtrl.h"
#include "TSC_ExtraLighting_HMICtrl.h"








Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_BBNetwBeaconLight_stat_BBNetwBeaconLight_stat(PushButtonStatus_T *data)
{
  return Rte_Read_BBNetwBeaconLight_stat_BBNetwBeaconLight_stat(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_BeaconSRocker_DeviceEvent_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_BeaconSRocker_DeviceEvent_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_Beacon_DeviceEven_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_Beacon_DeviceEven_PushButtonStatus(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_CabBeaconLightFeedback_Status_CabBeaconLightFeedback_Status(OffOn_T *data)
{
  return Rte_Read_CabBeaconLightFeedback_Status_CabBeaconLightFeedback_Status(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_SwcActivation_Parked_Parked(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Parked_Parked(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_WRCBeaconRequest_WRCBeaconRequest(PushButtonStatus_T *data)
{
  return Rte_Read_WRCBeaconRequest_WRCBeaconRequest(data);
}




Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_BeaconSRocker_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_BeaconSRocker_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_Beacon_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_Beacon_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_CabBeaconLight_rqst_CabBeaconLight_rqst(Request_T data)
{
  return Rte_Write_CabBeaconLight_rqst_CabBeaconLight_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_BBNetwBodyOrCabWrknLight_stat_BBNetwBodyOrCabWrknLight_stat(PushButtonStatus_T *data)
{
  return Rte_Read_BBNetwBodyOrCabWrknLight_stat_BBNetwBodyOrCabWrknLight_stat(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_BodyOrCabWorkingLightFdbk_stat_BodyOrCabWorkingLightFdbk_stat(OffOn_T *data)
{
  return Rte_Read_BodyOrCabWorkingLightFdbk_stat_BodyOrCabWorkingLightFdbk_stat(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_CabWorkLight_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_CabWorkLight_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_WRCCabBodyWLightsRqst_WRCCabBodyWLightsRqst(PushButtonStatus_T *data)
{
  return Rte_Read_WRCCabBodyWLightsRqst_WRCCabBodyWLightsRqst(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_WorkLight_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_WorkLight_ButtonStatus_PushButtonStatus(data);
}




Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_BodyOrCabWorkingLight_rqst_BodyOrCabWorkingLight_rqst(Request_T data)
{
  return Rte_Write_BodyOrCabWorkingLight_rqst_BodyOrCabWorkingLight_rqst(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_CabWorkingLight_DevInd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_CabWorkingLight_DevInd_DeviceIndication(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_WorkingLight_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_WorkingLight_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_BBNetwWrknLightChassis_stat_BBNetwWrknLightChassis_stat(PushButtonStatus_T *data)
{
  return Rte_Read_BBNetwWrknLightChassis_stat_BBNetwWrknLightChassis_stat(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_CabWrknLightChassisFdbk_stat_CabWrknLightChassisFdbk_stat(OffOn_T *data)
{
  return Rte_Read_CabWrknLightChassisFdbk_stat_CabWrknLightChassisFdbk_stat(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_FifthWheelLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_FifthWheelLight_DeviceEvent_PushButtonStatus(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_RearWorkProjector_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_RearWorkProjector_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_WRC5thWheelRequest_WRC5thWheelRequest(PushButtonStatus_T *data)
{
  return Rte_Read_WRC5thWheelRequest_WRC5thWheelRequest(data);
}




Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_CabWorkingLightChassis_rqst_CabWorkingLightChassis_rqst(Request_T data)
{
  return Rte_Write_CabWorkingLightChassis_rqst_CabWorkingLightChassis_rqst(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_FifthWheelLightInd_cmd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_FifthWheelLightInd_cmd_DeviceIndication(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_RearWorkProjector_Indication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_RearWorkProjector_Indication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_BBNetwTrailerBodyLighting_rqst_BBNetwTrailerBodyLighting_rqst(PushButtonStatus_T *data)
{
  return Rte_Read_BBNetwTrailerBodyLighting_rqst_BBNetwTrailerBodyLighting_rqst(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_CabTrailerBodyLgthnFdbk_stat_CabTrailerBodyLgthnFdbk_stat(OffOn_T *data)
{
  return Rte_Read_CabTrailerBodyLgthnFdbk_stat_CabTrailerBodyLgthnFdbk_stat(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_EquipmentLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_EquipmentLight_DeviceEvent_PushButtonStatus(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_TrailerBodyLampDI_stat_TrailerBodyLampDI_stat(PushButtonStatus_T *data)
{
  return Rte_Read_TrailerBodyLampDI_stat_TrailerBodyLampDI_stat(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_TrailerBodyLampFdbk_stat_TrailerBodyLampFdbk_stat(OffOn_T *data)
{
  return Rte_Read_TrailerBodyLampFdbk_stat_TrailerBodyLampFdbk_stat(data);
}




Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_CabTrailerBodyLighting_rqst_CabTrailerBodyLighting_rqst(Request_T data)
{
  return Rte_Write_CabTrailerBodyLighting_rqst_CabTrailerBodyLighting_rqst(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_EquipmentLightInd_cmd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_EquipmentLightInd_cmd_DeviceIndication(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_TrailerBodyLamp_rqst_TrailerBodyLamp_rqst(PushButtonStatus_T data)
{
  return Rte_Write_TrailerBodyLamp_rqst_TrailerBodyLamp_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_SpotlightFront_DeviceEvent_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_SpotlightFront_DeviceEvent_PushButtonStatus(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_SpotlightRoof_DeviceEvent_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_SpotlightRoof_DeviceEvent_PushButtonStatus(data);
}




Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_CabFrontSpot_rqst_CabFrontSpot_rqst(Request_T data)
{
  return Rte_Write_CabFrontSpot_rqst_CabFrontSpot_rqst(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_CabRoofSpot_rqst_CabRoofSpot_rqst(Request_T data)
{
  return Rte_Write_CabRoofSpot_rqst_CabRoofSpot_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_PloughLampModeStatus_PloughLampModeStatus(InactiveActive_T *data)
{
  return Rte_Read_PloughLampModeStatus_PloughLampModeStatus(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_PloughLight_DeviceEvent_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_PloughLight_DeviceEvent_PushButtonStatus(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_PloughtLightsPushButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_PloughtLightsPushButtonStatus_PushButtonStatus(data);
}




Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_CabPlowLight_rqst_CabPlowLight_rqst(Request_T data)
{
  return Rte_Write_CabPlowLight_rqst_CabPlowLight_rqst(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_PloughtLights_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_PloughtLights_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_CabRoofSignLightFeedback_stat_CabRoofSignLightFeedback_stat(OffOn_T *data)
{
  return Rte_Read_CabRoofSignLightFeedback_stat_CabRoofSignLightFeedback_stat(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Read_LEDVega_DeviceEvent_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LEDVega_DeviceEvent_PushButtonStatus(data);
}




Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_CabRoofSignOrVegaLight_rqst_CabRoofSignOrVegaLight_rqst(Request_T data)
{
  return Rte_Write_CabRoofSignOrVegaLight_rqst_CabRoofSignOrVegaLight_rqst(data);
}

Std_ReturnType TSC_ExtraLighting_HMICtrl_Rte_Write_LEDVega_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LEDVega_DeviceIndication_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* ExtraLighting_HMICtrl */
      /* ExtraLighting_HMICtrl */



