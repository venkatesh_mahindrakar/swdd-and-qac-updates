/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  NvM_Cfg.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  NvMBlockDescriptor definitions
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _NVM_CFG_H
# define _NVM_CFG_H

/* NvMBlockDescriptor definitions */

# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_DriverAuth2_Ctrl_NVM (0UL)
# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_EngTraceHW_NvM (1UL)
# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_InteriorLights_HMICtrl_NVM (2UL)
# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_MUT_UICtrl_Difflock_NVM (3UL)
# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_MUT_UICtrl_Traction_NVM (4UL)
# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_PinCode_ctrl_NVM (5UL)
# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_RGW_HMICtrl_NVM (6UL)
# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_SCM_HMICtrl_NVM (7UL)
# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_VehicleAccess_Ctrl_NVM (8UL)
# define NvMConf_NvMBlockDescriptor_Application_Data_NVM_VehicleModeDistribution_NVM (9UL)
# define NvMConf_NvMBlockDescriptor_Calibration_Data_NVM_FSP_NVM (10UL)
# define NvMConf_NvMBlockDescriptor_Keyfob_Radio_Com_NVM_KeyFobNVBlock (11UL)

#endif /* _NVM_CFG_H */
