/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Keyfob_Mgr.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  Keyfob_Mgr
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <Keyfob_Mgr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 * SEWS_CrankingLockActivation_P1DS3_T
 *   
 *
 * SEWS_KeyMatchingIndicationTimeout_X1CV3_T
 *   
 *
 * SEWS_KeyfobEncryptCode_P1DS4_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_Keyfob_Mgr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_Keyfob_Mgr.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void Keyfob_Mgr_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_CrankingLockActivation_P1DS3_T: Integer in interval [0...255]
 * SEWS_KeyMatchingIndicationTimeout_X1CV3_T: Integer in interval [0...255]
 * SEWS_KeyfobEncryptCode_P1DS4_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * DriverAuthDeviceMatching_T: Enumeration of integer in interval [0...3] with enumerators
 *   DriverAuthDeviceMatching_Idle (0U)
 *   DriverAuthDeviceMatching_DeviceToMatchIsPresent (1U)
 *   DriverAuthDeviceMatching_Error (2U)
 *   DriverAuthDeviceMatching_NotAvailable (3U)
 * KeyfobAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_rqst_Idle (0U)
 *   KeyfobAuth_rqst_RequestByPassiveMechanism (1U)
 *   KeyfobAuth_rqst_RequestByImmobilizerMechanism (2U)
 *   KeyfobAuth_rqst_Spare1 (3U)
 *   KeyfobAuth_rqst_Spare2 (4U)
 *   KeyfobAuth_rqst_Spare3 (5U)
 *   KeyfobAuth_rqst_Error (6U)
 *   KeyfobAuth_rqst_NotAavailable (7U)
 * KeyfobAuth_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_stat_Idle (0U)
 *   KeyfobAuth_stat_NokeyfobAuthenticated (1U)
 *   KeyfobAuth_stat_KeyfobAuthenticated (2U)
 *   KeyfobAuth_stat_Spare1 (3U)
 *   KeyfobAuth_stat_Spare2 (4U)
 *   KeyfobAuth_stat_Spare3 (5U)
 *   KeyfobAuth_stat_Error (6U)
 *   KeyfobAuth_stat_NotAvailable (7U)
 * KeyfobInCabLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobInCabLocation_stat_Idle (0U)
 *   KeyfobInCabLocation_stat_NotDetected_Incab (1U)
 *   KeyfobInCabLocation_stat_Detected_Incab (2U)
 *   KeyfobInCabLocation_stat_Spare1 (3U)
 *   KeyfobInCabLocation_stat_Spare2 (4U)
 *   KeyfobInCabLocation_stat_Spare3 (5U)
 *   KeyfobInCabLocation_stat_Error (6U)
 *   KeyfobInCabLocation_stat_NotAvailable (7U)
 * KeyfobInCabPresencePS_T: Enumeration of integer in interval [0...3] with enumerators
 *   KeyfobInCabPresencePS_rqst_NoRequest (0U)
 *   KeyfobInCabPresencePS_rqst_RequestDetectionInCab (1U)
 *   KeyfobInCabPresencePS_rqst_Error (2U)
 *   KeyfobInCabPresencePS_rqst_NotAvailable (3U)
 * KeyfobLocation_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   KeyfobLocation_rqst_NoRequest (0U)
 *   KeyfobLocation_rqst_Request (1U)
 *   KeyfobLocation_rqst_Error (2U)
 *   KeyfobLocation_rqst_NotAvailable (3U)
 * KeyfobOutsideLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobOutsideLocation_stat_Idle (0U)
 *   KeyfobOutsideLocation_stat_NotDetectedOutside (1U)
 *   KeyfobOutsideLocation_stat_DetectedOnRightSide (2U)
 *   KeyfobOutsideLocation_stat_DetectedOnLeftSide (3U)
 *   KeyfobOutsideLocation_stat_DetectedOnBothSides (4U)
 *   KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas (5U)
 *   KeyfobOutsideLocation_stat_Error (6U)
 *   KeyfobOutsideLocation_stat_NotAvailable (7U)
 * SCIM_ImmoDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 *   ImmoDriver_ProcessingStatus_Idle (0U)
 *   ImmoDriver_ProcessingStatus_Ongoing (1U)
 *   ImmoDriver_ProcessingStatus_LfSent (2U)
 *   ImmoDriver_ProcessingStatus_DecryptedInList (3U)
 *   ImmoDriver_ProcessingStatus_DecryptedNotInList (4U)
 *   ImmoDriver_ProcessingStatus_NotDecrypted (5U)
 *   ImmoDriver_ProcessingStatus_HwError (6U)
 * SCIM_ImmoType_T: Enumeration of integer in interval [0...255] with enumerators
 *   ImmoType_ATA5702 (0U)
 *   ImmoType_ATA5577 (1U)
 * SCIM_PassiveDriver_ProcessingStatus_T: Enumeration of integer in interval [0...255] with enumerators
 *   PassiveDriver_ProcessingStatus_Idle (0U)
 *   PassiveDriver_ProcessingStatus_Ongoing (1U)
 *   PassiveDriver_ProcessingStatus_LfSent (2U)
 *   PassiveDriver_ProcessingStatus_Detected (3U)
 *   PassiveDriver_ProcessingStatus_NotDetected (4U)
 *   PassiveDriver_ProcessingStatus_HwError (5U)
 * SCIM_PassiveSearchCoverage_T: Enumeration of integer in interval [0...255] with enumerators
 *   PassiveSearchCoverage_PassiveStart2Ant (0U)
 *   PassiveSearchCoverage_PassiveStart1Ant (1U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data256ByteType: Array with 256 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data3ByteType: Array with 3 element(s) of type uint8
 * Dcm_Data4ByteType: Array with 4 element(s) of type uint8
 * SEWS_KeyfobEncryptCode_P1DS4_a_T: Array with 24 element(s) of type SEWS_KeyfobEncryptCode_P1DS4_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_KeyMatchingIndicationTimeout_X1CV3_T Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v(void)
 *   boolean Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v(void)
 *   boolean Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v(void)
 *   boolean Rte_Prm_P1B2U_KeyfobPresent_v(void)
 *   SEWS_KeyfobEncryptCode_P1DS4_T *Rte_Prm_P1DS4_KeyfobEncryptCode_v(void)
 *     Returnvalue: SEWS_KeyfobEncryptCode_P1DS4_T* is of type SEWS_KeyfobEncryptCode_P1DS4_a_T
 *   SEWS_CrankingLockActivation_P1DS3_T Rte_Prm_P1DS3_CrankingLockActivation_v(void)
 *   boolean Rte_Prm_P1C54_FactoryModeActive_v(void)
 *
 *********************************************************************************************************************/


#define Keyfob_Mgr_START_SEC_CODE
#include "Keyfob_Mgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount(void)
 *   uint8 Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1B0T_Data_P1B0T_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  uint8 DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount;
  uint8 DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount = TSC_Keyfob_Mgr_Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount();
  DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus = TSC_Keyfob_Mgr_Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus();

  Keyfob_Mgr_TestDefines();

  return RTE_E_DataServices_P1B0T_Data_P1B0T_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DS3_Data_P1DS3_CrankingLockActivation>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DS3_Data_P1DS3_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  return RTE_E_DataServices_P1DS3_Data_P1DS3_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1FDL_Data_P1FDL_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  return RTE_E_DataServices_P1FDL_Data_P1FDL_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1FM6_Data_P1FM6_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  return RTE_E_DataServices_P1FM6_Data_P1FM6_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data256ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  return RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <WriteData> of PortPrototype <DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data256ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  return RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VKK_Data_P1VKK_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  return RTE_E_DataServices_P1VKK_Data_P1VKK_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Keyfob_Mgr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching(DriverAuthDeviceMatching_T *data)
 *   Std_ReturnType Rte_Read_KeyfobAuth_rqst_KeyfobAuth_rqst(KeyfobAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst(KeyfobInCabPresencePS_T *data)
 *   Std_ReturnType Rte_Read_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AddrParP1DS4_stat_dataP1DS4(const SEWS_KeyfobEncryptCode_P1DS4_T *data)
 *     Argument data: SEWS_KeyfobEncryptCode_P1DS4_T* is of type SEWS_KeyfobEncryptCode_P1DS4_a_T
 *   Std_ReturnType Rte_Write_KeyfobAuth_stat_KeyfobAuth_stat(KeyfobAuth_stat_T data)
 *   Std_ReturnType Rte_Write_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T data)
 *   Std_ReturnType Rte_Write_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(KeyfobOutsideLocation_stat_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(void)
 *   uint8 Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(void)
 *   uint8 Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(uint8 data)
 *   void Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(uint8 data)
 *   void Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(uint8 data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(SCIM_ImmoDriver_ProcessingStatus_T *ImmoProcessingStatus)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(SCIM_ImmoType_T ImmoType, Boolean AuthRequestedbyImmo)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(uint8 *matchingStatus, uint8 *matchedKeyfobCount)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList(void)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(SCIM_PassiveDriver_ProcessingStatus_T *ProcessingStatus, KeyfobInCabLocation_stat_T *KeyfobLocationbyPassive_Incab, KeyfobOutsideLocation_stat_T *KeyfobLocationbyPassive_Outcab)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(SCIM_PassiveSearchCoverage_T PassiveSearchCoverage, boolean SearchRequestedbyPassive)
 *     Synchronous Server Invocation. Timeout: None
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Keyfob_Mgr_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Keyfob_Mgr_CODE) Keyfob_Mgr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Keyfob_Mgr_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  DiagActiveState_T Read_DiagActiveState_isDiagActive;
  DriverAuthDeviceMatching_T Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching;
  KeyfobAuth_rqst_T Read_KeyfobAuth_rqst_KeyfobAuth_rqst;
  KeyfobInCabPresencePS_T Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst;
  KeyfobLocation_rqst_T Read_KeyfobLocation_rqst_KeyfobLocation_rqst;
  VehicleModeDistribution_T Read_SwcActivation_Security_SwcActivation_Security;

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  SEWS_KeyfobEncryptCode_P1DS4_a_T Write_AddrParP1DS4_stat_dataP1DS4;

  uint8 Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF;
  uint8 Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount;
  uint8 Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus;

  SCIM_ImmoDriver_ProcessingStatus_T Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult_ImmoProcessingStatus = 0U;
  uint8 Call_KeyfobMatchingOperations_GetMatchingStatus_matchingStatus = 0U;
  uint8 Call_KeyfobMatchingOperations_GetMatchingStatus_matchedKeyfobCount = 0U;
  SCIM_PassiveDriver_ProcessingStatus_T Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult_ProcessingStatus = 0U;
  KeyfobInCabLocation_stat_T Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult_KeyfobLocationbyPassive_Incab = 0U;
  KeyfobOutsideLocation_stat_T Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult_KeyfobLocationbyPassive_Outcab = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  fct_status = TSC_Keyfob_Mgr_Rte_Read_DiagActiveState_isDiagActive(&Read_DiagActiveState_isDiagActive);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching(&Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Read_KeyfobAuth_rqst_KeyfobAuth_rqst(&Read_KeyfobAuth_rqst_KeyfobAuth_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst(&Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Read_KeyfobLocation_rqst_KeyfobLocation_rqst(&Read_KeyfobLocation_rqst_KeyfobLocation_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Read_SwcActivation_Security_SwcActivation_Security(&Read_SwcActivation_Security_SwcActivation_Security);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  (void)memset(&Write_AddrParP1DS4_stat_dataP1DS4, 0, sizeof(Write_AddrParP1DS4_stat_dataP1DS4));
  fct_status = TSC_Keyfob_Mgr_Rte_Write_AddrParP1DS4_stat_dataP1DS4(Write_AddrParP1DS4_stat_dataP1DS4);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Write_KeyfobAuth_stat_KeyfobAuth_stat(Rte_InitValue_KeyfobAuth_stat_KeyfobAuth_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Write_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(Rte_InitValue_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Write_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(Rte_InitValue_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF = TSC_Keyfob_Mgr_Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF();
  Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount = TSC_Keyfob_Mgr_Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount();
  Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus = TSC_Keyfob_Mgr_Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus();

  TSC_Keyfob_Mgr_Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(0U);
  TSC_Keyfob_Mgr_Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(0U);
  TSC_Keyfob_Mgr_Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(0U);

  fct_status = TSC_Keyfob_Mgr_Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(&Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult_ImmoProcessingStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(&Call_KeyfobMatchingOperations_GetMatchingStatus_matchingStatus, &Call_KeyfobMatchingOperations_GetMatchingStatus_matchedKeyfobCount);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(&Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult_ProcessingStatus, &Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult_KeyfobLocationbyPassive_Incab, &Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult_KeyfobLocationbyPassive_Outcab);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Keyfob_Mgr_Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(0U, FALSE);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_R1AAA_KeyfobMatchingRoutineByLF>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAA_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  uint8 RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF = TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF();

  TSC_Keyfob_Mgr_Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(0U);

  return RTE_E_RoutineServices_R1AAA_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_R1AAA_KeyfobMatchingRoutineByLF>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAA_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  uint8 RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF = TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF();

  TSC_Keyfob_Mgr_Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(0U);

  return RTE_E_RoutineServices_R1AAA_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_R1AAA_KeyfobMatchingRoutineByLF>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAA_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAA_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop (returns application error)
 *********************************************************************************************************************/

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  uint8 RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF = TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF();

  TSC_Keyfob_Mgr_Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(0U);

  return RTE_E_RoutineServices_R1AAA_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_Y1ABD_ClearECU_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_Y1ABD_ClearECU>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF(void)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList(void)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_Y1ABD_ClearECU_Start(const uint8 *In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument In_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data4ByteType
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data3ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_Y1ABD_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_Y1ABD_DCM_E_PENDING
 *   RTE_E_RoutineServices_Y1ABD_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Y1ABD_ClearECU_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_Y1ABD_ClearECU_Start(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_Y1ABD_ClearECU_Start (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_KeyMatchingIndicationTimeout_X1CV3_T X1CV3_KeyMatchingIndicationTimeout_v_data;
  boolean X1CV4_KeyMatchingReinforcedAuth_v_data;
  boolean X1CXE_isKeyfobSecurityFuseBlowAct_v_data;

  boolean P1B2U_KeyfobPresent_v_data;
  SEWS_KeyfobEncryptCode_P1DS4_a_T P1DS4_KeyfobEncryptCode_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  boolean P1C54_FactoryModeActive_v_data;

  uint8 RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CV3_KeyMatchingIndicationTimeout_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v();
  X1CV4_KeyMatchingReinforcedAuth_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v();
  X1CXE_isKeyfobSecurityFuseBlowAct_v_data = TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v();

  P1B2U_KeyfobPresent_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v();
  (void)memcpy(P1DS4_KeyfobEncryptCode_v_data, TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(), sizeof(SEWS_KeyfobEncryptCode_P1DS4_a_T));

  P1DS3_CrankingLockActivation_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v();

  P1C54_FactoryModeActive_v_data = TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v();

  RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF = TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF();

  fct_status = TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_Y1ABD_DCM_E_FORCE_RCRRP:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_Y1ABD_DCM_E_PENDING:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_Y1ABD_E_NOT_OK:
      fct_error = 1;
      break;
  }

  return RTE_E_RoutineServices_Y1ABD_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Keyfob_Mgr_STOP_SEC_CODE
#include "Keyfob_Mgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void Keyfob_Mgr_TestDefines(void)
{
  /* Enumeration Data Types */

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  DiagActiveState_T Test_DiagActiveState_T_V_1 = Diag_Active_FALSE;
  DiagActiveState_T Test_DiagActiveState_T_V_2 = Diag_Active_TRUE;

  DriverAuthDeviceMatching_T Test_DriverAuthDeviceMatching_T_V_1 = DriverAuthDeviceMatching_Idle;
  DriverAuthDeviceMatching_T Test_DriverAuthDeviceMatching_T_V_2 = DriverAuthDeviceMatching_DeviceToMatchIsPresent;
  DriverAuthDeviceMatching_T Test_DriverAuthDeviceMatching_T_V_3 = DriverAuthDeviceMatching_Error;
  DriverAuthDeviceMatching_T Test_DriverAuthDeviceMatching_T_V_4 = DriverAuthDeviceMatching_NotAvailable;

  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_1 = KeyfobAuth_rqst_Idle;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_2 = KeyfobAuth_rqst_RequestByPassiveMechanism;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_3 = KeyfobAuth_rqst_RequestByImmobilizerMechanism;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_4 = KeyfobAuth_rqst_Spare1;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_5 = KeyfobAuth_rqst_Spare2;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_6 = KeyfobAuth_rqst_Spare3;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_7 = KeyfobAuth_rqst_Error;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_8 = KeyfobAuth_rqst_NotAavailable;

  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_1 = KeyfobAuth_stat_Idle;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_2 = KeyfobAuth_stat_NokeyfobAuthenticated;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_3 = KeyfobAuth_stat_KeyfobAuthenticated;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_4 = KeyfobAuth_stat_Spare1;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_5 = KeyfobAuth_stat_Spare2;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_6 = KeyfobAuth_stat_Spare3;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_7 = KeyfobAuth_stat_Error;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_8 = KeyfobAuth_stat_NotAvailable;

  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_1 = KeyfobInCabLocation_stat_Idle;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_2 = KeyfobInCabLocation_stat_NotDetected_Incab;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_3 = KeyfobInCabLocation_stat_Detected_Incab;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_4 = KeyfobInCabLocation_stat_Spare1;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_5 = KeyfobInCabLocation_stat_Spare2;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_6 = KeyfobInCabLocation_stat_Spare3;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_7 = KeyfobInCabLocation_stat_Error;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_8 = KeyfobInCabLocation_stat_NotAvailable;

  KeyfobInCabPresencePS_T Test_KeyfobInCabPresencePS_T_V_1 = KeyfobInCabPresencePS_rqst_NoRequest;
  KeyfobInCabPresencePS_T Test_KeyfobInCabPresencePS_T_V_2 = KeyfobInCabPresencePS_rqst_RequestDetectionInCab;
  KeyfobInCabPresencePS_T Test_KeyfobInCabPresencePS_T_V_3 = KeyfobInCabPresencePS_rqst_Error;
  KeyfobInCabPresencePS_T Test_KeyfobInCabPresencePS_T_V_4 = KeyfobInCabPresencePS_rqst_NotAvailable;

  KeyfobLocation_rqst_T Test_KeyfobLocation_rqst_T_V_1 = KeyfobLocation_rqst_NoRequest;
  KeyfobLocation_rqst_T Test_KeyfobLocation_rqst_T_V_2 = KeyfobLocation_rqst_Request;
  KeyfobLocation_rqst_T Test_KeyfobLocation_rqst_T_V_3 = KeyfobLocation_rqst_Error;
  KeyfobLocation_rqst_T Test_KeyfobLocation_rqst_T_V_4 = KeyfobLocation_rqst_NotAvailable;

  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_1 = KeyfobOutsideLocation_stat_Idle;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_2 = KeyfobOutsideLocation_stat_NotDetectedOutside;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_3 = KeyfobOutsideLocation_stat_DetectedOnRightSide;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_4 = KeyfobOutsideLocation_stat_DetectedOnLeftSide;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_5 = KeyfobOutsideLocation_stat_DetectedOnBothSides;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_6 = KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_7 = KeyfobOutsideLocation_stat_Error;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_8 = KeyfobOutsideLocation_stat_NotAvailable;

  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_1 = ImmoDriver_ProcessingStatus_Idle;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_2 = ImmoDriver_ProcessingStatus_Ongoing;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_3 = ImmoDriver_ProcessingStatus_LfSent;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_4 = ImmoDriver_ProcessingStatus_DecryptedInList;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_5 = ImmoDriver_ProcessingStatus_DecryptedNotInList;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_6 = ImmoDriver_ProcessingStatus_NotDecrypted;
  SCIM_ImmoDriver_ProcessingStatus_T Test_SCIM_ImmoDriver_ProcessingStatus_T_V_7 = ImmoDriver_ProcessingStatus_HwError;

  SCIM_ImmoType_T Test_SCIM_ImmoType_T_V_1 = ImmoType_ATA5702;
  SCIM_ImmoType_T Test_SCIM_ImmoType_T_V_2 = ImmoType_ATA5577;

  SCIM_PassiveDriver_ProcessingStatus_T Test_SCIM_PassiveDriver_ProcessingStatus_T_V_1 = PassiveDriver_ProcessingStatus_Idle;
  SCIM_PassiveDriver_ProcessingStatus_T Test_SCIM_PassiveDriver_ProcessingStatus_T_V_2 = PassiveDriver_ProcessingStatus_Ongoing;
  SCIM_PassiveDriver_ProcessingStatus_T Test_SCIM_PassiveDriver_ProcessingStatus_T_V_3 = PassiveDriver_ProcessingStatus_LfSent;
  SCIM_PassiveDriver_ProcessingStatus_T Test_SCIM_PassiveDriver_ProcessingStatus_T_V_4 = PassiveDriver_ProcessingStatus_Detected;
  SCIM_PassiveDriver_ProcessingStatus_T Test_SCIM_PassiveDriver_ProcessingStatus_T_V_5 = PassiveDriver_ProcessingStatus_NotDetected;
  SCIM_PassiveDriver_ProcessingStatus_T Test_SCIM_PassiveDriver_ProcessingStatus_T_V_6 = PassiveDriver_ProcessingStatus_HwError;

  SCIM_PassiveSearchCoverage_T Test_SCIM_PassiveSearchCoverage_T_V_1 = PassiveSearchCoverage_PassiveStart2Ant;
  SCIM_PassiveSearchCoverage_T Test_SCIM_PassiveSearchCoverage_T_V_2 = PassiveSearchCoverage_PassiveStart1Ant;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
