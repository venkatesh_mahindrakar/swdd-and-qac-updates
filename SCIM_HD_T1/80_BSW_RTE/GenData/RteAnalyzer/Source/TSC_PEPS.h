/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_PEPS.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Client server interfaces */
Std_ReturnType TSC_PEPS_Rte_Call_LFSearchCompleteFlag_Get_LFSearchCompleteFlag(uint8 *LFSearchCompleteFlag_PEPS);
Std_ReturnType TSC_PEPS_Rte_Call_LFSearchCompleteFlag_Set_LFSearchCompleteFlag(uint8 LFSearchCompleteFlag_PEPS);
Std_ReturnType TSC_PEPS_Rte_Call_LfICInit_CS(uint8 Gain_vehicleOption);
Std_ReturnType TSC_PEPS_Rte_Call_RficDioInterface_P_Read(IOHWAB_UINT8 *ReadValue);
Std_ReturnType TSC_PEPS_Rte_Call_RficIRQActiveCheck_CS(uint8 kb_TerminalControlState);
Std_ReturnType TSC_PEPS_Rte_Call_RficInit_CS(void);
Std_ReturnType TSC_PEPS_Rte_Call_SearchSysMode_CS(uint8 RficSystemMode, uint8 RFIC_ReqCmd);
Std_ReturnType TSC_PEPS_Rte_Call_SetupLfTelegram_CS(uint8 b_AntennaIndex, uint8 b_SearchPattern, uint16 Counter_LFRawDataBit, const uint8 *Buffer_LFRawData);
Std_ReturnType TSC_PEPS_Rte_Call_TimeoutTxTelegram_CS(void);

/** Calibration Component Calibration Parameters */
SEWS_AntMappingConfig_Gain_X1C03_a_T * TSC_PEPS_Rte_Prm_X1C03_AntMappingConfig_Gain_v(void);
SEWS_AntMappingConfig_Multi_X1CY3_a_T * TSC_PEPS_Rte_Prm_X1CY3_AntMappingConfig_Multi_v(void);
SEWS_AntMappingConfig_Single_X1CY5_s_T * TSC_PEPS_Rte_Prm_X1CY5_AntMappingConfig_Single_v(void);
SEWS_KeyfobDetectionMappingSelection_P1WIP_T  TSC_PEPS_Rte_Prm_P1WIP_KeyfobDetectionMappingSelection_v(void);
SEWS_KeyfobDetectionMappingConfig_P1WIR_T  TSC_PEPS_Rte_Prm_P1WIR_KeyfobDetectionMappingConfig_v(void);
SEWS_AuxPassiveAntennasActivation_P1WIQ_s_T * TSC_PEPS_Rte_Prm_P1WIQ_AuxPassiveAntennasActivation_v(void);




