/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SideReverseLight_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  SideReverseLight_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <SideReverseLight_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_SideReverseLight_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_SideReverseLight_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void SideReverseLight_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Enumeration Types:
 * ==================
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * BBNetworkExtraSideWrknLights_T: Enumeration of integer in interval [0...7] with enumerators
 *   BBNetworkExtraSideWrknLights_KeepCurrentLightStatus (0U)
 *   BBNetworkExtraSideWrknLights_TurnOnLightWorkingMode (1U)
 *   BBNetworkExtraSideWrknLights_TurnOffLightWorkingMode (2U)
 *   BBNetworkExtraSideWrknLights_Reserved (3U)
 *   BBNetworkExtraSideWrknLights_Reserved_01 (4U)
 *   BBNetworkExtraSideWrknLights_Reserved_02 (5U)
 *   BBNetworkExtraSideWrknLights_Error (6U)
 *   BBNetworkExtraSideWrknLights_NotAvailable (7U)
 * CabExtraSideWorkingLight_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   CabExtraSideWorkingLight_rqst_ExtraSideWorkingLightNotRqstdCab (0U)
 *   CabExtraSideWorkingLight_rqst_WorkingModeRequestedCab (1U)
 *   CabExtraSideWorkingLight_rqst_ReverseModeRequestedCab (2U)
 *   CabExtraSideWorkingLight_rqst_SpareValue (3U)
 *   CabExtraSideWorkingLight_rqst_SpareValue_01 (4U)
 *   CabExtraSideWorkingLight_rqst_SpareValue_02 (5U)
 *   CabExtraSideWorkingLight_rqst_Error (6U)
 *   CabExtraSideWorkingLight_rqst_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DualDeviceIndication_T: Enumeration of integer in interval [0...15] with enumerators
 *   DualDeviceIndication_UpperOffLowerOff (0U)
 *   DualDeviceIndication_UpperOnLowerOff (1U)
 *   DualDeviceIndication_UpperBlinkLowerOff (2U)
 *   DualDeviceIndication_UpperDontCareLowerOff (3U)
 *   DualDeviceIndication_UpperOffLowerOn (4U)
 *   DualDeviceIndication_UpperOnLowerOn (5U)
 *   DualDeviceIndication_UpperBlinkLowerOn (6U)
 *   DualDeviceIndication_UpperDontCareLowerOn (7U)
 *   DualDeviceIndication_UpperOffLowerBlink (8U)
 *   DualDeviceIndication_UpperOnLowerBlink (9U)
 *   DualDeviceIndication_UpperBlinkLowerBlink (10U)
 *   DualDeviceIndication_UpperDontCareLowerBlink (11U)
 *   DualDeviceIndication_UpperOffLowerDontCare (12U)
 *   DualDeviceIndication_UpperOnLowerDontCare (13U)
 *   DualDeviceIndication_UpperBlinkLowerDontCare (14U)
 *   DualDeviceIndication_UpperDontCareLowerDontCare (15U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void SideReverseLight_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_1 = A3PosSwitchStatus_Middle;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_2 = A3PosSwitchStatus_Lower;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_3 = A3PosSwitchStatus_Upper;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_4 = A3PosSwitchStatus_Spare;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_5 = A3PosSwitchStatus_Spare_01;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_6 = A3PosSwitchStatus_Spare_02;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_7 = A3PosSwitchStatus_Error;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_8 = A3PosSwitchStatus_NotAvailable;

  BBNetworkExtraSideWrknLights_T Test_BBNetworkExtraSideWrknLights_T_V_1 = BBNetworkExtraSideWrknLights_KeepCurrentLightStatus;
  BBNetworkExtraSideWrknLights_T Test_BBNetworkExtraSideWrknLights_T_V_2 = BBNetworkExtraSideWrknLights_TurnOnLightWorkingMode;
  BBNetworkExtraSideWrknLights_T Test_BBNetworkExtraSideWrknLights_T_V_3 = BBNetworkExtraSideWrknLights_TurnOffLightWorkingMode;
  BBNetworkExtraSideWrknLights_T Test_BBNetworkExtraSideWrknLights_T_V_4 = BBNetworkExtraSideWrknLights_Reserved;
  BBNetworkExtraSideWrknLights_T Test_BBNetworkExtraSideWrknLights_T_V_5 = BBNetworkExtraSideWrknLights_Reserved_01;
  BBNetworkExtraSideWrknLights_T Test_BBNetworkExtraSideWrknLights_T_V_6 = BBNetworkExtraSideWrknLights_Reserved_02;
  BBNetworkExtraSideWrknLights_T Test_BBNetworkExtraSideWrknLights_T_V_7 = BBNetworkExtraSideWrknLights_Error;
  BBNetworkExtraSideWrknLights_T Test_BBNetworkExtraSideWrknLights_T_V_8 = BBNetworkExtraSideWrknLights_NotAvailable;

  CabExtraSideWorkingLight_rqst_T Test_CabExtraSideWorkingLight_rqst_T_V_1 = CabExtraSideWorkingLight_rqst_ExtraSideWorkingLightNotRqstdCab;
  CabExtraSideWorkingLight_rqst_T Test_CabExtraSideWorkingLight_rqst_T_V_2 = CabExtraSideWorkingLight_rqst_WorkingModeRequestedCab;
  CabExtraSideWorkingLight_rqst_T Test_CabExtraSideWorkingLight_rqst_T_V_3 = CabExtraSideWorkingLight_rqst_ReverseModeRequestedCab;
  CabExtraSideWorkingLight_rqst_T Test_CabExtraSideWorkingLight_rqst_T_V_4 = CabExtraSideWorkingLight_rqst_SpareValue;
  CabExtraSideWorkingLight_rqst_T Test_CabExtraSideWorkingLight_rqst_T_V_5 = CabExtraSideWorkingLight_rqst_SpareValue_01;
  CabExtraSideWorkingLight_rqst_T Test_CabExtraSideWorkingLight_rqst_T_V_6 = CabExtraSideWorkingLight_rqst_SpareValue_02;
  CabExtraSideWorkingLight_rqst_T Test_CabExtraSideWorkingLight_rqst_T_V_7 = CabExtraSideWorkingLight_rqst_Error;
  CabExtraSideWorkingLight_rqst_T Test_CabExtraSideWorkingLight_rqst_T_V_8 = CabExtraSideWorkingLight_rqst_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DualDeviceIndication_T Test_DualDeviceIndication_T_V_1 = DualDeviceIndication_UpperOffLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_2 = DualDeviceIndication_UpperOnLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_3 = DualDeviceIndication_UpperBlinkLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_4 = DualDeviceIndication_UpperDontCareLowerOff;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_5 = DualDeviceIndication_UpperOffLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_6 = DualDeviceIndication_UpperOnLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_7 = DualDeviceIndication_UpperBlinkLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_8 = DualDeviceIndication_UpperDontCareLowerOn;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_9 = DualDeviceIndication_UpperOffLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_10 = DualDeviceIndication_UpperOnLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_11 = DualDeviceIndication_UpperBlinkLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_12 = DualDeviceIndication_UpperDontCareLowerBlink;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_13 = DualDeviceIndication_UpperOffLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_14 = DualDeviceIndication_UpperOnLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_15 = DualDeviceIndication_UpperBlinkLowerDontCare;
  DualDeviceIndication_T Test_DualDeviceIndication_T_V_16 = DualDeviceIndication_UpperDontCareLowerDontCare;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
