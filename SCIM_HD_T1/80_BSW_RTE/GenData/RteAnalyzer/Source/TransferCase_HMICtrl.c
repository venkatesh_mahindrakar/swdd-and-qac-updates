/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TransferCase_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  TransferCase_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <TransferCase_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_TransferCase_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_TransferCase_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void TransferCase_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Enumeration Types:
 * ==================
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * NotEngagedEngaged_T: Enumeration of integer in interval [0...3] with enumerators
 *   NotEngagedEngaged_NotEngaged (0U)
 *   NotEngagedEngaged_Engaged (1U)
 *   NotEngagedEngaged_Error (2U)
 *   NotEngagedEngaged_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * TransferCaseNeutral_Req2_T: Enumeration of integer in interval [0...7] with enumerators
 *   TransferCaseNeutral_Req2_NoRequest (0U)
 *   TransferCaseNeutral_Req2_PutTransferCaseInDrive (1U)
 *   TransferCaseNeutral_Req2_PutTransferCaseInNeutral (2U)
 *   TransferCaseNeutral_Req2_Spare1 (3U)
 *   TransferCaseNeutral_Req2_Spare2 (4U)
 *   TransferCaseNeutral_Req2_Spare3 (5U)
 *   TransferCaseNeutral_Req2_Error (6U)
 *   TransferCaseNeutral_Req2_NotAvailable (7U)
 * TransferCaseNeutral_T: Enumeration of integer in interval [0...3] with enumerators
 *   TransferCaseNeutral_NoAction (0U)
 *   TransferCaseNeutral_RequestConfirmed (1U)
 *   TransferCaseNeutral_Error (2U)
 *   TransferCaseNeutral_NotAvailable (3U)
 * TransferCaseNeutral_status_T: Enumeration of integer in interval [0...3] with enumerators
 *   TransferCaseNeutral_status_TransferCaseInDrive (0U)
 *   TransferCaseNeutral_status_TransferCaseInNeutral (1U)
 *   TransferCaseNeutral_status_Error (2U)
 *   TransferCaseNeutral_status_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


#define TransferCase_HMICtrl_START_SEC_CODE
#include "TransferCase_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: TransferCase_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack(TransferCaseNeutral_T *data)
 *   Std_ReturnType Rte_Read_TransferCaseNeutral_SwitchStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_TransferCaseNeutral_status_TransferCaseNeutral_status(TransferCaseNeutral_status_T *data)
 *   Std_ReturnType Rte_Read_TransferCasePTOEngaged_TransferCasePTOEngaged(NotEngagedEngaged_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_TransferCaseNeutral_DevInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_TransferCaseNeutral_Req_TransferCaseNeutral_Req(TransferCaseNeutral_Req2_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: TransferCase_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, TransferCase_HMICtrl_CODE) TransferCase_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: TransferCase_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;
  TransferCaseNeutral_T Read_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack;
  PushButtonStatus_T Read_TransferCaseNeutral_SwitchStat_PushButtonStatus;
  TransferCaseNeutral_status_T Read_TransferCaseNeutral_status_TransferCaseNeutral_status;
  NotEngagedEngaged_T Read_TransferCasePTOEngaged_TransferCasePTOEngaged;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_TransferCase_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TransferCase_HMICtrl_Rte_Read_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack(&Read_TransferCaseNeutral_Ack_TransferCaseNeutral_Ack);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TransferCase_HMICtrl_Rte_Read_TransferCaseNeutral_SwitchStat_PushButtonStatus(&Read_TransferCaseNeutral_SwitchStat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TransferCase_HMICtrl_Rte_Read_TransferCaseNeutral_status_TransferCaseNeutral_status(&Read_TransferCaseNeutral_status_TransferCaseNeutral_status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TransferCase_HMICtrl_Rte_Read_TransferCasePTOEngaged_TransferCasePTOEngaged(&Read_TransferCasePTOEngaged_TransferCasePTOEngaged);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_TransferCase_HMICtrl_Rte_Write_TransferCaseNeutral_DevInd_DeviceIndication(Rte_InitValue_TransferCaseNeutral_DevInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_TransferCase_HMICtrl_Rte_Write_TransferCaseNeutral_Req_TransferCaseNeutral_Req(Rte_InitValue_TransferCaseNeutral_Req_TransferCaseNeutral_Req);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  TransferCase_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define TransferCase_HMICtrl_STOP_SEC_CODE
#include "TransferCase_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void TransferCase_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  NotEngagedEngaged_T Test_NotEngagedEngaged_T_V_1 = NotEngagedEngaged_NotEngaged;
  NotEngagedEngaged_T Test_NotEngagedEngaged_T_V_2 = NotEngagedEngaged_Engaged;
  NotEngagedEngaged_T Test_NotEngagedEngaged_T_V_3 = NotEngagedEngaged_Error;
  NotEngagedEngaged_T Test_NotEngagedEngaged_T_V_4 = NotEngagedEngaged_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  TransferCaseNeutral_Req2_T Test_TransferCaseNeutral_Req2_T_V_1 = TransferCaseNeutral_Req2_NoRequest;
  TransferCaseNeutral_Req2_T Test_TransferCaseNeutral_Req2_T_V_2 = TransferCaseNeutral_Req2_PutTransferCaseInDrive;
  TransferCaseNeutral_Req2_T Test_TransferCaseNeutral_Req2_T_V_3 = TransferCaseNeutral_Req2_PutTransferCaseInNeutral;
  TransferCaseNeutral_Req2_T Test_TransferCaseNeutral_Req2_T_V_4 = TransferCaseNeutral_Req2_Spare1;
  TransferCaseNeutral_Req2_T Test_TransferCaseNeutral_Req2_T_V_5 = TransferCaseNeutral_Req2_Spare2;
  TransferCaseNeutral_Req2_T Test_TransferCaseNeutral_Req2_T_V_6 = TransferCaseNeutral_Req2_Spare3;
  TransferCaseNeutral_Req2_T Test_TransferCaseNeutral_Req2_T_V_7 = TransferCaseNeutral_Req2_Error;
  TransferCaseNeutral_Req2_T Test_TransferCaseNeutral_Req2_T_V_8 = TransferCaseNeutral_Req2_NotAvailable;

  TransferCaseNeutral_T Test_TransferCaseNeutral_T_V_1 = TransferCaseNeutral_NoAction;
  TransferCaseNeutral_T Test_TransferCaseNeutral_T_V_2 = TransferCaseNeutral_RequestConfirmed;
  TransferCaseNeutral_T Test_TransferCaseNeutral_T_V_3 = TransferCaseNeutral_Error;
  TransferCaseNeutral_T Test_TransferCaseNeutral_T_V_4 = TransferCaseNeutral_NotAvailable;

  TransferCaseNeutral_status_T Test_TransferCaseNeutral_status_T_V_1 = TransferCaseNeutral_status_TransferCaseInDrive;
  TransferCaseNeutral_status_T Test_TransferCaseNeutral_status_T_V_2 = TransferCaseNeutral_status_TransferCaseInNeutral;
  TransferCaseNeutral_status_T Test_TransferCaseNeutral_status_T_V_3 = TransferCaseNeutral_status_Error;
  TransferCaseNeutral_status_T Test_TransferCaseNeutral_status_T_V_4 = TransferCaseNeutral_status_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
