/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_PassengersSeatBelt_hdlr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Write_PassengersSeatBelt_PassengersSeatBelt(PassengersSeatBelt_T data);

/** Client server interfaces */
Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus);

/** Service interfaces */
Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Call_Event_D1F0O_1E_ResistOutOfRange_SecondPsgrSeat_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_PassengersSeatBelt_hdlr_Rte_Call_Event_D1FZ9_1E_ResistOutOfRange_FirstPsgrSeat_SetEventStatus(Dem_EventStatusType EventStatus);

/** Calibration Component Calibration Parameters */
SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_PassengersSeatBelt_hdlr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void);
SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T * TSC_PassengersSeatBelt_hdlr_Rte_Prm_X1CY2_PassengersSeatBeltVoltageLevels_v(void);
SEWS_PassengersSeatBeltInstalled_P1VQB_T  TSC_PassengersSeatBelt_hdlr_Rte_Prm_P1VQB_PassengersSeatBeltInstalled_v(void);
SEWS_PassengersSeatBeltSensorType_P1VYK_T  TSC_PassengersSeatBelt_hdlr_Rte_Prm_P1VYK_PassengersSeatBeltSensorType_v(void);




