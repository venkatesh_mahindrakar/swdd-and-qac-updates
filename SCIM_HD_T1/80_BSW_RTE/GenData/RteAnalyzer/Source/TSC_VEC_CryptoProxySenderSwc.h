/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_VEC_CryptoProxySenderSwc.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_CryptoTrigger_CryptoTrigger(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, Boolean *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt8 *data);

/** Sender receiver - Queued - Explicit read */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);

/** Sender receiver - Queued - Explicit send */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Send_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, const uint8 *data);

/** Service interfaces */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);

/** Tx acknowledgment of S/R - Rte_Feedback */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Explicit inter-runnable variables */
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);

/** Calibration Component Calibration Parameters */
SEWS_ComCryptoKey_P1DLX_a_T * TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** SW-C local Calibration Parameters */
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Per Instance Memories */
UInt16 *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);



/** Sender receiver - explicit read services */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_CryptoTrigger_CryptoTrigger(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, Boolean *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt8 *data);

/** Sender receiver - Queued - Explicit read */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);

/** Sender receiver - Queued - Explicit send */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Send_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, const uint8 *data);

/** Service interfaces */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);

/** Tx acknowledgment of S/R - Rte_Feedback */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Explicit inter-runnable variables */
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);

/** Calibration Component Calibration Parameters */
SEWS_ComCryptoKey_P1DLX_a_T * TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** SW-C local Calibration Parameters */
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Per Instance Memories */
UInt16 *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);



/** Sender receiver - explicit read services */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_CryptoTrigger_CryptoTrigger(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, Boolean *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt8 *data);

/** Sender receiver - Queued - Explicit read */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);

/** Sender receiver - Queued - Explicit send */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Send_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, const uint8 *data);

/** Service interfaces */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);

/** Tx acknowledgment of S/R - Rte_Feedback */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Explicit inter-runnable variables */
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);

/** Calibration Component Calibration Parameters */
SEWS_ComCryptoKey_P1DLX_a_T * TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** SW-C local Calibration Parameters */
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Per Instance Memories */
UInt16 *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);



/** Sender receiver - explicit read services */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_CryptoTrigger_CryptoTrigger(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, Boolean *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt8 *data);

/** Sender receiver - Queued - Explicit read */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);

/** Sender receiver - Queued - Explicit send */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Send_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, const uint8 *data);

/** Service interfaces */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);

/** Tx acknowledgment of S/R - Rte_Feedback */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Explicit inter-runnable variables */
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);

/** Calibration Component Calibration Parameters */
SEWS_ComCryptoKey_P1DLX_a_T * TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** SW-C local Calibration Parameters */
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Per Instance Memories */
UInt16 *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);



/** Sender receiver - explicit read services */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_CryptoTrigger_CryptoTrigger(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, Boolean *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt8 *data);

/** Sender receiver - Queued - Explicit read */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);

/** Sender receiver - Queued - Explicit send */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Send_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, const uint8 *data);

/** Service interfaces */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);

/** Tx acknowledgment of S/R - Rte_Feedback */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Explicit inter-runnable variables */
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);

/** Calibration Component Calibration Parameters */
SEWS_ComCryptoKey_P1DLX_a_T * TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** SW-C local Calibration Parameters */
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Per Instance Memories */
UInt16 *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);



/** Sender receiver - explicit read services */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_CryptoTrigger_CryptoTrigger(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, Boolean *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt8 *data);

/** Sender receiver - Queued - Explicit read */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);

/** Sender receiver - Queued - Explicit send */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Send_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, const uint8 *data);

/** Service interfaces */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);

/** Tx acknowledgment of S/R - Rte_Feedback */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Explicit inter-runnable variables */
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);

/** Calibration Component Calibration Parameters */
SEWS_ComCryptoKey_P1DLX_a_T * TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** SW-C local Calibration Parameters */
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Per Instance Memories */
UInt16 *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);



/** Sender receiver - explicit read services */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_CryptoTrigger_CryptoTrigger(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, Boolean *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt8 *data);

/** Sender receiver - Queued - Explicit read */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);

/** Sender receiver - Queued - Explicit send */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Send_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, const uint8 *data);

/** Service interfaces */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);

/** Tx acknowledgment of S/R - Rte_Feedback */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Explicit inter-runnable variables */
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);

/** Calibration Component Calibration Parameters */
SEWS_ComCryptoKey_P1DLX_a_T * TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** SW-C local Calibration Parameters */
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Per Instance Memories */
UInt16 *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);



/** Sender receiver - explicit read services */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_CryptoTrigger_CryptoTrigger(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, Boolean *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Read_VEC_CryptoProxySerializedData_Crypto_Function_serialized(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt8 *data);

/** Sender receiver - Queued - Explicit read */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Receive_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, uint8 *data);

/** Sender receiver - Queued - Explicit send */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Send_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, const uint8 *data);

/** Service interfaces */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength);
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(const struct Rte_CDS_VEC_CryptoProxySenderSwc* self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength);

/** Tx acknowledgment of S/R - Rte_Feedback */
Std_ReturnType TSC_VEC_CryptoProxySenderSwc_Rte_Feedback_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Explicit inter-runnable variables */
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderConfirmation_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderMainFunction_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);
UInt32 TSC_VEC_CryptoProxySenderSwc_Rte_IrvRead_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
void TSC_VEC_CryptoProxySenderSwc_Rte_IrvWrite_VEC_CryptoProxySenderReception_VEC_CryptoProxyIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self, UInt32);

/** Calibration Component Calibration Parameters */
SEWS_ComCryptoKey_P1DLX_a_T * TSC_VEC_CryptoProxySenderSwc_Rte_Prm_ComCryptoKey_P1DLX_v(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** SW-C local Calibration Parameters */
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleFactor(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);
UInt16  TSC_VEC_CryptoProxySenderSwc_Rte_CData_VEC_CryptoProxyCycleOffset(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);

/** Per Instance Memories */
UInt16 *TSC_VEC_CryptoProxySenderSwc_Rte_Pim_VEC_CryptoProxy_CycleTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxySenderSwc, AUTOMATIC, RTE_CONST) self);



