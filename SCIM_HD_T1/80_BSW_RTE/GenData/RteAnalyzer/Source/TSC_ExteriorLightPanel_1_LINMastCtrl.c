/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ExteriorLightPanel_1_LINMastCtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_ExteriorLightPanel_1_LINMastCtrl.h"
#include "TSC_ExteriorLightPanel_1_LINMastCtrl.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvRead_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl(void)
{
return Rte_IrvRead_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl();
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP1LinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP1LinCtrl( data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */

void TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP1LinCtrl(uint8 data)
{
  Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP1LinCtrl( data);
}





Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN4_ComMode_LIN(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DaytimeRunningLight_Indication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_DaytimeRunningLight_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
{
  return Rte_Read_DiagActiveState_isDiagActive(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DiagInfoELCP1_DiagInfo(DiagInfo_T *data)
{
  return Rte_Read_DiagInfoELCP1_DiagInfo(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DrivingLightPlus_Indication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_DrivingLightPlus_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DrivingLight_Indication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_DrivingLight_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_FrontFog_Indication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_FrontFog_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_BackLightDimming_Status_Thumbwheel_stat(Thumbwheel_stat_T *data)
{
  return Rte_Read_LIN_BackLightDimming_Status_Thumbwheel_stat(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat(Thumbwheel_stat_T *data)
{
  return Rte_Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T *data)
{
  return Rte_Read_LIN_LightMode_Status_1_FreeWheel_Status(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_ParkingLight_Indication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_ParkingLight_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_RearFog_Indication_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_RearFog_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_ResponseErrorELCP1_ResponseErrorELCP1(ResponseErrorELCP1_T *data)
{
  return Rte_Read_ResponseErrorELCP1_ResponseErrorELCP1(data);
}


boolean TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status(void)
{
  return Rte_IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status();
}



Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_BackLightDimming_Status_Thumbwheel_stat(Thumbwheel_stat_T data)
{
  return Rte_Write_BackLightDimming_Status_Thumbwheel_stat(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_BlackPanelMode_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BlackPanelMode_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_FogLightFront_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_FogLightFront_ButtonStatus_1_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_FogLightRear_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_FogLightRear_ButtonStatus_1_PushButtonStatus(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DaytimeRunningLight_Indica_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_DaytimeRunningLight_Indica_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DrivingLightPlus_Indicatio_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_DrivingLightPlus_Indicatio_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DrivingLight_Indication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_DrivingLight_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_FrontFog_Indication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_FrontFog_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_ParkingLight_Indication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_ParkingLight_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_RearFog_Indication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_RearFog_Indication_DeviceIndication(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LevelingThumbwheel_stat_Thumbwheel_stat(Thumbwheel_stat_T data)
{
  return Rte_Write_LevelingThumbwheel_stat_Thumbwheel_stat(data);
}

Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T data)
{
  return Rte_Write_LightMode_Status_1_FreeWheel_Status(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_44_ELCP_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN9_44_ELCP_RAM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_DimmingAdjustment1_ActivateIss(void)
{
  return Rte_Call_UR_ANW_DimmingAdjustment1_ActivateIss();
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_DimmingAdjustment1_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_DimmingAdjustment1_DeactivateIss();
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest1_ActivateIss(void)
{
  return Rte_Call_UR_ANW_ExteriorLightsRequest1_ActivateIss();
}
Std_ReturnType TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest1_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_ExteriorLightsRequest1_DeactivateIss();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
uint8 TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvRead_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(void)
{
return Rte_IrvRead_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl();
}

void TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(uint8 data)
{
  Rte_IrvWrite_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl( data);
}




boolean  TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Prm_P1VR3_ELCP1_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1VR3_ELCP1_Installed_v();
}


     /* ExteriorLightPanel_1_LINMastCtrl */
      /* ExteriorLightPanel_1_LINMastCtrl */



