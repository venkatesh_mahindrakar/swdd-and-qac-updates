/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Os_Cfg.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Os definitions
 *********************************************************************************************************************/

#ifndef _OS_CFG_H_
# define _OS_CFG_H_

/* Os definitions */

/* Tasks */
# define ASW_10ms_Task (0U)
# define ASW_20ms_Task (1U)
# define ASW_Async_Task (2U)
# define ASW_Init_Task (3U)
# define BSW_10ms_Task (4U)
# define BSW_5ms_Task (5U)
# define BSW_Async_Task (6U)
# define BSW_Diag_Task (7U)
# define BSW_Lin_Task (8U)

/* Alarms */
# define Rte_Al_TE_ASW_10ms_Task_0_10ms (0U)
# define Rte_Al_TE_ASW_20ms_Task_0_100ms (1U)
# define Rte_Al_TE_ASW_20ms_Task_0_20ms (2U)
# define Rte_Al_TE_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable (3U)
# define Rte_Al_TE_Application_Data_NVM_Application_Data_NVM_1s_runnable (4U)
# define Rte_Al_TE2_BSW_10ms_Task_0_10ms (5U)
# define Rte_Al_TE2_BSW_5ms_Task_0_5ms (6U)
# define Rte_Al_TE2_BSW_Diag_Task_0_10ms (7U)
# define Rte_Al_TE2_BSW_Diag_Task_0_5ms (8U)

/* Events */
# define Rte_Ev_Cyclic2_BSW_Diag_Task_0_10ms (0x01)
# define Rte_Ev_Cyclic2_BSW_Diag_Task_0_5ms (0x02)
# define Rte_Ev_Cyclic_ASW_10ms_Task_0_10ms (0x01)
# define Rte_Ev_Cyclic_ASW_20ms_Task_0_100ms (0x01)
# define Rte_Ev_Cyclic_ASW_20ms_Task_0_20ms (0x02)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif (0x08)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif (0x20)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif (0x20000)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif (0x200)
# define Rte_Ev_OnEntry_ASW_Async_Task_BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotif (0x04)
# define Rte_Ev_Run1_DiagnosticComponent_DiagnosticComponent_Dcm_ActivateIss (0x400000)
# define Rte_Ev_Run1_DiagnosticComponent_DiagnosticComponent_Dcm_DeactivateIss (0x2000)
# define Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN6SchEndNotif (0x10000)
# define Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN7SchEndNotif (0x4000000)
# define Rte_Ev_Run1_LINMgr_SCIM_LINMgr_LIN8SchEndNotif (0x40000000)
# define Rte_Ev_Run_Application_Data_NVM_Application_Data_NVM_1s_runnable (0x200000)
# define Rte_Ev_Run_BswM_BswM_Read_LIN1_ScheduleTableRequestMode (0x04)
# define Rte_Ev_Run_BswM_BswM_Read_LIN2_ScheduleTableRequestMode (0x01)
# define Rte_Ev_Run_BswM_BswM_Read_LIN3_ScheduleTableRequestMode (0x20)
# define Rte_Ev_Run_BswM_BswM_Read_LIN4_ScheduleTableRequestMode (0x10)
# define Rte_Ev_Run_BswM_BswM_Read_LIN5_ScheduleTableRequestMode (0x08)
# define Rte_Ev_Run_BswM_BswM_Read_LIN6_ScheduleTableRequestMode (0x40)
# define Rte_Ev_Run_BswM_BswM_Read_LIN7_ScheduleTableRequestMode (0x02)
# define Rte_Ev_Run_BswM_BswM_Read_LIN8_ScheduleTableRequestMode (0x80)
# define Rte_Ev_Run_Calibration_Data_NVM_FspNVDataRxEventRunnable (0x8000)
# define Rte_Ev_Run_CryptoDriverDoorLatch_Tx_VEC_CryptoProxySenderConfirmation (0x40000)
# define Rte_Ev_Run_CryptoDriverDoorLatch_Tx_VEC_CryptoProxySenderReception (0x80000)
# define Rte_Ev_Run_CryptoDriverKeyCyl_Rx_VEC_CryptoProxyReceiverReception (0x80000000)
# define Rte_Ev_Run_CryptoEngineStart_Tx_VEC_CryptoProxySenderConfirmation (0x01)
# define Rte_Ev_Run_CryptoEngineStart_Tx_VEC_CryptoProxySenderReception (0x80)
# define Rte_Ev_Run_CryptoGearboxLock_Tx_VEC_CryptoProxySenderConfirmation (0x4000)
# define Rte_Ev_Run_CryptoGearboxLock_Tx_VEC_CryptoProxySenderReception (0x8000000)
# define Rte_Ev_Run_CryptoKeyAuth_Tx_VEC_CryptoProxySenderConfirmation (0x100)
# define Rte_Ev_Run_CryptoKeyAuth_Tx_VEC_CryptoProxySenderReception (0x400)
# define Rte_Ev_Run_CryptoLockingSwitch_Rx_VEC_CryptoProxyReceiverReception (0x2000000)
# define Rte_Ev_Run_CryptoLuggageCompartment_Tx_VEC_CryptoProxySenderConfirmation (0x100000)
# define Rte_Ev_Run_CryptoLuggageCompartment_Tx_VEC_CryptoProxySenderReception (0x40)
# define Rte_Ev_Run_CryptoPsngDoorLatch_Tx_VEC_CryptoProxySenderConfirmation (0x800)
# define Rte_Ev_Run_CryptoPsngDoorLatch_Tx_VEC_CryptoProxySenderReception (0x10)
# define Rte_Ev_Run_CryptoPsngKeyCyl_Rx_VEC_CryptoProxyReceiverReception (0x800000)
# define Rte_Ev_Run_CryptoReducedSetMode_Tx_VEC_CryptoProxySenderConfirmation (0x02)
# define Rte_Ev_Run_CryptoReducedSetMode_Tx_VEC_CryptoProxySenderReception (0x1000)
# define Rte_Ev_Run_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxySenderConfirmation (0x10000000)
# define Rte_Ev_Run_CryptoTheftAlarmActivation_Tx_VEC_CryptoProxySenderReception (0x20000000)
# define Rte_Ev_Run_DiagnosticComponent_LocalTimeDistribution_200ms_Runnable (0x04)
# define Rte_Ev_Run_Keyfob_Radio_Com_NVM_KeyFobNVDataRxEventRunnable (0x1000000)
# define Rte_Ev_Run_SCIM_Manager_SCIM_Manager_EnterRun (0x02)

/* Spinlocks */

/* Resources */

/* ScheduleTables */

/* Cores */

/* Trusted Functions */


#endif /* _OS_CFG_H_ */
