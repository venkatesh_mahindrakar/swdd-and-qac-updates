/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CollisionMitigation_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  CollisionMitigation_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <CollisionMitigation_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_CM_Configuration_P1LGD_T
 *   
 *
 * SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T
 *   
 *
 * SEWS_FCW_ConfirmTimeout_P1LGF_T
 *   
 *
 * SEWS_FCW_LedLogic_P1LG1_T
 *   
 *
 * SEWS_FCW_SwPushThreshold_P1LGE_T
 *   
 *
 * SEWS_FCW_SwStuckTimeout_P1LGG_T
 *   
 *
 * SEWS_HeadwaySupport_P1BEX_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CollisionMitigation_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_CollisionMitigation_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CollisionMitigation_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_CM_Configuration_P1LGD_T: Integer in interval [0...255]
 * SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T: Integer in interval [0...255]
 * SEWS_FCW_ConfirmTimeout_P1LGF_T: Integer in interval [0...255]
 * SEWS_FCW_LedLogic_P1LG1_T: Integer in interval [0...255]
 * SEWS_FCW_SwPushThreshold_P1LGE_T: Integer in interval [0...255]
 * SEWS_FCW_SwStuckTimeout_P1LGG_T: Integer in interval [0...255]
 * SEWS_HeadwaySupport_P1BEX_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * CM_Status_T: Enumeration of integer in interval [0...7] with enumerators
 *   CM_Status_CM_Disabled (0U)
 *   CM_Status_CM_Enabled (1U)
 *   CM_Status_CM_PreMitigationBraking (2U)
 *   CM_Status_CM_MitigationBraking (3U)
 *   CM_Status_CM_MitigationBrakingFinished (4U)
 *   CM_Status_CM_DisabledBySystem (5U)
 *   CM_Status_ErrorIndicator (6U)
 *   CM_Status_NotAvailable (7U)
 * CollSituationHMICtrlRequestVM_T: Enumeration of integer in interval [0...7] with enumerators
 *   CollSituationHMICtrlRequestVM_NoIndication (0U)
 *   CollSituationHMICtrlRequestVM_CM_FCWdisabledOnAftermarket (1U)
 *   CollSituationHMICtrlRequestVM_FCWdisabledOnAftermarket (2U)
 *   CollSituationHMICtrlRequestVM_Spare1 (3U)
 *   CollSituationHMICtrlRequestVM_Spare2 (4U)
 *   CollSituationHMICtrlRequestVM_Spare3 (5U)
 *   CollSituationHMICtrlRequestVM_Spare4 (6U)
 *   CollSituationHMICtrlRequestVM_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DisableEnable_T: Enumeration of integer in interval [0...3] with enumerators
 *   DisableEnable_Disable (0U)
 *   DisableEnable_Enable (1U)
 *   DisableEnable_Error (2U)
 *   DisableEnable_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * SetCMOperation_T: Enumeration of integer in interval [0...7] with enumerators
 *   SetCMOperation_NoRequest (0U)
 *   SetCMOperation_SetEmergencyBrakeON (1U)
 *   SetCMOperation_SetEmergencyBrakeOFF (2U)
 *   SetCMOperation_Spare1 (3U)
 *   SetCMOperation_Spare2 (4U)
 *   SetCMOperation_Spare3 (5U)
 *   SetCMOperation_Error (6U)
 *   SetCMOperation_NotAvailable (7U)
 * SetFCWOperation_T: Enumeration of integer in interval [0...7] with enumerators
 *   SetFCWOperation_NoRequest (0U)
 *   SetFCWOperation_SetCollisionWarningON (1U)
 *   SetFCWOperation_SetCollisionWarningOFF (2U)
 *   SetFCWOperation_Spare1 (3U)
 *   SetFCWOperation_Spare2 (4U)
 *   SetFCWOperation_Spare3 (5U)
 *   SetFCWOperation_Error (6U)
 *   SetFCWOperation_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HeadwaySupport_P1BEX_T Rte_Prm_P1BEX_HeadwaySupport_v(void)
 *   SEWS_FCW_LedLogic_P1LG1_T Rte_Prm_P1LG1_FCW_LedLogic_v(void)
 *   SEWS_CM_Configuration_P1LGD_T Rte_Prm_P1LGD_CM_Configuration_v(void)
 *   SEWS_FCW_SwPushThreshold_P1LGE_T Rte_Prm_P1LGE_FCW_SwPushThreshold_v(void)
 *   SEWS_FCW_ConfirmTimeout_P1LGF_T Rte_Prm_P1LGF_FCW_ConfirmTimeout_v(void)
 *   SEWS_FCW_SwStuckTimeout_P1LGG_T Rte_Prm_P1LGG_FCW_SwStuckTimeout_v(void)
 *   SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T Rte_Prm_P1MOT_CollSituationHMICtrlRequestVM_Time_v(void)
 *   boolean Rte_Prm_P1NT1_CM_DeviceType_v(void)
 *
 *********************************************************************************************************************/


#define CollisionMitigation_HMICtrl_START_SEC_CODE
#include "CollisionMitigation_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CollisionMitigation_HMICtrl_20ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AEBS_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_CM_Status_CM_Status(CM_Status_T *data)
 *   Std_ReturnType Rte_Read_FCWPushButton_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_FCW_Status_FCW_Status(DisableEnable_T *data)
 *   Std_ReturnType Rte_Read_FCW_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM(CollSituationHMICtrlRequestVM_T data)
 *   Std_ReturnType Rte_Write_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FCW_Enable_FCW_Enable(DisableEnable_T data)
 *   Std_ReturnType Rte_Write_SetCMOperation_SetCMOperation(SetCMOperation_T data)
 *   Std_ReturnType Rte_Write_SetFCWOperation_SetFCWOperation(SetFCWOperation_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CollisionMitigation_HMICtrl_20ms_Runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CollisionMitigation_HMICtrl_CODE) CollisionMitigation_HMICtrl_20ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CollisionMitigation_HMICtrl_20ms_Runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PushButtonStatus_T Read_AEBS_ButtonStatus_PushButtonStatus;
  CM_Status_T Read_CM_Status_CM_Status;
  PushButtonStatus_T Read_FCWPushButton_PushButtonStatus;
  DisableEnable_T Read_FCW_Status_FCW_Status;
  A2PosSwitchStatus_T Read_FCW_SwitchStatus_A2PosSwitchStatus;
  VehicleModeDistribution_T Read_SwcActivation_EngineRun_EngineRun;

  SEWS_HeadwaySupport_P1BEX_T P1BEX_HeadwaySupport_v_data;
  SEWS_FCW_LedLogic_P1LG1_T P1LG1_FCW_LedLogic_v_data;
  SEWS_CM_Configuration_P1LGD_T P1LGD_CM_Configuration_v_data;
  SEWS_FCW_SwPushThreshold_P1LGE_T P1LGE_FCW_SwPushThreshold_v_data;
  SEWS_FCW_ConfirmTimeout_P1LGF_T P1LGF_FCW_ConfirmTimeout_v_data;
  SEWS_FCW_SwStuckTimeout_P1LGG_T P1LGG_FCW_SwStuckTimeout_v_data;
  SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T P1MOT_CollSituationHMICtrlRequestVM_Time_v_data;
  boolean P1NT1_CM_DeviceType_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1BEX_HeadwaySupport_v_data = TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1BEX_HeadwaySupport_v();
  P1LG1_FCW_LedLogic_v_data = TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LG1_FCW_LedLogic_v();
  P1LGD_CM_Configuration_v_data = TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGD_CM_Configuration_v();
  P1LGE_FCW_SwPushThreshold_v_data = TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGE_FCW_SwPushThreshold_v();
  P1LGF_FCW_ConfirmTimeout_v_data = TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGF_FCW_ConfirmTimeout_v();
  P1LGG_FCW_SwStuckTimeout_v_data = TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGG_FCW_SwStuckTimeout_v();
  P1MOT_CollSituationHMICtrlRequestVM_Time_v_data = TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1MOT_CollSituationHMICtrlRequestVM_Time_v();
  P1NT1_CM_DeviceType_v_data = TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1NT1_CM_DeviceType_v();

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Read_AEBS_ButtonStatus_PushButtonStatus(&Read_AEBS_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Read_CM_Status_CM_Status(&Read_CM_Status_CM_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Read_FCWPushButton_PushButtonStatus(&Read_FCWPushButton_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Read_FCW_Status_FCW_Status(&Read_FCW_Status_FCW_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Read_FCW_SwitchStatus_A2PosSwitchStatus(&Read_FCW_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Read_SwcActivation_EngineRun_EngineRun(&Read_SwcActivation_EngineRun_EngineRun);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Write_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM(Rte_InitValue_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Write_FCW_DeviceIndication_DeviceIndication(Rte_InitValue_FCW_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Write_FCW_Enable_FCW_Enable(Rte_InitValue_FCW_Enable_FCW_Enable);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Write_SetCMOperation_SetCMOperation(Rte_InitValue_SetCMOperation_SetCMOperation);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_CollisionMitigation_HMICtrl_Rte_Write_SetFCWOperation_SetFCWOperation(Rte_InitValue_SetFCWOperation_SetFCWOperation);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  CollisionMitigation_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CollisionMitigation_HMICtrl_STOP_SEC_CODE
#include "CollisionMitigation_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CollisionMitigation_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  CM_Status_T Test_CM_Status_T_V_1 = CM_Status_CM_Disabled;
  CM_Status_T Test_CM_Status_T_V_2 = CM_Status_CM_Enabled;
  CM_Status_T Test_CM_Status_T_V_3 = CM_Status_CM_PreMitigationBraking;
  CM_Status_T Test_CM_Status_T_V_4 = CM_Status_CM_MitigationBraking;
  CM_Status_T Test_CM_Status_T_V_5 = CM_Status_CM_MitigationBrakingFinished;
  CM_Status_T Test_CM_Status_T_V_6 = CM_Status_CM_DisabledBySystem;
  CM_Status_T Test_CM_Status_T_V_7 = CM_Status_ErrorIndicator;
  CM_Status_T Test_CM_Status_T_V_8 = CM_Status_NotAvailable;

  CollSituationHMICtrlRequestVM_T Test_CollSituationHMICtrlRequestVM_T_V_1 = CollSituationHMICtrlRequestVM_NoIndication;
  CollSituationHMICtrlRequestVM_T Test_CollSituationHMICtrlRequestVM_T_V_2 = CollSituationHMICtrlRequestVM_CM_FCWdisabledOnAftermarket;
  CollSituationHMICtrlRequestVM_T Test_CollSituationHMICtrlRequestVM_T_V_3 = CollSituationHMICtrlRequestVM_FCWdisabledOnAftermarket;
  CollSituationHMICtrlRequestVM_T Test_CollSituationHMICtrlRequestVM_T_V_4 = CollSituationHMICtrlRequestVM_Spare1;
  CollSituationHMICtrlRequestVM_T Test_CollSituationHMICtrlRequestVM_T_V_5 = CollSituationHMICtrlRequestVM_Spare2;
  CollSituationHMICtrlRequestVM_T Test_CollSituationHMICtrlRequestVM_T_V_6 = CollSituationHMICtrlRequestVM_Spare3;
  CollSituationHMICtrlRequestVM_T Test_CollSituationHMICtrlRequestVM_T_V_7 = CollSituationHMICtrlRequestVM_Spare4;
  CollSituationHMICtrlRequestVM_T Test_CollSituationHMICtrlRequestVM_T_V_8 = CollSituationHMICtrlRequestVM_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DisableEnable_T Test_DisableEnable_T_V_1 = DisableEnable_Disable;
  DisableEnable_T Test_DisableEnable_T_V_2 = DisableEnable_Enable;
  DisableEnable_T Test_DisableEnable_T_V_3 = DisableEnable_Error;
  DisableEnable_T Test_DisableEnable_T_V_4 = DisableEnable_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  SetCMOperation_T Test_SetCMOperation_T_V_1 = SetCMOperation_NoRequest;
  SetCMOperation_T Test_SetCMOperation_T_V_2 = SetCMOperation_SetEmergencyBrakeON;
  SetCMOperation_T Test_SetCMOperation_T_V_3 = SetCMOperation_SetEmergencyBrakeOFF;
  SetCMOperation_T Test_SetCMOperation_T_V_4 = SetCMOperation_Spare1;
  SetCMOperation_T Test_SetCMOperation_T_V_5 = SetCMOperation_Spare2;
  SetCMOperation_T Test_SetCMOperation_T_V_6 = SetCMOperation_Spare3;
  SetCMOperation_T Test_SetCMOperation_T_V_7 = SetCMOperation_Error;
  SetCMOperation_T Test_SetCMOperation_T_V_8 = SetCMOperation_NotAvailable;

  SetFCWOperation_T Test_SetFCWOperation_T_V_1 = SetFCWOperation_NoRequest;
  SetFCWOperation_T Test_SetFCWOperation_T_V_2 = SetFCWOperation_SetCollisionWarningON;
  SetFCWOperation_T Test_SetFCWOperation_T_V_3 = SetFCWOperation_SetCollisionWarningOFF;
  SetFCWOperation_T Test_SetFCWOperation_T_V_4 = SetFCWOperation_Spare1;
  SetFCWOperation_T Test_SetFCWOperation_T_V_5 = SetFCWOperation_Spare2;
  SetFCWOperation_T Test_SetFCWOperation_T_V_6 = SetFCWOperation_Spare3;
  SetFCWOperation_T Test_SetFCWOperation_T_V_7 = SetFCWOperation_Error;
  SetFCWOperation_T Test_SetFCWOperation_T_V_8 = SetFCWOperation_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
