/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_VEC_CryptoProxyReceiverSwc.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_VEC_CryptoProxyReceiverSwc.h"
#include "TSC_VEC_CryptoProxyReceiverSwc.h"







Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Receive_VEC_EncryptedSignal_EncryptedSignal(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self, uint8 *data)
{
  return Rte_Receive_VEC_EncryptedSignal_EncryptedSignal(self, data);
}



Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Send_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self, const uint8 *data)
{
  return Rte_Send_VEC_CryptoIdKey_CryptoIdKey(self, data);
}


Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self, const UInt8 *data)
{
  return Rte_Write_VEC_CryptoProxySerializedData_Crypto_Function_serialized(self, data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_VEC_IdentificationKeyInit_IdentificationKeyInit(const struct Rte_CDS_VEC_CryptoProxyReceiverSwc* self, UInt32 *IdentificationKey)
{
  return Rte_Call_VEC_IdentificationKeyInit_IdentificationKeyInit(self, IdentificationKey);
}


     /* Service calls */
Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymEncrypt_SymEncryptFinish(const struct Rte_CDS_VEC_CryptoProxyReceiverSwc* self, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
{
  return Rte_Call_CsmSymEncrypt_SymEncryptFinish(&Rte_Instance_CryptoDriverKeyCyl_Rx, cipherTextBuffer, cipherTextLength);
}
Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymEncrypt_SymEncryptStart(const struct Rte_CDS_VEC_CryptoProxyReceiverSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
{
  return Rte_Call_CsmSymEncrypt_SymEncryptStart(&Rte_Instance_CryptoDriverKeyCyl_Rx, key, InitVectorBuffer, InitVectorLength);
}
Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymEncrypt_SymEncryptUpdate(const struct Rte_CDS_VEC_CryptoProxyReceiverSwc* self, const UInt8 *plainTextBuffer, UInt32_Length plainTextLength, UInt8 *cipherTextBuffer, UInt32_Length *cipherTextLength)
{
  return Rte_Call_CsmSymEncrypt_SymEncryptUpdate(&Rte_Instance_CryptoDriverKeyCyl_Rx, plainTextBuffer, plainTextLength, cipherTextBuffer, cipherTextLength);
}


     /* Mode Interfaces */



Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Feedback_VEC_CryptoIdKey_CryptoIdKey(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
  return Rte_Feedback_VEC_CryptoIdKey_CryptoIdKey(self);
}

     /* Trigger Interfaces */

     /* Inter-Runnable variables */
UInt32 TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
return Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(self);
}
VEC_CryptoProxy_IdentificationState_Type TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
return Rte_IrvRead_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(self);
}

void TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self, UInt32 data)
{
  Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationNumber(self,  data);
}
void TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self, VEC_CryptoProxy_IdentificationState_Type data)
{
  Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_IdentificationState(self,  data);
}
void TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_NewReceivedIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self, UInt32 data)
{
  Rte_IrvWrite_VEC_CryptoProxyReceiverMainFunction_VEC_CryptoProxy_NewReceivedIdentificationNumber(self,  data);
}












     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymDecrypt_SymDecryptFinish(const struct Rte_CDS_VEC_CryptoProxyReceiverSwc* self, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
{
  return Rte_Call_CsmSymDecrypt_SymDecryptFinish(&Rte_Instance_CryptoDriverKeyCyl_Rx, plainTextBuffer, plainTextLength);
}
Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymDecrypt_SymDecryptStart(const struct Rte_CDS_VEC_CryptoProxyReceiverSwc* self, const SymKeyType *key, const UInt8 *InitVectorBuffer, UInt32_Length InitVectorLength)
{
  return Rte_Call_CsmSymDecrypt_SymDecryptStart(&Rte_Instance_CryptoDriverKeyCyl_Rx, key, InitVectorBuffer, InitVectorLength);
}
Std_ReturnType TSC_VEC_CryptoProxyReceiverSwc_Rte_Call_CsmSymDecrypt_SymDecryptUpdate(const struct Rte_CDS_VEC_CryptoProxyReceiverSwc* self, const UInt8 *cipherTextBuffer, UInt32_Length cipherTextLength, UInt8 *plainTextBuffer, UInt32_Length *plainTextLength)
{
  return Rte_Call_CsmSymDecrypt_SymDecryptUpdate(&Rte_Instance_CryptoDriverKeyCyl_Rx, cipherTextBuffer, cipherTextLength, plainTextBuffer, plainTextLength);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
UInt32 TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
return Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(self);
}
VEC_CryptoProxy_IdentificationState_Type TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
return Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(self);
}
UInt32 TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
return Rte_IrvRead_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_NewReceivedIdentificationNumber(self);
}

void TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self, UInt32 data)
{
  Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationNumber(self,  data);
}
void TSC_VEC_CryptoProxyReceiverSwc_Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self, VEC_CryptoProxy_IdentificationState_Type data)
{
  Rte_IrvWrite_VEC_CryptoProxyReceiverReception_VEC_CryptoProxy_IdentificationState(self,  data);
}












     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_ComCryptoKey_P1DLX_a_T * TSC_VEC_CryptoProxyReceiverSwc_Rte_Prm_ComCryptoKey_P1DLX_v(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
  return (SEWS_ComCryptoKey_P1DLX_a_T *) Rte_Prm_ComCryptoKey_P1DLX_v(self);
}

UInt16  TSC_VEC_CryptoProxyReceiverSwc_Rte_CData_VEC_CryptoProxyTimeFactorResentIdentification(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
  return (UInt16 ) Rte_CData_VEC_CryptoProxyTimeFactorResentIdentification(self);
}
UInt16  TSC_VEC_CryptoProxyReceiverSwc_Rte_CData_VEC_CryptoProxyTimeFactorTillNewIdentificationKey(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
  return (UInt16 ) Rte_CData_VEC_CryptoProxyTimeFactorTillNewIdentificationKey(self);
}

     /* VEC_CryptoProxyReceiverSwc */
      /* VEC_CryptoProxyReceiverSwc */
      /* VEC_CryptoProxyReceiverSwc */
      /* VEC_CryptoProxyReceiverSwc */

/** Per Instance Memories */
UInt16 *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_DelayTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
  return Rte_Pim_VEC_CryptoProxy_DelayTimer(self);
}
UInt16 *TSC_VEC_CryptoProxyReceiverSwc_Rte_Pim_VEC_CryptoProxy_ResentTimer(P2CONST(struct Rte_CDS_VEC_CryptoProxyReceiverSwc, AUTOMATIC, RTE_CONST) self)
{
  return Rte_Pim_VEC_CryptoProxy_ResentTimer(self);
}



