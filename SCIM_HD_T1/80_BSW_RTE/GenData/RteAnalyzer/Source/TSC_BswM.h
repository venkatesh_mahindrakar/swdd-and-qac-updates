/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_BswM.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_BswM_Rte_Read_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode(BswM_BswMRteMDG_NvmWriteAllRequest *data);
Std_ReturnType TSC_BswM_Rte_Read_Request_SwcModeRequest_PvtReportCtrl_requestedMode(BswM_BswMRteMDG_PvtReport_X1C14 *data);
Std_ReturnType TSC_BswM_Rte_Read_Request_LIN1_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN1Schedule *data);
Std_ReturnType TSC_BswM_Rte_Read_Request_LIN2_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN2Schedule *data);
Std_ReturnType TSC_BswM_Rte_Read_Request_LIN3_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN3Schedule *data);
Std_ReturnType TSC_BswM_Rte_Read_Request_LIN4_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN4Schedule *data);
Std_ReturnType TSC_BswM_Rte_Read_Request_LIN5_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN5Schedule *data);
Std_ReturnType TSC_BswM_Rte_Read_Request_LIN6_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN6Schedule *data);
Std_ReturnType TSC_BswM_Rte_Read_Request_LIN7_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN7Schedule *data);
Std_ReturnType TSC_BswM_Rte_Read_Request_LIN8_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN8Schedule *data);

/** Mode switches */
BswM_ESH_Mode TSC_BswM_Rte_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode(void);
Dcm_EcuResetType TSC_BswM_Rte_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset(void);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(BswM_BswMRteMDG_LIN1Schedule mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(BswM_BswMRteMDG_LIN2Schedule mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(BswM_BswMRteMDG_LIN3Schedule mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(BswM_BswMRteMDG_LIN4Schedule mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(BswM_BswMRteMDG_LIN5Schedule mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(BswM_BswMRteMDG_LIN6Schedule mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(BswM_BswMRteMDG_LIN7Schedule mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(BswM_BswMRteMDG_LIN8Schedule mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode);
Std_ReturnType TSC_BswM_Rte_Switch_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode(BswM_ESH_Mode mode);




