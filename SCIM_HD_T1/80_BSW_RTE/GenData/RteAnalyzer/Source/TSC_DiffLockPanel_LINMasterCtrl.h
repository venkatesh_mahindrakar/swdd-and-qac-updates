/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DiffLockPanel_LINMasterCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_DiagInfoDLFW_DiagInfo(DiagInfo_T *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_EscButtonMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_LIN_DifflockDeactivationBtn_st_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_LIN_DifflockMode_Wheelstatus_FreeWheel_Status(FreeWheel_Status_T *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_LIN_EscButtonMuddySiteStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_LIN_Offroad_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_Offroad_Indication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Read_ResponseErrorDLFW_ResponseErrorDLFW(ResponseErrorDLFW_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_DifflockDeactivationBtn_stat_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_DifflockMode_Wheelstatus_FreeWheel_Status(FreeWheel_Status_T data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_EscButtonMuddySiteStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_LIN_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_LIN_EscButtonMuddySiteDeviceIn_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_LIN_Offroad_Indication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Write_Offroad_ButtonStatus_PushButtonStatus(PushButtonStatus_T data);

/** Service interfaces */
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BKH_87_DLFWLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_16_DLFW_VBT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_17_DLFW_VAT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_44_DLFW_RAM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_45_DLFW_FLASH_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_46_DLFW_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_DiffLockPanel_LINMasterCtrl_Rte_Call_Event_D1BN2_94_DLFW_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus);

/** Explicit inter-runnable variables */
uint8 TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvRead_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData_Irv_IOCTL_DiffLockLinCtrl(void);
void TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_DiffLockLinCtrl(uint8);
void TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvWrite_DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_DiffLockLinCtrl(uint8);
uint8 TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvRead_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl(void);
void TSC_DiffLockPanel_LINMasterCtrl_Rte_IrvWrite_DiffLockPanel_LINMasterCtrl_20ms_runnable_Irv_IOCTL_DiffLockLinCtrl(uint8);

/** Calibration Component Calibration Parameters */
boolean  TSC_DiffLockPanel_LINMasterCtrl_Rte_Prm_P1B04_DLFW_Installed_v(void);




