/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  LevelControl_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  LevelControl_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <LevelControl_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   
 *
 * SEWS_ECSActiveStateTimeout_P1CUE_T
 *   
 *
 * SEWS_ECSStandbyActivationTimeout_P1CUA_T
 *   
 *
 * SEWS_ECSStandbyExtendedActTimeout_P1CUB_T
 *   
 *
 * SEWS_ECS_StandbyBlinkTime_P1GCL_T
 *   
 *
 * SEWS_FPBRSwitchRequestACKTime_P1LXR_T
 *   
 *
 * SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T
 *   
 *
 * SEWS_FerryFuncSwStuckedTimeout_P1EXK_T
 *   
 *
 * SEWS_FrontSuspensionType_P1JBR_T
 *   
 *
 * SEWS_KneelButtonStuckedTimeout_P1DWD_T
 *   
 *
 * SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T
 *   
 *
 * SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_LevelControl_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_LevelControl_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void LevelControl_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ECSActiveStateTimeout_P1CUE_T: Integer in interval [0...65535]
 * SEWS_ECSStandbyActivationTimeout_P1CUA_T: Integer in interval [0...65535]
 * SEWS_ECSStandbyExtendedActTimeout_P1CUB_T: Integer in interval [0...65535]
 * SEWS_ECS_StandbyBlinkTime_P1GCL_T: Integer in interval [0...255]
 * SEWS_FPBRSwitchRequestACKTime_P1LXR_T: Integer in interval [0...255]
 * SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T: Integer in interval [0...255]
 * SEWS_FerryFuncSwStuckedTimeout_P1EXK_T: Integer in interval [0...255]
 * SEWS_FrontSuspensionType_P1JBR_T: Integer in interval [0...255]
 * SEWS_KneelButtonStuckedTimeout_P1DWD_T: Integer in interval [0...255]
 * SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T: Integer in interval [0...255]
 * SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * Ack2Bit_T: Enumeration of integer in interval [0...3] with enumerators
 *   Ack2Bit_NoAction (0U)
 *   Ack2Bit_ChangeAcknowledged (1U)
 *   Ack2Bit_Error (2U)
 *   Ack2Bit_NotAvailable (3U)
 * BackToDriveReqACK_T: Enumeration of integer in interval [0...3] with enumerators
 *   BackToDriveReqACK_TakNoAction (0U)
 *   BackToDriveReqACK_ChangeAcknowledged (1U)
 *   BackToDriveReqACK_Error (2U)
 *   BackToDriveReqACK_NotAvailable (3U)
 * BackToDriveReq_T: Enumeration of integer in interval [0...3] with enumerators
 *   BackToDriveReq_Idle (0U)
 *   BackToDriveReq_B2DRequested (1U)
 *   Requested_Error (2U)
 *   Requested_NotAvailable (3U)
 * ChangeKneelACK_T: Enumeration of integer in interval [0...3] with enumerators
 *   ChangeKneelACK_NoAction (0U)
 *   ChangeKneelACK_ChangeAcknowledged (1U)
 *   ChangeKneelACK_Error (2U)
 *   ChangeKneelACK_NotAvailable (3U)
 * ChangeRequest2Bit_T: Enumeration of integer in interval [0...3] with enumerators
 *   ChangeRequest2Bit_TakeNoAction (0U)
 *   ChangeRequest2Bit_Change (1U)
 *   ChangeRequest2Bit_Error (2U)
 *   ChangeRequest2Bit_NotAvailable (3U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * ECSStandByReq_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByReq_Idle (0U)
 *   ECSStandByReq_StandbyRequested (1U)
 *   ECSStandByReq_StopStandby (2U)
 *   ECSStandByReq_Reserved (3U)
 *   ECSStandByReq_Reserved_01 (4U)
 *   ECSStandByReq_Reserved_02 (5U)
 *   ECSStandByReq_Error (6U)
 *   ECSStandByReq_NotAvailable (7U)
 * ECSStandByRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByRequest_NoRequest (0U)
 *   ECSStandByRequest_Initiate (1U)
 *   ECSStandByRequest_StandbyRequestedRCECS (2U)
 *   ECSStandByRequest_StandbyRequestedWRC (3U)
 *   ECSStandByRequest_Reserved (4U)
 *   ECSStandByRequest_Reserved_01 (5U)
 *   ECSStandByRequest_Error (6U)
 *   ECSStandByRequest_NotAvailable (7U)
 * ElectricalLoadReduction_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   ElectricalLoadReduction_rqst_NoRequest (0U)
 *   ElectricalLoadReduction_rqst_Level1Request (1U)
 *   ElectricalLoadReduction_rqst_Level2Request (2U)
 *   ElectricalLoadReduction_rqst_SpareValue (3U)
 *   ElectricalLoadReduction_rqst_SpareValue_01 (4U)
 *   ElectricalLoadReduction_rqst_SpareValue_02 (5U)
 *   ElectricalLoadReduction_rqst_Error (6U)
 *   ElectricalLoadReduction_rqst_NotAvailable (7U)
 * FPBRChangeReq_T: Enumeration of integer in interval [0...3] with enumerators
 *   FPBRChangeReq_TakeNoAction (0U)
 *   FPBRChangeReq_ChangeFPBRFunction (1U)
 *   FPBRChangeReq_Error (2U)
 *   FPBRChangeReq_NotAvailable (3U)
 * FPBRStatusInd_T: Enumeration of integer in interval [0...3] with enumerators
 *   FPBRStatusInd_Off (0U)
 *   FPBRStatusInd_On (1U)
 *   FPBRStatusInd_Changing (2U)
 *   FPBRStatusInd_NotAvailable (3U)
 * FalseTrue_T: Enumeration of integer in interval [0...3] with enumerators
 *   FalseTrue_False (0U)
 *   FalseTrue_True (1U)
 *   FalseTrue_Error (2U)
 *   FalseTrue_NotAvaiable (3U)
 * FerryFunctionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   FerryFunctionStatus_FerryFunctionNotActive (0U)
 *   FerryFunctionStatus_FerryFunctionActiveLevelReached (1U)
 *   FerryFunctionStatus_FerryFunctionActiveLeveNotlReached (2U)
 *   FerryFunctionStatus_Spare_01 (3U)
 *   FerryFunctionStatus_Spare_02 (4U)
 *   FerryFunctionStatus_Spare_03 (5U)
 *   FerryFunctionStatus_Error (6U)
 *   FerryFunctionStatus_NotAvailable (7U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * KneelingChangeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 *   KneelingChangeRequest_TakeNoAction (0U)
 *   KneelingChangeRequest_ChangeKneelFunction (1U)
 *   KneelingChangeRequest_Error (2U)
 *   KneelingChangeRequest_NotAvailable (3U)
 * KneelingStatusHMI_T: Enumeration of integer in interval [0...3] with enumerators
 *   KneelingStatusHMI_Inactive (0U)
 *   KneelingStatusHMI_Active (1U)
 *   KneelingStatusHMI_Error (2U)
 *   KneelingStatusHMI_NotAvailable (3U)
 * LevelAdjustmentAction_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelAdjustmentAction_Idle (0U)
 *   LevelAdjustmentAction_UpBasic (1U)
 *   LevelAdjustmentAction_DownBasic (2U)
 *   LevelAdjustmentAction_UpShortMovement (3U)
 *   LevelAdjustmentAction_DownShortMovement (4U)
 *   LevelAdjustmentAction_Reserved (5U)
 *   LevelAdjustmentAction_GotoDriveLevel (6U)
 *   LevelAdjustmentAction_Reserved_01 (7U)
 *   LevelAdjustmentAction_Ferry (8U)
 *   LevelAdjustmentAction_Reserved_02 (9U)
 *   LevelAdjustmentAction_Reserved_03 (10U)
 *   LevelAdjustmentAction_Reserved_04 (11U)
 *   LevelAdjustmentAction_Reserved_05 (12U)
 *   LevelAdjustmentAction_Reserved_06 (13U)
 *   LevelAdjustmentAction_Error (14U)
 *   LevelAdjustmentAction_NotAvailable (15U)
 * LevelAdjustmentAxles_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentAxles_Rear (0U)
 *   LevelAdjustmentAxles_Front (1U)
 *   LevelAdjustmentAxles_Parallel (2U)
 *   LevelAdjustmentAxles_Reserved (3U)
 *   LevelAdjustmentAxles_Reserved_01 (4U)
 *   LevelAdjustmentAxles_Reserved_02 (5U)
 *   LevelAdjustmentAxles_Error (6U)
 *   LevelAdjustmentAxles_NotAvailable (7U)
 * LevelAdjustmentStroke_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentStroke_DriveStroke (0U)
 *   LevelAdjustmentStroke_DockingStroke (1U)
 *   LevelAdjustmentStroke_Reserved (2U)
 *   LevelAdjustmentStroke_Reserved_01 (3U)
 *   LevelAdjustmentStroke_Reserved_02 (4U)
 *   LevelAdjustmentStroke_Reserved_03 (5U)
 *   LevelAdjustmentStroke_Error (6U)
 *   LevelAdjustmentStroke_NotAvailable (7U)
 * LevelChangeRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelChangeRequest_TakeNoAction (0U)
 *   LevelChangeRequest_VehicleBodyUpLifting (1U)
 *   LevelChangeRequest_VehicleBodyDownLowering (2U)
 *   LevelChangeRequest_VehicleBodyUpMinimumMovementLifting (3U)
 *   LevelChangeRequest_VehicleBodyDownMinimumMovementLowering (4U)
 *   LevelChangeRequest_NotDefined (5U)
 *   LevelChangeRequest_ErrorIndicator (6U)
 *   LevelChangeRequest_NotAvailable (7U)
 * LevelStrokeRequest_T: Enumeration of integer in interval [0...3] with enumerators
 *   LevelStrokeRequest_DockingLevelControlStroke (0U)
 *   LevelStrokeRequest_DriveLevelControlStroke (1U)
 *   LevelStrokeRequest_ErrorIndicator (2U)
 *   LevelStrokeRequest_NotAvailable (3U)
 * LevelUserMemoryAction_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelUserMemoryAction_Inactive (0U)
 *   LevelUserMemoryAction_Recall (1U)
 *   LevelUserMemoryAction_Store (2U)
 *   LevelUserMemoryAction_Default (3U)
 *   LevelUserMemoryAction_Reserved (4U)
 *   LevelUserMemoryAction_Reserved_01 (5U)
 *   LevelUserMemoryAction_Error (6U)
 *   LevelUserMemoryAction_NotAvailable (7U)
 * LevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelUserMemory_NotUsed (0U)
 *   LevelUserMemory_M1 (1U)
 *   LevelUserMemory_M2 (2U)
 *   LevelUserMemory_M3 (3U)
 *   LevelUserMemory_M4 (4U)
 *   LevelUserMemory_M5 (5U)
 *   LevelUserMemory_M6 (6U)
 *   LevelUserMemory_M7 (7U)
 *   LevelUserMemory_Spare (8U)
 *   LevelUserMemory_Spare01 (9U)
 *   LevelUserMemory_Spare02 (10U)
 *   LevelUserMemory_Spare03 (11U)
 *   LevelUserMemory_Spare04 (12U)
 *   LevelUserMemory_Spare05 (13U)
 *   LevelUserMemory_Error (14U)
 *   LevelUserMemory_NotAvailable (15U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * RampLevelRequest_T: Enumeration of integer in interval [0...15] with enumerators
 *   RampLevelRequest_TakeNoAction (0U)
 *   RampLevelRequest_RampLevelM1 (1U)
 *   RampLevelRequest_RampLevelM2 (2U)
 *   RampLevelRequest_RampLevelM3 (3U)
 *   RampLevelRequest_RampLevelM4 (4U)
 *   RampLevelRequest_RampLevelM5 (5U)
 *   RampLevelRequest_RampLevelM6 (6U)
 *   RampLevelRequest_RampLevelM7 (7U)
 *   RampLevelRequest_NotDefined_04 (8U)
 *   RampLevelRequest_NotDefined_05 (9U)
 *   RampLevelRequest_NotDefined_06 (10U)
 *   RampLevelRequest_NotDefined_07 (11U)
 *   RampLevelRequest_NotDefined_08 (12U)
 *   RampLevelRequest_NotDefined_09 (13U)
 *   RampLevelRequest_ErrorIndicator (14U)
 *   RampLevelRequest_NotAvailable (15U)
 * Request_T: Enumeration of integer in interval [0...3] with enumerators
 *   Request_NotRequested (0U)
 *   Request_RequestActive (1U)
 *   Request_Error (2U)
 *   Request_NotAvailable (3U)
 * RideHeightFunction_T: Enumeration of integer in interval [0...15] with enumerators
 *   RideHeightFunction_Inactive (0U)
 *   RideHeightFunction_StandardDrivePosition (1U)
 *   RideHeightFunction_AlternativeDrivePosition (2U)
 *   RideHeightFunction_SpeedDependentDrivePosition (3U)
 *   RideHeightFunction_HighDrivePosition (4U)
 *   RideHeightFunction_LowDrivePosition (5U)
 *   RideHeightFunction_AlternativeDrivePosition1 (6U)
 *   RideHeightFunction_AlternativeDrivePosition2 (7U)
 *   RideHeightFunction_NotDefined (8U)
 *   RideHeightFunction_NotDefined_01 (9U)
 *   RideHeightFunction_NotDefined_02 (10U)
 *   RideHeightFunction_NotDefined_03 (11U)
 *   RideHeightFunction_NotDefined_04 (12U)
 *   RideHeightFunction_NotDefined_05 (13U)
 *   RideHeightFunction_ErrorIndicator (14U)
 *   RideHeightFunction_NotAvailable (15U)
 * RideHeightStorageRequest_T: Enumeration of integer in interval [0...15] with enumerators
 *   RideHeightStorageRequest_TakeNoAction (0U)
 *   RideHeightStorageRequest_ResetToStoreFactoryDrivePosition (1U)
 *   RideHeightStorageRequest_StoreStandardUserDrivePosition (2U)
 *   RideHeightStorageRequest_StoreLowDrivePosition (3U)
 *   RideHeightStorageRequest_StoreHighDrivePosition (4U)
 *   RideHeightStorageRequest_ResetLowDrivePosition (5U)
 *   RideHeightStorageRequest_ResetHighDrivePosition (6U)
 *   RideHeightStorageRequest_NotDefined (7U)
 *   RideHeightStorageRequest_NotDefined_01 (8U)
 *   RideHeightStorageRequest_NotDefined_02 (9U)
 *   RideHeightStorageRequest_NotDefined_03 (10U)
 *   RideHeightStorageRequest_NotDefined_04 (11U)
 *   RideHeightStorageRequest_NotDefined_05 (12U)
 *   RideHeightStorageRequest_NotDefined_06 (13U)
 *   RideHeightStorageRequest_ErrorIndicator (14U)
 *   RideHeightStorageRequest_NotAvailable (15U)
 * RollRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   RollRequest_Idle (0U)
 *   RollRequest_Clockwise (1U)
 *   RollRequest_CounterClockwise (2U)
 *   RollRequest_ClockwiseShort (3U)
 *   RollRequest_CounterClockwiseShort (4U)
 *   RollRequest_Reserved (5U)
 *   RollRequest_Error (6U)
 *   RollRequest_NotAvailable (7U)
 * StopLevelChangeStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   StopLevelChangeStatus_NoStopRequest (0U)
 *   StopLevelChangeStatus_LevelChangedStopped (1U)
 *   StopLevelChangeStatus_ErrorIndicator (2U)
 *   StopLevelChangeStatus_NotAvailable (3U)
 * StorageAck_T: Enumeration of integer in interval [0...3] with enumerators
 *   StorageAck_TakeNoAction (0U)
 *   StorageAck_ChangeAcknowledged (1U)
 *   StorageAck_Error (2U)
 *   StorageAck_NotAvaiable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * WiredLevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 *   WiredLevelUserMemory_MemOff (0U)
 *   WiredLevelUserMemory_M1 (1U)
 *   WiredLevelUserMemory_M2 (2U)
 *   WiredLevelUserMemory_M3 (3U)
 *   WiredLevelUserMemory_M4 (4U)
 *   WiredLevelUserMemory_M5 (5U)
 *   WiredLevelUserMemory_M6 (6U)
 *   WiredLevelUserMemory_M7 (7U)
 *   WiredLevelUserMemory_Spare (8U)
 *   WiredLevelUserMemory_Spare01 (9U)
 *   WiredLevelUserMemory_Spare02 (10U)
 *   WiredLevelUserMemory_Spare03 (11U)
 *   WiredLevelUserMemory_Spare04 (12U)
 *   WiredLevelUserMemory_Spare05 (13U)
 *   WiredLevelUserMemory_Error (14U)
 *   WiredLevelUserMemory_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 *
 * Record Types:
 * =============
 * FPBRMMIStat_T: Record with elements
 *   FPBRStatusInd_RE of type FPBRStatusInd_T
 *   FPBRChangeAck_RE of type Ack2Bit_T
 * LevelRequest_T: Record with elements
 *   FrontAxle_RE of type LevelChangeRequest_T
 *   RearAxle_RE of type LevelChangeRequest_T
 *   RollRequest_RE of type RollRequest_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ECSStandbyActivationTimeout_P1CUA_T Rte_Prm_P1CUA_ECSStandbyActivationTimeout_v(void)
 *   SEWS_ECSStandbyExtendedActTimeout_P1CUB_T Rte_Prm_P1CUB_ECSStandbyExtendedActTimeout_v(void)
 *   SEWS_ECSActiveStateTimeout_P1CUE_T Rte_Prm_P1CUE_ECSActiveStateTimeout_v(void)
 *   SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T Rte_Prm_P1CUF_LoadingLevelSwStuckedTimeout_v(void)
 *   SEWS_KneelButtonStuckedTimeout_P1DWD_T Rte_Prm_P1DWD_KneelButtonStuckedTimeout_v(void)
 *   SEWS_FerryFuncSwStuckedTimeout_P1EXK_T Rte_Prm_P1EXK_FerryFuncSwStuckedTimeout_v(void)
 *   SEWS_ECS_StandbyBlinkTime_P1GCL_T Rte_Prm_P1GCL_ECS_StandbyBlinkTime_v(void)
 *   SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T Rte_Prm_P1IZ2_LoadingLevelAdjSwStuckTimeout_v(void)
 *   SEWS_FrontSuspensionType_P1JBR_T Rte_Prm_P1JBR_FrontSuspensionType_v(void)
 *   SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T Rte_Prm_P1LXQ_FPBRSwitchStuckedTimeout_v(void)
 *   SEWS_FPBRSwitchRequestACKTime_P1LXR_T Rte_Prm_P1LXR_FPBRSwitchRequestACKTime_v(void)
 *   boolean Rte_Prm_P1A12_ADL_Sw_v(void)
 *   boolean Rte_Prm_P1CT4_FerrySw_Installed_v(void)
 *   boolean Rte_Prm_P1CT9_LoadingLevelSw_Installed_v(void)
 *   boolean Rte_Prm_P1EXH_KneelingSwitchInstalled_v(void)
 *   boolean Rte_Prm_P1IZ1_LoadingLevelAdjSwitchInstalled_v(void)
 *   boolean Rte_Prm_P1LXP_FPBRSwitchInstalled_v(void)
 *   boolean Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void)
 *   boolean Rte_Prm_P1ALU_ECS_FullAirSystem_v(void)
 *   boolean Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
 *
 *********************************************************************************************************************/


#define LevelControl_HMICtrl_START_SEC_CODE
#include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint32 Rte_IrvRead_DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_IRV_ECS_StandbyTimer(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DVZ_Data_P1DVZ_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, LevelControl_HMICtrl_CODE) DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(P2VAR(uint8, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_ECSStandbyActivationTimeout_P1CUA_T P1CUA_ECSStandbyActivationTimeout_v_data;
  SEWS_ECSStandbyExtendedActTimeout_P1CUB_T P1CUB_ECSStandbyExtendedActTimeout_v_data;
  SEWS_ECSActiveStateTimeout_P1CUE_T P1CUE_ECSActiveStateTimeout_v_data;
  SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T P1CUF_LoadingLevelSwStuckedTimeout_v_data;
  SEWS_KneelButtonStuckedTimeout_P1DWD_T P1DWD_KneelButtonStuckedTimeout_v_data;
  SEWS_FerryFuncSwStuckedTimeout_P1EXK_T P1EXK_FerryFuncSwStuckedTimeout_v_data;
  SEWS_ECS_StandbyBlinkTime_P1GCL_T P1GCL_ECS_StandbyBlinkTime_v_data;
  SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T P1IZ2_LoadingLevelAdjSwStuckTimeout_v_data;
  SEWS_FrontSuspensionType_P1JBR_T P1JBR_FrontSuspensionType_v_data;
  SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T P1LXQ_FPBRSwitchStuckedTimeout_v_data;
  SEWS_FPBRSwitchRequestACKTime_P1LXR_T P1LXR_FPBRSwitchRequestACKTime_v_data;
  boolean P1A12_ADL_Sw_v_data;
  boolean P1CT4_FerrySw_Installed_v_data;
  boolean P1CT9_LoadingLevelSw_Installed_v_data;
  boolean P1EXH_KneelingSwitchInstalled_v_data;
  boolean P1IZ1_LoadingLevelAdjSwitchInstalled_v_data;
  boolean P1LXP_FPBRSwitchInstalled_v_data;

  boolean P1ALT_ECS_PartialAirSystem_v_data;
  boolean P1ALU_ECS_FullAirSystem_v_data;
  boolean P1B9X_WirelessRC_Enable_v_data;

  uint32 DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_IRV_ECS_StandbyTimer;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1CUA_ECSStandbyActivationTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUA_ECSStandbyActivationTimeout_v();
  P1CUB_ECSStandbyExtendedActTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUB_ECSStandbyExtendedActTimeout_v();
  P1CUE_ECSActiveStateTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUE_ECSActiveStateTimeout_v();
  P1CUF_LoadingLevelSwStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUF_LoadingLevelSwStuckedTimeout_v();
  P1DWD_KneelButtonStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1DWD_KneelButtonStuckedTimeout_v();
  P1EXK_FerryFuncSwStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1EXK_FerryFuncSwStuckedTimeout_v();
  P1GCL_ECS_StandbyBlinkTime_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1GCL_ECS_StandbyBlinkTime_v();
  P1IZ2_LoadingLevelAdjSwStuckTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1IZ2_LoadingLevelAdjSwStuckTimeout_v();
  P1JBR_FrontSuspensionType_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1JBR_FrontSuspensionType_v();
  P1LXQ_FPBRSwitchStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1LXQ_FPBRSwitchStuckedTimeout_v();
  P1LXR_FPBRSwitchRequestACKTime_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1LXR_FPBRSwitchRequestACKTime_v();
  P1A12_ADL_Sw_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1A12_ADL_Sw_v();
  P1CT4_FerrySw_Installed_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CT4_FerrySw_Installed_v();
  P1CT9_LoadingLevelSw_Installed_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CT9_LoadingLevelSw_Installed_v();
  P1EXH_KneelingSwitchInstalled_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1EXH_KneelingSwitchInstalled_v();
  P1IZ1_LoadingLevelAdjSwitchInstalled_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1IZ1_LoadingLevelAdjSwitchInstalled_v();
  P1LXP_FPBRSwitchInstalled_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1LXP_FPBRSwitchInstalled_v();

  P1ALT_ECS_PartialAirSystem_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1ALT_ECS_PartialAirSystem_v();
  P1ALU_ECS_FullAirSystem_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1ALU_ECS_FullAirSystem_v();
  P1B9X_WirelessRC_Enable_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v();

  DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_IRV_ECS_StandbyTimer = TSC_LevelControl_HMICtrl_Rte_IrvRead_DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData_IRV_ECS_StandbyTimer();

  LevelControl_HMICtrl_TestDefines();

  return RTE_E_DataServices_P1DVZ_Data_P1DVZ_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LevelControl_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_BackToDriveReqACK_BackToDriveReqACK(BackToDriveReqACK_T *data)
 *   Std_ReturnType Rte_Read_ChangeKneelACK_ChangeKneelACK(ChangeKneelACK_T *data)
 *   Std_ReturnType Rte_Read_ECSStandByReqRCECS_ECSStandByReqRCECS(ECSStandByReq_T *data)
 *   Std_ReturnType Rte_Read_ECSStandByReqWRC_ECSStandByReqWRC(ECSStandByReq_T *data)
 *   Std_ReturnType Rte_Read_ECSStandbyAllowed_ECSStandbyAllowed(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst(ElectricalLoadReduction_rqst_T *data)
 *   Std_ReturnType Rte_Read_FPBRMMIStat_FPBRMMIStat(FPBRMMIStat_T *data)
 *   Std_ReturnType Rte_Read_FPBRSwitchStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_FerryFunctionStatus_FerryFunctionStatus(FerryFunctionStatus_T *data)
 *   Std_ReturnType Rte_Read_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK(Ack2Bit_T *data)
 *   Std_ReturnType Rte_Read_FerryFunctionSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_KneelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_KneelingStatusHMI_KneelingStatusHMI(KneelingStatusHMI_T *data)
 *   Std_ReturnType Rte_Read_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_LoadingLevelSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_RampLevelRequestACK_RampLevelRequestACK(ChangeRequest2Bit_T *data)
 *   Std_ReturnType Rte_Read_RampLevelStorageAck_RampLevelStorageAck(StorageAck_T *data)
 *   Std_ReturnType Rte_Read_RideHeightStorageAck_RideHeightStorageAck(StorageAck_T *data)
 *   Std_ReturnType Rte_Read_StopLevelChangeAck_StopLevelChangeStatus(StopLevelChangeStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_WRCAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelUserMemory_LevelUserMemory(LevelUserMemory_T *data)
 *   Std_ReturnType Rte_Read_WRCLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T *data)
 *   Std_ReturnType Rte_Read_WRCRollRequest_WRCRollRequest(RollRequest_T *data)
 *   Std_ReturnType Rte_Read_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelUserMemory_WiredLevelUserMemory(WiredLevelUserMemory_T *data)
 *   Std_ReturnType Rte_Read_WiredLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BackToDriveReq_BackToDriveReq(BackToDriveReq_T data)
 *   Std_ReturnType Rte_Write_BlinkECSWiredLEDs_BlinkECSWiredLEDs(FalseTrue_T data)
 *   Std_ReturnType Rte_Write_ECSStandByRequest_ECSStandByRequest(ECSStandByRequest_T data)
 *   Std_ReturnType Rte_Write_ECSStandbyActive_ECSStandbyActive(FalseTrue_T data)
 *   Std_ReturnType Rte_Write_FPBRChangeReq_FPBRChangeReq(FPBRChangeReq_T data)
 *   Std_ReturnType Rte_Write_FPBR_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FerryFunctionRequest_FerryFunctionRequest(Request_T data)
 *   Std_ReturnType Rte_Write_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq(ChangeRequest2Bit_T data)
 *   Std_ReturnType Rte_Write_FerryFunction_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd(InactiveActive_T data)
 *   Std_ReturnType Rte_Write_KneelDeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_KneelingChangeRequest_KneelingChangeRequest(KneelingChangeRequest_T data)
 *   Std_ReturnType Rte_Write_LevelRequest_LevelRequest(const LevelRequest_T *data)
 *   Std_ReturnType Rte_Write_LevelStrokeRequest_LevelStrokeRequest(LevelStrokeRequest_T data)
 *   Std_ReturnType Rte_Write_RampLevelRequest_RampLevelRequest(RampLevelRequest_T data)
 *   Std_ReturnType Rte_Write_RampLevelStorageRequest_RampLevelStorageRequest(RampLevelRequest_T data)
 *   Std_ReturnType Rte_Write_RideHeightFunctionRequest_RideHeightFunctionRequest(RideHeightFunction_T data)
 *   Std_ReturnType Rte_Write_RideHeightStorageRequest_RideHeightStorageRequest(RideHeightStorageRequest_T data)
 *   Std_ReturnType Rte_Write_StopLevelChangeRequest_StopLevelChangeRequest(Request_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_LevelControl_HMICtrl_20ms_runnable_IRV_ECS_StandbyTimer(uint32 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BUO_63_LoadingLevelSw_stuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1CXA_63_FerryFlexibleSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1CXB_63_KneelingFlexibleSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1CXC_63_LoadingLevelAdjSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1DOO_63_FPBRSwitchStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ECSStandByActive_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ECSStandByActive_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LevelControl_HMICtrl_CODE) LevelControl_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  A3PosSwitchStatus_T Read_AlternativeDriveLevelSw_stat_A3PosSwitchStatus;
  BackToDriveReqACK_T Read_BackToDriveReqACK_BackToDriveReqACK;
  ChangeKneelACK_T Read_ChangeKneelACK_ChangeKneelACK;
  ECSStandByReq_T Read_ECSStandByReqRCECS_ECSStandByReqRCECS;
  ECSStandByReq_T Read_ECSStandByReqWRC_ECSStandByReqWRC;
  FalseTrue_T Read_ECSStandbyAllowed_ECSStandbyAllowed;
  ElectricalLoadReduction_rqst_T Read_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst;
  FPBRMMIStat_T Read_FPBRMMIStat_FPBRMMIStat;
  PushButtonStatus_T Read_FPBRSwitchStatus_PushButtonStatus;
  FerryFunctionStatus_T Read_FerryFunctionStatus_FerryFunctionStatus;
  Ack2Bit_T Read_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK;
  A2PosSwitchStatus_T Read_FerryFunctionSwitchStatus_A2PosSwitchStatus;
  FalseTrue_T Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed;
  A2PosSwitchStatus_T Read_KneelSwitchStatus_A2PosSwitchStatus;
  KneelingStatusHMI_T Read_KneelingStatusHMI_KneelingStatusHMI;
  A3PosSwitchStatus_T Read_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus;
  A3PosSwitchStatus_T Read_LoadingLevelSwitchStatus_A3PosSwitchStatus;
  ChangeRequest2Bit_T Read_RampLevelRequestACK_RampLevelRequestACK;
  StorageAck_T Read_RampLevelStorageAck_RampLevelStorageAck;
  StorageAck_T Read_RideHeightStorageAck_RideHeightStorageAck;
  StopLevelChangeStatus_T Read_StopLevelChangeAck_StopLevelChangeStatus;
  VehicleModeDistribution_T Read_SwcActivation_Living_Living;
  VehicleMode_T Read_VehicleModeInternal_VehicleMode;
  FalseTrue_T Read_WRCAirSuspensionStopRequest_AirSuspensionStopRequest;
  LevelAdjustmentAction_T Read_WRCLevelAdjustmentAction_LevelAdjustmentAction;
  LevelAdjustmentAxles_T Read_WRCLevelAdjustmentAxles_LevelAdjustmentAxles;
  LevelAdjustmentStroke_T Read_WRCLevelAdjustmentStroke_LevelAdjustmentStroke;
  LevelUserMemory_T Read_WRCLevelUserMemory_LevelUserMemory;
  LevelUserMemoryAction_T Read_WRCLevelUserMemoryAction_LevelUserMemoryAction;
  RollRequest_T Read_WRCRollRequest_WRCRollRequest;
  FalseTrue_T Read_WiredAirSuspensionStopRequest_AirSuspensionStopRequest;
  LevelAdjustmentAction_T Read_WiredLevelAdjustmentAction_LevelAdjustmentAction;
  LevelAdjustmentAxles_T Read_WiredLevelAdjustmentAxles_LevelAdjustmentAxles;
  LevelAdjustmentStroke_T Read_WiredLevelAdjustmentStroke_LevelAdjustmentStroke;
  WiredLevelUserMemory_T Read_WiredLevelUserMemory_WiredLevelUserMemory;
  LevelUserMemoryAction_T Read_WiredLevelUserMemoryAction_LevelUserMemoryAction;

  SEWS_ECSStandbyActivationTimeout_P1CUA_T P1CUA_ECSStandbyActivationTimeout_v_data;
  SEWS_ECSStandbyExtendedActTimeout_P1CUB_T P1CUB_ECSStandbyExtendedActTimeout_v_data;
  SEWS_ECSActiveStateTimeout_P1CUE_T P1CUE_ECSActiveStateTimeout_v_data;
  SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T P1CUF_LoadingLevelSwStuckedTimeout_v_data;
  SEWS_KneelButtonStuckedTimeout_P1DWD_T P1DWD_KneelButtonStuckedTimeout_v_data;
  SEWS_FerryFuncSwStuckedTimeout_P1EXK_T P1EXK_FerryFuncSwStuckedTimeout_v_data;
  SEWS_ECS_StandbyBlinkTime_P1GCL_T P1GCL_ECS_StandbyBlinkTime_v_data;
  SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T P1IZ2_LoadingLevelAdjSwStuckTimeout_v_data;
  SEWS_FrontSuspensionType_P1JBR_T P1JBR_FrontSuspensionType_v_data;
  SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T P1LXQ_FPBRSwitchStuckedTimeout_v_data;
  SEWS_FPBRSwitchRequestACKTime_P1LXR_T P1LXR_FPBRSwitchRequestACKTime_v_data;
  boolean P1A12_ADL_Sw_v_data;
  boolean P1CT4_FerrySw_Installed_v_data;
  boolean P1CT9_LoadingLevelSw_Installed_v_data;
  boolean P1EXH_KneelingSwitchInstalled_v_data;
  boolean P1IZ1_LoadingLevelAdjSwitchInstalled_v_data;
  boolean P1LXP_FPBRSwitchInstalled_v_data;

  boolean P1ALT_ECS_PartialAirSystem_v_data;
  boolean P1ALU_ECS_FullAirSystem_v_data;
  boolean P1B9X_WirelessRC_Enable_v_data;

  LevelRequest_T Write_LevelRequest_LevelRequest;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1CUA_ECSStandbyActivationTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUA_ECSStandbyActivationTimeout_v();
  P1CUB_ECSStandbyExtendedActTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUB_ECSStandbyExtendedActTimeout_v();
  P1CUE_ECSActiveStateTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUE_ECSActiveStateTimeout_v();
  P1CUF_LoadingLevelSwStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUF_LoadingLevelSwStuckedTimeout_v();
  P1DWD_KneelButtonStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1DWD_KneelButtonStuckedTimeout_v();
  P1EXK_FerryFuncSwStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1EXK_FerryFuncSwStuckedTimeout_v();
  P1GCL_ECS_StandbyBlinkTime_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1GCL_ECS_StandbyBlinkTime_v();
  P1IZ2_LoadingLevelAdjSwStuckTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1IZ2_LoadingLevelAdjSwStuckTimeout_v();
  P1JBR_FrontSuspensionType_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1JBR_FrontSuspensionType_v();
  P1LXQ_FPBRSwitchStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1LXQ_FPBRSwitchStuckedTimeout_v();
  P1LXR_FPBRSwitchRequestACKTime_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1LXR_FPBRSwitchRequestACKTime_v();
  P1A12_ADL_Sw_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1A12_ADL_Sw_v();
  P1CT4_FerrySw_Installed_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CT4_FerrySw_Installed_v();
  P1CT9_LoadingLevelSw_Installed_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CT9_LoadingLevelSw_Installed_v();
  P1EXH_KneelingSwitchInstalled_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1EXH_KneelingSwitchInstalled_v();
  P1IZ1_LoadingLevelAdjSwitchInstalled_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1IZ1_LoadingLevelAdjSwitchInstalled_v();
  P1LXP_FPBRSwitchInstalled_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1LXP_FPBRSwitchInstalled_v();

  P1ALT_ECS_PartialAirSystem_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1ALT_ECS_PartialAirSystem_v();
  P1ALU_ECS_FullAirSystem_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1ALU_ECS_FullAirSystem_v();
  P1B9X_WirelessRC_Enable_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v();

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(&Read_AlternativeDriveLevelSw_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_BackToDriveReqACK_BackToDriveReqACK(&Read_BackToDriveReqACK_BackToDriveReqACK);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_ChangeKneelACK_ChangeKneelACK(&Read_ChangeKneelACK_ChangeKneelACK);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_ECSStandByReqRCECS_ECSStandByReqRCECS(&Read_ECSStandByReqRCECS_ECSStandByReqRCECS);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_ECSStandByReqWRC_ECSStandByReqWRC(&Read_ECSStandByReqWRC_ECSStandByReqWRC);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_ECSStandbyAllowed_ECSStandbyAllowed(&Read_ECSStandbyAllowed_ECSStandbyAllowed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst(&Read_ElectricalLoadReduction_rqst_ElectricalLoadReduction_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_FPBRMMIStat_FPBRMMIStat(&Read_FPBRMMIStat_FPBRMMIStat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_FPBRSwitchStatus_PushButtonStatus(&Read_FPBRSwitchStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_FerryFunctionStatus_FerryFunctionStatus(&Read_FerryFunctionStatus_FerryFunctionStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK(&Read_FerryFunctionSwitchChangeACK_FerryFunctionSwitchChangeACK);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_FerryFunctionSwitchStatus_A2PosSwitchStatus(&Read_FerryFunctionSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(&Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_KneelSwitchStatus_A2PosSwitchStatus(&Read_KneelSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_KneelingStatusHMI_KneelingStatusHMI(&Read_KneelingStatusHMI_KneelingStatusHMI);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(&Read_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_LoadingLevelSwitchStatus_A3PosSwitchStatus(&Read_LoadingLevelSwitchStatus_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_RampLevelRequestACK_RampLevelRequestACK(&Read_RampLevelRequestACK_RampLevelRequestACK);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_RampLevelStorageAck_RampLevelStorageAck(&Read_RampLevelStorageAck_RampLevelStorageAck);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_RideHeightStorageAck_RideHeightStorageAck(&Read_RideHeightStorageAck_RideHeightStorageAck);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_StopLevelChangeAck_StopLevelChangeStatus(&Read_StopLevelChangeAck_StopLevelChangeStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_SwcActivation_Living_Living(&Read_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WRCAirSuspensionStopRequest_AirSuspensionStopRequest(&Read_WRCAirSuspensionStopRequest_AirSuspensionStopRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelAdjustmentAction_LevelAdjustmentAction(&Read_WRCLevelAdjustmentAction_LevelAdjustmentAction);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelAdjustmentAxles_LevelAdjustmentAxles(&Read_WRCLevelAdjustmentAxles_LevelAdjustmentAxles);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelAdjustmentStroke_LevelAdjustmentStroke(&Read_WRCLevelAdjustmentStroke_LevelAdjustmentStroke);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelUserMemory_LevelUserMemory(&Read_WRCLevelUserMemory_LevelUserMemory);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WRCLevelUserMemoryAction_LevelUserMemoryAction(&Read_WRCLevelUserMemoryAction_LevelUserMemoryAction);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WRCRollRequest_WRCRollRequest(&Read_WRCRollRequest_WRCRollRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(&Read_WiredAirSuspensionStopRequest_AirSuspensionStopRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelAdjustmentAction_LevelAdjustmentAction(&Read_WiredLevelAdjustmentAction_LevelAdjustmentAction);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(&Read_WiredLevelAdjustmentAxles_LevelAdjustmentAxles);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(&Read_WiredLevelAdjustmentStroke_LevelAdjustmentStroke);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelUserMemory_WiredLevelUserMemory(&Read_WiredLevelUserMemory_WiredLevelUserMemory);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Read_WiredLevelUserMemoryAction_LevelUserMemoryAction(&Read_WiredLevelUserMemoryAction_LevelUserMemoryAction);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_BackToDriveReq_BackToDriveReq(Rte_InitValue_BackToDriveReq_BackToDriveReq);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_BlinkECSWiredLEDs_BlinkECSWiredLEDs(Rte_InitValue_BlinkECSWiredLEDs_BlinkECSWiredLEDs);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_ECSStandByRequest_ECSStandByRequest(Rte_InitValue_ECSStandByRequest_ECSStandByRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_ECSStandbyActive_ECSStandbyActive(Rte_InitValue_ECSStandbyActive_ECSStandbyActive);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_FPBRChangeReq_FPBRChangeReq(Rte_InitValue_FPBRChangeReq_FPBRChangeReq);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_FPBR_DeviceIndication_DeviceIndication(Rte_InitValue_FPBR_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_FerryFunctionRequest_FerryFunctionRequest(Rte_InitValue_FerryFunctionRequest_FerryFunctionRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq(Rte_InitValue_FerryFunctionSwitchChangeReq_FerryFunctionSwitchChangeReq);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_FerryFunction_DeviceIndication_DeviceIndication(Rte_InitValue_FerryFunction_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd(Rte_InitValue_InhibitWRCECSMenuCmd_InhibitWRCECSMenuCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_KneelDeviceIndication_DeviceIndication(Rte_InitValue_KneelDeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_KneelingChangeRequest_KneelingChangeRequest(Rte_InitValue_KneelingChangeRequest_KneelingChangeRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_LevelRequest_LevelRequest, 0, sizeof(Write_LevelRequest_LevelRequest));
  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_LevelRequest_LevelRequest(&Write_LevelRequest_LevelRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_LevelStrokeRequest_LevelStrokeRequest(Rte_InitValue_LevelStrokeRequest_LevelStrokeRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_RampLevelRequest_RampLevelRequest(Rte_InitValue_RampLevelRequest_RampLevelRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_RampLevelStorageRequest_RampLevelStorageRequest(Rte_InitValue_RampLevelStorageRequest_RampLevelStorageRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_RideHeightFunctionRequest_RideHeightFunctionRequest(Rte_InitValue_RideHeightFunctionRequest_RideHeightFunctionRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_RideHeightStorageRequest_RideHeightStorageRequest(Rte_InitValue_RideHeightStorageRequest_RideHeightStorageRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Write_StopLevelChangeRequest_StopLevelChangeRequest(Rte_InitValue_StopLevelChangeRequest_StopLevelChangeRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  TSC_LevelControl_HMICtrl_Rte_IrvWrite_LevelControl_HMICtrl_20ms_runnable_IRV_ECS_StandbyTimer(0U);

  fct_status = TSC_LevelControl_HMICtrl_Rte_Call_Event_D1BUO_63_LoadingLevelSw_stuck_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Call_Event_D1CXA_63_FerryFlexibleSwitchStuck_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Call_Event_D1CXB_63_KneelingFlexibleSwitchStuck_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Call_Event_D1CXC_63_LoadingLevelAdjSwitchStuck_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Call_Event_D1DOO_63_FPBRSwitchStuck_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Call_UR_ANW_ECSStandByActive_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LevelControl_HMICtrl_Rte_Call_UR_ANW_ECSStandByActive_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LevelControl_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LevelControl_HMICtrl_CODE) LevelControl_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LevelControl_HMICtrl_Init
 *********************************************************************************************************************/

  SEWS_ECSStandbyActivationTimeout_P1CUA_T P1CUA_ECSStandbyActivationTimeout_v_data;
  SEWS_ECSStandbyExtendedActTimeout_P1CUB_T P1CUB_ECSStandbyExtendedActTimeout_v_data;
  SEWS_ECSActiveStateTimeout_P1CUE_T P1CUE_ECSActiveStateTimeout_v_data;
  SEWS_LoadingLevelSwStuckedTimeout_P1CUF_T P1CUF_LoadingLevelSwStuckedTimeout_v_data;
  SEWS_KneelButtonStuckedTimeout_P1DWD_T P1DWD_KneelButtonStuckedTimeout_v_data;
  SEWS_FerryFuncSwStuckedTimeout_P1EXK_T P1EXK_FerryFuncSwStuckedTimeout_v_data;
  SEWS_ECS_StandbyBlinkTime_P1GCL_T P1GCL_ECS_StandbyBlinkTime_v_data;
  SEWS_LoadingLevelAdjSwStuckTimeout_P1IZ2_T P1IZ2_LoadingLevelAdjSwStuckTimeout_v_data;
  SEWS_FrontSuspensionType_P1JBR_T P1JBR_FrontSuspensionType_v_data;
  SEWS_FPBRSwitchStuckedTimeout_P1LXQ_T P1LXQ_FPBRSwitchStuckedTimeout_v_data;
  SEWS_FPBRSwitchRequestACKTime_P1LXR_T P1LXR_FPBRSwitchRequestACKTime_v_data;
  boolean P1A12_ADL_Sw_v_data;
  boolean P1CT4_FerrySw_Installed_v_data;
  boolean P1CT9_LoadingLevelSw_Installed_v_data;
  boolean P1EXH_KneelingSwitchInstalled_v_data;
  boolean P1IZ1_LoadingLevelAdjSwitchInstalled_v_data;
  boolean P1LXP_FPBRSwitchInstalled_v_data;

  boolean P1ALT_ECS_PartialAirSystem_v_data;
  boolean P1ALU_ECS_FullAirSystem_v_data;
  boolean P1B9X_WirelessRC_Enable_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1CUA_ECSStandbyActivationTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUA_ECSStandbyActivationTimeout_v();
  P1CUB_ECSStandbyExtendedActTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUB_ECSStandbyExtendedActTimeout_v();
  P1CUE_ECSActiveStateTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUE_ECSActiveStateTimeout_v();
  P1CUF_LoadingLevelSwStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CUF_LoadingLevelSwStuckedTimeout_v();
  P1DWD_KneelButtonStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1DWD_KneelButtonStuckedTimeout_v();
  P1EXK_FerryFuncSwStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1EXK_FerryFuncSwStuckedTimeout_v();
  P1GCL_ECS_StandbyBlinkTime_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1GCL_ECS_StandbyBlinkTime_v();
  P1IZ2_LoadingLevelAdjSwStuckTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1IZ2_LoadingLevelAdjSwStuckTimeout_v();
  P1JBR_FrontSuspensionType_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1JBR_FrontSuspensionType_v();
  P1LXQ_FPBRSwitchStuckedTimeout_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1LXQ_FPBRSwitchStuckedTimeout_v();
  P1LXR_FPBRSwitchRequestACKTime_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1LXR_FPBRSwitchRequestACKTime_v();
  P1A12_ADL_Sw_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1A12_ADL_Sw_v();
  P1CT4_FerrySw_Installed_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CT4_FerrySw_Installed_v();
  P1CT9_LoadingLevelSw_Installed_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1CT9_LoadingLevelSw_Installed_v();
  P1EXH_KneelingSwitchInstalled_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1EXH_KneelingSwitchInstalled_v();
  P1IZ1_LoadingLevelAdjSwitchInstalled_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1IZ1_LoadingLevelAdjSwitchInstalled_v();
  P1LXP_FPBRSwitchInstalled_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1LXP_FPBRSwitchInstalled_v();

  P1ALT_ECS_PartialAirSystem_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1ALT_ECS_PartialAirSystem_v();
  P1ALU_ECS_FullAirSystem_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1ALU_ECS_FullAirSystem_v();
  P1B9X_WirelessRC_Enable_v_data = TSC_LevelControl_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define LevelControl_HMICtrl_STOP_SEC_CODE
#include "LevelControl_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void LevelControl_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_1 = A3PosSwitchStatus_Middle;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_2 = A3PosSwitchStatus_Lower;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_3 = A3PosSwitchStatus_Upper;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_4 = A3PosSwitchStatus_Spare;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_5 = A3PosSwitchStatus_Spare_01;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_6 = A3PosSwitchStatus_Spare_02;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_7 = A3PosSwitchStatus_Error;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_8 = A3PosSwitchStatus_NotAvailable;

  Ack2Bit_T Test_Ack2Bit_T_V_1 = Ack2Bit_NoAction;
  Ack2Bit_T Test_Ack2Bit_T_V_2 = Ack2Bit_ChangeAcknowledged;
  Ack2Bit_T Test_Ack2Bit_T_V_3 = Ack2Bit_Error;
  Ack2Bit_T Test_Ack2Bit_T_V_4 = Ack2Bit_NotAvailable;

  BackToDriveReqACK_T Test_BackToDriveReqACK_T_V_1 = BackToDriveReqACK_TakNoAction;
  BackToDriveReqACK_T Test_BackToDriveReqACK_T_V_2 = BackToDriveReqACK_ChangeAcknowledged;
  BackToDriveReqACK_T Test_BackToDriveReqACK_T_V_3 = BackToDriveReqACK_Error;
  BackToDriveReqACK_T Test_BackToDriveReqACK_T_V_4 = BackToDriveReqACK_NotAvailable;

  BackToDriveReq_T Test_BackToDriveReq_T_V_1 = BackToDriveReq_Idle;
  BackToDriveReq_T Test_BackToDriveReq_T_V_2 = BackToDriveReq_B2DRequested;
  BackToDriveReq_T Test_BackToDriveReq_T_V_3 = Requested_Error;
  BackToDriveReq_T Test_BackToDriveReq_T_V_4 = Requested_NotAvailable;

  ChangeKneelACK_T Test_ChangeKneelACK_T_V_1 = ChangeKneelACK_NoAction;
  ChangeKneelACK_T Test_ChangeKneelACK_T_V_2 = ChangeKneelACK_ChangeAcknowledged;
  ChangeKneelACK_T Test_ChangeKneelACK_T_V_3 = ChangeKneelACK_Error;
  ChangeKneelACK_T Test_ChangeKneelACK_T_V_4 = ChangeKneelACK_NotAvailable;

  ChangeRequest2Bit_T Test_ChangeRequest2Bit_T_V_1 = ChangeRequest2Bit_TakeNoAction;
  ChangeRequest2Bit_T Test_ChangeRequest2Bit_T_V_2 = ChangeRequest2Bit_Change;
  ChangeRequest2Bit_T Test_ChangeRequest2Bit_T_V_3 = ChangeRequest2Bit_Error;
  ChangeRequest2Bit_T Test_ChangeRequest2Bit_T_V_4 = ChangeRequest2Bit_NotAvailable;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  ECSStandByReq_T Test_ECSStandByReq_T_V_1 = ECSStandByReq_Idle;
  ECSStandByReq_T Test_ECSStandByReq_T_V_2 = ECSStandByReq_StandbyRequested;
  ECSStandByReq_T Test_ECSStandByReq_T_V_3 = ECSStandByReq_StopStandby;
  ECSStandByReq_T Test_ECSStandByReq_T_V_4 = ECSStandByReq_Reserved;
  ECSStandByReq_T Test_ECSStandByReq_T_V_5 = ECSStandByReq_Reserved_01;
  ECSStandByReq_T Test_ECSStandByReq_T_V_6 = ECSStandByReq_Reserved_02;
  ECSStandByReq_T Test_ECSStandByReq_T_V_7 = ECSStandByReq_Error;
  ECSStandByReq_T Test_ECSStandByReq_T_V_8 = ECSStandByReq_NotAvailable;

  ECSStandByRequest_T Test_ECSStandByRequest_T_V_1 = ECSStandByRequest_NoRequest;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_2 = ECSStandByRequest_Initiate;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_3 = ECSStandByRequest_StandbyRequestedRCECS;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_4 = ECSStandByRequest_StandbyRequestedWRC;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_5 = ECSStandByRequest_Reserved;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_6 = ECSStandByRequest_Reserved_01;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_7 = ECSStandByRequest_Error;
  ECSStandByRequest_T Test_ECSStandByRequest_T_V_8 = ECSStandByRequest_NotAvailable;

  ElectricalLoadReduction_rqst_T Test_ElectricalLoadReduction_rqst_T_V_1 = ElectricalLoadReduction_rqst_NoRequest;
  ElectricalLoadReduction_rqst_T Test_ElectricalLoadReduction_rqst_T_V_2 = ElectricalLoadReduction_rqst_Level1Request;
  ElectricalLoadReduction_rqst_T Test_ElectricalLoadReduction_rqst_T_V_3 = ElectricalLoadReduction_rqst_Level2Request;
  ElectricalLoadReduction_rqst_T Test_ElectricalLoadReduction_rqst_T_V_4 = ElectricalLoadReduction_rqst_SpareValue;
  ElectricalLoadReduction_rqst_T Test_ElectricalLoadReduction_rqst_T_V_5 = ElectricalLoadReduction_rqst_SpareValue_01;
  ElectricalLoadReduction_rqst_T Test_ElectricalLoadReduction_rqst_T_V_6 = ElectricalLoadReduction_rqst_SpareValue_02;
  ElectricalLoadReduction_rqst_T Test_ElectricalLoadReduction_rqst_T_V_7 = ElectricalLoadReduction_rqst_Error;
  ElectricalLoadReduction_rqst_T Test_ElectricalLoadReduction_rqst_T_V_8 = ElectricalLoadReduction_rqst_NotAvailable;

  FPBRChangeReq_T Test_FPBRChangeReq_T_V_1 = FPBRChangeReq_TakeNoAction;
  FPBRChangeReq_T Test_FPBRChangeReq_T_V_2 = FPBRChangeReq_ChangeFPBRFunction;
  FPBRChangeReq_T Test_FPBRChangeReq_T_V_3 = FPBRChangeReq_Error;
  FPBRChangeReq_T Test_FPBRChangeReq_T_V_4 = FPBRChangeReq_NotAvailable;

  FPBRStatusInd_T Test_FPBRStatusInd_T_V_1 = FPBRStatusInd_Off;
  FPBRStatusInd_T Test_FPBRStatusInd_T_V_2 = FPBRStatusInd_On;
  FPBRStatusInd_T Test_FPBRStatusInd_T_V_3 = FPBRStatusInd_Changing;
  FPBRStatusInd_T Test_FPBRStatusInd_T_V_4 = FPBRStatusInd_NotAvailable;

  FalseTrue_T Test_FalseTrue_T_V_1 = FalseTrue_False;
  FalseTrue_T Test_FalseTrue_T_V_2 = FalseTrue_True;
  FalseTrue_T Test_FalseTrue_T_V_3 = FalseTrue_Error;
  FalseTrue_T Test_FalseTrue_T_V_4 = FalseTrue_NotAvaiable;

  FerryFunctionStatus_T Test_FerryFunctionStatus_T_V_1 = FerryFunctionStatus_FerryFunctionNotActive;
  FerryFunctionStatus_T Test_FerryFunctionStatus_T_V_2 = FerryFunctionStatus_FerryFunctionActiveLevelReached;
  FerryFunctionStatus_T Test_FerryFunctionStatus_T_V_3 = FerryFunctionStatus_FerryFunctionActiveLeveNotlReached;
  FerryFunctionStatus_T Test_FerryFunctionStatus_T_V_4 = FerryFunctionStatus_Spare_01;
  FerryFunctionStatus_T Test_FerryFunctionStatus_T_V_5 = FerryFunctionStatus_Spare_02;
  FerryFunctionStatus_T Test_FerryFunctionStatus_T_V_6 = FerryFunctionStatus_Spare_03;
  FerryFunctionStatus_T Test_FerryFunctionStatus_T_V_7 = FerryFunctionStatus_Error;
  FerryFunctionStatus_T Test_FerryFunctionStatus_T_V_8 = FerryFunctionStatus_NotAvailable;

  InactiveActive_T Test_InactiveActive_T_V_1 = InactiveActive_Inactive;
  InactiveActive_T Test_InactiveActive_T_V_2 = InactiveActive_Active;
  InactiveActive_T Test_InactiveActive_T_V_3 = InactiveActive_Error;
  InactiveActive_T Test_InactiveActive_T_V_4 = InactiveActive_NotAvailable;

  KneelingChangeRequest_T Test_KneelingChangeRequest_T_V_1 = KneelingChangeRequest_TakeNoAction;
  KneelingChangeRequest_T Test_KneelingChangeRequest_T_V_2 = KneelingChangeRequest_ChangeKneelFunction;
  KneelingChangeRequest_T Test_KneelingChangeRequest_T_V_3 = KneelingChangeRequest_Error;
  KneelingChangeRequest_T Test_KneelingChangeRequest_T_V_4 = KneelingChangeRequest_NotAvailable;

  KneelingStatusHMI_T Test_KneelingStatusHMI_T_V_1 = KneelingStatusHMI_Inactive;
  KneelingStatusHMI_T Test_KneelingStatusHMI_T_V_2 = KneelingStatusHMI_Active;
  KneelingStatusHMI_T Test_KneelingStatusHMI_T_V_3 = KneelingStatusHMI_Error;
  KneelingStatusHMI_T Test_KneelingStatusHMI_T_V_4 = KneelingStatusHMI_NotAvailable;

  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_1 = LevelAdjustmentAction_Idle;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_2 = LevelAdjustmentAction_UpBasic;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_3 = LevelAdjustmentAction_DownBasic;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_4 = LevelAdjustmentAction_UpShortMovement;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_5 = LevelAdjustmentAction_DownShortMovement;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_6 = LevelAdjustmentAction_Reserved;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_7 = LevelAdjustmentAction_GotoDriveLevel;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_8 = LevelAdjustmentAction_Reserved_01;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_9 = LevelAdjustmentAction_Ferry;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_10 = LevelAdjustmentAction_Reserved_02;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_11 = LevelAdjustmentAction_Reserved_03;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_12 = LevelAdjustmentAction_Reserved_04;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_13 = LevelAdjustmentAction_Reserved_05;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_14 = LevelAdjustmentAction_Reserved_06;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_15 = LevelAdjustmentAction_Error;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_16 = LevelAdjustmentAction_NotAvailable;

  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_1 = LevelAdjustmentAxles_Rear;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_2 = LevelAdjustmentAxles_Front;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_3 = LevelAdjustmentAxles_Parallel;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_4 = LevelAdjustmentAxles_Reserved;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_5 = LevelAdjustmentAxles_Reserved_01;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_6 = LevelAdjustmentAxles_Reserved_02;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_7 = LevelAdjustmentAxles_Error;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_8 = LevelAdjustmentAxles_NotAvailable;

  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_1 = LevelAdjustmentStroke_DriveStroke;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_2 = LevelAdjustmentStroke_DockingStroke;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_3 = LevelAdjustmentStroke_Reserved;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_4 = LevelAdjustmentStroke_Reserved_01;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_5 = LevelAdjustmentStroke_Reserved_02;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_6 = LevelAdjustmentStroke_Reserved_03;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_7 = LevelAdjustmentStroke_Error;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_8 = LevelAdjustmentStroke_NotAvailable;

  LevelChangeRequest_T Test_LevelChangeRequest_T_V_1 = LevelChangeRequest_TakeNoAction;
  LevelChangeRequest_T Test_LevelChangeRequest_T_V_2 = LevelChangeRequest_VehicleBodyUpLifting;
  LevelChangeRequest_T Test_LevelChangeRequest_T_V_3 = LevelChangeRequest_VehicleBodyDownLowering;
  LevelChangeRequest_T Test_LevelChangeRequest_T_V_4 = LevelChangeRequest_VehicleBodyUpMinimumMovementLifting;
  LevelChangeRequest_T Test_LevelChangeRequest_T_V_5 = LevelChangeRequest_VehicleBodyDownMinimumMovementLowering;
  LevelChangeRequest_T Test_LevelChangeRequest_T_V_6 = LevelChangeRequest_NotDefined;
  LevelChangeRequest_T Test_LevelChangeRequest_T_V_7 = LevelChangeRequest_ErrorIndicator;
  LevelChangeRequest_T Test_LevelChangeRequest_T_V_8 = LevelChangeRequest_NotAvailable;

  LevelStrokeRequest_T Test_LevelStrokeRequest_T_V_1 = LevelStrokeRequest_DockingLevelControlStroke;
  LevelStrokeRequest_T Test_LevelStrokeRequest_T_V_2 = LevelStrokeRequest_DriveLevelControlStroke;
  LevelStrokeRequest_T Test_LevelStrokeRequest_T_V_3 = LevelStrokeRequest_ErrorIndicator;
  LevelStrokeRequest_T Test_LevelStrokeRequest_T_V_4 = LevelStrokeRequest_NotAvailable;

  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_1 = LevelUserMemoryAction_Inactive;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_2 = LevelUserMemoryAction_Recall;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_3 = LevelUserMemoryAction_Store;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_4 = LevelUserMemoryAction_Default;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_5 = LevelUserMemoryAction_Reserved;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_6 = LevelUserMemoryAction_Reserved_01;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_7 = LevelUserMemoryAction_Error;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_8 = LevelUserMemoryAction_NotAvailable;

  LevelUserMemory_T Test_LevelUserMemory_T_V_1 = LevelUserMemory_NotUsed;
  LevelUserMemory_T Test_LevelUserMemory_T_V_2 = LevelUserMemory_M1;
  LevelUserMemory_T Test_LevelUserMemory_T_V_3 = LevelUserMemory_M2;
  LevelUserMemory_T Test_LevelUserMemory_T_V_4 = LevelUserMemory_M3;
  LevelUserMemory_T Test_LevelUserMemory_T_V_5 = LevelUserMemory_M4;
  LevelUserMemory_T Test_LevelUserMemory_T_V_6 = LevelUserMemory_M5;
  LevelUserMemory_T Test_LevelUserMemory_T_V_7 = LevelUserMemory_M6;
  LevelUserMemory_T Test_LevelUserMemory_T_V_8 = LevelUserMemory_M7;
  LevelUserMemory_T Test_LevelUserMemory_T_V_9 = LevelUserMemory_Spare;
  LevelUserMemory_T Test_LevelUserMemory_T_V_10 = LevelUserMemory_Spare01;
  LevelUserMemory_T Test_LevelUserMemory_T_V_11 = LevelUserMemory_Spare02;
  LevelUserMemory_T Test_LevelUserMemory_T_V_12 = LevelUserMemory_Spare03;
  LevelUserMemory_T Test_LevelUserMemory_T_V_13 = LevelUserMemory_Spare04;
  LevelUserMemory_T Test_LevelUserMemory_T_V_14 = LevelUserMemory_Spare05;
  LevelUserMemory_T Test_LevelUserMemory_T_V_15 = LevelUserMemory_Error;
  LevelUserMemory_T Test_LevelUserMemory_T_V_16 = LevelUserMemory_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  RampLevelRequest_T Test_RampLevelRequest_T_V_1 = RampLevelRequest_TakeNoAction;
  RampLevelRequest_T Test_RampLevelRequest_T_V_2 = RampLevelRequest_RampLevelM1;
  RampLevelRequest_T Test_RampLevelRequest_T_V_3 = RampLevelRequest_RampLevelM2;
  RampLevelRequest_T Test_RampLevelRequest_T_V_4 = RampLevelRequest_RampLevelM3;
  RampLevelRequest_T Test_RampLevelRequest_T_V_5 = RampLevelRequest_RampLevelM4;
  RampLevelRequest_T Test_RampLevelRequest_T_V_6 = RampLevelRequest_RampLevelM5;
  RampLevelRequest_T Test_RampLevelRequest_T_V_7 = RampLevelRequest_RampLevelM6;
  RampLevelRequest_T Test_RampLevelRequest_T_V_8 = RampLevelRequest_RampLevelM7;
  RampLevelRequest_T Test_RampLevelRequest_T_V_9 = RampLevelRequest_NotDefined_04;
  RampLevelRequest_T Test_RampLevelRequest_T_V_10 = RampLevelRequest_NotDefined_05;
  RampLevelRequest_T Test_RampLevelRequest_T_V_11 = RampLevelRequest_NotDefined_06;
  RampLevelRequest_T Test_RampLevelRequest_T_V_12 = RampLevelRequest_NotDefined_07;
  RampLevelRequest_T Test_RampLevelRequest_T_V_13 = RampLevelRequest_NotDefined_08;
  RampLevelRequest_T Test_RampLevelRequest_T_V_14 = RampLevelRequest_NotDefined_09;
  RampLevelRequest_T Test_RampLevelRequest_T_V_15 = RampLevelRequest_ErrorIndicator;
  RampLevelRequest_T Test_RampLevelRequest_T_V_16 = RampLevelRequest_NotAvailable;

  Request_T Test_Request_T_V_1 = Request_NotRequested;
  Request_T Test_Request_T_V_2 = Request_RequestActive;
  Request_T Test_Request_T_V_3 = Request_Error;
  Request_T Test_Request_T_V_4 = Request_NotAvailable;

  RideHeightFunction_T Test_RideHeightFunction_T_V_1 = RideHeightFunction_Inactive;
  RideHeightFunction_T Test_RideHeightFunction_T_V_2 = RideHeightFunction_StandardDrivePosition;
  RideHeightFunction_T Test_RideHeightFunction_T_V_3 = RideHeightFunction_AlternativeDrivePosition;
  RideHeightFunction_T Test_RideHeightFunction_T_V_4 = RideHeightFunction_SpeedDependentDrivePosition;
  RideHeightFunction_T Test_RideHeightFunction_T_V_5 = RideHeightFunction_HighDrivePosition;
  RideHeightFunction_T Test_RideHeightFunction_T_V_6 = RideHeightFunction_LowDrivePosition;
  RideHeightFunction_T Test_RideHeightFunction_T_V_7 = RideHeightFunction_AlternativeDrivePosition1;
  RideHeightFunction_T Test_RideHeightFunction_T_V_8 = RideHeightFunction_AlternativeDrivePosition2;
  RideHeightFunction_T Test_RideHeightFunction_T_V_9 = RideHeightFunction_NotDefined;
  RideHeightFunction_T Test_RideHeightFunction_T_V_10 = RideHeightFunction_NotDefined_01;
  RideHeightFunction_T Test_RideHeightFunction_T_V_11 = RideHeightFunction_NotDefined_02;
  RideHeightFunction_T Test_RideHeightFunction_T_V_12 = RideHeightFunction_NotDefined_03;
  RideHeightFunction_T Test_RideHeightFunction_T_V_13 = RideHeightFunction_NotDefined_04;
  RideHeightFunction_T Test_RideHeightFunction_T_V_14 = RideHeightFunction_NotDefined_05;
  RideHeightFunction_T Test_RideHeightFunction_T_V_15 = RideHeightFunction_ErrorIndicator;
  RideHeightFunction_T Test_RideHeightFunction_T_V_16 = RideHeightFunction_NotAvailable;

  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_1 = RideHeightStorageRequest_TakeNoAction;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_2 = RideHeightStorageRequest_ResetToStoreFactoryDrivePosition;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_3 = RideHeightStorageRequest_StoreStandardUserDrivePosition;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_4 = RideHeightStorageRequest_StoreLowDrivePosition;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_5 = RideHeightStorageRequest_StoreHighDrivePosition;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_6 = RideHeightStorageRequest_ResetLowDrivePosition;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_7 = RideHeightStorageRequest_ResetHighDrivePosition;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_8 = RideHeightStorageRequest_NotDefined;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_9 = RideHeightStorageRequest_NotDefined_01;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_10 = RideHeightStorageRequest_NotDefined_02;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_11 = RideHeightStorageRequest_NotDefined_03;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_12 = RideHeightStorageRequest_NotDefined_04;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_13 = RideHeightStorageRequest_NotDefined_05;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_14 = RideHeightStorageRequest_NotDefined_06;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_15 = RideHeightStorageRequest_ErrorIndicator;
  RideHeightStorageRequest_T Test_RideHeightStorageRequest_T_V_16 = RideHeightStorageRequest_NotAvailable;

  RollRequest_T Test_RollRequest_T_V_1 = RollRequest_Idle;
  RollRequest_T Test_RollRequest_T_V_2 = RollRequest_Clockwise;
  RollRequest_T Test_RollRequest_T_V_3 = RollRequest_CounterClockwise;
  RollRequest_T Test_RollRequest_T_V_4 = RollRequest_ClockwiseShort;
  RollRequest_T Test_RollRequest_T_V_5 = RollRequest_CounterClockwiseShort;
  RollRequest_T Test_RollRequest_T_V_6 = RollRequest_Reserved;
  RollRequest_T Test_RollRequest_T_V_7 = RollRequest_Error;
  RollRequest_T Test_RollRequest_T_V_8 = RollRequest_NotAvailable;

  StopLevelChangeStatus_T Test_StopLevelChangeStatus_T_V_1 = StopLevelChangeStatus_NoStopRequest;
  StopLevelChangeStatus_T Test_StopLevelChangeStatus_T_V_2 = StopLevelChangeStatus_LevelChangedStopped;
  StopLevelChangeStatus_T Test_StopLevelChangeStatus_T_V_3 = StopLevelChangeStatus_ErrorIndicator;
  StopLevelChangeStatus_T Test_StopLevelChangeStatus_T_V_4 = StopLevelChangeStatus_NotAvailable;

  StorageAck_T Test_StorageAck_T_V_1 = StorageAck_TakeNoAction;
  StorageAck_T Test_StorageAck_T_V_2 = StorageAck_ChangeAcknowledged;
  StorageAck_T Test_StorageAck_T_V_3 = StorageAck_Error;
  StorageAck_T Test_StorageAck_T_V_4 = StorageAck_NotAvaiable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;

  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_1 = WiredLevelUserMemory_MemOff;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_2 = WiredLevelUserMemory_M1;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_3 = WiredLevelUserMemory_M2;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_4 = WiredLevelUserMemory_M3;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_5 = WiredLevelUserMemory_M4;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_6 = WiredLevelUserMemory_M5;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_7 = WiredLevelUserMemory_M6;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_8 = WiredLevelUserMemory_M7;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_9 = WiredLevelUserMemory_Spare;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_10 = WiredLevelUserMemory_Spare01;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_11 = WiredLevelUserMemory_Spare02;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_12 = WiredLevelUserMemory_Spare03;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_13 = WiredLevelUserMemory_Spare04;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_14 = WiredLevelUserMemory_Spare05;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_15 = WiredLevelUserMemory_Error;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_16 = WiredLevelUserMemory_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
