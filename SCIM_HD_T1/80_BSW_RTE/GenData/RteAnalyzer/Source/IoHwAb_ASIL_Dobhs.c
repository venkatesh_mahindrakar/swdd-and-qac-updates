/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  IoHwAb_ASIL_Dobhs.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  IoHwAb_ASIL_Dobhs
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <IoHwAb_ASIL_Dobhs>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IOHWAB_BOOL
 *   
 *
 * IOHWAB_UINT8
 *   
 *
 * Rte_DT_EcuHwDioCtrlArray_T_0
 *   
 *
 * Rte_DT_EcuHwFaultValues_T_0
 *   
 *
 * SEWS_HwToleranceThreshold_X1C04_T
 *   
 *
 * SEWS_PcbConfig_DOBHS_X1CXX_T
 *   
 *
 * SEWS_X1CY1_DigitalBiLevelHigh_T
 *   
 *
 * SEWS_X1CY1_DigitalBiLevelLow_T
 *   
 *
 * VGTT_EcuPinFaultStatus
 *   
 *
 * VGTT_EcuPinVoltage_0V2
 *   
 *
 *********************************************************************************************************************/

#include "Rte_IoHwAb_ASIL_Dobhs.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_IoHwAb_ASIL_Dobhs.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void IoHwAb_ASIL_Dobhs_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_BOOL: Boolean
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_HwToleranceThreshold_X1C04_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * Rte_DT_EcuHwDioCtrlArray_T_0: Enumeration of integer in interval [0...255] with enumerators
 *   STD_LOW (0U)
 *   STD_HIGH (1U)
 *   Inactive (255U)
 * Rte_DT_EcuHwFaultValues_T_0: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * SEWS_PcbConfig_DOBHS_X1CXX_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_Populated (1U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 *
 * Array Types:
 * ============
 * EcuHwDioCtrlArray_T: Array with 40 element(s) of type Rte_DT_EcuHwDioCtrlArray_T_0
 * EcuHwFaultValues_T: Array with 40 element(s) of type Rte_DT_EcuHwFaultValues_T_0
 * EcuHwVoltageValues_T: Array with 40 element(s) of type VGTT_EcuPinVoltage_0V2
 * SEWS_PcbConfig_DOBHS_X1CXX_a_T: Array with 4 element(s) of type SEWS_PcbConfig_DOBHS_X1CXX_T
 *
 * Record Types:
 * =============
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HwToleranceThreshold_X1C04_T Rte_Prm_X1C04_HwToleranceThreshold_v(void)
 *   SEWS_PcbConfig_DOBHS_X1CXX_T *Rte_Prm_X1CXX_PcbConfig_DOBHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOBHS_X1CXX_T* is of type SEWS_PcbConfig_DOBHS_X1CXX_a_T
 *   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
 *
 *********************************************************************************************************************/


#define IoHwAb_ASIL_Dobhs_START_SEC_CODE
#include "IoHwAb_ASIL_Dobhs_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_1_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsCtrlInterface_P_1>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_1_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_1_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_1_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_1_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  EcuHwFaultValues_T DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus;
  EcuHwDioCtrlArray_T DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus);
  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray);

  IoHwAb_ASIL_Dobhs_TestDefines();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_1_SetDobhsActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDobhsActive_CS> of PortPrototype <DobhsCtrlInterface_P_1>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_1_SetDobhsActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_1_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_1_SetDobhsActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_2_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsCtrlInterface_P_2>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_2_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_2_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_2_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_2_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  EcuHwFaultValues_T DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus;
  EcuHwDioCtrlArray_T DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus);
  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_2_SetDobhsActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDobhsActive_CS> of PortPrototype <DobhsCtrlInterface_P_2>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_2_SetDobhsActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_2_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_2_SetDobhsActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_3_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsCtrlInterface_P_3>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_3_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_3_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_3_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_3_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  EcuHwFaultValues_T DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus;
  EcuHwDioCtrlArray_T DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus);
  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_3_SetDobhsActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDobhsActive_CS> of PortPrototype <DobhsCtrlInterface_P_3>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_3_SetDobhsActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_3_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_3_SetDobhsActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_4_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsCtrlInterface_P_4>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_4_GetDobhsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_4_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_4_GetDobhsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_4_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  EcuHwFaultValues_T DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus;
  EcuHwDioCtrlArray_T DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus);
  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsCtrlInterface_P_4_SetDobhsActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDobhsActive_CS> of PortPrototype <DobhsCtrlInterface_P_4>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_4_SetDobhsActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsCtrlInterface_P_4_SetDobhsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsCtrlInterface_P_4_SetDobhsActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DobhsDiagInterface_P_GetDobhsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDobhsPinState_CS> of PortPrototype <DobhsDiagInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *isDioActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DobhsDiagInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsDiagInterface_P_GetDobhsPinState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Dobhs_CODE) DobhsDiagInterface_P_GetDobhsPinState_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) isDioActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_DOBHS_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DobhsDiagInterface_P_GetDobhsPinState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  EcuHwFaultValues_T DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus;
  EcuHwDioCtrlArray_T DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus);
  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: IoHwAb_ASIL_Dobhs_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
 *   Std_ReturnType Rte_Read_ScimPvtControl_P_Status(uint8 *data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(const Rte_DT_EcuHwFaultValues_T_0 *data)
 *   void Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(const Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_EcuHwState_P_GetEcuVoltages_CS(VGTT_EcuPinVoltage_0V2 *EcuVoltageValues)
 *     Argument EcuVoltageValues: VGTT_EcuPinVoltage_0V2* is of type EcuHwVoltageValues_T
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuHwState_I_AdcInFailure
 *   Std_ReturnType Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_VbatInterface_I_AdcInFailure, RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: IoHwAb_ASIL_Dobhs_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, IoHwAb_ASIL_Dobhs_CODE) IoHwAb_ASIL_Dobhs_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: IoHwAb_ASIL_Dobhs_10ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  DiagActiveState_T Read_DiagActiveState_P_isDiagActive;
  Fsc_OperationalMode_T Read_Fsc_OperationalMode_P_Fsc_OperationalMode;
  uint8 Read_ScimPvtControl_P_Status;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  EcuHwFaultValues_T IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus;
  EcuHwDioCtrlArray_T IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray;

  EcuHwFaultValues_T IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus_Write;
  EcuHwDioCtrlArray_T IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray_Write;

  EcuHwVoltageValues_T Call_EcuHwState_P_GetEcuVoltages_CS_EcuVoltageValues = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  VGTT_EcuPinVoltage_0V2 Call_VbatInterface_P_GetVbatVoltage_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_VbatInterface_P_GetVbatVoltage_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v();
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  fct_status = TSC_IoHwAb_ASIL_Dobhs_Rte_Read_DiagActiveState_P_isDiagActive(&Read_DiagActiveState_P_isDiagActive);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_IoHwAb_ASIL_Dobhs_Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&Read_Fsc_OperationalMode_P_Fsc_OperationalMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_IoHwAb_ASIL_Dobhs_Rte_Read_ScimPvtControl_P_Status(&Read_ScimPvtControl_P_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus);
  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray);

  (void)memset(&IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus_Write, 0, sizeof(IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus_Write));
  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus_Write);
  (void)memset(&IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray_Write, 0, sizeof(IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray_Write));
  TSC_IoHwAb_ASIL_Dobhs_Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray_Write);

  fct_status = TSC_IoHwAb_ASIL_Dobhs_Rte_Call_EcuHwState_P_GetEcuVoltages_CS(Call_EcuHwState_P_GetEcuVoltages_CS_EcuVoltageValues);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_EcuHwState_I_AdcInFailure:
      fct_error = 1;
      break;
  }

  fct_status = TSC_IoHwAb_ASIL_Dobhs_Rte_Call_VbatInterface_P_GetVbatVoltage_CS(&Call_VbatInterface_P_GetVbatVoltage_CS_BatteryVoltage, &Call_VbatInterface_P_GetVbatVoltage_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_VbatInterface_I_AdcInFailure:
      fct_error = 1;
      break;
    case RTE_E_VbatInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define IoHwAb_ASIL_Dobhs_STOP_SEC_CODE
#include "IoHwAb_ASIL_Dobhs_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void IoHwAb_ASIL_Dobhs_TestDefines(void)
{
  /* Enumeration Data Types */

  DiagActiveState_T Test_DiagActiveState_T_V_1 = Diag_Active_FALSE;
  DiagActiveState_T Test_DiagActiveState_T_V_2 = Diag_Active_TRUE;

  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_1 = FSC_ShutdownReady;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_2 = FSC_Reduced_12vDcDcLimit;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_3 = FSC_Reduced;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_4 = FSC_Operating;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_5 = FSC_Protecting;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_6 = FSC_Withstand;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_7 = FSC_NotAvailable;

  IOCtrlReq_T Test_IOCtrlReq_T_V_1 = IOCtrl_AppRequest;
  IOCtrlReq_T Test_IOCtrlReq_T_V_2 = IOCtrl_DiagReturnCtrlToApp;
  IOCtrlReq_T Test_IOCtrlReq_T_V_3 = IOCtrl_DiagShortTermAdjust;

  Rte_DT_EcuHwDioCtrlArray_T_0 Test_Rte_DT_EcuHwDioCtrlArray_T_0_V_1 = STD_LOW;
  Rte_DT_EcuHwDioCtrlArray_T_0 Test_Rte_DT_EcuHwDioCtrlArray_T_0_V_2 = STD_HIGH;
  Rte_DT_EcuHwDioCtrlArray_T_0 Test_Rte_DT_EcuHwDioCtrlArray_T_0_V_3 = Inactive;

  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_1 = TestNotRun;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_2 = OffState_NoFaultDetected;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_3 = OffState_FaultDetected_STG;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_4 = OffState_FaultDetected_STB;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_5 = OffState_FaultDetected_OC;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_6 = OffState_FaultDetected_VBT;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_7 = OffState_FaultDetected_VAT;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_8 = OnState_NoFaultDetected;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_9 = OnState_FaultDetected_STG;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_10 = OnState_FaultDetected_STB;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_11 = OnState_FaultDetected_OC;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_12 = OnState_FaultDetected_VBT;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_13 = OnState_FaultDetected_VAT;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_14 = OnState_FaultDetected_VOR;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_15 = OnState_FaultDetected_CAT;

  SEWS_PcbConfig_DOBHS_X1CXX_T Test_SEWS_PcbConfig_DOBHS_X1CXX_T_V_1 = SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated;
  SEWS_PcbConfig_DOBHS_X1CXX_T Test_SEWS_PcbConfig_DOBHS_X1CXX_T_V_2 = SEWS_PcbConfig_DOBHS_X1CXX_T_Populated;

  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_1 = TestNotRun;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_2 = OffState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_3 = OffState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_4 = OffState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_5 = OffState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_6 = OffState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_7 = OffState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_8 = OnState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_9 = OnState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_10 = OnState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_11 = OnState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_12 = OnState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_13 = OnState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_14 = OnState_FaultDetected_VOR;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_15 = OnState_FaultDetected_CAT;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
