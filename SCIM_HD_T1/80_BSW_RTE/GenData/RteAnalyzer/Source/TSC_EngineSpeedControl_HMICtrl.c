/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_EngineSpeedControl_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_EngineSpeedControl_HMICtrl.h"
#include "TSC_EngineSpeedControl_HMICtrl.h"








Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_ACCEnableRqst_ACCEnableRqst(DisableEnable_T *data)
{
  return Rte_Read_ACCEnableRqst_ACCEnableRqst(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_AcceleratorPedalStatus_AcceleratorPedalStatus(AcceleratorPedalStatus_T *data)
{
  return Rte_Read_AcceleratorPedalStatus_AcceleratorPedalStatus(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_CCEnableRequest_CCEnableRequest(DisableEnable_T *data)
{
  return Rte_Read_CCEnableRequest_CCEnableRequest(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_EngineSpeedControlStatus_EngineSpeedControlStatus(EngineSpeedControlStatus_T *data)
{
  return Rte_Read_EngineSpeedControlStatus_EngineSpeedControlStatus(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscButtonMuddySiteStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_EscButtonMuddySiteStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscSwitchEnableStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_EscSwitchEnableStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscSwitchIncDecStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
{
  return Rte_Read_EscSwitchIncDecStatus_A3PosSwitchStatus(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscSwitchMuddySiteStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_EscSwitchMuddySiteStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_EscSwitchResumeStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_EscSwitchResumeStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1(SWSpdCtrlButtonsStatus1_T *data)
{
  return Rte_Read_SWSpdCtrlButtonsStatus1_SWSpdCtrlButtonsStatus1(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode(SWSpeedControlAdjustMode_T *data)
{
  return Rte_Read_SWSpeedControlAdjustMode_SWSpeedControlAdjustMode(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
{
  return Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat(ButtonStatus_T *data)
{
  return Rte_Read_WrcEngSpdCtrlDecreaseButtStat_WrcEngSpdCtrlDecreaseButtStat(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus(DisableEnable_T *data)
{
  return Rte_Read_WrcEngSpdCtrlEnableStatus_WrcEngSpdCtrlEnableStatus(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat(ButtonStatus_T *data)
{
  return Rte_Read_WrcEngSpdCtrlIncreaseButtStat_WrcEngSpdCtrlIncreaseButtStat(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Read_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat(ButtonStatus_T *data)
{
  return Rte_Read_WrcEngSpdCtrlResumeButtonStat_WrcEngSpdCtrlResumeButtonStat(data);
}




Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Write_AdjustRequestForIdle_AdjustRequestForIdle(EngineSpeedRequest_T data)
{
  return Rte_Write_AdjustRequestForIdle_AdjustRequestForIdle(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscButtonMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_EscButtonMuddySiteDeviceInd_DeviceIndication(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscCabActionRequest_EscCabActionRequest(EscActionRequest_T data)
{
  return Rte_Write_EscCabActionRequest_EscCabActionRequest(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscCabEnable_EscCabEnable(DisableEnable_T data)
{
  return Rte_Write_EscCabEnable_EscCabEnable(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst(DisableEnable_T data)
{
  return Rte_Write_EscCrossCountryActivation_rqst_EscCrossCountryActivation_rqst(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscSwitchEnableDeviceInd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_EscSwitchEnableDeviceInd_DeviceIndication(data);
}

Std_ReturnType TSC_EngineSpeedControl_HMICtrl_Rte_Write_EscSwitchMuddySiteDeviceInd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_EscSwitchMuddySiteDeviceInd_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T  TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1IZ3_ESC_InhibitionByPrimaryPedal_v(void)
{
  return (SEWS_ESC_InhibitionByPrimaryPedal_P1IZ3_T ) Rte_Prm_P1IZ3_ESC_InhibitionByPrimaryPedal_v();
}
boolean  TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1B0X_EngineSpeedControlSw_v(void)
{
  return (boolean ) Rte_Prm_P1B0X_EngineSpeedControlSw_v();
}
SEWS_BodybuilderAccessToAccelPedal_P1B72_T  TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1B72_BodybuilderAccessToAccelPedal_v(void)
{
  return (SEWS_BodybuilderAccessToAccelPedal_P1B72_T ) Rte_Prm_P1B72_BodybuilderAccessToAccelPedal_v();
}
boolean  TSC_EngineSpeedControl_HMICtrl_Rte_Prm_P1B0W_CrossCountryCC_Act_v(void)
{
  return (boolean ) Rte_Prm_P1B0W_CrossCountryCC_Act_v();
}


     /* EngineSpeedControl_HMICtrl */
      /* EngineSpeedControl_HMICtrl */



