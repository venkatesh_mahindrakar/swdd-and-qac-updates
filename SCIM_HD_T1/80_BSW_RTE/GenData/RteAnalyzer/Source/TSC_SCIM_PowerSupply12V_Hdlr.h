/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SCIM_PowerSupply12V_Hdlr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Read_Living12VResetRequest_Living12VResetRequest(Boolean *data);
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Write_Living12VPowerStability_Living12VPowerStability(Living12VPowerStability data);

/** Client server interfaces */
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus);
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus);
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation);
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation);
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation);

/** Service interfaces */
Std_ReturnType TSC_SCIM_PowerSupply12V_Hdlr_Rte_Call_Issm_GetAllActiveIss_GetAllActiveIss(uint32 *activeIssField);




