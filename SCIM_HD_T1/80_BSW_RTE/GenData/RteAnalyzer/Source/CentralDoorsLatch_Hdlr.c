/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  CentralDoorsLatch_Hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  CentralDoorsLatch_Hdlr
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <CentralDoorsLatch_Hdlr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IOHWAB_UINT8
 *   
 *
 * VGTT_EcuPinFaultStatus
 *   
 *
 * VGTT_EcuPinVoltage_0V2
 *   
 *
 *********************************************************************************************************************/

#include "Rte_CentralDoorsLatch_Hdlr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_CentralDoorsLatch_Hdlr.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void CentralDoorsLatch_Hdlr_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_UINT8: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DoorLatch_rqst_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLatch_rqst_decrypt_Idle (0U)
 *   DoorLatch_rqst_decrypt_LockDoorCommand (1U)
 *   DoorLatch_rqst_decrypt_UnlockDoorCommand (2U)
 *   DoorLatch_rqst_decrypt_EmergencyUnlockRequest (3U)
 *   DoorLatch_rqst_decrypt_Spare1 (4U)
 *   DoorLatch_rqst_decrypt_Spare2 (5U)
 *   DoorLatch_rqst_decrypt_Error (6U)
 *   DoorLatch_rqst_decrypt_NotAvailable (7U)
 * DoorLatch_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLatch_stat_Idle (0U)
 *   DoorLatch_stat_LatchUnlockedFiltered (1U)
 *   DoorLatch_stat_LatchLockedFiltered (2U)
 *   DoorLatch_stat_Error (6U)
 *   DoorLatch_stat_NotAvailable (7U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


#define CentralDoorsLatch_Hdlr_START_SEC_CODE
#include "CentralDoorsLatch_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CentralDoorsLatch_Hdlr_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T *data)
 *   Std_ReturnType Rte_Read_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DriverDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T data)
 *   Std_ReturnType Rte_Write_PsngDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CentralDoorsLatch_Hdlr_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, CentralDoorsLatch_Hdlr_CODE) CentralDoorsLatch_Hdlr_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CentralDoorsLatch_Hdlr_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  DoorLatch_rqst_decrypt_T Read_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt;
  DoorLatch_rqst_decrypt_T Read_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt;
  VehicleModeDistribution_T Read_SwcActivation_Security_SwcActivation_Security;

  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage = 0U;
  VGTT_EcuPinVoltage_0V2 Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_CentralDoorsLatch_Hdlr_Rte_Read_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(&Read_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_CentralDoorsLatch_Hdlr_Rte_Read_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(&Read_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_CentralDoorsLatch_Hdlr_Rte_Read_SwcActivation_Security_SwcActivation_Security(&Read_SwcActivation_Security_SwcActivation_Security);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_CentralDoorsLatch_Hdlr_Rte_Write_DriverDoorLatchInternal_stat_DoorLatch_stat(Rte_InitValue_DriverDoorLatchInternal_stat_DoorLatch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_CentralDoorsLatch_Hdlr_Rte_Write_PsngDoorLatchInternal_stat_DoorLatch_stat(Rte_InitValue_PsngDoorLatchInternal_stat_DoorLatch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_CentralDoorsLatch_Hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(0U, &Call_AdiInterface_P_GetAdiPinState_CS_AdiPinVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_BatteryVoltage, &Call_AdiInterface_P_GetAdiPinState_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_AdiInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }

  CentralDoorsLatch_Hdlr_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define CentralDoorsLatch_Hdlr_STOP_SEC_CODE
#include "CentralDoorsLatch_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void CentralDoorsLatch_Hdlr_TestDefines(void)
{
  /* Enumeration Data Types */

  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_1 = DoorLatch_rqst_decrypt_Idle;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_2 = DoorLatch_rqst_decrypt_LockDoorCommand;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_3 = DoorLatch_rqst_decrypt_UnlockDoorCommand;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_4 = DoorLatch_rqst_decrypt_EmergencyUnlockRequest;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_5 = DoorLatch_rqst_decrypt_Spare1;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_6 = DoorLatch_rqst_decrypt_Spare2;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_7 = DoorLatch_rqst_decrypt_Error;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_8 = DoorLatch_rqst_decrypt_NotAvailable;

  DoorLatch_stat_T Test_DoorLatch_stat_T_V_1 = DoorLatch_stat_Idle;
  DoorLatch_stat_T Test_DoorLatch_stat_T_V_2 = DoorLatch_stat_LatchUnlockedFiltered;
  DoorLatch_stat_T Test_DoorLatch_stat_T_V_3 = DoorLatch_stat_LatchLockedFiltered;
  DoorLatch_stat_T Test_DoorLatch_stat_T_V_4 = DoorLatch_stat_Error;
  DoorLatch_stat_T Test_DoorLatch_stat_T_V_5 = DoorLatch_stat_NotAvailable;

  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_1 = TestNotRun;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_2 = OffState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_3 = OffState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_4 = OffState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_5 = OffState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_6 = OffState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_7 = OffState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_8 = OnState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_9 = OnState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_10 = OnState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_11 = OnState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_12 = OnState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_13 = OnState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_14 = OnState_FaultDetected_VOR;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_15 = OnState_FaultDetected_CAT;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
