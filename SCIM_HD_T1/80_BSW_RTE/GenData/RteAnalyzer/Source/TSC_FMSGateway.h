/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_FMSGateway.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_FMSGateway_Rte_Read_AcceleratorPedalPosition1_AcceleratorPedalPosition1(Percent8bitFactor04_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_ActualDrvlnRetdrPercentTorque_ActualDrvlnRetdrPercentTorque(Percent8bit125NegOffset_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_ActualEnginePercentTorque_ActualEnginePercentTorque(Percent8bit125NegOffset_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_ActualEngineRetarderPercentTrq_ActualEngineRetarderPercentTrq(Percent8bit125NegOffset_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_AmbientAirTemperature_AmbientAirTemperature(Temperature16bit_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_BrakeSwitch_BrakeSwitch(BrakeSwitch_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_CCActive_CCActive(OffOn_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_CatalystTankLevel_CatalystTankLevel(Percent8bitFactor04_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_ClutchSwitch_ClutchSwitch(ClutchSwitch_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_Driver1TimeRelatedStates_Driver1TimeRelatedStates(Driver1TimeRelatedStates_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_Driver1WorkingState_Driver1WorkingState(DriverWorkingState_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_Driver2TimeRelatedStates_Driver2TimeRelatedStates(DriverTimeRelatedStates_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_Driver2WorkingState_Driver2WorkingState(DriverWorkingState_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_DriverCardDriver1_DriverCardDriver1(NotPresentPresent_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_DriverCardDriver2_DriverCardDriver2(NotPresentPresent_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_EngineCoolantTemp_stat_EngineCoolantTemp_stat(EngineTemp_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_EngineFuelRate_EngineFuelRate(FuelRate_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_EngineGasRate_EngineGasRate(GasRate_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_EnginePercentLoadAtCurrentSpd_EnginePercentLoadAtCurrentSpd(Percent8bitNoOffset_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_EngineRetarderTorqueMode_EngineRetarderTorqueMode(EngineRetarderTorqueMode_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_EngineRunningTime_EngineRunningTime(Seconds32bitExtra_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_EngineSpeed_EngineSpeed(SpeedRpm16bit_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_EngineTotalFuelConsumed_EngineTotalFuelConsumed(Volume32bit_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_EngineTotalLngConsumed_EngineTotalLngConsumed(TotalLngConsumed_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_FMS1_FMS1(FMS1_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_FuelLevel_FuelLevel(Percent8bitFactor04_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_GrossCombinationVehicleWeight_GrossCombinationVehicleWeight(VehicleWeight16bit_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_HandlingInformation_HandlingInformation(HandlingInformation_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_HighResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(Volume32BitFact001_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_InstantaneousFuelEconomy_FMSInstantFuelEconomy(InstantFuelEconomy_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_LngTank1RemainingGasVolume_LngTank1RemainingGasVolume(Percent8bitFactor04_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_LngTank2RemainingGasVolume_LngTank2RemainingGasVolume(Percent8bitFactor04_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_OilPrediction_OilPrediction(OilPrediction_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_PtosStatus_PtosStatus(PtosStatus_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_RetarderTorqueMode_RetarderTorqueMode(RetarderTorqueMode_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_ReverseGearEngaged_ReverseGearEngaged(ReverseGearEngaged_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_ServiceBrakeAirPrsCircuit1_ServiceBrakeAirPrsCircuit1(PressureFactor8_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_ServiceBrakeAirPrsCircuit2_ServiceBrakeAirPrsCircuit2(PressureFactor8_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_SystemEvent_SystemEvent(SystemEvent_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_TachographPerformance_TachographPerformance(TachographPerformance_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_TachographVehicleSpeed_TachographVehicleSpeed(Speed16bit_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(Distance32bit_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_VehFrontAxle1CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_VehRearAxle1CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_VehRearAxle2CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_VehRearAxle3CalibratedLoad_VehicleAxleCalibratedLoad(AxleLoad_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_VehicleMotion_VehicleMotion(NotDetected_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_VehicleOverspeed_VehicleOverspeed(VehicleOverspeed_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_FMSGateway_Rte_Write_AcceleratorPedalPosition1_fms_AcceleratorPedalPosition1(Percent8bitFactor04_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_ActualDrvlnRetdrPctTrq_fms_ActualDrvlnRetdrPercentTorque(Percent8bit125NegOffset_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_ActualEngRetdrPercentTrq_fms_ActualEngineRetarderPercentTrq(Percent8bit125NegOffset_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_ActualEnginePercentTorque_fms_ActualEnginePercentTorque(Percent8bit125NegOffset_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_AmbientAirTemperature_fms_AmbientAirTemperature(Temperature16bit_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_BrakeSwitch_fms_BrakeSwitch(BrakeSwitch_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_CCActive_fms_CCActive(OffOn_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_CatalystTankLevel_fms_CatalystTankLevel(Percent8bitFactor04_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_ClutchSwitch_fms_ClutchSwitch(ClutchSwitch_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_DirectionIndicator_fms_DirectionIndicator(DirectionIndicator_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_Driver1TimeRelatedStates_fms_Driver1TimeRelatedStates(Driver1TimeRelatedStates_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_Driver1WorkingState_fms_Driver1WorkingState(DriverWorkingState_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_Driver2TimeRelatedStates_fms_Driver2TimeRelatedStates(DriverTimeRelatedStates_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_Driver2WorkingState_fms_Driver2WorkingState(DriverWorkingState_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_DriverCardDriver1_fms_DriverCardDriver1(NotPresentPresent_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_DriverCardDriver2_fms_DriverCardDriver2(NotPresentPresent_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_EngineCoolantTemp_stat_fms_EngineCoolantTemp_stat(EngineTemp_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_EngineFuelRate_fms_EngineFuelRate(FuelRate_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_EnginePctLoadAtCurrentSpd_fms_EnginePercentLoadAtCurrentSpd(Percent8bitNoOffset_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_EngineRetarderTorqueMode_fms_EngineRetarderTorqueMode(EngineRetarderTorqueMode_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_EngineSpeed_fms_EngineSpeed(SpeedRpm16bit_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMS1_fms_FMS1(const FMS1_T *data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSAtLeastOnePTOEngaged_AtLeastOnePTOEngaged(OffOn_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSAxleLocation_AxleLocation(Int8Bit_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSAxleWeight_AxleWeight(AxleLoad_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSDiagnosisSupported_FMSDiagnosisSupported(Supported_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSEngineTotHoursOfOp_EngineTotalHoursOfOperation(EngineTotalHoursOfOperation_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSEngineTotalFuelUsed_FMSEngineTotalFuelUsed(Volume32bitFact05_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSHiResEngineTotalFuelUsed_FMSHiResEngineTotalFuelUsed(Volume32BitFact001_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSInstantFuelEconomy_FMSInstantFuelEconomy(InstantFuelEconomy_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSPtoState_PtoState(PtoState_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSRequestsSupported_FMSRequestsSupported(Supported_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSServiceDistance_FMSServiceDistance(ServiceDistance16BitFact5Offset_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FMSSoftwareVersionSupported_FMSSoftwareVersionSupported(const uint8 *data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FuelLevel_fms_FuelLevel(Percent8bitFactor04_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_FuelType_FuelType(FuelType_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_GrossCombVehicleWeight_fms_GrossCombinationVehicleWeight(VehicleWeight16bit_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_HandlingInformation_fms_HandlingInformation(HandlingInformation_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_PGNReq_Tacho_CIOM_PGNRequest(PGNRequest_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_RetarderTorqueMode_fms_RetarderTorqueMode(RetarderTorqueMode_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_ServiceBrakeAirPrsCircuit1_fms_ServiceBrakeAirPrsCircuit1(PressureFactor8_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_ServiceBrakeAirPrsCircuit2_fms_ServiceBrakeAirPrsCircuit2(PressureFactor8_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_SystemEvent_fms_SystemEvent(SystemEvent_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_TachographPerformance_fms_TachographPerformance(TachographPerformance_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_TachographVehicleSpeed_fms_TachographVehicleSpeed(Speed16bit_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_TotalVehicleDistanceHiRes_fms_TotalVehicleDistanceHighRes(Distance32bit_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_VehicleMotion_fms_VehicleMotion(NotDetected_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_VehicleOverspeed_fms_VehicleOverspeed(VehicleOverspeed_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_WeightClass_WeightClass(WeightClass_T data);
Std_ReturnType TSC_FMSGateway_Rte_Write_WheelBasedVehicleSpeed_fms_WheelBasedVehicleSpeed(Speed16bit_T data);

/** Sender receiver - Queued - Explicit read */
Std_ReturnType TSC_FMSGateway_Rte_Receive_Driver1Identification_Driver1Identification(uint8 *data);
Std_ReturnType TSC_FMSGateway_Rte_Receive_DriversIdentifications_DriversIdentifications(uint8 *data, uint16* length);
Std_ReturnType TSC_FMSGateway_Rte_Receive_MaintService_MaintService(MaintService_T *data);

/** Sender receiver - Queued - Explicit send */
Std_ReturnType TSC_FMSGateway_Rte_Send_FMSDriversIdentifications_DriversIdentifications(const uint8 *data, uint16 length);
Std_ReturnType TSC_FMSGateway_Rte_Send_FMSVehicleIdentNumber_VehicleIdentNumber(const uint8 *data);

/** Calibration Component Calibration Parameters */
SEWS_LNGTank1Volume_P1LX9_T  TSC_FMSGateway_Rte_Prm_P1LX9_LNGTank1Volume_v(void);
SEWS_LNGTank2Volume_P1LYA_T  TSC_FMSGateway_Rte_Prm_P1LYA_LNGTank2Volume_v(void);
SEWS_WeightClassInformation_P1M7S_T  TSC_FMSGateway_Rte_Prm_P1M7S_WeightClassInformation_v(void);
SEWS_FuelTypeInformation_P1M7T_T  TSC_FMSGateway_Rte_Prm_P1M7T_FuelTypeInformation_v(void);
SEWS_CabHeightValue_P1R0P_T  TSC_FMSGateway_Rte_Prm_P1R0P_CabHeightValue_v(void);
boolean  TSC_FMSGateway_Rte_Prm_P1DXX_FMSgateway_Act_v(void);
boolean  TSC_FMSGateway_Rte_Prm_P1LX8_EngineFuelTypeLNG_v(void);
boolean  TSC_FMSGateway_Rte_Prm_P1M7Q_EraGlonassUnitInstalled_v(void);
SEWS_VIN_VINNO_T * TSC_FMSGateway_Rte_Prm_VINNO_VIN_v(void);




