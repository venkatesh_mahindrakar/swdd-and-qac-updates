/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Cdd_LinDiagnostics.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Read_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Read_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Write_LinDiagRequestFlag_CCNADRequest(uint8 data);
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Write_LinDiagRequestFlag_PNSNRequest(uint8 data);

/** Client server interfaces */
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Call_CddLinClearRequest_ClearRequest(void);
Std_ReturnType TSC_Cdd_LinDiagnostics_Rte_Call_CddLinTxHandling_Transmit(uint8 TxId, uint8 *TxData, uint8 Length);

/** Calibration Component Calibration Parameters */
SEWS_LIN_topology_P1AJR_T  TSC_Cdd_LinDiagnostics_Rte_Prm_P1AJR_LIN_topology_v(void);




