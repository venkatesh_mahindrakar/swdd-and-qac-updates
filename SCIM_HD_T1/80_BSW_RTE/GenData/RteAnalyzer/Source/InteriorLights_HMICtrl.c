/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  InteriorLights_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  InteriorLights_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <InteriorLights_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IntLghtLvlIndScaled_cmd_T
 *   
 *
 * Issm_IssStateType
 *   
 *
 * Percent8bitNoOffset_T
 *   
 *
 * SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T
 *   
 *
 * SEWS_IL_CtrlDeviceTypeFront_P1DKH_T
 *   
 *
 * SEWS_IL_LockingCmdDelayOff_P1K7E_T
 *   
 *
 * SEWS_P1DKF_Long_press_threshold_T
 *   
 *
 * SEWS_P1DKF_Shut_off_threshold_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_InteriorLights_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_InteriorLights_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void InteriorLights_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * EventFlag_T: Boolean
 * IntLghtLvlIndScaled_cmd_T: Integer in interval [0...15]
 *   Unit: [Step], Factor: 1, Offset: 0
 * Percent8bitNoOffset_T: Integer in interval [0...255]
 *   Unit: [Percent], Factor: 1, Offset: 0
 * SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T: Integer in interval [0...255]
 * SEWS_IL_CtrlDeviceTypeFront_P1DKH_T: Integer in interval [0...255]
 * SEWS_IL_LockingCmdDelayOff_P1K7E_T: Integer in interval [0...255]
 * SEWS_P1DKF_Long_press_threshold_T: Integer in interval [0...255]
 * SEWS_P1DKF_Shut_off_threshold_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * A3PosSwitchStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   A3PosSwitchStatus_Middle (0U)
 *   A3PosSwitchStatus_Lower (1U)
 *   A3PosSwitchStatus_Upper (2U)
 *   A3PosSwitchStatus_Spare (3U)
 *   A3PosSwitchStatus_Spare_01 (4U)
 *   A3PosSwitchStatus_Spare_02 (5U)
 *   A3PosSwitchStatus_Error (6U)
 *   A3PosSwitchStatus_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * IL_ModeReq_T: Enumeration of integer in interval [0...7] with enumerators
 *   IL_ModeReq_OFF (0U)
 *   IL_ModeReq_NightDriving (1U)
 *   IL_ModeReq_Resting (2U)
 *   IL_ModeReq_Max (3U)
 *   IL_ModeReq_NonHMIModeResting (4U)
 *   IL_ModeReq_NonHMIModeMax (5U)
 *   IL_ModeReq_ErrorIndicator (6U)
 *   IL_ModeReq_NotAvailable (7U)
 * IL_Mode_T: Enumeration of integer in interval [0...7] with enumerators
 *   IL_Mode_OFF (0U)
 *   IL_Mode_NightDriving (1U)
 *   IL_Mode_Resting (2U)
 *   IL_Mode_Max (3U)
 *   IL_Mode_spare_1 (4U)
 *   IL_Mode_spare_2 (5U)
 *   IL_Mode_ErrorIndicator (6U)
 *   IL_Mode_NotAvailable (7U)
 * InteriorLightDimming_rqst_T: Enumeration of integer in interval [0...31] with enumerators
 *   InteriorLightDimming_rqst_NoMovement (0U)
 *   InteriorLightDimming_rqst_Position1 (1U)
 *   InteriorLightDimming_rqst_Position2 (2U)
 *   InteriorLightDimming_rqst_Position3 (3U)
 *   InteriorLightDimming_rqst_Position4 (4U)
 *   InteriorLightDimming_rqst_Position5 (5U)
 *   InteriorLightDimming_rqst_Position6 (6U)
 *   InteriorLightDimming_rqst_Position7 (7U)
 *   InteriorLightDimming_rqst_Position8 (8U)
 *   InteriorLightDimming_rqst_Position9 (9U)
 *   InteriorLightDimming_rqst_Position10 (10U)
 *   InteriorLightDimming_rqst_Position11 (11U)
 *   InteriorLightDimming_rqst_Position12 (12U)
 *   InteriorLightDimming_rqst_Position13 (13U)
 *   InteriorLightDimming_rqst_DimUpContinously (14U)
 *   InteriorLightDimming_rqst_DimDownContinously (15U)
 *   InteriorLightDimming_rqst_Error (30U)
 *   InteriorLightDimming_rqst_NotAvailable (31U)
 * Issm_IssStateType: Enumeration of integer in interval [0...2] with enumerators
 *   ISSM_STATE_INACTIVE (0U)
 *   ISSM_STATE_PENDING (1U)
 *   ISSM_STATE_ACTIVE (2U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * Rte_DT_InteriorLightMode_rqst_T_1: Enumeration of integer in interval [0...1] with enumerators
 *   EventFlag_Low (0U)
 *   EventFlag_High (1U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 * Record Types:
 * =============
 * InteriorLightMode_T: Record with elements
 *   IL_Mode_RE of type IL_Mode_T
 *   EventFlag_RE of type EventFlag_T
 * InteriorLightMode_rqst_T: Record with elements
 *   IL_Mode_RE of type IL_ModeReq_T
 *   EventFlag_RE of type Rte_DT_InteriorLightMode_rqst_T_1
 * SEWS_IL_ShortLongPushThresholds_P1DKF_s_T: Record with elements
 *   Long_press_threshold of type SEWS_P1DKF_Long_press_threshold_T
 *   Shut_off_threshold of type SEWS_P1DKF_Shut_off_threshold_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_IL_LockingCmdDelayOff_P1K7E_T Rte_Prm_P1K7E_IL_LockingCmdDelayOff_v(void)
 *   SEWS_IL_ShortLongPushThresholds_P1DKF_s_T *Rte_Prm_P1DKF_IL_ShortLongPushThresholds_v(void)
 *   SEWS_IL_CtrlDeviceTypeFront_P1DKH_T Rte_Prm_P1DKH_IL_CtrlDeviceTypeFront_v(void)
 *   SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T Rte_Prm_P1DKI_IL_CtrlDeviceTypeBunk_v(void)
 *
 *********************************************************************************************************************/


#define InteriorLights_HMICtrl_START_SEC_CODE
#include "InteriorLights_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: InteriorLights_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_BnkH1IntLghtMMenu_stat_InteriorLightMode(InteriorLightMode_T *data)
 *   Std_ReturnType Rte_Read_BunkBIntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH1IntLghtActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2IntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_DoorAutoFuncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtActvnBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_IntLghtNightModeBtn2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtNightModeBtn_stat_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtNightModeFlxSw2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_IntLightMode_CoreRqst_InteriorLightMode_rqst(InteriorLightMode_rqst_T *data)
 *   Std_ReturnType Rte_Read_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd(Percent8bitNoOffset_T *data)
 *   Std_ReturnType Rte_Read_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   boolean Rte_IsUpdated_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_DoorAutoFunction_rqst_DoorAutoFunction_rqst(OffOn_T data)
 *   Std_ReturnType Rte_Write_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T data)
 *   Std_ReturnType Rte_Write_IntLghtModeInd_cmd_InteriorLightMode(const InteriorLightMode_T *data)
 *   Std_ReturnType Rte_Write_IntLghtOffModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_IntLightMaxModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_IntLightNightModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_IntLightRestingModeInd_cmd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_InteriorLightDimming_rqst_InteriorLightDimming_rqst(InteriorLightDimming_rqst_T data)
 *   Std_ReturnType Rte_Write_InteriorLightMode_rqst_InteriorLightMode_rqst(const InteriorLightMode_rqst_T *data)
 *   Std_ReturnType Rte_Write_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment2_GetIssState(Issm_IssStateType *issState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_OtherInteriorLights3_GetIssState(Issm_IssStateType *issState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: InteriorLights_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, InteriorLights_HMICtrl_CODE) InteriorLights_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: InteriorLights_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  InteriorLightMode_T Read_BnkH1IntLghtMMenu_stat_InteriorLightMode;
  PushButtonStatus_T Read_BunkBIntLightActvnBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH1IntLghtActvnBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH2IntLightActvnBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus;
  PushButtonStatus_T Read_DoorAutoFuncBtn_stat_PushButtonStatus;
  A2PosSwitchStatus_T Read_IntLghtActvnBtn_stat_A2PosSwitchStatus;
  PushButtonStatus_T Read_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus;
  PushButtonStatus_T Read_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus;
  FreeWheel_Status_T Read_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status;
  boolean IsUpdated_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status;
  PushButtonStatus_T Read_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus;
  A3PosSwitchStatus_T Read_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus;
  PushButtonStatus_T Read_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus;
  PushButtonStatus_T Read_IntLghtMaxModeFlxSw2_stat_PushButtonStatus;
  FreeWheel_Status_T Read_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status;
  PushButtonStatus_T Read_IntLghtNightModeBtn2_stat_PushButtonStatus;
  A2PosSwitchStatus_T Read_IntLghtNightModeBtn_stat_A2PosSwitchStatus;
  PushButtonStatus_T Read_IntLghtNightModeFlxSw2_stat_PushButtonStatus;
  PushButtonStatus_T Read_IntLghtRestModeBtnPnl2_stat_PushButtonStatus;
  InteriorLightMode_rqst_T Read_IntLightMode_CoreRqst_InteriorLightMode_rqst;
  Percent8bitNoOffset_T Read_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd;
  StandardNVM_T Read_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I;
  PushButtonStatus_T Read_KeyfobLockButton_Status_PushButtonStatus;
  VehicleModeDistribution_T Read_SwcActivation_Living_Living;

  SEWS_IL_LockingCmdDelayOff_P1K7E_T P1K7E_IL_LockingCmdDelayOff_v_data;
  SEWS_IL_ShortLongPushThresholds_P1DKF_s_T P1DKF_IL_ShortLongPushThresholds_v_data;

  SEWS_IL_CtrlDeviceTypeFront_P1DKH_T P1DKH_IL_CtrlDeviceTypeFront_v_data;
  SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T P1DKI_IL_CtrlDeviceTypeBunk_v_data;

  InteriorLightMode_T Write_IntLghtModeInd_cmd_InteriorLightMode;
  InteriorLightMode_rqst_T Write_InteriorLightMode_rqst_InteriorLightMode_rqst;
  StandardNVM_T Write_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I;

  Issm_IssStateType Call_UR_ANW_DimmingAdjustment2_GetIssState_issState = 0U;
  Issm_IssStateType Call_UR_ANW_OtherInteriorLights3_GetIssState_issState = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1K7E_IL_LockingCmdDelayOff_v_data = TSC_InteriorLights_HMICtrl_Rte_Prm_P1K7E_IL_LockingCmdDelayOff_v();
  P1DKF_IL_ShortLongPushThresholds_v_data = *TSC_InteriorLights_HMICtrl_Rte_Prm_P1DKF_IL_ShortLongPushThresholds_v();

  P1DKH_IL_CtrlDeviceTypeFront_v_data = TSC_InteriorLights_HMICtrl_Rte_Prm_P1DKH_IL_CtrlDeviceTypeFront_v();
  P1DKI_IL_CtrlDeviceTypeBunk_v_data = TSC_InteriorLights_HMICtrl_Rte_Prm_P1DKI_IL_CtrlDeviceTypeBunk_v();

  IsUpdated_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status = TSC_InteriorLights_HMICtrl_Rte_IsUpdated_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status();

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_BnkH1IntLghtMMenu_stat_InteriorLightMode(&Read_BnkH1IntLghtMMenu_stat_InteriorLightMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_BunkBIntLightActvnBtn_stat_PushButtonStatus(&Read_BunkBIntLightActvnBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_BunkH1IntLghtActvnBtn_stat_PushButtonStatus(&Read_BunkH1IntLghtActvnBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus(&Read_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus(&Read_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_BunkH2IntLightActvnBtn_stat_PushButtonStatus(&Read_BunkH2IntLightActvnBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(&Read_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(&Read_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_DoorAutoFuncBtn_stat_PushButtonStatus(&Read_DoorAutoFuncBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtActvnBtn_stat_A2PosSwitchStatus(&Read_IntLghtActvnBtn_stat_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(&Read_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(&Read_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(&Read_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(&Read_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(&Read_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(&Read_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(&Read_IntLghtMaxModeFlxSw2_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(&Read_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtNightModeBtn2_stat_PushButtonStatus(&Read_IntLghtNightModeBtn2_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtNightModeBtn_stat_A2PosSwitchStatus(&Read_IntLghtNightModeBtn_stat_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtNightModeFlxSw2_stat_PushButtonStatus(&Read_IntLghtNightModeFlxSw2_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(&Read_IntLghtRestModeBtnPnl2_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_IntLightMode_CoreRqst_InteriorLightMode_rqst(&Read_IntLightMode_CoreRqst_InteriorLightMode_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd(&Read_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(Read_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_KeyfobLockButton_Status_PushButtonStatus(&Read_KeyfobLockButton_Status_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Read_SwcActivation_Living_Living(&Read_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_DoorAutoFuncInd_cmd_DeviceIndication(Rte_InitValue_DoorAutoFuncInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_DoorAutoFunction_rqst_DoorAutoFunction_rqst(Rte_InitValue_DoorAutoFunction_rqst_DoorAutoFunction_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(Rte_InitValue_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_IntLghtModeInd_cmd_InteriorLightMode, 0, sizeof(Write_IntLghtModeInd_cmd_InteriorLightMode));
  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_IntLghtModeInd_cmd_InteriorLightMode(&Write_IntLghtModeInd_cmd_InteriorLightMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_IntLghtOffModeInd_cmd_DeviceIndication(Rte_InitValue_IntLghtOffModeInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_IntLightMaxModeInd_cmd_DeviceIndication(Rte_InitValue_IntLightMaxModeInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_IntLightNightModeInd_cmd_DeviceIndication(Rte_InitValue_IntLightNightModeInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_IntLightRestingModeInd_cmd_DeviceIndication(Rte_InitValue_IntLightRestingModeInd_cmd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_InteriorLightDimming_rqst_InteriorLightDimming_rqst(Rte_InitValue_InteriorLightDimming_rqst_InteriorLightDimming_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_InteriorLightMode_rqst_InteriorLightMode_rqst, 0, sizeof(Write_InteriorLightMode_rqst_InteriorLightMode_rqst));
  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_InteriorLightMode_rqst_InteriorLightMode_rqst(&Write_InteriorLightMode_rqst_InteriorLightMode_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I, 0, sizeof(Write_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I));
  fct_status = TSC_InteriorLights_HMICtrl_Rte_Write_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(Write_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_DimmingAdjustment2_GetIssState(&Call_UR_ANW_DimmingAdjustment2_GetIssState_issState);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_InteriorLights_HMICtrl_Rte_Call_UR_ANW_OtherInteriorLights3_GetIssState(&Call_UR_ANW_OtherInteriorLights3_GetIssState_issState);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  InteriorLights_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define InteriorLights_HMICtrl_STOP_SEC_CODE
#include "InteriorLights_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void InteriorLights_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_1 = A3PosSwitchStatus_Middle;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_2 = A3PosSwitchStatus_Lower;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_3 = A3PosSwitchStatus_Upper;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_4 = A3PosSwitchStatus_Spare;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_5 = A3PosSwitchStatus_Spare_01;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_6 = A3PosSwitchStatus_Spare_02;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_7 = A3PosSwitchStatus_Error;
  A3PosSwitchStatus_T Test_A3PosSwitchStatus_T_V_8 = A3PosSwitchStatus_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  FreeWheel_Status_T Test_FreeWheel_Status_T_V_1 = FreeWheel_Status_NoMovement;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_2 = FreeWheel_Status_1StepClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_3 = FreeWheel_Status_2StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_4 = FreeWheel_Status_3StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_5 = FreeWheel_Status_4StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_6 = FreeWheel_Status_5StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_7 = FreeWheel_Status_6StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_8 = FreeWheel_Status_1StepCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_9 = FreeWheel_Status_2StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_10 = FreeWheel_Status_3StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_11 = FreeWheel_Status_4StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_12 = FreeWheel_Status_5StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_13 = FreeWheel_Status_6StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_14 = FreeWheel_Status_Spare;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_15 = FreeWheel_Status_Error;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_16 = FreeWheel_Status_NotAvailable;

  IL_ModeReq_T Test_IL_ModeReq_T_V_1 = IL_ModeReq_OFF;
  IL_ModeReq_T Test_IL_ModeReq_T_V_2 = IL_ModeReq_NightDriving;
  IL_ModeReq_T Test_IL_ModeReq_T_V_3 = IL_ModeReq_Resting;
  IL_ModeReq_T Test_IL_ModeReq_T_V_4 = IL_ModeReq_Max;
  IL_ModeReq_T Test_IL_ModeReq_T_V_5 = IL_ModeReq_NonHMIModeResting;
  IL_ModeReq_T Test_IL_ModeReq_T_V_6 = IL_ModeReq_NonHMIModeMax;
  IL_ModeReq_T Test_IL_ModeReq_T_V_7 = IL_ModeReq_ErrorIndicator;
  IL_ModeReq_T Test_IL_ModeReq_T_V_8 = IL_ModeReq_NotAvailable;

  IL_Mode_T Test_IL_Mode_T_V_1 = IL_Mode_OFF;
  IL_Mode_T Test_IL_Mode_T_V_2 = IL_Mode_NightDriving;
  IL_Mode_T Test_IL_Mode_T_V_3 = IL_Mode_Resting;
  IL_Mode_T Test_IL_Mode_T_V_4 = IL_Mode_Max;
  IL_Mode_T Test_IL_Mode_T_V_5 = IL_Mode_spare_1;
  IL_Mode_T Test_IL_Mode_T_V_6 = IL_Mode_spare_2;
  IL_Mode_T Test_IL_Mode_T_V_7 = IL_Mode_ErrorIndicator;
  IL_Mode_T Test_IL_Mode_T_V_8 = IL_Mode_NotAvailable;

  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_1 = InteriorLightDimming_rqst_NoMovement;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_2 = InteriorLightDimming_rqst_Position1;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_3 = InteriorLightDimming_rqst_Position2;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_4 = InteriorLightDimming_rqst_Position3;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_5 = InteriorLightDimming_rqst_Position4;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_6 = InteriorLightDimming_rqst_Position5;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_7 = InteriorLightDimming_rqst_Position6;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_8 = InteriorLightDimming_rqst_Position7;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_9 = InteriorLightDimming_rqst_Position8;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_10 = InteriorLightDimming_rqst_Position9;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_11 = InteriorLightDimming_rqst_Position10;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_12 = InteriorLightDimming_rqst_Position11;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_13 = InteriorLightDimming_rqst_Position12;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_14 = InteriorLightDimming_rqst_Position13;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_15 = InteriorLightDimming_rqst_DimUpContinously;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_16 = InteriorLightDimming_rqst_DimDownContinously;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_17 = InteriorLightDimming_rqst_Error;
  InteriorLightDimming_rqst_T Test_InteriorLightDimming_rqst_T_V_18 = InteriorLightDimming_rqst_NotAvailable;

  Issm_IssStateType Test_Issm_IssStateType_V_1 = ISSM_STATE_INACTIVE;
  Issm_IssStateType Test_Issm_IssStateType_V_2 = ISSM_STATE_PENDING;
  Issm_IssStateType Test_Issm_IssStateType_V_3 = ISSM_STATE_ACTIVE;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  Rte_DT_InteriorLightMode_rqst_T_1 Test_Rte_DT_InteriorLightMode_rqst_T_1_V_1 = EventFlag_Low;
  Rte_DT_InteriorLightMode_rqst_T_1 Test_Rte_DT_InteriorLightMode_rqst_T_1_V_2 = EventFlag_High;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
