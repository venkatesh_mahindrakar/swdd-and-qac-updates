/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_RoofHatch_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_RoofHatch_HMICtrl.h"
#include "TSC_RoofHatch_HMICtrl.h"








Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH1RoofhatchCloseBtn_Stat_PushButtonStatus(data);
}

Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH1RoofhatchOpenBtn_Stat_PushButtonStatus(data);
}

Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(data);
}

Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(data);
}

Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_RoofHatch_SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data)
{
  return Rte_Read_RoofHatch_SwitchStatus_A3PosSwitchStatus(data);
}

Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Living_Living(data);
}




Std_ReturnType TSC_RoofHatch_HMICtrl_Rte_Write_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst(RoofHatch_HMI_rqst_T data)
{
  return Rte_Write_RoofHatch_HMI_rqst_RoofHatch_HMI_rqst(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T  TSC_RoofHatch_HMICtrl_Rte_Prm_P1GCD_RoofHatch_FlexibleSwitchLogic_v(void)
{
  return (SEWS_RoofHatch_FlexibleSwitchLogic_P1GCD_T ) Rte_Prm_P1GCD_RoofHatch_FlexibleSwitchLogic_v();
}
boolean  TSC_RoofHatch_HMICtrl_Rte_Prm_P1CW8_RoofHatchCtrl_Act_v(void)
{
  return (boolean ) Rte_Prm_P1CW8_RoofHatchCtrl_Act_v();
}


     /* RoofHatch_HMICtrl */
      /* RoofHatch_HMICtrl */



