/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  IoHwAb_QM_IO.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  IoHwAb_QM_IO
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <IoHwAb_QM_IO>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IOHWAB_BOOL
 *   
 *
 * IOHWAB_SINT8
 *   
 *
 * IOHWAB_UINT16
 *   
 *
 * IOHWAB_UINT8
 *   
 *
 * Rte_DT_EcuHwDioCtrlArray_T_0
 *   
 *
 * Rte_DT_EcuHwFaultValues_T_0
 *   
 *
 * SEWS_Diag_Act_DOWHS01_P1V6O_T
 *   
 *
 * SEWS_Diag_Act_DOWHS02_P1V6P_T
 *   
 *
 * SEWS_Diag_Act_DOWLS02_P1V7E_T
 *   
 *
 * SEWS_Diag_Act_DOWLS03_P1V7F_T
 *   
 *
 * SEWS_HwToleranceThreshold_X1C04_T
 *   
 *
 * SEWS_P1V60_ContactClosed_T
 *   
 *
 * SEWS_P1V60_ContactOpen_T
 *   
 *
 * SEWS_P1V61_ContactClosed_T
 *   
 *
 * SEWS_P1V61_ContactOpen_T
 *   
 *
 * SEWS_P1V62_ContactClosed_T
 *   
 *
 * SEWS_P1V62_ContactOpen_T
 *   
 *
 * SEWS_P1V63_ContactClosed_T
 *   
 *
 * SEWS_P1V63_ContactOpen_T
 *   
 *
 * SEWS_P1V64_ContactClosed_T
 *   
 *
 * SEWS_P1V64_ContactOpen_T
 *   
 *
 * SEWS_P1V65_ContactClosed_T
 *   
 *
 * SEWS_P1V65_ContactOpen_T
 *   
 *
 * SEWS_P1V66_ContactClosed_T
 *   
 *
 * SEWS_P1V66_ContactOpen_T
 *   
 *
 * SEWS_P1V67_ContactClosed_T
 *   
 *
 * SEWS_P1V67_ContactOpen_T
 *   
 *
 * SEWS_P1V68_ContactClosed_T
 *   
 *
 * SEWS_P1V68_ContactOpen_T
 *   
 *
 * SEWS_P1V69_ContactClosed_T
 *   
 *
 * SEWS_P1V69_ContactOpen_T
 *   
 *
 * SEWS_P1V6U_Threshold_OC_STB_T
 *   
 *
 * SEWS_P1V6U_Threshold_STG_T
 *   
 *
 * SEWS_P1V6V_Threshold_OC_STB_T
 *   
 *
 * SEWS_P1V6V_Threshold_STG_T
 *   
 *
 * SEWS_P1V6W_ContactClosed_T
 *   
 *
 * SEWS_P1V6W_ContactOpen_T
 *   
 *
 * SEWS_P1V6X_ContactClosed_T
 *   
 *
 * SEWS_P1V6X_ContactOpen_T
 *   
 *
 * SEWS_P1V6Y_ContactClosed_T
 *   
 *
 * SEWS_P1V6Y_ContactOpen_T
 *   
 *
 * SEWS_P1V6Z_ContactClosed_T
 *   
 *
 * SEWS_P1V6Z_ContactOpen_T
 *   
 *
 * SEWS_P1V8F_Threshold_VAT_T
 *   
 *
 * SEWS_P1V8F_Threshold_VBT_T
 *   
 *
 * SEWS_P1WMD_ThresholdHigh_T
 *   
 *
 * SEWS_P1WMD_ThresholdLow_T
 *   
 *
 * SEWS_PcbConfig_Adi_X1CXW_T
 *   
 *
 * SEWS_PcbConfig_CanInterfaces_X1CX2_T
 *   
 *
 * SEWS_PcbConfig_DOWHS_X1CXY_T
 *   
 *
 * SEWS_PcbConfig_DOWLS_X1CXZ_T
 *   
 *
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T
 *   
 *
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *   
 *
 * SEWS_X1CX4_P1Interface_T
 *   
 *
 * SEWS_X1CX4_P2Interface_T
 *   
 *
 * SEWS_X1CX4_P3Interface_T
 *   
 *
 * SEWS_X1CX4_P4Interface_T
 *   
 *
 * SEWS_X1CX4_PiInterface_T
 *   
 *
 * SEWS_X1CY1_DigitalBiLevelHigh_T
 *   
 *
 * SEWS_X1CY1_DigitalBiLevelLow_T
 *   
 *
 * VGTT_EcuPinFaultStatus
 *   
 *
 * VGTT_EcuPinVoltage_0V2
 *   
 *
 * VGTT_EcuPwmDutycycle
 *   
 *
 * VGTT_EcuPwmPeriod
 *   
 *
 *********************************************************************************************************************/

#include "Rte_IoHwAb_QM_IO.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_IoHwAb_QM_IO.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void IoHwAb_QM_IO_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_BOOL: Boolean
 * IOHWAB_SINT8: Integer in interval [-128...127]
 * IOHWAB_UINT16: Integer in interval [0...65535]
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS01_P1V6O_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS02_P1V6P_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS02_P1V7E_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS03_P1V7F_T: Integer in interval [0...255]
 * SEWS_HwToleranceThreshold_X1C04_T: Integer in interval [0...255]
 * SEWS_P1V60_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V60_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V61_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V61_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V62_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V62_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V63_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V63_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V64_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V64_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V65_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V65_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V66_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V66_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V67_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V67_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V68_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V68_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V69_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V69_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6U_Threshold_OC_STB_T: Integer in interval [0...255]
 * SEWS_P1V6U_Threshold_STG_T: Integer in interval [0...255]
 * SEWS_P1V6V_Threshold_OC_STB_T: Integer in interval [0...255]
 * SEWS_P1V6V_Threshold_STG_T: Integer in interval [0...255]
 * SEWS_P1V6W_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6W_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6X_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6X_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6Y_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6Y_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V6Z_ContactClosed_T: Integer in interval [0...255]
 * SEWS_P1V6Z_ContactOpen_T: Integer in interval [0...255]
 * SEWS_P1V8F_Threshold_VAT_T: Integer in interval [0...255]
 * SEWS_P1V8F_Threshold_VBT_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdHigh_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdLow_T: Integer in interval [0...255]
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T: Integer in interval [0...255]
 * SEWS_X1CX4_P1Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P2Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P3Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P4Interface_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * VGTT_EcuPwmDutycycle: Integer in interval [-128...127]
 * VGTT_EcuPwmPeriod: Integer in interval [0...65535]
 * boolean: Boolean (standard type)
 * sint8: Integer in interval [-128...127] (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * IOCtrlReq_T: Enumeration of integer in interval [0...255] with enumerators
 *   IOCtrl_AppRequest (0U)
 *   IOCtrl_DiagReturnCtrlToApp (1U)
 *   IOCtrl_DiagShortTermAdjust (2U)
 * Rte_DT_EcuHwDioCtrlArray_T_0: Enumeration of integer in interval [0...255] with enumerators
 *   STD_LOW (0U)
 *   STD_HIGH (1U)
 *   Inactive (255U)
 * Rte_DT_EcuHwFaultValues_T_0: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * SEWS_PcbConfig_Adi_X1CXW_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated (0U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration (1U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration (2U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration (3U)
 * SEWS_PcbConfig_CanInterfaces_X1CX2_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T_NotPopulated (0U)
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T_Populated (1U)
 * SEWS_PcbConfig_DOWHS_X1CXY_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_Populated (1U)
 * SEWS_PcbConfig_DOWLS_X1CXZ_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated (1U)
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated (0U)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated (1U)
 * SEWS_X1CX4_PiInterface_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_X1CX4_PiInterface_T_NotPopulated (0U)
 *   SEWS_X1CX4_PiInterface_T_Populated (1U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * EcuHwDioCtrlArray_T: Array with 40 element(s) of type Rte_DT_EcuHwDioCtrlArray_T_0
 * EcuHwFaultValues_T: Array with 40 element(s) of type Rte_DT_EcuHwFaultValues_T_0
 * EcuHwVoltageValues_T: Array with 40 element(s) of type VGTT_EcuPinVoltage_0V2
 * SEWS_AdiWakeUpConfig_P1WMD_a_T: Array with 16 element(s) of type SEWS_AdiWakeUpConfig_P1WMD_s_T
 * SEWS_PcbConfig_Adi_X1CXW_a_T: Array with 19 element(s) of type SEWS_PcbConfig_Adi_X1CXW_T
 * SEWS_PcbConfig_CanInterfaces_X1CX2_a_T: Array with 6 element(s) of type SEWS_PcbConfig_CanInterfaces_X1CX2_T
 * SEWS_PcbConfig_DOWHS_X1CXY_a_T: Array with 2 element(s) of type SEWS_PcbConfig_DOWHS_X1CXY_T
 * SEWS_PcbConfig_DOWLS_X1CXZ_a_T: Array with 3 element(s) of type SEWS_PcbConfig_DOWLS_X1CXZ_T
 * SEWS_PcbConfig_LinInterfaces_X1CX0_a_T: Array with 7 element(s) of type SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *
 * Record Types:
 * =============
 * SEWS_AdiWakeUpConfig_P1WMD_s_T: Record with elements
 *   ThresholdHigh of type SEWS_P1WMD_ThresholdHigh_T
 *   ThresholdLow of type SEWS_P1WMD_ThresholdLow_T
 *   isActiveInLiving of type boolean
 *   isActiveInParked of type boolean
 * SEWS_DAI_Installed_P1WMP_s_T: Record with elements
 *   LeftDoor of type boolean
 *   RightDoor of type boolean
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 * SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T: Record with elements
 *   Threshold_VBT of type SEWS_P1V8F_Threshold_VBT_T
 *   Threshold_VAT of type SEWS_P1V8F_Threshold_VAT_T
 * SEWS_Fault_Config_ADI01_P1V6U_s_T: Record with elements
 *   Threshold_OC_STB of type SEWS_P1V6U_Threshold_OC_STB_T
 *   Threshold_STG of type SEWS_P1V6U_Threshold_STG_T
 * SEWS_Fault_Config_ADI02_P1V6V_s_T: Record with elements
 *   Threshold_OC_STB of type SEWS_P1V6V_Threshold_OC_STB_T
 *   Threshold_STG of type SEWS_P1V6V_Threshold_STG_T
 * SEWS_Fault_Config_ADI03_P1V6W_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6W_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6W_ContactClosed_T
 * SEWS_Fault_Config_ADI04_P1V6X_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6X_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6X_ContactClosed_T
 * SEWS_Fault_Config_ADI05_P1V6Y_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6Y_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6Y_ContactClosed_T
 * SEWS_Fault_Config_ADI06_P1V6Z_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V6Z_ContactOpen_T
 *   ContactClosed of type SEWS_P1V6Z_ContactClosed_T
 * SEWS_Fault_Config_ADI07_P1V60_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V60_ContactOpen_T
 *   ContactClosed of type SEWS_P1V60_ContactClosed_T
 * SEWS_Fault_Config_ADI08_P1V61_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V61_ContactOpen_T
 *   ContactClosed of type SEWS_P1V61_ContactClosed_T
 * SEWS_Fault_Config_ADI09_P1V62_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V62_ContactOpen_T
 *   ContactClosed of type SEWS_P1V62_ContactClosed_T
 * SEWS_Fault_Config_ADI10_P1V63_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V63_ContactOpen_T
 *   ContactClosed of type SEWS_P1V63_ContactClosed_T
 * SEWS_Fault_Config_ADI11_P1V64_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V64_ContactOpen_T
 *   ContactClosed of type SEWS_P1V64_ContactClosed_T
 * SEWS_Fault_Config_ADI12_P1V65_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V65_ContactOpen_T
 *   ContactClosed of type SEWS_P1V65_ContactClosed_T
 * SEWS_Fault_Config_ADI13_P1V66_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V66_ContactOpen_T
 *   ContactClosed of type SEWS_P1V66_ContactClosed_T
 * SEWS_Fault_Config_ADI14_P1V67_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V67_ContactOpen_T
 *   ContactClosed of type SEWS_P1V67_ContactClosed_T
 * SEWS_Fault_Config_ADI15_P1V68_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V68_ContactOpen_T
 *   ContactClosed of type SEWS_P1V68_ContactClosed_T
 * SEWS_Fault_Config_ADI16_P1V69_s_T: Record with elements
 *   ContactOpen of type SEWS_P1V69_ContactOpen_T
 *   ContactClosed of type SEWS_P1V69_ContactClosed_T
 * SEWS_PcbConfig_AdiPullUp_X1CX5_s_T: Record with elements
 *   AdiPullupLiving of type boolean
 *   AdiPullupParked of type boolean
 * SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T: Record with elements
 *   PiInterface of type SEWS_X1CX4_PiInterface_T
 *   P1Interface of type SEWS_X1CX4_P1Interface_T
 *   P2Interface of type SEWS_X1CX4_P2Interface_T
 *   P3Interface of type SEWS_X1CX4_P3Interface_T
 *   P4Interface of type SEWS_X1CX4_P4Interface_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HwToleranceThreshold_X1C04_T Rte_Prm_X1C04_HwToleranceThreshold_v(void)
 *   SEWS_PcbConfig_DoorAccessIf_X1CX3_T Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(void)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T *Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_LinInterfaces_X1CX0_T* is of type SEWS_PcbConfig_LinInterfaces_X1CX0_a_T
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T *Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_CanInterfaces_X1CX2_T* is of type SEWS_PcbConfig_CanInterfaces_X1CX2_a_T
 *   SEWS_PcbConfig_Adi_X1CXW_T *Rte_Prm_X1CXW_PcbConfig_Adi_v(void)
 *     Returnvalue: SEWS_PcbConfig_Adi_X1CXW_T* is of type SEWS_PcbConfig_Adi_X1CXW_a_T
 *   SEWS_PcbConfig_DOWHS_X1CXY_T *Rte_Prm_X1CXY_PcbConfig_DOWHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWHS_X1CXY_T* is of type SEWS_PcbConfig_DOWHS_X1CXY_a_T
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T *Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWLS_X1CXZ_T* is of type SEWS_PcbConfig_DOWLS_X1CXZ_a_T
 *   SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T *Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v(void)
 *   SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(void)
 *   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
 *   SEWS_Diag_Act_DOWHS01_P1V6O_T Rte_Prm_P1V6O_Diag_Act_DOWHS01_v(void)
 *   SEWS_Diag_Act_DOWHS02_P1V6P_T Rte_Prm_P1V6P_Diag_Act_DOWHS02_v(void)
 *   SEWS_Diag_Act_DOWLS02_P1V7E_T Rte_Prm_P1V7E_Diag_Act_DOWLS02_v(void)
 *   SEWS_Diag_Act_DOWLS03_P1V7F_T Rte_Prm_P1V7F_Diag_Act_DOWLS03_v(void)
 *   boolean Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v(void)
 *   boolean Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v(void)
 *   boolean Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v(void)
 *   boolean Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v(void)
 *   SEWS_AdiWakeUpConfig_P1WMD_s_T *Rte_Prm_P1WMD_AdiWakeUpConfig_v(void)
 *     Returnvalue: SEWS_AdiWakeUpConfig_P1WMD_s_T* is of type SEWS_AdiWakeUpConfig_P1WMD_a_T
 *   SEWS_Fault_Config_ADI07_P1V60_s_T *Rte_Prm_P1V60_Fault_Config_ADI07_v(void)
 *   SEWS_Fault_Config_ADI08_P1V61_s_T *Rte_Prm_P1V61_Fault_Config_ADI08_v(void)
 *   SEWS_Fault_Config_ADI09_P1V62_s_T *Rte_Prm_P1V62_Fault_Config_ADI09_v(void)
 *   SEWS_Fault_Config_ADI10_P1V63_s_T *Rte_Prm_P1V63_Fault_Config_ADI10_v(void)
 *   SEWS_Fault_Config_ADI11_P1V64_s_T *Rte_Prm_P1V64_Fault_Config_ADI11_v(void)
 *   SEWS_Fault_Config_ADI12_P1V65_s_T *Rte_Prm_P1V65_Fault_Config_ADI12_v(void)
 *   SEWS_Fault_Config_ADI13_P1V66_s_T *Rte_Prm_P1V66_Fault_Config_ADI13_v(void)
 *   SEWS_Fault_Config_ADI14_P1V67_s_T *Rte_Prm_P1V67_Fault_Config_ADI14_v(void)
 *   SEWS_Fault_Config_ADI15_P1V68_s_T *Rte_Prm_P1V68_Fault_Config_ADI15_v(void)
 *   SEWS_Fault_Config_ADI16_P1V69_s_T *Rte_Prm_P1V69_Fault_Config_ADI16_v(void)
 *   SEWS_Fault_Config_ADI01_P1V6U_s_T *Rte_Prm_P1V6U_Fault_Config_ADI01_v(void)
 *   SEWS_Fault_Config_ADI02_P1V6V_s_T *Rte_Prm_P1V6V_Fault_Config_ADI02_v(void)
 *   SEWS_Fault_Config_ADI03_P1V6W_s_T *Rte_Prm_P1V6W_Fault_Config_ADI03_v(void)
 *   SEWS_Fault_Config_ADI04_P1V6X_s_T *Rte_Prm_P1V6X_Fault_Config_ADI04_v(void)
 *   SEWS_Fault_Config_ADI05_P1V6Y_s_T *Rte_Prm_P1V6Y_Fault_Config_ADI05_v(void)
 *   SEWS_Fault_Config_ADI06_P1V6Z_s_T *Rte_Prm_P1V6Z_Fault_Config_ADI06_v(void)
 *   SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T *Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v(void)
 *   SEWS_DAI_Installed_P1WMP_s_T *Rte_Prm_P1WMP_DAI_Installed_v(void)
 *   boolean Rte_Prm_P1WPP_isSecurityLinActive_v(void)
 *
 *********************************************************************************************************************/


#define IoHwAb_QM_IO_START_SEC_CODE
#include "IoHwAb_QM_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AdiInterface_P_GetAdiPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetAdiPinState_CS> of PortPrototype <AdiInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_GetAdiPinState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) AdiPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_GetAdiPinState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  EcuHwDioCtrlArray_T AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray;
  EcuHwFaultValues_T AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  TSC_IoHwAb_QM_IO_Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray(AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray);
  TSC_IoHwAb_QM_IO_Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus(AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus);

  IoHwAb_QM_IO_TestDefines();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AdiInterface_P_GetPullUpState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetPullUpState_CS> of PortPrototype <AdiInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType AdiInterface_P_GetPullUpState_CS(IOHWAB_BOOL *isPullUpActive_Strong, IOHWAB_BOOL *isPullUpActive_Weak, IOHWAB_BOOL *isPullUpActive_DAI)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_GetPullUpState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) AdiInterface_P_GetPullUpState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Strong, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Weak, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_DAI) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_GetPullUpState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  EcuHwDioCtrlArray_T AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray;
  EcuHwFaultValues_T AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  TSC_IoHwAb_QM_IO_Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray(AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray);
  TSC_IoHwAb_QM_IO_Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus(AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: AdiInterface_P_SetPullUp_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetPullUp_CS> of PortPrototype <AdiInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType AdiInterface_P_SetPullUp_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp, IOHWAB_BOOL ActivateWeakPullUp, IOHWAB_BOOL ActivateDAIPullUp)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_AdiInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_SetPullUp_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) AdiInterface_P_SetPullUp_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp, IOHWAB_BOOL ActivateWeakPullUp, IOHWAB_BOOL ActivateDAIPullUp) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: AdiInterface_P_SetPullUp_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_GetDcdc12VState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDcdc12VState_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_GetDcdc12VState_CS(VGTT_EcuPinVoltage_0V2 *DcDc12vRefVoltage, IOHWAB_BOOL *IsDcDc12vActivated, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_GetDcdc12VState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_GetDcdc12VState_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DcDc12vRefVoltage, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDcDc12vActivated, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_GetDcdc12VState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  EcuHwDioCtrlArray_T Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray;
  EcuHwFaultValues_T Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  TSC_IoHwAb_QM_IO_Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray(Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray);
  TSC_IoHwAb_QM_IO_Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus(Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_GetDo12VPinsState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDo12VPinsState_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, IOHWAB_BOOL *IsDo12VActivated, VGTT_EcuPinVoltage_0V2 *Do12VPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_GetDo12VPinsState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDo12VActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Do12VPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_GetDo12VPinsState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  EcuHwDioCtrlArray_T Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray;
  EcuHwFaultValues_T Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  TSC_IoHwAb_QM_IO_Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray);
  TSC_IoHwAb_QM_IO_Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_SetDcdc12VActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDcdc12VActive_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDcdc12VActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDcdc12VActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_SetDo12VLivingActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDo12VLivingActive_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDo12VLivingActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDo12VLivingActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Do12VInterface_P_SetDo12VParkedActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDo12VParkedActive_CS> of PortPrototype <Do12VInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_Do12VInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDo12VParkedActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Do12VInterface_P_SetDo12VParkedActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DoblsCtrlInterface_P_GetDoblsPinState_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDoblsPinState_CS> of PortPrototype <DoblsCtrlInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DoblsCtrlInterface_P_GetDoblsPinState_CS(IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DoblsCtrlInterface_P_GetDoblsPinState_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DoblsCtrlInterface_P_GetDoblsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DoblsCtrlInterface_P_GetDoblsPinState_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  EcuHwDioCtrlArray_T DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray;
  EcuHwFaultValues_T DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  TSC_IoHwAb_QM_IO_Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray(DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray);
  TSC_IoHwAb_QM_IO_Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus(DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DoblsCtrlInterface_P_SetDoblsActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDoblsActive_CS> of PortPrototype <DoblsCtrlInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DoblsCtrlInterface_P_SetDoblsActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DoblsCtrlInterface_P_SetDoblsActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DowhsInterface_P_GetDoPinStateOne_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDoPinStateOne_CS> of PortPrototype <DowhsInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DowhsInterface_P_GetDoPinStateOne_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DowhsInterface_P_GetDoPinStateOne_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  EcuHwDioCtrlArray_T DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray;
  EcuHwFaultValues_T DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  TSC_IoHwAb_QM_IO_Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray);
  TSC_IoHwAb_QM_IO_Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DowhsInterface_P_SetDowActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDowActive_CS> of PortPrototype <DowhsInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DowhsInterface_P_SetDowActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DowhsInterface_P_SetDowActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DowlsInterface_P_GetDoPinStateOne_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetDoPinStateOne_CS> of PortPrototype <DowlsInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, IOHWAB_BOOL *IsDoActivated, VGTT_EcuPinVoltage_0V2 *DoPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPwmDutycycle *DutyCycle, VGTT_EcuPwmPeriod *Period, VGTT_EcuPinFaultStatus *DiagStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DowlsInterface_P_GetDoPinStateOne_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DowlsInterface_P_GetDoPinStateOne_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  EcuHwDioCtrlArray_T DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray;
  EcuHwFaultValues_T DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  TSC_IoHwAb_QM_IO_Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray);
  TSC_IoHwAb_QM_IO_Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DowlsInterface_P_SetDowActive_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <SetDowActive_CS> of PortPrototype <DowlsInterface_P>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DowxsInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DowlsInterface_P_SetDowActive_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DowlsInterface_P_SetDowActive_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: IoHwAb_QM_IO_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
 *   Std_ReturnType Rte_Read_ScimPvtControl_P_Status(uint8 *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(const Rte_DT_EcuHwDioCtrlArray_T_0 *data)
 *   void Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus(const Rte_DT_EcuHwFaultValues_T_0 *data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_EcuHwState_P_GetEcuVoltages_CS(VGTT_EcuPinVoltage_0V2 *EcuVoltageValues)
 *     Argument EcuVoltageValues: VGTT_EcuPinVoltage_0V2* is of type EcuHwVoltageValues_T
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_EcuHwState_I_AdcInFailure
 *   Std_ReturnType Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_VbatInterface_I_AdcInFailure, RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: IoHwAb_QM_IO_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, IoHwAb_QM_IO_CODE) IoHwAb_QM_IO_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: IoHwAb_QM_IO_10ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  DiagActiveState_T Read_DiagActiveState_P_isDiagActive;
  Fsc_OperationalMode_T Read_Fsc_OperationalMode_P_Fsc_OperationalMode;
  uint8 Read_ScimPvtControl_P_Status;
  VehicleMode_T Read_VehicleModeInternal_VehicleMode;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_Fault_Config_ADI07_P1V60_s_T P1V60_Fault_Config_ADI07_v_data;
  SEWS_Fault_Config_ADI08_P1V61_s_T P1V61_Fault_Config_ADI08_v_data;
  SEWS_Fault_Config_ADI09_P1V62_s_T P1V62_Fault_Config_ADI09_v_data;
  SEWS_Fault_Config_ADI10_P1V63_s_T P1V63_Fault_Config_ADI10_v_data;
  SEWS_Fault_Config_ADI11_P1V64_s_T P1V64_Fault_Config_ADI11_v_data;
  SEWS_Fault_Config_ADI12_P1V65_s_T P1V65_Fault_Config_ADI12_v_data;
  SEWS_Fault_Config_ADI13_P1V66_s_T P1V66_Fault_Config_ADI13_v_data;
  SEWS_Fault_Config_ADI14_P1V67_s_T P1V67_Fault_Config_ADI14_v_data;
  SEWS_Fault_Config_ADI15_P1V68_s_T P1V68_Fault_Config_ADI15_v_data;
  SEWS_Fault_Config_ADI16_P1V69_s_T P1V69_Fault_Config_ADI16_v_data;
  SEWS_Fault_Config_ADI01_P1V6U_s_T P1V6U_Fault_Config_ADI01_v_data;
  SEWS_Fault_Config_ADI02_P1V6V_s_T P1V6V_Fault_Config_ADI02_v_data;
  SEWS_Fault_Config_ADI03_P1V6W_s_T P1V6W_Fault_Config_ADI03_v_data;
  SEWS_Fault_Config_ADI04_P1V6X_s_T P1V6X_Fault_Config_ADI04_v_data;
  SEWS_Fault_Config_ADI05_P1V6Y_s_T P1V6Y_Fault_Config_ADI05_v_data;
  SEWS_Fault_Config_ADI06_P1V6Z_s_T P1V6Z_Fault_Config_ADI06_v_data;
  SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T P1V8F_Fault_Cfg_DcDc12v_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  EcuHwDioCtrlArray_T IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray;

  EcuHwDioCtrlArray_T IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray_Write;
  EcuHwFaultValues_T IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus_Write;

  EcuHwVoltageValues_T Call_EcuHwState_P_GetEcuVoltages_CS_EcuVoltageValues = {
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 
  0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U, 0U
};
  VGTT_EcuPinVoltage_0V2 Call_VbatInterface_P_GetVbatVoltage_CS_BatteryVoltage = 0U;
  VGTT_EcuPinFaultStatus Call_VbatInterface_P_GetVbatVoltage_CS_FaultStatus = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_IoHwAb_QM_IO_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1V60_Fault_Config_ADI07_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V60_Fault_Config_ADI07_v();
  P1V61_Fault_Config_ADI08_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V61_Fault_Config_ADI08_v();
  P1V62_Fault_Config_ADI09_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V62_Fault_Config_ADI09_v();
  P1V63_Fault_Config_ADI10_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V63_Fault_Config_ADI10_v();
  P1V64_Fault_Config_ADI11_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V64_Fault_Config_ADI11_v();
  P1V65_Fault_Config_ADI12_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V65_Fault_Config_ADI12_v();
  P1V66_Fault_Config_ADI13_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V66_Fault_Config_ADI13_v();
  P1V67_Fault_Config_ADI14_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V67_Fault_Config_ADI14_v();
  P1V68_Fault_Config_ADI15_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V68_Fault_Config_ADI15_v();
  P1V69_Fault_Config_ADI16_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V69_Fault_Config_ADI16_v();
  P1V6U_Fault_Config_ADI01_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6U_Fault_Config_ADI01_v();
  P1V6V_Fault_Config_ADI02_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6V_Fault_Config_ADI02_v();
  P1V6W_Fault_Config_ADI03_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6W_Fault_Config_ADI03_v();
  P1V6X_Fault_Config_ADI04_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6X_Fault_Config_ADI04_v();
  P1V6Y_Fault_Config_ADI05_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Y_Fault_Config_ADI05_v();
  P1V6Z_Fault_Config_ADI06_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V6Z_Fault_Config_ADI06_v();
  P1V8F_Fault_Cfg_DcDc12v_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v();
  P1WMP_DAI_Installed_v_data = *TSC_IoHwAb_QM_IO_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_IoHwAb_QM_IO_Rte_Prm_P1WPP_isSecurityLinActive_v();

  fct_status = TSC_IoHwAb_QM_IO_Rte_Read_DiagActiveState_P_isDiagActive(&Read_DiagActiveState_P_isDiagActive);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_IoHwAb_QM_IO_Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(&Read_Fsc_OperationalMode_P_Fsc_OperationalMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_IoHwAb_QM_IO_Rte_Read_ScimPvtControl_P_Status(&Read_ScimPvtControl_P_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_IoHwAb_QM_IO_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  TSC_IoHwAb_QM_IO_Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray);

  (void)memset(&IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray_Write, 0, sizeof(IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray_Write));
  TSC_IoHwAb_QM_IO_Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray_Write);
  (void)memset(&IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus_Write, 0, sizeof(IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus_Write));
  TSC_IoHwAb_QM_IO_Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus(IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus_Write);

  fct_status = TSC_IoHwAb_QM_IO_Rte_Call_EcuHwState_P_GetEcuVoltages_CS(Call_EcuHwState_P_GetEcuVoltages_CS_EcuVoltageValues);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_EcuHwState_I_AdcInFailure:
      fct_error = 1;
      break;
  }

  fct_status = TSC_IoHwAb_QM_IO_Rte_Call_VbatInterface_P_GetVbatVoltage_CS(&Call_VbatInterface_P_GetVbatVoltage_CS_BatteryVoltage, &Call_VbatInterface_P_GetVbatVoltage_CS_FaultStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_VbatInterface_I_AdcInFailure:
      fct_error = 1;
      break;
    case RTE_E_VbatInterface_I_IoHwAbApplicationError:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define IoHwAb_QM_IO_STOP_SEC_CODE
#include "IoHwAb_QM_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void IoHwAb_QM_IO_TestDefines(void)
{
  /* Enumeration Data Types */

  DiagActiveState_T Test_DiagActiveState_T_V_1 = Diag_Active_FALSE;
  DiagActiveState_T Test_DiagActiveState_T_V_2 = Diag_Active_TRUE;

  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_1 = FSC_ShutdownReady;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_2 = FSC_Reduced_12vDcDcLimit;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_3 = FSC_Reduced;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_4 = FSC_Operating;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_5 = FSC_Protecting;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_6 = FSC_Withstand;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_7 = FSC_NotAvailable;

  IOCtrlReq_T Test_IOCtrlReq_T_V_1 = IOCtrl_AppRequest;
  IOCtrlReq_T Test_IOCtrlReq_T_V_2 = IOCtrl_DiagReturnCtrlToApp;
  IOCtrlReq_T Test_IOCtrlReq_T_V_3 = IOCtrl_DiagShortTermAdjust;

  Rte_DT_EcuHwDioCtrlArray_T_0 Test_Rte_DT_EcuHwDioCtrlArray_T_0_V_1 = STD_LOW;
  Rte_DT_EcuHwDioCtrlArray_T_0 Test_Rte_DT_EcuHwDioCtrlArray_T_0_V_2 = STD_HIGH;
  Rte_DT_EcuHwDioCtrlArray_T_0 Test_Rte_DT_EcuHwDioCtrlArray_T_0_V_3 = Inactive;

  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_1 = TestNotRun;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_2 = OffState_NoFaultDetected;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_3 = OffState_FaultDetected_STG;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_4 = OffState_FaultDetected_STB;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_5 = OffState_FaultDetected_OC;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_6 = OffState_FaultDetected_VBT;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_7 = OffState_FaultDetected_VAT;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_8 = OnState_NoFaultDetected;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_9 = OnState_FaultDetected_STG;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_10 = OnState_FaultDetected_STB;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_11 = OnState_FaultDetected_OC;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_12 = OnState_FaultDetected_VBT;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_13 = OnState_FaultDetected_VAT;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_14 = OnState_FaultDetected_VOR;
  Rte_DT_EcuHwFaultValues_T_0 Test_Rte_DT_EcuHwFaultValues_T_0_V_15 = OnState_FaultDetected_CAT;

  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_1 = SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated;
  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_2 = SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration;
  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_3 = SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration;
  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_4 = SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration;

  SEWS_PcbConfig_CanInterfaces_X1CX2_T Test_SEWS_PcbConfig_CanInterfaces_X1CX2_T_V_1 = SEWS_PcbConfig_CanInterfaces_X1CX2_T_NotPopulated;
  SEWS_PcbConfig_CanInterfaces_X1CX2_T Test_SEWS_PcbConfig_CanInterfaces_X1CX2_T_V_2 = SEWS_PcbConfig_CanInterfaces_X1CX2_T_Populated;

  SEWS_PcbConfig_DOWHS_X1CXY_T Test_SEWS_PcbConfig_DOWHS_X1CXY_T_V_1 = SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated;
  SEWS_PcbConfig_DOWHS_X1CXY_T Test_SEWS_PcbConfig_DOWHS_X1CXY_T_V_2 = SEWS_PcbConfig_DOWHS_X1CXY_T_Populated;

  SEWS_PcbConfig_DOWLS_X1CXZ_T Test_SEWS_PcbConfig_DOWLS_X1CXZ_T_V_1 = SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated;
  SEWS_PcbConfig_DOWLS_X1CXZ_T Test_SEWS_PcbConfig_DOWLS_X1CXZ_T_V_2 = SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated;

  SEWS_PcbConfig_LinInterfaces_X1CX0_T Test_SEWS_PcbConfig_LinInterfaces_X1CX0_T_V_1 = SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated;
  SEWS_PcbConfig_LinInterfaces_X1CX0_T Test_SEWS_PcbConfig_LinInterfaces_X1CX0_T_V_2 = SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated;

  SEWS_X1CX4_PiInterface_T Test_SEWS_X1CX4_PiInterface_T_V_1 = SEWS_X1CX4_PiInterface_T_NotPopulated;
  SEWS_X1CX4_PiInterface_T Test_SEWS_X1CX4_PiInterface_T_V_2 = SEWS_X1CX4_PiInterface_T_Populated;

  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_1 = TestNotRun;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_2 = OffState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_3 = OffState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_4 = OffState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_5 = OffState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_6 = OffState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_7 = OffState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_8 = OnState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_9 = OnState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_10 = OnState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_11 = OnState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_12 = OnState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_13 = OnState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_14 = OnState_FaultDetected_VOR;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_15 = OnState_FaultDetected_CAT;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
