/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  LpuMgr.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  LpuMgr
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <LpuMgr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_HwToleranceThreshold_X1C04_T
 *   
 *
 * SEWS_P1WMD_ThresholdHigh_T
 *   
 *
 * SEWS_P1WMD_ThresholdLow_T
 *   
 *
 * SEWS_PcbConfig_Adi_X1CXW_T
 *   
 *
 * SEWS_PcbConfig_CanInterfaces_X1CX2_T
 *   
 *
 * SEWS_PcbConfig_DOBHS_X1CXX_T
 *   
 *
 * SEWS_PcbConfig_DOWHS_X1CXY_T
 *   
 *
 * SEWS_PcbConfig_DOWLS_X1CXZ_T
 *   
 *
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T
 *   
 *
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *   
 *
 * SEWS_X1CX4_P1Interface_T
 *   
 *
 * SEWS_X1CX4_P2Interface_T
 *   
 *
 * SEWS_X1CX4_P3Interface_T
 *   
 *
 * SEWS_X1CX4_P4Interface_T
 *   
 *
 * SEWS_X1CX4_PiInterface_T
 *   
 *
 * SEWS_X1CY1_DigitalBiLevelHigh_T
 *   
 *
 * SEWS_X1CY1_DigitalBiLevelLow_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_LpuMgr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_LpuMgr.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void LpuMgr_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_HwToleranceThreshold_X1C04_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdHigh_T: Integer in interval [0...255]
 * SEWS_P1WMD_ThresholdLow_T: Integer in interval [0...255]
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T: Integer in interval [0...255]
 * SEWS_X1CX4_P1Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P2Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P3Interface_T: Integer in interval [0...255]
 * SEWS_X1CX4_P4Interface_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelHigh_T: Integer in interval [0...255]
 * SEWS_X1CY1_DigitalBiLevelLow_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * SEWS_PcbConfig_Adi_X1CXW_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated (0U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration (1U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration (2U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration (3U)
 * SEWS_PcbConfig_CanInterfaces_X1CX2_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T_NotPopulated (0U)
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T_Populated (1U)
 * SEWS_PcbConfig_DOBHS_X1CXX_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_Populated (1U)
 * SEWS_PcbConfig_DOWHS_X1CXY_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_Populated (1U)
 * SEWS_PcbConfig_DOWLS_X1CXZ_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated (1U)
 * SEWS_PcbConfig_LinInterfaces_X1CX0_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated (0U)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated (1U)
 * SEWS_X1CX4_PiInterface_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_X1CX4_PiInterface_T_NotPopulated (0U)
 *   SEWS_X1CX4_PiInterface_T_Populated (1U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * SEWS_AdiWakeUpConfig_P1WMD_a_T: Array with 16 element(s) of type SEWS_AdiWakeUpConfig_P1WMD_s_T
 * SEWS_PcbConfig_Adi_X1CXW_a_T: Array with 19 element(s) of type SEWS_PcbConfig_Adi_X1CXW_T
 * SEWS_PcbConfig_CanInterfaces_X1CX2_a_T: Array with 6 element(s) of type SEWS_PcbConfig_CanInterfaces_X1CX2_T
 * SEWS_PcbConfig_DOBHS_X1CXX_a_T: Array with 4 element(s) of type SEWS_PcbConfig_DOBHS_X1CXX_T
 * SEWS_PcbConfig_DOWHS_X1CXY_a_T: Array with 2 element(s) of type SEWS_PcbConfig_DOWHS_X1CXY_T
 * SEWS_PcbConfig_DOWLS_X1CXZ_a_T: Array with 3 element(s) of type SEWS_PcbConfig_DOWLS_X1CXZ_T
 * SEWS_PcbConfig_LinInterfaces_X1CX0_a_T: Array with 7 element(s) of type SEWS_PcbConfig_LinInterfaces_X1CX0_T
 *
 * Record Types:
 * =============
 * SEWS_AdiWakeUpConfig_P1WMD_s_T: Record with elements
 *   ThresholdHigh of type SEWS_P1WMD_ThresholdHigh_T
 *   ThresholdLow of type SEWS_P1WMD_ThresholdLow_T
 *   isActiveInLiving of type boolean
 *   isActiveInParked of type boolean
 * SEWS_DAI_Installed_P1WMP_s_T: Record with elements
 *   LeftDoor of type boolean
 *   RightDoor of type boolean
 * SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T: Record with elements
 *   DigitalBiLevelLow of type SEWS_X1CY1_DigitalBiLevelLow_T
 *   DigitalBiLevelHigh of type SEWS_X1CY1_DigitalBiLevelHigh_T
 * SEWS_PcbConfig_AdiPullUp_X1CX5_s_T: Record with elements
 *   AdiPullupLiving of type boolean
 *   AdiPullupParked of type boolean
 * SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T: Record with elements
 *   PiInterface of type SEWS_X1CX4_PiInterface_T
 *   P1Interface of type SEWS_X1CX4_P1Interface_T
 *   P2Interface of type SEWS_X1CX4_P2Interface_T
 *   P3Interface of type SEWS_X1CX4_P3Interface_T
 *   P4Interface of type SEWS_X1CX4_P4Interface_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_HwToleranceThreshold_X1C04_T Rte_Prm_X1C04_HwToleranceThreshold_v(void)
 *   SEWS_PcbConfig_DoorAccessIf_X1CX3_T Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(void)
 *   SEWS_PcbConfig_LinInterfaces_X1CX0_T *Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_LinInterfaces_X1CX0_T* is of type SEWS_PcbConfig_LinInterfaces_X1CX0_a_T
 *   SEWS_PcbConfig_CanInterfaces_X1CX2_T *Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(void)
 *     Returnvalue: SEWS_PcbConfig_CanInterfaces_X1CX2_T* is of type SEWS_PcbConfig_CanInterfaces_X1CX2_a_T
 *   SEWS_PcbConfig_Adi_X1CXW_T *Rte_Prm_X1CXW_PcbConfig_Adi_v(void)
 *     Returnvalue: SEWS_PcbConfig_Adi_X1CXW_T* is of type SEWS_PcbConfig_Adi_X1CXW_a_T
 *   SEWS_PcbConfig_DOBHS_X1CXX_T *Rte_Prm_X1CXX_PcbConfig_DOBHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOBHS_X1CXX_T* is of type SEWS_PcbConfig_DOBHS_X1CXX_a_T
 *   SEWS_PcbConfig_DOWHS_X1CXY_T *Rte_Prm_X1CXY_PcbConfig_DOWHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWHS_X1CXY_T* is of type SEWS_PcbConfig_DOWHS_X1CXY_a_T
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T *Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWLS_X1CXZ_T* is of type SEWS_PcbConfig_DOWLS_X1CXZ_a_T
 *   SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T *Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v(void)
 *   SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(void)
 *   SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
 *   boolean Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v(void)
 *   boolean Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v(void)
 *   boolean Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v(void)
 *   boolean Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v(void)
 *   SEWS_AdiWakeUpConfig_P1WMD_s_T *Rte_Prm_P1WMD_AdiWakeUpConfig_v(void)
 *     Returnvalue: SEWS_AdiWakeUpConfig_P1WMD_s_T* is of type SEWS_AdiWakeUpConfig_P1WMD_a_T
 *   SEWS_DAI_Installed_P1WMP_s_T *Rte_Prm_P1WMP_DAI_Installed_v(void)
 *   boolean Rte_Prm_P1WPP_isSecurityLinActive_v(void)
 *
 *********************************************************************************************************************/


#define LpuMgr_START_SEC_CODE
#include "LpuMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: LpToVapEntryRunnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_LpModeRunTime_P_LpModeRunTime(uint32 data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: LpToVapEntryRunnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LpuMgr_CODE) LpToVapEntryRunnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: LpToVapEntryRunnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_LpuMgr_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_LpuMgr_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LpuMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_LpuMgr_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_LpuMgr_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_LpuMgr_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_LpuMgr_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_LpuMgr_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_LpuMgr_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_LpuMgr_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_LpuMgr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_LpuMgr_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_LpuMgr_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_LpuMgr_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_LpuMgr_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_LpuMgr_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1WMP_DAI_Installed_v_data = *TSC_LpuMgr_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LpuMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  fct_status = TSC_LpuMgr_Rte_Write_LpModeRunTime_P_LpModeRunTime(Rte_InitValue_LpModeRunTime_P_LpModeRunTime);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  LpuMgr_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VapToLpEntryRunnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * This runnable is never executed by the RTE.
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_FSC_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VapToLpEntryRunnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, LpuMgr_CODE) VapToLpEntryRunnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VapToLpEntryRunnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Fsc_OperationalMode_T Read_FSC_OperationalMode_P_Fsc_OperationalMode;
  VehicleMode_T Read_VehicleModeInternal_VehicleMode;

  SEWS_HwToleranceThreshold_X1C04_T X1C04_HwToleranceThreshold_v_data;
  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_PcbConfig_LinInterfaces_X1CX0_a_T X1CX0_PcbConfig_LinInterfaces_v_data;
  SEWS_PcbConfig_CanInterfaces_X1CX2_a_T X1CX2_PcbConfig_CanInterfaces_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T X1CX4_PcbConfig_PassiveAntenna_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T X1CY1_DigitalBiLevelVoltageConfig_v_data;

  boolean P1WME_LowPowerPullUpAct_Parked_v_data;
  boolean P1WMF_LowPowerPullUpAct_Living_v_data;
  boolean P1WMN_LowPower12VOutputAct_Living_v_data;
  boolean P1WMO_LowPower12VOutputAct_Parked_v_data;
  SEWS_AdiWakeUpConfig_P1WMD_a_T P1WMD_AdiWakeUpConfig_v_data;
  SEWS_DAI_Installed_P1WMP_s_T P1WMP_DAI_Installed_v_data;

  boolean P1WPP_isSecurityLinActive_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1C04_HwToleranceThreshold_v_data = TSC_LpuMgr_Rte_Prm_X1C04_HwToleranceThreshold_v();
  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_LpuMgr_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  (void)memcpy(X1CX0_PcbConfig_LinInterfaces_v_data, TSC_LpuMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(), sizeof(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T));
  (void)memcpy(X1CX2_PcbConfig_CanInterfaces_v_data, TSC_LpuMgr_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(), sizeof(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T));
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_LpuMgr_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_LpuMgr_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_LpuMgr_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_LpuMgr_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX4_PcbConfig_PassiveAntenna_v_data = *TSC_LpuMgr_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v();
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_LpuMgr_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CY1_DigitalBiLevelVoltageConfig_v_data = *TSC_LpuMgr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();

  P1WME_LowPowerPullUpAct_Parked_v_data = TSC_LpuMgr_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v();
  P1WMF_LowPowerPullUpAct_Living_v_data = TSC_LpuMgr_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v();
  P1WMN_LowPower12VOutputAct_Living_v_data = TSC_LpuMgr_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v();
  P1WMO_LowPower12VOutputAct_Parked_v_data = TSC_LpuMgr_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v();
  (void)memcpy(P1WMD_AdiWakeUpConfig_v_data, TSC_LpuMgr_Rte_Prm_P1WMD_AdiWakeUpConfig_v(), sizeof(SEWS_AdiWakeUpConfig_P1WMD_a_T));
  P1WMP_DAI_Installed_v_data = *TSC_LpuMgr_Rte_Prm_P1WMP_DAI_Installed_v();

  P1WPP_isSecurityLinActive_v_data = TSC_LpuMgr_Rte_Prm_P1WPP_isSecurityLinActive_v();

  fct_status = TSC_LpuMgr_Rte_Read_FSC_OperationalMode_P_Fsc_OperationalMode(&Read_FSC_OperationalMode_P_Fsc_OperationalMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_LpuMgr_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define LpuMgr_STOP_SEC_CODE
#include "LpuMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void LpuMgr_TestDefines(void)
{
  /* Enumeration Data Types */

  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_1 = FSC_ShutdownReady;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_2 = FSC_Reduced_12vDcDcLimit;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_3 = FSC_Reduced;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_4 = FSC_Operating;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_5 = FSC_Protecting;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_6 = FSC_Withstand;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_7 = FSC_NotAvailable;

  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_1 = SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated;
  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_2 = SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration;
  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_3 = SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration;
  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_4 = SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration;

  SEWS_PcbConfig_CanInterfaces_X1CX2_T Test_SEWS_PcbConfig_CanInterfaces_X1CX2_T_V_1 = SEWS_PcbConfig_CanInterfaces_X1CX2_T_NotPopulated;
  SEWS_PcbConfig_CanInterfaces_X1CX2_T Test_SEWS_PcbConfig_CanInterfaces_X1CX2_T_V_2 = SEWS_PcbConfig_CanInterfaces_X1CX2_T_Populated;

  SEWS_PcbConfig_DOBHS_X1CXX_T Test_SEWS_PcbConfig_DOBHS_X1CXX_T_V_1 = SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated;
  SEWS_PcbConfig_DOBHS_X1CXX_T Test_SEWS_PcbConfig_DOBHS_X1CXX_T_V_2 = SEWS_PcbConfig_DOBHS_X1CXX_T_Populated;

  SEWS_PcbConfig_DOWHS_X1CXY_T Test_SEWS_PcbConfig_DOWHS_X1CXY_T_V_1 = SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated;
  SEWS_PcbConfig_DOWHS_X1CXY_T Test_SEWS_PcbConfig_DOWHS_X1CXY_T_V_2 = SEWS_PcbConfig_DOWHS_X1CXY_T_Populated;

  SEWS_PcbConfig_DOWLS_X1CXZ_T Test_SEWS_PcbConfig_DOWLS_X1CXZ_T_V_1 = SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated;
  SEWS_PcbConfig_DOWLS_X1CXZ_T Test_SEWS_PcbConfig_DOWLS_X1CXZ_T_V_2 = SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated;

  SEWS_PcbConfig_LinInterfaces_X1CX0_T Test_SEWS_PcbConfig_LinInterfaces_X1CX0_T_V_1 = SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated;
  SEWS_PcbConfig_LinInterfaces_X1CX0_T Test_SEWS_PcbConfig_LinInterfaces_X1CX0_T_V_2 = SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated;

  SEWS_X1CX4_PiInterface_T Test_SEWS_X1CX4_PiInterface_T_V_1 = SEWS_X1CX4_PiInterface_T_NotPopulated;
  SEWS_X1CX4_PiInterface_T Test_SEWS_X1CX4_PiInterface_T_V_2 = SEWS_X1CX4_PiInterface_T_Populated;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
