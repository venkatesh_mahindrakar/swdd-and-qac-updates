/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  ExtraBbContOrSlid_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  ExtraBbContOrSlid_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <ExtraBbContOrSlid_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_ContainerUnlockHMIDeviceType_P1CXO_T
 *   
 *
 * SEWS_Slid5thWheelTimeoutForReq_P1DV9_T
 *   
 *
 * SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_ExtraBbContOrSlid_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_ExtraBbContOrSlid_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void ExtraBbContOrSlid_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ContainerUnlockHMIDeviceType_P1CXO_T: Integer in interval [0...255]
 * SEWS_Slid5thWheelTimeoutForReq_P1DV9_T: Integer in interval [0...255]
 * SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ContainerUnlockHMIDeviceType_P1CXO_T Rte_Prm_P1CXO_ContainerUnlockHMIDeviceType_v(void)
 *   SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T Rte_Prm_P1CXP_Slidable5thWheelHMIDeviceType_v(void)
 *   SEWS_Slid5thWheelTimeoutForReq_P1DV9_T Rte_Prm_P1DV9_Slid5thWheelTimeoutForReq_v(void)
 *
 *********************************************************************************************************************/


#define ExtraBbContOrSlid_HMICtrl_START_SEC_CODE
#include "ExtraBbContOrSlid_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExtraBbContOrSlid_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ContUnlockSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_Slid5thWheelSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BBContainerUnlockRequest_BBContainerUnlockRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_BBSlidable5thWheelRequest_BBSlidable5thWheelRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_ContUnlock_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Slid5thWheel_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbContOrSlid_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExtraBbContOrSlid_HMICtrl_CODE) ExtraBbContOrSlid_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbContOrSlid_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  A2PosSwitchStatus_T Read_ContUnlockSwitchStatus_A2PosSwitchStatus;
  InactiveActive_T Read_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus;
  InactiveActive_T Read_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus;
  A2PosSwitchStatus_T Read_Slid5thWheelSwitchStatus_A2PosSwitchStatus;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;

  SEWS_ContainerUnlockHMIDeviceType_P1CXO_T P1CXO_ContainerUnlockHMIDeviceType_v_data;
  SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T P1CXP_Slidable5thWheelHMIDeviceType_v_data;
  SEWS_Slid5thWheelTimeoutForReq_P1DV9_T P1DV9_Slid5thWheelTimeoutForReq_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1CXO_ContainerUnlockHMIDeviceType_v_data = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Prm_P1CXO_ContainerUnlockHMIDeviceType_v();
  P1CXP_Slidable5thWheelHMIDeviceType_v_data = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Prm_P1CXP_Slidable5thWheelHMIDeviceType_v();
  P1DV9_Slid5thWheelTimeoutForReq_v_data = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Prm_P1DV9_Slid5thWheelTimeoutForReq_v();

  fct_status = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_ContUnlockSwitchStatus_A2PosSwitchStatus(&Read_ContUnlockSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus(&Read_ExtraBBContainerUnlockStatus_ExtraBBContainerUnlockStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus(&Read_ExtraBBSlidable5thWheelStatus_ExtraBBSlidable5thWheelStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_Slid5thWheelSwitchStatus_A2PosSwitchStatus(&Read_Slid5thWheelSwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Write_BBContainerUnlockRequest_BBContainerUnlockRequest(Rte_InitValue_BBContainerUnlockRequest_BBContainerUnlockRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Write_BBSlidable5thWheelRequest_BBSlidable5thWheelRequest(Rte_InitValue_BBSlidable5thWheelRequest_BBSlidable5thWheelRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Write_ContUnlock_DeviceIndication_DeviceIndication(Rte_InitValue_ContUnlock_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Write_Slid5thWheel_DeviceIndication_DeviceIndication(Rte_InitValue_Slid5thWheel_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  ExtraBbContOrSlid_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExtraBbContOrSlid_HMICtrl_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbContOrSlid_HMICtrl_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExtraBbContOrSlid_HMICtrl_CODE) ExtraBbContOrSlid_HMICtrl_init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExtraBbContOrSlid_HMICtrl_init
 *********************************************************************************************************************/

  SEWS_ContainerUnlockHMIDeviceType_P1CXO_T P1CXO_ContainerUnlockHMIDeviceType_v_data;
  SEWS_Slidable5thWheelHMIDeviceType_P1CXP_T P1CXP_Slidable5thWheelHMIDeviceType_v_data;
  SEWS_Slid5thWheelTimeoutForReq_P1DV9_T P1DV9_Slid5thWheelTimeoutForReq_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1CXO_ContainerUnlockHMIDeviceType_v_data = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Prm_P1CXO_ContainerUnlockHMIDeviceType_v();
  P1CXP_Slidable5thWheelHMIDeviceType_v_data = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Prm_P1CXP_Slidable5thWheelHMIDeviceType_v();
  P1DV9_Slid5thWheelTimeoutForReq_v_data = TSC_ExtraBbContOrSlid_HMICtrl_Rte_Prm_P1DV9_Slid5thWheelTimeoutForReq_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define ExtraBbContOrSlid_HMICtrl_STOP_SEC_CODE
#include "ExtraBbContOrSlid_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void ExtraBbContOrSlid_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  InactiveActive_T Test_InactiveActive_T_V_1 = InactiveActive_Inactive;
  InactiveActive_T Test_InactiveActive_T_V_2 = InactiveActive_Active;
  InactiveActive_T Test_InactiveActive_T_V_3 = InactiveActive_Error;
  InactiveActive_T Test_InactiveActive_T_V_4 = InactiveActive_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
