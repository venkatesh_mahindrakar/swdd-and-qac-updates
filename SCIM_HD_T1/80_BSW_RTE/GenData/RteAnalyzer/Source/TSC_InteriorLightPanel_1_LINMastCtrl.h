/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_InteriorLightPanel_1_LINMastCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_IntLghtOffModeInd_cmd_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_IntLightMaxModeInd_cmd_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_IntLightNightModeInd_cmd_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_IntLightRestingModeInd_cmd_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_DoorAutoFuncBtn_stat_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_IntLghtDimmingLvlDecBtn_s_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_IntLghtDimmingLvlIncBtn_s_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status(FreeWheel_Status_T *data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Read_ResponseErrorILCP1_ResponseErrorILCP1(ResponseErrorILCP1_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Write_DoorAutoFuncBtn_stat_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Write_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Write_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Write_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(FreeWheel_Status_T data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DoorAutoFuncInd_cmd_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_IntLghtOffModeInd_cmd_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_IntLightMaxModeInd_cmd_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_IntLightNightModeInd_cmd_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_IntLightRestingModeInd_cmd_DeviceIndication(DeviceIndication_T data);

/** Sender receiver - update flag */
boolean TSC_InteriorLightPanel_1_LINMastCtrl_Rte_IsUpdated_LIN_IntLghtModeSelrFreeWheel_s_FreeWheel_Status(void);

/** Service interfaces */
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BKD_87_ILCP1Link_NoResp_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_OtherInteriorLights2_ActivateIss(void);
Std_ReturnType TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_OtherInteriorLights2_DeactivateIss(void);

/** Explicit inter-runnable variables */
uint8 TSC_InteriorLightPanel_1_LINMastCtrl_Rte_IrvRead_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ILCP1Lin(void);
void TSC_InteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ILCP1Lin(uint8);
void TSC_InteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ILCP1Lin(uint8);
uint8 TSC_InteriorLightPanel_1_LINMastCtrl_Rte_IrvRead_InteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ILCP1Lin(void);
void TSC_InteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_InteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ILCP1Lin(uint8);

/** Calibration Component Calibration Parameters */
boolean  TSC_InteriorLightPanel_1_LINMastCtrl_Rte_Prm_P1VR1_ILCP1_Installed_v(void);




