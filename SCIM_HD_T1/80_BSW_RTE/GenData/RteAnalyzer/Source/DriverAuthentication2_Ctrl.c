/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  DriverAuthentication2_Ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  DriverAuthentication2_Ctrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <DriverAuthentication2_Ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * SEWS_CrankingLockActivation_P1DS3_T
 *   
 *
 * SEWS_P1VKG_APM_Check_Active_T
 *   
 *
 * SEWS_P1VKG_DISPLAY_Check_Active_T
 *   
 *
 * SEWS_P1VKG_EMS_Check_Active_T
 *   
 *
 * SEWS_P1VKG_MVUC_Check_Active_T
 *   
 *
 * SEWS_P1VKG_TECU_Check_Active_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_DriverAuthentication2_Ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_DriverAuthentication2_Ctrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void DriverAuthentication2_Ctrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_CrankingLockActivation_P1DS3_T: Integer in interval [0...255]
 * SEWS_P1VKG_DISPLAY_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_EMS_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_MVUC_Check_Active_T: Integer in interval [0...255]
 * SEWS_P1VKG_TECU_Check_Active_T: Integer in interval [0...255]
 * VIN_rqst_T: Boolean
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * DeviceAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   DeviceAuthentication_rqst_Idle (0U)
 *   DeviceAuthentication_rqst_DeviceAuthenticationRequest (1U)
 *   DeviceAuthentication_rqst_DeviceDeauthenticationRequest (2U)
 *   DeviceAuthentication_rqst_DeviceMatching (3U)
 *   DeviceAuthentication_rqst_Spare1 (4U)
 *   DeviceAuthentication_rqst_Spare2 (5U)
 *   DeviceAuthentication_rqst_Error (6U)
 *   DeviceAuthentication_rqst_NotAvailable (7U)
 * DeviceInCab_stat_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceInCab_stat_Idle (0U)
 *   DeviceInCab_stat_DeviceNotAuthenticated (1U)
 *   DeviceInCab_stat_DeviceAuthenticated (2U)
 *   DeviceInCab_stat_NotAvailable (3U)
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorsAjar_stat_Idle (0U)
 *   DoorsAjar_stat_BothDoorsAreClosed (1U)
 *   DoorsAjar_stat_DriverDoorIsOpen (2U)
 *   DoorsAjar_stat_PassengerDoorIsOpen (3U)
 *   DoorsAjar_stat_BothDoorsAreOpen (4U)
 *   DoorsAjar_stat_Spare (5U)
 *   DoorsAjar_stat_Error (6U)
 *   DoorsAjar_stat_NotAvailable (7U)
 * EngineStartAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineStartAuth_rqst_StartingOfTheTruckNotRequested (0U)
 *   EngineStartAuth_rqst_StartingOfTheTruckRequested (1U)
 *   EngineStartAuth_rqst_Spare (2U)
 *   EngineStartAuth_rqst_Spare_01 (3U)
 *   EngineStartAuth_rqst_Spare_02 (4U)
 *   EngineStartAuth_rqst_Spare_03 (5U)
 *   EngineStartAuth_rqst_Error (6U)
 *   EngineStartAuth_rqst_NotAvailable (7U)
 * EngineStartAuth_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   EngineStartAuth_stat_decrypt_Idle (0U)
 *   EngineStartAuth_stat_decrypt_CrankingIsAuthorized (1U)
 *   EngineStartAuth_stat_decrypt_CrankingIsProhibited (2U)
 *   EngineStartAuth_stat_decrypt_Spare1 (3U)
 *   EngineStartAuth_stat_decrypt_Spare2 (4U)
 *   EngineStartAuth_stat_decrypt_Spare3 (5U)
 *   EngineStartAuth_stat_decrypt_Error (6U)
 *   EngineStartAuth_stat_decrypt_NotAvailable (7U)
 * GearBoxUnlockAuth_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   GearBoxUnlockAuth_rqst_GearEngagementNotRequested (0U)
 *   GearBoxUnlockAuth_rqst_GearEngagementRequested (1U)
 *   GearBoxUnlockAuth_rqst_Error (2U)
 *   GearBoxUnlockAuth_rqst_NotAvaiable (3U)
 * GearboxUnlockAuth_stat_decrypt_T: Enumeration of integer in interval [0...3] with enumerators
 *   GearboxUnlockAuth_stat_decrypt_Idle (0U)
 *   GearboxUnlockAuth_stat_decrypt_GearEngagementAllowed (1U)
 *   GearboxUnlockAuth_stat_decrypt_GearEngagementRefused (2U)
 *   GearboxUnlockAuth_stat_decrypt_Spare1 (3U)
 *   GearboxUnlockAuth_stat_decrypt_Spare2 (4U)
 *   GearboxUnlockAuth_stat_decrypt_Spare3 (5U)
 *   GearboxUnlockAuth_stat_decrypt_Error (6U)
 *   GearboxUnlockAuth_stat_decrypt_NotAvailable (7U)
 * KeyAuthentication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyAuthentication_rqst_KeyNotPresent (0U)
 *   KeyAuthentication_rqst_KeyIsInserted (1U)
 *   KeyAuthentication_rqst_RequestAuthentication (2U)
 *   KeyAuthentication_rqst_Spare (3U)
 *   KeyAuthentication_rqst_Spare01 (4U)
 *   KeyAuthentication_rqst_Spare02 (5U)
 *   KeyAuthentication_rqst_Error (6U)
 *   KeyAuthentication_rqst_NotAvailable (7U)
 * KeyAuthentication_stat_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyAuthentication_stat_decrypt_KeyNotAuthenticated (0U)
 *   KeyAuthentication_stat_decrypt_KeyAuthenticated (1U)
 *   KeyAuthentication_stat_decrypt_Spare1 (2U)
 *   KeyAuthentication_stat_decrypt_Spare2 (3U)
 *   KeyAuthentication_stat_decrypt_Spare3 (4U)
 *   KeyAuthentication_stat_decrypt_Spare4 (5U)
 *   KeyAuthentication_stat_decrypt_Error (6U)
 *   KeyAuthentication_stat_decrypt_NotAvailable (7U)
 * KeyNotValid_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyNotValid_Idle (0U)
 *   KeyNotValid_KeyNotValid (1U)
 *   KeyNotValid_Error (6U)
 *   KeyNotValid_NotAvailable (7U)
 * KeyfobAuth_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_rqst_Idle (0U)
 *   KeyfobAuth_rqst_RequestByPassiveMechanism (1U)
 *   KeyfobAuth_rqst_RequestByImmobilizerMechanism (2U)
 *   KeyfobAuth_rqst_Spare1 (3U)
 *   KeyfobAuth_rqst_Spare2 (4U)
 *   KeyfobAuth_rqst_Spare3 (5U)
 *   KeyfobAuth_rqst_Error (6U)
 *   KeyfobAuth_rqst_NotAavailable (7U)
 * KeyfobAuth_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobAuth_stat_Idle (0U)
 *   KeyfobAuth_stat_NokeyfobAuthenticated (1U)
 *   KeyfobAuth_stat_KeyfobAuthenticated (2U)
 *   KeyfobAuth_stat_Spare1 (3U)
 *   KeyfobAuth_stat_Spare2 (4U)
 *   KeyfobAuth_stat_Spare3 (5U)
 *   KeyfobAuth_stat_Error (6U)
 *   KeyfobAuth_stat_NotAvailable (7U)
 * PinCode_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   PinCode_rqst_Idle (0U)
 *   PinCode_rqst_PINCodeNotNeeded (1U)
 *   PinCode_rqst_StatusOfThePinCodeRequested (2U)
 *   PinCode_rqst_PinCodeNeeded (3U)
 *   PinCode_rqst_ResetPinCodeStatus (4U)
 *   PinCode_rqst_Spare (5U)
 *   PinCode_rqst_Error (6U)
 *   PinCode_rqst_NotAvailable (7U)
 * PinCode_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   PinCode_stat_Idle (0U)
 *   PinCode_stat_NoPinCodeEntered (1U)
 *   PinCode_stat_WrongPinCode (2U)
 *   PinCode_stat_GoodPinCode (3U)
 *   PinCode_stat_Error (6U)
 *   PinCode_stat_NotAvailable (7U)
 * SEWS_P1VKG_APM_Check_Active_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_P1VKG_APM_Check_Active_T_No (0U)
 *   SEWS_P1VKG_APM_Check_Active_T_Yes (1U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Crypto_Function_serialized_T: Array with 12 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * SEWS_ChassisId_CHANO_T: Array with 16 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 * VIN_stat_T: Array with 11 element(s) of type uint8
 *
 * Record Types:
 * =============
 * SEWS_VINCheckProcessing_P1VKG_s_T: Record with elements
 *   APM_Check_Active of type SEWS_P1VKG_APM_Check_Active_T
 *   MVUC_Check_Active of type SEWS_P1VKG_MVUC_Check_Active_T
 *   EMS_Check_Active of type SEWS_P1VKG_EMS_Check_Active_T
 *   TECU_Check_Active of type SEWS_P1VKG_TECU_Check_Active_T
 *   DISPLAY_Check_Active of type SEWS_P1VKG_DISPLAY_Check_Active_T
 * VINCheckStatus_T: Record with elements
 *   isDISPLAY_CheckPassed of type boolean
 *   isAPM_CheckPassed of type boolean
 *   isVMCU_CheckPassed of type boolean
 *   isEMS_CheckPassed of type boolean
 *   isTECU_CheckPassed of type boolean
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1VKI_PassiveStart_Installed_v(void)
 *   SEWS_VINCheckProcessing_P1VKG_s_T *Rte_Prm_P1VKG_VINCheckProcessing_v(void)
 *   boolean Rte_Prm_P1TTA_GearBoxLockActivation_v(void)
 *   SEWS_CrankingLockActivation_P1DS3_T Rte_Prm_P1DS3_CrankingLockActivation_v(void)
 *   uint8 *Rte_Prm_CHANO_ChassisId_v(void)
 *     Returnvalue: uint8* is of type SEWS_ChassisId_CHANO_T
 *
 *********************************************************************************************************************/


#define DriverAuthentication2_Ctrl_START_SEC_CODE
#include "DriverAuthentication2_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1VKH_Data_P1VKH_VINCheck_Status>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus(VINCheckStatus_T *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1VKH_Data_P1VKH_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, DriverAuthentication2_Ctrl_CODE) DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean P1VKI_PassiveStart_Installed_v_data;
  SEWS_VINCheckProcessing_P1VKG_s_T P1VKG_VINCheckProcessing_v_data;

  boolean P1TTA_GearBoxLockActivation_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;

  VINCheckStatus_T DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1VKI_PassiveStart_Installed_v_data = TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1VKI_PassiveStart_Installed_v();
  P1VKG_VINCheckProcessing_v_data = *TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1VKG_VINCheckProcessing_v();

  P1TTA_GearBoxLockActivation_v_data = TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1TTA_GearBoxLockActivation_v();

  P1DS3_CrankingLockActivation_v_data = TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1DS3_CrankingLockActivation_v();

  (void)memcpy(CHANO_ChassisId_v_data, TSC_DriverAuthentication2_Ctrl_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));

  TSC_DriverAuthentication2_Ctrl_Rte_IrvRead_DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus(&DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData_Vincheckstatus);

  DriverAuthentication2_Ctrl_TestDefines();

  return RTE_E_DataServices_P1VKH_Data_P1VKH_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DriverAuthentication2_Ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Receive_ECU1VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU2VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU3VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU4VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Receive_ECU5VIN_stat_VIN_stat(uint8 *data)
 *     Argument data: uint8* is of type VIN_stat_T
 *   Std_ReturnType Rte_Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst(DeviceAuthentication_rqst_T *data)
 *   Std_ReturnType Rte_Read_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_EngineStartAuth_rqst_EngineStartAuth_rqst(EngineStartAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst(GearBoxUnlockAuth_rqst_T *data)
 *   Std_ReturnType Rte_Read_KeyAuthentication_rqst_KeyAuthentication_rqst(KeyAuthentication_rqst_T *data)
 *   Std_ReturnType Rte_Read_KeyfobAuth_stat_KeyfobAuth_stat(KeyfobAuth_stat_T *data)
 *   Std_ReturnType Rte_Read_PinCode_stat_PinCode_stat(PinCode_stat_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_DeviceInCab_stat_DeviceInCab_stat(DeviceInCab_stat_T data)
 *   Std_ReturnType Rte_Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_EngineStartAuth_st_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_EngineStartAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt(EngineStartAuth_stat_decrypt_T data)
 *   Std_ReturnType Rte_Write_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt(GearboxUnlockAuth_stat_decrypt_T data)
 *   Std_ReturnType Rte_Write_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_KeyAuth_stat_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_KeyAuth_stat_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(KeyAuthentication_stat_decrypt_T data)
 *   Std_ReturnType Rte_Write_KeyNotValid_KeyNotValid(KeyNotValid_T data)
 *   Std_ReturnType Rte_Write_KeyfobAuth_rqst_KeyfobAuth_rqst(KeyfobAuth_rqst_T data)
 *   Std_ReturnType Rte_Write_PinCode_rqst_PinCode_rqst(PinCode_rqst_T data)
 *   Std_ReturnType Rte_Write_VIN_rqst_VIN_rqst(VIN_rqst_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus(const VINCheckStatus_T *data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_ImmobilizerPINCode_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ImmobilizerPINCode_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DriverAuthentication2_Ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, DriverAuthentication2_Ctrl_CODE) DriverAuthentication2_Ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DriverAuthentication2_Ctrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  VIN_stat_T Receive_ECU1VIN_stat_VIN_stat;
  VIN_stat_T Receive_ECU2VIN_stat_VIN_stat;
  VIN_stat_T Receive_ECU3VIN_stat_VIN_stat;
  VIN_stat_T Receive_ECU4VIN_stat_VIN_stat;
  VIN_stat_T Receive_ECU5VIN_stat_VIN_stat;
  DeviceAuthentication_rqst_T Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst;
  DoorsAjar_stat_T Read_DoorsAjar_stat_DoorsAjar_stat;
  StandardNVM_T Read_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM;
  EngineStartAuth_rqst_T Read_EngineStartAuth_rqst_EngineStartAuth_rqst;
  GearBoxUnlockAuth_rqst_T Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst;
  KeyAuthentication_rqst_T Read_KeyAuthentication_rqst_KeyAuthentication_rqst;
  KeyfobAuth_stat_T Read_KeyfobAuth_stat_KeyfobAuth_stat;
  PinCode_stat_T Read_PinCode_stat_PinCode_stat;
  VehicleModeDistribution_T Read_SwcActivation_Security_SwcActivation_Security;
  VehicleMode_T Read_VehicleModeInternal_VehicleMode;

  boolean P1VKI_PassiveStart_Installed_v_data;
  SEWS_VINCheckProcessing_P1VKG_s_T P1VKG_VINCheckProcessing_v_data;

  boolean P1TTA_GearBoxLockActivation_v_data;

  SEWS_CrankingLockActivation_P1DS3_T P1DS3_CrankingLockActivation_v_data;

  SEWS_ChassisId_CHANO_T CHANO_ChassisId_v_data;

  StandardNVM_T Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM;
  Crypto_Function_serialized_T Write_EngineStartAuth_st_serialized_Crypto_Function_serialized;
  Crypto_Function_serialized_T Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized;
  Crypto_Function_serialized_T Write_KeyAuth_stat_serialized_Crypto_Function_serialized;

  VINCheckStatus_T DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus_Write;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1VKI_PassiveStart_Installed_v_data = TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1VKI_PassiveStart_Installed_v();
  P1VKG_VINCheckProcessing_v_data = *TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1VKG_VINCheckProcessing_v();

  P1TTA_GearBoxLockActivation_v_data = TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1TTA_GearBoxLockActivation_v();

  P1DS3_CrankingLockActivation_v_data = TSC_DriverAuthentication2_Ctrl_Rte_Prm_P1DS3_CrankingLockActivation_v();

  (void)memcpy(CHANO_ChassisId_v_data, TSC_DriverAuthentication2_Ctrl_Rte_Prm_CHANO_ChassisId_v(), sizeof(SEWS_ChassisId_CHANO_T));

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU1VIN_stat_VIN_stat(Receive_ECU1VIN_stat_VIN_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU2VIN_stat_VIN_stat(Receive_ECU2VIN_stat_VIN_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU3VIN_stat_VIN_stat(Receive_ECU3VIN_stat_VIN_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU4VIN_stat_VIN_stat(Receive_ECU4VIN_stat_VIN_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Receive_ECU5VIN_stat_VIN_stat(Receive_ECU5VIN_stat_VIN_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NO_DATA:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LOST_DATA:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst(&Read_DeviceAuthentication_rqst_DeviceAuthentication_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_DoorsAjar_stat_DoorsAjar_stat(&Read_DoorsAjar_stat_DoorsAjar_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(Read_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_EngineStartAuth_rqst_EngineStartAuth_rqst(&Read_EngineStartAuth_rqst_EngineStartAuth_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst(&Read_GearBoxUnlockAuth_rqst_GearBoxUnlockAuth_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_KeyAuthentication_rqst_KeyAuthentication_rqst(&Read_KeyAuthentication_rqst_KeyAuthentication_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_KeyfobAuth_stat_KeyfobAuth_stat(&Read_KeyfobAuth_stat_KeyfobAuth_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_PinCode_stat_PinCode_stat(&Read_PinCode_stat_PinCode_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_SwcActivation_Security_SwcActivation_Security(&Read_SwcActivation_Security_SwcActivation_Security);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_DeviceInCab_stat_DeviceInCab_stat(Rte_InitValue_DeviceInCab_stat_DeviceInCab_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM, 0, sizeof(Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM));
  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM(Write_DriverAuth2_Ctrl_NVM_I_DriverAuth2_Ctrl_NVM);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_EngineStartAuth_st_serialized_Crypto_Function_serialized, 0, sizeof(Write_EngineStartAuth_st_serialized_Crypto_Function_serialized));
  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_EngineStartAuth_st_serialized_Crypto_Function_serialized(Write_EngineStartAuth_st_serialized_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_EngineStartAuth_stat_CryptTrig_CryptoTrigger(Rte_InitValue_EngineStartAuth_stat_CryptTrig_CryptoTrigger);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt(Rte_InitValue_EngineStartAuth_stat_decrypt_EngineStartAuth_stat_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt(Rte_InitValue_GearboxUnlockAuth_stat_decrypt_GearboxUnlockAuth_stat_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger(Rte_InitValue_GrbxUnlockAuth_stat_CryptTrig_CryptoTrigger);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized, 0, sizeof(Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized));
  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized(Write_GrbxUnlockAuth_stat_serialized_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyAuth_stat_CryptTrig_CryptoTrigger(Rte_InitValue_KeyAuth_stat_CryptTrig_CryptoTrigger);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_KeyAuth_stat_serialized_Crypto_Function_serialized, 0, sizeof(Write_KeyAuth_stat_serialized_Crypto_Function_serialized));
  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyAuth_stat_serialized_Crypto_Function_serialized(Write_KeyAuth_stat_serialized_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(Rte_InitValue_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyNotValid_KeyNotValid(Rte_InitValue_KeyNotValid_KeyNotValid);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_KeyfobAuth_rqst_KeyfobAuth_rqst(Rte_InitValue_KeyfobAuth_rqst_KeyfobAuth_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_PinCode_rqst_PinCode_rqst(Rte_InitValue_PinCode_rqst_PinCode_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Write_VIN_rqst_VIN_rqst(Rte_InitValue_VIN_rqst_VIN_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus_Write, 0, sizeof(DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus_Write));
  TSC_DriverAuthentication2_Ctrl_Rte_IrvWrite_DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus(&DriverAuthentication2_Ctrl_20ms_runnable_Vincheckstatus_Write);

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Call_UR_ANW_ImmobilizerPINCode_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_DriverAuthentication2_Ctrl_Rte_Call_UR_ANW_ImmobilizerPINCode_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define DriverAuthentication2_Ctrl_STOP_SEC_CODE
#include "DriverAuthentication2_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void DriverAuthentication2_Ctrl_TestDefines(void)
{
  /* Enumeration Data Types */

  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_1 = DeviceAuthentication_rqst_Idle;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_2 = DeviceAuthentication_rqst_DeviceAuthenticationRequest;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_3 = DeviceAuthentication_rqst_DeviceDeauthenticationRequest;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_4 = DeviceAuthentication_rqst_DeviceMatching;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_5 = DeviceAuthentication_rqst_Spare1;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_6 = DeviceAuthentication_rqst_Spare2;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_7 = DeviceAuthentication_rqst_Error;
  DeviceAuthentication_rqst_T Test_DeviceAuthentication_rqst_T_V_8 = DeviceAuthentication_rqst_NotAvailable;

  DeviceInCab_stat_T Test_DeviceInCab_stat_T_V_1 = DeviceInCab_stat_Idle;
  DeviceInCab_stat_T Test_DeviceInCab_stat_T_V_2 = DeviceInCab_stat_DeviceNotAuthenticated;
  DeviceInCab_stat_T Test_DeviceInCab_stat_T_V_3 = DeviceInCab_stat_DeviceAuthenticated;
  DeviceInCab_stat_T Test_DeviceInCab_stat_T_V_4 = DeviceInCab_stat_NotAvailable;

  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_1 = DoorsAjar_stat_Idle;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_2 = DoorsAjar_stat_BothDoorsAreClosed;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_3 = DoorsAjar_stat_DriverDoorIsOpen;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_4 = DoorsAjar_stat_PassengerDoorIsOpen;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_5 = DoorsAjar_stat_BothDoorsAreOpen;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_6 = DoorsAjar_stat_Spare;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_7 = DoorsAjar_stat_Error;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_8 = DoorsAjar_stat_NotAvailable;

  EngineStartAuth_rqst_T Test_EngineStartAuth_rqst_T_V_1 = EngineStartAuth_rqst_StartingOfTheTruckNotRequested;
  EngineStartAuth_rqst_T Test_EngineStartAuth_rqst_T_V_2 = EngineStartAuth_rqst_StartingOfTheTruckRequested;
  EngineStartAuth_rqst_T Test_EngineStartAuth_rqst_T_V_3 = EngineStartAuth_rqst_Spare;
  EngineStartAuth_rqst_T Test_EngineStartAuth_rqst_T_V_4 = EngineStartAuth_rqst_Spare_01;
  EngineStartAuth_rqst_T Test_EngineStartAuth_rqst_T_V_5 = EngineStartAuth_rqst_Spare_02;
  EngineStartAuth_rqst_T Test_EngineStartAuth_rqst_T_V_6 = EngineStartAuth_rqst_Spare_03;
  EngineStartAuth_rqst_T Test_EngineStartAuth_rqst_T_V_7 = EngineStartAuth_rqst_Error;
  EngineStartAuth_rqst_T Test_EngineStartAuth_rqst_T_V_8 = EngineStartAuth_rqst_NotAvailable;

  EngineStartAuth_stat_decrypt_T Test_EngineStartAuth_stat_decrypt_T_V_1 = EngineStartAuth_stat_decrypt_Idle;
  EngineStartAuth_stat_decrypt_T Test_EngineStartAuth_stat_decrypt_T_V_2 = EngineStartAuth_stat_decrypt_CrankingIsAuthorized;
  EngineStartAuth_stat_decrypt_T Test_EngineStartAuth_stat_decrypt_T_V_3 = EngineStartAuth_stat_decrypt_CrankingIsProhibited;
  EngineStartAuth_stat_decrypt_T Test_EngineStartAuth_stat_decrypt_T_V_4 = EngineStartAuth_stat_decrypt_Spare1;
  EngineStartAuth_stat_decrypt_T Test_EngineStartAuth_stat_decrypt_T_V_5 = EngineStartAuth_stat_decrypt_Spare2;
  EngineStartAuth_stat_decrypt_T Test_EngineStartAuth_stat_decrypt_T_V_6 = EngineStartAuth_stat_decrypt_Spare3;
  EngineStartAuth_stat_decrypt_T Test_EngineStartAuth_stat_decrypt_T_V_7 = EngineStartAuth_stat_decrypt_Error;
  EngineStartAuth_stat_decrypt_T Test_EngineStartAuth_stat_decrypt_T_V_8 = EngineStartAuth_stat_decrypt_NotAvailable;

  GearBoxUnlockAuth_rqst_T Test_GearBoxUnlockAuth_rqst_T_V_1 = GearBoxUnlockAuth_rqst_GearEngagementNotRequested;
  GearBoxUnlockAuth_rqst_T Test_GearBoxUnlockAuth_rqst_T_V_2 = GearBoxUnlockAuth_rqst_GearEngagementRequested;
  GearBoxUnlockAuth_rqst_T Test_GearBoxUnlockAuth_rqst_T_V_3 = GearBoxUnlockAuth_rqst_Error;
  GearBoxUnlockAuth_rqst_T Test_GearBoxUnlockAuth_rqst_T_V_4 = GearBoxUnlockAuth_rqst_NotAvaiable;

  GearboxUnlockAuth_stat_decrypt_T Test_GearboxUnlockAuth_stat_decrypt_T_V_1 = GearboxUnlockAuth_stat_decrypt_Idle;
  GearboxUnlockAuth_stat_decrypt_T Test_GearboxUnlockAuth_stat_decrypt_T_V_2 = GearboxUnlockAuth_stat_decrypt_GearEngagementAllowed;
  GearboxUnlockAuth_stat_decrypt_T Test_GearboxUnlockAuth_stat_decrypt_T_V_3 = GearboxUnlockAuth_stat_decrypt_GearEngagementRefused;
  GearboxUnlockAuth_stat_decrypt_T Test_GearboxUnlockAuth_stat_decrypt_T_V_4 = GearboxUnlockAuth_stat_decrypt_Spare1;
  GearboxUnlockAuth_stat_decrypt_T Test_GearboxUnlockAuth_stat_decrypt_T_V_5 = GearboxUnlockAuth_stat_decrypt_Spare2;
  GearboxUnlockAuth_stat_decrypt_T Test_GearboxUnlockAuth_stat_decrypt_T_V_6 = GearboxUnlockAuth_stat_decrypt_Spare3;
  GearboxUnlockAuth_stat_decrypt_T Test_GearboxUnlockAuth_stat_decrypt_T_V_7 = GearboxUnlockAuth_stat_decrypt_Error;
  GearboxUnlockAuth_stat_decrypt_T Test_GearboxUnlockAuth_stat_decrypt_T_V_8 = GearboxUnlockAuth_stat_decrypt_NotAvailable;

  KeyAuthentication_rqst_T Test_KeyAuthentication_rqst_T_V_1 = KeyAuthentication_rqst_KeyNotPresent;
  KeyAuthentication_rqst_T Test_KeyAuthentication_rqst_T_V_2 = KeyAuthentication_rqst_KeyIsInserted;
  KeyAuthentication_rqst_T Test_KeyAuthentication_rqst_T_V_3 = KeyAuthentication_rqst_RequestAuthentication;
  KeyAuthentication_rqst_T Test_KeyAuthentication_rqst_T_V_4 = KeyAuthentication_rqst_Spare;
  KeyAuthentication_rqst_T Test_KeyAuthentication_rqst_T_V_5 = KeyAuthentication_rqst_Spare01;
  KeyAuthentication_rqst_T Test_KeyAuthentication_rqst_T_V_6 = KeyAuthentication_rqst_Spare02;
  KeyAuthentication_rqst_T Test_KeyAuthentication_rqst_T_V_7 = KeyAuthentication_rqst_Error;
  KeyAuthentication_rqst_T Test_KeyAuthentication_rqst_T_V_8 = KeyAuthentication_rqst_NotAvailable;

  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_1 = KeyAuthentication_stat_decrypt_KeyNotAuthenticated;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_2 = KeyAuthentication_stat_decrypt_KeyAuthenticated;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_3 = KeyAuthentication_stat_decrypt_Spare1;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_4 = KeyAuthentication_stat_decrypt_Spare2;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_5 = KeyAuthentication_stat_decrypt_Spare3;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_6 = KeyAuthentication_stat_decrypt_Spare4;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_7 = KeyAuthentication_stat_decrypt_Error;
  KeyAuthentication_stat_decrypt_T Test_KeyAuthentication_stat_decrypt_T_V_8 = KeyAuthentication_stat_decrypt_NotAvailable;

  KeyNotValid_T Test_KeyNotValid_T_V_1 = KeyNotValid_Idle;
  KeyNotValid_T Test_KeyNotValid_T_V_2 = KeyNotValid_KeyNotValid;
  KeyNotValid_T Test_KeyNotValid_T_V_3 = KeyNotValid_Error;
  KeyNotValid_T Test_KeyNotValid_T_V_4 = KeyNotValid_NotAvailable;

  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_1 = KeyfobAuth_rqst_Idle;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_2 = KeyfobAuth_rqst_RequestByPassiveMechanism;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_3 = KeyfobAuth_rqst_RequestByImmobilizerMechanism;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_4 = KeyfobAuth_rqst_Spare1;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_5 = KeyfobAuth_rqst_Spare2;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_6 = KeyfobAuth_rqst_Spare3;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_7 = KeyfobAuth_rqst_Error;
  KeyfobAuth_rqst_T Test_KeyfobAuth_rqst_T_V_8 = KeyfobAuth_rqst_NotAavailable;

  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_1 = KeyfobAuth_stat_Idle;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_2 = KeyfobAuth_stat_NokeyfobAuthenticated;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_3 = KeyfobAuth_stat_KeyfobAuthenticated;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_4 = KeyfobAuth_stat_Spare1;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_5 = KeyfobAuth_stat_Spare2;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_6 = KeyfobAuth_stat_Spare3;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_7 = KeyfobAuth_stat_Error;
  KeyfobAuth_stat_T Test_KeyfobAuth_stat_T_V_8 = KeyfobAuth_stat_NotAvailable;

  PinCode_rqst_T Test_PinCode_rqst_T_V_1 = PinCode_rqst_Idle;
  PinCode_rqst_T Test_PinCode_rqst_T_V_2 = PinCode_rqst_PINCodeNotNeeded;
  PinCode_rqst_T Test_PinCode_rqst_T_V_3 = PinCode_rqst_StatusOfThePinCodeRequested;
  PinCode_rqst_T Test_PinCode_rqst_T_V_4 = PinCode_rqst_PinCodeNeeded;
  PinCode_rqst_T Test_PinCode_rqst_T_V_5 = PinCode_rqst_ResetPinCodeStatus;
  PinCode_rqst_T Test_PinCode_rqst_T_V_6 = PinCode_rqst_Spare;
  PinCode_rqst_T Test_PinCode_rqst_T_V_7 = PinCode_rqst_Error;
  PinCode_rqst_T Test_PinCode_rqst_T_V_8 = PinCode_rqst_NotAvailable;

  PinCode_stat_T Test_PinCode_stat_T_V_1 = PinCode_stat_Idle;
  PinCode_stat_T Test_PinCode_stat_T_V_2 = PinCode_stat_NoPinCodeEntered;
  PinCode_stat_T Test_PinCode_stat_T_V_3 = PinCode_stat_WrongPinCode;
  PinCode_stat_T Test_PinCode_stat_T_V_4 = PinCode_stat_GoodPinCode;
  PinCode_stat_T Test_PinCode_stat_T_V_5 = PinCode_stat_Error;
  PinCode_stat_T Test_PinCode_stat_T_V_6 = PinCode_stat_NotAvailable;

  SEWS_P1VKG_APM_Check_Active_T Test_SEWS_P1VKG_APM_Check_Active_T_V_1 = SEWS_P1VKG_APM_Check_Active_T_No;
  SEWS_P1VKG_APM_Check_Active_T Test_SEWS_P1VKG_APM_Check_Active_T_V_2 = SEWS_P1VKG_APM_Check_Active_T_Yes;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
