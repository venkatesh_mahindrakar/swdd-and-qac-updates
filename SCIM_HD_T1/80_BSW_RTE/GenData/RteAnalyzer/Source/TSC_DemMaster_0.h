/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_DemMaster_0.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Service interfaces */
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBInitEvt_D1BUL_31_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBInitEvt_D1BUL_95_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataClass_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_DemDataElementClass_StartApplication_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1AFR_Data_P1AFR_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1AFS_Data_P1AFS_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1AFT_Data_P1AFT_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1CXF_Data_P1CXF_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1DCT_Data_P1DCT_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1DCU_Data_P1DCU_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1QXI_Data_P1QXI_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1QXJ_Data_P1QXJ_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1QXM_Data_P1QXM_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1QXP_Data_P1QXP_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_P1QXR_Data_P1QXR_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_First_Day_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_First_Hour_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_First_Minutes_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_First_Month_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_First_Seconds_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_First_Year_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Day_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Hour_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Month_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBReadData_UTCTimeStamp_Latest_Year_ReadData(uint8 *Data);
Std_ReturnType TSC_DemMaster_0_Rte_Call_CBStatusDTC_DemCallbackDTCStatusChanged_DTCStatusChanged(uint32 DTC, Dem_UdsStatusByteType DTCStatusOld, Dem_UdsStatusByteType DTCStatusNew);




