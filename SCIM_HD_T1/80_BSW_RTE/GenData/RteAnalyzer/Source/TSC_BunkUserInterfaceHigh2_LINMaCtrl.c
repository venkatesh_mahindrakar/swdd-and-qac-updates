/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_BunkUserInterfaceHigh2_LINMaCtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_BunkUserInterfaceHigh2_LINMaCtrl.h"
#include "TSC_BunkUserInterfaceHigh2_LINMaCtrl.h"








Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(AlmClkCurAlarm_stat_T *data)
{
  return Rte_Read_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_AudioSystemStatus_AudioSystemStatus(OffOn_T *data)
{
  return Rte_Read_AudioSystemStatus_AudioSystemStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_AudioVolumeIndicationCmd_VolumeValueType(VolumeValueType_T *data)
{
  return Rte_Read_AudioVolumeIndicationCmd_VolumeValueType(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_BTStatus_BTStatus(BTStatus_T *data)
{
  return Rte_Read_BTStatus_BTStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN1_ComMode_LIN(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_DiagInfoLECM2_DiagInfo(DiagInfo_T *data)
{
  return Rte_Read_DiagInfoLECM2_DiagInfo(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T *data)
{
  return Rte_Read_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_IntLghtModeInd_cmd_InteriorLightMode(InteriorLightMode_T *data)
{
  return Rte_Read_IntLghtModeInd_cmd_InteriorLightMode(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(AlmClkSetCurAlm_rqst_T *data)
{
  return Rte_Read_LIN_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2AudioOnOff_ButtonSta_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2Fade_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2IntLightActvnBtn_sta_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2IntLightDecBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2IntLightIncBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2LockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2LockButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2OnOFF_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst(SetParkHtrTmr_rqst_T *data)
{
  return Rte_Read_LIN_BunkH2PHTi_rqs_SetParkHtrTmr_rqst(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2ParkHeater_ButtonSta_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2Phone_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2PowerWinCloseDSBtn_s_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2PowerWinClosePSBtn_s_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2PowerWinOpenDSBtn_st_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2PowerWinOpenPSBtn_st_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2RoofhatchCloseBtn_St_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2RoofhatchOpenBtn_Sta_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2TempDec_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2TempInc_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2VolumeDown_ButtonSta_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_BunkH2VolumeUp_ButtonStatu_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_PhoneButtonIndication_cmd_DeviceIndication(DeviceIndication_T *data)
{
  return Rte_Read_PhoneButtonIndication_cmd_DeviceIndication(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Read_ResponseErrorLECM2_ResponseErrorLECM2(ResponseErrorLECM2_T *data)
{
  return Rte_Read_ResponseErrorLECM2_ResponseErrorLECM2(data);
}




Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(const AlmClkSetCurAlm_rqst_T *data)
{
  return Rte_Write_AlmClkSetCurAlm_rqst_AlmClkSetCurAlm_rqst(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2AudioOnOff_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2Fade_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2Fade_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2IntLightActvnBtn_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2IntLightActvnBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2LockButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2LockButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OnOFF_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2OnOFF_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst(const SetParkHtrTmr_rqst_T *data)
{
  return Rte_Write_BunkH2PHTimer_rqst_SetParkHtrTmr_rqst(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2ParkHeater_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2ParkHeater_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2Phone_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2Phone_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2RoofhatchCloseBtn_Stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2RoofhatchOpenBtn_Stat_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2TempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2TempDec_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2TempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2TempInc_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2VolumeDown_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2VolumeDown_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_BunkH2VolumeUp_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_BunkH2VolumeUp_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(const AlmClkCurAlarm_stat_T *data)
{
  return Rte_Write_LIN_AlmClkCurAlarm_stat_AlmClkCurAlarm_stat(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_AudioSystemStatus_AudioSystemStatus(OffOn_T data)
{
  return Rte_Write_LIN_AudioSystemStatus_AudioSystemStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_AudioVolumeIndicationCmd_VolumeValueType(VolumeValueType_T data)
{
  return Rte_Write_LIN_AudioVolumeIndicationCmd_VolumeValueType(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_BTStatus_BTStatus(BTStatus_T data)
{
  return Rte_Write_LIN_BTStatus_BTStatus(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(IntLghtLvlIndScaled_cmd_T data)
{
  return Rte_Write_LIN_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_IntLghtModeInd_cmd_InteriorLightMode(const InteriorLightMode_T *data)
{
  return Rte_Write_LIN_IntLghtModeInd_cmd_InteriorLightMode(data);
}

Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Write_LIN_PhoneButtonIndication_cmd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_LIN_PhoneButtonIndication_cmd_DeviceIndication(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BKF_87_LECMHighLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BKF_87_LECMHighLink_NoResp_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_16_LECMHigh_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BOH_16_LECMHigh_VBT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_17_LECMHigh_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BOH_17_LECMHigh_VAT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_44_LECMHigh_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BOH_44_LECMHigh_RAM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_45_LECMHigh_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BOH_45_LECMHigh_FLASH_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_46_LECMHigh_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BOH_46_LECMHigh_EEPROM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_49_LECMHigh_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BOH_49_LECMHigh_HWFAIL_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_Event_D1BOH_94_LECMHigh_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BOH_94_LECMHigh_SWFAIL_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_AudioRadio2_ActivateIss(void)
{
  return Rte_Call_UR_ANW_AudioRadio2_ActivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_AudioRadio2_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_AudioRadio2_DeactivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_ClimPHTimerSettings3_ActivateIss(void)
{
  return Rte_Call_UR_ANW_ClimPHTimerSettings3_ActivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_ClimPHTimerSettings3_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_ClimPHTimerSettings3_DeactivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_LockControlCabRqst2_ActivateIss(void)
{
  return Rte_Call_UR_ANW_LockControlCabRqst2_ActivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_LockControlCabRqst2_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_LockControlCabRqst2_DeactivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_OtherInteriorLights4_ActivateIss(void)
{
  return Rte_Call_UR_ANW_OtherInteriorLights4_ActivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_OtherInteriorLights4_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_OtherInteriorLights4_DeactivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_PHActMaintainLiving6_ActivateIss(void)
{
  return Rte_Call_UR_ANW_PHActMaintainLiving6_ActivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_PHActMaintainLiving6_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_PHActMaintainLiving6_DeactivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_PowerWindowsActivate3_ActivateIss(void)
{
  return Rte_Call_UR_ANW_PowerWindowsActivate3_ActivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_PowerWindowsActivate3_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_PowerWindowsActivate3_DeactivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_RoofHatchRequest3_ActivateIss(void)
{
  return Rte_Call_UR_ANW_RoofHatchRequest3_ActivateIss();
}
Std_ReturnType TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Call_UR_ANW_RoofHatchRequest3_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_RoofHatchRequest3_DeactivateIss();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





boolean  TSC_BunkUserInterfaceHigh2_LINMaCtrl_Rte_Prm_P1B2G_LECMH_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1B2G_LECMH_Installed_v();
}


     /* BunkUserInterfaceHigh2_LINMaCtrl */
      /* BunkUserInterfaceHigh2_LINMaCtrl */



