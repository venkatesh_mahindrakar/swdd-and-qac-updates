/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  VehicleAccess_Ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  VehicleAccess_Ctrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <VehicleAccess_Ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * Issm_IssStateType
 *   
 *
 * SEWS_AlarmAutoRelockRequestDuration_X1CYA_T
 *   
 *
 * SEWS_AutoAlarmReactivationTimeout_P1B2S_T
 *   
 *
 * SEWS_DashboardLedTimeout_P1IZ4_T
 *   
 *
 * SEWS_DoorAutoLockingSpeed_P1B2Q_T
 *   
 *
 * SEWS_DoorIndicationReqDuration_P1DWP_T
 *   
 *
 * SEWS_DoorLatchProtectMaxOperation_P1DXA_T
 *   
 *
 * SEWS_DoorLatchProtectionRestingTime_P1DW9_T
 *   
 *
 * SEWS_DoorLatchProtectionTimeWindow_P1DW8_T
 *   
 *
 * SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T
 *   
 *
 * SEWS_DoorLockingFailureTimeout_X1CX9_T
 *   
 *
 * SEWS_LockFunctionHardwareInterface_P1MXZ_T
 *   
 *
 * SEWS_PassiveEntryFunction_Type_P1VKF_T
 *   
 *
 * SEWS_SpeedRelockingReinitThreshold_P1H55_T
 *   
 *
 * Speed16bit_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_VehicleAccess_Ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_VehicleAccess_Ctrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void VehicleAccess_Ctrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * SEWS_AlarmAutoRelockRequestDuration_X1CYA_T: Integer in interval [0...255]
 * SEWS_AutoAlarmReactivationTimeout_P1B2S_T: Integer in interval [0...255]
 * SEWS_DashboardLedTimeout_P1IZ4_T: Integer in interval [0...255]
 * SEWS_DoorAutoLockingSpeed_P1B2Q_T: Integer in interval [0...255]
 * SEWS_DoorIndicationReqDuration_P1DWP_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectMaxOperation_P1DXA_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionRestingTime_P1DW9_T: Integer in interval [0...255]
 * SEWS_DoorLatchProtectionTimeWindow_P1DW8_T: Integer in interval [0...255]
 * SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T: Integer in interval [0...65535]
 * SEWS_DoorLockingFailureTimeout_X1CX9_T: Integer in interval [0...255]
 * SEWS_LockFunctionHardwareInterface_P1MXZ_T: Integer in interval [0...255]
 * SEWS_PassiveEntryFunction_Type_P1VKF_T: Integer in interval [0...255]
 * SEWS_SpeedRelockingReinitThreshold_P1H55_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint16: Integer in interval [0...65535] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * AutoRelock_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   AutoRelock_rqst_Idle (0U)
 *   AutoRelock_rqst_AutoRelockActivated (1U)
 *   AutoRelock_rqst_Error (2U)
 *   AutoRelock_rqst_NotAvailable (3U)
 * AutorelockingMovements_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   AutorelockingMovements_stat_Idle (0U)
 *   AutorelockingMovements_stat_NoMovementHasBeenDetected (1U)
 *   AutorelockingMovements_stat_MovementHasBeenDetected (2U)
 *   AutorelockingMovements_stat_Spare (3U)
 *   AutorelockingMovements_stat_Spare_01 (4U)
 *   AutorelockingMovements_stat_Spare_02 (5U)
 *   AutorelockingMovements_stat_Error (6U)
 *   AutorelockingMovements_stat_NotAvailable (7U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DoorAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorAjar_stat_Idle (0U)
 *   DoorAjar_stat_DoorClosed (1U)
 *   DoorAjar_stat_DoorOpen (2U)
 *   DoorAjar_stat_Error (6U)
 *   DoorAjar_stat_NotAvailable (7U)
 * DoorLatch_rqst_decrypt_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLatch_rqst_decrypt_Idle (0U)
 *   DoorLatch_rqst_decrypt_LockDoorCommand (1U)
 *   DoorLatch_rqst_decrypt_UnlockDoorCommand (2U)
 *   DoorLatch_rqst_decrypt_EmergencyUnlockRequest (3U)
 *   DoorLatch_rqst_decrypt_Spare1 (4U)
 *   DoorLatch_rqst_decrypt_Spare2 (5U)
 *   DoorLatch_rqst_decrypt_Error (6U)
 *   DoorLatch_rqst_decrypt_NotAvailable (7U)
 * DoorLatch_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLatch_stat_Idle (0U)
 *   DoorLatch_stat_LatchUnlockedFiltered (1U)
 *   DoorLatch_stat_LatchLockedFiltered (2U)
 *   DoorLatch_stat_Error (6U)
 *   DoorLatch_stat_NotAvailable (7U)
 * DoorLockUnlock_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorLockUnlock_Idle (0U)
 *   DoorLockUnlock_Unlock (1U)
 *   DoorLockUnlock_Lock (2U)
 *   DoorLockUnlock_MonoLockUnlock (3U)
 *   DoorLockUnlock_Spare1 (4U)
 *   DoorLockUnlock_Spare2 (5U)
 *   DoorLockUnlock_Error (6U)
 *   DoorLockUnlock_NotAvailable (7U)
 * DoorLock_stat_T: Enumeration of integer in interval [0...15] with enumerators
 *   DoorLock_stat_Idle (0U)
 *   DoorLock_stat_BothDoorsAreUnlocked (1U)
 *   DoorLock_stat_DriverDoorIsUnlocked (2U)
 *   DoorLock_stat_PassengerDoorIsUnlocked (3U)
 *   DoorLock_stat_BothDoorsAreLocked (4U)
 *   DoorLock_stat_Error (14U)
 *   DoorLock_stat_NotAvailable (15U)
 * DoorsAjar_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   DoorsAjar_stat_Idle (0U)
 *   DoorsAjar_stat_BothDoorsAreClosed (1U)
 *   DoorsAjar_stat_DriverDoorIsOpen (2U)
 *   DoorsAjar_stat_PassengerDoorIsOpen (3U)
 *   DoorsAjar_stat_BothDoorsAreOpen (4U)
 *   DoorsAjar_stat_Spare (5U)
 *   DoorsAjar_stat_Error (6U)
 *   DoorsAjar_stat_NotAvailable (7U)
 * EmergencyDoorsUnlock_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   EmergencyDoorsUnlock_rqst_Idle (0U)
 *   EmergencyDoorsUnlock_rqst_Spare (1U)
 *   EmergencyDoorsUnlock_rqst_EmergencyUnlockRequest (2U)
 *   EmergencyDoorsUnlock_rqst_Error (6U)
 *   EmergencyDoorsUnlock_rqst_NotAvailable (7U)
 * FrontLidLatch_cmd_T: Enumeration of integer in interval [0...7] with enumerators
 *   FrontLidLatch_cmd_Idle (0U)
 *   FrontLidLatch_cmd_LockFrontLidCommand (1U)
 *   FrontLidLatch_cmd_UnlockFrontLidCommand (2U)
 *   FrontLidLatch_cmd_Error (6U)
 *   FrontLidLatch_cmd_NotAvailable (7U)
 * FrontLidLatch_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   FrontLidLatch_stat_Idle (0U)
 *   FrontLidLatch_stat_LatchUnlocked (1U)
 *   FrontLidLatch_stat_LatchLocked (2U)
 *   FrontLidLatch_stat_Error (6U)
 *   FrontLidLatch_stat_NotAvailable (7U)
 * Issm_IssStateType: Enumeration of integer in interval [0...2] with enumerators
 *   ISSM_STATE_INACTIVE (0U)
 *   ISSM_STATE_PENDING (1U)
 *   ISSM_STATE_ACTIVE (2U)
 * KeyfobInCabLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobInCabLocation_stat_Idle (0U)
 *   KeyfobInCabLocation_stat_NotDetected_Incab (1U)
 *   KeyfobInCabLocation_stat_Detected_Incab (2U)
 *   KeyfobInCabLocation_stat_Spare1 (3U)
 *   KeyfobInCabLocation_stat_Spare2 (4U)
 *   KeyfobInCabLocation_stat_Spare3 (5U)
 *   KeyfobInCabLocation_stat_Error (6U)
 *   KeyfobInCabLocation_stat_NotAvailable (7U)
 * KeyfobLocation_rqst_T: Enumeration of integer in interval [0...3] with enumerators
 *   KeyfobLocation_rqst_NoRequest (0U)
 *   KeyfobLocation_rqst_Request (1U)
 *   KeyfobLocation_rqst_Error (2U)
 *   KeyfobLocation_rqst_NotAvailable (3U)
 * KeyfobOutsideLocation_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   KeyfobOutsideLocation_stat_Idle (0U)
 *   KeyfobOutsideLocation_stat_NotDetectedOutside (1U)
 *   KeyfobOutsideLocation_stat_DetectedOnRightSide (2U)
 *   KeyfobOutsideLocation_stat_DetectedOnLeftSide (3U)
 *   KeyfobOutsideLocation_stat_DetectedOnBothSides (4U)
 *   KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas (5U)
 *   KeyfobOutsideLocation_stat_Error (6U)
 *   KeyfobOutsideLocation_stat_NotAvailable (7U)
 * LockingIndication_rqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   LockingIndication_rqst_Idle (0U)
 *   LockingIndication_rqst_Lock (1U)
 *   LockingIndication_rqst_Unlock (2U)
 *   LockingIndication_rqst_Spare (3U)
 *   LockingIndication_rqst_Spare01 (4U)
 *   LockingIndication_rqst_Spare02 (5U)
 *   LockingIndication_rqst_Error (6U)
 *   LockingIndication_rqst_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * SpeedLockingInhibition_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   SpeedLockingInhibition_stat_Idle (0U)
 *   SpeedLockingInhibition_stat_SpeedLockingActivate (1U)
 *   SpeedLockingInhibition_stat_SpeedLockingDeactivate (2U)
 *   SpeedLockingInhibition_stat_Error (6U)
 *   SpeedLockingInhibition_stat_NotAvailable (7U)
 * Synch_Unsynch_Mode_stat_T: Enumeration of integer in interval [0...7] with enumerators
 *   Synch_Unsynch_Mode_stat_Idle (0U)
 *   Synch_Unsynch_Mode_stat_SynchronizedMode (1U)
 *   Synch_Unsynch_Mode_stat_UnsynchronizedMode (2U)
 *   Synch_Unsynch_Mode_stat_Spare (3U)
 *   Synch_Unsynch_Mode_stat_Spare_01 (4U)
 *   Synch_Unsynch_Mode_stat_Spare_02 (5U)
 *   Synch_Unsynch_Mode_stat_Error (6U)
 *   Synch_Unsynch_Mode_stat_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 *
 * Array Types:
 * ============
 * Crypto_Function_serialized_T: Array with 12 element(s) of type uint8
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_DoorLockingFailureTimeout_X1CX9_T Rte_Prm_X1CX9_DoorLockingFailureTimeout_v(void)
 *   SEWS_AlarmAutoRelockRequestDuration_X1CYA_T Rte_Prm_X1CYA_AlarmAutoRelockRequestDuration_v(void)
 *   SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T Rte_Prm_P1O8Q_DoorLockHazardIndicationRqstDelay_v(void)
 *   SEWS_DoorAutoLockingSpeed_P1B2Q_T Rte_Prm_P1B2Q_DoorAutoLockingSpeed_v(void)
 *   SEWS_AutoAlarmReactivationTimeout_P1B2S_T Rte_Prm_P1B2S_AutoAlarmReactivationTimeout_v(void)
 *   SEWS_DoorLatchProtectionTimeWindow_P1DW8_T Rte_Prm_P1DW8_DoorLatchProtectionTimeWindow_v(void)
 *   SEWS_DoorLatchProtectionRestingTime_P1DW9_T Rte_Prm_P1DW9_DoorLatchProtectionRestingTime_v(void)
 *   SEWS_DoorIndicationReqDuration_P1DWP_T Rte_Prm_P1DWP_DoorIndicationReqDuration_v(void)
 *   SEWS_DoorLatchProtectMaxOperation_P1DXA_T Rte_Prm_P1DXA_DoorLatchProtectMaxOperation_v(void)
 *   SEWS_SpeedRelockingReinitThreshold_P1H55_T Rte_Prm_P1H55_SpeedRelockingReinitThreshold_v(void)
 *   SEWS_DashboardLedTimeout_P1IZ4_T Rte_Prm_P1IZ4_DashboardLedTimeout_v(void)
 *   SEWS_LockFunctionHardwareInterface_P1MXZ_T Rte_Prm_P1MXZ_LockFunctionHardwareInterface_v(void)
 *   SEWS_PassiveEntryFunction_Type_P1VKF_T Rte_Prm_P1VKF_PassiveEntryFunction_Type_v(void)
 *   boolean Rte_Prm_P1NE9_KeyInsertDetection_Enabled_v(void)
 *   boolean Rte_Prm_P1NQE_LockModeHandling_v(void)
 *   boolean Rte_Prm_P1VKI_PassiveStart_Installed_v(void)
 *
 *********************************************************************************************************************/


#define VehicleAccess_Ctrl_START_SEC_CODE
#include "VehicleAccess_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VehicleAccess_Ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AutorelockingMovements_stat_AutorelockingMovements_stat(AutorelockingMovements_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorAjar_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_DriverDoorLatch_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized(uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Read_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst(EmergencyDoorsUnlock_rqst_T *data)
 *   Std_ReturnType Rte_Read_FrontLidLatch_stat_FrontLidLatch_stat(FrontLidLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(DoorLockUnlock_T *data)
 *   Std_ReturnType Rte_Read_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(KeyfobOutsideLocation_stat_T *data)
 *   Std_ReturnType Rte_Read_KeyfobSuperLockButton_Sta_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LeftDoorButton_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_PassengerDoorAjar_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_PassengerDoorLatch_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized(uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Read_PsngDoorLatchInternal_stat_DoorLatch_stat(DoorLatch_stat_T *data)
 *   Std_ReturnType Rte_Read_PsngrDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T *data)
 *   Std_ReturnType Rte_Read_RightDoorButton_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat(SpeedLockingInhibition_stat_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat(Synch_Unsynch_Mode_stat_T *data)
 *   Std_ReturnType Rte_Read_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_WRCLockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WRCUnlockButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AutoRelock_rqst_AutoRelock_rqst(AutoRelock_rqst_T data)
 *   Std_ReturnType Rte_Write_DashboardLockSwitch_Devic_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_DoorLock_stat_DoorLock_stat(DoorLock_stat_T data)
 *   Std_ReturnType Rte_Write_DoorsAjar_stat_DoorsAjar_stat(DoorsAjar_stat_T data)
 *   Std_ReturnType Rte_Write_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T data)
 *   Std_ReturnType Rte_Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_FrontLidLatch_cmd_FrontLidLatch_cmd(FrontLidLatch_cmd_T data)
 *   Std_ReturnType Rte_Write_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst_T data)
 *   Std_ReturnType Rte_Write_LockingIndication_rqst_LockingIndication_rqst(LockingIndication_rqst_T data)
 *   Std_ReturnType Rte_Write_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(DoorLatch_rqst_decrypt_T data)
 *   Std_ReturnType Rte_Write_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger(Boolean data)
 *   Std_ReturnType Rte_Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized(const uint8 *data)
 *     Argument data: uint8* is of type Crypto_Function_serialized_T
 *   Std_ReturnType Rte_Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlActDeactivation_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlActDeactivation_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlActDeactivation_GetIssState(Issm_IssStateType *issState)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_Pending_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_LockControlCabRqst2_Pending_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VehicleAccess_Ctrl_CODE) VehicleAccess_Ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  AutorelockingMovements_stat_T Read_AutorelockingMovements_stat_AutorelockingMovements_stat;
  DoorAjar_stat_T Read_DriverDoorAjarInternal_stat_DoorAjar_stat;
  DoorAjar_stat_T Read_DriverDoorAjar_stat_DoorAjar_stat;
  DoorLatch_stat_T Read_DriverDoorLatchInternal_stat_DoorLatch_stat;
  DoorLatch_stat_T Read_DriverDoorLatch_stat_DoorLatch_stat;
  Crypto_Function_serialized_T Read_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized;
  EmergencyDoorsUnlock_rqst_T Read_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst;
  FrontLidLatch_stat_T Read_FrontLidLatch_stat_FrontLidLatch_stat;
  DoorLockUnlock_T Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst;
  KeyfobInCabLocation_stat_T Read_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat;
  PushButtonStatus_T Read_KeyfobLockButton_Status_PushButtonStatus;
  KeyfobOutsideLocation_stat_T Read_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle;
  PushButtonStatus_T Read_KeyfobSuperLockButton_Sta_PushButtonStatus;
  PushButtonStatus_T Read_KeyfobUnlockButton_Status_PushButtonStatus;
  PushButtonStatus_T Read_LeftDoorButton_stat_PushButtonStatus;
  DoorAjar_stat_T Read_PassengerDoorAjar_stat_DoorAjar_stat;
  DoorLatch_stat_T Read_PassengerDoorLatch_stat_DoorLatch_stat;
  Crypto_Function_serialized_T Read_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized;
  DoorLatch_stat_T Read_PsngDoorLatchInternal_stat_DoorLatch_stat;
  DoorAjar_stat_T Read_PsngrDoorAjarInternal_stat_DoorAjar_stat;
  PushButtonStatus_T Read_RightDoorButton_stat_PushButtonStatus;
  SpeedLockingInhibition_stat_T Read_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat;
  VehicleModeDistribution_T Read_SwcActivation_Security_SwcActivation_Security;
  Synch_Unsynch_Mode_stat_T Read_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat;
  StandardNVM_T Read_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I;
  VehicleMode_T Read_VehicleModeInternal_VehicleMode;
  PushButtonStatus_T Read_WRCLockButtonStatus_PushButtonStatus;
  PushButtonStatus_T Read_WRCUnlockButtonStatus_PushButtonStatus;
  Speed16bit_T Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed;

  SEWS_DoorLockingFailureTimeout_X1CX9_T X1CX9_DoorLockingFailureTimeout_v_data;
  SEWS_AlarmAutoRelockRequestDuration_X1CYA_T X1CYA_AlarmAutoRelockRequestDuration_v_data;

  SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T P1O8Q_DoorLockHazardIndicationRqstDelay_v_data;
  SEWS_DoorAutoLockingSpeed_P1B2Q_T P1B2Q_DoorAutoLockingSpeed_v_data;
  SEWS_AutoAlarmReactivationTimeout_P1B2S_T P1B2S_AutoAlarmReactivationTimeout_v_data;
  SEWS_DoorLatchProtectionTimeWindow_P1DW8_T P1DW8_DoorLatchProtectionTimeWindow_v_data;
  SEWS_DoorLatchProtectionRestingTime_P1DW9_T P1DW9_DoorLatchProtectionRestingTime_v_data;
  SEWS_DoorIndicationReqDuration_P1DWP_T P1DWP_DoorIndicationReqDuration_v_data;
  SEWS_DoorLatchProtectMaxOperation_P1DXA_T P1DXA_DoorLatchProtectMaxOperation_v_data;
  SEWS_SpeedRelockingReinitThreshold_P1H55_T P1H55_SpeedRelockingReinitThreshold_v_data;
  SEWS_DashboardLedTimeout_P1IZ4_T P1IZ4_DashboardLedTimeout_v_data;
  SEWS_LockFunctionHardwareInterface_P1MXZ_T P1MXZ_LockFunctionHardwareInterface_v_data;
  SEWS_PassiveEntryFunction_Type_P1VKF_T P1VKF_PassiveEntryFunction_Type_v_data;
  boolean P1NE9_KeyInsertDetection_Enabled_v_data;
  boolean P1NQE_LockModeHandling_v_data;
  boolean P1VKI_PassiveStart_Installed_v_data;

  Crypto_Function_serialized_T Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized;
  Crypto_Function_serialized_T Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized;
  StandardNVM_T Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I;

  Issm_IssStateType Call_UR_ANW_LockControlActDeactivation_GetIssState_issState = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CX9_DoorLockingFailureTimeout_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_X1CX9_DoorLockingFailureTimeout_v();
  X1CYA_AlarmAutoRelockRequestDuration_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_X1CYA_AlarmAutoRelockRequestDuration_v();

  P1O8Q_DoorLockHazardIndicationRqstDelay_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1O8Q_DoorLockHazardIndicationRqstDelay_v();
  P1B2Q_DoorAutoLockingSpeed_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1B2Q_DoorAutoLockingSpeed_v();
  P1B2S_AutoAlarmReactivationTimeout_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1B2S_AutoAlarmReactivationTimeout_v();
  P1DW8_DoorLatchProtectionTimeWindow_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1DW8_DoorLatchProtectionTimeWindow_v();
  P1DW9_DoorLatchProtectionRestingTime_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1DW9_DoorLatchProtectionRestingTime_v();
  P1DWP_DoorIndicationReqDuration_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1DWP_DoorIndicationReqDuration_v();
  P1DXA_DoorLatchProtectMaxOperation_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1DXA_DoorLatchProtectMaxOperation_v();
  P1H55_SpeedRelockingReinitThreshold_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1H55_SpeedRelockingReinitThreshold_v();
  P1IZ4_DashboardLedTimeout_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1IZ4_DashboardLedTimeout_v();
  P1MXZ_LockFunctionHardwareInterface_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1MXZ_LockFunctionHardwareInterface_v();
  P1VKF_PassiveEntryFunction_Type_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1VKF_PassiveEntryFunction_Type_v();
  P1NE9_KeyInsertDetection_Enabled_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1NE9_KeyInsertDetection_Enabled_v();
  P1NQE_LockModeHandling_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1NQE_LockModeHandling_v();
  P1VKI_PassiveStart_Installed_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1VKI_PassiveStart_Installed_v();

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_AutorelockingMovements_stat_AutorelockingMovements_stat(&Read_AutorelockingMovements_stat_AutorelockingMovements_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_DriverDoorAjarInternal_stat_DoorAjar_stat(&Read_DriverDoorAjarInternal_stat_DoorAjar_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_DriverDoorAjar_stat_DoorAjar_stat(&Read_DriverDoorAjar_stat_DoorAjar_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_DriverDoorLatchInternal_stat_DoorLatch_stat(&Read_DriverDoorLatchInternal_stat_DoorLatch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_DriverDoorLatch_stat_DoorLatch_stat(&Read_DriverDoorLatch_stat_DoorLatch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized(Read_DrivrDrKeyCylTrn_st_serialized_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst(&Read_EmergencyDoorsUnlock_rqst_EmergencyDoorsUnlock_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_FrontLidLatch_stat_FrontLidLatch_stat(&Read_FrontLidLatch_stat_FrontLidLatch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst(&Read_IncabDoorLockUnlock_rqst_IncabDoorLockUnlock_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(&Read_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobLockButton_Status_PushButtonStatus(&Read_KeyfobLockButton_Status_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(&Read_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobSuperLockButton_Sta_PushButtonStatus(&Read_KeyfobSuperLockButton_Sta_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(&Read_KeyfobUnlockButton_Status_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_LeftDoorButton_stat_PushButtonStatus(&Read_LeftDoorButton_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_PassengerDoorAjar_stat_DoorAjar_stat(&Read_PassengerDoorAjar_stat_DoorAjar_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_PassengerDoorLatch_stat_DoorLatch_stat(&Read_PassengerDoorLatch_stat_DoorLatch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized(Read_PsgDoorKeyCylTrn_st_serialized_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_PsngDoorLatchInternal_stat_DoorLatch_stat(&Read_PsngDoorLatchInternal_stat_DoorLatch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_PsngrDoorAjarInternal_stat_DoorAjar_stat(&Read_PsngrDoorAjarInternal_stat_DoorAjar_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_RightDoorButton_stat_PushButtonStatus(&Read_RightDoorButton_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat(&Read_SpeedLockingInhibition_stat_SpeedLockingInhibition_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_SwcActivation_Security_SwcActivation_Security(&Read_SwcActivation_Security_SwcActivation_Security);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat(&Read_Synch_Unsynch_Mode_stat_Synch_Unsynch_Mode_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(Read_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_WRCLockButtonStatus_PushButtonStatus(&Read_WRCLockButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_WRCUnlockButtonStatus_PushButtonStatus(&Read_WRCUnlockButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(&Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_AutoRelock_rqst_AutoRelock_rqst(Rte_InitValue_AutoRelock_rqst_AutoRelock_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_DashboardLockSwitch_Devic_DeviceIndication(Rte_InitValue_DashboardLockSwitch_Devic_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_DoorLock_stat_DoorLock_stat(Rte_InitValue_DoorLock_stat_DoorLock_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_DoorsAjar_stat_DoorsAjar_stat(Rte_InitValue_DoorsAjar_stat_DoorsAjar_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger(Rte_InitValue_DriverDoorLatch_rqst_CryptTrig_CryptoTrigger);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(Rte_InitValue_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized, 0, sizeof(Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized));
  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized(Write_DriverDoorLatch_rqt_serialized_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_FrontLidLatch_cmd_FrontLidLatch_cmd(Rte_InitValue_FrontLidLatch_cmd_FrontLidLatch_cmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_KeyfobLocation_rqst_KeyfobLocation_rqst(Rte_InitValue_KeyfobLocation_rqst_KeyfobLocation_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_LockingIndication_rqst_LockingIndication_rqst(Rte_InitValue_LockingIndication_rqst_LockingIndication_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(Rte_InitValue_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger(Rte_InitValue_PsngrDoorLatch_rqst_CryptTrig_CryptoTrigger);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized, 0, sizeof(Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized));
  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized(Write_PsngrDoorLatch_rqst_serialized_Crypto_Function_serialized);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I, 0, sizeof(Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I));
  fct_status = TSC_VehicleAccess_Ctrl_Rte_Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I(Write_VehicleAccess_Ctrl_NVM_I_VehicleAccess_Ctrl_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlActDeactivation_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlActDeactivation_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlActDeactivation_GetIssState(&Call_UR_ANW_LockControlActDeactivation_GetIssState_issState);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlCabRqst2_Pending_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_VehicleAccess_Ctrl_Rte_Call_UR_ANW_LockControlCabRqst2_Pending_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  VehicleAccess_Ctrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VehicleAccess_Ctrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, VehicleAccess_Ctrl_CODE) VehicleAccess_Ctrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VehicleAccess_Ctrl_Init
 *********************************************************************************************************************/

  SEWS_DoorLockingFailureTimeout_X1CX9_T X1CX9_DoorLockingFailureTimeout_v_data;
  SEWS_AlarmAutoRelockRequestDuration_X1CYA_T X1CYA_AlarmAutoRelockRequestDuration_v_data;

  SEWS_DoorLockHazardIndicationRqstDelay_P1O8Q_T P1O8Q_DoorLockHazardIndicationRqstDelay_v_data;
  SEWS_DoorAutoLockingSpeed_P1B2Q_T P1B2Q_DoorAutoLockingSpeed_v_data;
  SEWS_AutoAlarmReactivationTimeout_P1B2S_T P1B2S_AutoAlarmReactivationTimeout_v_data;
  SEWS_DoorLatchProtectionTimeWindow_P1DW8_T P1DW8_DoorLatchProtectionTimeWindow_v_data;
  SEWS_DoorLatchProtectionRestingTime_P1DW9_T P1DW9_DoorLatchProtectionRestingTime_v_data;
  SEWS_DoorIndicationReqDuration_P1DWP_T P1DWP_DoorIndicationReqDuration_v_data;
  SEWS_DoorLatchProtectMaxOperation_P1DXA_T P1DXA_DoorLatchProtectMaxOperation_v_data;
  SEWS_SpeedRelockingReinitThreshold_P1H55_T P1H55_SpeedRelockingReinitThreshold_v_data;
  SEWS_DashboardLedTimeout_P1IZ4_T P1IZ4_DashboardLedTimeout_v_data;
  SEWS_LockFunctionHardwareInterface_P1MXZ_T P1MXZ_LockFunctionHardwareInterface_v_data;
  SEWS_PassiveEntryFunction_Type_P1VKF_T P1VKF_PassiveEntryFunction_Type_v_data;
  boolean P1NE9_KeyInsertDetection_Enabled_v_data;
  boolean P1NQE_LockModeHandling_v_data;
  boolean P1VKI_PassiveStart_Installed_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CX9_DoorLockingFailureTimeout_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_X1CX9_DoorLockingFailureTimeout_v();
  X1CYA_AlarmAutoRelockRequestDuration_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_X1CYA_AlarmAutoRelockRequestDuration_v();

  P1O8Q_DoorLockHazardIndicationRqstDelay_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1O8Q_DoorLockHazardIndicationRqstDelay_v();
  P1B2Q_DoorAutoLockingSpeed_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1B2Q_DoorAutoLockingSpeed_v();
  P1B2S_AutoAlarmReactivationTimeout_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1B2S_AutoAlarmReactivationTimeout_v();
  P1DW8_DoorLatchProtectionTimeWindow_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1DW8_DoorLatchProtectionTimeWindow_v();
  P1DW9_DoorLatchProtectionRestingTime_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1DW9_DoorLatchProtectionRestingTime_v();
  P1DWP_DoorIndicationReqDuration_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1DWP_DoorIndicationReqDuration_v();
  P1DXA_DoorLatchProtectMaxOperation_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1DXA_DoorLatchProtectMaxOperation_v();
  P1H55_SpeedRelockingReinitThreshold_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1H55_SpeedRelockingReinitThreshold_v();
  P1IZ4_DashboardLedTimeout_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1IZ4_DashboardLedTimeout_v();
  P1MXZ_LockFunctionHardwareInterface_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1MXZ_LockFunctionHardwareInterface_v();
  P1VKF_PassiveEntryFunction_Type_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1VKF_PassiveEntryFunction_Type_v();
  P1NE9_KeyInsertDetection_Enabled_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1NE9_KeyInsertDetection_Enabled_v();
  P1NQE_LockModeHandling_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1NQE_LockModeHandling_v();
  P1VKI_PassiveStart_Installed_v_data = TSC_VehicleAccess_Ctrl_Rte_Prm_P1VKI_PassiveStart_Installed_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define VehicleAccess_Ctrl_STOP_SEC_CODE
#include "VehicleAccess_Ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void VehicleAccess_Ctrl_TestDefines(void)
{
  /* Enumeration Data Types */

  AutoRelock_rqst_T Test_AutoRelock_rqst_T_V_1 = AutoRelock_rqst_Idle;
  AutoRelock_rqst_T Test_AutoRelock_rqst_T_V_2 = AutoRelock_rqst_AutoRelockActivated;
  AutoRelock_rqst_T Test_AutoRelock_rqst_T_V_3 = AutoRelock_rqst_Error;
  AutoRelock_rqst_T Test_AutoRelock_rqst_T_V_4 = AutoRelock_rqst_NotAvailable;

  AutorelockingMovements_stat_T Test_AutorelockingMovements_stat_T_V_1 = AutorelockingMovements_stat_Idle;
  AutorelockingMovements_stat_T Test_AutorelockingMovements_stat_T_V_2 = AutorelockingMovements_stat_NoMovementHasBeenDetected;
  AutorelockingMovements_stat_T Test_AutorelockingMovements_stat_T_V_3 = AutorelockingMovements_stat_MovementHasBeenDetected;
  AutorelockingMovements_stat_T Test_AutorelockingMovements_stat_T_V_4 = AutorelockingMovements_stat_Spare;
  AutorelockingMovements_stat_T Test_AutorelockingMovements_stat_T_V_5 = AutorelockingMovements_stat_Spare_01;
  AutorelockingMovements_stat_T Test_AutorelockingMovements_stat_T_V_6 = AutorelockingMovements_stat_Spare_02;
  AutorelockingMovements_stat_T Test_AutorelockingMovements_stat_T_V_7 = AutorelockingMovements_stat_Error;
  AutorelockingMovements_stat_T Test_AutorelockingMovements_stat_T_V_8 = AutorelockingMovements_stat_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DoorAjar_stat_T Test_DoorAjar_stat_T_V_1 = DoorAjar_stat_Idle;
  DoorAjar_stat_T Test_DoorAjar_stat_T_V_2 = DoorAjar_stat_DoorClosed;
  DoorAjar_stat_T Test_DoorAjar_stat_T_V_3 = DoorAjar_stat_DoorOpen;
  DoorAjar_stat_T Test_DoorAjar_stat_T_V_4 = DoorAjar_stat_Error;
  DoorAjar_stat_T Test_DoorAjar_stat_T_V_5 = DoorAjar_stat_NotAvailable;

  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_1 = DoorLatch_rqst_decrypt_Idle;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_2 = DoorLatch_rqst_decrypt_LockDoorCommand;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_3 = DoorLatch_rqst_decrypt_UnlockDoorCommand;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_4 = DoorLatch_rqst_decrypt_EmergencyUnlockRequest;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_5 = DoorLatch_rqst_decrypt_Spare1;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_6 = DoorLatch_rqst_decrypt_Spare2;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_7 = DoorLatch_rqst_decrypt_Error;
  DoorLatch_rqst_decrypt_T Test_DoorLatch_rqst_decrypt_T_V_8 = DoorLatch_rqst_decrypt_NotAvailable;

  DoorLatch_stat_T Test_DoorLatch_stat_T_V_1 = DoorLatch_stat_Idle;
  DoorLatch_stat_T Test_DoorLatch_stat_T_V_2 = DoorLatch_stat_LatchUnlockedFiltered;
  DoorLatch_stat_T Test_DoorLatch_stat_T_V_3 = DoorLatch_stat_LatchLockedFiltered;
  DoorLatch_stat_T Test_DoorLatch_stat_T_V_4 = DoorLatch_stat_Error;
  DoorLatch_stat_T Test_DoorLatch_stat_T_V_5 = DoorLatch_stat_NotAvailable;

  DoorLockUnlock_T Test_DoorLockUnlock_T_V_1 = DoorLockUnlock_Idle;
  DoorLockUnlock_T Test_DoorLockUnlock_T_V_2 = DoorLockUnlock_Unlock;
  DoorLockUnlock_T Test_DoorLockUnlock_T_V_3 = DoorLockUnlock_Lock;
  DoorLockUnlock_T Test_DoorLockUnlock_T_V_4 = DoorLockUnlock_MonoLockUnlock;
  DoorLockUnlock_T Test_DoorLockUnlock_T_V_5 = DoorLockUnlock_Spare1;
  DoorLockUnlock_T Test_DoorLockUnlock_T_V_6 = DoorLockUnlock_Spare2;
  DoorLockUnlock_T Test_DoorLockUnlock_T_V_7 = DoorLockUnlock_Error;
  DoorLockUnlock_T Test_DoorLockUnlock_T_V_8 = DoorLockUnlock_NotAvailable;

  DoorLock_stat_T Test_DoorLock_stat_T_V_1 = DoorLock_stat_Idle;
  DoorLock_stat_T Test_DoorLock_stat_T_V_2 = DoorLock_stat_BothDoorsAreUnlocked;
  DoorLock_stat_T Test_DoorLock_stat_T_V_3 = DoorLock_stat_DriverDoorIsUnlocked;
  DoorLock_stat_T Test_DoorLock_stat_T_V_4 = DoorLock_stat_PassengerDoorIsUnlocked;
  DoorLock_stat_T Test_DoorLock_stat_T_V_5 = DoorLock_stat_BothDoorsAreLocked;
  DoorLock_stat_T Test_DoorLock_stat_T_V_6 = DoorLock_stat_Error;
  DoorLock_stat_T Test_DoorLock_stat_T_V_7 = DoorLock_stat_NotAvailable;

  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_1 = DoorsAjar_stat_Idle;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_2 = DoorsAjar_stat_BothDoorsAreClosed;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_3 = DoorsAjar_stat_DriverDoorIsOpen;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_4 = DoorsAjar_stat_PassengerDoorIsOpen;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_5 = DoorsAjar_stat_BothDoorsAreOpen;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_6 = DoorsAjar_stat_Spare;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_7 = DoorsAjar_stat_Error;
  DoorsAjar_stat_T Test_DoorsAjar_stat_T_V_8 = DoorsAjar_stat_NotAvailable;

  EmergencyDoorsUnlock_rqst_T Test_EmergencyDoorsUnlock_rqst_T_V_1 = EmergencyDoorsUnlock_rqst_Idle;
  EmergencyDoorsUnlock_rqst_T Test_EmergencyDoorsUnlock_rqst_T_V_2 = EmergencyDoorsUnlock_rqst_Spare;
  EmergencyDoorsUnlock_rqst_T Test_EmergencyDoorsUnlock_rqst_T_V_3 = EmergencyDoorsUnlock_rqst_EmergencyUnlockRequest;
  EmergencyDoorsUnlock_rqst_T Test_EmergencyDoorsUnlock_rqst_T_V_4 = EmergencyDoorsUnlock_rqst_Error;
  EmergencyDoorsUnlock_rqst_T Test_EmergencyDoorsUnlock_rqst_T_V_5 = EmergencyDoorsUnlock_rqst_NotAvailable;

  FrontLidLatch_cmd_T Test_FrontLidLatch_cmd_T_V_1 = FrontLidLatch_cmd_Idle;
  FrontLidLatch_cmd_T Test_FrontLidLatch_cmd_T_V_2 = FrontLidLatch_cmd_LockFrontLidCommand;
  FrontLidLatch_cmd_T Test_FrontLidLatch_cmd_T_V_3 = FrontLidLatch_cmd_UnlockFrontLidCommand;
  FrontLidLatch_cmd_T Test_FrontLidLatch_cmd_T_V_4 = FrontLidLatch_cmd_Error;
  FrontLidLatch_cmd_T Test_FrontLidLatch_cmd_T_V_5 = FrontLidLatch_cmd_NotAvailable;

  FrontLidLatch_stat_T Test_FrontLidLatch_stat_T_V_1 = FrontLidLatch_stat_Idle;
  FrontLidLatch_stat_T Test_FrontLidLatch_stat_T_V_2 = FrontLidLatch_stat_LatchUnlocked;
  FrontLidLatch_stat_T Test_FrontLidLatch_stat_T_V_3 = FrontLidLatch_stat_LatchLocked;
  FrontLidLatch_stat_T Test_FrontLidLatch_stat_T_V_4 = FrontLidLatch_stat_Error;
  FrontLidLatch_stat_T Test_FrontLidLatch_stat_T_V_5 = FrontLidLatch_stat_NotAvailable;

  Issm_IssStateType Test_Issm_IssStateType_V_1 = ISSM_STATE_INACTIVE;
  Issm_IssStateType Test_Issm_IssStateType_V_2 = ISSM_STATE_PENDING;
  Issm_IssStateType Test_Issm_IssStateType_V_3 = ISSM_STATE_ACTIVE;

  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_1 = KeyfobInCabLocation_stat_Idle;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_2 = KeyfobInCabLocation_stat_NotDetected_Incab;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_3 = KeyfobInCabLocation_stat_Detected_Incab;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_4 = KeyfobInCabLocation_stat_Spare1;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_5 = KeyfobInCabLocation_stat_Spare2;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_6 = KeyfobInCabLocation_stat_Spare3;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_7 = KeyfobInCabLocation_stat_Error;
  KeyfobInCabLocation_stat_T Test_KeyfobInCabLocation_stat_T_V_8 = KeyfobInCabLocation_stat_NotAvailable;

  KeyfobLocation_rqst_T Test_KeyfobLocation_rqst_T_V_1 = KeyfobLocation_rqst_NoRequest;
  KeyfobLocation_rqst_T Test_KeyfobLocation_rqst_T_V_2 = KeyfobLocation_rqst_Request;
  KeyfobLocation_rqst_T Test_KeyfobLocation_rqst_T_V_3 = KeyfobLocation_rqst_Error;
  KeyfobLocation_rqst_T Test_KeyfobLocation_rqst_T_V_4 = KeyfobLocation_rqst_NotAvailable;

  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_1 = KeyfobOutsideLocation_stat_Idle;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_2 = KeyfobOutsideLocation_stat_NotDetectedOutside;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_3 = KeyfobOutsideLocation_stat_DetectedOnRightSide;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_4 = KeyfobOutsideLocation_stat_DetectedOnLeftSide;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_5 = KeyfobOutsideLocation_stat_DetectedOnBothSides;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_6 = KeyfobOutsideLocation_stat_DetectedOutOfDoorAreas;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_7 = KeyfobOutsideLocation_stat_Error;
  KeyfobOutsideLocation_stat_T Test_KeyfobOutsideLocation_stat_T_V_8 = KeyfobOutsideLocation_stat_NotAvailable;

  LockingIndication_rqst_T Test_LockingIndication_rqst_T_V_1 = LockingIndication_rqst_Idle;
  LockingIndication_rqst_T Test_LockingIndication_rqst_T_V_2 = LockingIndication_rqst_Lock;
  LockingIndication_rqst_T Test_LockingIndication_rqst_T_V_3 = LockingIndication_rqst_Unlock;
  LockingIndication_rqst_T Test_LockingIndication_rqst_T_V_4 = LockingIndication_rqst_Spare;
  LockingIndication_rqst_T Test_LockingIndication_rqst_T_V_5 = LockingIndication_rqst_Spare01;
  LockingIndication_rqst_T Test_LockingIndication_rqst_T_V_6 = LockingIndication_rqst_Spare02;
  LockingIndication_rqst_T Test_LockingIndication_rqst_T_V_7 = LockingIndication_rqst_Error;
  LockingIndication_rqst_T Test_LockingIndication_rqst_T_V_8 = LockingIndication_rqst_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  SpeedLockingInhibition_stat_T Test_SpeedLockingInhibition_stat_T_V_1 = SpeedLockingInhibition_stat_Idle;
  SpeedLockingInhibition_stat_T Test_SpeedLockingInhibition_stat_T_V_2 = SpeedLockingInhibition_stat_SpeedLockingActivate;
  SpeedLockingInhibition_stat_T Test_SpeedLockingInhibition_stat_T_V_3 = SpeedLockingInhibition_stat_SpeedLockingDeactivate;
  SpeedLockingInhibition_stat_T Test_SpeedLockingInhibition_stat_T_V_4 = SpeedLockingInhibition_stat_Error;
  SpeedLockingInhibition_stat_T Test_SpeedLockingInhibition_stat_T_V_5 = SpeedLockingInhibition_stat_NotAvailable;

  Synch_Unsynch_Mode_stat_T Test_Synch_Unsynch_Mode_stat_T_V_1 = Synch_Unsynch_Mode_stat_Idle;
  Synch_Unsynch_Mode_stat_T Test_Synch_Unsynch_Mode_stat_T_V_2 = Synch_Unsynch_Mode_stat_SynchronizedMode;
  Synch_Unsynch_Mode_stat_T Test_Synch_Unsynch_Mode_stat_T_V_3 = Synch_Unsynch_Mode_stat_UnsynchronizedMode;
  Synch_Unsynch_Mode_stat_T Test_Synch_Unsynch_Mode_stat_T_V_4 = Synch_Unsynch_Mode_stat_Spare;
  Synch_Unsynch_Mode_stat_T Test_Synch_Unsynch_Mode_stat_T_V_5 = Synch_Unsynch_Mode_stat_Spare_01;
  Synch_Unsynch_Mode_stat_T Test_Synch_Unsynch_Mode_stat_T_V_6 = Synch_Unsynch_Mode_stat_Spare_02;
  Synch_Unsynch_Mode_stat_T Test_Synch_Unsynch_Mode_stat_T_V_7 = Synch_Unsynch_Mode_stat_Error;
  Synch_Unsynch_Mode_stat_T Test_Synch_Unsynch_Mode_stat_T_V_8 = Synch_Unsynch_Mode_stat_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
