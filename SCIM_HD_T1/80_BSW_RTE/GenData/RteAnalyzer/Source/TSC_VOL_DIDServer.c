/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_VOL_DIDServer.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_VOL_DIDServer.h"
#include "TSC_VOL_DIDServer.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_VOL_DIDServer_Rte_Read_AmbientAirTemperature_AmbientAirTemperature(Temperature16bit_T *data)
{
  return Rte_Read_AmbientAirTemperature_AmbientAirTemperature(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_VOL_DIDServer_Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(Distance32bit_T *data)
{
  return Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_VOL_DIDServer_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */
Std_ReturnType TSC_VOL_DIDServer_Rte_Call_CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo)
{
  return Rte_Call_CddLinDiagServices_SlaveNodePnSnReq(LinBusInfo);
}
Std_ReturnType TSC_VOL_DIDServer_Rte_Call_CddLinDiagServices_SlaveNodePnSnResp(LinDiagServiceStatus *DiagServiceStatus, uint8 *NoOfLinSlaves, uint8 *LinDiagRespPNSN)
{
  return Rte_Call_CddLinDiagServices_SlaveNodePnSnResp(DiagServiceStatus, NoOfLinSlaves, LinDiagRespPNSN);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */
uint8 TSC_VOL_DIDServer_Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(void)
{
  return Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl();
}




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





boolean  TSC_VOL_DIDServer_Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v(void)
{
  return (boolean ) Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v();
}
boolean  TSC_VOL_DIDServer_Rte_Prm_P1C54_FactoryModeActive_v(void)
{
  return (boolean ) Rte_Prm_P1C54_FactoryModeActive_v();
}
SEWS_ChassisId_CHANO_T * TSC_VOL_DIDServer_Rte_Prm_CHANO_ChassisId_v(void)
{
  return (SEWS_ChassisId_CHANO_T *) Rte_Prm_CHANO_ChassisId_v();
}
SEWS_VIN_VINNO_T * TSC_VOL_DIDServer_Rte_Prm_VINNO_VIN_v(void)
{
  return (SEWS_VIN_VINNO_T *) Rte_Prm_VINNO_VIN_v();
}


     /* VOL_DIDServer */
      /* VOL_DIDServer */



