/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_VEC_RomTest.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Service interfaces */
Std_ReturnType TSC_VEC_RomTest_Rte_Call_CsmSignatureVerify_SignatureVerifyFinish(const uint8 *signatureBuffer, uint32 signatureLength, Csm_VerifyResultType *resultBuffer);
Std_ReturnType TSC_VEC_RomTest_Rte_Call_CsmSignatureVerify_SignatureVerifyStart(const AsymPublicKeyType *key);
Std_ReturnType TSC_VEC_RomTest_Rte_Call_CsmSignatureVerify_SignatureVerifyUpdate(const uint8 *dataBuffer, uint32 dataLength);

/** Exclusive Areas */
void TSC_VEC_RomTest_Rte_Enter_VEC_ExclusiveArea(void);
void TSC_VEC_RomTest_Rte_Exit_VEC_ExclusiveArea(void);




