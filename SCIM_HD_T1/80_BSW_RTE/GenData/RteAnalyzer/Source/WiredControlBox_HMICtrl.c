/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  WiredControlBox_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  WiredControlBox_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <WiredControlBox_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   
 *
 * SEWS_ECSStopButtonHoldTimeout_P1DWI_T
 *   
 *
 * SEWS_P1BWF_Level_Memorization_Min_Press_T
 *   
 *
 * SEWS_P1BWF_Memory_Recall_Max_Press_T
 *   
 *
 * SEWS_P1BWF_Memory_Recall_Min_Press_T
 *   
 *
 * SEWS_P1JSY_DriveStroke_T
 *   
 *
 * SEWS_P1JSY_RampStroke_T
 *   
 *
 * SEWS_P1JSZ_DriveStroke_T
 *   
 *
 * SEWS_P1JSZ_RampStroke_T
 *   
 *
 * SEWS_RCECSButtonStucked_P1DWJ_T
 *   
 *
 * SEWS_RCECSUpDownStucked_P1DWK_T
 *   
 *
 * SEWS_RCECS_HoldCircuitTimer_P1IUS_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_WiredControlBox_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_WiredControlBox_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void WiredControlBox_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_ECSStopButtonHoldTimeout_P1DWI_T: Integer in interval [0...255]
 * SEWS_P1BWF_Level_Memorization_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Max_Press_T: Integer in interval [0...255]
 * SEWS_P1BWF_Memory_Recall_Min_Press_T: Integer in interval [0...255]
 * SEWS_P1JSY_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSY_RampStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_DriveStroke_T: Integer in interval [0...255]
 * SEWS_P1JSZ_RampStroke_T: Integer in interval [0...255]
 * SEWS_RCECSButtonStucked_P1DWJ_T: Integer in interval [0...255]
 * SEWS_RCECSUpDownStucked_P1DWK_T: Integer in interval [0...255]
 * SEWS_RCECS_HoldCircuitTimer_P1IUS_T: Integer in interval [0...255]
 * ShortPulseMaxLength_T: Integer in interval [0...15]
 *   Unit: [ms], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * ECSStandByReq_T: Enumeration of integer in interval [0...7] with enumerators
 *   ECSStandByReq_Idle (0U)
 *   ECSStandByReq_StandbyRequested (1U)
 *   ECSStandByReq_StopStandby (2U)
 *   ECSStandByReq_Reserved (3U)
 *   ECSStandByReq_Reserved_01 (4U)
 *   ECSStandByReq_Reserved_02 (5U)
 *   ECSStandByReq_Error (6U)
 *   ECSStandByReq_NotAvailable (7U)
 * EvalButtonRequest_T: Enumeration of integer in interval [0...7] with enumerators
 *   EvalButtonRequest_Neutral (0U)
 *   EvalButtonRequest_EvaluatingPush (1U)
 *   EvalButtonRequest_ContinuouslyPushed (2U)
 *   EvalButtonRequest_ShortPush (3U)
 *   EvalButtonRequest_Spare1 (4U)
 *   EvalButtonRequest_Spare2 (5U)
 *   EvalButtonRequest_Error (6U)
 *   EvalButtonRequest_NotAvailable (7U)
 * FalseTrue_T: Enumeration of integer in interval [0...3] with enumerators
 *   FalseTrue_False (0U)
 *   FalseTrue_True (1U)
 *   FalseTrue_Error (2U)
 *   FalseTrue_NotAvaiable (3U)
 * LevelAdjustmentAction_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelAdjustmentAction_Idle (0U)
 *   LevelAdjustmentAction_UpBasic (1U)
 *   LevelAdjustmentAction_DownBasic (2U)
 *   LevelAdjustmentAction_UpShortMovement (3U)
 *   LevelAdjustmentAction_DownShortMovement (4U)
 *   LevelAdjustmentAction_Reserved (5U)
 *   LevelAdjustmentAction_GotoDriveLevel (6U)
 *   LevelAdjustmentAction_Reserved_01 (7U)
 *   LevelAdjustmentAction_Ferry (8U)
 *   LevelAdjustmentAction_Reserved_02 (9U)
 *   LevelAdjustmentAction_Reserved_03 (10U)
 *   LevelAdjustmentAction_Reserved_04 (11U)
 *   LevelAdjustmentAction_Reserved_05 (12U)
 *   LevelAdjustmentAction_Reserved_06 (13U)
 *   LevelAdjustmentAction_Error (14U)
 *   LevelAdjustmentAction_NotAvailable (15U)
 * LevelAdjustmentAxles_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentAxles_Rear (0U)
 *   LevelAdjustmentAxles_Front (1U)
 *   LevelAdjustmentAxles_Parallel (2U)
 *   LevelAdjustmentAxles_Reserved (3U)
 *   LevelAdjustmentAxles_Reserved_01 (4U)
 *   LevelAdjustmentAxles_Reserved_02 (5U)
 *   LevelAdjustmentAxles_Error (6U)
 *   LevelAdjustmentAxles_NotAvailable (7U)
 * LevelAdjustmentStroke_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelAdjustmentStroke_DriveStroke (0U)
 *   LevelAdjustmentStroke_DockingStroke (1U)
 *   LevelAdjustmentStroke_Reserved (2U)
 *   LevelAdjustmentStroke_Reserved_01 (3U)
 *   LevelAdjustmentStroke_Reserved_02 (4U)
 *   LevelAdjustmentStroke_Reserved_03 (5U)
 *   LevelAdjustmentStroke_Error (6U)
 *   LevelAdjustmentStroke_NotAvailable (7U)
 * LevelControlInformation_T: Enumeration of integer in interval [0...15] with enumerators
 *   LevelControlInformation_Prohibit (0U)
 *   LevelControlInformation_HeightAdjustment (1U)
 *   LevelControlInformation_DockingHeight (2U)
 *   LevelControlInformation_DriveHeight (3U)
 *   LevelControlInformation_KneelingHeight (4U)
 *   LevelControlInformation_FerryFunction (5U)
 *   LevelControlInformation_LimpHome (6U)
 *   LevelControlInformation_Reserved (7U)
 *   LevelControlInformation_Reserved_01 (8U)
 *   LevelControlInformation_Reserved_02 (9U)
 *   LevelControlInformation_Reserved_03 (10U)
 *   LevelControlInformation_Reserved_04 (11U)
 *   LevelControlInformation_Reserved_05 (12U)
 *   LevelControlInformation_Reserved_06 (13U)
 *   LevelControlInformation_Error (14U)
 *   LevelControlInformation_NotAvailable (15U)
 * LevelUserMemoryAction_T: Enumeration of integer in interval [0...7] with enumerators
 *   LevelUserMemoryAction_Inactive (0U)
 *   LevelUserMemoryAction_Recall (1U)
 *   LevelUserMemoryAction_Store (2U)
 *   LevelUserMemoryAction_Default (3U)
 *   LevelUserMemoryAction_Reserved (4U)
 *   LevelUserMemoryAction_Reserved_01 (5U)
 *   LevelUserMemoryAction_Error (6U)
 *   LevelUserMemoryAction_NotAvailable (7U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * RampLevelRequest_T: Enumeration of integer in interval [0...15] with enumerators
 *   RampLevelRequest_TakeNoAction (0U)
 *   RampLevelRequest_RampLevelM1 (1U)
 *   RampLevelRequest_RampLevelM2 (2U)
 *   RampLevelRequest_RampLevelM3 (3U)
 *   RampLevelRequest_RampLevelM4 (4U)
 *   RampLevelRequest_RampLevelM5 (5U)
 *   RampLevelRequest_RampLevelM6 (6U)
 *   RampLevelRequest_RampLevelM7 (7U)
 *   RampLevelRequest_NotDefined_04 (8U)
 *   RampLevelRequest_NotDefined_05 (9U)
 *   RampLevelRequest_NotDefined_06 (10U)
 *   RampLevelRequest_NotDefined_07 (11U)
 *   RampLevelRequest_NotDefined_08 (12U)
 *   RampLevelRequest_NotDefined_09 (13U)
 *   RampLevelRequest_ErrorIndicator (14U)
 *   RampLevelRequest_NotAvailable (15U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 * VehicleMode_T: Enumeration of integer in interval [0...15] with enumerators
 *   VehicleMode_Hibernate (0U)
 *   VehicleMode_Parked (1U)
 *   VehicleMode_Living (2U)
 *   VehicleMode_Accessory (3U)
 *   VehicleMode_PreRunning (4U)
 *   VehicleMode_Cranking (5U)
 *   VehicleMode_Running (6U)
 *   VehicleMode_Spare_1 (7U)
 *   VehicleMode_Spare_2 (8U)
 *   VehicleMode_Spare_3 (9U)
 *   VehicleMode_Spare_4 (10U)
 *   VehicleMode_Spare_5 (11U)
 *   VehicleMode_Spare_6 (12U)
 *   VehicleMode_Spare_7 (13U)
 *   VehicleMode_Error (14U)
 *   VehicleMode_NotAvailable (15U)
 * WiredLevelUserMemory_T: Enumeration of integer in interval [0...15] with enumerators
 *   WiredLevelUserMemory_MemOff (0U)
 *   WiredLevelUserMemory_M1 (1U)
 *   WiredLevelUserMemory_M2 (2U)
 *   WiredLevelUserMemory_M3 (3U)
 *   WiredLevelUserMemory_M4 (4U)
 *   WiredLevelUserMemory_M5 (5U)
 *   WiredLevelUserMemory_M6 (6U)
 *   WiredLevelUserMemory_M7 (7U)
 *   WiredLevelUserMemory_Spare (8U)
 *   WiredLevelUserMemory_Spare01 (9U)
 *   WiredLevelUserMemory_Spare02 (10U)
 *   WiredLevelUserMemory_Spare03 (11U)
 *   WiredLevelUserMemory_Spare04 (12U)
 *   WiredLevelUserMemory_Spare05 (13U)
 *   WiredLevelUserMemory_Error (14U)
 *   WiredLevelUserMemory_NotAvailable (15U)
 *
 * Record Types:
 * =============
 * SEWS_ECS_MemSwTimings_P1BWF_s_T: Record with elements
 *   Level_Memorization_Min_Press of type SEWS_P1BWF_Level_Memorization_Min_Press_T
 *   Memory_Recall_Min_Press of type SEWS_P1BWF_Memory_Recall_Min_Press_T
 *   Memory_Recall_Max_Press of type SEWS_P1BWF_Memory_Recall_Max_Press_T
 * SEWS_ForcedPositionStatus_Front_P1JSY_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSY_DriveStroke_T
 *   RampStroke of type SEWS_P1JSY_RampStroke_T
 * SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T: Record with elements
 *   DriveStroke of type SEWS_P1JSZ_DriveStroke_T
 *   RampStroke of type SEWS_P1JSZ_RampStroke_T
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_ECSStopButtonHoldTimeout_P1DWI_T Rte_Prm_P1DWI_ECSStopButtonHoldTimeout_v(void)
 *   SEWS_RCECSButtonStucked_P1DWJ_T Rte_Prm_P1DWJ_RCECSButtonStucked_v(void)
 *   SEWS_RCECSUpDownStucked_P1DWK_T Rte_Prm_P1DWK_RCECSUpDownStucked_v(void)
 *   SEWS_RCECS_HoldCircuitTimer_P1IUS_T Rte_Prm_P1IUS_RCECS_HoldCircuitTimer_v(void)
 *   SEWS_ECS_MemSwTimings_P1BWF_s_T *Rte_Prm_P1BWF_ECS_MemSwTimings_v(void)
 *   SEWS_ForcedPositionStatus_Front_P1JSY_s_T *Rte_Prm_P1JSY_ForcedPositionStatus_Front_v(void)
 *   SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T *Rte_Prm_P1JSZ_ForcedPositionStatus_Rear_v(void)
 *   boolean Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void)
 *   boolean Rte_Prm_P1ALU_ECS_FullAirSystem_v(void)
 *
 *********************************************************************************************************************/


#define WiredControlBox_HMICtrl_START_SEC_CODE
#include "WiredControlBox_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: WiredControlBox_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AdjustButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BackButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(FalseTrue_T *data)
 *   Std_ReturnType Rte_Read_LevelControlInformation_LevelControlInformation(LevelControlInformation_T *data)
 *   Std_ReturnType Rte_Read_MemButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_RampLevelRequest_RampLevelRequest(RampLevelRequest_T *data)
 *   Std_ReturnType Rte_Read_SelectButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_StopButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
 *   Std_ReturnType Rte_Read_WRDownButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
 *   Std_ReturnType Rte_Read_WRUpButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Adjust_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Down_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ECSStandByReqRCECS_ECSStandByReqRCECS(ECSStandByReq_T data)
 *   Std_ReturnType Rte_Write_M1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_M2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_M3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ShortPulseMaxLength_ShortPulseMaxLength(ShortPulseMaxLength_T data)
 *   Std_ReturnType Rte_Write_Up_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(FalseTrue_T data)
 *   Std_ReturnType Rte_Write_WiredLevelAdjustmentAction_LevelAdjustmentAction(LevelAdjustmentAction_T data)
 *   Std_ReturnType Rte_Write_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(LevelAdjustmentAxles_T data)
 *   Std_ReturnType Rte_Write_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(LevelAdjustmentStroke_T data)
 *   Std_ReturnType Rte_Write_WiredLevelUserMemory_WiredLevelUserMemory(WiredLevelUserMemory_T data)
 *   Std_ReturnType Rte_Write_WiredLevelUserMemoryAction_LevelUserMemoryAction(LevelUserMemoryAction_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOI_63_RCECS_ButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, WiredControlBox_HMICtrl_CODE) WiredControlBox_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PushButtonStatus_T Read_AdjustButtonStatus_PushButtonStatus;
  PushButtonStatus_T Read_BackButtonStatus_PushButtonStatus;
  FalseTrue_T Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs;
  FalseTrue_T Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed;
  LevelControlInformation_T Read_LevelControlInformation_LevelControlInformation;
  PushButtonStatus_T Read_MemButtonStatus_PushButtonStatus;
  RampLevelRequest_T Read_RampLevelRequest_RampLevelRequest;
  PushButtonStatus_T Read_SelectButtonStatus_PushButtonStatus;
  PushButtonStatus_T Read_StopButtonStatus_PushButtonStatus;
  VehicleModeDistribution_T Read_SwcActivation_Living_Living;
  VehicleMode_T Read_VehicleModeInternal_VehicleMode;
  EvalButtonRequest_T Read_WRDownButtonStatus_EvalButtonRequest;
  EvalButtonRequest_T Read_WRUpButtonStatus_EvalButtonRequest;

  SEWS_ECSStopButtonHoldTimeout_P1DWI_T P1DWI_ECSStopButtonHoldTimeout_v_data;
  SEWS_RCECSButtonStucked_P1DWJ_T P1DWJ_RCECSButtonStucked_v_data;
  SEWS_RCECSUpDownStucked_P1DWK_T P1DWK_RCECSUpDownStucked_v_data;
  SEWS_RCECS_HoldCircuitTimer_P1IUS_T P1IUS_RCECS_HoldCircuitTimer_v_data;
  SEWS_ECS_MemSwTimings_P1BWF_s_T P1BWF_ECS_MemSwTimings_v_data;
  SEWS_ForcedPositionStatus_Front_P1JSY_s_T P1JSY_ForcedPositionStatus_Front_v_data;
  SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T P1JSZ_ForcedPositionStatus_Rear_v_data;

  boolean P1ALT_ECS_PartialAirSystem_v_data;
  boolean P1ALU_ECS_FullAirSystem_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1DWI_ECSStopButtonHoldTimeout_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1DWI_ECSStopButtonHoldTimeout_v();
  P1DWJ_RCECSButtonStucked_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1DWJ_RCECSButtonStucked_v();
  P1DWK_RCECSUpDownStucked_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1DWK_RCECSUpDownStucked_v();
  P1IUS_RCECS_HoldCircuitTimer_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1IUS_RCECS_HoldCircuitTimer_v();
  P1BWF_ECS_MemSwTimings_v_data = *TSC_WiredControlBox_HMICtrl_Rte_Prm_P1BWF_ECS_MemSwTimings_v();
  P1JSY_ForcedPositionStatus_Front_v_data = *TSC_WiredControlBox_HMICtrl_Rte_Prm_P1JSY_ForcedPositionStatus_Front_v();
  P1JSZ_ForcedPositionStatus_Rear_v_data = *TSC_WiredControlBox_HMICtrl_Rte_Prm_P1JSZ_ForcedPositionStatus_Rear_v();

  P1ALT_ECS_PartialAirSystem_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1ALT_ECS_PartialAirSystem_v();
  P1ALU_ECS_FullAirSystem_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1ALU_ECS_FullAirSystem_v();

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_AdjustButtonStatus_PushButtonStatus(&Read_AdjustButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_BackButtonStatus_PushButtonStatus(&Read_BackButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs(&Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed(&Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_LevelControlInformation_LevelControlInformation(&Read_LevelControlInformation_LevelControlInformation);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_MemButtonStatus_PushButtonStatus(&Read_MemButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_RampLevelRequest_RampLevelRequest(&Read_RampLevelRequest_RampLevelRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_SelectButtonStatus_PushButtonStatus(&Read_SelectButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_StopButtonStatus_PushButtonStatus(&Read_StopButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_SwcActivation_Living_Living(&Read_SwcActivation_Living_Living);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_VehicleModeInternal_VehicleMode(&Read_VehicleModeInternal_VehicleMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_WRDownButtonStatus_EvalButtonRequest(&Read_WRDownButtonStatus_EvalButtonRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Read_WRUpButtonStatus_EvalButtonRequest(&Read_WRUpButtonStatus_EvalButtonRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_Adjust_DeviceIndication_DeviceIndication(Rte_InitValue_Adjust_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_Down_DeviceIndication_DeviceIndication(Rte_InitValue_Down_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_ECSStandByReqRCECS_ECSStandByReqRCECS(Rte_InitValue_ECSStandByReqRCECS_ECSStandByReqRCECS);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_M1_DeviceIndication_DeviceIndication(Rte_InitValue_M1_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_M2_DeviceIndication_DeviceIndication(Rte_InitValue_M2_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_M3_DeviceIndication_DeviceIndication(Rte_InitValue_M3_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_ShortPulseMaxLength_ShortPulseMaxLength(Rte_InitValue_ShortPulseMaxLength_ShortPulseMaxLength);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_Up_DeviceIndication_DeviceIndication(Rte_InitValue_Up_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(Rte_InitValue_WiredAirSuspensionStopRequest_AirSuspensionStopRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelAdjustmentAction_LevelAdjustmentAction(Rte_InitValue_WiredLevelAdjustmentAction_LevelAdjustmentAction);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(Rte_InitValue_WiredLevelAdjustmentAxles_LevelAdjustmentAxles);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(Rte_InitValue_WiredLevelAdjustmentStroke_LevelAdjustmentStroke);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelUserMemory_WiredLevelUserMemory(Rte_InitValue_WiredLevelUserMemory_WiredLevelUserMemory);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Write_WiredLevelUserMemoryAction_LevelUserMemoryAction(Rte_InitValue_WiredLevelUserMemoryAction_LevelUserMemoryAction);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_WiredControlBox_HMICtrl_Rte_Call_Event_D1BOI_63_RCECS_ButtonStuck_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  WiredControlBox_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: WiredControlBox_HMICtrl_Init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_Init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, WiredControlBox_HMICtrl_CODE) WiredControlBox_HMICtrl_Init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: WiredControlBox_HMICtrl_Init
 *********************************************************************************************************************/

  SEWS_ECSStopButtonHoldTimeout_P1DWI_T P1DWI_ECSStopButtonHoldTimeout_v_data;
  SEWS_RCECSButtonStucked_P1DWJ_T P1DWJ_RCECSButtonStucked_v_data;
  SEWS_RCECSUpDownStucked_P1DWK_T P1DWK_RCECSUpDownStucked_v_data;
  SEWS_RCECS_HoldCircuitTimer_P1IUS_T P1IUS_RCECS_HoldCircuitTimer_v_data;
  SEWS_ECS_MemSwTimings_P1BWF_s_T P1BWF_ECS_MemSwTimings_v_data;
  SEWS_ForcedPositionStatus_Front_P1JSY_s_T P1JSY_ForcedPositionStatus_Front_v_data;
  SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T P1JSZ_ForcedPositionStatus_Rear_v_data;

  boolean P1ALT_ECS_PartialAirSystem_v_data;
  boolean P1ALU_ECS_FullAirSystem_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1DWI_ECSStopButtonHoldTimeout_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1DWI_ECSStopButtonHoldTimeout_v();
  P1DWJ_RCECSButtonStucked_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1DWJ_RCECSButtonStucked_v();
  P1DWK_RCECSUpDownStucked_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1DWK_RCECSUpDownStucked_v();
  P1IUS_RCECS_HoldCircuitTimer_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1IUS_RCECS_HoldCircuitTimer_v();
  P1BWF_ECS_MemSwTimings_v_data = *TSC_WiredControlBox_HMICtrl_Rte_Prm_P1BWF_ECS_MemSwTimings_v();
  P1JSY_ForcedPositionStatus_Front_v_data = *TSC_WiredControlBox_HMICtrl_Rte_Prm_P1JSY_ForcedPositionStatus_Front_v();
  P1JSZ_ForcedPositionStatus_Rear_v_data = *TSC_WiredControlBox_HMICtrl_Rte_Prm_P1JSZ_ForcedPositionStatus_Rear_v();

  P1ALT_ECS_PartialAirSystem_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1ALT_ECS_PartialAirSystem_v();
  P1ALU_ECS_FullAirSystem_v_data = TSC_WiredControlBox_HMICtrl_Rte_Prm_P1ALU_ECS_FullAirSystem_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define WiredControlBox_HMICtrl_STOP_SEC_CODE
#include "WiredControlBox_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void WiredControlBox_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  ECSStandByReq_T Test_ECSStandByReq_T_V_1 = ECSStandByReq_Idle;
  ECSStandByReq_T Test_ECSStandByReq_T_V_2 = ECSStandByReq_StandbyRequested;
  ECSStandByReq_T Test_ECSStandByReq_T_V_3 = ECSStandByReq_StopStandby;
  ECSStandByReq_T Test_ECSStandByReq_T_V_4 = ECSStandByReq_Reserved;
  ECSStandByReq_T Test_ECSStandByReq_T_V_5 = ECSStandByReq_Reserved_01;
  ECSStandByReq_T Test_ECSStandByReq_T_V_6 = ECSStandByReq_Reserved_02;
  ECSStandByReq_T Test_ECSStandByReq_T_V_7 = ECSStandByReq_Error;
  ECSStandByReq_T Test_ECSStandByReq_T_V_8 = ECSStandByReq_NotAvailable;

  EvalButtonRequest_T Test_EvalButtonRequest_T_V_1 = EvalButtonRequest_Neutral;
  EvalButtonRequest_T Test_EvalButtonRequest_T_V_2 = EvalButtonRequest_EvaluatingPush;
  EvalButtonRequest_T Test_EvalButtonRequest_T_V_3 = EvalButtonRequest_ContinuouslyPushed;
  EvalButtonRequest_T Test_EvalButtonRequest_T_V_4 = EvalButtonRequest_ShortPush;
  EvalButtonRequest_T Test_EvalButtonRequest_T_V_5 = EvalButtonRequest_Spare1;
  EvalButtonRequest_T Test_EvalButtonRequest_T_V_6 = EvalButtonRequest_Spare2;
  EvalButtonRequest_T Test_EvalButtonRequest_T_V_7 = EvalButtonRequest_Error;
  EvalButtonRequest_T Test_EvalButtonRequest_T_V_8 = EvalButtonRequest_NotAvailable;

  FalseTrue_T Test_FalseTrue_T_V_1 = FalseTrue_False;
  FalseTrue_T Test_FalseTrue_T_V_2 = FalseTrue_True;
  FalseTrue_T Test_FalseTrue_T_V_3 = FalseTrue_Error;
  FalseTrue_T Test_FalseTrue_T_V_4 = FalseTrue_NotAvaiable;

  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_1 = LevelAdjustmentAction_Idle;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_2 = LevelAdjustmentAction_UpBasic;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_3 = LevelAdjustmentAction_DownBasic;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_4 = LevelAdjustmentAction_UpShortMovement;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_5 = LevelAdjustmentAction_DownShortMovement;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_6 = LevelAdjustmentAction_Reserved;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_7 = LevelAdjustmentAction_GotoDriveLevel;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_8 = LevelAdjustmentAction_Reserved_01;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_9 = LevelAdjustmentAction_Ferry;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_10 = LevelAdjustmentAction_Reserved_02;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_11 = LevelAdjustmentAction_Reserved_03;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_12 = LevelAdjustmentAction_Reserved_04;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_13 = LevelAdjustmentAction_Reserved_05;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_14 = LevelAdjustmentAction_Reserved_06;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_15 = LevelAdjustmentAction_Error;
  LevelAdjustmentAction_T Test_LevelAdjustmentAction_T_V_16 = LevelAdjustmentAction_NotAvailable;

  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_1 = LevelAdjustmentAxles_Rear;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_2 = LevelAdjustmentAxles_Front;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_3 = LevelAdjustmentAxles_Parallel;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_4 = LevelAdjustmentAxles_Reserved;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_5 = LevelAdjustmentAxles_Reserved_01;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_6 = LevelAdjustmentAxles_Reserved_02;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_7 = LevelAdjustmentAxles_Error;
  LevelAdjustmentAxles_T Test_LevelAdjustmentAxles_T_V_8 = LevelAdjustmentAxles_NotAvailable;

  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_1 = LevelAdjustmentStroke_DriveStroke;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_2 = LevelAdjustmentStroke_DockingStroke;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_3 = LevelAdjustmentStroke_Reserved;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_4 = LevelAdjustmentStroke_Reserved_01;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_5 = LevelAdjustmentStroke_Reserved_02;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_6 = LevelAdjustmentStroke_Reserved_03;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_7 = LevelAdjustmentStroke_Error;
  LevelAdjustmentStroke_T Test_LevelAdjustmentStroke_T_V_8 = LevelAdjustmentStroke_NotAvailable;

  LevelControlInformation_T Test_LevelControlInformation_T_V_1 = LevelControlInformation_Prohibit;
  LevelControlInformation_T Test_LevelControlInformation_T_V_2 = LevelControlInformation_HeightAdjustment;
  LevelControlInformation_T Test_LevelControlInformation_T_V_3 = LevelControlInformation_DockingHeight;
  LevelControlInformation_T Test_LevelControlInformation_T_V_4 = LevelControlInformation_DriveHeight;
  LevelControlInformation_T Test_LevelControlInformation_T_V_5 = LevelControlInformation_KneelingHeight;
  LevelControlInformation_T Test_LevelControlInformation_T_V_6 = LevelControlInformation_FerryFunction;
  LevelControlInformation_T Test_LevelControlInformation_T_V_7 = LevelControlInformation_LimpHome;
  LevelControlInformation_T Test_LevelControlInformation_T_V_8 = LevelControlInformation_Reserved;
  LevelControlInformation_T Test_LevelControlInformation_T_V_9 = LevelControlInformation_Reserved_01;
  LevelControlInformation_T Test_LevelControlInformation_T_V_10 = LevelControlInformation_Reserved_02;
  LevelControlInformation_T Test_LevelControlInformation_T_V_11 = LevelControlInformation_Reserved_03;
  LevelControlInformation_T Test_LevelControlInformation_T_V_12 = LevelControlInformation_Reserved_04;
  LevelControlInformation_T Test_LevelControlInformation_T_V_13 = LevelControlInformation_Reserved_05;
  LevelControlInformation_T Test_LevelControlInformation_T_V_14 = LevelControlInformation_Reserved_06;
  LevelControlInformation_T Test_LevelControlInformation_T_V_15 = LevelControlInformation_Error;
  LevelControlInformation_T Test_LevelControlInformation_T_V_16 = LevelControlInformation_NotAvailable;

  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_1 = LevelUserMemoryAction_Inactive;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_2 = LevelUserMemoryAction_Recall;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_3 = LevelUserMemoryAction_Store;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_4 = LevelUserMemoryAction_Default;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_5 = LevelUserMemoryAction_Reserved;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_6 = LevelUserMemoryAction_Reserved_01;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_7 = LevelUserMemoryAction_Error;
  LevelUserMemoryAction_T Test_LevelUserMemoryAction_T_V_8 = LevelUserMemoryAction_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  RampLevelRequest_T Test_RampLevelRequest_T_V_1 = RampLevelRequest_TakeNoAction;
  RampLevelRequest_T Test_RampLevelRequest_T_V_2 = RampLevelRequest_RampLevelM1;
  RampLevelRequest_T Test_RampLevelRequest_T_V_3 = RampLevelRequest_RampLevelM2;
  RampLevelRequest_T Test_RampLevelRequest_T_V_4 = RampLevelRequest_RampLevelM3;
  RampLevelRequest_T Test_RampLevelRequest_T_V_5 = RampLevelRequest_RampLevelM4;
  RampLevelRequest_T Test_RampLevelRequest_T_V_6 = RampLevelRequest_RampLevelM5;
  RampLevelRequest_T Test_RampLevelRequest_T_V_7 = RampLevelRequest_RampLevelM6;
  RampLevelRequest_T Test_RampLevelRequest_T_V_8 = RampLevelRequest_RampLevelM7;
  RampLevelRequest_T Test_RampLevelRequest_T_V_9 = RampLevelRequest_NotDefined_04;
  RampLevelRequest_T Test_RampLevelRequest_T_V_10 = RampLevelRequest_NotDefined_05;
  RampLevelRequest_T Test_RampLevelRequest_T_V_11 = RampLevelRequest_NotDefined_06;
  RampLevelRequest_T Test_RampLevelRequest_T_V_12 = RampLevelRequest_NotDefined_07;
  RampLevelRequest_T Test_RampLevelRequest_T_V_13 = RampLevelRequest_NotDefined_08;
  RampLevelRequest_T Test_RampLevelRequest_T_V_14 = RampLevelRequest_NotDefined_09;
  RampLevelRequest_T Test_RampLevelRequest_T_V_15 = RampLevelRequest_ErrorIndicator;
  RampLevelRequest_T Test_RampLevelRequest_T_V_16 = RampLevelRequest_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;

  VehicleMode_T Test_VehicleMode_T_V_1 = VehicleMode_Hibernate;
  VehicleMode_T Test_VehicleMode_T_V_2 = VehicleMode_Parked;
  VehicleMode_T Test_VehicleMode_T_V_3 = VehicleMode_Living;
  VehicleMode_T Test_VehicleMode_T_V_4 = VehicleMode_Accessory;
  VehicleMode_T Test_VehicleMode_T_V_5 = VehicleMode_PreRunning;
  VehicleMode_T Test_VehicleMode_T_V_6 = VehicleMode_Cranking;
  VehicleMode_T Test_VehicleMode_T_V_7 = VehicleMode_Running;
  VehicleMode_T Test_VehicleMode_T_V_8 = VehicleMode_Spare_1;
  VehicleMode_T Test_VehicleMode_T_V_9 = VehicleMode_Spare_2;
  VehicleMode_T Test_VehicleMode_T_V_10 = VehicleMode_Spare_3;
  VehicleMode_T Test_VehicleMode_T_V_11 = VehicleMode_Spare_4;
  VehicleMode_T Test_VehicleMode_T_V_12 = VehicleMode_Spare_5;
  VehicleMode_T Test_VehicleMode_T_V_13 = VehicleMode_Spare_6;
  VehicleMode_T Test_VehicleMode_T_V_14 = VehicleMode_Spare_7;
  VehicleMode_T Test_VehicleMode_T_V_15 = VehicleMode_Error;
  VehicleMode_T Test_VehicleMode_T_V_16 = VehicleMode_NotAvailable;

  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_1 = WiredLevelUserMemory_MemOff;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_2 = WiredLevelUserMemory_M1;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_3 = WiredLevelUserMemory_M2;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_4 = WiredLevelUserMemory_M3;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_5 = WiredLevelUserMemory_M4;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_6 = WiredLevelUserMemory_M5;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_7 = WiredLevelUserMemory_M6;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_8 = WiredLevelUserMemory_M7;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_9 = WiredLevelUserMemory_Spare;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_10 = WiredLevelUserMemory_Spare01;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_11 = WiredLevelUserMemory_Spare02;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_12 = WiredLevelUserMemory_Spare03;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_13 = WiredLevelUserMemory_Spare04;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_14 = WiredLevelUserMemory_Spare05;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_15 = WiredLevelUserMemory_Error;
  WiredLevelUserMemory_T Test_WiredLevelUserMemory_T_V_16 = WiredLevelUserMemory_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
