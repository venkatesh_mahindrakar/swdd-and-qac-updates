/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_AxleLoadDistribution_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_ALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_ECSStandByRequest_ECSStandByRequest(ECSStandByRequest_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1PositionStatus_LiftAxle1PositionStatus(LiftAxlePositionStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1Switch2_Status_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK(LiftAxleUpRequestACK_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle2SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK(LiftAxleUpRequestACK_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK(LoadDistributionALDChangeACK_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionChangeACK_LoadDistributionChangeACK(LoadDistributionChangeACK_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionFuncSelected_LoadDistributionFuncSelected(LoadDistributionFuncSelected_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionRequestedACK_LoadDistributionRequestedACK(LoadDistributionRequestedACK_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_LoadDistributionSelected_LoadDistributionSelected(LoadDistributionSelected_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio1SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio2SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio3SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio4SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio5SwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_Ratio6SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_RatioALDSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_SwcActivation_Living_Living(VehicleModeDistribution_T *data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Read_TridemALDSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_ALD_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_AltLoadDistribution_rqst_AltLoadDistribution_rqst(AltLoadDistribution_rqst_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_BogieSwitch_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest(InactiveActive_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle1DirectControl_LiftAxle1DirectControl(LiftAxleLiftPositionRequest_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest(LiftAxleLiftPositionRequest_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest(InactiveActive_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest(LiftAxleLiftPositionRequest_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LoadDistributionChangeRequest_LoadDistributionChangeRequest(LoadDistributionChangeRequest_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_LoadDistributionRequested_LoadDistributionRequested(LoadDistributionRequested_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_MaxTract_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio1_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio2_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio4_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio5_DeviceIndication_DualDeviceIndication(DualDeviceIndication_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_Ratio6_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_RatioALD_DualDeviceIndication_DualDeviceIndication(DualDeviceIndication_T data);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Write_TridemALD_DeviceIndication_DeviceIndication(DeviceIndication_T data);

/** Service interfaces */
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Call_Event_D1BOV_56_ALD_InvalidConfiguration_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_AxleLoadDistribution_HMICtrl_Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(Dem_EventStatusType EventStatus);

/** Calibration Component Calibration Parameters */
SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1KN5_AxleLoad_CRideLEDIndicationType_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOS_AxleLoad_AccessoryBoggieALD_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOV_AxleLoad_RatioALD_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOW_AxleLoad_ArideLiftAxle_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOX_AxleLoad_TridemFirstAxleLift_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ0_AxleLoad_MaxTractionTag_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ2_AxleLoad_RatioPusherRocker_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZ3_AxleLoad_RatioTagRocker_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZW_AxleLoad_OneLiftPusher_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZY_AxleLoad_OneLiftTag_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1CZZ_AxleLoad_MaxTractionPusher_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1J6B_AxleLoad_AccessoryTridemALD_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1J6C_AxleLoad_BoggieDualRatio_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1J6D_AxleLoad_TridemDualRatio_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1KN2_AxleLoad_CRideLiftAxle_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1KN4_AxleLoad_CRideLEDlowerEnd_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1M5B_AxleLoad_RatioRoadGripPusher_v(void);
boolean  TSC_AxleLoadDistribution_HMICtrl_Rte_Prm_P1BOY_AxleLoad_TridemSecondAxleLift_v(void);




