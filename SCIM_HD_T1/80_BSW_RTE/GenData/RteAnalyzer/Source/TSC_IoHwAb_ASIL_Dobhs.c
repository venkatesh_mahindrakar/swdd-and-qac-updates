/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_IoHwAb_ASIL_Dobhs.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_IoHwAb_ASIL_Dobhs.h"
#include "TSC_IoHwAb_ASIL_Dobhs.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
{
Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuFaultStatus( data);
}
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
{
Rte_IrvRead_DobhsCtrlInterface_P_1_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray( data);
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
{
Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuFaultStatus( data);
}
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
{
Rte_IrvRead_DobhsCtrlInterface_P_2_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray( data);
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
{
Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuFaultStatus( data);
}
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
{
Rte_IrvRead_DobhsCtrlInterface_P_3_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray( data);
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
{
Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuFaultStatus( data);
}
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
{
Rte_IrvRead_DobhsCtrlInterface_P_4_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray( data);
}













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
{
Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuFaultStatus( data);
}
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
{
Rte_IrvRead_DobhsDiagInterface_P_GetDobhsPinState_CS_IrvEcuHwDioCtrlArray( data);
}






Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Read_DiagActiveState_P_isDiagActive(DiagActiveState_T *data)
{
  return Rte_Read_DiagActiveState_P_isDiagActive(data);
}

Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data)
{
  return Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode(data);
}

Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Read_ScimPvtControl_P_Status(uint8 *data)
{
  return Rte_Read_ScimPvtControl_P_Status(data);
}








     /* Client Server Interfaces: */
Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Call_EcuHwState_P_GetEcuVoltages_CS(VGTT_EcuPinVoltage_0V2 *EcuVoltageValues)
{
  return Rte_Call_EcuHwState_P_GetEcuVoltages_CS(EcuVoltageValues);
}
Std_ReturnType TSC_IoHwAb_ASIL_Dobhs_Rte_Call_VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_VbatInterface_P_GetVbatVoltage_CS(BatteryVoltage, FaultStatus);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
{
Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus( data);
}
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
{
Rte_IrvRead_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray( data);
}

void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus(Rte_DT_EcuHwFaultValues_T_0 *data)
{
  Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuFaultStatus( data);
}
void TSC_IoHwAb_ASIL_Dobhs_Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray(Rte_DT_EcuHwDioCtrlArray_T_0 *data)
{
  Rte_IrvWrite_IoHwAb_ASIL_Dobhs_10ms_runnable_IrvEcuHwDioCtrlArray( data);
}




SEWS_HwToleranceThreshold_X1C04_T  TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1C04_HwToleranceThreshold_v(void)
{
  return (SEWS_HwToleranceThreshold_X1C04_T ) Rte_Prm_X1C04_HwToleranceThreshold_v();
}
SEWS_PcbConfig_DOBHS_X1CXX_a_T * TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(void)
{
  return (SEWS_PcbConfig_DOBHS_X1CXX_a_T *) Rte_Prm_X1CXX_PcbConfig_DOBHS_v();
}
SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_IoHwAb_ASIL_Dobhs_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
{
  return (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *) Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
}


     /* IoHwAb_ASIL_Dobhs */
      /* IoHwAb_ASIL_Dobhs */



