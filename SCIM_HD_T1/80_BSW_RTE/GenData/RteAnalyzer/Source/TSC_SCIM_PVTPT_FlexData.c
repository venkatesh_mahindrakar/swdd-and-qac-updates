/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SCIM_PVTPT_FlexData.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_SCIM_PVTPT_FlexData.h"
#include "TSC_SCIM_PVTPT_FlexData.h"











Std_ReturnType TSC_SCIM_PVTPT_FlexData_Rte_Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData(const uint8 *data)
{
  return Rte_Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData(data);
}

Std_ReturnType TSC_SCIM_PVTPT_FlexData_Rte_Write_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1(Debug_PVT_SCIM_FlexArrayData1 data)
{
  return Rte_Write_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1(data);
}

Std_ReturnType TSC_SCIM_PVTPT_FlexData_Rte_Write_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId(Debug_PVT_SCIM_FlexArrayDataId data)
{
  return Rte_Write_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_SCIM_PVTPT_FlexData_Rte_Call_LfInterface_P_GetLfAntState(LfRssi *LfRssiStatus, uint8 *FobFound, uint8 *FobLocation)
{
  return Rte_Call_LfInterface_P_GetLfAntState(LfRssiStatus, FobFound, FobLocation);
}
Std_ReturnType TSC_SCIM_PVTPT_FlexData_Rte_Call_RkeInterface_P_GetFobRkeState(ButtonStatus *RkeButton, uint16 *FobID, uint32 *FobSN, uint16 *FobRollingCounter, uint16 *ScimRollingCounter, uint8 *FobBattery)
{
  return Rte_Call_RkeInterface_P_GetFobRkeState(RkeButton, FobID, FobSN, FobRollingCounter, ScimRollingCounter, FobBattery);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */






Std_ReturnType TSC_SCIM_PVTPT_FlexData_Rte_Read_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest(Debug_PVT_FlexDataRequest *data)
{
  return Rte_Read_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest(data);
}

Std_ReturnType TSC_SCIM_PVTPT_FlexData_Rte_Read_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig(Debug_PVT_LF_Trig *data)
{
  return Rte_Read_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig(data);
}








     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* SCIM_PVTPT_FlexData */
      /* SCIM_PVTPT_FlexData */



