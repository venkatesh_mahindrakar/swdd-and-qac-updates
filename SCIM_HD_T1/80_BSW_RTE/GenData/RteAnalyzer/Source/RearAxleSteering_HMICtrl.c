/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  RearAxleSteering_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  RearAxleSteering_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <RearAxleSteering_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_RAS_LEDFeedbackIndication_P1GCC_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_RearAxleSteering_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_RearAxleSteering_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void RearAxleSteering_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_RAS_LEDFeedbackIndication_P1GCC_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * RearAxleSteeringFunctionDsbl_T: Enumeration of integer in interval [0...3] with enumerators
 *   RearAxleSteeringFunctionDsbl_NoDisableRequest (0U)
 *   RearAxleSteeringFunctionDsbl_DisableRequest (1U)
 *   RearAxleSteeringFunctionDsbl_Error (2U)
 *   RearAxleSteeringFunctionDsbl_NotAvailable (3U)
 * RearAxleSteeringFunctionStatus_T: Enumeration of integer in interval [0...7] with enumerators
 *   RearAxleSteeringFunctionStatus_NotActive (0U)
 *   RearAxleSteeringFunctionStatus_Active (1U)
 *   RearAxleSteeringFunctionStatus_PassiveFailureMode (2U)
 *   RearAxleSteeringFunctionStatus_ActiveFailureMode (3U)
 *   RearAxleSteeringFunctionStatus_SpareValue (4U)
 *   RearAxleSteeringFunctionStatus_SpareValue_01 (5U)
 *   RearAxleSteeringFunctionStatus_Error (6U)
 *   RearAxleSteeringFunctionStatus_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_RAS_LEDFeedbackIndication_P1GCC_T Rte_Prm_P1GCC_RAS_LEDFeedbackIndication_v(void)
 *   boolean Rte_Prm_P1GBT_RAS_SwitchInstalled_v(void)
 *   boolean Rte_Prm_P1RRH_RAS_ToggleButton_v(void)
 *
 *********************************************************************************************************************/


#define RearAxleSteering_HMICtrl_START_SEC_CODE
#include "RearAxleSteering_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RearAxleSteering_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ExtraAxleSteeringFunctionStat_RearAxleSteeringFunctionStatus(RearAxleSteeringFunctionStatus_T *data)
 *   Std_ReturnType Rte_Read_RearAxleSteeringFunctionStatus_RearAxleSteeringFunctionStatus(RearAxleSteeringFunctionStatus_T *data)
 *   Std_ReturnType Rte_Read_RearAxleSteering_DeviceEvent_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_RearAxleSteeringDeviceInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_RearAxleSteeringFunctionDsbl_RearAxleSteeringFunctionDsbl(RearAxleSteeringFunctionDsbl_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RearAxleSteering_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, RearAxleSteering_HMICtrl_CODE) RearAxleSteering_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RearAxleSteering_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  RearAxleSteeringFunctionStatus_T Read_ExtraAxleSteeringFunctionStat_RearAxleSteeringFunctionStatus;
  RearAxleSteeringFunctionStatus_T Read_RearAxleSteeringFunctionStatus_RearAxleSteeringFunctionStatus;
  A2PosSwitchStatus_T Read_RearAxleSteering_DeviceEvent_A2PosSwitchStatus;
  VehicleModeDistribution_T Read_SwcActivation_EngineRun_EngineRun;

  SEWS_RAS_LEDFeedbackIndication_P1GCC_T P1GCC_RAS_LEDFeedbackIndication_v_data;
  boolean P1GBT_RAS_SwitchInstalled_v_data;
  boolean P1RRH_RAS_ToggleButton_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1GCC_RAS_LEDFeedbackIndication_v_data = TSC_RearAxleSteering_HMICtrl_Rte_Prm_P1GCC_RAS_LEDFeedbackIndication_v();
  P1GBT_RAS_SwitchInstalled_v_data = TSC_RearAxleSteering_HMICtrl_Rte_Prm_P1GBT_RAS_SwitchInstalled_v();
  P1RRH_RAS_ToggleButton_v_data = TSC_RearAxleSteering_HMICtrl_Rte_Prm_P1RRH_RAS_ToggleButton_v();

  fct_status = TSC_RearAxleSteering_HMICtrl_Rte_Read_ExtraAxleSteeringFunctionStat_RearAxleSteeringFunctionStatus(&Read_ExtraAxleSteeringFunctionStat_RearAxleSteeringFunctionStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RearAxleSteering_HMICtrl_Rte_Read_RearAxleSteeringFunctionStatus_RearAxleSteeringFunctionStatus(&Read_RearAxleSteeringFunctionStatus_RearAxleSteeringFunctionStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RearAxleSteering_HMICtrl_Rte_Read_RearAxleSteering_DeviceEvent_A2PosSwitchStatus(&Read_RearAxleSteering_DeviceEvent_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RearAxleSteering_HMICtrl_Rte_Read_SwcActivation_EngineRun_EngineRun(&Read_SwcActivation_EngineRun_EngineRun);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_RearAxleSteering_HMICtrl_Rte_Write_RearAxleSteeringDeviceInd_DeviceIndication(Rte_InitValue_RearAxleSteeringDeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_RearAxleSteering_HMICtrl_Rte_Write_RearAxleSteeringFunctionDsbl_RearAxleSteeringFunctionDsbl(Rte_InitValue_RearAxleSteeringFunctionDsbl_RearAxleSteeringFunctionDsbl);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  RearAxleSteering_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define RearAxleSteering_HMICtrl_STOP_SEC_CODE
#include "RearAxleSteering_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void RearAxleSteering_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  RearAxleSteeringFunctionDsbl_T Test_RearAxleSteeringFunctionDsbl_T_V_1 = RearAxleSteeringFunctionDsbl_NoDisableRequest;
  RearAxleSteeringFunctionDsbl_T Test_RearAxleSteeringFunctionDsbl_T_V_2 = RearAxleSteeringFunctionDsbl_DisableRequest;
  RearAxleSteeringFunctionDsbl_T Test_RearAxleSteeringFunctionDsbl_T_V_3 = RearAxleSteeringFunctionDsbl_Error;
  RearAxleSteeringFunctionDsbl_T Test_RearAxleSteeringFunctionDsbl_T_V_4 = RearAxleSteeringFunctionDsbl_NotAvailable;

  RearAxleSteeringFunctionStatus_T Test_RearAxleSteeringFunctionStatus_T_V_1 = RearAxleSteeringFunctionStatus_NotActive;
  RearAxleSteeringFunctionStatus_T Test_RearAxleSteeringFunctionStatus_T_V_2 = RearAxleSteeringFunctionStatus_Active;
  RearAxleSteeringFunctionStatus_T Test_RearAxleSteeringFunctionStatus_T_V_3 = RearAxleSteeringFunctionStatus_PassiveFailureMode;
  RearAxleSteeringFunctionStatus_T Test_RearAxleSteeringFunctionStatus_T_V_4 = RearAxleSteeringFunctionStatus_ActiveFailureMode;
  RearAxleSteeringFunctionStatus_T Test_RearAxleSteeringFunctionStatus_T_V_5 = RearAxleSteeringFunctionStatus_SpareValue;
  RearAxleSteeringFunctionStatus_T Test_RearAxleSteeringFunctionStatus_T_V_6 = RearAxleSteeringFunctionStatus_SpareValue_01;
  RearAxleSteeringFunctionStatus_T Test_RearAxleSteeringFunctionStatus_T_V_7 = RearAxleSteeringFunctionStatus_Error;
  RearAxleSteeringFunctionStatus_T Test_RearAxleSteeringFunctionStatus_T_V_8 = RearAxleSteeringFunctionStatus_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
