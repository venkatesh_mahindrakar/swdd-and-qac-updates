/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  BswM.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  BswM
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <BswM>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * BswM_BswMRteMDG_CanBusOff
 *   
 *
 * BswM_BswMRteMDG_LIN1Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN2Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN3Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN4Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN5Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN6Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN7Schedule
 *   
 *
 * BswM_BswMRteMDG_LIN8Schedule
 *   
 *
 * BswM_BswMRteMDG_LINSchTableState
 *   
 *
 * BswM_BswMRteMDG_NvmWriteAllRequest
 *   
 *
 * BswM_BswMRteMDG_PvtReport_X1C14
 *   
 *
 * BswM_BswMRteMDG_ResetProcess
 *   
 *
 * BswM_ESH_Mode
 *   
 *
 * Dcm_EcuResetType
 *   
 *
 *********************************************************************************************************************/

#include "Rte_BswM.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_BswM.h"
#include "SchM_BswM.h"
#include "TSC_SchM_BswM.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void BswM_TestDefines(void);

typedef P2FUNC(Std_ReturnType, RTE_CODE, FncPtrType)(void); /* PRQA S 3448 */ /* MD_Rte_TestCode */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * BswM_BswMRteMDG_CanBusOff: Enumeration of integer in interval [0...255] with enumerators
 *   CAN_NormalCom (0U)
 *   CAN_BusOff (1U)
 * BswM_BswMRteMDG_LIN1Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN1_Table1 (1U)
 *   LIN1_Table2 (2U)
 *   LIN1_Table_E (3U)
 *   LIN1_MasterReq_SlaveResp_Table1 (4U)
 *   LIN1_MasterReq_SlaveResp_Table2 (5U)
 *   LIN1_NULL (0U)
 *   LIN1_MasterReq_SlaveResp (6U)
 * BswM_BswMRteMDG_LIN2Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN2_NULL (0U)
 *   LIN2_TABLE0 (1U)
 *   LIN2_TABLE_E (2U)
 *   LIN2_MasterReq_SlaveResp_TABLE0 (3U)
 *   LIN2_MasterReq_SlaveResp (4U)
 * BswM_BswMRteMDG_LIN3Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN3_NULL (0U)
 *   LIN3_TABLE1 (1U)
 *   LIN3_TABLE2 (2U)
 *   LIN3_TABLE_E (3U)
 *   LIN3_MasterReq_SlaveResp_Table1 (4U)
 *   LIN3_MasterReq_SlaveResp_Table2 (5U)
 *   LIN3_MasterReq_SlaveResp (6U)
 * BswM_BswMRteMDG_LIN4Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN4_NULL (0U)
 *   LIN4_MasterReq_SlaveResp_Table1 (4U)
 *   LIN4_TABLE1 (1U)
 *   LIN4_TABLE2 (2U)
 *   LIN4_TABLE_E (3U)
 *   LIN4_MasterReq_SlaveResp_Table2 (5U)
 *   LIN4_MasterReq_SlaveResp (6U)
 * BswM_BswMRteMDG_LIN5Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN5_NULL (0U)
 *   LIN5_TABLE1 (1U)
 *   LIN5_MasterReq_SlaveResp_Table1 (4U)
 *   LIN5_MasterReq_SlaveResp_Table2 (5U)
 *   LIN5_MasterReq_SlaveResp (6U)
 *   LIN5_TABLE2 (2U)
 *   LIN5_TABLE_E (3U)
 * BswM_BswMRteMDG_LIN6Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN6_NULL (0U)
 *   LIN6_TABLE0 (1U)
 *   LIN6_MasterReq_SlaveResp_Table0 (3U)
 *   LIN6_MasterReq_SlaveResp (2U)
 * BswM_BswMRteMDG_LIN7Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN7_NULL (0U)
 *   LIN7_TABLE0 (1U)
 *   LIN7_MasterReq_SlaveResp_Table0 (3U)
 *   LIN7_MasterReq_SlaveResp (2U)
 * BswM_BswMRteMDG_LIN8Schedule: Enumeration of integer in interval [0...255] with enumerators
 *   LIN8_NULL (0U)
 *   LIN8_TABLE0 (1U)
 *   LIN8_MasterReq_SlaveResp_Table0 (3U)
 *   LIN8_MasterReq_SlaveResp (2U)
 * BswM_BswMRteMDG_LINSchTableState: Enumeration of integer in interval [0...255] with enumerators
 *   LIN_SchTable_INIT (0U)
 *   LIN_SchTable_EndOfNotification (1U)
 *   LIN_SchTable_StartOfIndication (2U)
 * BswM_BswMRteMDG_NvmWriteAllRequest: Enumeration of integer in interval [0...255] with enumerators
 *   NvmWriteAll_NoRequest (0U)
 *   NvmWriteAll_Request (1U)
 * BswM_BswMRteMDG_PvtReport_X1C14: Enumeration of integer in interval [0...255] with enumerators
 *   PvtReport_Enabled (0U)
 *   PvtReport_Disabled (1U)
 * BswM_BswMRteMDG_ResetProcess: Enumeration of integer in interval [0...255] with enumerators
 *   ResetProcess_Inprogress (2U)
 *   ResetProcess_Completed (3U)
 *   ResetProcess_Started (1U)
 *   ResetProcess_Idle (0U)
 * BswM_ESH_Mode: Enumeration of integer in interval [0...255] with enumerators
 *   STARTUP (0U)
 *   RUN (1U)
 *   POSTRUN (2U)
 *   WAKEUP (3U)
 *   SHUTDOWN (4U)
 * Dcm_EcuResetType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_ENUM_NONE (0U)
 *   DCM_ENUM_HARD (1U)
 *   DCM_ENUM_KEYONOFF (2U)
 *   DCM_ENUM_SOFT (3U)
 *   DCM_ENUM_JUMPTOBOOTLOADER (4U)
 *   DCM_ENUM_JUMPTOSYSSUPPLIERBOOTLOADER (5U)
 *   DCM_ENUM_EXECUTE (6U)
 *
 *********************************************************************************************************************/


#define BswM_START_SEC_CODE
#include "BswM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BswM_MainFunction
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode(BswM_BswMRteMDG_NvmWriteAllRequest *data)
 *   Std_ReturnType Rte_Read_Request_SwcModeRequest_PvtReportCtrl_requestedMode(BswM_BswMRteMDG_PvtReport_X1C14 *data)
 *
 * Mode Interfaces:
 * ================
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(BswM_BswMRteMDG_LIN1Schedule mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN1Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2
 *   - RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN1Schedule
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LINSchTableState:
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
 *   - RTE_TRANSITION_BswMRteMDG_LINSchTableState
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(BswM_BswMRteMDG_LIN2Schedule mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN2Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0
 *   - RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN2Schedule
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LINSchTableState:
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
 *   - RTE_TRANSITION_BswMRteMDG_LINSchTableState
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(BswM_BswMRteMDG_LIN3Schedule mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN3Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN3Schedule
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LINSchTableState:
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
 *   - RTE_TRANSITION_BswMRteMDG_LINSchTableState
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(BswM_BswMRteMDG_LIN4Schedule mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN4Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN4Schedule
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LINSchTableState:
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
 *   - RTE_TRANSITION_BswMRteMDG_LINSchTableState
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(BswM_BswMRteMDG_LIN5Schedule mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN5Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2
 *   - RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E
 *   - RTE_TRANSITION_BswMRteMDG_LIN5Schedule
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LINSchTableState:
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
 *   - RTE_TRANSITION_BswMRteMDG_LINSchTableState
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(BswM_BswMRteMDG_LIN6Schedule mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN6Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL
 *   - RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0
 *   - RTE_TRANSITION_BswMRteMDG_LIN6Schedule
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LINSchTableState:
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
 *   - RTE_TRANSITION_BswMRteMDG_LINSchTableState
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(BswM_BswMRteMDG_LIN7Schedule mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN7Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL
 *   - RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0
 *   - RTE_TRANSITION_BswMRteMDG_LIN7Schedule
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LINSchTableState:
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
 *   - RTE_TRANSITION_BswMRteMDG_LINSchTableState
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(BswM_BswMRteMDG_LIN8Schedule mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LIN8Schedule:
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL
 *   - RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0
 *   - RTE_TRANSITION_BswMRteMDG_LIN8Schedule
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_BswMRteMDG_LINSchTableState mode)
 *   Modes of Rte_ModeType_BswMRteMDG_LINSchTableState:
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
 *   - RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
 *   - RTE_TRANSITION_BswMRteMDG_LINSchTableState
 *   Std_ReturnType Rte_Switch_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_BswMRteMDG_CanBusOff mode)
 *   Modes of Rte_ModeType_BswMRteMDG_CanBusOff:
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
 *   - RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
 *   - RTE_TRANSITION_BswMRteMDG_CanBusOff
 *   Std_ReturnType Rte_Switch_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode(BswM_ESH_Mode mode)
 *   Modes of Rte_ModeType_ESH_Mode:
 *   - RTE_MODE_ESH_Mode_POSTRUN
 *   - RTE_MODE_ESH_Mode_RUN
 *   - RTE_MODE_ESH_Mode_SHUTDOWN
 *   - RTE_MODE_ESH_Mode_STARTUP
 *   - RTE_MODE_ESH_Mode_WAKEUP
 *   - RTE_TRANSITION_ESH_Mode
 *   BswM_ESH_Mode Rte_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode(void)
 *   Modes of Rte_ModeType_ESH_Mode:
 *   - RTE_MODE_ESH_Mode_POSTRUN
 *   - RTE_MODE_ESH_Mode_RUN
 *   - RTE_MODE_ESH_Mode_SHUTDOWN
 *   - RTE_MODE_ESH_Mode_STARTUP
 *   - RTE_MODE_ESH_Mode_WAKEUP
 *   - RTE_TRANSITION_ESH_Mode
 *   Dcm_EcuResetType Rte_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset(void)
 *   Modes of Rte_ModeType_DcmEcuReset:
 *   - RTE_MODE_DcmEcuReset_EXECUTE
 *   - RTE_MODE_DcmEcuReset_HARD
 *   - RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER
 *   - RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER
 *   - RTE_MODE_DcmEcuReset_KEYONOFF
 *   - RTE_MODE_DcmEcuReset_NONE
 *   - RTE_MODE_DcmEcuReset_SOFT
 *   - RTE_TRANSITION_DcmEcuReset
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_MainFunction_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BswM_CODE) BswM_MainFunction(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_MainFunction
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  BswM_BswMRteMDG_NvmWriteAllRequest Read_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode;
  BswM_BswMRteMDG_PvtReport_X1C14 Read_Request_SwcModeRequest_PvtReportCtrl_requestedMode;

  Rte_ModeType_ESH_Mode Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode;
  Rte_ModeType_DcmEcuReset Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode(&Read_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_SwcModeRequest_PvtReportCtrl_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_SwcModeRequest_PvtReportCtrl_requestedMode(&Read_Request_SwcModeRequest_PvtReportCtrl_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Switch_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Switch_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode(RTE_MODE_ESH_Mode_STARTUP);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_LIMIT:
      fct_error = 1;
      break;
  }

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode = TSC_BswM_Rte_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode();
  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset = TSC_BswM_Rte_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset();

  TSC_BswM_SchM_Enter_BswM_BSWM_EXCLUSIVE_AREA_0();
  TSC_BswM_SchM_Exit_BswM_BSWM_EXCLUSIVE_AREA_0();

  BswM_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BswM_Read_LIN1_ScheduleTableRequestMode
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <requestedMode> of PortPrototype <Request_LIN1_ScheduleTableRequestMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Request_LIN1_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN1Schedule *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN1_ScheduleTableRequestMode_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BswM_CODE) BswM_Read_LIN1_ScheduleTableRequestMode(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN1_ScheduleTableRequestMode
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  BswM_BswMRteMDG_LIN1Schedule Read_Request_LIN1_ScheduleTableRequestMode_requestedMode;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_LIN1_ScheduleTableRequestMode_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_LIN1_ScheduleTableRequestMode_requestedMode(&Read_Request_LIN1_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BswM_Read_LIN2_ScheduleTableRequestMode
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <requestedMode> of PortPrototype <Request_LIN2_ScheduleTableRequestMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Request_LIN2_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN2Schedule *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN2_ScheduleTableRequestMode_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BswM_CODE) BswM_Read_LIN2_ScheduleTableRequestMode(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN2_ScheduleTableRequestMode
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  BswM_BswMRteMDG_LIN2Schedule Read_Request_LIN2_ScheduleTableRequestMode_requestedMode;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_LIN2_ScheduleTableRequestMode_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_LIN2_ScheduleTableRequestMode_requestedMode(&Read_Request_LIN2_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BswM_Read_LIN3_ScheduleTableRequestMode
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <requestedMode> of PortPrototype <Request_LIN3_ScheduleTableRequestMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Request_LIN3_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN3Schedule *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN3_ScheduleTableRequestMode_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BswM_CODE) BswM_Read_LIN3_ScheduleTableRequestMode(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN3_ScheduleTableRequestMode
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  BswM_BswMRteMDG_LIN3Schedule Read_Request_LIN3_ScheduleTableRequestMode_requestedMode;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_LIN3_ScheduleTableRequestMode_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_LIN3_ScheduleTableRequestMode_requestedMode(&Read_Request_LIN3_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BswM_Read_LIN4_ScheduleTableRequestMode
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <requestedMode> of PortPrototype <Request_LIN4_ScheduleTableRequestMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Request_LIN4_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN4Schedule *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN4_ScheduleTableRequestMode_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BswM_CODE) BswM_Read_LIN4_ScheduleTableRequestMode(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN4_ScheduleTableRequestMode
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  BswM_BswMRteMDG_LIN4Schedule Read_Request_LIN4_ScheduleTableRequestMode_requestedMode;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_LIN4_ScheduleTableRequestMode_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_LIN4_ScheduleTableRequestMode_requestedMode(&Read_Request_LIN4_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BswM_Read_LIN5_ScheduleTableRequestMode
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <requestedMode> of PortPrototype <Request_LIN5_ScheduleTableRequestMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Request_LIN5_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN5Schedule *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN5_ScheduleTableRequestMode_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BswM_CODE) BswM_Read_LIN5_ScheduleTableRequestMode(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN5_ScheduleTableRequestMode
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  BswM_BswMRteMDG_LIN5Schedule Read_Request_LIN5_ScheduleTableRequestMode_requestedMode;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_LIN5_ScheduleTableRequestMode_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_LIN5_ScheduleTableRequestMode_requestedMode(&Read_Request_LIN5_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BswM_Read_LIN6_ScheduleTableRequestMode
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <requestedMode> of PortPrototype <Request_LIN6_ScheduleTableRequestMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Request_LIN6_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN6Schedule *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN6_ScheduleTableRequestMode_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BswM_CODE) BswM_Read_LIN6_ScheduleTableRequestMode(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN6_ScheduleTableRequestMode
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  BswM_BswMRteMDG_LIN6Schedule Read_Request_LIN6_ScheduleTableRequestMode_requestedMode;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_LIN6_ScheduleTableRequestMode_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_LIN6_ScheduleTableRequestMode_requestedMode(&Read_Request_LIN6_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BswM_Read_LIN7_ScheduleTableRequestMode
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <requestedMode> of PortPrototype <Request_LIN7_ScheduleTableRequestMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Request_LIN7_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN7Schedule *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN7_ScheduleTableRequestMode_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BswM_CODE) BswM_Read_LIN7_ScheduleTableRequestMode(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN7_ScheduleTableRequestMode
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  BswM_BswMRteMDG_LIN7Schedule Read_Request_LIN7_ScheduleTableRequestMode_requestedMode;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_LIN7_ScheduleTableRequestMode_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_LIN7_ScheduleTableRequestMode_requestedMode(&Read_Request_LIN7_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: BswM_Read_LIN8_ScheduleTableRequestMode
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on DataReceivedEvent for DataElementPrototype <requestedMode> of PortPrototype <Request_LIN8_ScheduleTableRequestMode>
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Request_LIN8_ScheduleTableRequestMode_requestedMode(BswM_BswMRteMDG_LIN8Schedule *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN8_ScheduleTableRequestMode_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, BswM_CODE) BswM_Read_LIN8_ScheduleTableRequestMode(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: BswM_Read_LIN8_ScheduleTableRequestMode
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  BswM_BswMRteMDG_LIN8Schedule Read_Request_LIN8_ScheduleTableRequestMode_requestedMode;

  /**********************************************************
  * Direct Function Accesses and Take Addresses of Functions
  **********************************************************/

  {
    FncPtrType BswM_FctPtr; /* PRQA S 3408 */ /* MD_Rte_TestCode */
    BswM_FctPtr = (FncPtrType)Rte_Read_Request_LIN8_ScheduleTableRequestMode_requestedMode; /* PRQA S 0313 */ /* MD_Rte_TestCode */
  }
  fct_status = TSC_BswM_Rte_Read_Request_LIN8_ScheduleTableRequestMode_requestedMode(&Read_Request_LIN8_ScheduleTableRequestMode_requestedMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define BswM_STOP_SEC_CODE
#include "BswM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void BswM_TestDefines(void)
{
  /* Enumeration Data Types */

  BswM_BswMRteMDG_CanBusOff Test_BswM_BswMRteMDG_CanBusOff_V_1 = CAN_NormalCom;
  BswM_BswMRteMDG_CanBusOff Test_BswM_BswMRteMDG_CanBusOff_V_2 = CAN_BusOff;

  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_1 = LIN1_Table1;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_2 = LIN1_Table2;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_3 = LIN1_Table_E;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_4 = LIN1_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_5 = LIN1_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_6 = LIN1_NULL;
  BswM_BswMRteMDG_LIN1Schedule Test_BswM_BswMRteMDG_LIN1Schedule_V_7 = LIN1_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_1 = LIN2_NULL;
  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_2 = LIN2_TABLE0;
  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_3 = LIN2_TABLE_E;
  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_4 = LIN2_MasterReq_SlaveResp_TABLE0;
  BswM_BswMRteMDG_LIN2Schedule Test_BswM_BswMRteMDG_LIN2Schedule_V_5 = LIN2_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_1 = LIN3_NULL;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_2 = LIN3_TABLE1;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_3 = LIN3_TABLE2;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_4 = LIN3_TABLE_E;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_5 = LIN3_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_6 = LIN3_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN3Schedule Test_BswM_BswMRteMDG_LIN3Schedule_V_7 = LIN3_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_1 = LIN4_NULL;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_2 = LIN4_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_3 = LIN4_TABLE1;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_4 = LIN4_TABLE2;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_5 = LIN4_TABLE_E;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_6 = LIN4_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN4Schedule Test_BswM_BswMRteMDG_LIN4Schedule_V_7 = LIN4_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_1 = LIN5_NULL;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_2 = LIN5_TABLE1;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_3 = LIN5_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_4 = LIN5_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_5 = LIN5_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_6 = LIN5_TABLE2;
  BswM_BswMRteMDG_LIN5Schedule Test_BswM_BswMRteMDG_LIN5Schedule_V_7 = LIN5_TABLE_E;

  BswM_BswMRteMDG_LIN6Schedule Test_BswM_BswMRteMDG_LIN6Schedule_V_1 = LIN6_NULL;
  BswM_BswMRteMDG_LIN6Schedule Test_BswM_BswMRteMDG_LIN6Schedule_V_2 = LIN6_TABLE0;
  BswM_BswMRteMDG_LIN6Schedule Test_BswM_BswMRteMDG_LIN6Schedule_V_3 = LIN6_MasterReq_SlaveResp_Table0;
  BswM_BswMRteMDG_LIN6Schedule Test_BswM_BswMRteMDG_LIN6Schedule_V_4 = LIN6_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN7Schedule Test_BswM_BswMRteMDG_LIN7Schedule_V_1 = LIN7_NULL;
  BswM_BswMRteMDG_LIN7Schedule Test_BswM_BswMRteMDG_LIN7Schedule_V_2 = LIN7_TABLE0;
  BswM_BswMRteMDG_LIN7Schedule Test_BswM_BswMRteMDG_LIN7Schedule_V_3 = LIN7_MasterReq_SlaveResp_Table0;
  BswM_BswMRteMDG_LIN7Schedule Test_BswM_BswMRteMDG_LIN7Schedule_V_4 = LIN7_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LIN8Schedule Test_BswM_BswMRteMDG_LIN8Schedule_V_1 = LIN8_NULL;
  BswM_BswMRteMDG_LIN8Schedule Test_BswM_BswMRteMDG_LIN8Schedule_V_2 = LIN8_TABLE0;
  BswM_BswMRteMDG_LIN8Schedule Test_BswM_BswMRteMDG_LIN8Schedule_V_3 = LIN8_MasterReq_SlaveResp_Table0;
  BswM_BswMRteMDG_LIN8Schedule Test_BswM_BswMRteMDG_LIN8Schedule_V_4 = LIN8_MasterReq_SlaveResp;

  BswM_BswMRteMDG_LINSchTableState Test_BswM_BswMRteMDG_LINSchTableState_V_1 = LIN_SchTable_INIT;
  BswM_BswMRteMDG_LINSchTableState Test_BswM_BswMRteMDG_LINSchTableState_V_2 = LIN_SchTable_EndOfNotification;
  BswM_BswMRteMDG_LINSchTableState Test_BswM_BswMRteMDG_LINSchTableState_V_3 = LIN_SchTable_StartOfIndication;

  BswM_BswMRteMDG_NvmWriteAllRequest Test_BswM_BswMRteMDG_NvmWriteAllRequest_V_1 = NvmWriteAll_NoRequest;
  BswM_BswMRteMDG_NvmWriteAllRequest Test_BswM_BswMRteMDG_NvmWriteAllRequest_V_2 = NvmWriteAll_Request;

  BswM_BswMRteMDG_PvtReport_X1C14 Test_BswM_BswMRteMDG_PvtReport_X1C14_V_1 = PvtReport_Enabled;
  BswM_BswMRteMDG_PvtReport_X1C14 Test_BswM_BswMRteMDG_PvtReport_X1C14_V_2 = PvtReport_Disabled;

  BswM_BswMRteMDG_ResetProcess Test_BswM_BswMRteMDG_ResetProcess_V_1 = ResetProcess_Inprogress;
  BswM_BswMRteMDG_ResetProcess Test_BswM_BswMRteMDG_ResetProcess_V_2 = ResetProcess_Completed;
  BswM_BswMRteMDG_ResetProcess Test_BswM_BswMRteMDG_ResetProcess_V_3 = ResetProcess_Started;
  BswM_BswMRteMDG_ResetProcess Test_BswM_BswMRteMDG_ResetProcess_V_4 = ResetProcess_Idle;

  BswM_ESH_Mode Test_BswM_ESH_Mode_V_1 = STARTUP;
  BswM_ESH_Mode Test_BswM_ESH_Mode_V_2 = RUN;
  BswM_ESH_Mode Test_BswM_ESH_Mode_V_3 = POSTRUN;
  BswM_ESH_Mode Test_BswM_ESH_Mode_V_4 = WAKEUP;
  BswM_ESH_Mode Test_BswM_ESH_Mode_V_5 = SHUTDOWN;

  Dcm_EcuResetType Test_Dcm_EcuResetType_V_1 = DCM_ENUM_NONE;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_2 = DCM_ENUM_HARD;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_3 = DCM_ENUM_KEYONOFF;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_4 = DCM_ENUM_SOFT;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_5 = DCM_ENUM_JUMPTOBOOTLOADER;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_6 = DCM_ENUM_JUMPTOSYSSUPPLIERBOOTLOADER;
  Dcm_EcuResetType Test_Dcm_EcuResetType_V_7 = DCM_ENUM_EXECUTE;

  /* Modes */

  BswM_BswMRteMDG_CanBusOff Test_BswMRteMDG_CanBusOff_MV_1 = RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff;
  BswM_BswMRteMDG_CanBusOff Test_BswMRteMDG_CanBusOff_MV_2 = RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom;
  BswM_BswMRteMDG_CanBusOff Test_BswMRteMDG_CanBusOff_TV = RTE_TRANSITION_BswMRteMDG_CanBusOff;

  BswM_BswMRteMDG_LIN1Schedule Test_BswMRteMDG_LIN1Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN1Schedule Test_BswMRteMDG_LIN1Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN1Schedule Test_BswMRteMDG_LIN1Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN1Schedule Test_BswMRteMDG_LIN1Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL;
  BswM_BswMRteMDG_LIN1Schedule Test_BswMRteMDG_LIN1Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1;
  BswM_BswMRteMDG_LIN1Schedule Test_BswMRteMDG_LIN1Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2;
  BswM_BswMRteMDG_LIN1Schedule Test_BswMRteMDG_LIN1Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E;
  BswM_BswMRteMDG_LIN1Schedule Test_BswMRteMDG_LIN1Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN1Schedule;

  BswM_BswMRteMDG_LIN2Schedule Test_BswMRteMDG_LIN2Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN2Schedule Test_BswMRteMDG_LIN2Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0;
  BswM_BswMRteMDG_LIN2Schedule Test_BswMRteMDG_LIN2Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL;
  BswM_BswMRteMDG_LIN2Schedule Test_BswMRteMDG_LIN2Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0;
  BswM_BswMRteMDG_LIN2Schedule Test_BswMRteMDG_LIN2Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E;
  BswM_BswMRteMDG_LIN2Schedule Test_BswMRteMDG_LIN2Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN2Schedule;

  BswM_BswMRteMDG_LIN3Schedule Test_BswMRteMDG_LIN3Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN3Schedule Test_BswMRteMDG_LIN3Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN3Schedule Test_BswMRteMDG_LIN3Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN3Schedule Test_BswMRteMDG_LIN3Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL;
  BswM_BswMRteMDG_LIN3Schedule Test_BswMRteMDG_LIN3Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1;
  BswM_BswMRteMDG_LIN3Schedule Test_BswMRteMDG_LIN3Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2;
  BswM_BswMRteMDG_LIN3Schedule Test_BswMRteMDG_LIN3Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E;
  BswM_BswMRteMDG_LIN3Schedule Test_BswMRteMDG_LIN3Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN3Schedule;

  BswM_BswMRteMDG_LIN4Schedule Test_BswMRteMDG_LIN4Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN4Schedule Test_BswMRteMDG_LIN4Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN4Schedule Test_BswMRteMDG_LIN4Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN4Schedule Test_BswMRteMDG_LIN4Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL;
  BswM_BswMRteMDG_LIN4Schedule Test_BswMRteMDG_LIN4Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1;
  BswM_BswMRteMDG_LIN4Schedule Test_BswMRteMDG_LIN4Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2;
  BswM_BswMRteMDG_LIN4Schedule Test_BswMRteMDG_LIN4Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E;
  BswM_BswMRteMDG_LIN4Schedule Test_BswMRteMDG_LIN4Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN4Schedule;

  BswM_BswMRteMDG_LIN5Schedule Test_BswMRteMDG_LIN5Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN5Schedule Test_BswMRteMDG_LIN5Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1;
  BswM_BswMRteMDG_LIN5Schedule Test_BswMRteMDG_LIN5Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2;
  BswM_BswMRteMDG_LIN5Schedule Test_BswMRteMDG_LIN5Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL;
  BswM_BswMRteMDG_LIN5Schedule Test_BswMRteMDG_LIN5Schedule_MV_5 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1;
  BswM_BswMRteMDG_LIN5Schedule Test_BswMRteMDG_LIN5Schedule_MV_6 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2;
  BswM_BswMRteMDG_LIN5Schedule Test_BswMRteMDG_LIN5Schedule_MV_7 = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E;
  BswM_BswMRteMDG_LIN5Schedule Test_BswMRteMDG_LIN5Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN5Schedule;

  BswM_BswMRteMDG_LIN6Schedule Test_BswMRteMDG_LIN6Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN6Schedule Test_BswMRteMDG_LIN6Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0;
  BswM_BswMRteMDG_LIN6Schedule Test_BswMRteMDG_LIN6Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_NULL;
  BswM_BswMRteMDG_LIN6Schedule Test_BswMRteMDG_LIN6Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0;
  BswM_BswMRteMDG_LIN6Schedule Test_BswMRteMDG_LIN6Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN6Schedule;

  BswM_BswMRteMDG_LIN7Schedule Test_BswMRteMDG_LIN7Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN7Schedule Test_BswMRteMDG_LIN7Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0;
  BswM_BswMRteMDG_LIN7Schedule Test_BswMRteMDG_LIN7Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_NULL;
  BswM_BswMRteMDG_LIN7Schedule Test_BswMRteMDG_LIN7Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0;
  BswM_BswMRteMDG_LIN7Schedule Test_BswMRteMDG_LIN7Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN7Schedule;

  BswM_BswMRteMDG_LIN8Schedule Test_BswMRteMDG_LIN8Schedule_MV_1 = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp;
  BswM_BswMRteMDG_LIN8Schedule Test_BswMRteMDG_LIN8Schedule_MV_2 = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0;
  BswM_BswMRteMDG_LIN8Schedule Test_BswMRteMDG_LIN8Schedule_MV_3 = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_NULL;
  BswM_BswMRteMDG_LIN8Schedule Test_BswMRteMDG_LIN8Schedule_MV_4 = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0;
  BswM_BswMRteMDG_LIN8Schedule Test_BswMRteMDG_LIN8Schedule_TV = RTE_TRANSITION_BswMRteMDG_LIN8Schedule;

  BswM_BswMRteMDG_LINSchTableState Test_BswMRteMDG_LINSchTableState_MV_1 = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  BswM_BswMRteMDG_LINSchTableState Test_BswMRteMDG_LINSchTableState_MV_2 = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT;
  BswM_BswMRteMDG_LINSchTableState Test_BswMRteMDG_LINSchTableState_MV_3 = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_BswMRteMDG_LINSchTableState Test_BswMRteMDG_LINSchTableState_TV = RTE_TRANSITION_BswMRteMDG_LINSchTableState;

  BswM_BswMRteMDG_NvmWriteAllRequest Test_BswMRteMDG_NvmWriteAllRequest_MV_1 = RTE_MODE_BswMRteMDG_NvmWriteAllRequest_NvmWriteAll_NoRequest;
  BswM_BswMRteMDG_NvmWriteAllRequest Test_BswMRteMDG_NvmWriteAllRequest_MV_2 = RTE_MODE_BswMRteMDG_NvmWriteAllRequest_NvmWriteAll_Request;
  BswM_BswMRteMDG_NvmWriteAllRequest Test_BswMRteMDG_NvmWriteAllRequest_TV = RTE_TRANSITION_BswMRteMDG_NvmWriteAllRequest;

  BswM_BswMRteMDG_PvtReport_X1C14 Test_BswMRteMDG_PvtReport_X1C14_MV_1 = RTE_MODE_BswMRteMDG_PvtReport_X1C14_PvtReport_Disabled;
  BswM_BswMRteMDG_PvtReport_X1C14 Test_BswMRteMDG_PvtReport_X1C14_MV_2 = RTE_MODE_BswMRteMDG_PvtReport_X1C14_PvtReport_Enabled;
  BswM_BswMRteMDG_PvtReport_X1C14 Test_BswMRteMDG_PvtReport_X1C14_TV = RTE_TRANSITION_BswMRteMDG_PvtReport_X1C14;

  BswM_BswMRteMDG_ResetProcess Test_BswMRteMDG_ResetProcess_MV_1 = RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Completed;
  BswM_BswMRteMDG_ResetProcess Test_BswMRteMDG_ResetProcess_MV_2 = RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Idle;
  BswM_BswMRteMDG_ResetProcess Test_BswMRteMDG_ResetProcess_MV_3 = RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Inprogress;
  BswM_BswMRteMDG_ResetProcess Test_BswMRteMDG_ResetProcess_MV_4 = RTE_MODE_BswMRteMDG_ResetProcess_ResetProcess_Started;
  BswM_BswMRteMDG_ResetProcess Test_BswMRteMDG_ResetProcess_TV = RTE_TRANSITION_BswMRteMDG_ResetProcess;

  Dcm_EcuResetType Test_DcmEcuReset_MV_1 = RTE_MODE_DcmEcuReset_NONE;
  Dcm_EcuResetType Test_DcmEcuReset_MV_2 = RTE_MODE_DcmEcuReset_HARD;
  Dcm_EcuResetType Test_DcmEcuReset_MV_3 = RTE_MODE_DcmEcuReset_KEYONOFF;
  Dcm_EcuResetType Test_DcmEcuReset_MV_4 = RTE_MODE_DcmEcuReset_SOFT;
  Dcm_EcuResetType Test_DcmEcuReset_MV_5 = RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER;
  Dcm_EcuResetType Test_DcmEcuReset_MV_6 = RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER;
  Dcm_EcuResetType Test_DcmEcuReset_MV_7 = RTE_MODE_DcmEcuReset_EXECUTE;
  Dcm_EcuResetType Test_DcmEcuReset_TV = RTE_TRANSITION_DcmEcuReset;

  BswM_ESH_Mode Test_ESH_Mode_MV_1 = RTE_MODE_ESH_Mode_POSTRUN;
  BswM_ESH_Mode Test_ESH_Mode_MV_2 = RTE_MODE_ESH_Mode_RUN;
  BswM_ESH_Mode Test_ESH_Mode_MV_3 = RTE_MODE_ESH_Mode_SHUTDOWN;
  BswM_ESH_Mode Test_ESH_Mode_MV_4 = RTE_MODE_ESH_Mode_STARTUP;
  BswM_ESH_Mode Test_ESH_Mode_MV_5 = RTE_MODE_ESH_Mode_WAKEUP;
  BswM_ESH_Mode Test_ESH_Mode_TV = RTE_TRANSITION_ESH_Mode;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
