/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  MovingUnitTraction_UICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  MovingUnitTraction_UICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <MovingUnitTraction_UICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dem_EventStatusType
 *   
 *
 * SEWS_AxleConfiguration_P1B16_T
 *   
 *
 * SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T
 *   
 *
 * SEWS_FrontAxleArrangement_P1CSH_T
 *   
 *
 * SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T
 *   
 *
 * SEWS_WheelDifferentialLockPushButtonType_P1UG1_T
 *   
 *
 * Speed16bit_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_MovingUnitTraction_UICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_MovingUnitTraction_UICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void MovingUnitTraction_UICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_AxleConfiguration_P1B16_T: Integer in interval [0...255]
 * SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T: Integer in interval [0...255]
 * SEWS_FrontAxleArrangement_P1CSH_T: Integer in interval [0...255]
 * SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T: Integer in interval [0...255]
 * SEWS_WheelDifferentialLockPushButtonType_P1UG1_T: Integer in interval [0...255]
 * Speed16bit_T: Integer in interval [0...65535]
 *   Unit: [Km_per_h], Factor: 1, Offset: 0
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * A2PosSwitchStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   A2PosSwitchStatus_Off (0U)
 *   A2PosSwitchStatus_On (1U)
 *   A2PosSwitchStatus_Error (2U)
 *   A2PosSwitchStatus_NotAvailable (3U)
 * DeactivateActivate_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeactivateActivate_Deactivate (0U)
 *   DeactivateActivate_Activate (1U)
 *   DeactivateActivate_Error (2U)
 *   DeactivateActivate_NotAvailable (3U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DisengageEngage_T: Enumeration of integer in interval [0...3] with enumerators
 *   DisengageEngage_Disengage (0U)
 *   DisengageEngage_Engage (1U)
 *   DisengageEngage_Error (2U)
 *   DisengageEngage_NotAvailable (3U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * TractionControlDriverRqst_T: Enumeration of integer in interval [0...7] with enumerators
 *   TractionControlDriverRqst_NoAction (0U)
 *   TractionControlDriverRqst_TractionON (1U)
 *   TractionControlDriverRqst_TractionOFF (2U)
 *   TractionControlDriverRqst_TractionOFFROAD (3U)
 *   TractionControlDriverRqst_Spare (4U)
 *   TractionControlDriverRqst_Spare_01 (5U)
 *   TractionControlDriverRqst_Error (6U)
 *   TractionControlDriverRqst_NotAvailable (7U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 * Array Types:
 * ============
 * StandardNVM_T: Array with 4 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_AxleConfiguration_P1B16_T Rte_Prm_P1B16_AxleConfiguration_v(void)
 *   SEWS_FrontAxleArrangement_P1CSH_T Rte_Prm_P1CSH_FrontAxleArrangement_v(void)
 *   SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T Rte_Prm_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v(void)
 *   SEWS_WheelDifferentialLockPushButtonType_P1UG1_T Rte_Prm_P1UG1_WheelDifferentialLockPushButtonType_v(void)
 *   SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T Rte_Prm_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v(void)
 *   boolean Rte_Prm_P1B03_RearWheelDiffLockPushSw_v(void)
 *   boolean Rte_Prm_P1B04_DLFW_Installed_v(void)
 *   boolean Rte_Prm_P1CUC_ConstructionSw_Act_v(void)
 *   boolean Rte_Prm_P1F7J_ASROffRoadFullVersion_v(void)
 *   boolean Rte_Prm_P1FNW_ASROffButtonInstalled_v(void)
 *   boolean Rte_Prm_P1NQA_AutomaticFrontWheelDrive_Act_v(void)
 *   boolean Rte_Prm_P1SDA_OptitrackSystemInstalled_v(void)
 *
 *********************************************************************************************************************/


#define MovingUnitTraction_UICtrl_START_SEC_CODE
#include "MovingUnitTraction_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: MovingUnitTraction_UICtrl_Difflock_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_DifflockDeactivationBtn_stat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_DifflockMode_Wheelstatus_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_RearAxleDiffLock_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(Speed16bit_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AutoFrontWheelDriveDrvReq_AutoFrontWheelDriveDrvReq(DisengageEngage_T data)
 *   Std_ReturnType Rte_Write_DifflockOnOff_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_FrAxleDiffLockActvnDriverRqst_FrAxleDiffLockActvnDriverRqst(DisengageEngage_T data)
 *   Std_ReturnType Rte_Write_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_RearAxleDiffLockActvnDrvrReq_RearAxleDiffLockActvnDrvrReq(DisengageEngage_T data)
 *   Std_ReturnType Rte_Write_RearDiffLock_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_RrInterAxlDiffLockActvnDrvrReq_RrInterAxlDiffLockActvnDrvrReq(DisengageEngage_T data)
 *   Std_ReturnType Rte_Write_SplitBoxDifflockActvnDrvrRqst_TransferBDifflockActvnDrvrRqst(DisengageEngage_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: MovingUnitTraction_UICtrl_Difflock_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, MovingUnitTraction_UICtrl_CODE) MovingUnitTraction_UICtrl_Difflock_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: MovingUnitTraction_UICtrl_Difflock_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PushButtonStatus_T Read_DifflockDeactivationBtn_stat_PushButtonStatus;
  FreeWheel_Status_T Read_DifflockMode_Wheelstatus_FreeWheel_Status;
  StandardNVM_T Read_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I;
  PushButtonStatus_T Read_RearAxleDiffLock_ButtonStatus_PushButtonStatus;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;
  Speed16bit_T Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed;

  SEWS_AxleConfiguration_P1B16_T P1B16_AxleConfiguration_v_data;
  SEWS_FrontAxleArrangement_P1CSH_T P1CSH_FrontAxleArrangement_v_data;
  SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T P1NAK_DiffLockSinglePushSwitch_LogicSelection_v_data;
  SEWS_WheelDifferentialLockPushButtonType_P1UG1_T P1UG1_WheelDifferentialLockPushButtonType_v_data;
  SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v_data;
  boolean P1B03_RearWheelDiffLockPushSw_v_data;
  boolean P1B04_DLFW_Installed_v_data;
  boolean P1CUC_ConstructionSw_Act_v_data;
  boolean P1F7J_ASROffRoadFullVersion_v_data;
  boolean P1FNW_ASROffButtonInstalled_v_data;
  boolean P1NQA_AutomaticFrontWheelDrive_Act_v_data;
  boolean P1SDA_OptitrackSystemInstalled_v_data;

  StandardNVM_T Write_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1B16_AxleConfiguration_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1B16_AxleConfiguration_v();
  P1CSH_FrontAxleArrangement_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1CSH_FrontAxleArrangement_v();
  P1NAK_DiffLockSinglePushSwitch_LogicSelection_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v();
  P1UG1_WheelDifferentialLockPushButtonType_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1UG1_WheelDifferentialLockPushButtonType_v();
  P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v();
  P1B03_RearWheelDiffLockPushSw_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1B03_RearWheelDiffLockPushSw_v();
  P1B04_DLFW_Installed_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1B04_DLFW_Installed_v();
  P1CUC_ConstructionSw_Act_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1CUC_ConstructionSw_Act_v();
  P1F7J_ASROffRoadFullVersion_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1F7J_ASROffRoadFullVersion_v();
  P1FNW_ASROffButtonInstalled_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1FNW_ASROffButtonInstalled_v();
  P1NQA_AutomaticFrontWheelDrive_Act_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1NQA_AutomaticFrontWheelDrive_Act_v();
  P1SDA_OptitrackSystemInstalled_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1SDA_OptitrackSystemInstalled_v();

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_DifflockDeactivationBtn_stat_PushButtonStatus(&Read_DifflockDeactivationBtn_stat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_DifflockMode_Wheelstatus_FreeWheel_Status(&Read_DifflockMode_Wheelstatus_FreeWheel_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(Read_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_RearAxleDiffLock_ButtonStatus_PushButtonStatus(&Read_RearAxleDiffLock_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed(&Read_WheelBasedVehicleSpeed_WheelBasedVehicleSpeed);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_AutoFrontWheelDriveDrvReq_AutoFrontWheelDriveDrvReq(Rte_InitValue_AutoFrontWheelDriveDrvReq_AutoFrontWheelDriveDrvReq);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_DifflockOnOff_Indication_DeviceIndication(Rte_InitValue_DifflockOnOff_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_FrAxleDiffLockActvnDriverRqst_FrAxleDiffLockActvnDriverRqst(Rte_InitValue_FrAxleDiffLockActvnDriverRqst_FrAxleDiffLockActvnDriverRqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I, 0, sizeof(Write_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I));
  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I(Write_MUT_UICtrl_Difflock_NVM_I_MUT_UICtrl_Difflock_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_RearAxleDiffLockActvnDrvrReq_RearAxleDiffLockActvnDrvrReq(Rte_InitValue_RearAxleDiffLockActvnDrvrReq_RearAxleDiffLockActvnDrvrReq);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_RearDiffLock_DeviceIndication_DeviceIndication(Rte_InitValue_RearDiffLock_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_RrInterAxlDiffLockActvnDrvrReq_RrInterAxlDiffLockActvnDrvrReq(Rte_InitValue_RrInterAxlDiffLockActvnDrvrReq_RrInterAxlDiffLockActvnDrvrReq);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_SplitBoxDifflockActvnDrvrRqst_TransferBDifflockActvnDrvrRqst(Rte_InitValue_SplitBoxDifflockActvnDrvrRqst_TransferBDifflockActvnDrvrRqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  MovingUnitTraction_UICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: MovingUnitTraction_UICtrl_Traction_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ASROffButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_Construction_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status(DeactivateActivate_T *data)
 *   Std_ReturnType Rte_Read_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Read_Offroad_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_ASROff_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ConstructionSwitch_DeviceInd_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_ConstructionSwitch_stat_ConstructionSwitch_stat(OffOn_T data)
 *   Std_ReturnType Rte_Write_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(const uint8 *data)
 *     Argument data: uint8* is of type StandardNVM_T
 *   Std_ReturnType Rte_Write_Offroad_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_TractionControlDriverRqst_TractionControlDriverRqst(TractionControlDriverRqst_T data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BOW_63_TractionSw_Stuck_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: MovingUnitTraction_UICtrl_Traction_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, MovingUnitTraction_UICtrl_CODE) MovingUnitTraction_UICtrl_Traction_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: MovingUnitTraction_UICtrl_Traction_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  PushButtonStatus_T Read_ASROffButtonStatus_PushButtonStatus;
  A2PosSwitchStatus_T Read_Construction_SwitchStatus_A2PosSwitchStatus;
  DeactivateActivate_T Read_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status;
  StandardNVM_T Read_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I;
  PushButtonStatus_T Read_Offroad_ButtonStatus_PushButtonStatus;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;

  SEWS_AxleConfiguration_P1B16_T P1B16_AxleConfiguration_v_data;
  SEWS_FrontAxleArrangement_P1CSH_T P1CSH_FrontAxleArrangement_v_data;
  SEWS_DiffLockSinglePushSwitch_LogicSelection_P1NAK_T P1NAK_DiffLockSinglePushSwitch_LogicSelection_v_data;
  SEWS_WheelDifferentialLockPushButtonType_P1UG1_T P1UG1_WheelDifferentialLockPushButtonType_v_data;
  SEWS_RearAxleDifflockDisregardRequestVehicleSpeed_P1WEB_T P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v_data;
  boolean P1B03_RearWheelDiffLockPushSw_v_data;
  boolean P1B04_DLFW_Installed_v_data;
  boolean P1CUC_ConstructionSw_Act_v_data;
  boolean P1F7J_ASROffRoadFullVersion_v_data;
  boolean P1FNW_ASROffButtonInstalled_v_data;
  boolean P1NQA_AutomaticFrontWheelDrive_Act_v_data;
  boolean P1SDA_OptitrackSystemInstalled_v_data;

  StandardNVM_T Write_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1B16_AxleConfiguration_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1B16_AxleConfiguration_v();
  P1CSH_FrontAxleArrangement_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1CSH_FrontAxleArrangement_v();
  P1NAK_DiffLockSinglePushSwitch_LogicSelection_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1NAK_DiffLockSinglePushSwitch_LogicSelection_v();
  P1UG1_WheelDifferentialLockPushButtonType_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1UG1_WheelDifferentialLockPushButtonType_v();
  P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1WEB_RearAxleDifflockDisregardRequestVehicleSpeed_v();
  P1B03_RearWheelDiffLockPushSw_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1B03_RearWheelDiffLockPushSw_v();
  P1B04_DLFW_Installed_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1B04_DLFW_Installed_v();
  P1CUC_ConstructionSw_Act_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1CUC_ConstructionSw_Act_v();
  P1F7J_ASROffRoadFullVersion_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1F7J_ASROffRoadFullVersion_v();
  P1FNW_ASROffButtonInstalled_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1FNW_ASROffButtonInstalled_v();
  P1NQA_AutomaticFrontWheelDrive_Act_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1NQA_AutomaticFrontWheelDrive_Act_v();
  P1SDA_OptitrackSystemInstalled_v_data = TSC_MovingUnitTraction_UICtrl_Rte_Prm_P1SDA_OptitrackSystemInstalled_v();

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_ASROffButtonStatus_PushButtonStatus(&Read_ASROffButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_Construction_SwitchStatus_A2PosSwitchStatus(&Read_Construction_SwitchStatus_A2PosSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status(&Read_FrtAxleHydroActive_Status_FrtAxleHydroActive_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(Read_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_Offroad_ButtonStatus_PushButtonStatus(&Read_Offroad_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_ASROff_DeviceIndication_DeviceIndication(Rte_InitValue_ASROff_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_ConstructionSwitch_DeviceInd_DeviceIndication(Rte_InitValue_ConstructionSwitch_DeviceInd_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_ConstructionSwitch_stat_ConstructionSwitch_stat(Rte_InitValue_ConstructionSwitch_stat_ConstructionSwitch_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I, 0, sizeof(Write_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I));
  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I(Write_MUT_UICtrl_Traction_NVM_I_MUT_UICtrl_Traction_NVM_I);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_Offroad_Indication_DeviceIndication(Rte_InitValue_Offroad_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Write_TractionControlDriverRqst_TractionControlDriverRqst(Rte_InitValue_TractionControlDriverRqst_TractionControlDriverRqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_MovingUnitTraction_UICtrl_Rte_Call_Event_D1BOW_63_TractionSw_Stuck_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define MovingUnitTraction_UICtrl_STOP_SEC_CODE
#include "MovingUnitTraction_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void MovingUnitTraction_UICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_1 = A2PosSwitchStatus_Off;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_2 = A2PosSwitchStatus_On;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_3 = A2PosSwitchStatus_Error;
  A2PosSwitchStatus_T Test_A2PosSwitchStatus_T_V_4 = A2PosSwitchStatus_NotAvailable;

  DeactivateActivate_T Test_DeactivateActivate_T_V_1 = DeactivateActivate_Deactivate;
  DeactivateActivate_T Test_DeactivateActivate_T_V_2 = DeactivateActivate_Activate;
  DeactivateActivate_T Test_DeactivateActivate_T_V_3 = DeactivateActivate_Error;
  DeactivateActivate_T Test_DeactivateActivate_T_V_4 = DeactivateActivate_NotAvailable;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DisengageEngage_T Test_DisengageEngage_T_V_1 = DisengageEngage_Disengage;
  DisengageEngage_T Test_DisengageEngage_T_V_2 = DisengageEngage_Engage;
  DisengageEngage_T Test_DisengageEngage_T_V_3 = DisengageEngage_Error;
  DisengageEngage_T Test_DisengageEngage_T_V_4 = DisengageEngage_NotAvailable;

  FreeWheel_Status_T Test_FreeWheel_Status_T_V_1 = FreeWheel_Status_NoMovement;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_2 = FreeWheel_Status_1StepClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_3 = FreeWheel_Status_2StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_4 = FreeWheel_Status_3StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_5 = FreeWheel_Status_4StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_6 = FreeWheel_Status_5StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_7 = FreeWheel_Status_6StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_8 = FreeWheel_Status_1StepCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_9 = FreeWheel_Status_2StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_10 = FreeWheel_Status_3StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_11 = FreeWheel_Status_4StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_12 = FreeWheel_Status_5StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_13 = FreeWheel_Status_6StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_14 = FreeWheel_Status_Spare;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_15 = FreeWheel_Status_Error;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_16 = FreeWheel_Status_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  TractionControlDriverRqst_T Test_TractionControlDriverRqst_T_V_1 = TractionControlDriverRqst_NoAction;
  TractionControlDriverRqst_T Test_TractionControlDriverRqst_T_V_2 = TractionControlDriverRqst_TractionON;
  TractionControlDriverRqst_T Test_TractionControlDriverRqst_T_V_3 = TractionControlDriverRqst_TractionOFF;
  TractionControlDriverRqst_T Test_TractionControlDriverRqst_T_V_4 = TractionControlDriverRqst_TractionOFFROAD;
  TractionControlDriverRqst_T Test_TractionControlDriverRqst_T_V_5 = TractionControlDriverRqst_Spare;
  TractionControlDriverRqst_T Test_TractionControlDriverRqst_T_V_6 = TractionControlDriverRqst_Spare_01;
  TractionControlDriverRqst_T Test_TractionControlDriverRqst_T_V_7 = TractionControlDriverRqst_Error;
  TractionControlDriverRqst_T Test_TractionControlDriverRqst_T_V_8 = TractionControlDriverRqst_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
