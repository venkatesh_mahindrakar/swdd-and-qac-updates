/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  PowerTakeOff_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  PowerTakeOff_HMICtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <PowerTakeOff_HMICtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T
 *   
 *
 * SEWS_PTO_EmergencyFilteringTimer_P1BD3_T
 *   
 *
 * SEWS_PTO_RequestFilteringTimer_P1BD4_T
 *   
 *
 *********************************************************************************************************************/

#include "Rte_PowerTakeOff_HMICtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_PowerTakeOff_HMICtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void PowerTakeOff_HMICtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T: Integer in interval [0...255]
 * SEWS_PTO_EmergencyFilteringTimer_P1BD3_T: Integer in interval [0...255]
 * SEWS_PTO_RequestFilteringTimer_P1BD4_T: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   ButtonStatus_Idle (0U)
 *   ButtonStatus_Pressed (1U)
 *   ButtonStatus_Error (2U)
 *   ButtonStatus_NotAvailable (3U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * InactiveActive_T: Enumeration of integer in interval [0...3] with enumerators
 *   InactiveActive_Inactive (0U)
 *   InactiveActive_Active (1U)
 *   InactiveActive_Error (2U)
 *   InactiveActive_NotAvailable (3U)
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * Request_T: Enumeration of integer in interval [0...3] with enumerators
 *   Request_NotRequested (0U)
 *   Request_RequestActive (1U)
 *   Request_Error (2U)
 *   Request_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T Rte_Prm_P1BD2_PTO_EmergencyDeactivationTimer_v(void)
 *   SEWS_PTO_EmergencyFilteringTimer_P1BD3_T Rte_Prm_P1BD3_PTO_EmergencyFilteringTimer_v(void)
 *   SEWS_PTO_RequestFilteringTimer_P1BD4_T Rte_Prm_P1BD4_PTO_RequestFilteringTimer_v(void)
 *   boolean Rte_Prm_P1AJL_PTO1_Act_v(void)
 *   boolean Rte_Prm_P1AJM_PTO2_Act_v(void)
 *   boolean Rte_Prm_P1B9X_WirelessRC_Enable_v(void)
 *   boolean Rte_Prm_P1BBG_PTO3_Act_v(void)
 *   boolean Rte_Prm_P1BBH_PTO4_Act_v(void)
 *
 *********************************************************************************************************************/


#define PowerTakeOff_HMICtrl_START_SEC_CODE
#include "PowerTakeOff_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PowerTakeOff_HMICtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Pto1Indication_Pto1Indication(Request_T *data)
 *   Std_ReturnType Rte_Read_Pto1Status_Pto1Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_Pto1SwitchStatus_Pto1SwitchStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_Pto2Indication_Pto2Indication(Request_T *data)
 *   Std_ReturnType Rte_Read_Pto2Status_Pto2Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_Pto2SwitchStatus_Pto2SwitchStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_Pto3Indication_Pto3Indication(Request_T *data)
 *   Std_ReturnType Rte_Read_Pto3Status_Pto3Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_Pto3SwitchStatus_Pto3SwitchStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_Pto4Indication_Pto4Indication(Request_T *data)
 *   Std_ReturnType Rte_Read_Pto4Status_Pto4Status(InactiveActive_T *data)
 *   Std_ReturnType Rte_Read_Pto4SwitchStatus_Pto4SwitchStatus(OffOn_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(VehicleModeDistribution_T *data)
 *   Std_ReturnType Rte_Read_WrcPto1ButtonStatus_WrcPto1ButtonStatus(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcPto2ButtonStatus_WrcPto2ButtonStatus(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcPto3ButtonStatus_WrcPto3ButtonStatus(ButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_WrcPto4ButtonStatus_WrcPto4ButtonStatus(ButtonStatus_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_PTO1_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_PTO2_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_PTO3_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_PTO4_DeviceIndication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_Pto1CabRequest_Pto1CabRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto2CabRequest_Pto2CabRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto3CabRequest_Pto3CabRequest(OffOn_T data)
 *   Std_ReturnType Rte_Write_Pto4CabRequest_Pto4CabRequest(OffOn_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PowerTakeOff_HMICtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PowerTakeOff_HMICtrl_CODE) PowerTakeOff_HMICtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PowerTakeOff_HMICtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Request_T Read_Pto1Indication_Pto1Indication;
  InactiveActive_T Read_Pto1Status_Pto1Status;
  OffOn_T Read_Pto1SwitchStatus_Pto1SwitchStatus;
  Request_T Read_Pto2Indication_Pto2Indication;
  InactiveActive_T Read_Pto2Status_Pto2Status;
  OffOn_T Read_Pto2SwitchStatus_Pto2SwitchStatus;
  Request_T Read_Pto3Indication_Pto3Indication;
  InactiveActive_T Read_Pto3Status_Pto3Status;
  OffOn_T Read_Pto3SwitchStatus_Pto3SwitchStatus;
  Request_T Read_Pto4Indication_Pto4Indication;
  InactiveActive_T Read_Pto4Status_Pto4Status;
  OffOn_T Read_Pto4SwitchStatus_Pto4SwitchStatus;
  VehicleModeDistribution_T Read_SwcActivation_IgnitionOn_IgnitionOn;
  ButtonStatus_T Read_WrcPto1ButtonStatus_WrcPto1ButtonStatus;
  ButtonStatus_T Read_WrcPto2ButtonStatus_WrcPto2ButtonStatus;
  ButtonStatus_T Read_WrcPto3ButtonStatus_WrcPto3ButtonStatus;
  ButtonStatus_T Read_WrcPto4ButtonStatus_WrcPto4ButtonStatus;

  SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T P1BD2_PTO_EmergencyDeactivationTimer_v_data;
  SEWS_PTO_EmergencyFilteringTimer_P1BD3_T P1BD3_PTO_EmergencyFilteringTimer_v_data;
  SEWS_PTO_RequestFilteringTimer_P1BD4_T P1BD4_PTO_RequestFilteringTimer_v_data;

  boolean P1AJL_PTO1_Act_v_data;
  boolean P1AJM_PTO2_Act_v_data;
  boolean P1B9X_WirelessRC_Enable_v_data;
  boolean P1BBG_PTO3_Act_v_data;
  boolean P1BBH_PTO4_Act_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1BD2_PTO_EmergencyDeactivationTimer_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BD2_PTO_EmergencyDeactivationTimer_v();
  P1BD3_PTO_EmergencyFilteringTimer_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BD3_PTO_EmergencyFilteringTimer_v();
  P1BD4_PTO_RequestFilteringTimer_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BD4_PTO_RequestFilteringTimer_v();

  P1AJL_PTO1_Act_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1AJL_PTO1_Act_v();
  P1AJM_PTO2_Act_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1AJM_PTO2_Act_v();
  P1B9X_WirelessRC_Enable_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v();
  P1BBG_PTO3_Act_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BBG_PTO3_Act_v();
  P1BBH_PTO4_Act_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BBH_PTO4_Act_v();

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto1Indication_Pto1Indication(&Read_Pto1Indication_Pto1Indication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto1Status_Pto1Status(&Read_Pto1Status_Pto1Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto1SwitchStatus_Pto1SwitchStatus(&Read_Pto1SwitchStatus_Pto1SwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto2Indication_Pto2Indication(&Read_Pto2Indication_Pto2Indication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto2Status_Pto2Status(&Read_Pto2Status_Pto2Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto2SwitchStatus_Pto2SwitchStatus(&Read_Pto2SwitchStatus_Pto2SwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto3Indication_Pto3Indication(&Read_Pto3Indication_Pto3Indication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto3Status_Pto3Status(&Read_Pto3Status_Pto3Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto3SwitchStatus_Pto3SwitchStatus(&Read_Pto3SwitchStatus_Pto3SwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto4Indication_Pto4Indication(&Read_Pto4Indication_Pto4Indication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto4Status_Pto4Status(&Read_Pto4Status_Pto4Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_Pto4SwitchStatus_Pto4SwitchStatus(&Read_Pto4SwitchStatus_Pto4SwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_SwcActivation_IgnitionOn_IgnitionOn(&Read_SwcActivation_IgnitionOn_IgnitionOn);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_WrcPto1ButtonStatus_WrcPto1ButtonStatus(&Read_WrcPto1ButtonStatus_WrcPto1ButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_WrcPto2ButtonStatus_WrcPto2ButtonStatus(&Read_WrcPto2ButtonStatus_WrcPto2ButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_WrcPto3ButtonStatus_WrcPto3ButtonStatus(&Read_WrcPto3ButtonStatus_WrcPto3ButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Read_WrcPto4ButtonStatus_WrcPto4ButtonStatus(&Read_WrcPto4ButtonStatus_WrcPto4ButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Write_PTO1_DeviceIndication_DeviceIndication(Rte_InitValue_PTO1_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Write_PTO2_DeviceIndication_DeviceIndication(Rte_InitValue_PTO2_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Write_PTO3_DeviceIndication_DeviceIndication(Rte_InitValue_PTO3_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Write_PTO4_DeviceIndication_DeviceIndication(Rte_InitValue_PTO4_DeviceIndication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Write_Pto1CabRequest_Pto1CabRequest(Rte_InitValue_Pto1CabRequest_Pto1CabRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Write_Pto2CabRequest_Pto2CabRequest(Rte_InitValue_Pto2CabRequest_Pto2CabRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Write_Pto3CabRequest_Pto3CabRequest(Rte_InitValue_Pto3CabRequest_Pto3CabRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_PowerTakeOff_HMICtrl_Rte_Write_Pto4CabRequest_Pto4CabRequest(Rte_InitValue_Pto4CabRequest_Pto4CabRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  PowerTakeOff_HMICtrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: PowerTakeOff_HMICtrl_init
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: PowerTakeOff_HMICtrl_init_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, PowerTakeOff_HMICtrl_CODE) PowerTakeOff_HMICtrl_init(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: PowerTakeOff_HMICtrl_init
 *********************************************************************************************************************/

  SEWS_PTO_EmergencyDeactivationTimer_P1BD2_T P1BD2_PTO_EmergencyDeactivationTimer_v_data;
  SEWS_PTO_EmergencyFilteringTimer_P1BD3_T P1BD3_PTO_EmergencyFilteringTimer_v_data;
  SEWS_PTO_RequestFilteringTimer_P1BD4_T P1BD4_PTO_RequestFilteringTimer_v_data;

  boolean P1AJL_PTO1_Act_v_data;
  boolean P1AJM_PTO2_Act_v_data;
  boolean P1B9X_WirelessRC_Enable_v_data;
  boolean P1BBG_PTO3_Act_v_data;
  boolean P1BBH_PTO4_Act_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1BD2_PTO_EmergencyDeactivationTimer_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BD2_PTO_EmergencyDeactivationTimer_v();
  P1BD3_PTO_EmergencyFilteringTimer_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BD3_PTO_EmergencyFilteringTimer_v();
  P1BD4_PTO_RequestFilteringTimer_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BD4_PTO_RequestFilteringTimer_v();

  P1AJL_PTO1_Act_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1AJL_PTO1_Act_v();
  P1AJM_PTO2_Act_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1AJM_PTO2_Act_v();
  P1B9X_WirelessRC_Enable_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1B9X_WirelessRC_Enable_v();
  P1BBG_PTO3_Act_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BBG_PTO3_Act_v();
  P1BBH_PTO4_Act_v_data = TSC_PowerTakeOff_HMICtrl_Rte_Prm_P1BBH_PTO4_Act_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define PowerTakeOff_HMICtrl_STOP_SEC_CODE
#include "PowerTakeOff_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void PowerTakeOff_HMICtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  ButtonStatus_T Test_ButtonStatus_T_V_1 = ButtonStatus_Idle;
  ButtonStatus_T Test_ButtonStatus_T_V_2 = ButtonStatus_Pressed;
  ButtonStatus_T Test_ButtonStatus_T_V_3 = ButtonStatus_Error;
  ButtonStatus_T Test_ButtonStatus_T_V_4 = ButtonStatus_NotAvailable;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  InactiveActive_T Test_InactiveActive_T_V_1 = InactiveActive_Inactive;
  InactiveActive_T Test_InactiveActive_T_V_2 = InactiveActive_Active;
  InactiveActive_T Test_InactiveActive_T_V_3 = InactiveActive_Error;
  InactiveActive_T Test_InactiveActive_T_V_4 = InactiveActive_NotAvailable;

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  Request_T Test_Request_T_V_1 = Request_NotRequested;
  Request_T Test_Request_T_V_2 = Request_RequestActive;
  Request_T Test_Request_T_V_3 = Request_Error;
  Request_T Test_Request_T_V_4 = Request_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
