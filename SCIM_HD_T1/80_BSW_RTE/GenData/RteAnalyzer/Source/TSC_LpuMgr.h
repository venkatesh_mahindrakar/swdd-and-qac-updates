/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_LpuMgr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_LpuMgr_Rte_Read_FSC_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T *data);
Std_ReturnType TSC_LpuMgr_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_LpuMgr_Rte_Write_LpModeRunTime_P_LpModeRunTime(uint32 data);

/** Calibration Component Calibration Parameters */
SEWS_HwToleranceThreshold_X1C04_T  TSC_LpuMgr_Rte_Prm_X1C04_HwToleranceThreshold_v(void);
SEWS_PcbConfig_DoorAccessIf_X1CX3_T  TSC_LpuMgr_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(void);
SEWS_PcbConfig_LinInterfaces_X1CX0_a_T * TSC_LpuMgr_Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v(void);
SEWS_PcbConfig_CanInterfaces_X1CX2_a_T * TSC_LpuMgr_Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v(void);
SEWS_PcbConfig_Adi_X1CXW_a_T * TSC_LpuMgr_Rte_Prm_X1CXW_PcbConfig_Adi_v(void);
SEWS_PcbConfig_DOBHS_X1CXX_a_T * TSC_LpuMgr_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(void);
SEWS_PcbConfig_DOWHS_X1CXY_a_T * TSC_LpuMgr_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(void);
SEWS_PcbConfig_DOWLS_X1CXZ_a_T * TSC_LpuMgr_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(void);
SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T * TSC_LpuMgr_Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v(void);
SEWS_PcbConfig_AdiPullUp_X1CX5_s_T * TSC_LpuMgr_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(void);
SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_LpuMgr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void);
boolean  TSC_LpuMgr_Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v(void);
boolean  TSC_LpuMgr_Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v(void);
boolean  TSC_LpuMgr_Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v(void);
boolean  TSC_LpuMgr_Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v(void);
SEWS_AdiWakeUpConfig_P1WMD_a_T * TSC_LpuMgr_Rte_Prm_P1WMD_AdiWakeUpConfig_v(void);
SEWS_DAI_Installed_P1WMP_s_T * TSC_LpuMgr_Rte_Prm_P1WMP_DAI_Installed_v(void);
boolean  TSC_LpuMgr_Rte_Prm_P1WPP_isSecurityLinActive_v(void);




