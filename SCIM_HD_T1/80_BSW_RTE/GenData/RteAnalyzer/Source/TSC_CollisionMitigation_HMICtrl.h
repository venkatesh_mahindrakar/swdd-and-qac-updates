/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CollisionMitigation_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_AEBS_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_CM_Status_CM_Status(CM_Status_T *data);
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_FCWPushButton_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_FCW_Status_FCW_Status(DisableEnable_T *data);
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_FCW_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data);
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM(CollSituationHMICtrlRequestVM_T data);
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_FCW_Enable_FCW_Enable(DisableEnable_T data);
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_SetCMOperation_SetCMOperation(SetCMOperation_T data);
Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_SetFCWOperation_SetFCWOperation(SetFCWOperation_T data);

/** Calibration Component Calibration Parameters */
SEWS_HeadwaySupport_P1BEX_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1BEX_HeadwaySupport_v(void);
SEWS_FCW_LedLogic_P1LG1_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LG1_FCW_LedLogic_v(void);
SEWS_CM_Configuration_P1LGD_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGD_CM_Configuration_v(void);
SEWS_FCW_SwPushThreshold_P1LGE_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGE_FCW_SwPushThreshold_v(void);
SEWS_FCW_ConfirmTimeout_P1LGF_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGF_FCW_ConfirmTimeout_v(void);
SEWS_FCW_SwStuckTimeout_P1LGG_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGG_FCW_SwStuckTimeout_v(void);
SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1MOT_CollSituationHMICtrlRequestVM_Time_v(void);
boolean  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1NT1_CM_DeviceType_v(void);




