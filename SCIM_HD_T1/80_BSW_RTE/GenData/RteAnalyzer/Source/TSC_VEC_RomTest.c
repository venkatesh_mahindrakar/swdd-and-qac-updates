/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_VEC_RomTest.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_VEC_RomTest.h"
#include "TSC_VEC_RomTest.h"















     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */













     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_VEC_RomTest_Rte_Call_CsmSignatureVerify_SignatureVerifyFinish(const uint8 *signatureBuffer, uint32 signatureLength, Csm_VerifyResultType *resultBuffer)
{
  return Rte_Call_CsmSignatureVerify_SignatureVerifyFinish(signatureBuffer, signatureLength, resultBuffer);
}
Std_ReturnType TSC_VEC_RomTest_Rte_Call_CsmSignatureVerify_SignatureVerifyStart(const AsymPublicKeyType *key)
{
  return Rte_Call_CsmSignatureVerify_SignatureVerifyStart(key);
}
Std_ReturnType TSC_VEC_RomTest_Rte_Call_CsmSignatureVerify_SignatureVerifyUpdate(const uint8 *dataBuffer, uint32 dataLength)
{
  return Rte_Call_CsmSignatureVerify_SignatureVerifyUpdate(dataBuffer, dataLength);
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */







     /* VEC_RomTest */
      /* VEC_RomTest */
void TSC_VEC_RomTest_Rte_Enter_VEC_ExclusiveArea(void)
{
  Rte_Enter_VEC_ExclusiveArea();
}
void TSC_VEC_RomTest_Rte_Exit_VEC_ExclusiveArea(void)
{
  Rte_Exit_VEC_ExclusiveArea();
}



