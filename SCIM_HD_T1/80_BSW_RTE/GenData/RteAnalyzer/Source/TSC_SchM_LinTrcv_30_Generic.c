/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_LinTrcv_30_Generic.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_LinTrcv_30_Generic.h"
#include "TSC_SchM_LinTrcv_30_Generic.h"
void TSC_LinTrcv_30_Generic_SchM_Enter_LinTrcv_30_Generic_LINTRCV_30_GENERIC_EXCLUSIVE_AREA_0(void)
{
  SchM_Enter_LinTrcv_30_Generic_LINTRCV_30_GENERIC_EXCLUSIVE_AREA_0();
}
void TSC_LinTrcv_30_Generic_SchM_Exit_LinTrcv_30_Generic_LINTRCV_30_GENERIC_EXCLUSIVE_AREA_0(void)
{
  SchM_Exit_LinTrcv_30_Generic_LINTRCV_30_GENERIC_EXCLUSIVE_AREA_0();
}
