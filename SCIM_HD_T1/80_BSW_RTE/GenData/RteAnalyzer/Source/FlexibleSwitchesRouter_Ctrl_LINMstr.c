/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  FlexibleSwitchesRouter_Ctrl_LINMstr.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  FlexibleSwitchesRouter_Ctrl_LINMstr
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <FlexibleSwitchesRouter_Ctrl_LINMstr>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Boolean
 *   
 *
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dcm_OpStatusType
 *   
 *
 * Dem_EventStatusType
 *   
 *
 * Dem_InitMonitorReasonType
 *   
 *
 * SEWS_FSPConfigSettingsLIN1_P1EWZ_T
 *   
 *
 * SEWS_FSPConfigSettingsLIN2_P1EW0_T
 *   
 *
 * SEWS_FSPConfigSettingsLIN3_P1EW1_T
 *   
 *
 * SEWS_FSPConfigSettingsLIN4_P1EW2_T
 *   
 *
 * SEWS_FSPConfigSettingsLIN5_P1EW3_T
 *   
 *
 *
 * Runnable Entities:
 * ==================
 * FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *   
 *
 *********************************************************************************************************************/

#include "Rte_FlexibleSwitchesRouter_Ctrl_LINMstr.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_FlexibleSwitchesRouter_Ctrl_LINMstr.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void FlexibleSwitchesRouter_Ctrl_LINMstr_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Boolean: Boolean
 * FSP1ResponseErrorL1_T: Boolean
 * FSP1ResponseErrorL2_T: Boolean
 * FSP1ResponseErrorL3_T: Boolean
 * FSP1ResponseErrorL4_T: Boolean
 * FSP1ResponseErrorL5_T: Boolean
 * FSP2ResponseErrorL1_T: Boolean
 * FSP2ResponseErrorL2_T: Boolean
 * FSP2ResponseErrorL3_T: Boolean
 * FSP3ResponseErrorL2_T: Boolean
 * FSP4ResponseErrorL2_T: Boolean
 * FSPDiagInfo_T: Integer in interval [0...63]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * FSPIndicationCmd_T: Integer in interval [0...65535]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * FSPSwitchStatus_T: Integer in interval [0...255]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * SEWS_FSPConfigSettingsLIN1_P1EWZ_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN2_P1EW0_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN3_P1EW1_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN4_P1EW2_T: Integer in interval [0...255]
 * SEWS_FSPConfigSettingsLIN5_P1EW3_T: Integer in interval [0...255]
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dcm_OpStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_INITIAL (0U)
 *   DCM_PENDING (1U)
 *   DCM_CANCEL (2U)
 *   DCM_FORCE_RCRRP_OK (3U)
 *   DCM_FORCE_RCRRP_NOT_OK (64U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * Dem_InitMonitorReasonType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_INIT_MONITOR_CLEAR (1U)
 *   DEM_INIT_MONITOR_RESTART (2U)
 *   DEM_INIT_MONITOR_REENABLED (3U)
 *   DEM_INIT_MONITOR_STORAGE_REENABLED (4U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * LinDiagBusInfo: Enumeration of integer in interval [0...255] with enumerators
 *   None (0U)
 *   LinDiag_BUS1 (1U)
 *   LinDiag_BUS2 (2U)
 *   LinDiag_BUS3 (3U)
 *   LinDiag_BUS4 (4U)
 *   LinDiag_BUS5 (5U)
 *   LinDiag_SpareBUS1 (6U)
 *   LinDiag_SpareBUS2 (7U)
 *   LinDiag_ALL_BUSSES (8U)
 * LinDiagRequest_T: Enumeration of integer in interval [0...255] with enumerators
 *   NO_OPEARATION (0U)
 *   START_OPEARATION (1U)
 *   STOP_OPERATION (2U)
 * LinDiagServiceStatus: Enumeration of integer in interval [0...255] with enumerators
 *   LinDiagService_None (0U)
 *   LinDiagService_Pending (1U)
 *   LinDiagService_Completed (2U)
 *   LinDiagService_Error (3U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 * Dcm_Data2ByteType: Array with 2 element(s) of type uint8
 * Dcm_Data5ByteType: Array with 5 element(s) of type uint8
 * Dcm_Data6ByteType: Array with 6 element(s) of type uint8
 * Dcm_Data7ByteType: Array with 7 element(s) of type uint8
 * FSPIndicationCmdArray_T: Array with 8 element(s) of type DeviceIndication_T
 * FSPSwitchStatusArray_T: Array with 8 element(s) of type PushButtonStatus_T
 * FSP_Array5: Array with 5 element(s) of type uint8
 * FspNVM_T: Array with 28 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_FSPConfigSettingsLIN2_P1EW0_T Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v(void)
 *   SEWS_FSPConfigSettingsLIN3_P1EW1_T Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v(void)
 *   SEWS_FSPConfigSettingsLIN4_P1EW2_T Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v(void)
 *   SEWS_FSPConfigSettingsLIN5_P1EW3_T Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v(void)
 *   SEWS_FSPConfigSettingsLIN1_P1EWZ_T Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v(void)
 *
 *********************************************************************************************************************/


#define FlexibleSwitchesRouter_Ctrl_LINMstr_START_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_LINMstr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <InitMonitorForEvent> of PortPrototype <CBInitEvt_D1BN8_79_FlexSwLostFCI>
 *
 **********************************************************************************************************************
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_44_FSP_MemoryFailure_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_CallbackInitMonitorForEvent_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CBInitEvt_D1BN8_79_FlexSwLostFCI_InitMonitorForEvent (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BK9_87_FSP_NoResp_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BN8_16_FSP_VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BN8_17_FSP_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BN8_44_FSP_MemoryFailure_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_Event_D1BN8_79_FlexSwLostFCI_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  FlexibleSwitchesRouter_Ctrl_LINMstr_TestDefines();

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1C1R_Data_P1C1R_NbFSP>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(uint8 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data6ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1C1R_Data_P1C1R_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  FSP_Array5 DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD(DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData_Irv_NbOfFspByCCNAD);

  return RTE_E_DataServices_P1C1R_Data_P1C1R_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(uint8 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data5ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1DCU_Data_P1DCU_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  FSP_Array5 DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure(DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData_Irv_NbOfFspInFailure);

  return RTE_E_DataServices_P1DCU_Data_P1DCU_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  return RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  uint8 DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData_Irv_IOCTL_FspLinCtrl();

  return RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_FspLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_FspLinCtrl(0U);

  return RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_FspLinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_FspLinCtrl(0U);

  return RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN1_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN2_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN3_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL1_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL3_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL4_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1DiagInfoL5_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP1IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL1_FSP1ResponseErrorL1(FSP1ResponseErrorL1_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL2_FSP1ResponseErrorL2(FSP1ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL3_FSP1ResponseErrorL3(FSP1ResponseErrorL3_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL4_FSP1ResponseErrorL4(FSP1ResponseErrorL4_T *data)
 *   Std_ReturnType Rte_Read_FSP1ResponseErrorL5_FSP1ResponseErrorL5(FSP1ResponseErrorL5_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL1_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL3_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL4_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP1SwitchStatusL5_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP2DiagInfoL1_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP2DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP2DiagInfoL3_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP2IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP2ResponseErrorL1_FSP2ResponseErrorL1(FSP2ResponseErrorL1_T *data)
 *   Std_ReturnType Rte_Read_FSP2ResponseErrorL2_FSP2ResponseErrorL2(FSP2ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP2ResponseErrorL3_FSP2ResponseErrorL3(FSP2ResponseErrorL3_T *data)
 *   Std_ReturnType Rte_Read_FSP2SwitchStatusL1_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP2SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP2SwitchStatusL3_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP3DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP3IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP3ResponseErrorL2_FSP3ResponseErrorL2(FSP3ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP3SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP4DiagInfoL2_FSPDiagInfo(FSPDiagInfo_T *data)
 *   Std_ReturnType Rte_Read_FSP4IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP4ResponseErrorL2_FSP4ResponseErrorL2(FSP4ResponseErrorL2_T *data)
 *   Std_ReturnType Rte_Read_FSP4SwitchStatusL2_FSPSwitchStatus(FSPSwitchStatus_T *data)
 *   Std_ReturnType Rte_Read_FSP5IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP6IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP7IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP8IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP9IndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FSP_BIndicationCmd_FSPIndicationCmdArray(DeviceIndication_T *data)
 *     Argument data: DeviceIndication_T* is of type FSPIndicationCmdArray_T
 *   Std_ReturnType Rte_Read_FspNV_PR_FspNV(uint8 *data)
 *     Argument data: uint8* is of type FspNVM_T
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL4_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1IndicationCmdL5_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP1SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP2IndicationCmdL1_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP2IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP2IndicationCmdL3_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP2SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP3IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP3SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP4IndicationCmdL2_FSPIndicationCmd(FSPIndicationCmd_T data)
 *   Std_ReturnType Rte_Write_FSP4SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP5SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP6SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP7SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP8SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP9SwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FSP_BSwitchStatus_FSPSwitchStatusArray(const PushButtonStatus_T *data)
 *     Argument data: PushButtonStatus_T* is of type FSPSwitchStatusArray_T
 *   Std_ReturnType Rte_Write_FspNV_PR_FspNV(const uint8 *data)
 *     Argument data: uint8* is of type FspNVM_T
 *   Std_ReturnType Rte_Write_Living12VResetRequest_Living12VResetRequest(Boolean data)
 *   Std_ReturnType Rte_Write_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(Boolean data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(void)
 *   void Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(uint8 *data)
 *   void Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(uint8 *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(uint8 data)
 *   void Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(const uint8 *data)
 *   void Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(const uint8 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) FlexibleSwitchPanel_LINMastCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: FlexibleSwitchPanel_LINMastCtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  ComMode_LIN_Type Read_ComMode_LIN1_ComMode_LIN;
  ComMode_LIN_Type Read_ComMode_LIN2_ComMode_LIN;
  ComMode_LIN_Type Read_ComMode_LIN3_ComMode_LIN;
  ComMode_LIN_Type Read_ComMode_LIN4_ComMode_LIN;
  ComMode_LIN_Type Read_ComMode_LIN5_ComMode_LIN;
  DiagActiveState_T Read_DiagActiveState_isDiagActive;
  FSPDiagInfo_T Read_FSP1DiagInfoL1_FSPDiagInfo;
  FSPDiagInfo_T Read_FSP1DiagInfoL2_FSPDiagInfo;
  FSPDiagInfo_T Read_FSP1DiagInfoL3_FSPDiagInfo;
  FSPDiagInfo_T Read_FSP1DiagInfoL4_FSPDiagInfo;
  FSPDiagInfo_T Read_FSP1DiagInfoL5_FSPDiagInfo;
  FSPIndicationCmdArray_T Read_FSP1IndicationCmd_FSPIndicationCmdArray;
  FSP1ResponseErrorL1_T Read_FSP1ResponseErrorL1_FSP1ResponseErrorL1;
  FSP1ResponseErrorL2_T Read_FSP1ResponseErrorL2_FSP1ResponseErrorL2;
  FSP1ResponseErrorL3_T Read_FSP1ResponseErrorL3_FSP1ResponseErrorL3;
  FSP1ResponseErrorL4_T Read_FSP1ResponseErrorL4_FSP1ResponseErrorL4;
  FSP1ResponseErrorL5_T Read_FSP1ResponseErrorL5_FSP1ResponseErrorL5;
  FSPSwitchStatus_T Read_FSP1SwitchStatusL1_FSPSwitchStatus;
  FSPSwitchStatus_T Read_FSP1SwitchStatusL2_FSPSwitchStatus;
  FSPSwitchStatus_T Read_FSP1SwitchStatusL3_FSPSwitchStatus;
  FSPSwitchStatus_T Read_FSP1SwitchStatusL4_FSPSwitchStatus;
  FSPSwitchStatus_T Read_FSP1SwitchStatusL5_FSPSwitchStatus;
  FSPDiagInfo_T Read_FSP2DiagInfoL1_FSPDiagInfo;
  FSPDiagInfo_T Read_FSP2DiagInfoL2_FSPDiagInfo;
  FSPDiagInfo_T Read_FSP2DiagInfoL3_FSPDiagInfo;
  FSPIndicationCmdArray_T Read_FSP2IndicationCmd_FSPIndicationCmdArray;
  FSP2ResponseErrorL1_T Read_FSP2ResponseErrorL1_FSP2ResponseErrorL1;
  FSP2ResponseErrorL2_T Read_FSP2ResponseErrorL2_FSP2ResponseErrorL2;
  FSP2ResponseErrorL3_T Read_FSP2ResponseErrorL3_FSP2ResponseErrorL3;
  FSPSwitchStatus_T Read_FSP2SwitchStatusL1_FSPSwitchStatus;
  FSPSwitchStatus_T Read_FSP2SwitchStatusL2_FSPSwitchStatus;
  FSPSwitchStatus_T Read_FSP2SwitchStatusL3_FSPSwitchStatus;
  FSPDiagInfo_T Read_FSP3DiagInfoL2_FSPDiagInfo;
  FSPIndicationCmdArray_T Read_FSP3IndicationCmd_FSPIndicationCmdArray;
  FSP3ResponseErrorL2_T Read_FSP3ResponseErrorL2_FSP3ResponseErrorL2;
  FSPSwitchStatus_T Read_FSP3SwitchStatusL2_FSPSwitchStatus;
  FSPDiagInfo_T Read_FSP4DiagInfoL2_FSPDiagInfo;
  FSPIndicationCmdArray_T Read_FSP4IndicationCmd_FSPIndicationCmdArray;
  FSP4ResponseErrorL2_T Read_FSP4ResponseErrorL2_FSP4ResponseErrorL2;
  FSPSwitchStatus_T Read_FSP4SwitchStatusL2_FSPSwitchStatus;
  FSPIndicationCmdArray_T Read_FSP5IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Read_FSP6IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Read_FSP7IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Read_FSP8IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Read_FSP9IndicationCmd_FSPIndicationCmdArray;
  FSPIndicationCmdArray_T Read_FSP_BIndicationCmd_FSPIndicationCmdArray;
  FspNVM_T Read_FspNV_PR_FspNV;

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  FSPSwitchStatusArray_T Write_FSP1SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Write_FSP2SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Write_FSP3SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Write_FSP4SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Write_FSP5SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Write_FSP6SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Write_FSP7SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Write_FSP8SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Write_FSP9SwitchStatus_FSPSwitchStatusArray;
  FSPSwitchStatusArray_T Write_FSP_BSwitchStatus_FSPSwitchStatusArray;
  FspNVM_T Write_FspNV_PR_FspNV;

  uint8 FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl;
  FSP_Array5 FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD;
  FSP_Array5 FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure;

  FSP_Array5 FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD_Write;
  FSP_Array5 FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure_Write;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN1_ComMode_LIN(&Read_ComMode_LIN1_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN2_ComMode_LIN(&Read_ComMode_LIN2_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN3_ComMode_LIN(&Read_ComMode_LIN3_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN4_ComMode_LIN(&Read_ComMode_LIN4_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_ComMode_LIN5_ComMode_LIN(&Read_ComMode_LIN5_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_DiagActiveState_isDiagActive(&Read_DiagActiveState_isDiagActive);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL1_FSPDiagInfo(&Read_FSP1DiagInfoL1_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL2_FSPDiagInfo(&Read_FSP1DiagInfoL2_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL3_FSPDiagInfo(&Read_FSP1DiagInfoL3_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL4_FSPDiagInfo(&Read_FSP1DiagInfoL4_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1DiagInfoL5_FSPDiagInfo(&Read_FSP1DiagInfoL5_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1IndicationCmd_FSPIndicationCmdArray(Read_FSP1IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL1_FSP1ResponseErrorL1(&Read_FSP1ResponseErrorL1_FSP1ResponseErrorL1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL2_FSP1ResponseErrorL2(&Read_FSP1ResponseErrorL2_FSP1ResponseErrorL2);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL3_FSP1ResponseErrorL3(&Read_FSP1ResponseErrorL3_FSP1ResponseErrorL3);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL4_FSP1ResponseErrorL4(&Read_FSP1ResponseErrorL4_FSP1ResponseErrorL4);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1ResponseErrorL5_FSP1ResponseErrorL5(&Read_FSP1ResponseErrorL5_FSP1ResponseErrorL5);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL1_FSPSwitchStatus(&Read_FSP1SwitchStatusL1_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL2_FSPSwitchStatus(&Read_FSP1SwitchStatusL2_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL3_FSPSwitchStatus(&Read_FSP1SwitchStatusL3_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL4_FSPSwitchStatus(&Read_FSP1SwitchStatusL4_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP1SwitchStatusL5_FSPSwitchStatus(&Read_FSP1SwitchStatusL5_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2DiagInfoL1_FSPDiagInfo(&Read_FSP2DiagInfoL1_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2DiagInfoL2_FSPDiagInfo(&Read_FSP2DiagInfoL2_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2DiagInfoL3_FSPDiagInfo(&Read_FSP2DiagInfoL3_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2IndicationCmd_FSPIndicationCmdArray(Read_FSP2IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2ResponseErrorL1_FSP2ResponseErrorL1(&Read_FSP2ResponseErrorL1_FSP2ResponseErrorL1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2ResponseErrorL2_FSP2ResponseErrorL2(&Read_FSP2ResponseErrorL2_FSP2ResponseErrorL2);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2ResponseErrorL3_FSP2ResponseErrorL3(&Read_FSP2ResponseErrorL3_FSP2ResponseErrorL3);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2SwitchStatusL1_FSPSwitchStatus(&Read_FSP2SwitchStatusL1_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2SwitchStatusL2_FSPSwitchStatus(&Read_FSP2SwitchStatusL2_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP2SwitchStatusL3_FSPSwitchStatus(&Read_FSP2SwitchStatusL3_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP3DiagInfoL2_FSPDiagInfo(&Read_FSP3DiagInfoL2_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP3IndicationCmd_FSPIndicationCmdArray(Read_FSP3IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP3ResponseErrorL2_FSP3ResponseErrorL2(&Read_FSP3ResponseErrorL2_FSP3ResponseErrorL2);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP3SwitchStatusL2_FSPSwitchStatus(&Read_FSP3SwitchStatusL2_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP4DiagInfoL2_FSPDiagInfo(&Read_FSP4DiagInfoL2_FSPDiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP4IndicationCmd_FSPIndicationCmdArray(Read_FSP4IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP4ResponseErrorL2_FSP4ResponseErrorL2(&Read_FSP4ResponseErrorL2_FSP4ResponseErrorL2);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP4SwitchStatusL2_FSPSwitchStatus(&Read_FSP4SwitchStatusL2_FSPSwitchStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP5IndicationCmd_FSPIndicationCmdArray(Read_FSP5IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP6IndicationCmd_FSPIndicationCmdArray(Read_FSP6IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP7IndicationCmd_FSPIndicationCmdArray(Read_FSP7IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP8IndicationCmd_FSPIndicationCmdArray(Read_FSP8IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP9IndicationCmd_FSPIndicationCmdArray(Read_FSP9IndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FSP_BIndicationCmd_FSPIndicationCmdArray(Read_FSP_BIndicationCmd_FSPIndicationCmdArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Read_FspNV_PR_FspNV(Read_FspNV_PR_FspNV);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL1_FSPIndicationCmd(Rte_InitValue_FSP1IndicationCmdL1_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL2_FSPIndicationCmd(Rte_InitValue_FSP1IndicationCmdL2_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL3_FSPIndicationCmd(Rte_InitValue_FSP1IndicationCmdL3_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL4_FSPIndicationCmd(Rte_InitValue_FSP1IndicationCmdL4_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1IndicationCmdL5_FSPIndicationCmd(Rte_InitValue_FSP1IndicationCmdL5_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP1SwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP1SwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP1SwitchStatus_FSPSwitchStatusArray(Write_FSP1SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP2IndicationCmdL1_FSPIndicationCmd(Rte_InitValue_FSP2IndicationCmdL1_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP2IndicationCmdL2_FSPIndicationCmd(Rte_InitValue_FSP2IndicationCmdL2_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP2IndicationCmdL3_FSPIndicationCmd(Rte_InitValue_FSP2IndicationCmdL3_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP2SwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP2SwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP2SwitchStatus_FSPSwitchStatusArray(Write_FSP2SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP3IndicationCmdL2_FSPIndicationCmd(Rte_InitValue_FSP3IndicationCmdL2_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP3SwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP3SwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP3SwitchStatus_FSPSwitchStatusArray(Write_FSP3SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP4IndicationCmdL2_FSPIndicationCmd(Rte_InitValue_FSP4IndicationCmdL2_FSPIndicationCmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP4SwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP4SwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP4SwitchStatus_FSPSwitchStatusArray(Write_FSP4SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP5SwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP5SwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP5SwitchStatus_FSPSwitchStatusArray(Write_FSP5SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP6SwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP6SwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP6SwitchStatus_FSPSwitchStatusArray(Write_FSP6SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP7SwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP7SwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP7SwitchStatus_FSPSwitchStatusArray(Write_FSP7SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP8SwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP8SwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP8SwitchStatus_FSPSwitchStatusArray(Write_FSP8SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP9SwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP9SwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP9SwitchStatus_FSPSwitchStatusArray(Write_FSP9SwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FSP_BSwitchStatus_FSPSwitchStatusArray, 0, sizeof(Write_FSP_BSwitchStatus_FSPSwitchStatusArray));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FSP_BSwitchStatus_FSPSwitchStatusArray(Write_FSP_BSwitchStatus_FSPSwitchStatusArray);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&Write_FspNV_PR_FspNV, 0, sizeof(Write_FspNV_PR_FspNV));
  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_FspNV_PR_FspNV(Write_FspNV_PR_FspNV);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_Living12VResetRequest_Living12VResetRequest(Rte_InitValue_Living12VResetRequest_Living12VResetRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Write_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted(Rte_InitValue_isFlexiblePanelsAssignmentCompleted_isFlexiblePanelsAssignmentCompleted);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl();
  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD);
  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvRead_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure);

  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_IOCTL_FspLinCtrl(0U);
  (void)memset(&FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD_Write, 0, sizeof(FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD_Write));
  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD(FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspByCCNAD_Write);
  (void)memset(&FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure_Write, 0, sizeof(FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure_Write));
  TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_IrvWrite_FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure(FlexibleSwitchPanel_LINMastCtrl_20ms_runnable_Irv_NbOfFspInFailure_Write);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <RequestResults> of PortPrototype <RoutineServices_R1AAI_FspAssignmentRoutine>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data7ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAI_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  LinDiagServiceStatus Call_CddLinDiagServices_FSPAssignResp_pDiagServiceStatus = 0U;
  uint8 Call_CddLinDiagServices_FSPAssignResp_pAvailableFSPCount = 0U;
  uint8 Call_CddLinDiagServices_FSPAssignResp_pFspErrorStatus = 0U;
  uint8 Call_CddLinDiagServices_FSPAssignResp_pFspNvData = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_CddLinDiagServices_FSPAssignReq(0U, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_PENDING:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_CddLinDiagServices_FSPAssignResp(&Call_CddLinDiagServices_FSPAssignResp_pDiagServiceStatus, &Call_CddLinDiagServices_FSPAssignResp_pAvailableFSPCount, &Call_CddLinDiagServices_FSPAssignResp_pFspErrorStatus, &Call_CddLinDiagServices_FSPAssignResp_pFspNvData);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_PENDING:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_E_NOT_OK:
      fct_error = 1;
      break;
  }

  return RTE_E_RoutineServices_R1AAI_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAI_FspAssignmentRoutine_Start
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Start> of PortPrototype <RoutineServices_R1AAI_FspAssignmentRoutine>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, uint8 *Out_Common_Diagnostics_DataRecord, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Out_Common_Diagnostics_DataRecord: uint8* is of type Dcm_Data2ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAI_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Start_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Start (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  LinDiagServiceStatus Call_CddLinDiagServices_FSPAssignResp_pDiagServiceStatus = 0U;
  uint8 Call_CddLinDiagServices_FSPAssignResp_pAvailableFSPCount = 0U;
  uint8 Call_CddLinDiagServices_FSPAssignResp_pFspErrorStatus = 0U;
  uint8 Call_CddLinDiagServices_FSPAssignResp_pFspNvData = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_CddLinDiagServices_FSPAssignReq(0U, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_PENDING:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_CddLinDiagServices_FSPAssignResp(&Call_CddLinDiagServices_FSPAssignResp_pDiagServiceStatus, &Call_CddLinDiagServices_FSPAssignResp_pAvailableFSPCount, &Call_CddLinDiagServices_FSPAssignResp_pFspErrorStatus, &Call_CddLinDiagServices_FSPAssignResp_pFspNvData);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_PENDING:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_E_NOT_OK:
      fct_error = 1;
      break;
  }

  return RTE_E_RoutineServices_R1AAI_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: RoutineServices_R1AAI_FspAssignmentRoutine_Stop
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <Stop> of PortPrototype <RoutineServices_R1AAI_FspAssignmentRoutine>
 *
 **********************************************************************************************************************
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignReq(LinDiagBusInfo LinBusInfo, LinDiagRequest_T RequestType)
 *     Synchronous Server Invocation. Timeout: None
 *   Std_ReturnType Rte_Call_CddLinDiagServices_FSPAssignResp(LinDiagServiceStatus *pDiagServiceStatus, uint8 *pAvailableFSPCount, uint8 *pFspErrorStatus, uint8 *pFspNvData)
 *     Synchronous Server Invocation. Timeout: None
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType RoutineServices_R1AAI_FspAssignmentRoutine_Stop(Dcm_OpStatusType OpStatus, Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP
 *   RTE_E_RoutineServices_R1AAI_DCM_E_PENDING
 *   RTE_E_RoutineServices_R1AAI_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Stop_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_LINMstr_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: RoutineServices_R1AAI_FspAssignmentRoutine_Stop (returns application error)
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_FSPConfigSettingsLIN2_P1EW0_T P1EW0_FSPConfigSettingsLIN2_v_data;
  SEWS_FSPConfigSettingsLIN3_P1EW1_T P1EW1_FSPConfigSettingsLIN3_v_data;
  SEWS_FSPConfigSettingsLIN4_P1EW2_T P1EW2_FSPConfigSettingsLIN4_v_data;
  SEWS_FSPConfigSettingsLIN5_P1EW3_T P1EW3_FSPConfigSettingsLIN5_v_data;
  SEWS_FSPConfigSettingsLIN1_P1EWZ_T P1EWZ_FSPConfigSettingsLIN1_v_data;

  LinDiagServiceStatus Call_CddLinDiagServices_FSPAssignResp_pDiagServiceStatus = 0U;
  uint8 Call_CddLinDiagServices_FSPAssignResp_pAvailableFSPCount = 0U;
  uint8 Call_CddLinDiagServices_FSPAssignResp_pFspErrorStatus = 0U;
  uint8 Call_CddLinDiagServices_FSPAssignResp_pFspNvData = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1EW0_FSPConfigSettingsLIN2_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW0_FSPConfigSettingsLIN2_v();
  P1EW1_FSPConfigSettingsLIN3_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW1_FSPConfigSettingsLIN3_v();
  P1EW2_FSPConfigSettingsLIN4_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW2_FSPConfigSettingsLIN4_v();
  P1EW3_FSPConfigSettingsLIN5_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EW3_FSPConfigSettingsLIN5_v();
  P1EWZ_FSPConfigSettingsLIN1_v_data = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Prm_P1EWZ_FSPConfigSettingsLIN1_v();

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_CddLinDiagServices_FSPAssignReq(0U, 0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_PENDING:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_FlexibleSwitchesRouter_Ctrl_LINMstr_Rte_Call_CddLinDiagServices_FSPAssignResp(&Call_CddLinDiagServices_FSPAssignResp_pDiagServiceStatus, &Call_CddLinDiagServices_FSPAssignResp_pAvailableFSPCount, &Call_CddLinDiagServices_FSPAssignResp_pFspErrorStatus, &Call_CddLinDiagServices_FSPAssignResp_pFspNvData);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_DCM_E_PENDING:
      fct_error = 1;
      break;
    case RTE_E_RoutineServices_R1AAI_E_NOT_OK:
      fct_error = 1;
      break;
  }

  return RTE_E_RoutineServices_R1AAI_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define FlexibleSwitchesRouter_Ctrl_LINMstr_STOP_SEC_CODE
#include "FlexibleSwitchesRouter_Ctrl_LINMstr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void FlexibleSwitchesRouter_Ctrl_LINMstr_TestDefines(void)
{
  /* Enumeration Data Types */

  ComMode_LIN_Type Test_ComMode_LIN_Type_V_1 = Inactive;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_2 = Diagnostic;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_3 = SwitchDetection;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_4 = ApplicationMonitoring;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_5 = Calibration;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_6 = Spare1;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_7 = Error;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_8 = NotAvailable;

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dcm_OpStatusType Test_Dcm_OpStatusType_V_1 = DCM_INITIAL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_2 = DCM_PENDING;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_3 = DCM_CANCEL;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_4 = DCM_FORCE_RCRRP_OK;
  Dcm_OpStatusType Test_Dcm_OpStatusType_V_5 = DCM_FORCE_RCRRP_NOT_OK;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_1 = DEM_INIT_MONITOR_CLEAR;
  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_2 = DEM_INIT_MONITOR_RESTART;
  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_3 = DEM_INIT_MONITOR_REENABLED;
  Dem_InitMonitorReasonType Test_Dem_InitMonitorReasonType_V_4 = DEM_INIT_MONITOR_STORAGE_REENABLED;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DiagActiveState_T Test_DiagActiveState_T_V_1 = Diag_Active_FALSE;
  DiagActiveState_T Test_DiagActiveState_T_V_2 = Diag_Active_TRUE;

  LinDiagBusInfo Test_LinDiagBusInfo_V_1 = None;
  LinDiagBusInfo Test_LinDiagBusInfo_V_2 = LinDiag_BUS1;
  LinDiagBusInfo Test_LinDiagBusInfo_V_3 = LinDiag_BUS2;
  LinDiagBusInfo Test_LinDiagBusInfo_V_4 = LinDiag_BUS3;
  LinDiagBusInfo Test_LinDiagBusInfo_V_5 = LinDiag_BUS4;
  LinDiagBusInfo Test_LinDiagBusInfo_V_6 = LinDiag_BUS5;
  LinDiagBusInfo Test_LinDiagBusInfo_V_7 = LinDiag_SpareBUS1;
  LinDiagBusInfo Test_LinDiagBusInfo_V_8 = LinDiag_SpareBUS2;
  LinDiagBusInfo Test_LinDiagBusInfo_V_9 = LinDiag_ALL_BUSSES;

  LinDiagRequest_T Test_LinDiagRequest_T_V_1 = NO_OPEARATION;
  LinDiagRequest_T Test_LinDiagRequest_T_V_2 = START_OPEARATION;
  LinDiagRequest_T Test_LinDiagRequest_T_V_3 = STOP_OPERATION;

  LinDiagServiceStatus Test_LinDiagServiceStatus_V_1 = LinDiagService_None;
  LinDiagServiceStatus Test_LinDiagServiceStatus_V_2 = LinDiagService_Pending;
  LinDiagServiceStatus Test_LinDiagServiceStatus_V_3 = LinDiagService_Completed;
  LinDiagServiceStatus Test_LinDiagServiceStatus_V_4 = LinDiagService_Error;

  PushButtonStatus_T Test_PushButtonStatus_T_IV_1 = InvalidValue_PushButtonStatus_T;
  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
