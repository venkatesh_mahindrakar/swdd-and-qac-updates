/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Cdd_LinTpHandling.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Client server interfaces */
Std_ReturnType TSC_Cdd_LinTpHandling_Rte_Call_CddLinRxHandling_ReceiveIndication(uint8 TxId, uint8 SubServiceId, uint8 *RxData, uint8 Length, CddLinTp_Status Status);

/** Mode switches */
uint8 TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(void);
uint8 TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(void);
uint8 TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(void);
uint8 TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(void);
uint8 TSC_Cdd_LinTpHandling_Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(void);




