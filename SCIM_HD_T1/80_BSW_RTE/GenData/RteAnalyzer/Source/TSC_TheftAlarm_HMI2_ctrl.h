/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_TheftAlarm_HMI2_ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_AlarmStatus_stat_AlarmStatus_stat(AlarmStatus_stat_T *data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(KeyAuthentication_stat_decrypt_T *data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyPosition_KeyPosition(KeyPosition_T *data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyfobSuperLockButton_Status_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_ReducedSetModeButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_SwcActivation_Parked_Parked(VehicleModeDistribution_T *data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_DevInd_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_rqst_CryptTrig_CryptoTrigger(Boolean data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt(ReducedSetMode_rqst_decrypt_T data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized(const uint8 *data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger(Boolean data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt(TheftAlarmAct_rqst_decrypt_I data);
Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized(const uint8 *data);

/** Calibration Component Calibration Parameters */
SEWS_TheftAlarmRequestDuration_X1CY8_T  TSC_TheftAlarm_HMI2_ctrl_Rte_Prm_X1CY8_TheftAlarmRequestDuration_v(void);
boolean  TSC_TheftAlarm_HMI2_ctrl_Rte_Prm_P1B2T_AlarmInstalled_v(void);




