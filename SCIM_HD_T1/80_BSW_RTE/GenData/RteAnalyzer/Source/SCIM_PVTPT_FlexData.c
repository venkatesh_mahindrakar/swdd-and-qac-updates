/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  SCIM_PVTPT_FlexData.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  SCIM_PVTPT_FlexData
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <SCIM_PVTPT_FlexData>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_SCIM_PVTPT_FlexData.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_SCIM_PVTPT_FlexData.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void SCIM_PVTPT_FlexData_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * Debug_PVT_SCIM_FlexArrayData1: Integer in interval [0...31]
 * uint16: Integer in interval [0...65535] (standard type)
 * uint32: Integer in interval [0...4294967295] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Debug_PVT_FlexDataRequest: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Ctr_CyclicReport (0U)
 *   Cx1_Universal_debug_trace (1U)
 *   Cx2_Keyfob_RF_data1 (2U)
 *   Cx3_Keyfob_RF_data2 (3U)
 *   Cx4_Keyfob_LF_data1 (4U)
 *   Cx5_Keyfob_LF_data2 (5U)
 *   Cx6_SW_execution_statistics (6U)
 *   Cx7_SW_state_statistics (7U)
 * Debug_PVT_LF_Trig: Enumeration of integer in interval [0...3] with enumerators
 *   Cx0_Idle (0U)
 *   Cx1_P1 (1U)
 *   Cx2_P2 (2U)
 *   Cx3_P3 (3U)
 * Debug_PVT_SCIM_FlexArrayDataId: Enumeration of integer in interval [0...7] with enumerators
 *   Cx0_Predefined_Cyclic_Report (0U)
 *   Cx1_Universal_debug_trace (1U)
 *   Cx2_Keyfob_RF_data1 (2U)
 *   Cx3_Keyfob_RF_data2 (3U)
 *   Cx4_Keyfob_LF_data1 (4U)
 *   Cx5_Keyfob_LF_data2 (5U)
 *   Cx6_SW_execution_statistics (6U)
 *   Cx7_SW_state_statistics (7U)
 *
 * Array Types:
 * ============
 * Debug_PVT_SCIM_FlexArrayData: Array with 7 element(s) of type uint8
 *
 * Record Types:
 * =============
 * ButtonStatus: Record with elements
 *   Button1ID of type uint8
 *   Button1PressCounter of type uint8
 *   Button1PressTime of type uint16
 *   Button2ID of type uint8
 *   Button2PressCounter of type uint8
 *   Button2PressTime of type uint16
 * LfRssi: Record with elements
 *   AntennaPi of type uint16
 *   AntennaP1 of type uint16
 *   AntennaP2 of type uint16
 *   AntennaP3 of type uint16
 *   AntennaP4 of type uint16
 *
 *********************************************************************************************************************/


#define SCIM_PVTPT_FlexData_START_SEC_CODE
#include "SCIM_PVTPT_FlexData_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Runnable_PVTPT_Flex_Report
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData(const uint8 *data)
 *     Argument data: uint8* is of type Debug_PVT_SCIM_FlexArrayData
 *   Std_ReturnType Rte_Write_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1(Debug_PVT_SCIM_FlexArrayData1 data)
 *   Std_ReturnType Rte_Write_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId(Debug_PVT_SCIM_FlexArrayDataId data)
 *
 * Client/Server Interfaces:
 * =========================
 *   Server Invocation:
 *   ------------------
 *   Std_ReturnType Rte_Call_LfInterface_P_GetLfAntState(LfRssi *LfRssiStatus, uint8 *FobFound, uint8 *FobLocation)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_LfInterface_I_RadioApplicationError
 *   Std_ReturnType Rte_Call_RkeInterface_P_GetFobRkeState(ButtonStatus *RkeButton, uint16 *FobID, uint32 *FobSN, uint16 *FobRollingCounter, uint16 *ScimRollingCounter, uint8 *FobBattery)
 *     Synchronous Server Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_RkeInterface_I_RadioApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_Flex_Report_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PVTPT_FlexData_CODE) Runnable_PVTPT_Flex_Report(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_Flex_Report
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Debug_PVT_SCIM_FlexArrayData Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData;

  LfRssi Call_LfInterface_P_GetLfAntState_LfRssiStatus = {
  0U, 0U, 0U, 0U, 0U
};
  uint8 Call_LfInterface_P_GetLfAntState_FobFound = 0U;
  uint8 Call_LfInterface_P_GetLfAntState_FobLocation = 0U;
  ButtonStatus Call_RkeInterface_P_GetFobRkeState_RkeButton = {
  0U, 0U, 0U, 0U, 0U, 0U
};
  uint16 Call_RkeInterface_P_GetFobRkeState_FobID = 0U;
  uint32 Call_RkeInterface_P_GetFobRkeState_FobSN = 0U;
  uint16 Call_RkeInterface_P_GetFobRkeState_FobRollingCounter = 0U;
  uint16 Call_RkeInterface_P_GetFobRkeState_ScimRollingCounter = 0U;
  uint8 Call_RkeInterface_P_GetFobRkeState_FobBattery = 0U;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  (void)memset(&Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData, 0, sizeof(Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData));
  fct_status = TSC_SCIM_PVTPT_FlexData_Rte_Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData(Write_Debug_PVT_SCIM_FlexArrayData_Debug_PVT_SCIM_FlexArrayData);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_SCIM_PVTPT_FlexData_Rte_Write_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1(Rte_InitValue_Debug_PVT_SCIM_FlexArrayData1_Debug_PVT_SCIM_FlexArrayData1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_SCIM_PVTPT_FlexData_Rte_Write_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId(Rte_InitValue_Debug_PVT_SCIM_FlexArrayDataId_Debug_PVT_SCIM_FlexArrayDataId);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_SCIM_PVTPT_FlexData_Rte_Call_LfInterface_P_GetLfAntState(&Call_LfInterface_P_GetLfAntState_LfRssiStatus, &Call_LfInterface_P_GetLfAntState_FobFound, &Call_LfInterface_P_GetLfAntState_FobLocation);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_LfInterface_I_RadioApplicationError:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SCIM_PVTPT_FlexData_Rte_Call_RkeInterface_P_GetFobRkeState(&Call_RkeInterface_P_GetFobRkeState_RkeButton, &Call_RkeInterface_P_GetFobRkeState_FobID, &Call_RkeInterface_P_GetFobRkeState_FobSN, &Call_RkeInterface_P_GetFobRkeState_FobRollingCounter, &Call_RkeInterface_P_GetFobRkeState_ScimRollingCounter, &Call_RkeInterface_P_GetFobRkeState_FobBattery);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_RkeInterface_I_RadioApplicationError:
      fct_error = 1;
      break;
  }

  SCIM_PVTPT_FlexData_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Runnable_PVTPT_Flex_Request
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest(Debug_PVT_FlexDataRequest *data)
 *   Std_ReturnType Rte_Read_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig(Debug_PVT_LF_Trig *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_Flex_Request_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, SCIM_PVTPT_FlexData_CODE) Runnable_PVTPT_Flex_Request(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Runnable_PVTPT_Flex_Request
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  Debug_PVT_FlexDataRequest Read_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest;
  Debug_PVT_LF_Trig Read_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  fct_status = TSC_SCIM_PVTPT_FlexData_Rte_Read_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest(&Read_Debug_PVT_Flex_Request_Debug_PVT_FlexDataRequest);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_SCIM_PVTPT_FlexData_Rte_Read_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig(&Read_Debug_PVT_LF_Trig_Debug_PVT_LF_Trig);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define SCIM_PVTPT_FlexData_STOP_SEC_CODE
#include "SCIM_PVTPT_FlexData_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void SCIM_PVTPT_FlexData_TestDefines(void)
{
  /* Enumeration Data Types */

  Debug_PVT_FlexDataRequest Test_Debug_PVT_FlexDataRequest_V_1 = Cx0_Ctr_CyclicReport;
  Debug_PVT_FlexDataRequest Test_Debug_PVT_FlexDataRequest_V_2 = Cx1_Universal_debug_trace;
  Debug_PVT_FlexDataRequest Test_Debug_PVT_FlexDataRequest_V_3 = Cx2_Keyfob_RF_data1;
  Debug_PVT_FlexDataRequest Test_Debug_PVT_FlexDataRequest_V_4 = Cx3_Keyfob_RF_data2;
  Debug_PVT_FlexDataRequest Test_Debug_PVT_FlexDataRequest_V_5 = Cx4_Keyfob_LF_data1;
  Debug_PVT_FlexDataRequest Test_Debug_PVT_FlexDataRequest_V_6 = Cx5_Keyfob_LF_data2;
  Debug_PVT_FlexDataRequest Test_Debug_PVT_FlexDataRequest_V_7 = Cx6_SW_execution_statistics;
  Debug_PVT_FlexDataRequest Test_Debug_PVT_FlexDataRequest_V_8 = Cx7_SW_state_statistics;

  Debug_PVT_LF_Trig Test_Debug_PVT_LF_Trig_V_1 = Cx0_Idle;
  Debug_PVT_LF_Trig Test_Debug_PVT_LF_Trig_V_2 = Cx1_P1;
  Debug_PVT_LF_Trig Test_Debug_PVT_LF_Trig_V_3 = Cx2_P2;
  Debug_PVT_LF_Trig Test_Debug_PVT_LF_Trig_V_4 = Cx3_P3;

  Debug_PVT_SCIM_FlexArrayDataId Test_Debug_PVT_SCIM_FlexArrayDataId_V_1 = Cx0_Predefined_Cyclic_Report;
  Debug_PVT_SCIM_FlexArrayDataId Test_Debug_PVT_SCIM_FlexArrayDataId_V_2 = Cx1_Universal_debug_trace;
  Debug_PVT_SCIM_FlexArrayDataId Test_Debug_PVT_SCIM_FlexArrayDataId_V_3 = Cx2_Keyfob_RF_data1;
  Debug_PVT_SCIM_FlexArrayDataId Test_Debug_PVT_SCIM_FlexArrayDataId_V_4 = Cx3_Keyfob_RF_data2;
  Debug_PVT_SCIM_FlexArrayDataId Test_Debug_PVT_SCIM_FlexArrayDataId_V_5 = Cx4_Keyfob_LF_data1;
  Debug_PVT_SCIM_FlexArrayDataId Test_Debug_PVT_SCIM_FlexArrayDataId_V_6 = Cx5_Keyfob_LF_data2;
  Debug_PVT_SCIM_FlexArrayDataId Test_Debug_PVT_SCIM_FlexArrayDataId_V_7 = Cx6_SW_execution_statistics;
  Debug_PVT_SCIM_FlexArrayDataId Test_Debug_PVT_SCIM_FlexArrayDataId_V_8 = Cx7_SW_state_statistics;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
