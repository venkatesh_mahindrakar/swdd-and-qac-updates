/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  IoHwAb_ASIL_Core.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  IoHwAb_ASIL_Core
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <IoHwAb_ASIL_Core>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * IOHWAB_UINT8
 *   
 *
 * PcbPopulatedInfo_T
 *   
 *
 * SEWS_Diag_Act_DOWHS01_P1V6O_T
 *   
 *
 * SEWS_Diag_Act_DOWHS02_P1V6P_T
 *   
 *
 * SEWS_Diag_Act_DOWLS02_P1V7E_T
 *   
 *
 * SEWS_Diag_Act_DOWLS03_P1V7F_T
 *   
 *
 * SEWS_FSC_TimeoutThreshold_X1CZR_T
 *   
 *
 * SEWS_P1QR6_Threshold_VAT_T
 *   
 *
 * SEWS_P1QR6_Threshold_VBT_T
 *   
 *
 * SEWS_P1QR6_Threshold_VOR_T
 *   
 *
 * SEWS_PcbConfig_Adi_X1CXW_T
 *   
 *
 * SEWS_PcbConfig_DOBHS_X1CXX_T
 *   
 *
 * SEWS_PcbConfig_DOWHS_X1CXY_T
 *   
 *
 * SEWS_PcbConfig_DOWLS_X1CXZ_T
 *   
 *
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T
 *   
 *
 * SEWS_X1CZQ_Operating_T
 *   
 *
 * SEWS_X1CZQ_Protecting_T
 *   
 *
 * SEWS_X1CZQ_Reduced_T
 *   
 *
 * SEWS_X1CZQ_ShutdownReady_T
 *   
 *
 * SEWS_X1CZQ_Withstand_T
 *   
 *
 * VGTT_EcuPinFaultStatus
 *   
 *
 * VGTT_EcuPinVoltage_0V2
 *   
 *
 *********************************************************************************************************************/

#include "Rte_IoHwAb_ASIL_Core.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_IoHwAb_ASIL_Core.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void IoHwAb_ASIL_Core_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * IOHWAB_UINT8: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS01_P1V6O_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWHS02_P1V6P_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS02_P1V7E_T: Integer in interval [0...255]
 * SEWS_Diag_Act_DOWLS03_P1V7F_T: Integer in interval [0...255]
 * SEWS_FSC_TimeoutThreshold_X1CZR_T: Integer in interval [0...255]
 * SEWS_P1QR6_Threshold_VAT_T: Integer in interval [0...255]
 * SEWS_P1QR6_Threshold_VBT_T: Integer in interval [0...255]
 * SEWS_P1QR6_Threshold_VOR_T: Integer in interval [0...255]
 * SEWS_PcbConfig_DoorAccessIf_X1CX3_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Operating_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Protecting_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Reduced_T: Integer in interval [0...255]
 * SEWS_X1CZQ_ShutdownReady_T: Integer in interval [0...255]
 * SEWS_X1CZQ_Withstand_T: Integer in interval [0...255]
 * VGTT_EcuPinVoltage_0V2: Integer in interval [0...255]
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * Fsc_OperationalMode_T: Enumeration of integer in interval [0...255] with enumerators
 *   FSC_ShutdownReady (0U)
 *   FSC_Reduced_12vDcDcLimit (1U)
 *   FSC_Reduced (2U)
 *   FSC_Operating (3U)
 *   FSC_Protecting (4U)
 *   FSC_Withstand (5U)
 *   FSC_NotAvailable (6U)
 * PcbPopulatedInfo_T: Enumeration of integer in interval [0...255] with enumerators
 *   NotPopulated (0U)
 *   Populated (1U)
 * SEWS_PcbConfig_Adi_X1CXW_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated (0U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration (1U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration (2U)
 *   SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration (3U)
 * SEWS_PcbConfig_DOBHS_X1CXX_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOBHS_X1CXX_T_Populated (1U)
 * SEWS_PcbConfig_DOWHS_X1CXY_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWHS_X1CXY_T_Populated (1U)
 * SEWS_PcbConfig_DOWLS_X1CXZ_T: Enumeration of integer in interval [0...255] with enumerators
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated (0U)
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated (1U)
 * VGTT_EcuPinFaultStatus: Enumeration of integer in interval [0...255] with enumerators
 *   TestNotRun (0U)
 *   OffState_NoFaultDetected (16U)
 *   OffState_FaultDetected_STG (17U)
 *   OffState_FaultDetected_STB (18U)
 *   OffState_FaultDetected_OC (19U)
 *   OffState_FaultDetected_VBT (22U)
 *   OffState_FaultDetected_VAT (23U)
 *   OnState_NoFaultDetected (32U)
 *   OnState_FaultDetected_STG (33U)
 *   OnState_FaultDetected_STB (34U)
 *   OnState_FaultDetected_OC (35U)
 *   OnState_FaultDetected_VBT (38U)
 *   OnState_FaultDetected_VAT (39U)
 *   OnState_FaultDetected_VOR (41U)
 *   OnState_FaultDetected_CAT (44U)
 *
 * Array Types:
 * ============
 * EcuHwVoltageValues_T: Array with 40 element(s) of type VGTT_EcuPinVoltage_0V2
 * IoAsilCorePcbConfig_T: Array with 40 element(s) of type PcbPopulatedInfo_T
 * SEWS_PcbConfig_Adi_X1CXW_a_T: Array with 19 element(s) of type SEWS_PcbConfig_Adi_X1CXW_T
 * SEWS_PcbConfig_DOBHS_X1CXX_a_T: Array with 4 element(s) of type SEWS_PcbConfig_DOBHS_X1CXX_T
 * SEWS_PcbConfig_DOWHS_X1CXY_a_T: Array with 2 element(s) of type SEWS_PcbConfig_DOWHS_X1CXY_T
 * SEWS_PcbConfig_DOWLS_X1CXZ_a_T: Array with 3 element(s) of type SEWS_PcbConfig_DOWLS_X1CXZ_T
 *
 * Record Types:
 * =============
 * SEWS_FSC_VoltageThreshold_X1CZQ_s_T: Record with elements
 *   ShutdownReady of type SEWS_X1CZQ_ShutdownReady_T
 *   Reduced of type SEWS_X1CZQ_Reduced_T
 *   Operating of type SEWS_X1CZQ_Operating_T
 *   Protecting of type SEWS_X1CZQ_Protecting_T
 *   Withstand of type SEWS_X1CZQ_Withstand_T
 * SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T: Record with elements
 *   Threshold_VBT of type SEWS_P1QR6_Threshold_VBT_T
 *   Threshold_VAT of type SEWS_P1QR6_Threshold_VAT_T
 *   Threshold_VOR of type SEWS_P1QR6_Threshold_VOR_T
 * SEWS_PcbConfig_AdiPullUp_X1CX5_s_T: Record with elements
 *   AdiPullupLiving of type boolean
 *   AdiPullupParked of type boolean
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   SEWS_PcbConfig_DoorAccessIf_X1CX3_T Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v(void)
 *   SEWS_FSC_TimeoutThreshold_X1CZR_T Rte_Prm_X1CZR_FSC_TimeoutThreshold_v(void)
 *   SEWS_PcbConfig_Adi_X1CXW_T *Rte_Prm_X1CXW_PcbConfig_Adi_v(void)
 *     Returnvalue: SEWS_PcbConfig_Adi_X1CXW_T* is of type SEWS_PcbConfig_Adi_X1CXW_a_T
 *   SEWS_PcbConfig_DOBHS_X1CXX_T *Rte_Prm_X1CXX_PcbConfig_DOBHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOBHS_X1CXX_T* is of type SEWS_PcbConfig_DOBHS_X1CXX_a_T
 *   SEWS_PcbConfig_DOWHS_X1CXY_T *Rte_Prm_X1CXY_PcbConfig_DOWHS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWHS_X1CXY_T* is of type SEWS_PcbConfig_DOWHS_X1CXY_a_T
 *   SEWS_PcbConfig_DOWLS_X1CXZ_T *Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(void)
 *     Returnvalue: SEWS_PcbConfig_DOWLS_X1CXZ_T* is of type SEWS_PcbConfig_DOWLS_X1CXZ_a_T
 *   SEWS_PcbConfig_AdiPullUp_X1CX5_s_T *Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v(void)
 *   SEWS_FSC_VoltageThreshold_X1CZQ_s_T *Rte_Prm_X1CZQ_FSC_VoltageThreshold_v(void)
 *   SEWS_Diag_Act_DOWHS01_P1V6O_T Rte_Prm_P1V6O_Diag_Act_DOWHS01_v(void)
 *   SEWS_Diag_Act_DOWHS02_P1V6P_T Rte_Prm_P1V6P_Diag_Act_DOWHS02_v(void)
 *   SEWS_Diag_Act_DOWLS02_P1V7E_T Rte_Prm_P1V7E_Diag_Act_DOWLS02_v(void)
 *   SEWS_Diag_Act_DOWLS03_P1V7F_T Rte_Prm_P1V7F_Diag_Act_DOWLS03_v(void)
 *   SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T *Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v(void)
 *
 *********************************************************************************************************************/


#define IoHwAb_ASIL_Core_START_SEC_CODE
#include "IoHwAb_ASIL_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CoreHW_ASIL_AdcCtrl_10ms_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated(PcbPopulatedInfo_T *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues(const VGTT_EcuPinVoltage_0V2 *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_AdcCtrl_10ms_Runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, IoHwAb_ASIL_Core_CODE) CoreHW_ASIL_AdcCtrl_10ms_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_AdcCtrl_10ms_Runnable
 *********************************************************************************************************************/

  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_FSC_TimeoutThreshold_X1CZR_T X1CZR_FSC_TimeoutThreshold_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_FSC_VoltageThreshold_X1CZQ_s_T X1CZQ_FSC_VoltageThreshold_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T P1QR6_HWIO_CfgFault_PWR24V_v_data;

  IoAsilCorePcbConfig_T CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated;

  EcuHwVoltageValues_T CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues_Write;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  X1CZR_FSC_TimeoutThreshold_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZR_FSC_TimeoutThreshold_v();
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CZQ_FSC_VoltageThreshold_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZQ_FSC_VoltageThreshold_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1QR6_HWIO_CfgFault_PWR24V_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v();

  TSC_IoHwAb_ASIL_Core_Rte_IrvRead_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated(CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated);

  (void)memset(&CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues_Write, 0, sizeof(CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues_Write));
  TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues(CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues_Write);

  IoHwAb_ASIL_Core_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CoreHW_ASIL_AdcCtrl_Init_Runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed once after the RTE is started
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvEcuVoltageValues(const VGTT_EcuPinVoltage_0V2 *data)
 *   void Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated(const PcbPopulatedInfo_T *data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_AdcCtrl_Init_Runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, IoHwAb_ASIL_Core_CODE) CoreHW_ASIL_AdcCtrl_Init_Runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_AdcCtrl_Init_Runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_FSC_TimeoutThreshold_X1CZR_T X1CZR_FSC_TimeoutThreshold_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_FSC_VoltageThreshold_X1CZQ_s_T X1CZQ_FSC_VoltageThreshold_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T P1QR6_HWIO_CfgFault_PWR24V_v_data;

  EcuHwVoltageValues_T CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvEcuVoltageValues_Write;
  IoAsilCorePcbConfig_T CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated_Write;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  X1CZR_FSC_TimeoutThreshold_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZR_FSC_TimeoutThreshold_v();
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CZQ_FSC_VoltageThreshold_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZQ_FSC_VoltageThreshold_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1QR6_HWIO_CfgFault_PWR24V_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v();

  fct_status = TSC_IoHwAb_ASIL_Core_Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode(Rte_InitValue_Fsc_OperationalMode_P_Fsc_OperationalMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  (void)memset(&CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvEcuVoltageValues_Write, 0, sizeof(CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvEcuVoltageValues_Write));
  TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvEcuVoltageValues(CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvEcuVoltageValues_Write);
  (void)memset(&CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated_Write, 0, sizeof(CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated_Write));
  TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated(CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated_Write);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: CoreHW_ASIL_VbatProcess_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 **********************************************************************************************************************
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode(Fsc_OperationalMode_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_FSCMode(IOHWAB_UINT8 data)
 *   void Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvBatteryFaultStatus(VGTT_EcuPinFaultStatus data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_VbatProcess_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, IoHwAb_ASIL_Core_CODE) CoreHW_ASIL_VbatProcess_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: CoreHW_ASIL_VbatProcess_10ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_FSC_TimeoutThreshold_X1CZR_T X1CZR_FSC_TimeoutThreshold_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_FSC_VoltageThreshold_X1CZQ_s_T X1CZQ_FSC_VoltageThreshold_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T P1QR6_HWIO_CfgFault_PWR24V_v_data;

  EcuHwVoltageValues_T CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  X1CZR_FSC_TimeoutThreshold_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZR_FSC_TimeoutThreshold_v();
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CZQ_FSC_VoltageThreshold_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZQ_FSC_VoltageThreshold_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1QR6_HWIO_CfgFault_PWR24V_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v();

  fct_status = TSC_IoHwAb_ASIL_Core_Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode(Rte_InitValue_Fsc_OperationalMode_P_Fsc_OperationalMode);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  TSC_IoHwAb_ASIL_Core_Rte_IrvRead_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues(CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues);

  TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_FSCMode(0U);
  TSC_IoHwAb_ASIL_Core_Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvBatteryFaultStatus(0U);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: EcuHwState_P_GetEcuVoltages_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetEcuVoltages_CS> of PortPrototype <EcuHwState_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   void Rte_IrvRead_EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType EcuHwState_P_GetEcuVoltages_CS(VGTT_EcuPinVoltage_0V2 *EcuVoltageValues)
 *     Argument EcuVoltageValues: VGTT_EcuPinVoltage_0V2* is of type EcuHwVoltageValues_T
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_EcuHwState_I_AdcInFailure
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: EcuHwState_P_GetEcuVoltages_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Core_CODE) EcuHwState_P_GetEcuVoltages_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) EcuVoltageValues) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: EcuHwState_P_GetEcuVoltages_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_FSC_TimeoutThreshold_X1CZR_T X1CZR_FSC_TimeoutThreshold_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_FSC_VoltageThreshold_X1CZQ_s_T X1CZQ_FSC_VoltageThreshold_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T P1QR6_HWIO_CfgFault_PWR24V_v_data;

  EcuHwVoltageValues_T EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  X1CZR_FSC_TimeoutThreshold_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZR_FSC_TimeoutThreshold_v();
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CZQ_FSC_VoltageThreshold_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZQ_FSC_VoltageThreshold_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1QR6_HWIO_CfgFault_PWR24V_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v();

  TSC_IoHwAb_ASIL_Core_Rte_IrvRead_EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues(EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: VbatInterface_P_GetVbatVoltage_CS
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <GetVbatVoltage_CS> of PortPrototype <VbatInterface_P>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   VGTT_EcuPinFaultStatus Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvBatteryFaultStatus(void)
 *   void Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues(VGTT_EcuPinVoltage_0V2 *data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType VbatInterface_P_GetVbatVoltage_CS(VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_VbatInterface_I_AdcInFailure
 *   RTE_E_VbatInterface_I_IoHwAbApplicationError
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: VbatInterface_P_GetVbatVoltage_CS_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, IoHwAb_ASIL_Core_CODE) VbatInterface_P_GetVbatVoltage_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) FaultStatus) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: VbatInterface_P_GetVbatVoltage_CS (returns application error)
 *********************************************************************************************************************/

  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_FSC_TimeoutThreshold_X1CZR_T X1CZR_FSC_TimeoutThreshold_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_FSC_VoltageThreshold_X1CZQ_s_T X1CZQ_FSC_VoltageThreshold_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T P1QR6_HWIO_CfgFault_PWR24V_v_data;

  VGTT_EcuPinFaultStatus VbatInterface_P_GetVbatVoltage_CS_IrvBatteryFaultStatus;
  EcuHwVoltageValues_T VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  X1CZR_FSC_TimeoutThreshold_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZR_FSC_TimeoutThreshold_v();
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CZQ_FSC_VoltageThreshold_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZQ_FSC_VoltageThreshold_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1QR6_HWIO_CfgFault_PWR24V_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v();

  VbatInterface_P_GetVbatVoltage_CS_IrvBatteryFaultStatus = TSC_IoHwAb_ASIL_Core_Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvBatteryFaultStatus();
  TSC_IoHwAb_ASIL_Core_Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues(VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues);

  return RTE_E_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Watchdog_ASIL_10ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 10ms
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Watchdog_ASIL_10ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, IoHwAb_ASIL_Core_CODE) Watchdog_ASIL_10ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Watchdog_ASIL_10ms_runnable
 *********************************************************************************************************************/

  SEWS_PcbConfig_DoorAccessIf_X1CX3_T X1CX3_PcbConfig_DoorAccessIf_v_data;
  SEWS_FSC_TimeoutThreshold_X1CZR_T X1CZR_FSC_TimeoutThreshold_v_data;
  SEWS_PcbConfig_Adi_X1CXW_a_T X1CXW_PcbConfig_Adi_v_data;
  SEWS_PcbConfig_DOBHS_X1CXX_a_T X1CXX_PcbConfig_DOBHS_v_data;
  SEWS_PcbConfig_DOWHS_X1CXY_a_T X1CXY_PcbConfig_DOWHS_v_data;
  SEWS_PcbConfig_DOWLS_X1CXZ_a_T X1CXZ_PcbConfig_DOWLS_v_data;
  SEWS_PcbConfig_AdiPullUp_X1CX5_s_T X1CX5_PcbConfig_AdiPullUp_v_data;
  SEWS_FSC_VoltageThreshold_X1CZQ_s_T X1CZQ_FSC_VoltageThreshold_v_data;

  SEWS_Diag_Act_DOWHS01_P1V6O_T P1V6O_Diag_Act_DOWHS01_v_data;
  SEWS_Diag_Act_DOWHS02_P1V6P_T P1V6P_Diag_Act_DOWHS02_v_data;
  SEWS_Diag_Act_DOWLS02_P1V7E_T P1V7E_Diag_Act_DOWLS02_v_data;
  SEWS_Diag_Act_DOWLS03_P1V7F_T P1V7F_Diag_Act_DOWLS03_v_data;
  SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T P1QR6_HWIO_CfgFault_PWR24V_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  X1CX3_PcbConfig_DoorAccessIf_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v();
  X1CZR_FSC_TimeoutThreshold_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZR_FSC_TimeoutThreshold_v();
  (void)memcpy(X1CXW_PcbConfig_Adi_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXW_PcbConfig_Adi_v(), sizeof(SEWS_PcbConfig_Adi_X1CXW_a_T));
  (void)memcpy(X1CXX_PcbConfig_DOBHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXX_PcbConfig_DOBHS_v(), sizeof(SEWS_PcbConfig_DOBHS_X1CXX_a_T));
  (void)memcpy(X1CXY_PcbConfig_DOWHS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXY_PcbConfig_DOWHS_v(), sizeof(SEWS_PcbConfig_DOWHS_X1CXY_a_T));
  (void)memcpy(X1CXZ_PcbConfig_DOWLS_v_data, TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CXZ_PcbConfig_DOWLS_v(), sizeof(SEWS_PcbConfig_DOWLS_X1CXZ_a_T));
  X1CX5_PcbConfig_AdiPullUp_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v();
  X1CZQ_FSC_VoltageThreshold_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_X1CZQ_FSC_VoltageThreshold_v();

  P1V6O_Diag_Act_DOWHS01_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6O_Diag_Act_DOWHS01_v();
  P1V6P_Diag_Act_DOWHS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V6P_Diag_Act_DOWHS02_v();
  P1V7E_Diag_Act_DOWLS02_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7E_Diag_Act_DOWLS02_v();
  P1V7F_Diag_Act_DOWLS03_v_data = TSC_IoHwAb_ASIL_Core_Rte_Prm_P1V7F_Diag_Act_DOWLS03_v();
  P1QR6_HWIO_CfgFault_PWR24V_v_data = *TSC_IoHwAb_ASIL_Core_Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define IoHwAb_ASIL_Core_STOP_SEC_CODE
#include "IoHwAb_ASIL_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void IoHwAb_ASIL_Core_TestDefines(void)
{
  /* Enumeration Data Types */

  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_1 = FSC_ShutdownReady;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_2 = FSC_Reduced_12vDcDcLimit;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_3 = FSC_Reduced;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_4 = FSC_Operating;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_5 = FSC_Protecting;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_6 = FSC_Withstand;
  Fsc_OperationalMode_T Test_Fsc_OperationalMode_T_V_7 = FSC_NotAvailable;

  PcbPopulatedInfo_T Test_PcbPopulatedInfo_T_V_1 = NotPopulated;
  PcbPopulatedInfo_T Test_PcbPopulatedInfo_T_V_2 = Populated;

  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_1 = SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated;
  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_2 = SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration;
  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_3 = SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration;
  SEWS_PcbConfig_Adi_X1CXW_T Test_SEWS_PcbConfig_Adi_X1CXW_T_V_4 = SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration;

  SEWS_PcbConfig_DOBHS_X1CXX_T Test_SEWS_PcbConfig_DOBHS_X1CXX_T_V_1 = SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated;
  SEWS_PcbConfig_DOBHS_X1CXX_T Test_SEWS_PcbConfig_DOBHS_X1CXX_T_V_2 = SEWS_PcbConfig_DOBHS_X1CXX_T_Populated;

  SEWS_PcbConfig_DOWHS_X1CXY_T Test_SEWS_PcbConfig_DOWHS_X1CXY_T_V_1 = SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated;
  SEWS_PcbConfig_DOWHS_X1CXY_T Test_SEWS_PcbConfig_DOWHS_X1CXY_T_V_2 = SEWS_PcbConfig_DOWHS_X1CXY_T_Populated;

  SEWS_PcbConfig_DOWLS_X1CXZ_T Test_SEWS_PcbConfig_DOWLS_X1CXZ_T_V_1 = SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated;
  SEWS_PcbConfig_DOWLS_X1CXZ_T Test_SEWS_PcbConfig_DOWLS_X1CXZ_T_V_2 = SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated;

  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_1 = TestNotRun;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_2 = OffState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_3 = OffState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_4 = OffState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_5 = OffState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_6 = OffState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_7 = OffState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_8 = OnState_NoFaultDetected;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_9 = OnState_FaultDetected_STG;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_10 = OnState_FaultDetected_STB;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_11 = OnState_FaultDetected_OC;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_12 = OnState_FaultDetected_VBT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_13 = OnState_FaultDetected_VAT;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_14 = OnState_FaultDetected_VOR;
  VGTT_EcuPinFaultStatus Test_VGTT_EcuPinFaultStatus_V_15 = OnState_FaultDetected_CAT;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
