/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_InteriorLightPanel_2_LINMastCtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_InteriorLightPanel_2_LINMastCtrl.h"
#include "TSC_InteriorLightPanel_2_LINMastCtrl.h"








Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
{
  return Rte_Read_ComMode_LIN4_ComMode_LIN(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Read_DiagInfoILCP2_DiagInfo(DiagInfo_T *data)
{
  return Rte_Read_DiagInfoILCP2_DiagInfo(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_IntLghtCenterBtnFreeWhl_s_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_IntLghtCenterBtnFreeWhl_s_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status(FreeWheel_Status_T *data)
{
  return Rte_Read_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_IntLghtMaxModeBtnPnl2_s_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_IntLghtMaxModeBtnPnl2_s_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_IntLghtNightModeBt2_s_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_IntLghtNightModeBt2_s_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Read_LIN_IntLghtRestModeBtnPnl2_s_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_LIN_IntLghtRestModeBtnPnl2_s_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Read_ResponseErrorILCP2_ResponseErrorILCP2(ResponseErrorILCP2_T *data)
{
  return Rte_Read_ResponseErrorILCP2_ResponseErrorILCP2(data);
}


boolean TSC_InteriorLightPanel_2_LINMastCtrl_Rte_IsUpdated_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status(void)
{
  return Rte_IsUpdated_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status();
}



Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Write_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Write_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(FreeWheel_Status_T data)
{
  return Rte_Write_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Write_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Write_IntLghtNightModeBtn2_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_IntLghtNightModeBtn2_stat_PushButtonStatus(data);
}

Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Write_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(PushButtonStatus_T data)
{
  return Rte_Write_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(data);
}





     /* Client Server Interfaces: */


     /* Service calls */
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1BN4_16_ILCP_VBT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN4_16_ILCP_VBT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1BN4_17_ILCP_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN4_17_ILCP_VAT_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1BN4_44_ILCP_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN4_44_ILCP_RAM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1BN4_45_ILCP_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN4_45_ILCP_FLASH_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1BN4_46_ILCP_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN4_46_ILCP_EEPROM_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1BN4_49_ILCP_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN4_49_ILCP_HWFAIL_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_Event_D1F1F_87_ILCP2Link_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
{
  return Rte_Call_Event_D1F1F_87_ILCP2Link_NoResp_SetEventStatus(EventStatus);
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss(void)
{
  return Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss();
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss();
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss(void)
{
  return Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss();
}
Std_ReturnType TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss(void)
{
  return Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss();
}


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





boolean  TSC_InteriorLightPanel_2_LINMastCtrl_Rte_Prm_P1VR2_ILCP2_Installed_v(void)
{
  return (boolean ) Rte_Prm_P1VR2_ILCP2_Installed_v();
}


     /* InteriorLightPanel_2_LINMastCtrl */
      /* InteriorLightPanel_2_LINMastCtrl */



