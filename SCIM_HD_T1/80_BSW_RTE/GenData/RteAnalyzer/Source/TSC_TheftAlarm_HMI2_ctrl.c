/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_TheftAlarm_HMI2_ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_TheftAlarm_HMI2_ctrl.h"
#include "TSC_TheftAlarm_HMI2_ctrl.h"








Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_AlarmStatus_stat_AlarmStatus_stat(AlarmStatus_stat_T *data)
{
  return Rte_Read_AlarmStatus_stat_AlarmStatus_stat(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(KeyAuthentication_stat_decrypt_T *data)
{
  return Rte_Read_KeyAuthentication_stat_decrypt_KeyAuthentication_stat_decrypt(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyPosition_KeyPosition(KeyPosition_T *data)
{
  return Rte_Read_KeyPosition_KeyPosition(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyfobLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_KeyfobLockButton_Status_PushButtonStatus(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyfobSuperLockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_KeyfobSuperLockButton_Status_PushButtonStatus(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_KeyfobUnlockButton_Status_PushButtonStatus(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_ReducedSetModeButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_ReducedSetModeButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_SwcActivation_Parked_Parked(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Parked_Parked(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Read_VehicleModeInternal_VehicleMode(VehicleMode_T *data)
{
  return Rte_Read_VehicleModeInternal_VehicleMode(data);
}




Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_DevInd_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_ReducedSetMode_DevInd_DeviceIndication(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_rqst_CryptTrig_CryptoTrigger(Boolean data)
{
  return Rte_Write_ReducedSetMode_rqst_CryptTrig_CryptoTrigger(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt(ReducedSetMode_rqst_decrypt_T data)
{
  return Rte_Write_ReducedSetMode_rqst_decrypt_ReducedSetMode_rqst_decrypt(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized(const uint8 *data)
{
  return Rte_Write_ReducedSetMode_rqst_serialized_Crypto_Function_serialized(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger(Boolean data)
{
  return Rte_Write_TheftAlarmAct_rqst_CryptTrig_CryptoTrigger(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt(TheftAlarmAct_rqst_decrypt_I data)
{
  return Rte_Write_TheftAlarmAct_rqst_decrypt_TheftAlarmAct_rqst_decrypt(data);
}

Std_ReturnType TSC_TheftAlarm_HMI2_ctrl_Rte_Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized(const uint8 *data)
{
  return Rte_Write_TheftAlarmAct_rqst_serialized_Crypto_Function_serialized(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_TheftAlarmRequestDuration_X1CY8_T  TSC_TheftAlarm_HMI2_ctrl_Rte_Prm_X1CY8_TheftAlarmRequestDuration_v(void)
{
  return (SEWS_TheftAlarmRequestDuration_X1CY8_T ) Rte_Prm_X1CY8_TheftAlarmRequestDuration_v();
}
boolean  TSC_TheftAlarm_HMI2_ctrl_Rte_Prm_P1B2T_AlarmInstalled_v(void)
{
  return (boolean ) Rte_Prm_P1B2T_AlarmInstalled_v();
}


     /* TheftAlarm_HMI2_ctrl */
      /* TheftAlarm_HMI2_ctrl */



