/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Horn_ctrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  Horn_ctrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <Horn_ctrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "Rte_Horn_ctrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_Horn_ctrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void Horn_ctrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * boolean: Boolean (standard type)
 *
 * Enumeration Types:
 * ==================
 * OffOn_T: Enumeration of integer in interval [0...3] with enumerators
 *   OffOn_Off (0U)
 *   OffOn_On (1U)
 *   OffOn_Error (2U)
 *   OffOn_NotAvailable (3U)
 * Request_T: Enumeration of integer in interval [0...3] with enumerators
 *   Request_NotRequested (0U)
 *   Request_RequestActive (1U)
 *   Request_Error (2U)
 *   Request_NotAvailable (3U)
 * VehicleModeDistribution_T: Enumeration of integer in interval [0...3] with enumerators
 *   Operational (0U)
 *   NonOperational (1U)
 *   OperationalEntry (2U)
 *   OperationalExit (3U)
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1A1T_CityHorn_Act_v(void)
 *
 *********************************************************************************************************************/


#define Horn_ctrl_START_SEC_CODE
#include "Horn_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: Horn_ctrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst(OffOn_T *data)
 *   Std_ReturnType Rte_Read_CityHorn_HMI_rqst_CityHorn_HMI_rqst(OffOn_T *data)
 *   Std_ReturnType Rte_Read_CtaBB_Horn_rqst_CtaBB_Horn_rqst(OffOn_T *data)
 *   Std_ReturnType Rte_Read_PanicAlarmFromKeyfob_rqst_PanicAlarmFromKeyfob_rqst(Request_T *data)
 *   Std_ReturnType Rte_Read_SwcActivation_Parked_Parked(VehicleModeDistribution_T *data)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_AuxiliaryHorn_cmd_AuxiliaryHorn_cmd(OffOn_T data)
 *   Std_ReturnType Rte_Write_CityHorn_cmd_CityHorn_cmd(OffOn_T data)
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: Horn_ctrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, Horn_ctrl_CODE) Horn_ctrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: Horn_ctrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  OffOn_T Read_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst;
  OffOn_T Read_CityHorn_HMI_rqst_CityHorn_HMI_rqst;
  OffOn_T Read_CtaBB_Horn_rqst_CtaBB_Horn_rqst;
  Request_T Read_PanicAlarmFromKeyfob_rqst_PanicAlarmFromKeyfob_rqst;
  VehicleModeDistribution_T Read_SwcActivation_Parked_Parked;

  boolean P1A1T_CityHorn_Act_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1A1T_CityHorn_Act_v_data = TSC_Horn_ctrl_Rte_Prm_P1A1T_CityHorn_Act_v();

  fct_status = TSC_Horn_ctrl_Rte_Read_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst(&Read_AuxiliaryHorn_HMI_rqst_AuxiliaryHorn_HMI_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Horn_ctrl_Rte_Read_CityHorn_HMI_rqst_CityHorn_HMI_rqst(&Read_CityHorn_HMI_rqst_CityHorn_HMI_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Horn_ctrl_Rte_Read_CtaBB_Horn_rqst_CtaBB_Horn_rqst(&Read_CtaBB_Horn_rqst_CtaBB_Horn_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Horn_ctrl_Rte_Read_PanicAlarmFromKeyfob_rqst_PanicAlarmFromKeyfob_rqst(&Read_PanicAlarmFromKeyfob_rqst_PanicAlarmFromKeyfob_rqst);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Horn_ctrl_Rte_Read_SwcActivation_Parked_Parked(&Read_SwcActivation_Parked_Parked);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_Horn_ctrl_Rte_Write_AuxiliaryHorn_cmd_AuxiliaryHorn_cmd(Rte_InitValue_AuxiliaryHorn_cmd_AuxiliaryHorn_cmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_Horn_ctrl_Rte_Write_CityHorn_cmd_CityHorn_cmd(Rte_InitValue_CityHorn_cmd_CityHorn_cmd);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  Horn_ctrl_TestDefines();


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define Horn_ctrl_STOP_SEC_CODE
#include "Horn_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void Horn_ctrl_TestDefines(void)
{
  /* Enumeration Data Types */

  OffOn_T Test_OffOn_T_V_1 = OffOn_Off;
  OffOn_T Test_OffOn_T_V_2 = OffOn_On;
  OffOn_T Test_OffOn_T_V_3 = OffOn_Error;
  OffOn_T Test_OffOn_T_V_4 = OffOn_NotAvailable;

  Request_T Test_Request_T_V_1 = Request_NotRequested;
  Request_T Test_Request_T_V_2 = Request_RequestActive;
  Request_T Test_Request_T_V_3 = Request_Error;
  Request_T Test_Request_T_V_4 = Request_NotAvailable;

  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_1 = Operational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_2 = NonOperational;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_3 = OperationalEntry;
  VehicleModeDistribution_T Test_VehicleModeDistribution_T_V_4 = OperationalExit;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
