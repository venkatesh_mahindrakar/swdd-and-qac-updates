/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_Keyfob_Mgr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching(DriverAuthDeviceMatching_T *data);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_KeyfobAuth_rqst_KeyfobAuth_rqst(KeyfobAuth_rqst_T *data);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst(KeyfobInCabPresencePS_T *data);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_KeyfobLocation_rqst_KeyfobLocation_rqst(KeyfobLocation_rqst_T *data);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Read_SwcActivation_Security_SwcActivation_Security(VehicleModeDistribution_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_Keyfob_Mgr_Rte_Write_AddrParP1DS4_stat_dataP1DS4(const SEWS_KeyfobEncryptCode_P1DS4_T *data);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Write_KeyfobAuth_stat_KeyfobAuth_stat(KeyfobAuth_stat_T data);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Write_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T data);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Write_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(KeyfobOutsideLocation_stat_T data);

/** Client server interfaces */
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(SCIM_ImmoDriver_ProcessingStatus_T *ImmoProcessingStatus);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(SCIM_ImmoType_T ImmoType, Boolean AuthRequestedbyImmo);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(uint8 *matchingStatus, uint8 *matchedKeyfobCount);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF(void);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList(void);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(SCIM_PassiveDriver_ProcessingStatus_T *ProcessingStatus, KeyfobInCabLocation_stat_T *KeyfobLocationbyPassive_Incab, KeyfobOutsideLocation_stat_T *KeyfobLocationbyPassive_Outcab);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(SCIM_PassiveSearchCoverage_T PassiveSearchCoverage, boolean SearchRequestedbyPassive);
Std_ReturnType TSC_Keyfob_Mgr_Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList(void);

/** Explicit inter-runnable variables */
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount(void);
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus(void);
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(void);
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(void);
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(void);
void TSC_Keyfob_Mgr_Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(uint8);
void TSC_Keyfob_Mgr_Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(uint8);
void TSC_Keyfob_Mgr_Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(uint8);
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(void);
void TSC_Keyfob_Mgr_Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(uint8);
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(void);
void TSC_Keyfob_Mgr_Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(uint8);
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(void);
void TSC_Keyfob_Mgr_Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(uint8);
uint8 TSC_Keyfob_Mgr_Rte_IrvRead_RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF(void);

/** Calibration Component Calibration Parameters */
SEWS_KeyMatchingIndicationTimeout_X1CV3_T  TSC_Keyfob_Mgr_Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v(void);
boolean  TSC_Keyfob_Mgr_Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v(void);
boolean  TSC_Keyfob_Mgr_Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v(void);
boolean  TSC_Keyfob_Mgr_Rte_Prm_P1B2U_KeyfobPresent_v(void);
SEWS_KeyfobEncryptCode_P1DS4_a_T * TSC_Keyfob_Mgr_Rte_Prm_P1DS4_KeyfobEncryptCode_v(void);
SEWS_CrankingLockActivation_P1DS3_T  TSC_Keyfob_Mgr_Rte_Prm_P1DS3_CrankingLockActivation_v(void);
boolean  TSC_Keyfob_Mgr_Rte_Prm_P1C54_FactoryModeActive_v(void);




