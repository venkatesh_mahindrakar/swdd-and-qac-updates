/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_CollisionMitigation_HMICtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_CollisionMitigation_HMICtrl.h"
#include "TSC_CollisionMitigation_HMICtrl.h"








Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_AEBS_ButtonStatus_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_AEBS_ButtonStatus_PushButtonStatus(data);
}

Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_CM_Status_CM_Status(CM_Status_T *data)
{
  return Rte_Read_CM_Status_CM_Status(data);
}

Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_FCWPushButton_PushButtonStatus(PushButtonStatus_T *data)
{
  return Rte_Read_FCWPushButton_PushButtonStatus(data);
}

Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_FCW_Status_FCW_Status(DisableEnable_T *data)
{
  return Rte_Read_FCW_Status_FCW_Status(data);
}

Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_FCW_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T *data)
{
  return Rte_Read_FCW_SwitchStatus_A2PosSwitchStatus(data);
}

Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Read_SwcActivation_EngineRun_EngineRun(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_EngineRun_EngineRun(data);
}




Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM(CollSituationHMICtrlRequestVM_T data)
{
  return Rte_Write_CollSituationHMICtrlRequestVM_CollSituationHMICtrlRequestVM(data);
}

Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_FCW_DeviceIndication_DeviceIndication(DeviceIndication_T data)
{
  return Rte_Write_FCW_DeviceIndication_DeviceIndication(data);
}

Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_FCW_Enable_FCW_Enable(DisableEnable_T data)
{
  return Rte_Write_FCW_Enable_FCW_Enable(data);
}

Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_SetCMOperation_SetCMOperation(SetCMOperation_T data)
{
  return Rte_Write_SetCMOperation_SetCMOperation(data);
}

Std_ReturnType TSC_CollisionMitigation_HMICtrl_Rte_Write_SetFCWOperation_SetFCWOperation(SetFCWOperation_T data)
{
  return Rte_Write_SetFCWOperation_SetFCWOperation(data);
}





     /* Client Server Interfaces: */


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_HeadwaySupport_P1BEX_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1BEX_HeadwaySupport_v(void)
{
  return (SEWS_HeadwaySupport_P1BEX_T ) Rte_Prm_P1BEX_HeadwaySupport_v();
}
SEWS_FCW_LedLogic_P1LG1_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LG1_FCW_LedLogic_v(void)
{
  return (SEWS_FCW_LedLogic_P1LG1_T ) Rte_Prm_P1LG1_FCW_LedLogic_v();
}
SEWS_CM_Configuration_P1LGD_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGD_CM_Configuration_v(void)
{
  return (SEWS_CM_Configuration_P1LGD_T ) Rte_Prm_P1LGD_CM_Configuration_v();
}
SEWS_FCW_SwPushThreshold_P1LGE_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGE_FCW_SwPushThreshold_v(void)
{
  return (SEWS_FCW_SwPushThreshold_P1LGE_T ) Rte_Prm_P1LGE_FCW_SwPushThreshold_v();
}
SEWS_FCW_ConfirmTimeout_P1LGF_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGF_FCW_ConfirmTimeout_v(void)
{
  return (SEWS_FCW_ConfirmTimeout_P1LGF_T ) Rte_Prm_P1LGF_FCW_ConfirmTimeout_v();
}
SEWS_FCW_SwStuckTimeout_P1LGG_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1LGG_FCW_SwStuckTimeout_v(void)
{
  return (SEWS_FCW_SwStuckTimeout_P1LGG_T ) Rte_Prm_P1LGG_FCW_SwStuckTimeout_v();
}
SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1MOT_CollSituationHMICtrlRequestVM_Time_v(void)
{
  return (SEWS_CollSituationHMICtrlRequestVM_Time_P1MOT_T ) Rte_Prm_P1MOT_CollSituationHMICtrlRequestVM_Time_v();
}
boolean  TSC_CollisionMitigation_HMICtrl_Rte_Prm_P1NT1_CM_DeviceType_v(void)
{
  return (boolean ) Rte_Prm_P1NT1_CM_DeviceType_v();
}


     /* CollisionMitigation_HMICtrl */
      /* CollisionMitigation_HMICtrl */



