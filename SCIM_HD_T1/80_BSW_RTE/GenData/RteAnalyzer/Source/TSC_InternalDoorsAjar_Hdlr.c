/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_InternalDoorsAjar_Hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_InternalDoorsAjar_Hdlr.h"
#include "TSC_InternalDoorsAjar_Hdlr.h"








Std_ReturnType TSC_InternalDoorsAjar_Hdlr_Rte_Read_SwcActivation_Parked_Parked(VehicleModeDistribution_T *data)
{
  return Rte_Read_SwcActivation_Parked_Parked(data);
}




Std_ReturnType TSC_InternalDoorsAjar_Hdlr_Rte_Write_DriverDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T data)
{
  return Rte_Write_DriverDoorAjarInternal_stat_DoorAjar_stat(data);
}

Std_ReturnType TSC_InternalDoorsAjar_Hdlr_Rte_Write_PsngrDoorAjarInternal_stat_DoorAjar_stat(DoorAjar_stat_T data)
{
  return Rte_Write_PsngrDoorAjarInternal_stat_DoorAjar_stat(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_InternalDoorsAjar_Hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef, AdiPinVoltage, BatteryVoltage, FaultStatus);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_InternalDoorsAjar_Hdlr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
{
  return (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *) Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
}
SEWS_LockFunctionHardwareInterface_P1MXZ_T  TSC_InternalDoorsAjar_Hdlr_Rte_Prm_P1MXZ_LockFunctionHardwareInterface_v(void)
{
  return (SEWS_LockFunctionHardwareInterface_P1MXZ_T ) Rte_Prm_P1MXZ_LockFunctionHardwareInterface_v();
}


     /* InternalDoorsAjar_Hdlr */
      /* InternalDoorsAjar_Hdlr */



