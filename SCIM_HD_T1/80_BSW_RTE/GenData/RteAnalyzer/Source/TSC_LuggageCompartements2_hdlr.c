/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_LuggageCompartements2_hdlr.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Implementation of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "Rte_LuggageCompartements2_hdlr.h"
#include "TSC_LuggageCompartements2_hdlr.h"











Std_ReturnType TSC_LuggageCompartements2_hdlr_Rte_Write_LuggageCompart_stat_CryptTrig_CryptoTrigger(Boolean data)
{
  return Rte_Write_LuggageCompart_stat_CryptTrig_CryptoTrigger(data);
}

Std_ReturnType TSC_LuggageCompartements2_hdlr_Rte_Write_LuggageCompart_stat_decrypt_LuggageCompart_stat_decrypt(LuggageCompart_stat_decrypt_T data)
{
  return Rte_Write_LuggageCompart_stat_decrypt_LuggageCompart_stat_decrypt(data);
}

Std_ReturnType TSC_LuggageCompartements2_hdlr_Rte_Write_LuggageCompart_stat_serialized_Crypto_Function_serialized(const uint8 *data)
{
  return Rte_Write_LuggageCompart_stat_serialized_Crypto_Function_serialized(data);
}





     /* Client Server Interfaces: */
Std_ReturnType TSC_LuggageCompartements2_hdlr_Rte_Call_AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, VGTT_EcuPinVoltage_0V2 *AdiPinVoltage, VGTT_EcuPinVoltage_0V2 *BatteryVoltage, VGTT_EcuPinFaultStatus *FaultStatus)
{
  return Rte_Call_AdiInterface_P_GetAdiPinState_CS(AdiPinRef, AdiPinVoltage, BatteryVoltage, FaultStatus);
}


     /* Service calls */


     /* Mode Interfaces */




     /* Trigger Interfaces */

     /* Inter-Runnable variables */





SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T * TSC_LuggageCompartements2_hdlr_Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v(void)
{
  return (SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T *) Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v();
}


     /* LuggageCompartements2_hdlr */
      /* LuggageCompartements2_hdlr */



