/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_ECSWiredRemote_LINMasterCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/


/** Sender receiver - explicit read services */
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_Adjust_DeviceIndication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_ComMode_LIN5_ComMode_LIN(ComMode_LIN_Type *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_DiagInfoRCECS_DiagInfo(DiagInfo_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_Down_DeviceIndication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_LIN_AdjustButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_LIN_BackButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_LIN_MemButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_LIN_SelectButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_LIN_StopButtonStatus_PushButtonStatus(PushButtonStatus_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_LIN_WRDownButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_LIN_WRUpButtonStatus_EvalButtonRequest(EvalButtonRequest_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_M1_DeviceIndication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_M2_DeviceIndication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_M3_DeviceIndication_DeviceIndication(DeviceIndication_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_ResponseErrorRCECS_ResponseErrorRCECS(ResponseErrorRCECS *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_ShortPulseMaxLength_ShortPulseMaxLength(ShortPulseMaxLength_T *data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Read_Up_DeviceIndication_DeviceIndication(DeviceIndication_T *data);

/** Sender receiver - explicit write services */
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_AdjustButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_BackButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_LIN_Adjust_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_LIN_Down_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_LIN_M1_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_LIN_M2_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_LIN_M3_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_LIN_ShortPulseMaxLength_ShortPulseMaxLength(ShortPulseMaxLength_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_LIN_Up_DeviceIndication_DeviceIndication(DeviceIndication_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_MemButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_SelectButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_StopButtonStatus_PushButtonStatus(PushButtonStatus_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_WRDownButtonStatus_EvalButtonRequest(EvalButtonRequest_T data);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Write_WRUpButtonStatus_EvalButtonRequest(EvalButtonRequest_T data);

/** Service interfaces */
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Call_Event_D1BKE_87_RCECSLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Call_Event_D1BOI_16_RCECS_VBT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Call_Event_D1BOI_17_RCECS_VAT_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Call_Event_D1BOI_46_RCECS_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Call_Event_D1BOI_94_RCECS_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Call_UR_ANW_ECSStandByTrigger1_ActivateIss(void);
Std_ReturnType TSC_ECSWiredRemote_LINMasterCtrl_Rte_Call_UR_ANW_ECSStandByTrigger1_DeactivateIss(void);

/** Explicit inter-runnable variables */
uint8 TSC_ECSWiredRemote_LINMasterCtrl_Rte_IrvRead_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData_Irv_IOCTL_RCECSLinCtrl(void);
void TSC_ECSWiredRemote_LINMasterCtrl_Rte_IrvWrite_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_RCECSLinCtrl(uint8);
void TSC_ECSWiredRemote_LINMasterCtrl_Rte_IrvWrite_DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_RCECSLinCtrl(uint8);
uint8 TSC_ECSWiredRemote_LINMasterCtrl_Rte_IrvRead_ECSWiredRemote_LINMasterCtrl_20ms_runnable_Irv_IOCTL_RCECSLinCtrl(void);
void TSC_ECSWiredRemote_LINMasterCtrl_Rte_IrvWrite_ECSWiredRemote_LINMasterCtrl_20ms_runnable_Irv_IOCTL_RCECSLinCtrl(uint8);

/** Calibration Component Calibration Parameters */
boolean  TSC_ECSWiredRemote_LINMasterCtrl_Rte_Prm_P1ALT_ECS_PartialAirSystem_v(void);
boolean  TSC_ECSWiredRemote_LINMasterCtrl_Rte_Prm_P1ALU_ECS_FullAirSystem_v(void);




