/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  TSC_SchM_J1939Rm.c
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Header of wrapper software component for Bte-based Rte test cases
 *********************************************************************************************************************/
#include "SchM_J1939Rm.h"
#include "TSC_SchM_J1939Rm.h"
void TSC_J1939Rm_SchM_Enter_J1939Rm_J1939RM_EXCLUSIVE_AREA_ACKQUEUELOCK(void)
{
  SchM_Enter_J1939Rm_J1939RM_EXCLUSIVE_AREA_ACKQUEUELOCK();
}
void TSC_J1939Rm_SchM_Exit_J1939Rm_J1939RM_EXCLUSIVE_AREA_ACKQUEUELOCK(void)
{
  SchM_Exit_J1939Rm_J1939RM_EXCLUSIVE_AREA_ACKQUEUELOCK();
}
void TSC_J1939Rm_SchM_Enter_J1939Rm_J1939RM_EXCLUSIVE_AREA_REQ2QUEUELOCK(void)
{
  SchM_Enter_J1939Rm_J1939RM_EXCLUSIVE_AREA_REQ2QUEUELOCK();
}
void TSC_J1939Rm_SchM_Exit_J1939Rm_J1939RM_EXCLUSIVE_AREA_REQ2QUEUELOCK(void)
{
  SchM_Exit_J1939Rm_J1939RM_EXCLUSIVE_AREA_REQ2QUEUELOCK();
}
void TSC_J1939Rm_SchM_Enter_J1939Rm_J1939RM_EXCLUSIVE_AREA_REQQUEUELOCK(void)
{
  SchM_Enter_J1939Rm_J1939RM_EXCLUSIVE_AREA_REQQUEUELOCK();
}
void TSC_J1939Rm_SchM_Exit_J1939Rm_J1939RM_EXCLUSIVE_AREA_REQQUEUELOCK(void)
{
  SchM_Exit_J1939Rm_J1939RM_EXCLUSIVE_AREA_REQQUEUELOCK();
}
void TSC_J1939Rm_SchM_Enter_J1939Rm_J1939RM_EXCLUSIVE_AREA_TXPDULOCK(void)
{
  SchM_Enter_J1939Rm_J1939RM_EXCLUSIVE_AREA_TXPDULOCK();
}
void TSC_J1939Rm_SchM_Exit_J1939Rm_J1939RM_EXCLUSIVE_AREA_TXPDULOCK(void)
{
  SchM_Exit_J1939Rm_J1939RM_EXCLUSIVE_AREA_TXPDULOCK();
}
