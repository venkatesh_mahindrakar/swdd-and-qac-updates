/**********************************************************************************************************************
 *  FILE REQUIRES USER MODIFICATIONS
 *  Template Scope: sections marked with Start and End comments
 *  -------------------------------------------------------------------------------------------------------------------
 *  This file includes template code that must be completed and/or adapted during BSW integration.
 *  The template code is incomplete and only intended for providing a signature and an empty implementation.
 *  It is neither intended nor qualified for use in series production without applying suitable quality measures.
 *  The template code must be completed as described in the instructions given within this file and/or in the.
 *  Technical Reference..
 *  The completed implementation must be tested with diligent care and must comply with all quality requirements which.
 *  are necessary according to the state of the art before its use..
 *********************************************************************************************************************/
/**********************************************************************************************************************
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  ExteriorLightPanel_1_LINMastCtrl.c
 *           Config:  SCIM_HD_T1.dpa
 *        SW-C Type:  ExteriorLightPanel_1_LINMastCtrl
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  C-Code implementation template for SW-C <ExteriorLightPanel_1_LINMastCtrl>
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of version logging area >>                DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/* PRQA S 0777, 0779 EOF */ /* MD_MSR_5.1_777, MD_MSR_5.1_779 */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
/* PRQA S 0614 EOF */ /* MD_Rte_TestCode */

/* PRQA S 3109 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3112 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3197 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3198 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3199 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3201 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3203 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3205 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3206 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3218 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3229 EOF */ /* MD_Rte_TestCode */
/* PRQA S 2002 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3334 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3417 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3426 EOF */ /* MD_Rte_TestCode */
/* PRQA S 3453 EOF */ /* MD_Rte_TestCode */

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of version logging area >>                  DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *
 * AUTOSAR Modelling Object Descriptions
 *
 **********************************************************************************************************************
 *
 * Data Types:
 * ===========
 * Dcm_NegativeResponseCodeType
 *   
 *
 * Dem_EventStatusType
 *   
 *
 *********************************************************************************************************************/

#include "Rte_ExteriorLightPanel_1_LINMastCtrl.h" /* PRQA S 0857 */ /* MD_MSR_1.1_857 */
#include "TSC_ExteriorLightPanel_1_LINMastCtrl.h"


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of include and declaration area >>        DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

#include "string.h"

static void ExteriorLightPanel_1_LINMastCtrl_TestDefines(void);


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of include and declaration area >>          DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * Used AUTOSAR Data Types
 *
 **********************************************************************************************************************
 *
 * Primitive Types:
 * ================
 * DiagInfo_T: Integer in interval [0...127]
 *   Unit: [NotApplicable], Factor: 1, Offset: 0
 * ResponseErrorELCP1_T: Boolean
 * boolean: Boolean (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 * uint8: Integer in interval [0...255] (standard type)
 *
 * Enumeration Types:
 * ==================
 * ComMode_LIN_Type: Enumeration of integer in interval [0...255] with enumerators
 *   Inactive (0U)
 *   Diagnostic (1U)
 *   SwitchDetection (2U)
 *   ApplicationMonitoring (3U)
 *   Calibration (4U)
 *   Spare1 (5U)
 *   Error (6U)
 *   NotAvailable (7U)
 * Dcm_NegativeResponseCodeType: Enumeration of integer in interval [0...255] with enumerators
 *   DCM_E_POSITIVERESPONSE (0U)
 *   DCM_E_GENERALREJECT (16U)
 *   DCM_E_SERVICENOTSUPPORTED (17U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTED (18U)
 *   DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT (19U)
 *   DCM_E_RESPONSETOOLONG (20U)
 *   DCM_E_BUSYREPEATREQUEST (33U)
 *   DCM_E_CONDITIONSNOTCORRECT (34U)
 *   DCM_E_REQUESTSEQUENCEERROR (36U)
 *   DCM_E_NORESPONSEFROMSUBNETCOMPONENT (37U)
 *   DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION (38U)
 *   DCM_E_REQUESTOUTOFRANGE (49U)
 *   DCM_E_SECURITYACCESSDENIED (51U)
 *   DCM_E_INVALIDKEY (53U)
 *   DCM_E_EXCEEDNUMBEROFATTEMPTS (54U)
 *   DCM_E_REQUIREDTIMEDELAYNOTEXPIRED (55U)
 *   DCM_E_UPLOADDOWNLOADNOTACCEPTED (112U)
 *   DCM_E_TRANSFERDATASUSPENDED (113U)
 *   DCM_E_GENERALPROGRAMMINGFAILURE (114U)
 *   DCM_E_WRONGBLOCKSEQUENCECOUNTER (115U)
 *   DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING (120U)
 *   DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION (126U)
 *   DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION (127U)
 *   DCM_E_RPMTOOHIGH (129U)
 *   DCM_E_RPMTOOLOW (130U)
 *   DCM_E_ENGINEISRUNNING (131U)
 *   DCM_E_ENGINEISNOTRUNNING (132U)
 *   DCM_E_ENGINERUNTIMETOOLOW (133U)
 *   DCM_E_TEMPERATURETOOHIGH (134U)
 *   DCM_E_TEMPERATURETOOLOW (135U)
 *   DCM_E_VEHICLESPEEDTOOHIGH (136U)
 *   DCM_E_VEHICLESPEEDTOOLOW (137U)
 *   DCM_E_THROTTLE_PEDALTOOHIGH (138U)
 *   DCM_E_THROTTLE_PEDALTOOLOW (139U)
 *   DCM_E_TRANSMISSIONRANGENOTINNEUTRAL (140U)
 *   DCM_E_TRANSMISSIONRANGENOTINGEAR (141U)
 *   DCM_E_BRAKESWITCH_NOTCLOSED (143U)
 *   DCM_E_SHIFTERLEVERNOTINPARK (144U)
 *   DCM_E_TORQUECONVERTERCLUTCHLOCKED (145U)
 *   DCM_E_VOLTAGETOOHIGH (146U)
 *   DCM_E_VOLTAGETOOLOW (147U)
 *   DCM_E_VMSCNC_0 (240U)
 *   DCM_E_VMSCNC_1 (241U)
 *   DCM_E_VMSCNC_2 (242U)
 *   DCM_E_VMSCNC_3 (243U)
 *   DCM_E_VMSCNC_4 (244U)
 *   DCM_E_VMSCNC_5 (245U)
 *   DCM_E_VMSCNC_6 (246U)
 *   DCM_E_VMSCNC_7 (247U)
 *   DCM_E_VMSCNC_8 (248U)
 *   DCM_E_VMSCNC_9 (249U)
 *   DCM_E_VMSCNC_A (250U)
 *   DCM_E_VMSCNC_B (251U)
 *   DCM_E_VMSCNC_C (252U)
 *   DCM_E_VMSCNC_D (253U)
 *   DCM_E_VMSCNC_E (254U)
 * Dem_EventStatusType: Enumeration of integer in interval [0...255] with enumerators
 *   DEM_EVENT_STATUS_PASSED (0U)
 *   DEM_EVENT_STATUS_FAILED (1U)
 *   DEM_EVENT_STATUS_PREPASSED (2U)
 *   DEM_EVENT_STATUS_PREFAILED (3U)
 *   DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
 *   DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
 *   DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
 *   DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
 *   DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
 * DeviceIndication_T: Enumeration of integer in interval [0...3] with enumerators
 *   DeviceIndication_Off (0U)
 *   DeviceIndication_On (1U)
 *   DeviceIndication_Blink (2U)
 *   DeviceIndication_SpareValue (3U)
 * DiagActiveState_T: Enumeration of integer in interval [0...255] with enumerators
 *   Diag_Active_FALSE (0U)
 *   Diag_Active_TRUE (1U)
 * FreeWheel_Status_T: Enumeration of integer in interval [0...15] with enumerators
 *   FreeWheel_Status_NoMovement (0U)
 *   FreeWheel_Status_1StepClockwise (1U)
 *   FreeWheel_Status_2StepsClockwise (2U)
 *   FreeWheel_Status_3StepsClockwise (3U)
 *   FreeWheel_Status_4StepsClockwise (4U)
 *   FreeWheel_Status_5StepsClockwise (5U)
 *   FreeWheel_Status_6StepsClockwise (6U)
 *   FreeWheel_Status_1StepCounterClockwise (7U)
 *   FreeWheel_Status_2StepsCounterClockwise (8U)
 *   FreeWheel_Status_3StepsCounterClockwise (9U)
 *   FreeWheel_Status_4StepsCounterClockwise (10U)
 *   FreeWheel_Status_5StepsCounterClockwise (11U)
 *   FreeWheel_Status_6StepsCounterClockwise (12U)
 *   FreeWheel_Status_Spare (13U)
 *   FreeWheel_Status_Error (14U)
 *   FreeWheel_Status_NotAvailable (15U)
 * PushButtonStatus_T: Enumeration of integer in interval [0...3] with enumerators
 *   PushButtonStatus_Neutral (0U)
 *   PushButtonStatus_Pushed (1U)
 *   PushButtonStatus_Error (2U)
 *   PushButtonStatus_NotAvailable (3U)
 * Thumbwheel_stat_T: Enumeration of integer in interval [0...31] with enumerators
 *   Thumbwheel_stat_ThumbWheelPos0 (0U)
 *   Thumbwheel_stat_ThumbWheelPos1 (1U)
 *   Thumbwheel_stat_ThumbWheelPos2 (2U)
 *   Thumbwheel_stat_ThumbWheelPos3 (3U)
 *   Thumbwheel_stat_ThumbWheelPos4 (4U)
 *   Thumbwheel_stat_ThumbWheelPos5 (5U)
 *   Thumbwheel_stat_ThumbWheelPos6 (6U)
 *   Thumbwheel_stat_ThumbWheelPos7 (7U)
 *   Thumbwheel_stat_ThumbWheelPos8 (8U)
 *   Thumbwheel_stat_ThumbWheelPos9 (9U)
 *   Thumbwheel_stat_ThumbWheelPos10 (10U)
 *   Thumbwheel_stat_ThumbWheelPos11 (11U)
 *   Thumbwheel_stat_ThumbWheelPos12 (12U)
 *   Thumbwheel_stat_ThumbWheelPos13 (13U)
 *   Thumbwheel_stat_ThumbWheelPos14 (14U)
 *   Thumbwheel_stat_ThumbWheelPos15 (15U)
 *   Thumbwheel_stat_ThumbWheelPos16 (16U)
 *   Thumbwheel_stat_Spare (17U)
 *   Thumbwheel_stat_Spare_01 (18U)
 *   Thumbwheel_stat_Spare_02 (19U)
 *   Thumbwheel_stat_Spare_03 (20U)
 *   Thumbwheel_stat_Spare_04 (21U)
 *   Thumbwheel_stat_Spare_05 (22U)
 *   Thumbwheel_stat_Spare_06 (23U)
 *   Thumbwheel_stat_Spare_07 (24U)
 *   Thumbwheel_stat_Spare_08 (25U)
 *   Thumbwheel_stat_Spare_09 (26U)
 *   Thumbwheel_stat_Spare_10 (27U)
 *   Thumbwheel_stat_Spare_11 (28U)
 *   Thumbwheel_stat_Spare_12 (29U)
 *   Thumbwheel_stat_Error (30U)
 *   Thumbwheel_stat_NotAvaliable (31U)
 *
 * Array Types:
 * ============
 * Dcm_Data1ByteType: Array with 1 element(s) of type uint8
 *
 *********************************************************************************************************************/


/**********************************************************************************************************************
 *
 * APIs which are accessible from all runnable entities of the SW-C
 *
 **********************************************************************************************************************
 * Calibration Parameters:
 * =======================
 *   Calibration Component Calibration Parameters:
 *   ---------------------------------------------
 *   boolean Rte_Prm_P1VR3_ELCP1_Installed_v(void)
 *
 *********************************************************************************************************************/


#define ExteriorLightPanel_1_LINMastCtrl_START_SEC_CODE
#include "ExteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <FreezeCurrentState> of PortPrototype <DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState (returns application error)
 *********************************************************************************************************************/

  boolean P1VR3_ELCP1_Installed_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1VR3_ELCP1_Installed_v_data = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Prm_P1VR3_ELCP1_Installed_v();

  ExteriorLightPanel_1_LINMastCtrl_TestDefines();

  return RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReadData> of PortPrototype <DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl(void)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData(uint8 *Data)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData (returns application error)
 *********************************************************************************************************************/

  boolean P1VR3_ELCP1_Installed_v_data;

  uint8 DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1VR3_ELCP1_Installed_v_data = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Prm_P1VR3_ELCP1_Installed_v();

  DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvRead_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl();

  return RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ReturnControlToECU> of PortPrototype <DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP1LinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU(Dcm_NegativeResponseCodeType *ErrorCode)
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU (returns application error)
 *********************************************************************************************************************/

  boolean P1VR3_ELCP1_Installed_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1VR3_ELCP1_Installed_v_data = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Prm_P1VR3_ELCP1_Installed_v();

  TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP1LinCtrl(0U);

  return RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment
 *
 * This runnable can be invoked concurrently (reentrant implementation).
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered by server invocation for OperationPrototype <ShortTermAdjustment> of PortPrototype <DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals>
 *
 **********************************************************************************************************************
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP1LinCtrl(uint8 data)
 *
 **********************************************************************************************************************
 *
 * Runnable prototype:
 * ===================
 *   Std_ReturnType DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment(const uint8 *Data, Dcm_NegativeResponseCodeType *ErrorCode)
 *     Argument Data: uint8* is of type Dcm_Data1ByteType
 *
 **********************************************************************************************************************
 *
 * Available Application Errors:
 * =============================
 *   RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment (returns application error)
 *********************************************************************************************************************/

  boolean P1VR3_ELCP1_Installed_v_data;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1VR3_ELCP1_Installed_v_data = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Prm_P1VR3_ELCP1_Installed_v();

  TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP1LinCtrl(0U);

  return RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK;

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}

/**********************************************************************************************************************
 *
 * Runnable Entity Name: ExteriorLightPanel_1_LINMastCtrl_20ms_runnable
 *
 *---------------------------------------------------------------------------------------------------------------------
 *
 * Executed if at least one of the following trigger conditions occurred:
 *   - triggered on TimingEvent every 20ms
 *
 **********************************************************************************************************************
 *
 * Input Interfaces:
 * =================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Read_ComMode_LIN4_ComMode_LIN(ComMode_LIN_Type *data)
 *   Std_ReturnType Rte_Read_DaytimeRunningLight_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DiagActiveState_isDiagActive(DiagActiveState_T *data)
 *   Std_ReturnType Rte_Read_DiagInfoELCP1_DiagInfo(DiagInfo_T *data)
 *   Std_ReturnType Rte_Read_DrivingLightPlus_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_DrivingLight_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_FrontFog_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_LIN_BackLightDimming_Status_Thumbwheel_stat(Thumbwheel_stat_T *data)
 *   Std_ReturnType Rte_Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus(PushButtonStatus_T *data)
 *   Std_ReturnType Rte_Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat(Thumbwheel_stat_T *data)
 *   Std_ReturnType Rte_Read_LIN_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T *data)
 *   Std_ReturnType Rte_Read_ParkingLight_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_RearFog_Indication_DeviceIndication(DeviceIndication_T *data)
 *   Std_ReturnType Rte_Read_ResponseErrorELCP1_ResponseErrorELCP1(ResponseErrorELCP1_T *data)
 *   boolean Rte_IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status(void)
 *
 * Output Interfaces:
 * ==================
 *   Explicit S/R API:
 *   -----------------
 *   Std_ReturnType Rte_Write_BackLightDimming_Status_Thumbwheel_stat(Thumbwheel_stat_T data)
 *   Std_ReturnType Rte_Write_BlackPanelMode_ButtonStatus_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightFront_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_FogLightRear_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data)
 *   Std_ReturnType Rte_Write_LIN_DaytimeRunningLight_Indica_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_DrivingLightPlus_Indicatio_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_DrivingLight_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_FrontFog_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_ParkingLight_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LIN_RearFog_Indication_DeviceIndication(DeviceIndication_T data)
 *   Std_ReturnType Rte_Write_LevelingThumbwheel_stat_Thumbwheel_stat(Thumbwheel_stat_T data)
 *   Std_ReturnType Rte_Write_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T data)
 *
 * Inter Runnable Variables:
 * =========================
 *   Explicit Read Access:
 *   ---------------------
 *   uint8 Rte_IrvRead_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(void)
 *
 *   Explicit Write Access:
 *   ----------------------
 *   void Rte_IrvWrite_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(uint8 data)
 *
 * Service Calls:
 * ==============
 *   Service Invocation:
 *   -------------------
 *   Std_ReturnType Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_44_ELCP_RAM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(Dem_EventStatusType EventStatus)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_DiagnosticMonitor_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_DimmingAdjustment1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExteriorLightsRequest1_ActivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *   Std_ReturnType Rte_Call_UR_ANW_ExteriorLightsRequest1_DeactivateIss(void)
 *     Synchronous Service Invocation. Timeout: None
 *     Returned Application Errors: RTE_E_Issm_IssRequest_E_NOT_OK
 *
 *********************************************************************************************************************/
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of documentation area >>                  DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_doc
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of documentation area >>                    DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

FUNC(void, ExteriorLightPanel_1_LINMastCtrl_CODE) ExteriorLightPanel_1_LINMastCtrl_20ms_runnable(void) /* PRQA S 0850 */ /* MD_MSR_19.8 */
{
/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of runnable implementation >>             DO NOT CHANGE THIS COMMENT!
 * Symbol: ExteriorLightPanel_1_LINMastCtrl_20ms_runnable
 *********************************************************************************************************************/

  Std_ReturnType fct_status;
  boolean fct_error = 0;

  ComMode_LIN_Type Read_ComMode_LIN4_ComMode_LIN;
  DeviceIndication_T Read_DaytimeRunningLight_Indication_DeviceIndication;
  DiagActiveState_T Read_DiagActiveState_isDiagActive;
  DiagInfo_T Read_DiagInfoELCP1_DiagInfo;
  DeviceIndication_T Read_DrivingLightPlus_Indication_DeviceIndication;
  DeviceIndication_T Read_DrivingLight_Indication_DeviceIndication;
  DeviceIndication_T Read_FrontFog_Indication_DeviceIndication;
  Thumbwheel_stat_T Read_LIN_BackLightDimming_Status_Thumbwheel_stat;
  PushButtonStatus_T Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus;
  PushButtonStatus_T Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus;
  PushButtonStatus_T Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus;
  Thumbwheel_stat_T Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat;
  FreeWheel_Status_T Read_LIN_LightMode_Status_1_FreeWheel_Status;
  boolean IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status;
  DeviceIndication_T Read_ParkingLight_Indication_DeviceIndication;
  DeviceIndication_T Read_RearFog_Indication_DeviceIndication;
  ResponseErrorELCP1_T Read_ResponseErrorELCP1_ResponseErrorELCP1;

  boolean P1VR3_ELCP1_Installed_v_data;

  uint8 ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl;

  /*************************************************
  * Direct Function Accesses
  *************************************************/

  P1VR3_ELCP1_Installed_v_data = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Prm_P1VR3_ELCP1_Installed_v();

  IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status();

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_ComMode_LIN4_ComMode_LIN(&Read_ComMode_LIN4_ComMode_LIN);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DaytimeRunningLight_Indication_DeviceIndication(&Read_DaytimeRunningLight_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DiagActiveState_isDiagActive(&Read_DiagActiveState_isDiagActive);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DiagInfoELCP1_DiagInfo(&Read_DiagInfoELCP1_DiagInfo);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DrivingLightPlus_Indication_DeviceIndication(&Read_DrivingLightPlus_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_DrivingLight_Indication_DeviceIndication(&Read_DrivingLight_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_FrontFog_Indication_DeviceIndication(&Read_FrontFog_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_BackLightDimming_Status_Thumbwheel_stat(&Read_LIN_BackLightDimming_Status_Thumbwheel_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus(&Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus(&Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus(&Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat(&Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_LIN_LightMode_Status_1_FreeWheel_Status(&Read_LIN_LightMode_Status_1_FreeWheel_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_ParkingLight_Indication_DeviceIndication(&Read_ParkingLight_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_RearFog_Indication_DeviceIndication(&Read_RearFog_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Read_ResponseErrorELCP1_ResponseErrorELCP1(&Read_ResponseErrorELCP1_ResponseErrorELCP1);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_NEVER_RECEIVED:
      fct_error = 1;
      break;
    case RTE_E_INVALID:
      fct_error = 1;
      break;
    case RTE_E_MAX_AGE_EXCEEDED:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_BackLightDimming_Status_Thumbwheel_stat(Rte_InitValue_BackLightDimming_Status_Thumbwheel_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_BlackPanelMode_ButtonStatus_PushButtonStatus(Rte_InitValue_BlackPanelMode_ButtonStatus_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_FogLightFront_ButtonStatus_1_PushButtonStatus(Rte_InitValue_FogLightFront_ButtonStatus_1_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_FogLightRear_ButtonStatus_1_PushButtonStatus(Rte_InitValue_FogLightRear_ButtonStatus_1_PushButtonStatus);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DaytimeRunningLight_Indica_DeviceIndication(Rte_InitValue_LIN_DaytimeRunningLight_Indica_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DrivingLightPlus_Indicatio_DeviceIndication(Rte_InitValue_LIN_DrivingLightPlus_Indicatio_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_DrivingLight_Indication_DeviceIndication(Rte_InitValue_LIN_DrivingLight_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_FrontFog_Indication_DeviceIndication(Rte_InitValue_LIN_FrontFog_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_ParkingLight_Indication_DeviceIndication(Rte_InitValue_LIN_ParkingLight_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LIN_RearFog_Indication_DeviceIndication(Rte_InitValue_LIN_RearFog_Indication_DeviceIndication);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LevelingThumbwheel_stat_Thumbwheel_stat(Rte_InitValue_LevelingThumbwheel_stat_Thumbwheel_stat);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Write_LightMode_Status_1_FreeWheel_Status(Rte_InitValue_LightMode_Status_1_FreeWheel_Status);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
  }

  ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvRead_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl();

  TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_IrvWrite_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(0U);

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_44_ELCP_RAM_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(0U);
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_DiagnosticMonitor_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_DimmingAdjustment1_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_DimmingAdjustment1_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest1_ActivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }

  fct_status = TSC_ExteriorLightPanel_1_LINMastCtrl_Rte_Call_UR_ANW_ExteriorLightsRequest1_DeactivateIss();
  switch (fct_status)
  {
    case RTE_E_OK:
      fct_error = 0;
      break;
    case RTE_E_UNCONNECTED:
      fct_error = 1;
      break;
    case RTE_E_TIMEOUT:
      fct_error = 1;
      break;
    case RTE_E_Issm_IssRequest_E_NOT_OK:
      fct_error = 1;
      break;
  }


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of runnable implementation >>               DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/
}


#define ExteriorLightPanel_1_LINMastCtrl_STOP_SEC_CODE
#include "ExteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of function definition area >>            DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

static void ExteriorLightPanel_1_LINMastCtrl_TestDefines(void)
{
  /* Enumeration Data Types */

  ComMode_LIN_Type Test_ComMode_LIN_Type_V_1 = Inactive;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_2 = Diagnostic;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_3 = SwitchDetection;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_4 = ApplicationMonitoring;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_5 = Calibration;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_6 = Spare1;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_7 = Error;
  ComMode_LIN_Type Test_ComMode_LIN_Type_V_8 = NotAvailable;

  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_1 = DCM_E_POSITIVERESPONSE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_2 = DCM_E_GENERALREJECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_3 = DCM_E_SERVICENOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_4 = DCM_E_SUBFUNCTIONNOTSUPPORTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_5 = DCM_E_INCORRECTMESSAGELENGTHORINVALIDFORMAT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_6 = DCM_E_RESPONSETOOLONG;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_7 = DCM_E_BUSYREPEATREQUEST;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_8 = DCM_E_CONDITIONSNOTCORRECT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_9 = DCM_E_REQUESTSEQUENCEERROR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_10 = DCM_E_NORESPONSEFROMSUBNETCOMPONENT;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_11 = DCM_E_FAILUREPREVENTSEXECUTIONOFREQUESTEDACTION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_12 = DCM_E_REQUESTOUTOFRANGE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_13 = DCM_E_SECURITYACCESSDENIED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_14 = DCM_E_INVALIDKEY;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_15 = DCM_E_EXCEEDNUMBEROFATTEMPTS;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_16 = DCM_E_REQUIREDTIMEDELAYNOTEXPIRED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_17 = DCM_E_UPLOADDOWNLOADNOTACCEPTED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_18 = DCM_E_TRANSFERDATASUSPENDED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_19 = DCM_E_GENERALPROGRAMMINGFAILURE;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_20 = DCM_E_WRONGBLOCKSEQUENCECOUNTER;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_21 = DCM_E_REQUESTCORRECTLYRECEIVEDRESPONSEPENDING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_22 = DCM_E_SUBFUNCTIONNOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_23 = DCM_E_SERVICENOTSUPPORTEDINACTIVESESSION;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_24 = DCM_E_RPMTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_25 = DCM_E_RPMTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_26 = DCM_E_ENGINEISRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_27 = DCM_E_ENGINEISNOTRUNNING;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_28 = DCM_E_ENGINERUNTIMETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_29 = DCM_E_TEMPERATURETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_30 = DCM_E_TEMPERATURETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_31 = DCM_E_VEHICLESPEEDTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_32 = DCM_E_VEHICLESPEEDTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_33 = DCM_E_THROTTLE_PEDALTOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_34 = DCM_E_THROTTLE_PEDALTOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_35 = DCM_E_TRANSMISSIONRANGENOTINNEUTRAL;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_36 = DCM_E_TRANSMISSIONRANGENOTINGEAR;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_37 = DCM_E_BRAKESWITCH_NOTCLOSED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_38 = DCM_E_SHIFTERLEVERNOTINPARK;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_39 = DCM_E_TORQUECONVERTERCLUTCHLOCKED;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_40 = DCM_E_VOLTAGETOOHIGH;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_41 = DCM_E_VOLTAGETOOLOW;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_42 = DCM_E_VMSCNC_0;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_43 = DCM_E_VMSCNC_1;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_44 = DCM_E_VMSCNC_2;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_45 = DCM_E_VMSCNC_3;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_46 = DCM_E_VMSCNC_4;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_47 = DCM_E_VMSCNC_5;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_48 = DCM_E_VMSCNC_6;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_49 = DCM_E_VMSCNC_7;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_50 = DCM_E_VMSCNC_8;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_51 = DCM_E_VMSCNC_9;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_52 = DCM_E_VMSCNC_A;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_53 = DCM_E_VMSCNC_B;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_54 = DCM_E_VMSCNC_C;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_55 = DCM_E_VMSCNC_D;
  Dcm_NegativeResponseCodeType Test_Dcm_NegativeResponseCodeType_V_56 = DCM_E_VMSCNC_E;

  Dem_EventStatusType Test_Dem_EventStatusType_V_1 = DEM_EVENT_STATUS_PASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_2 = DEM_EVENT_STATUS_FAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_3 = DEM_EVENT_STATUS_PREPASSED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_4 = DEM_EVENT_STATUS_PREFAILED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_5 = DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_6 = DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_7 = DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_8 = DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED;
  Dem_EventStatusType Test_Dem_EventStatusType_V_9 = DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED;

  DeviceIndication_T Test_DeviceIndication_T_V_1 = DeviceIndication_Off;
  DeviceIndication_T Test_DeviceIndication_T_V_2 = DeviceIndication_On;
  DeviceIndication_T Test_DeviceIndication_T_V_3 = DeviceIndication_Blink;
  DeviceIndication_T Test_DeviceIndication_T_V_4 = DeviceIndication_SpareValue;

  DiagActiveState_T Test_DiagActiveState_T_V_1 = Diag_Active_FALSE;
  DiagActiveState_T Test_DiagActiveState_T_V_2 = Diag_Active_TRUE;

  FreeWheel_Status_T Test_FreeWheel_Status_T_V_1 = FreeWheel_Status_NoMovement;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_2 = FreeWheel_Status_1StepClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_3 = FreeWheel_Status_2StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_4 = FreeWheel_Status_3StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_5 = FreeWheel_Status_4StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_6 = FreeWheel_Status_5StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_7 = FreeWheel_Status_6StepsClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_8 = FreeWheel_Status_1StepCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_9 = FreeWheel_Status_2StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_10 = FreeWheel_Status_3StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_11 = FreeWheel_Status_4StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_12 = FreeWheel_Status_5StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_13 = FreeWheel_Status_6StepsCounterClockwise;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_14 = FreeWheel_Status_Spare;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_15 = FreeWheel_Status_Error;
  FreeWheel_Status_T Test_FreeWheel_Status_T_V_16 = FreeWheel_Status_NotAvailable;

  PushButtonStatus_T Test_PushButtonStatus_T_V_1 = PushButtonStatus_Neutral;
  PushButtonStatus_T Test_PushButtonStatus_T_V_2 = PushButtonStatus_Pushed;
  PushButtonStatus_T Test_PushButtonStatus_T_V_3 = PushButtonStatus_Error;
  PushButtonStatus_T Test_PushButtonStatus_T_V_4 = PushButtonStatus_NotAvailable;

  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_1 = Thumbwheel_stat_ThumbWheelPos0;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_2 = Thumbwheel_stat_ThumbWheelPos1;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_3 = Thumbwheel_stat_ThumbWheelPos2;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_4 = Thumbwheel_stat_ThumbWheelPos3;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_5 = Thumbwheel_stat_ThumbWheelPos4;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_6 = Thumbwheel_stat_ThumbWheelPos5;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_7 = Thumbwheel_stat_ThumbWheelPos6;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_8 = Thumbwheel_stat_ThumbWheelPos7;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_9 = Thumbwheel_stat_ThumbWheelPos8;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_10 = Thumbwheel_stat_ThumbWheelPos9;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_11 = Thumbwheel_stat_ThumbWheelPos10;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_12 = Thumbwheel_stat_ThumbWheelPos11;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_13 = Thumbwheel_stat_ThumbWheelPos12;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_14 = Thumbwheel_stat_ThumbWheelPos13;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_15 = Thumbwheel_stat_ThumbWheelPos14;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_16 = Thumbwheel_stat_ThumbWheelPos15;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_17 = Thumbwheel_stat_ThumbWheelPos16;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_18 = Thumbwheel_stat_Spare;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_19 = Thumbwheel_stat_Spare_01;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_20 = Thumbwheel_stat_Spare_02;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_21 = Thumbwheel_stat_Spare_03;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_22 = Thumbwheel_stat_Spare_04;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_23 = Thumbwheel_stat_Spare_05;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_24 = Thumbwheel_stat_Spare_06;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_25 = Thumbwheel_stat_Spare_07;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_26 = Thumbwheel_stat_Spare_08;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_27 = Thumbwheel_stat_Spare_09;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_28 = Thumbwheel_stat_Spare_10;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_29 = Thumbwheel_stat_Spare_11;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_30 = Thumbwheel_stat_Spare_12;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_31 = Thumbwheel_stat_Error;
  Thumbwheel_stat_T Test_Thumbwheel_stat_T_V_32 = Thumbwheel_stat_NotAvaliable;
}

/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of function definition area >>              DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << Start of removed code area >>                   DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/


/**********************************************************************************************************************
 * DO NOT CHANGE THIS COMMENT!           << End of removed code area >>                     DO NOT CHANGE THIS COMMENT!
 *********************************************************************************************************************/

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_TestCode:
     Reason:     This justification is used within the generated test code by the Rte Analyzer.
     Risk:       No functional risk.
     Prevention: Not required.

*/
