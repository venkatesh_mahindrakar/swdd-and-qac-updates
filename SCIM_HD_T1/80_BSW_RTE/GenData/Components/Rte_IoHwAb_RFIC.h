/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_IoHwAb_RFIC.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <IoHwAb_RFIC>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_IOHWAB_RFIC_H
# define _RTE_IOHWAB_RFIC_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_IoHwAb_RFIC_Type.h"
# include "Rte_DataHandleType.h"


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_PEPS_APPL_CODE) RE_Check_Passive_Encryption(P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) RcvData); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RficCheckPassiveEncryption_CS(arg1) (RE_Check_Passive_Encryption(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_PEPS_APPL_CODE) RE_Clear_HighFixCheckTimer(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RficClearHighFixCheckTimer_CS() (RE_Clear_HighFixCheckTimer(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_RKE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_RKE_APPL_CODE) RE_Confirm_RKE_Event(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_RKE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RficIsrLogic_CS() (RE_Confirm_RKE_Event(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_PEPS_APPL_CODE) RE_Set_ValidFobFoundResult(uint8 fobnum); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RficSetValidFobFoundResult_CS(arg1) (RE_Set_ValidFobFoundResult(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define IoHwAb_RFIC_START_SEC_CODE
# include "IoHwAb_RFIC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_IoHwAb_RFIC_10ms_runnable IoHwAb_RFIC_10ms_runnable
#  define RTE_RUNNABLE_RE_IoHwAb_IcuNotification_RFIC_IRQ IoHwAb_IcuNotification_RFIC_IRQ
#  define RTE_RUNNABLE_RE_RFIC_Control_ISR RE_RFIC_Control_ISR
#  define RTE_RUNNABLE_RE_RFIC_WakeUp RE_RFIC_WakeUp
#  define RTE_RUNNABLE_RE_RficDioInterface_Read RE_RficDioInterface_Read
#  define RTE_RUNNABLE_RE_RficSysModeConfirm_RF RE_RficSysModeConfirm_RF
#  define RTE_RUNNABLE_RE_SearchSysmode RE_SearchSysmode
#  define RTE_RUNNABLE_RE_SetRcvRkeData SetRcvRkeData
#  define RTE_RUNNABLE_RE_rfic_IRQ_ActiveCheck RE_rfic_IRQ_ActiveCheck
#  define RTE_RUNNABLE_RE_rfic_IRQ_PendingCheck RE_rfic_IRQ_PendingCheck
#  define RTE_RUNNABLE_RE_rfic_init RE_rfic_init
# endif

FUNC(void, IoHwAb_RFIC_CODE) IoHwAb_RFIC_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, IoHwAb_RFIC_CODE) IoHwAb_IcuNotification_RFIC_IRQ(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, IoHwAb_RFIC_CODE) RE_RFIC_Control_ISR(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, IoHwAb_RFIC_CODE) RE_RFIC_WakeUp(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, IoHwAb_RFIC_CODE) RE_RficDioInterface_Read(P2VAR(IOHWAB_UINT8, AUTOMATIC, RTE_IOHWAB_RFIC_APPL_VAR) ReadValue); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_RFIC_CODE) RE_RficSysModeConfirm_RF(P2VAR(uint8, AUTOMATIC, RTE_IOHWAB_RFIC_APPL_VAR) RficSystemMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_RFIC_CODE) RE_SearchSysmode(uint8 RficSystemMode, uint8 RFIC_ReqCmd); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_RFIC_CODE) SetRcvRkeData(P2VAR(st_RKEdata, AUTOMATIC, RTE_IOHWAB_RFIC_APPL_VAR) RcvRKEDataonRFIC); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_RFIC_CODE) RE_rfic_IRQ_ActiveCheck(uint8 kb_TerminalControlState); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_RFIC_CODE) RE_rfic_IRQ_PendingCheck(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_RFIC_CODE) RE_rfic_init(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define IoHwAb_RFIC_STOP_SEC_CODE
# include "IoHwAb_RFIC_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_Dio_I_IoHwAbApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_IOHWAB_RFIC_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
