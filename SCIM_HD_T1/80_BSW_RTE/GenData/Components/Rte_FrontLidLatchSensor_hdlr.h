/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_FrontLidLatchSensor_hdlr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <FrontLidLatchSensor_hdlr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_FRONTLIDLATCHSENSOR_HDLR_H
# define _RTE_FRONTLIDLATCHSENSOR_HDLR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_FrontLidLatchSensor_hdlr_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(FrontLidLatch_stat_T, RTE_VAR_NOINIT) Rte_FrontLidLatchSensor_hdlr_FrontLidLatch_stat_FrontLidLatch_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_FrontLidLatch_stat_FrontLidLatch_stat (7U)
#  define Rte_InitValue_SwcActivation_Security_SwcActivation_Security (1U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_SwcActivation_Security_SwcActivation_Security Rte_Read_FrontLidLatchSensor_hdlr_SwcActivation_Security_SwcActivation_Security
#  define Rte_Read_FrontLidLatchSensor_hdlr_SwcActivation_Security_SwcActivation_Security(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_FrontLidLatch_stat_FrontLidLatch_stat Rte_Write_FrontLidLatchSensor_hdlr_FrontLidLatch_stat_FrontLidLatch_stat
#  define Rte_Write_FrontLidLatchSensor_hdlr_FrontLidLatch_stat_FrontLidLatch_stat(data) (Rte_FrontLidLatchSensor_hdlr_FrontLidLatch_stat_FrontLidLatch_stat = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define FrontLidLatchSensor_hdlr_START_SEC_CODE
# include "FrontLidLatchSensor_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_FrontLidLatchSensor_hdlr_20ms_runnable FrontLidLatchSensor_hdlr_20ms_runnable
# endif

FUNC(void, FrontLidLatchSensor_hdlr_CODE) FrontLidLatchSensor_hdlr_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define FrontLidLatchSensor_hdlr_STOP_SEC_CODE
# include "FrontLidLatchSensor_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_FRONTLIDLATCHSENSOR_HDLR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
