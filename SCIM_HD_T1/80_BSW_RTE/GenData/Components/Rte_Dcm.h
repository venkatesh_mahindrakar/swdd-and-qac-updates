/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Dcm.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <Dcm>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DCM_H
# define _RTE_DCM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Dcm_Type.h"
# include "Rte_DataHandleType.h"


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54(Dcm_CommunicationModeType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmControlDtcSetting_DcmControlDtcSetting(Dcm_ControlDtcSettingType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(Dcm_DiagnosticSessionControlType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Switch_Dcm_DcmEcuReset_DcmEcuReset(Dcm_EcuResetType nextMode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_SwitchAck_Dcm_DcmEcuReset_DcmEcuReset(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Switch_<p>_<m>
 *********************************************************************************************************************/
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c_DcmCommunicationControl_ComMConf_ComMChannel_CN_Backbone2_78967e2c
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1 Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1_DcmCommunicationControl_ComMConf_ComMChannel_CN_CabSubnet_9ea693f1
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5 Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5_DcmCommunicationControl_ComMConf_ComMChannel_CN_FMSNet_fce1aae5
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN00_2cd9a7df
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749 Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN01_5bde9749
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3 Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN02_c2d7c6f3
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665 Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN03_b5d0f665
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6 Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6_DcmCommunicationControl_ComMConf_ComMChannel_CN_LIN04_2bb463c6
#  define Rte_Switch_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54 Rte_Switch_Dcm_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54_DcmCommunicationControl_ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54
#  define Rte_Switch_DcmControlDtcSetting_DcmControlDtcSetting Rte_Switch_Dcm_DcmControlDtcSetting_DcmControlDtcSetting
#  define Rte_Switch_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl Rte_Switch_Dcm_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl
#  define Rte_Switch_DcmEcuReset_DcmEcuReset Rte_Switch_Dcm_DcmEcuReset_DcmEcuReset


/**********************************************************************************************************************
 * Rte_Feedback_<p>_<m> (mode switch acknowledge)
 *********************************************************************************************************************/
#  define Rte_SwitchAck_DcmEcuReset_DcmEcuReset Rte_SwitchAck_Dcm_DcmEcuReset_DcmEcuReset


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_CHANO_Data_CHANO_ChassisId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_CHANO_Data_CHANO_ChassisId_ReadData(P2VAR(Dcm_Data16ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_CHANO_Data_CHANO_ReadData DataServices_CHANO_Data_CHANO_ChassisId_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1AFR_Data_P1AFR_ReadData DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1AFS_Data_P1AFS_Odometer_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1AFS_Data_P1AFS_Odometer_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1AFS_Data_P1AFS_ReadData DataServices_P1AFS_Data_P1AFS_Odometer_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1AFT_Data_P1AFT_ReadData DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data406ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1ALA_Data_P1ALA_ReadData DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength(Dcm_OpStatusType OpStatus, P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1ALA_Data_P1ALA_ReadDataLength DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1ALB_Data_P1ALB_ReadData DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData(P2VAR(Dcm_Data241ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1ALP_Data_P1ALP_ReadData DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength(P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1ALP_Data_P1ALP_ReadDataLength DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData(P2VAR(Dcm_Data241ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1ALQ_Data_P1ALQ_ReadData DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength(P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1ALQ_Data_P1ALQ_ReadDataLength DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1B0T_Data_P1B0T_ReadData DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData(P2VAR(Dcm_Data221ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1B1O_Data_P1B1O_ReadData DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BW0_Data_P1BW0_ReadData DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BW1_Data_P1BW1_ReadData DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BW3_Data_P1BW3_ReadData DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BW4_Data_P1BW4_ReadData DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BW5_Data_P1BW5_ReadData DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BW6_Data_P1BW6_ReadData DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BW8_Data_P1BW8_ReadData DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BW9_Data_P1BW9_ReadData DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWI_Data_P1BWI_ReadData DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWJ_Data_P1BWJ_ReadData DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWL_Data_P1BWL_ReadData DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWM_Data_P1BWM_ReadData DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWN_Data_P1BWN_ReadData DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWO_Data_P1BWO_ReadData DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWQ_Data_P1BWQ_ReadData DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWR_Data_P1BWR_ReadData DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWV_Data_P1BWV_ReadData DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BWX_Data_P1BWX_ReadData DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BXD_Data_P1BXD_ReadData DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1BXF_Data_P1BXF_ReadData DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData(P2VAR(Dcm_Data6ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1C1R_Data_P1C1R_ReadData DataServices_P1C1R_Data_P1C1R_NbFSP_ReadData
#  define RTE_START_SEC_LINMGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_LINMGR_APPL_CODE) DataServices_P1CXF_Data_P1CXF_FCI_ReadData(P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_LINMGR_APPL_CODE) DataServices_P1CXF_Data_P1CXF_FCI_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_LINMGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_LINMGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1CXF_Data_P1CXF_ReadData DataServices_P1CXF_Data_P1CXF_FCI_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1DCT_Data_P1DCT_ReadData DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData(P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1DCU_Data_P1DCU_ReadData DataServices_P1DCU_Data_P1DCU_SnapshotFSPinFailure_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1DIH_Data_P1DIH_ReadData DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData
#  define RTE_START_SEC_KEYFOB_UICTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_UICTRL_APPL_CODE) DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_UICTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_UICTRL_APPL_CODE) DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData(P2VAR(Dcm_Data12ByteType, AUTOMATIC, RTE_KEYFOB_UICTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_UICTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1DJ8_Data_P1DJ8_ReadData DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1DS3_Data_P1DS3_ReadData DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData
#  define RTE_START_SEC_LEVELCONTROL_HMICTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_LEVELCONTROL_HMICTRL_APPL_CODE) DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(P2VAR(uint8, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_LEVELCONTROL_HMICTRL_APPL_CODE) DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_LEVELCONTROL_HMICTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_LEVELCONTROL_HMICTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1DVZ_Data_P1DVZ_ReadData DataServices_P1DVZ_Data_P1DVZ_ECSStandbyTimer_ReadData
#  define RTE_START_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EIJ_Data_P1EIJ_FreezeCurrentState DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState
#  define RTE_START_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EIJ_Data_P1EIJ_ReadData DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData
#  define RTE_START_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EIJ_Data_P1EIJ_ReturnControlToECU DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU
#  define RTE_START_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EIJ_Data_P1EIJ_ShortTermAdjustment DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment
#  define RTE_START_SEC_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOQ_Data_P1EOQ_FreezeCurrentState DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_START_SEC_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOQ_Data_P1EOQ_ReadData DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReadData
#  define RTE_START_SEC_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOQ_Data_P1EOQ_ReturnControlToECU DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_START_SEC_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE) DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_SPEEDCONTROLFREEWHEEL_LINMASCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOQ_Data_P1EOQ_ShortTermAdjustment DataServices_P1EOQ_Data_P1EOQ_CCFW_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_START_SEC_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOR_Data_P1EOR_FreezeCurrentState DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_START_SEC_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOR_Data_P1EOR_ReadData DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReadData
#  define RTE_START_SEC_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOR_Data_P1EOR_ReturnControlToECU DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_START_SEC_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE) DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIFFLOCKPANEL_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIFFLOCKPANEL_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOR_Data_P1EOR_ShortTermAdjustment DataServices_P1EOR_Data_P1EOR_DLFW_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_START_SEC_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOS_Data_P1EOS_FreezeCurrentState DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_START_SEC_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOS_Data_P1EOS_ReadData DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData
#  define RTE_START_SEC_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOS_Data_P1EOS_ReturnControlToECU DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_START_SEC_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOS_Data_P1EOS_ShortTermAdjustment DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_START_SEC_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOT_Data_P1EOT_FreezeCurrentState DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_START_SEC_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOT_Data_P1EOT_ReadData DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReadData
#  define RTE_START_SEC_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOT_Data_P1EOT_ReturnControlToECU DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_START_SEC_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE) DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_EXTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOT_Data_P1EOT_ShortTermAdjustment DataServices_P1EOT_Data_P1EOT_ELCP2_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_START_SEC_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOU_Data_P1EOU_FreezeCurrentState DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_START_SEC_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOU_Data_P1EOU_ReadData DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReadData
#  define RTE_START_SEC_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOU_Data_P1EOU_ReturnControlToECU DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_START_SEC_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE) DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_INTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOU_Data_P1EOU_ShortTermAdjustment DataServices_P1EOU_Data_P1EOU_ILCP1_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_START_SEC_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOV_Data_P1EOV_FreezeCurrentState DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_START_SEC_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOV_Data_P1EOV_ReadData DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReadData
#  define RTE_START_SEC_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOV_Data_P1EOV_ReturnControlToECU DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_START_SEC_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE) DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_ECSWIREDREMOTE_LINMASTERCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_ECSWIREDREMOTE_LINMASTERCTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOV_Data_P1EOV_ShortTermAdjustment DataServices_P1EOV_Data_P1EOV_RCECS_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOW_Data_P1EOW_FreezeCurrentState DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOW_Data_P1EOW_ReadData DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOW_Data_P1EOW_ReturnControlToECU DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1EOW_Data_P1EOW_ShortTermAdjustment DataServices_P1EOW_Data_P1EOW_FSP_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1FDL_Data_P1FDL_ReadData DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1FM6_Data_P1FM6_ReadData DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData(P2VAR(Dcm_Data120ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1GCM_Data_P1GCM_ReadData DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE) DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData(P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1ILR_Data_P1ILR_ReadData DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(P2VAR(Dcm_Data256ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1KAO_Data_P1KAO_ReadData DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(P2CONST(Dcm_Data256ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1KAO_Data_P1KAO_WriteData DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData(P2VAR(Dcm_Data64ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1OLT_Data_P1OLT_ReadData DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData(P2VAR(Dcm_Data64ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1Q82_Data_P1Q82_ReadData DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1QXI_Data_P1QXI_ReadData DataServices_P1QXI_Data_P1QXI_Diag_ReadbyId_PWR24V_ReadData
#  define RTE_START_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1QXJ_Data_P1QXJ_ReadData DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1QXM_Data_P1QXM_ReadData DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1QXP_Data_P1QXP_ReadData DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1QXR_Data_P1QXR_ReadData DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_APPL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_APPL_APPL_CODE) DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_APPL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_APPL_APPL_CODE) DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_APPL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_APPL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1QXU_Data_P1QXU_ReadData DataServices_P1QXU_Data_P1QXU_EcuNotConfiguredFailureCause_ReadData
#  define RTE_START_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEHICLEMODEDISTRIBUTION_APPL_CODE) DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEHICLEMODEDISTRIBUTION_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1RG1_Data_P1RG1_ReadData DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData
#  define RTE_START_SEC_DRIVERAUTHENTICATION2_CTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_CODE) DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_CODE) DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData(P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_DRIVERAUTHENTICATION2_CTRL_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DRIVERAUTHENTICATION2_CTRL_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VKH_Data_P1VKH_ReadData DataServices_P1VKH_Data_P1VKH_VINCheck_Status_ReadData
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VKK_Data_P1VKK_ReadData DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VQ1_Data_P1VQ1_ReadData DataServices_P1VQ1_Data_P1VQ1_Diag_ReadById_AO12_L_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VQ2_Data_P1VQ2_ReadData DataServices_P1VQ2_Data_P1VQ2_Diag_ReadById_AO12_P_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VQ3_Data_P1VQ3_ReadData DataServices_P1VQ3_Data_P1VQ3_Diag_ReadById_DOWHS01_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VQ4_Data_P1VQ4_ReadData DataServices_P1VQ4_Data_P1VQ4_Diag_ReadById_DOWHS02_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VQ5_Data_P1VQ5_ReadData DataServices_P1VQ5_Data_P1VQ5_Diag_ReadById_DOBHS01_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VQ6_Data_P1VQ6_ReadData DataServices_P1VQ6_Data_P1VQ6_Diag_ReadById_DOBHS02_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VQ7_Data_P1VQ7_ReadData DataServices_P1VQ7_Data_P1VQ7_Diag_ReadById_DOBHS03_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VQ8_Data_P1VQ8_ReadData DataServices_P1VQ8_Data_P1VQ8_Diag_ReadById_DOBHS04_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VQ9_Data_P1VQ9_ReadData DataServices_P1VQ9_Data_P1VQ9_Diag_ReadById_ADI01_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR0_Data_P1VR0_FreezeCurrentState DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR0_Data_P1VR0_ReadData DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR0_Data_P1VR0_ReturnControlToECU DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR0_Data_P1VR0_ShortTermAdjustment DataServices_P1VR0_Data_P1VR0_Diag_IOCTL_DOWHS01_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR4_Data_P1VR4_FreezeCurrentState DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR4_Data_P1VR4_ReadData DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR4_Data_P1VR4_ReturnControlToECU DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR4_Data_P1VR4_ShortTermAdjustment DataServices_P1VR4_Data_P1VR4_Diag_IOCTL_DOWHS02_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR6_Data_P1VR6_FreezeCurrentState DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR6_Data_P1VR6_ReadData DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR6_Data_P1VR6_ReturnControlToECU DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR6_Data_P1VR6_ShortTermAdjustment DataServices_P1VR6_Data_P1VR6_Diag_IOCTL_DOBHS01_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR7_Data_P1VR7_FreezeCurrentState DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR7_Data_P1VR7_ReadData DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR7_Data_P1VR7_ReturnControlToECU DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR7_Data_P1VR7_ShortTermAdjustment DataServices_P1VR7_Data_P1VR7_Diag_IOCTL_DOBHS02_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR8_Data_P1VR8_FreezeCurrentState DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR8_Data_P1VR8_ReadData DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR8_Data_P1VR8_ReturnControlToECU DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR8_Data_P1VR8_ShortTermAdjustment DataServices_P1VR8_Data_P1VR8_Diag_IOCTL_DOBHS03_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR9_Data_P1VR9_FreezeCurrentState DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR9_Data_P1VR9_ReadData DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR9_Data_P1VR9_ReturnControlToECU DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VR9_Data_P1VR9_ShortTermAdjustment DataServices_P1VR9_Data_P1VR9_Diag_IOCTL_DOBHS04_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRA_Data_P1VRA_ReadData DataServices_P1VRA_Data_P1VRA_Diag_ReadById_ADI02_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRB_Data_P1VRB_ReadData DataServices_P1VRB_Data_P1VRB_Diag_ReadById_ADI03_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRC_Data_P1VRC_ReadData DataServices_P1VRC_Data_P1VRC_Diag_ReadById_ADI04_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRD_Data_P1VRD_ReadData DataServices_P1VRD_Data_P1VRD_Diag_ReadById_ADI05_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRE_Data_P1VRE_ReadData DataServices_P1VRE_Data_P1VRE_Diag_ReadById_ADI06_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRF_Data_P1VRF_ReadData DataServices_P1VRF_Data_P1VRF_Diag_ReadById_ADI07_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRG_Data_P1VRG_ReadData DataServices_P1VRG_Data_P1VRG_Diag_ReadById_ADI08_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRH_Data_P1VRH_ReadData DataServices_P1VRH_Data_P1VRH_Diag_ReadById_ADI09_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRI_Data_P1VRI_ReadData DataServices_P1VRI_Data_P1VRI_Diag_ReadById_ADI10_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRJ_Data_P1VRJ_ReadData DataServices_P1VRJ_Data_P1VRJ_Diag_ReadById_ADI11_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRK_Data_P1VRK_ReadData DataServices_P1VRK_Data_P1VRK_Diag_ReadById_ADI12_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRL_Data_P1VRL_ReadData DataServices_P1VRL_Data_P1VRL_Diag_ReadById_ADI13_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRM_Data_P1VRM_ReadData DataServices_P1VRM_Data_P1VRM_Diag_ReadById_ADI14_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRN_Data_P1VRN_ReadData DataServices_P1VRN_Data_P1VRN_Diag_ReadById_ADI15_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRO_Data_P1VRO_ReadData DataServices_P1VRO_Data_P1VRO_Diag_ReadById_ADI16_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRS_Data_P1VRS_ReadData DataServices_P1VRS_Data_P1VRS_Diag_ReadById_DOWLS01_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRT_Data_P1VRT_ReadData DataServices_P1VRT_Data_P1VRT_Diag_ReadById_DOWLS02_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRU_Data_P1VRU_ReadData DataServices_P1VRU_Data_P1VRU_Diag_ReadById_DOWLS03_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRV_Data_P1VRV_ReadData DataServices_P1VRV_Data_P1VRV_Diag_ReadById_DAI01_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRW_Data_P1VRW_ReadData DataServices_P1VRW_Data_P1VRW_Diag_ReadById_DAI02_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRZ_Data_P1VRZ_FreezeCurrentState DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRZ_Data_P1VRZ_ReadData DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRZ_Data_P1VRZ_ReturnControlToECU DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VRZ_Data_P1VRZ_ShortTermAdjustment DataServices_P1VRZ_Data_P1VRZ_Diag_IOCTL_AO12_P_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSB_Data_P1VSB_FreezeCurrentState DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSB_Data_P1VSB_ReadData DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSB_Data_P1VSB_ReturnControlToECU DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSB_Data_P1VSB_ShortTermAdjustment DataServices_P1VSB_Data_P1VSB_Diag_IOCTL_DOWLS01_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSC_Data_P1VSC_FreezeCurrentState DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSC_Data_P1VSC_ReadData DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSC_Data_P1VSC_ReturnControlToECU DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSC_Data_P1VSC_ShortTermAdjustment DataServices_P1VSC_Data_P1VSC_Diag_IOCTL_DOWLS02_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSD_Data_P1VSD_FreezeCurrentState DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSD_Data_P1VSD_ReadData DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSD_Data_P1VSD_ReturnControlToECU DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSD_Data_P1VSD_ShortTermAdjustment DataServices_P1VSD_Data_P1VSD_Diag_IOCTL_DOWLS03_ShortTermAdjustment
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSG_Data_P1VSG_FreezeCurrentState DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSG_Data_P1VSG_ReadData DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSG_Data_P1VSG_ReturnControlToECU DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_P1VSG_Data_P1VSG_ShortTermAdjustment DataServices_P1VSG_Data_P1VSG_Diag_IOCTL_AO12_L_ShortTermAdjustment
#  define RTE_START_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_VINNO_Data_VINNO_VIN_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VOL_DIDSERVER_APPL_CODE) DataServices_VINNO_Data_VINNO_VIN_ReadData(P2VAR(Dcm_Data17ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VOL_DIDSERVER_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_VINNO_Data_VINNO_ReadData DataServices_VINNO_Data_VINNO_VIN_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData(P2VAR(Dcm_Data80ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1C12_Data_X1C12_ReadData DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData(P2CONST(Dcm_Data80ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1C12_Data_X1C12_WriteData DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData(P2VAR(Dcm_Data130ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1C13_Data_X1C13_ReadData DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData(P2CONST(Dcm_Data130ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1C13_Data_X1C13_WriteData DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData(P2VAR(Dcm_Data126ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1C1U_Data_X1C1U_ReadData DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData(P2CONST(Dcm_Data126ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1C1U_Data_X1C1U_WriteData DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_CORE_APPL_CODE) DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData(P2VAR(Dcm_Data60ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1C1Z_Data_X1C1Z_ReadData DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1CV5_Data_X1CV5_ReadData DataServices_X1CV5_Data_X1CV5_Diag_ReadById_DCDC12_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1CV7_Data_X1CV7_FreezeCurrentState DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_FreezeCurrentState
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1CV7_Data_X1CV7_ReadData DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReadData
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1CV7_Data_X1CV7_ReturnControlToECU DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ReturnControlToECU
#  define RTE_START_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICMONITOR_IO_APPL_CODE) DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_IO_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICMONITOR_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1CV7_Data_X1CV7_ShortTermAdjustment DataServices_X1CV7_Data_X1CV7_Diag_IOCTL_DCDC12_ShortTermAdjustment
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_PEPS_APPL_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData(P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_PEPS_APPL_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData(P2VAR(Dcm_Data40ByteType, AUTOMATIC, RTE_PEPS_APPL_VAR) Data); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1CY4_Data_X1CY4_ReadData DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_ReadData
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_PEPS_APPL_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData(P2CONST(uint8, AUTOMATIC, RTE_PEPS_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_PEPS_APPL_CODE) DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData(P2CONST(Dcm_Data40ByteType, AUTOMATIC, RTE_PEPS_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_DataServices_X1CY4_Data_X1CY4_WriteData DataServices_X1CY4_Data_X1CY4_AntMappingCalib_Multi_WriteData
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_R1AAA_RequestResults RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_R1AAA_Start RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_R1AAA_Stop RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data7ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_R1AAI_RequestResults RoutineServices_R1AAI_FspAssignmentRoutine_RequestResults
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Start(uint8 In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_R1AAI_Start RoutineServices_R1AAI_FspAssignmentRoutine_Start
#  define RTE_START_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE) RoutineServices_R1AAI_FspAssignmentRoutine_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_FLEXIBLESWITCHESROUTER_CTRL_LINMSTR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_R1AAI_Stop RoutineServices_R1AAI_FspAssignmentRoutine_Stop
#  define RTE_START_SEC_LINMGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_LINMGR_APPL_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_LINMGR_APPL_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_LINMGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_R1AAJ_RequestResults RoutineServices_R1AAJ_FlexibleSwitchDetection_RequestResults
#  define RTE_START_SEC_LINMGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_LINMGR_APPL_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_LINMGR_APPL_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_LINMGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_LINMGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_R1AAJ_Start RoutineServices_R1AAJ_FlexibleSwitchDetection_Start
#  define RTE_START_SEC_LINMGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_LINMGR_APPL_CODE) RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_LINMGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_LINMGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_R1AAJ_Stop RoutineServices_R1AAJ_FlexibleSwitchDetection_Stop
#  define RTE_START_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) RoutineServices_Y1ABD_ClearECU_Start(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_KEYFOB_MGR_APPL_CODE) RoutineServices_Y1ABD_ClearECU_Start(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_KEYFOB_MGR_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_Y1ABD_Start RoutineServices_Y1ABD_ClearECU_Start
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_PEPS_APPL_CODE) RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_PEPS_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_PEPS_APPL_CODE) RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_PEPS_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_PEPS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RoutineServices_Y1ABE_Start RoutineServices_Y1ABE_AntennaMappingByDidActivation_Start
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_01_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_01_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_01_CompareKey VEC_SecurityAccess_SA_Seed_01_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_01_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_01_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_01_GetSeed VEC_SecurityAccess_SA_Seed_01_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_07_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_07_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_07_CompareKey VEC_SecurityAccess_SA_Seed_07_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_07_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_07_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_07_GetSeed VEC_SecurityAccess_SA_Seed_07_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_09_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_09_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_09_CompareKey VEC_SecurityAccess_SA_Seed_09_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_09_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_09_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_09_GetSeed VEC_SecurityAccess_SA_Seed_09_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0B_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_0B_CompareKey VEC_SecurityAccess_SA_Seed_0B_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_0B_GetSeed VEC_SecurityAccess_SA_Seed_0B_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0D_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_0D_CompareKey VEC_SecurityAccess_SA_Seed_0D_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_0D_GetSeed VEC_SecurityAccess_SA_Seed_0D_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0F_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_0F_CompareKey VEC_SecurityAccess_SA_Seed_0F_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_0F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_0F_GetSeed VEC_SecurityAccess_SA_Seed_0F_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_11_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_11_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_11_CompareKey VEC_SecurityAccess_SA_Seed_11_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_11_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_11_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_11_GetSeed VEC_SecurityAccess_SA_Seed_11_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_15_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_15_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_15_CompareKey VEC_SecurityAccess_SA_Seed_15_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_15_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_15_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_15_GetSeed VEC_SecurityAccess_SA_Seed_15_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_17_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_17_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_17_CompareKey VEC_SecurityAccess_SA_Seed_17_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_17_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_17_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_17_GetSeed VEC_SecurityAccess_SA_Seed_17_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_1B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_1B_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_1B_CompareKey VEC_SecurityAccess_SA_Seed_1B_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_1B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_1B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_1B_GetSeed VEC_SecurityAccess_SA_Seed_1B_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_29_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_29_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_29_CompareKey VEC_SecurityAccess_SA_Seed_29_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_29_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_29_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_29_GetSeed VEC_SecurityAccess_SA_Seed_29_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2B_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_2B_CompareKey VEC_SecurityAccess_SA_Seed_2B_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_2B_GetSeed VEC_SecurityAccess_SA_Seed_2B_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2D_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_2D_CompareKey VEC_SecurityAccess_SA_Seed_2D_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_2D_GetSeed VEC_SecurityAccess_SA_Seed_2D_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2F_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_2F_CompareKey VEC_SecurityAccess_SA_Seed_2F_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_2F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_2F_GetSeed VEC_SecurityAccess_SA_Seed_2F_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_31_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_31_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_31_CompareKey VEC_SecurityAccess_SA_Seed_31_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_31_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_31_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_31_GetSeed VEC_SecurityAccess_SA_Seed_31_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_33_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_33_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_33_CompareKey VEC_SecurityAccess_SA_Seed_33_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_33_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_33_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_33_GetSeed VEC_SecurityAccess_SA_Seed_33_GetSeed
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_37_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_37_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_37_CompareKey VEC_SecurityAccess_SA_Seed_37_CompareKey
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_37_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_SA_Seed_37_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SecurityAccess_SA_Seed_37_GetSeed VEC_SecurityAccess_SA_Seed_37_GetSeed
#  define RTE_START_SEC_DIAGNOSTICCOMPONENT_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DIAGNOSTICCOMPONENT_APPL_CODE) DcmRequestManufacturerNotification_Confirmation(uint8 SID, uint8 ReqType, uint16 SourceAddress, Dcm_ConfirmationStatusType ConfirmationStatus); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  define RTE_STOP_SEC_DIAGNOSTICCOMPONENT_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Confirmation DcmRequestManufacturerNotification_Confirmation
#  define RTE_START_SEC_DIAGNOSTICCOMPONENT_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_DIAGNOSTICCOMPONENT_APPL_CODE) DcmRequestManufacturerNotification_Indication(uint8 SID, P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_DATA) RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  else
FUNC(Std_ReturnType, RTE_DIAGNOSTICCOMPONENT_APPL_CODE) DcmRequestManufacturerNotification_Indication(uint8 SID, P2CONST(Dcm_Data2000ByteType, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_DATA) RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) ErrorCode); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
#  endif
#  define RTE_STOP_SEC_DIAGNOSTICCOMPONENT_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_ServiceRequestManufacturerNotification_DcmDslServiceRequestManufacturerNotification_Indication DcmRequestManufacturerNotification_Indication


# endif /* !defined(RTE_CORE) */


# define Dcm_START_SEC_CODE
# include "Dcm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_Dcm_MainFunction Dcm_MainFunction
#  define RTE_RUNNABLE_GetActiveProtocol Dcm_GetActiveProtocol
#  define RTE_RUNNABLE_GetRequestKind Dcm_GetRequestKind
#  define RTE_RUNNABLE_GetSecurityLevel Dcm_GetSecurityLevel
#  define RTE_RUNNABLE_GetSesCtrlType Dcm_GetSesCtrlType
#  define RTE_RUNNABLE_ResetToDefaultSession Dcm_ResetToDefaultSession
#  define RTE_RUNNABLE_SetActiveDiagnostic Dcm_SetActiveDiagnostic
# endif

FUNC(void, Dcm_CODE) Dcm_MainFunction(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetActiveProtocol(P2VAR(Dcm_ProtocolType, AUTOMATIC, RTE_DCM_APPL_VAR) ActiveProtocol); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetRequestKind(uint16 TesterSourceAddress, P2VAR(Dcm_RequestKindType, AUTOMATIC, RTE_DCM_APPL_VAR) RequestKind); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetSecurityLevel(P2VAR(Dcm_SecLevelType, AUTOMATIC, RTE_DCM_APPL_VAR) SecLevel); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_GetSesCtrlType(P2VAR(Dcm_SesCtrlType, AUTOMATIC, RTE_DCM_APPL_VAR) SesCtrlType); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_ResetToDefaultSession(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(Std_ReturnType, Dcm_CODE) Dcm_SetActiveDiagnostic(boolean active); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define Dcm_STOP_SEC_CODE
# include "Dcm_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DCMServices_E_NOT_OK (1U)

#  define RTE_E_DCMServices_E_OK (0U)

#  define RTE_E_DataServices_CHANO_Data_CHANO_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1AFR_Data_P1AFR_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1AFS_Data_P1AFS_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1AFT_Data_P1AFT_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ALB_Data_P1ALB_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ALP_Data_P1ALP_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ALQ_Data_P1ALQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1B0T_Data_P1B0T_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1B1O_Data_P1B1O_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW0_Data_P1BW0_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW1_Data_P1BW1_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW3_Data_P1BW3_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW4_Data_P1BW4_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW5_Data_P1BW5_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW6_Data_P1BW6_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW8_Data_P1BW8_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW9_Data_P1BW9_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWI_Data_P1BWI_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWJ_Data_P1BWJ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWL_Data_P1BWL_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWM_Data_P1BWM_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWN_Data_P1BWN_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWO_Data_P1BWO_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWQ_Data_P1BWQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWR_Data_P1BWR_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWV_Data_P1BWV_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWX_Data_P1BWX_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BXD_Data_P1BXD_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BXF_Data_P1BXF_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1C1R_Data_P1C1R_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1CXF_Data_P1CXF_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DCT_Data_P1DCT_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DCU_Data_P1DCU_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DIH_Data_P1DIH_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DJ8_Data_P1DJ8_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DS3_Data_P1DS3_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DVZ_Data_P1DVZ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1EOQ_Data_P1EOQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1EOR_Data_P1EOR_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1EOT_Data_P1EOT_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1EOU_Data_P1EOU_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1EOV_Data_P1EOV_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1EOW_Data_P1EOW_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1FDL_Data_P1FDL_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1FM6_Data_P1FM6_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1GCM_Data_P1GCM_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ILR_Data_P1ILR_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1OLT_Data_P1OLT_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1Q82_Data_P1Q82_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1QXI_Data_P1QXI_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1QXJ_Data_P1QXJ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1QXM_Data_P1QXM_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1QXP_Data_P1QXP_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1QXR_Data_P1QXR_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1QXU_Data_P1QXU_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1RG1_Data_P1RG1_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VKH_Data_P1VKH_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VKK_Data_P1VKK_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ1_Data_P1VQ1_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ2_Data_P1VQ2_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ3_Data_P1VQ3_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ4_Data_P1VQ4_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ5_Data_P1VQ5_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ6_Data_P1VQ6_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ7_Data_P1VQ7_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ8_Data_P1VQ8_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VQ9_Data_P1VQ9_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR0_Data_P1VR0_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR4_Data_P1VR4_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR6_Data_P1VR6_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR7_Data_P1VR7_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR8_Data_P1VR8_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VR9_Data_P1VR9_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRA_Data_P1VRA_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRB_Data_P1VRB_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRC_Data_P1VRC_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRD_Data_P1VRD_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRE_Data_P1VRE_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRF_Data_P1VRF_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRG_Data_P1VRG_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRH_Data_P1VRH_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRI_Data_P1VRI_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRJ_Data_P1VRJ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRK_Data_P1VRK_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRL_Data_P1VRL_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRM_Data_P1VRM_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRN_Data_P1VRN_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRO_Data_P1VRO_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRS_Data_P1VRS_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRT_Data_P1VRT_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRU_Data_P1VRU_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRV_Data_P1VRV_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRW_Data_P1VRW_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VRZ_Data_P1VRZ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VSB_Data_P1VSB_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VSC_Data_P1VSC_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VSD_Data_P1VSD_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VSG_Data_P1VSG_E_NOT_OK (1U)

#  define RTE_E_DataServices_VINNO_Data_VINNO_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1C12_Data_X1C12_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1C13_Data_X1C13_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1C1U_Data_X1C1U_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1C1Z_Data_X1C1Z_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1CV5_Data_X1CV5_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1CV7_Data_X1CV7_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1CY4_Data_X1CY4_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_R1AAA_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_R1AAA_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_R1AAI_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_R1AAI_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_R1AAI_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_R1AAJ_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_R1AAJ_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_R1AAJ_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Y1ABD_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Y1ABD_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Y1ABD_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Y1ABE_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Y1ABE_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Y1ABE_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_01_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_01_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_01_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_07_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_07_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_07_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_09_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_09_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_09_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_0B_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_0B_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_0B_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_0D_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_0D_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_0D_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_0F_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_0F_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_0F_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_11_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_11_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_11_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_15_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_15_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_15_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_17_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_17_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_17_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_1B_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_1B_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_1B_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_29_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_29_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_29_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_2B_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_2B_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_2B_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_2D_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_2D_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_2D_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_2F_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_2F_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_2F_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_31_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_31_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_31_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_33_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_33_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_33_E_NOT_OK (1U)

#  define RTE_E_SecurityAccess_SA_Seed_37_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_SA_Seed_37_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_SA_Seed_37_E_NOT_OK (1U)

#  define RTE_E_ServiceRequestNotification_E_NOT_OK (1U)

#  define RTE_E_ServiceRequestNotification_E_REQUEST_NOT_ACCEPTED (8U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DCM_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
