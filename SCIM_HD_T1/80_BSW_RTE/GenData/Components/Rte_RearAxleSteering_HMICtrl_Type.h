/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_RearAxleSteering_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <RearAxleSteering_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_REARAXLESTEERING_HMICTRL_TYPE_H
# define _RTE_REARAXLESTEERING_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A2PosSwitchStatus_Off
#   define A2PosSwitchStatus_Off (0U)
#  endif

#  ifndef A2PosSwitchStatus_On
#   define A2PosSwitchStatus_On (1U)
#  endif

#  ifndef A2PosSwitchStatus_Error
#   define A2PosSwitchStatus_Error (2U)
#  endif

#  ifndef A2PosSwitchStatus_NotAvailable
#   define A2PosSwitchStatus_NotAvailable (3U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef RearAxleSteeringFunctionDsbl_NoDisableRequest
#   define RearAxleSteeringFunctionDsbl_NoDisableRequest (0U)
#  endif

#  ifndef RearAxleSteeringFunctionDsbl_DisableRequest
#   define RearAxleSteeringFunctionDsbl_DisableRequest (1U)
#  endif

#  ifndef RearAxleSteeringFunctionDsbl_Error
#   define RearAxleSteeringFunctionDsbl_Error (2U)
#  endif

#  ifndef RearAxleSteeringFunctionDsbl_NotAvailable
#   define RearAxleSteeringFunctionDsbl_NotAvailable (3U)
#  endif

#  ifndef RearAxleSteeringFunctionStatus_NotActive
#   define RearAxleSteeringFunctionStatus_NotActive (0U)
#  endif

#  ifndef RearAxleSteeringFunctionStatus_Active
#   define RearAxleSteeringFunctionStatus_Active (1U)
#  endif

#  ifndef RearAxleSteeringFunctionStatus_PassiveFailureMode
#   define RearAxleSteeringFunctionStatus_PassiveFailureMode (2U)
#  endif

#  ifndef RearAxleSteeringFunctionStatus_ActiveFailureMode
#   define RearAxleSteeringFunctionStatus_ActiveFailureMode (3U)
#  endif

#  ifndef RearAxleSteeringFunctionStatus_SpareValue
#   define RearAxleSteeringFunctionStatus_SpareValue (4U)
#  endif

#  ifndef RearAxleSteeringFunctionStatus_SpareValue_01
#   define RearAxleSteeringFunctionStatus_SpareValue_01 (5U)
#  endif

#  ifndef RearAxleSteeringFunctionStatus_Error
#   define RearAxleSteeringFunctionStatus_Error (6U)
#  endif

#  ifndef RearAxleSteeringFunctionStatus_NotAvailable
#   define RearAxleSteeringFunctionStatus_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_REARAXLESTEERING_HMICTRL_TYPE_H */
