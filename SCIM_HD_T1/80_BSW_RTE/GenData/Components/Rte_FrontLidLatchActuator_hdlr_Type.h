/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_FrontLidLatchActuator_hdlr_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <FrontLidLatchActuator_hdlr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_FRONTLIDLATCHACTUATOR_HDLR_TYPE_H
# define _RTE_FRONTLIDLATCHACTUATOR_HDLR_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef FrontLidLatch_cmd_Idle
#   define FrontLidLatch_cmd_Idle (0U)
#  endif

#  ifndef FrontLidLatch_cmd_LockFrontLidCommand
#   define FrontLidLatch_cmd_LockFrontLidCommand (1U)
#  endif

#  ifndef FrontLidLatch_cmd_UnlockFrontLidCommand
#   define FrontLidLatch_cmd_UnlockFrontLidCommand (2U)
#  endif

#  ifndef FrontLidLatch_cmd_Error
#   define FrontLidLatch_cmd_Error (6U)
#  endif

#  ifndef FrontLidLatch_cmd_NotAvailable
#   define FrontLidLatch_cmd_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_FRONTLIDLATCHACTUATOR_HDLR_TYPE_H */
