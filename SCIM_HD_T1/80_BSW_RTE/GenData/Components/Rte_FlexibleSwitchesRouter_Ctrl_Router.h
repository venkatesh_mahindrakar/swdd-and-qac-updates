/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_FlexibleSwitchesRouter_Ctrl_Router.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <FlexibleSwitchesRouter_Ctrl_Router>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_H
# define _RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_FlexibleSwitchesRouter_Ctrl_Router_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ABSInhibitSwitchStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AEBS_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ALDSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ASROffButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AlternativeDriveLevelSw_stat_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch1SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch2SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch3SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch4SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch5SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch6SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_BeaconSRocker_DeviceEvent_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Beacon_DeviceEven_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_CabWorkLight_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Construction_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ContUnlockSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_CranePushButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_CraneSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_DAS_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_DashboardLockButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EconomyPowerSwitch_status_EconomyPowerSwitch_status; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EquipmentLight_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchEnableStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchIncDecStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchMuddySiteStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchResumeStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FCW_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FPBRSwitchStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FerryFunctionSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FifthWheelLight_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FrtAxleHydro_ButtonPush_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_HillStartAidButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtActvnBtn_stat_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtMaxModeFlxSw2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeBtn_stat_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeFlxSw2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_KneelSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LEDVega_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LKSCS_SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LKS_SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1Switch2_Status_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_MirrorHeatingSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_PloughLight_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_PloughtLightsPushButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto1SwitchStatus_Pto1SwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto2SwitchStatus_Pto2SwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto3SwitchStatus_Pto3SwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(OffOn_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Pto4SwitchStatus_Pto4SwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio1SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio2SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio3SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio4SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio5SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio6SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RatioALDSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RearAxleDiffLock_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RearAxleSteering_DeviceEvent_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ReducedSetModeButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningSw_stat_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_1_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_2_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Slid5thWheelSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_SpotlightFront_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_SpotlightRoof_DeviceEvent_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TailLiftPushButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TailLiftSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TransferCaseNeutral_SwitchStat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TridemALDSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_WorkLight_ButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Boolean, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_ALD_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_ASROff_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch1_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch2_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch3_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch4_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch5_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch6_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_BeaconSRocker_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_Beacon_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_BogieSwitch_DeviceIndication_DualDeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_CabWorkingLight_DevInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN1_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN2_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN3_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN5_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_ConstructionSwitch_DeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbContOrSlid_HMICtrl_ContUnlock_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbTailLiftCrane_HMICtrl_CraneSupply_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_DAS_HMICtrl_DAS_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_DashboardLockSwitch_Devic_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EconomyPower_HMICtrl_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_EquipmentLightInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EngineSpeedControl_HMICtrl_EscSwitchEnableDeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EngineSpeedControl_HMICtrl_EscSwitchMuddySiteDeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_CollisionMitigation_HMICtrl_FCW_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_FPBR_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_FerryFunction_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_FifthWheelLightInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_FrontPropulsion_UOCtrl_FrtAxleHydro_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ServiceBraking_HMICtrl_HillStartAid_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_KneelDeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_LEDVega_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_LKS_HMICtrl_LKSCS_DeviceIndication_DualDeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_LKS_HMICtrl_LKS_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_MaxTract_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MirrorHeating_HMICtrl_MirrorHeatingDeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO1_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO2_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO3_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_PowerTakeOff_HMICtrl_PTO4_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_PloughtLights_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio1_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio2_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio4_DeviceIndication_DualDeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio5_DeviceIndication_DualDeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio6_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_RatioALD_DualDeviceIndication_DualDeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_RearAxleSteering_HMICtrl_RearAxleSteeringDeviceInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_MovingUnitTraction_UICtrl_RearDiffLock_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_TheftAlarm_HMI2_ctrl_ReducedSetMode_DevInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ReverseGearWarning_HMICtrl_ReverseWarningInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbContOrSlid_HMICtrl_Slid5thWheel_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_LIN_SwcActivation_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraBbTailLiftCrane_HMICtrl_TailLift_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_TransferCase_HMICtrl_TransferCaseNeutral_DevInd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_TridemALD_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_ExtraLighting_HMICtrl_WorkingLight_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ABSInhibitSwitchStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ABSInhibit_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_ABSSwitchStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ABS_Indication_DeviceIndication (3U)
#  define Rte_InitValue_AEBS_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ALDSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ALD_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_ASROffButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ASROff_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_AdjustFrontBeamInclination_A3PosSwitchStatus (7U)
#  define Rte_InitValue_AlternativeDriveLevelSw_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_AuxBbSwitch1_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch2_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch3_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch4_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch5_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch6_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxSwitch1SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch2SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch3SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch4SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch5SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch6SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_BeaconSRocker_DeviceEvent_A2PosSwitchStatus (3U)
#  define Rte_InitValue_BeaconSRocker_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_Beacon_DeviceEven_PushButtonStatus (3U)
#  define Rte_InitValue_Beacon_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_BlackOutConvoyModeSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_BogieSwitch_DeviceIndication_DualDeviceIndication (15U)
#  define Rte_InitValue_BrakeBlendingButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BrakeBlending_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_BunkB1ParkHeaterBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkB1ParkHeaterTempSw_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_CabTilt_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_CabTilt_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_CabWorkLight_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_CabWorkingLight_DevInd_DeviceIndication (0U)
#  define Rte_InitValue_ChildLock_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_ComMode_LIN1_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN2_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN3_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN4_ComMode_LIN (7U)
#  define Rte_InitValue_ComMode_LIN5_ComMode_LIN (7U)
#  define Rte_InitValue_ConstructionSwitch_DeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_Construction_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ContUnlockSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ContUnlock_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_CranePushButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_CraneSupply_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_CraneSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_DAS_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_DAS_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_DRLandAHSinhib_stat_DRLandAHSinhib_stat (3U)
#  define Rte_InitValue_DashboardLockButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_DoorAutoFuncInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_DoorLockSwitch_DeviceIndic_DeviceIndication (0U)
#  define Rte_InitValue_ESCOff_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_ESCOff_SwitchStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ESCReducedOff_Switch_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ESC_Indication_DeviceIndication (3U)
#  define Rte_InitValue_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd (3U)
#  define Rte_InitValue_EconomyPowerSwitch_status_EconomyPowerSwitch_status (3U)
#  define Rte_InitValue_EngineTmpryStopButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_EngineTmpryStopDisableDevInd_DeviceIndication (0U)
#  define Rte_InitValue_EquipmentLightInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_EquipmentLight_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_EscSwitchEnableDeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_EscSwitchEnableStatus_PushButtonStatus (3U)
#  define Rte_InitValue_EscSwitchIncDecStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_EscSwitchMuddySiteDeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_EscSwitchMuddySiteStatus_PushButtonStatus (3U)
#  define Rte_InitValue_EscSwitchResumeStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ExtraMainbeam_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ExtraSideMarkers_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_ExtraSideMarkers_DeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_FCW_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_FCW_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_FPBRSwitchStatus_PushButtonStatus (3U)
#  define Rte_InitValue_FPBR_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_FerryFunctionSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_FerryFunction_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_FifthWheelLightInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_FifthWheelLight_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_FlexSwitchChildLockButton_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_FogLightFront_ButtonStatus_3_PushButtonStatus (3U)
#  define Rte_InitValue_FogLightRear_ButtonStatus_3_PushButtonStatus (3U)
#  define Rte_InitValue_FrontDiffLock_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_FrontFog_Indication_DeviceIndication (3U)
#  define Rte_InitValue_FrtAxleHydro_ButtonPush_PushButtonStatus (3U)
#  define Rte_InitValue_FrtAxleHydro_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_HillStartAidButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_HillStartAid_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_InhibRegenerationSwitch_stat_A2PosSwitchStatus (3U)
#  define Rte_InitValue_InhibRegeneration_ButtonStat_PushButtonStatus (3U)
#  define Rte_InitValue_InhibRegeneration_DeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_IntLghtActvnBtn_stat_A2PosSwitchStatus (3U)
#  define Rte_InitValue_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_IntLghtMaxModeFlxSw2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtNightModeBtn_stat_A2PosSwitchStatus (3U)
#  define Rte_InitValue_IntLghtNightModeFlxSw2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_KneelDeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_KneelSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_LCS_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_LCS_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_LEDVega_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_LEDVega_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_LKSCS_DeviceIndication_DualDeviceIndication (0U)
#  define Rte_InitValue_LKSCS_SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LKS_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_LKS_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle1Switch2_Status_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle1SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle2SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LoadingLevelSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_MaxTract_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_MirrorFoldingSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_MirrorHeatingDeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_MirrorHeatingSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_PTO1_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_PTO2_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_PTO3_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_PTO4_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_ParkingHeater_IndicationCmd_IndicationCmd (0U)
#  define Rte_InitValue_PloughLight_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_PloughtLightsPushButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_PloughtLights_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_Pto1SwitchStatus_Pto1SwitchStatus (3U)
#  define Rte_InitValue_Pto2SwitchStatus_Pto2SwitchStatus (3U)
#  define Rte_InitValue_Pto3SwitchStatus_Pto3SwitchStatus (3U)
#  define Rte_InitValue_Pto4SwitchStatus_Pto4SwitchStatus (3U)
#  define Rte_InitValue_Ratio1SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Ratio1_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_Ratio2SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Ratio2_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_Ratio3SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Ratio4SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_Ratio4_DeviceIndication_DualDeviceIndication (15U)
#  define Rte_InitValue_Ratio5SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_Ratio5_DeviceIndication_DualDeviceIndication (15U)
#  define Rte_InitValue_Ratio6SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Ratio6_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_RatioALDSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_RatioALD_DualDeviceIndication_DualDeviceIndication (15U)
#  define Rte_InitValue_RearAxleDiffLock_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_RearAxleSteeringDeviceInd_DeviceIndication (3U)
#  define Rte_InitValue_RearAxleSteering_DeviceEvent_A2PosSwitchStatus (3U)
#  define Rte_InitValue_RearDiffLockSwap_DeviceIndicat_DeviceIndication (0U)
#  define Rte_InitValue_RearDiffLock_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_RearDifflockSwap_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_RearFog_Indication_DeviceIndication (3U)
#  define Rte_InitValue_ReducedSetModeButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ReducedSetMode_DevInd_DeviceIndication (0U)
#  define Rte_InitValue_Regeneration2PosSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_RegenerationIndication_DeviceIndication (0U)
#  define Rte_InitValue_RegenerationPushButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_RegenerationSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_Regeneration_DeviceInd_DualDeviceIndication (15U)
#  define Rte_InitValue_ReverseGearWarningBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_ReverseGearWarningSw_stat_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ReverseWarningInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_RoofHatch_SwitchStatus_1_A3PosSwitchStatus (7U)
#  define Rte_InitValue_RoofHatch_SwitchStatus_2_A3PosSwitchStatus (7U)
#  define Rte_InitValue_RoofSignLight_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_RrAutoDifflck_DeviceIndication_RrAutoDifflck_DeviceIndication (0U)
#  define Rte_InitValue_RrAutoDifflockSwitch_stat_RrAutoDifflockSwitch_stat (3U)
#  define Rte_InitValue_RrAxlDifflck_DeviceIndication_RrAxlDifflck_DeviceIndication (0U)
#  define Rte_InitValue_RrItrAxlDifflckSwitch_stat_RrItrAxlDifflckSwitch_stat (3U)
#  define Rte_InitValue_RrItrWhl1DifflckSwitch_stat_RrItrWhl1DifflckSwitch_stat (3U)
#  define Rte_InitValue_RrItrWhl2DifflckSwitch_stat_RrItrWhl2DifflckSwitch_stat (3U)
#  define Rte_InitValue_RrWhl1Difflck_DeviceIndication_RrWhl1Difflck_DeviceIndication (0U)
#  define Rte_InitValue_RrWhl2Difflck_DeviceIndication_RrWhl2Difflck_DeviceIndication (0U)
#  define Rte_InitValue_SideReverseLightInd_cmd_DualDeviceIndication (15U)
#  define Rte_InitValue_SideReverseLight_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_SideReverseLight_SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_SideWheelLightInd_cmd_DeviceIndication (0U)
#  define Rte_InitValue_Slid5thWheelSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Slid5thWheel_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_SpecialLightSituation_ind_SpecialLightSituation_ind (0U)
#  define Rte_InitValue_SpotlightFront_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_SpotlightRoof_DeviceEvent_PushButtonStatus (3U)
#  define Rte_InitValue_StatTrailerBrakeSwitchStatus_PushButtonStatus (3U)
#  define Rte_InitValue_StaticCornerLight_SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_SwcActivation_LIN_SwcActivation_LIN (1U)
#  define Rte_InitValue_SwitchDetectionNeeded1_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_SwitchDetectionNeeded2_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_SwitchDetectionNeeded3_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_SwitchDetectionNeeded4_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_SwitchDetectionNeeded5_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_SwitchDetectionNeeded6_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_SwitchDetectionNeeded7_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_SwitchDetectionNeeded8_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_SwitchDetectionNeeded9_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_SwitchDetectionNeededB_SwitchDetectionNeeded (FALSE)
#  define Rte_InitValue_TCS_DualDeviceIndication_DualDeviceIndication (15U)
#  define Rte_InitValue_TailLiftPushButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_TailLiftSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_TailLift_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_TemporaryRSLDeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_TemporaryRSLSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ThreePosDifflockSwitch_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_ThreePosFrDiffLockSwitch_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_TractionControlSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_TransferCaseNeutral_DevInd_DeviceIndication (3U)
#  define Rte_InitValue_TransferCaseNeutral_SwitchStat_PushButtonStatus (3U)
#  define Rte_InitValue_TridemALDSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_TridemALD_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_TwoPosDifflockSwitch_stat_A2PosSwitchStatus (3U)
#  define Rte_InitValue_WarmUp_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_WarmUp_SwitchStatus_PushButtonStatus (3U)
#  define Rte_InitValue_WorkLight_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_WorkingLight_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type (FALSE)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ABSInhibit_DeviceIndication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_BrakeBlending_DeviceIndication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP1SwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP1SwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP2SwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP2SwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP3SwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP3SwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP4SwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP4SwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP5SwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP5SwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP6SwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP6SwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP7SwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP7SwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP8SwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP8SwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP9SwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP9SwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP_BSwitchStatus_FSPSwitchStatusArray(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP_BSwitchStatus_FSPSwitchStatusArray(P2VAR(FSPSwitchStatusArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FrontFog_Indication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RearFog_Indication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RegenerationIndication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Regeneration_DeviceInd_DualDeviceIndication(P2VAR(DualDeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SpecialLightSituation_ind_SpecialLightSituation_ind(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp1_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp1_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp2_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp2_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp3_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp3_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp4_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp4_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp5_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp5_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp6_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp6_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp7_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp7_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp8_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp8_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp9_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp9_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionRespB_SwitchDetectionResp(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionRespB_SwitchDetectionResp(P2VAR(SwitchDetectionResp_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BlackOutConvoyModeSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BrakeBlendingButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_DRLandAHSinhib_stat_DRLandAHSinhib_stat(NeutralPushed_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ExtraMainbeam_SwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP1IndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP1IndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP2IndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP2IndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP3IndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP3IndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP4IndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP4IndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP5IndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP5IndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP6IndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP6IndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP7IndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP7IndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP8IndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP8IndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP9IndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP9IndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP_BIndicationCmd_FSPIndicationCmdArray(P2CONST(DeviceIndication_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP_BIndicationCmd_FSPIndicationCmdArray(P2CONST(FSPIndicationCmdArray_T, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FogLightFront_ButtonStatus_3_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FogLightRear_ButtonStatus_3_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_InhibRegenerationSwitch_stat_A2PosSwitchStatus(A2PosSwitchStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_InhibRegeneration_ButtonStat_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Regeneration2PosSwitchStatus_A2PosSwitchStatus(A2PosSwitchStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RegenerationPushButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RegenerationSwitchStatus_A3PosSwitchStatus(A3PosSwitchStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_StatTrailerBrakeSwitchStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ABSInhibit_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ABSInhibit_DeviceIndication_DeviceIndication
#  define Rte_Read_ABS_Indication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ABS_Indication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ABS_Indication_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ALD_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ALD_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ALD_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_ALD_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ASROff_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ASROff_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ASROff_DeviceIndication_DeviceIndication(data) (*(data) = Rte_MovingUnitTraction_UICtrl_ASROff_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxBbSwitch1_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch1_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch1_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch1_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxBbSwitch2_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch2_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch2_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch2_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxBbSwitch3_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch3_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch3_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch3_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxBbSwitch4_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch4_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch4_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch4_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxBbSwitch5_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch5_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch5_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch5_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxBbSwitch6_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch6_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_AuxBbSwitch6_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch6_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BeaconSRocker_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_BeaconSRocker_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_BeaconSRocker_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ExtraLighting_HMICtrl_BeaconSRocker_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Beacon_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Beacon_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Beacon_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ExtraLighting_HMICtrl_Beacon_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BogieSwitch_DeviceIndication_DualDeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_BogieSwitch_DeviceIndication_DualDeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_BogieSwitch_DeviceIndication_DualDeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_BogieSwitch_DeviceIndication_DualDeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BrakeBlending_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_BrakeBlending_DeviceIndication_DeviceIndication
#  define Rte_Read_CabTilt_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_CabTilt_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_CabTilt_DeviceIndication_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_CabWorkingLight_DevInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_CabWorkingLight_DevInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_CabWorkingLight_DevInd_DeviceIndication(data) (*(data) = Rte_ExtraLighting_HMICtrl_CabWorkingLight_DevInd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ChildLock_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ChildLock_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ChildLock_DeviceIndication_DeviceIndication(data) (*(data) = 0U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN1_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN1_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN1_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN1_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN2_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN2_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN2_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN2_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN3_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN3_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN3_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN3_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN4_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN4_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN4_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN4_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ComMode_LIN5_ComMode_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN5_ComMode_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ComMode_LIN5_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN5_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ConstructionSwitch_DeviceInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ConstructionSwitch_DeviceInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ConstructionSwitch_DeviceInd_DeviceIndication(data) (*(data) = Rte_MovingUnitTraction_UICtrl_ConstructionSwitch_DeviceInd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ContUnlock_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ContUnlock_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ContUnlock_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ExtraBbContOrSlid_HMICtrl_ContUnlock_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_CraneSupply_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_CraneSupply_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_CraneSupply_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ExtraBbTailLiftCrane_HMICtrl_CraneSupply_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DAS_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_DAS_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_DAS_DeviceIndication_DeviceIndication(data) (*(data) = Rte_DAS_HMICtrl_DAS_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DoorAutoFuncInd_cmd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_DoorAutoFuncInd_cmd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_DoorAutoFuncInd_cmd_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DoorLockSwitch_DeviceIndic_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_DoorLockSwitch_DeviceIndic_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_DoorLockSwitch_DeviceIndic_DeviceIndication(data) (*(data) = Rte_VehicleAccess_Ctrl_DashboardLockSwitch_Devic_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ESCOff_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ESCOff_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ESCOff_DeviceIndication_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ESC_Indication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ESC_Indication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ESC_Indication_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd(data) (*(data) = Rte_EconomyPower_HMICtrl_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EngineTmpryStopDisableDevInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EngineTmpryStopDisableDevInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EngineTmpryStopDisableDevInd_DeviceIndication(data) (*(data) = 0U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EquipmentLightInd_cmd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EquipmentLightInd_cmd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EquipmentLightInd_cmd_DeviceIndication(data) (*(data) = Rte_ExtraLighting_HMICtrl_EquipmentLightInd_cmd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EscSwitchEnableDeviceInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchEnableDeviceInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchEnableDeviceInd_DeviceIndication(data) (*(data) = Rte_EngineSpeedControl_HMICtrl_EscSwitchEnableDeviceInd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_EscSwitchMuddySiteDeviceInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchMuddySiteDeviceInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchMuddySiteDeviceInd_DeviceIndication(data) (*(data) = Rte_EngineSpeedControl_HMICtrl_EscSwitchMuddySiteDeviceInd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ExtraSideMarkers_DeviceInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ExtraSideMarkers_DeviceInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ExtraSideMarkers_DeviceInd_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FCW_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FCW_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FCW_DeviceIndication_DeviceIndication(data) (*(data) = Rte_CollisionMitigation_HMICtrl_FCW_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FPBR_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FPBR_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FPBR_DeviceIndication_DeviceIndication(data) (*(data) = Rte_LevelControl_HMICtrl_FPBR_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FSP1SwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP1SwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FSP2SwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP2SwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FSP3SwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP3SwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FSP4SwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP4SwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FSP5SwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP5SwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FSP6SwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP6SwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FSP7SwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP7SwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FSP8SwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP8SwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FSP9SwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP9SwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FSP_BSwitchStatus_FSPSwitchStatusArray Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FSP_BSwitchStatus_FSPSwitchStatusArray
#  define Rte_Read_FerryFunction_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FerryFunction_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FerryFunction_DeviceIndication_DeviceIndication(data) (*(data) = Rte_LevelControl_HMICtrl_FerryFunction_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FifthWheelLightInd_cmd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FifthWheelLightInd_cmd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FifthWheelLightInd_cmd_DeviceIndication(data) (*(data) = Rte_ExtraLighting_HMICtrl_FifthWheelLightInd_cmd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FrontDiffLock_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FrontDiffLock_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FrontDiffLock_DeviceIndication_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FrontFog_Indication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FrontFog_Indication_DeviceIndication
#  define Rte_Read_FrtAxleHydro_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FrtAxleHydro_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_FrtAxleHydro_DeviceIndication_DeviceIndication(data) (*(data) = Rte_FrontPropulsion_UOCtrl_FrtAxleHydro_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_HillStartAid_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_HillStartAid_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_HillStartAid_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ServiceBraking_HMICtrl_HillStartAid_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_InhibRegeneration_DeviceInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_InhibRegeneration_DeviceInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_InhibRegeneration_DeviceInd_DeviceIndication(data) (Com_ReceiveSignal(ComConf_ComSignal_InhibRegeneration_DeviceInd_oHMIIOM_BB2_25P_oBackbone2_74a33ac3_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KneelDeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_KneelDeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_KneelDeviceIndication_DeviceIndication(data) (*(data) = Rte_LevelControl_HMICtrl_KneelDeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LCS_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_LCS_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_LCS_DeviceIndication_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LEDVega_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_LEDVega_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_LEDVega_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ExtraLighting_HMICtrl_LEDVega_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LKSCS_DeviceIndication_DualDeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_LKSCS_DeviceIndication_DualDeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_LKSCS_DeviceIndication_DualDeviceIndication(data) (*(data) = Rte_LKS_HMICtrl_LKSCS_DeviceIndication_DualDeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LKS_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_LKS_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_LKS_DeviceIndication_DeviceIndication(data) (*(data) = Rte_LKS_HMICtrl_LKS_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_MaxTract_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_MaxTract_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_MaxTract_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_MaxTract_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_MirrorHeatingDeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_MirrorHeatingDeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_MirrorHeatingDeviceIndication_DeviceIndication(data) (*(data) = Rte_MirrorHeating_HMICtrl_MirrorHeatingDeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PTO1_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PTO1_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PTO1_DeviceIndication_DeviceIndication(data) (*(data) = Rte_PowerTakeOff_HMICtrl_PTO1_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PTO2_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PTO2_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PTO2_DeviceIndication_DeviceIndication(data) (*(data) = Rte_PowerTakeOff_HMICtrl_PTO2_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PTO3_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PTO3_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PTO3_DeviceIndication_DeviceIndication(data) (*(data) = Rte_PowerTakeOff_HMICtrl_PTO3_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PTO4_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PTO4_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PTO4_DeviceIndication_DeviceIndication(data) (*(data) = Rte_PowerTakeOff_HMICtrl_PTO4_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ParkingHeater_IndicationCmd_IndicationCmd Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ParkingHeater_IndicationCmd_IndicationCmd
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ParkingHeater_IndicationCmd_IndicationCmd(data) (*(data) = 0U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PloughtLights_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PloughtLights_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_PloughtLights_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ExtraLighting_HMICtrl_PloughtLights_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio1_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio1_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio1_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_Ratio1_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio2_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio2_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio2_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_Ratio2_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio4_DeviceIndication_DualDeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio4_DeviceIndication_DualDeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio4_DeviceIndication_DualDeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_Ratio4_DeviceIndication_DualDeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio5_DeviceIndication_DualDeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio5_DeviceIndication_DualDeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio5_DeviceIndication_DualDeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_Ratio5_DeviceIndication_DualDeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio6_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio6_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Ratio6_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_Ratio6_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RatioALD_DualDeviceIndication_DualDeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RatioALD_DualDeviceIndication_DualDeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RatioALD_DualDeviceIndication_DualDeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_RatioALD_DualDeviceIndication_DualDeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RearAxleSteeringDeviceInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RearAxleSteeringDeviceInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RearAxleSteeringDeviceInd_DeviceIndication(data) (*(data) = Rte_RearAxleSteering_HMICtrl_RearAxleSteeringDeviceInd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RearDiffLockSwap_DeviceIndicat_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RearDiffLockSwap_DeviceIndicat_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RearDiffLockSwap_DeviceIndicat_DeviceIndication(data) (*(data) = 0U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RearDiffLock_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RearDiffLock_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RearDiffLock_DeviceIndication_DeviceIndication(data) (*(data) = Rte_MovingUnitTraction_UICtrl_RearDiffLock_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RearFog_Indication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RearFog_Indication_DeviceIndication
#  define Rte_Read_ReducedSetMode_DevInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ReducedSetMode_DevInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ReducedSetMode_DevInd_DeviceIndication(data) (*(data) = Rte_TheftAlarm_HMI2_ctrl_ReducedSetMode_DevInd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RegenerationIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RegenerationIndication_DeviceIndication
#  define Rte_Read_Regeneration_DeviceInd_DualDeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Regeneration_DeviceInd_DualDeviceIndication
#  define Rte_Read_ReverseWarningInd_cmd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ReverseWarningInd_cmd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_ReverseWarningInd_cmd_DeviceIndication(data) (*(data) = Rte_ReverseGearWarning_HMICtrl_ReverseWarningInd_cmd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RrAutoDifflck_DeviceIndication_RrAutoDifflck_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RrAutoDifflck_DeviceIndication_RrAutoDifflck_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RrAutoDifflck_DeviceIndication_RrAutoDifflck_DeviceIndication(data) (*(data) = 0U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RrAxlDifflck_DeviceIndication_RrAxlDifflck_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RrAxlDifflck_DeviceIndication_RrAxlDifflck_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RrAxlDifflck_DeviceIndication_RrAxlDifflck_DeviceIndication(data) (*(data) = 0U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RrWhl1Difflck_DeviceIndication_RrWhl1Difflck_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RrWhl1Difflck_DeviceIndication_RrWhl1Difflck_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RrWhl1Difflck_DeviceIndication_RrWhl1Difflck_DeviceIndication(data) (*(data) = 0U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RrWhl2Difflck_DeviceIndication_RrWhl2Difflck_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RrWhl2Difflck_DeviceIndication_RrWhl2Difflck_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_RrWhl2Difflck_DeviceIndication_RrWhl2Difflck_DeviceIndication(data) (*(data) = 0U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SideReverseLightInd_cmd_DualDeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SideReverseLightInd_cmd_DualDeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SideReverseLightInd_cmd_DualDeviceIndication(data) (*(data) = 15U, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SideWheelLightInd_cmd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SideWheelLightInd_cmd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SideWheelLightInd_cmd_DeviceIndication(data) (*(data) = 0U, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Slid5thWheel_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Slid5thWheel_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_Slid5thWheel_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ExtraBbContOrSlid_HMICtrl_Slid5thWheel_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SpecialLightSituation_ind_SpecialLightSituation_ind Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SpecialLightSituation_ind_SpecialLightSituation_ind
#  define Rte_Read_SwcActivation_LIN_SwcActivation_LIN Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwcActivation_LIN_SwcActivation_LIN
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwcActivation_LIN_SwcActivation_LIN(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_LIN_SwcActivation_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeeded1_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded1_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded1_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded1L1_oFSP1_Frame_L1_oLIN00_099b825d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeeded2_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded2_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded2_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded2L1_oFSP2_Frame_L1_oLIN00_6fec6f8a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeeded3_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded3_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded3_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded1L2_oFSP1_Frame_L2_oLIN01_86f68c15_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeeded4_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded4_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded4_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded2L2_oFSP2_Frame_L2_oLIN01_e08161c2_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeeded5_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded5_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded5_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded3L2_oFSP3_Frame_L2_oLIN01_c2ac3a8f_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeeded6_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded6_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded6_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded4L2_oFSP4_Frame_L2_oLIN01_2c6eba6c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeeded7_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded7_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded7_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded1L3_oFSP1_Frame_L3_oLIN02_482637e5_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeeded8_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded8_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded8_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded2L3_oFSP2_Frame_L3_oLIN02_2e51da32_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeeded9_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded9_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeeded9_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded1L4_oFSP1_Frame_L4_oLIN03_435d96c4_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionNeededB_SwitchDetectionNeeded Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeededB_SwitchDetectionNeeded
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionNeededB_SwitchDetectionNeeded(data) (Com_ReceiveSignal(ComConf_ComSignal_SwitchDetectionNeeded1L5_oFSP1_Frame_L5_oLIN04_8ae0e92d_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwitchDetectionResp1_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp1_SwitchDetectionResp
#  define Rte_Read_SwitchDetectionResp2_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp2_SwitchDetectionResp
#  define Rte_Read_SwitchDetectionResp3_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp3_SwitchDetectionResp
#  define Rte_Read_SwitchDetectionResp4_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp4_SwitchDetectionResp
#  define Rte_Read_SwitchDetectionResp5_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp5_SwitchDetectionResp
#  define Rte_Read_SwitchDetectionResp6_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp6_SwitchDetectionResp
#  define Rte_Read_SwitchDetectionResp7_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp7_SwitchDetectionResp
#  define Rte_Read_SwitchDetectionResp8_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp8_SwitchDetectionResp
#  define Rte_Read_SwitchDetectionResp9_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp9_SwitchDetectionResp
#  define Rte_Read_SwitchDetectionRespB_SwitchDetectionResp Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionRespB_SwitchDetectionResp
#  define Rte_Read_TCS_DualDeviceIndication_DualDeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TCS_DualDeviceIndication_DualDeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TCS_DualDeviceIndication_DualDeviceIndication(data) (*(data) = 15U, ((Std_ReturnType)RTE_E_UNCONNECTED)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TailLift_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TailLift_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TailLift_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ExtraBbTailLiftCrane_HMICtrl_TailLift_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TemporaryRSLDeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TemporaryRSLDeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TemporaryRSLDeviceIndication_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TransferCaseNeutral_DevInd_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TransferCaseNeutral_DevInd_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TransferCaseNeutral_DevInd_DeviceIndication(data) (*(data) = Rte_TransferCase_HMICtrl_TransferCaseNeutral_DevInd_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TridemALD_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TridemALD_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_TridemALD_DeviceIndication_DeviceIndication(data) (*(data) = Rte_AxleLoadDistribution_HMICtrl_TridemALD_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WarmUp_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_WarmUp_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_WarmUp_DeviceIndication_DeviceIndication(data) (*(data) = 3U, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WorkingLight_DeviceIndication_DeviceIndication Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_WorkingLight_DeviceIndication_DeviceIndication
#  define Rte_Read_FlexibleSwitchesRouter_Ctrl_Router_WorkingLight_DeviceIndication_DeviceIndication(data) (*(data) = Rte_ExtraLighting_HMICtrl_WorkingLight_DeviceIndication_DeviceIndication, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_IsUpdated_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_IsUpdated_SwitchDetectionResp1_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp1_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp1_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp1_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_SwitchDetectionResp2_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp2_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp2_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp2_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_SwitchDetectionResp3_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp3_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp3_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp3_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_SwitchDetectionResp4_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp4_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp4_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp4_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_SwitchDetectionResp5_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp5_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp5_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp5_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_SwitchDetectionResp6_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp6_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp6_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp6_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_SwitchDetectionResp7_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp7_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp7_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp7_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_SwitchDetectionResp8_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp8_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp8_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp8_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_SwitchDetectionResp9_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp9_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionResp9_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionResp9_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_IsUpdated_SwitchDetectionRespB_SwitchDetectionResp Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionRespB_SwitchDetectionResp
#  define Rte_IsUpdated_FlexibleSwitchesRouter_Ctrl_Router_SwitchDetectionRespB_SwitchDetectionResp() ((Rte_RxUpdateFlags.Rte_RxUpdate_FlexibleSwitchesRouter_Ctrl_SwitchDetectionRespB_SwitchDetectionResp == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_ABSInhibitSwitchStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ABSInhibitSwitchStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ABSInhibitSwitchStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_ABSInhibitSwitchStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ABSSwitchStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ABSSwitchStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ABSSwitchStatus_PushButtonStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AEBS_ButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AEBS_ButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AEBS_ButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_AEBS_ButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ALDSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ALDSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ALDSwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_ALDSwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ASROffButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ASROffButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ASROffButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_ASROffButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AdjustFrontBeamInclination_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AdjustFrontBeamInclination_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AdjustFrontBeamInclination_A3PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AlternativeDriveLevelSw_stat_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AlternativeDriveLevelSw_stat_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AlternativeDriveLevelSw_stat_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_AlternativeDriveLevelSw_stat_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxSwitch1SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch1SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch1SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch1SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxSwitch2SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch2SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch2SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch2SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxSwitch3SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch3SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch3SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch3SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxSwitch4SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch4SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch4SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch4SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxSwitch5SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch5SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch5SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch5SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxSwitch6SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch6SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_AuxSwitch6SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch6SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BeaconSRocker_DeviceEvent_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BeaconSRocker_DeviceEvent_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BeaconSRocker_DeviceEvent_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_BeaconSRocker_DeviceEvent_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Beacon_DeviceEven_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Beacon_DeviceEven_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Beacon_DeviceEven_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Beacon_DeviceEven_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BlackOutConvoyModeSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BlackOutConvoyModeSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_BrakeBlendingButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BrakeBlendingButtonStatus_PushButtonStatus
#  define Rte_Write_BunkB1ParkHeaterBtn_stat_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BunkB1ParkHeaterBtn_stat_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BunkB1ParkHeaterBtn_stat_PushButtonStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkB1ParkHeaterTempSw_stat_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BunkB1ParkHeaterTempSw_stat_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_BunkB1ParkHeaterTempSw_stat_A3PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_CabTilt_SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_CabTilt_SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_CabTilt_SwitchStatus_A2PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_CabWorkLight_ButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_CabWorkLight_ButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_CabWorkLight_ButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_CabWorkLight_ButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Construction_SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Construction_SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Construction_SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Construction_SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ContUnlockSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ContUnlockSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ContUnlockSwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_ContUnlockSwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_CranePushButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_CranePushButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_CranePushButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_CranePushButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_CraneSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_CraneSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_CraneSwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_CraneSwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_DAS_SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_DAS_SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_DAS_SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_DAS_SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_DRLandAHSinhib_stat_DRLandAHSinhib_stat Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_DRLandAHSinhib_stat_DRLandAHSinhib_stat
#  define Rte_Write_DashboardLockButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_DashboardLockButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_DashboardLockButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_DashboardLockButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ESCOff_SwitchStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ESCOff_SwitchStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ESCOff_SwitchStatus_PushButtonStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ESCReducedOff_Switch_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ESCReducedOff_Switch_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ESCReducedOff_Switch_A2PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EconomyPowerSwitch_status_EconomyPowerSwitch_status Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EconomyPowerSwitch_status_EconomyPowerSwitch_status
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EconomyPowerSwitch_status_EconomyPowerSwitch_status(data) (Rte_FlexibleSwitchesRouter_Ctrl_EconomyPowerSwitch_status_EconomyPowerSwitch_status = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EngineTmpryStopButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EngineTmpryStopButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EngineTmpryStopButtonStatus_PushButtonStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EquipmentLight_DeviceEvent_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EquipmentLight_DeviceEvent_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EquipmentLight_DeviceEvent_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_EquipmentLight_DeviceEvent_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EscSwitchEnableStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchEnableStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchEnableStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchEnableStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EscSwitchIncDecStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchIncDecStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchIncDecStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchIncDecStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EscSwitchMuddySiteStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchMuddySiteStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchMuddySiteStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchMuddySiteStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_EscSwitchResumeStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchResumeStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_EscSwitchResumeStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_EscSwitchResumeStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ExtraMainbeam_SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ExtraMainbeam_SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_ExtraSideMarkers_DeviceEvent_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ExtraSideMarkers_DeviceEvent_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ExtraSideMarkers_DeviceEvent_PushButtonStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FCW_SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FCW_SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FCW_SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_FCW_SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FPBRSwitchStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FPBRSwitchStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FPBRSwitchStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_FPBRSwitchStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FSP1IndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP1IndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FSP2IndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP2IndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FSP3IndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP3IndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FSP4IndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP4IndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FSP5IndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP5IndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FSP6IndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP6IndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FSP7IndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP7IndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FSP8IndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP8IndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FSP9IndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP9IndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FSP_BIndicationCmd_FSPIndicationCmdArray Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FSP_BIndicationCmd_FSPIndicationCmdArray
#  define Rte_Write_FerryFunctionSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FerryFunctionSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FerryFunctionSwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_FerryFunctionSwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FifthWheelLight_DeviceEvent_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FifthWheelLight_DeviceEvent_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FifthWheelLight_DeviceEvent_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_FifthWheelLight_DeviceEvent_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FlexSwitchChildLockButton_stat_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FlexSwitchChildLockButton_stat_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FlexSwitchChildLockButton_stat_A3PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_FogLightFront_ButtonStatus_3_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FogLightFront_ButtonStatus_3_PushButtonStatus
#  define Rte_Write_FogLightRear_ButtonStatus_3_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FogLightRear_ButtonStatus_3_PushButtonStatus
#  define Rte_Write_FrtAxleHydro_ButtonPush_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FrtAxleHydro_ButtonPush_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_FrtAxleHydro_ButtonPush_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_FrtAxleHydro_ButtonPush_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_HillStartAidButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_HillStartAidButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_HillStartAidButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_HillStartAidButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_InhibRegenerationSwitch_stat_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_InhibRegenerationSwitch_stat_A2PosSwitchStatus
#  define Rte_Write_InhibRegeneration_ButtonStat_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_InhibRegeneration_ButtonStat_PushButtonStatus
#  define Rte_Write_IntLghtActvnBtn_stat_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtActvnBtn_stat_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtActvnBtn_stat_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_IntLghtActvnBtn_stat_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtMaxModeFlxSw2_stat_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtMaxModeFlxSw2_stat_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_IntLghtMaxModeFlxSw2_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtNightModeBtn_stat_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtNightModeBtn_stat_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtNightModeBtn_stat_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeBtn_stat_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtNightModeFlxSw2_stat_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtNightModeFlxSw2_stat_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_IntLghtNightModeFlxSw2_stat_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeFlxSw2_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KneelSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_KneelSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_KneelSwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_KneelSwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LCS_SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LCS_SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LCS_SwitchStatus_A2PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LEDVega_DeviceEvent_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LEDVega_DeviceEvent_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LEDVega_DeviceEvent_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LEDVega_DeviceEvent_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LKSCS_SwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LKSCS_SwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LKSCS_SwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LKSCS_SwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LKS_SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LKS_SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LKS_SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LKS_SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LiftAxle1Switch2_Status_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle1Switch2_Status_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle1Switch2_Status_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1Switch2_Status_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LiftAxle1SwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle1SwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle1SwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1SwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LiftAxle2SwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle2SwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle2SwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2SwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelAdjSwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LoadingLevelSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LoadingLevelSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_LoadingLevelSwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_LoadingLevelSwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_MirrorFoldingSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_MirrorFoldingSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_MirrorFoldingSwitchStatus_A3PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_MirrorHeatingSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_MirrorHeatingSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_MirrorHeatingSwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_MirrorHeatingSwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PloughLight_DeviceEvent_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_PloughLight_DeviceEvent_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_PloughLight_DeviceEvent_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_PloughLight_DeviceEvent_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PloughtLightsPushButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_PloughtLightsPushButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_PloughtLightsPushButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_PloughtLightsPushButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Pto1SwitchStatus_Pto1SwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Pto1SwitchStatus_Pto1SwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Pto1SwitchStatus_Pto1SwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Pto1SwitchStatus_Pto1SwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Pto2SwitchStatus_Pto2SwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Pto2SwitchStatus_Pto2SwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Pto2SwitchStatus_Pto2SwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Pto2SwitchStatus_Pto2SwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Pto3SwitchStatus_Pto3SwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Pto3SwitchStatus_Pto3SwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Pto3SwitchStatus_Pto3SwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Pto3SwitchStatus_Pto3SwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Pto4SwitchStatus_Pto4SwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Pto4SwitchStatus_Pto4SwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Pto4SwitchStatus_Pto4SwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Pto4SwitchStatus_Pto4SwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio1SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio1SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio1SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Ratio1SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio2SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio2SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio2SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Ratio2SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio3SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio3SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio3SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Ratio3SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio4SwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio4SwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio4SwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Ratio4SwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio5SwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio5SwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio5SwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Ratio5SwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio6SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio6SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Ratio6SwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Ratio6SwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RatioALDSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RatioALDSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RatioALDSwitchStatus_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_RatioALDSwitchStatus_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RearAxleDiffLock_ButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RearAxleDiffLock_ButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RearAxleDiffLock_ButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_RearAxleDiffLock_ButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RearAxleSteering_DeviceEvent_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RearAxleSteering_DeviceEvent_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RearAxleSteering_DeviceEvent_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_RearAxleSteering_DeviceEvent_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RearDifflockSwap_ButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RearDifflockSwap_ButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RearDifflockSwap_ButtonStatus_PushButtonStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ReducedSetModeButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ReducedSetModeButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ReducedSetModeButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_ReducedSetModeButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Regeneration2PosSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Regeneration2PosSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_RegenerationPushButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RegenerationPushButtonStatus_PushButtonStatus
#  define Rte_Write_RegenerationSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RegenerationSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_ReverseGearWarningBtn_stat_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ReverseGearWarningBtn_stat_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ReverseGearWarningBtn_stat_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ReverseGearWarningSw_stat_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ReverseGearWarningSw_stat_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ReverseGearWarningSw_stat_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_ReverseGearWarningSw_stat_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RoofHatch_SwitchStatus_1_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RoofHatch_SwitchStatus_1_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RoofHatch_SwitchStatus_1_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_1_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RoofHatch_SwitchStatus_2_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RoofHatch_SwitchStatus_2_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RoofHatch_SwitchStatus_2_A3PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_2_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RoofSignLight_DeviceEvent_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RoofSignLight_DeviceEvent_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RoofSignLight_DeviceEvent_PushButtonStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RrAutoDifflockSwitch_stat_RrAutoDifflockSwitch_stat Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RrAutoDifflockSwitch_stat_RrAutoDifflockSwitch_stat
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RrAutoDifflockSwitch_stat_RrAutoDifflockSwitch_stat(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RrItrAxlDifflckSwitch_stat_RrItrAxlDifflckSwitch_stat Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RrItrAxlDifflckSwitch_stat_RrItrAxlDifflckSwitch_stat
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RrItrAxlDifflckSwitch_stat_RrItrAxlDifflckSwitch_stat(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RrItrWhl1DifflckSwitch_stat_RrItrWhl1DifflckSwitch_stat Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RrItrWhl1DifflckSwitch_stat_RrItrWhl1DifflckSwitch_stat
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RrItrWhl1DifflckSwitch_stat_RrItrWhl1DifflckSwitch_stat(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RrItrWhl2DifflckSwitch_stat_RrItrWhl2DifflckSwitch_stat Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RrItrWhl2DifflckSwitch_stat_RrItrWhl2DifflckSwitch_stat
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_RrItrWhl2DifflckSwitch_stat_RrItrWhl2DifflckSwitch_stat(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SideReverseLight_ButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_SideReverseLight_ButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_SideReverseLight_ButtonStatus_PushButtonStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SideReverseLight_SwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_SideReverseLight_SwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_SideReverseLight_SwitchStatus_A3PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Slid5thWheelSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Slid5thWheelSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_Slid5thWheelSwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_Slid5thWheelSwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SpotlightFront_DeviceEvent_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_SpotlightFront_DeviceEvent_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_SpotlightFront_DeviceEvent_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_SpotlightFront_DeviceEvent_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SpotlightRoof_DeviceEvent_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_SpotlightRoof_DeviceEvent_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_SpotlightRoof_DeviceEvent_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_SpotlightRoof_DeviceEvent_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_StatTrailerBrakeSwitchStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_StatTrailerBrakeSwitchStatus_PushButtonStatus
#  define Rte_Write_StaticCornerLight_SwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_StaticCornerLight_SwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_StaticCornerLight_SwitchStatus_A2PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TailLiftPushButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TailLiftPushButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TailLiftPushButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_TailLiftPushButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TailLiftSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TailLiftSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TailLiftSwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_TailLiftSwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TemporaryRSLSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TemporaryRSLSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TemporaryRSLSwitchStatus_A2PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ThreePosDifflockSwitch_stat_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ThreePosDifflockSwitch_stat_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ThreePosDifflockSwitch_stat_A3PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ThreePosFrDiffLockSwitch_stat_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ThreePosFrDiffLockSwitch_stat_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_ThreePosFrDiffLockSwitch_stat_A3PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TractionControlSwitchStatus_A3PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TractionControlSwitchStatus_A3PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TractionControlSwitchStatus_A3PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TransferCaseNeutral_SwitchStat_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TransferCaseNeutral_SwitchStat_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TransferCaseNeutral_SwitchStat_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_TransferCaseNeutral_SwitchStat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TridemALDSwitchStatus_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TridemALDSwitchStatus_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TridemALDSwitchStatus_A2PosSwitchStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_TridemALDSwitchStatus_A2PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TwoPosDifflockSwitch_stat_A2PosSwitchStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TwoPosDifflockSwitch_stat_A2PosSwitchStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_TwoPosDifflockSwitch_stat_A2PosSwitchStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WarmUp_SwitchStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_WarmUp_SwitchStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_WarmUp_SwitchStatus_PushButtonStatus(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WorkLight_ButtonStatus_PushButtonStatus Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_WorkLight_ButtonStatus_PushButtonStatus
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_WorkLight_ButtonStatus_PushButtonStatus(data) (Rte_FlexibleSwitchesRouter_Ctrl_WorkLight_ButtonStatus_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type
#  define Rte_Write_FlexibleSwitchesRouter_Ctrl_Router_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type(data) (Rte_FlexibleSwitchesRouter_Ctrl_isFlexibleSwitchDetectionCompleted_isFlexibleSwitchDetectionCompleted_Type = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BOX_4A_FS_NotSupportedID_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)263, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOY_4A_FS_NotSupportedDoubleID_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)264, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BUL_31_FlexSwEEPROM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)267, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BUL_95_FlexSwConfigFailure_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)268, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ASLight_InputFSP_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)0)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ASLight_InputFSP_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)0)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_AlarmSetUnset1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_AlarmSetUnset1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_BlackoutConvoyMode_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)8)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_BlackoutConvoyMode_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)8)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_CabTiltSwitchRequest_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)10)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_CabTiltSwitchRequest_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)10)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)23)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_ExtraBBAuxiliarySwitches1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)23)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)24)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_ExtraBBTailLiftFSP2_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)24)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_FlexibleSwitchDetection_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)25)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_FlexibleSwitchDetection_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)25)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_InteriorLightsRqst1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)27)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_InteriorLightsRqst1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)27)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_LockControlCabRqst1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)30)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_LockControlCabRqst1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)30)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PHActMaintainLiving2_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)37)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PHActMaintainLiving2_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)37)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_RoofHatchRequest1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)44)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_RoofHatchRequest1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)44)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_WLight_InputFSP_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)49)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_WLight_InputFSP_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)49)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_FS_DiagAct_ID001_ID009_P1EAA_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAA_FS_DiagAct_ID001_ID009_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID010_ID019_P1EAB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAB_FS_DiagAct_ID010_ID019_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID02_ID029_P1EAC_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAC_FS_DiagAct_ID02_ID029_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID030_ID039_P1EAD_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAD_FS_DiagAct_ID030_ID039_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID040_ID049_P1EAE_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAE_FS_DiagAct_ID040_ID049_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID050_ID059_P1EAF_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAF_FS_DiagAct_ID050_ID059_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID060_ID069_P1EAG_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAG_FS_DiagAct_ID060_ID069_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID070_ID079_P1EAH_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAH_FS_DiagAct_ID070_ID079_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID080_ID089_P1EAI_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAI_FS_DiagAct_ID080_ID089_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID090_ID099_P1EAJ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAJ_FS_DiagAct_ID090_ID099_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID100_ID109_P1EAK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAK_FS_DiagAct_ID100_ID109_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID110_ID119_P1EAL_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAL_FS_DiagAct_ID110_ID119_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID120_ID129_P1EAM_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAM_FS_DiagAct_ID120_ID129_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID130_ID139_P1EAN_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAN_FS_DiagAct_ID130_ID139_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID140_ID149_P1EAO_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAO_FS_DiagAct_ID140_ID149_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID150_ID159_P1EAP_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAP_FS_DiagAct_ID150_ID159_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID160_ID169_P1EAQ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAQ_FS_DiagAct_ID160_ID169_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID170_ID179_P1EAR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAR_FS_DiagAct_ID170_ID179_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID180_ID189_P1EAS_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAS_FS_DiagAct_ID180_ID189_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID190_ID199_P1EAT_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAT_FS_DiagAct_ID190_ID199_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID200_ID209_P1EAU_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAU_FS_DiagAct_ID200_ID209_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID210_ID219_P1EAV_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAV_FS_DiagAct_ID210_ID219_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID220_ID229_P1EAW_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAW_FS_DiagAct_ID220_ID229_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID230_ID239_P1EAX_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAX_FS_DiagAct_ID230_ID239_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID240_ID249_P1EAY_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAY_FS_DiagAct_ID240_ID249_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FS_DiagAct_ID250_ID254_P1EAZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1EAZ_FS_DiagAct_ID250_ID254_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_LIN_topology_P1AJR_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1AJR_LIN_topology_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BWZ_DoubleRoofHatchSwConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1EAA_FS_DiagAct_ID001_ID009_v() (Rte_AddrPar_0x2B_P1EAA_FS_DiagAct_ID001_ID009_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAB_FS_DiagAct_ID010_ID019_v() (Rte_AddrPar_0x2B_P1EAB_FS_DiagAct_ID010_ID019_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAC_FS_DiagAct_ID02_ID029_v() (Rte_AddrPar_0x2B_P1EAC_FS_DiagAct_ID02_ID029_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAD_FS_DiagAct_ID030_ID039_v() (Rte_AddrPar_0x2B_P1EAD_FS_DiagAct_ID030_ID039_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAE_FS_DiagAct_ID040_ID049_v() (Rte_AddrPar_0x2B_P1EAE_FS_DiagAct_ID040_ID049_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAF_FS_DiagAct_ID050_ID059_v() (Rte_AddrPar_0x2B_P1EAF_FS_DiagAct_ID050_ID059_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAG_FS_DiagAct_ID060_ID069_v() (Rte_AddrPar_0x2B_P1EAG_FS_DiagAct_ID060_ID069_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAH_FS_DiagAct_ID070_ID079_v() (Rte_AddrPar_0x2B_P1EAH_FS_DiagAct_ID070_ID079_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAI_FS_DiagAct_ID080_ID089_v() (Rte_AddrPar_0x2B_P1EAI_FS_DiagAct_ID080_ID089_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAJ_FS_DiagAct_ID090_ID099_v() (Rte_AddrPar_0x2B_P1EAJ_FS_DiagAct_ID090_ID099_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAK_FS_DiagAct_ID100_ID109_v() (Rte_AddrPar_0x2B_P1EAK_FS_DiagAct_ID100_ID109_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAL_FS_DiagAct_ID110_ID119_v() (Rte_AddrPar_0x2B_P1EAL_FS_DiagAct_ID110_ID119_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAM_FS_DiagAct_ID120_ID129_v() (Rte_AddrPar_0x2B_P1EAM_FS_DiagAct_ID120_ID129_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAN_FS_DiagAct_ID130_ID139_v() (Rte_AddrPar_0x2B_P1EAN_FS_DiagAct_ID130_ID139_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAO_FS_DiagAct_ID140_ID149_v() (Rte_AddrPar_0x2B_P1EAO_FS_DiagAct_ID140_ID149_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAP_FS_DiagAct_ID150_ID159_v() (Rte_AddrPar_0x2B_P1EAP_FS_DiagAct_ID150_ID159_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAQ_FS_DiagAct_ID160_ID169_v() (Rte_AddrPar_0x2B_P1EAQ_FS_DiagAct_ID160_ID169_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAR_FS_DiagAct_ID170_ID179_v() (Rte_AddrPar_0x2B_P1EAR_FS_DiagAct_ID170_ID179_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAS_FS_DiagAct_ID180_ID189_v() (Rte_AddrPar_0x2B_P1EAS_FS_DiagAct_ID180_ID189_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAT_FS_DiagAct_ID190_ID199_v() (Rte_AddrPar_0x2B_P1EAT_FS_DiagAct_ID190_ID199_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAU_FS_DiagAct_ID200_ID209_v() (Rte_AddrPar_0x2B_P1EAU_FS_DiagAct_ID200_ID209_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAV_FS_DiagAct_ID210_ID219_v() (Rte_AddrPar_0x2B_P1EAV_FS_DiagAct_ID210_ID219_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAW_FS_DiagAct_ID220_ID229_v() (Rte_AddrPar_0x2B_P1EAW_FS_DiagAct_ID220_ID229_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAX_FS_DiagAct_ID230_ID239_v() (Rte_AddrPar_0x2B_P1EAX_FS_DiagAct_ID230_ID239_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAY_FS_DiagAct_ID240_ID249_v() (Rte_AddrPar_0x2B_P1EAY_FS_DiagAct_ID240_ID249_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1EAZ_FS_DiagAct_ID250_ID254_v() (Rte_AddrPar_0x2B_P1EAZ_FS_DiagAct_ID250_ID254_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1AJR_LIN_topology_v() (Rte_AddrPar_0x2B_P1AJR_LIN_topology_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BWZ_DoubleRoofHatchSwConfig_v() (Rte_AddrPar_0x2B_P1BWZ_DoubleRoofHatchSwConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

/**********************************************************************************************************************
 * Per-Instance Memory User Types
 *********************************************************************************************************************/

/**********************************************************************************************************************
 * Rte_Pim (Per-Instance Memory)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_P1DCT_Info; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FlexibleSwDisableDiagPresence_Type, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_FlexibleSwDisableDiagPresence; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FlexibleSwitchesinFailure_Type, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_FlexibleSwFailureData; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FSP_Array10_8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_FlexibleSwStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FSP_Array10_8, RTE_VAR_DEFAULT_RTE_PIM_GROUP) Rte_FlexibleSwitchesRouter_Ctrl_Pim_TableOfDetectedId; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_VAR_DEFAULT_RTE_PIM_GROUP_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Pim_Pim_P1DCT_Info() \
  (&Rte_FlexibleSwitchesRouter_Ctrl_Pim_P1DCT_Info) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Pim_Pim_FlexibleSwDisableDiagPresence() (&((*RtePim_Pim_FlexibleSwDisableDiagPresence())[0]))
#  else
#   define Rte_Pim_Pim_FlexibleSwDisableDiagPresence() RtePim_Pim_FlexibleSwDisableDiagPresence()
#  endif
#  define RtePim_Pim_FlexibleSwDisableDiagPresence() \
  (&Rte_FlexibleSwitchesRouter_Ctrl_Pim_FlexibleSwDisableDiagPresence) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Pim_Pim_FlexibleSwFailureData() (&((*RtePim_Pim_FlexibleSwFailureData())[0]))
#  else
#   define Rte_Pim_Pim_FlexibleSwFailureData() RtePim_Pim_FlexibleSwFailureData()
#  endif
#  define RtePim_Pim_FlexibleSwFailureData() \
  (&Rte_FlexibleSwitchesRouter_Ctrl_Pim_FlexibleSwFailureData) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Pim_Pim_FlexibleSwStatus() ((P2VAR(uint8, AUTOMATIC, RTE_VAR_NOINIT)) &((*RtePim_Pim_FlexibleSwStatus())[0][0]))
#  else
#   define Rte_Pim_Pim_FlexibleSwStatus() RtePim_Pim_FlexibleSwStatus()
#  endif
#  define RtePim_Pim_FlexibleSwStatus() \
  (&Rte_FlexibleSwitchesRouter_Ctrl_Pim_FlexibleSwStatus) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Pim_Pim_TableOfDetectedId() ((P2VAR(uint8, AUTOMATIC, RTE_VAR_NOINIT)) &((*RtePim_Pim_TableOfDetectedId())[0][0]))
#  else
#   define Rte_Pim_Pim_TableOfDetectedId() RtePim_Pim_TableOfDetectedId()
#  endif
#  define RtePim_Pim_TableOfDetectedId() \
  (&Rte_FlexibleSwitchesRouter_Ctrl_Pim_TableOfDetectedId) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define FlexibleSwitchesRouter_Ctrl_Router_START_SEC_CODE
# include "FlexibleSwitchesRouter_Ctrl_Router_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent
#  define RTE_RUNNABLE_CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent
#  define RTE_RUNNABLE_DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData
#  define RTE_RUNNABLE_DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData
#  define RTE_RUNNABLE_DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData
#  define RTE_RUNNABLE_DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData
#  define RTE_RUNNABLE_FlexibleSwitchesRouter_Ctrl_20ms_runnable FlexibleSwitchesRouter_Ctrl_20ms_runnable
#  define RTE_RUNNABLE_FlexibleSwitchesRouter_Ctrl_Init FlexibleSwitchesRouter_Ctrl_Init
# endif

FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) CBInitEvt_D1BUL_31_FlexSwEEPROM_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) CBInitEvt_D1BUL_95_FlexSwConfigFailure_InitMonitorForEvent(Dem_InitMonitorReasonType InitMonitorReason); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW0_Data_P1BW0_LIN1_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW1_Data_P1BW1_LIN1_FSP2_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW3_Data_P1BW3_LIN2_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW4_Data_P1BW4_LIN2_FSP2_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW5_Data_P1BW5_LIN2_FSP3_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW6_Data_P1BW6_LIN2_FSP4_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW8_Data_P1BW8_LIN3_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BW9_Data_P1BW9_LIN3_FSP2_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWI_Data_P1BWI_LIN1_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWJ_Data_P1BWJ_LIN1_FSP2_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWL_Data_P1BWL_LIN2_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWM_Data_P1BWM_LIN2_FSP2_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWN_Data_P1BWN_LIN2_FSP3_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWO_Data_P1BWO_LIN2_FSP4_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWQ_Data_P1BWQ_LIN3_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWR_Data_P1BWR_LIN3_FSP2_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWV_Data_P1BWV_LIN4_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BWX_Data_P1BWX_LIN5_FSP1_SwConfig_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BXD_Data_P1BXD_LIN4_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1BXF_Data_P1BXF_LIN5_FSP1_SwStatus_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1DCT_Data_P1DCT_SnapshotFlexibleSwitchID_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1GCM_Data_P1GCM_FlexibleSwitchesInFaliure_ReadData(P2VAR(Dcm_Data120ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData(P2VAR(uint8, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, FlexibleSwitchesRouter_Ctrl_Router_CODE) DataServices_P1ILR_Data_P1ILR_FlexSwitchWithDiagOffPresent_ReadData(P2VAR(Dcm_Data5ByteType, AUTOMATIC, RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, FlexibleSwitchesRouter_Ctrl_Router_CODE) FlexibleSwitchesRouter_Ctrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, FlexibleSwitchesRouter_Ctrl_Router_CODE) FlexibleSwitchesRouter_Ctrl_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define FlexibleSwitchesRouter_Ctrl_Router_STOP_SEC_CODE
# include "FlexibleSwitchesRouter_Ctrl_Router_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_CallbackInitMonitorForEvent_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW0_Data_P1BW0_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW1_Data_P1BW1_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW3_Data_P1BW3_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW4_Data_P1BW4_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW5_Data_P1BW5_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW6_Data_P1BW6_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW8_Data_P1BW8_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BW9_Data_P1BW9_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWI_Data_P1BWI_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWJ_Data_P1BWJ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWL_Data_P1BWL_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWM_Data_P1BWM_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWN_Data_P1BWN_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWO_Data_P1BWO_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWQ_Data_P1BWQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWR_Data_P1BWR_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWV_Data_P1BWV_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BWX_Data_P1BWX_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BXD_Data_P1BXD_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1BXF_Data_P1BXF_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DCT_Data_P1DCT_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1GCM_Data_P1GCM_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ILR_Data_P1ILR_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
