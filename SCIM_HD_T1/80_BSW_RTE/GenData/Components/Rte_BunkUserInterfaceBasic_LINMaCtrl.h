/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_BunkUserInterfaceBasic_LINMaCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <BunkUserInterfaceBasic_LINMaCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_BUNKUSERINTERFACEBASIC_LINMACTRL_H
# define _RTE_BUNKUSERINTERFACEBASIC_LINMACTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_BunkUserInterfaceBasic_LINMaCtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceBasic_LINMaCtrl_BunkBIntLightActvnBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN1_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BunkBAudioOnOff_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkBIntLightActvnBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkBParkHeater_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkBTempDec_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkBTempInc_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkBVolumeDown_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BunkBVolumeUp_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ComMode_LIN1_ComMode_LIN (7U)
#  define Rte_InitValue_DiagInfoLECMBasic_DiagInfo (0U)
#  define Rte_InitValue_LIN_BunkBAudioOnOff_ButtonStat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkBIntLightActvnBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkBParkHeater_ButtonStat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkBTempDec_ButtonStat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkBTempInc_ButtonStat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkBVolumeDown_ButtonStat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_BunkBVolumeUp_ButtonStat_PushButtonStatus (3U)
#  define Rte_InitValue_ResponseErrorLECMBasic_ResponseErrorLECMBasic (FALSE)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_ResponseErrorLECMBasic_ResponseErrorLECMBasic(P2VAR(ResponseErrorLECMBasic_T, AUTOMATIC, RTE_BUNKUSERINTERFACEBASIC_LINMACTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBAudioOnOff_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBParkHeater_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBTempDec_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBTempInc_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBVolumeDown_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBVolumeUp_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ComMode_LIN1_ComMode_LIN Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_ComMode_LIN1_ComMode_LIN
#  define Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_ComMode_LIN1_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN1_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagInfoLECMBasic_DiagInfo Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_DiagInfoLECMBasic_DiagInfo
#  define Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_DiagInfoLECMBasic_DiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_DiagInfoLECMBasic_oLECMBasic2CIOM_L1_oLIN00_209584a0_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkBAudioOnOff_ButtonStat_PushButtonStatus Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBAudioOnOff_ButtonStat_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBAudioOnOff_ButtonStat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkBAudioOnOff_ButtonStat_oLECMBasic2CIOM_L1_oLIN00_a735a09a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkBIntLightActvnBtn_stat_PushButtonStatus Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBIntLightActvnBtn_stat_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBIntLightActvnBtn_stat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkBIntLightActvnBtn_stat_oLECMBasic2CIOM_L1_oLIN00_5d53684c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkBParkHeater_ButtonStat_PushButtonStatus Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBParkHeater_ButtonStat_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBParkHeater_ButtonStat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkBParkHeater_ButtonStat_oLECMBasic2CIOM_L1_oLIN00_0c567d49_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkBTempDec_ButtonStat_PushButtonStatus Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBTempDec_ButtonStat_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBTempDec_ButtonStat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkBTempDec_ButtonStat_oLECMBasic2CIOM_L1_oLIN00_bad7c8e9_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkBTempInc_ButtonStat_PushButtonStatus Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBTempInc_ButtonStat_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBTempInc_ButtonStat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkBTempInc_ButtonStat_oLECMBasic2CIOM_L1_oLIN00_7521d3fe_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkBVolumeDown_ButtonStat_PushButtonStatus Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBVolumeDown_ButtonStat_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBVolumeDown_ButtonStat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkBVolumeDown_ButtonStat_oLECMBasic2CIOM_L1_oLIN00_c7ff11cb_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BunkBVolumeUp_ButtonStat_PushButtonStatus Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBVolumeUp_ButtonStat_PushButtonStatus
#  define Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_LIN_BunkBVolumeUp_ButtonStat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BunkBVolumeUp_ButtonStat_oLECMBasic2CIOM_L1_oLIN00_4fd90c4b_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ResponseErrorLECMBasic_ResponseErrorLECMBasic Rte_Read_BunkUserInterfaceBasic_LINMaCtrl_ResponseErrorLECMBasic_ResponseErrorLECMBasic


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_BunkBAudioOnOff_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBAudioOnOff_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkBIntLightActvnBtn_stat_PushButtonStatus Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBIntLightActvnBtn_stat_PushButtonStatus
#  define Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBIntLightActvnBtn_stat_PushButtonStatus(data) (Rte_BunkUserInterfaceBasic_LINMaCtrl_BunkBIntLightActvnBtn_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_BunkBParkHeater_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBParkHeater_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkBTempDec_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBTempDec_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkBTempInc_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBTempInc_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkBVolumeDown_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBVolumeDown_ButtonStatus_PushButtonStatus
#  define Rte_Write_BunkBVolumeUp_ButtonStatus_PushButtonStatus Rte_Write_BunkUserInterfaceBasic_LINMaCtrl_BunkBVolumeUp_ButtonStatus_PushButtonStatus


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BKG_87_LECMLowLink_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)258, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOG_16_LECMLow_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)235, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOG_17_LECMLow_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)236, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOG_46_LECMLow_EEPROM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)237, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOG_94_LECMLow_SWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)238, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_AudioRadio3_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)6)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_AudioRadio3_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)6)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_OtherInteriorLights5_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)36)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_OtherInteriorLights5_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)36)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PHActMaintainLiving5_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)38)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PHActMaintainLiving5_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)38)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CAQ_LECML_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1CAQ_LECML_Installed_v() (Rte_AddrPar_0x2B_P1CAQ_LECML_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define BunkUserInterfaceBasic_LINMaCtrl_START_SEC_CODE
# include "BunkUserInterfaceBasic_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable
# endif

FUNC(void, BunkUserInterfaceBasic_LINMaCtrl_CODE) BunkUserInterfaceBasic_LINMaCtrl_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define BunkUserInterfaceBasic_LINMaCtrl_STOP_SEC_CODE
# include "BunkUserInterfaceBasic_LINMaCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_BUNKUSERINTERFACEBASIC_LINMACTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
