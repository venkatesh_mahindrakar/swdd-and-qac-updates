/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Keyfob_UICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <Keyfob_UICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_KEYFOB_UICTRL_H
# define _RTE_KEYFOB_UICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Keyfob_UICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_ApproachLightButton_Statu_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_KeyfobSuperLockButton_Sta_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ApproachLightButton_Statu_PushButtonStatus (3U)
#  define Rte_InitValue_KeyfobLockBtn_DoublePress_KeyfobLockBtn_DoublePress (3U)
#  define Rte_InitValue_KeyfobLockButton_Status_PushButtonStatus (3U)
#  define Rte_InitValue_KeyfobPanicButton_Status_KeyfobPanicButton_Status (7U)
#  define Rte_InitValue_KeyfobSuperLockButton_Sta_PushButtonStatus (3U)
#  define Rte_InitValue_KeyfobUnlockButton_Status_PushButtonStatus (3U)
#  define Rte_InitValue_MainSwitchButton_Status_PushButtonStatus (3U)
#  define Rte_InitValue_PredeliveryModeIndication_cmd_PredeliveryModeIndication_cmd (3U)
#  define Rte_InitValue_SwcActivation_Security_SwcActivation_Security (1U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_Keyfob_UICtrl_PredeliveryModeIndication_cmd_PredeliveryModeIndication_cmd(P2VAR(DeactivateActivate_T, AUTOMATIC, RTE_KEYFOB_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Keyfob_UICtrl_KeyfobLockBtn_DoublePress_KeyfobLockBtn_DoublePress(NotDetected_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Keyfob_UICtrl_KeyfobUnlockButton_Status_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Keyfob_UICtrl_MainSwitchButton_Status_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_Keyfob_UICtrl_DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData_Irv_KeyfobStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_Keyfob_UICtrl_DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData_Irv_KeyfobStatus_ReadData(P2VAR(Dcm_Data12ByteType, AUTOMATIC, RTE_KEYFOB_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvWrite_Keyfob_UICtrl_Keyfob_UICtrl_20ms_runnable_Irv_KeyfobStatus_ReadData(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_UICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvWrite_Keyfob_UICtrl_Keyfob_UICtrl_20ms_runnable_Irv_KeyfobStatus_ReadData(P2CONST(Dcm_Data12ByteType, AUTOMATIC, RTE_KEYFOB_UICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_PredeliveryModeIndication_cmd_PredeliveryModeIndication_cmd Rte_Read_Keyfob_UICtrl_PredeliveryModeIndication_cmd_PredeliveryModeIndication_cmd
#  define Rte_Read_SwcActivation_Security_SwcActivation_Security Rte_Read_Keyfob_UICtrl_SwcActivation_Security_SwcActivation_Security
#  define Rte_Read_Keyfob_UICtrl_SwcActivation_Security_SwcActivation_Security(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_Keyfob_UICtrl_VehicleModeInternal_VehicleMode
#  define Rte_Read_Keyfob_UICtrl_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_ApproachLightButton_Statu_PushButtonStatus Rte_Write_Keyfob_UICtrl_ApproachLightButton_Statu_PushButtonStatus
#  define Rte_Write_Keyfob_UICtrl_ApproachLightButton_Statu_PushButtonStatus(data) (Rte_Keyfob_UICtrl_ApproachLightButton_Statu_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KeyfobLockBtn_DoublePress_KeyfobLockBtn_DoublePress Rte_Write_Keyfob_UICtrl_KeyfobLockBtn_DoublePress_KeyfobLockBtn_DoublePress
#  define Rte_Write_KeyfobLockButton_Status_PushButtonStatus Rte_Write_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus
#  define Rte_Write_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus(data) (Rte_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KeyfobPanicButton_Status_KeyfobPanicButton_Status Rte_Write_Keyfob_UICtrl_KeyfobPanicButton_Status_KeyfobPanicButton_Status
#  define Rte_Write_Keyfob_UICtrl_KeyfobPanicButton_Status_KeyfobPanicButton_Status(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KeyfobSuperLockButton_Sta_PushButtonStatus Rte_Write_Keyfob_UICtrl_KeyfobSuperLockButton_Sta_PushButtonStatus
#  define Rte_Write_Keyfob_UICtrl_KeyfobSuperLockButton_Sta_PushButtonStatus(data) (Rte_Keyfob_UICtrl_KeyfobSuperLockButton_Sta_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KeyfobUnlockButton_Status_PushButtonStatus Rte_Write_Keyfob_UICtrl_KeyfobUnlockButton_Status_PushButtonStatus
#  define Rte_Write_MainSwitchButton_Status_PushButtonStatus Rte_Write_Keyfob_UICtrl_MainSwitchButton_Status_PushButtonStatus


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BUK_16_KeyfobLowBattery_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)265, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BUK_63_KeyfobButtonStuck_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)266, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_RKE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_RKE_APPL_CODE) RE_GetKeyfobButtonStatus(P2VAR(uint8, AUTOMATIC, RTE_RKE_APPL_VAR) BatteryStatus, P2VAR(ButtonStatus, AUTOMATIC, RTE_RKE_APPL_VAR) ButtonStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_RKE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_KeyfobButtonStatus_GetKeyfobButtonStatus(arg1, arg2) (RE_GetKeyfobButtonStatus(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ApproachLights1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)4)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ApproachLights1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)4)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_GetIssState(Issm_UserHandleType parg0, P2VAR(Issm_IssStateType, AUTOMATIC, RTE_ISSM_APPL_VAR) issState); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_ApproachLights1_GetIssState(arg1) (Issm_GetIssState((Issm_UserHandleType)4, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_KeyfobPowerControl_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)28)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_KeyfobPowerControl_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)28)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_KeyfobPowerControl_GetIssState(arg1) (Issm_GetIssState((Issm_UserHandleType)28, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_LockControlKeyfobRqst_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)32)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_LockControlKeyfobRqst_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)32)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_LockControlKeyfobRqst_GetIssState(arg1) (Issm_GetIssState((Issm_UserHandleType)32, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PanicAlarmFromKeyfob_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)40)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PanicAlarmFromKeyfob_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)40)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PanicAlarmFromKeyfob_GetIssState(arg1) (Issm_GetIssState((Issm_UserHandleType)40, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PowerWindowsActivate1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)42)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PowerWindowsActivate1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)42)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_PowerWindowsActivate1_GetIssState(arg1) (Issm_GetIssState((Issm_UserHandleType)42, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData_Irv_KeyfobStatus_ReadData(data) \
  Rte_IrvRead_Keyfob_UICtrl_DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData_Irv_KeyfobStatus_ReadData(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_Keyfob_UICtrl_20ms_runnable_Irv_KeyfobStatus_ReadData(data) \
  Rte_IrvWrite_Keyfob_UICtrl_Keyfob_UICtrl_20ms_runnable_Irv_KeyfobStatus_ReadData(data)
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_KeyfobType_P1VKL_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VKL_KeyfobType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1Y1C_SuperlockInhibition_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1VKL_KeyfobType_v() (Rte_AddrPar_0x2B_P1VKL_KeyfobType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1Y1C_SuperlockInhibition_v() (Rte_AddrPar_0x2B_P1Y1C_SuperlockInhibition_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1C54_FactoryModeActive_v() (Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define Keyfob_UICtrl_START_SEC_CODE
# include "Keyfob_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData
#  define RTE_RUNNABLE_Keyfob_UICtrl_20ms_runnable Keyfob_UICtrl_20ms_runnable
#  define RTE_RUNNABLE_Keyfob_UICtrl_Init Keyfob_UICtrl_Init
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_UICtrl_CODE) DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_UICTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_UICtrl_CODE) DataServices_P1DJ8_Data_P1DJ8_KeyfobStatus_ReadData(P2VAR(Dcm_Data12ByteType, AUTOMATIC, RTE_KEYFOB_UICTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, Keyfob_UICtrl_CODE) Keyfob_UICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, Keyfob_UICtrl_CODE) Keyfob_UICtrl_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define Keyfob_UICtrl_STOP_SEC_CODE
# include "Keyfob_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1DJ8_Data_P1DJ8_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_KEYFOB_UICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
