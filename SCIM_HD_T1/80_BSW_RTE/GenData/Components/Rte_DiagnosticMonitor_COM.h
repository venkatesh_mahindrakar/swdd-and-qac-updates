/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DiagnosticMonitor_COM.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DiagnosticMonitor_COM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DIAGNOSTICMONITOR_COM_H
# define _RTE_DIAGNOSTICMONITOR_COM_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DiagnosticMonitor_COM_Type.h"
# include "Rte_DataHandleType.h"


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(uint8, RTE_CODE) Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Mode_<p>_<m>
 *********************************************************************************************************************/
#  define Rte_Mode_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Mode_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Mode_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Mode_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Mode_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff
#  define Rte_Mode_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff Rte_Mode_DiagnosticMonitor_COM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1A6D_88_BB2Net_ComFail_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)277, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1A6E_88_BB1Net_ComFail_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)278, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1A6F_88_CabNet_ComFail_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)279, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1A6H_88_SecurityNet_ComFail_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)280, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1A6L_88_FMSNet_ComFail_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)281, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1E4Q_88_CAN6_ComFail_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)365, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DXX_FMSgateway_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8I_BB2_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8J_BB1_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8K_CabSubnet_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8L_SecuritySubnet_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8M_CAN6_active_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1DXX_FMSgateway_Act_v() (Rte_AddrPar_0x2B_P1DXX_FMSgateway_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V8I_BB2_active_v() (Rte_AddrPar_0x2B_P1V8I_BB2_active_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V8J_BB1_active_v() (Rte_AddrPar_0x2B_P1V8J_BB1_active_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V8K_CabSubnet_active_v() (Rte_AddrPar_0x2B_P1V8K_CabSubnet_active_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V8L_SecuritySubnet_active_v() (Rte_AddrPar_0x2B_P1V8L_SecuritySubnet_active_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V8M_CAN6_active_v() (Rte_AddrPar_0x2B_P1V8M_CAN6_active_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define DiagnosticMonitor_COM_START_SEC_CODE
# include "DiagnosticMonitor_COM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DiagnosticMonitor_COM_10ms_Runnable DiagnosticMonitor_COM_10ms_Runnable
# endif

FUNC(void, DiagnosticMonitor_COM_CODE) DiagnosticMonitor_COM_10ms_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define DiagnosticMonitor_COM_STOP_SEC_CODE
# include "DiagnosticMonitor_COM_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DIAGNOSTICMONITOR_COM_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
