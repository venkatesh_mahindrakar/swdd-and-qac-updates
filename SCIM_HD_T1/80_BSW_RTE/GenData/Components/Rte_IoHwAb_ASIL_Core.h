/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_IoHwAb_ASIL_Core.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <IoHwAb_ASIL_Core>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_IOHWAB_ASIL_CORE_H
# define _RTE_IOHWAB_ASIL_CORE_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_IoHwAb_ASIL_Core_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Fsc_OperationalMode_T, RTE_VAR_NOINIT) Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */

# ifndef RTE_CORE

#  define RTE_START_SEC_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/* RTE Helper-Functions */
FUNC(void, RTE_CODE) Rte_MemCpy(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num);
FUNC(void, RTE_CODE) Rte_MemCpy32(P2VAR(void, AUTOMATIC, RTE_APPL_VAR) destination, P2CONST(void, AUTOMATIC, RTE_APPL_DATA) source, uint32_least num);

#  define RTE_STOP_SEC_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_Fsc_OperationalMode_P_Fsc_OperationalMode (6U)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(IOHWAB_UINT8, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Core_FSCMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VGTT_EcuPinFaultStatus, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Core_IrvBatteryFaultStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(EcuHwVoltageValues_T, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Core_IrvEcuVoltageValues; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(IoAsilCorePcbConfig_T, RTE_VAR_NOINIT) Rte_Irv_IoHwAb_ASIL_Core_IrvIsAdcPinPopulated; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Core_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated(P2VAR(PcbPopulatedInfo_T, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Core_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated(P2VAR(IoAsilCorePcbConfig_T, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_ASIL_Core_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues(P2CONST(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_ASIL_Core_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues(P2CONST(EcuHwVoltageValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Core_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Core_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues(P2VAR(EcuHwVoltageValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Core_EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Core_EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues(P2VAR(EcuHwVoltageValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Core_VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_ASIL_Core_VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues(P2VAR(EcuHwVoltageValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_Fsc_OperationalMode_P_Fsc_OperationalMode Rte_Write_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode
#  define Rte_Write_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode(data) (Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated(data) \
  Rte_IrvRead_IoHwAb_ASIL_Core_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvIsAdcPinPopulated(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues(data) \
  Rte_IrvWrite_IoHwAb_ASIL_Core_CoreHW_ASIL_AdcCtrl_10ms_Runnable_IrvEcuVoltageValues(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvEcuVoltageValues(data) \
   Rte_MemCpy32(Rte_Irv_IoHwAb_ASIL_Core_IrvEcuVoltageValues, data, sizeof(EcuHwVoltageValues_T)); /* PRQA S 3412 */ /* MD_MSR_19.4 */
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_CoreHW_ASIL_AdcCtrl_Init_Runnable_IrvIsAdcPinPopulated(data) \
   Rte_MemCpy32(Rte_Irv_IoHwAb_ASIL_Core_IrvIsAdcPinPopulated, data, sizeof(IoAsilCorePcbConfig_T)); /* PRQA S 3412 */ /* MD_MSR_19.4 */
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues(data) \
  Rte_IrvRead_IoHwAb_ASIL_Core_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvEcuVoltageValues(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_FSCMode(data) \
  (Rte_Irv_IoHwAb_ASIL_Core_FSCMode = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_CoreHW_ASIL_VbatProcess_10ms_runnable_IrvBatteryFaultStatus(data) \
  (Rte_Irv_IoHwAb_ASIL_Core_IrvBatteryFaultStatus = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues(data) \
  Rte_IrvRead_IoHwAb_ASIL_Core_EcuHwState_P_GetEcuVoltages_CS_IrvEcuVoltageValues(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvBatteryFaultStatus() \
  Rte_Irv_IoHwAb_ASIL_Core_IrvBatteryFaultStatus
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues(data) \
  Rte_IrvRead_IoHwAb_ASIL_Core_VbatInterface_P_GetVbatVoltage_CS_IrvEcuVoltageValues(data)
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_PcbConfig_DoorAccessIf_X1CX3_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX3_PcbConfig_DoorAccessIf_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSC_TimeoutThreshold_X1CZR_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CZR_FSC_TimeoutThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_Adi_X1CXW_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOBHS_X1CXX_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOWHS_X1CXY_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOWLS_X1CXZ_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_AdiPullUp_X1CX5_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX5_PcbConfig_AdiPullUp_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_FSC_VoltageThreshold_X1CZQ_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CZQ_FSC_VoltageThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v() (Rte_AddrPar_0x29_X1CX3_PcbConfig_DoorAccessIf_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CZR_FSC_TimeoutThreshold_v() (Rte_AddrPar_0x29_X1CZR_FSC_TimeoutThreshold_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXW_PcbConfig_Adi_v() (&(Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXW_PcbConfig_Adi_v() (&Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXX_PcbConfig_DOBHS_v() (&(Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXX_PcbConfig_DOBHS_v() (&Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXY_PcbConfig_DOWHS_v() (&(Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXY_PcbConfig_DOWHS_v() (&Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXZ_PcbConfig_DOWLS_v() (&(Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXZ_PcbConfig_DOWLS_v() (&Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  define Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v() (&Rte_AddrPar_0x29_X1CX5_PcbConfig_AdiPullUp_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CZQ_FSC_VoltageThreshold_v() (&Rte_AddrPar_0x29_X1CZQ_FSC_VoltageThreshold_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_Diag_Act_DOWHS01_P1V6O_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6O_Diag_Act_DOWHS01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWHS02_P1V6P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6P_Diag_Act_DOWHS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWLS02_P1V7E_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7E_Diag_Act_DOWLS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWLS03_P1V7F_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7F_Diag_Act_DOWLS03_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_HWIO_CfgFault_PWR24V_P1QR6_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1QR6_HWIO_CfgFault_PWR24V_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() (Rte_AddrPar_0x2B_P1V6O_Diag_Act_DOWHS01_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() (Rte_AddrPar_0x2B_P1V6P_Diag_Act_DOWHS02_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() (Rte_AddrPar_0x2B_P1V7E_Diag_Act_DOWLS02_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() (Rte_AddrPar_0x2B_P1V7F_Diag_Act_DOWLS03_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1QR6_HWIO_CfgFault_PWR24V_v() (&Rte_AddrPar_0x2B_P1QR6_HWIO_CfgFault_PWR24V_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define IoHwAb_ASIL_Core_START_SEC_CODE
# include "IoHwAb_ASIL_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CoreHW_ASIL_AdcCtrl_10ms_Runnable CoreHW_ASIL_AdcCtrl_10ms_Runnable
#  define RTE_RUNNABLE_CoreHW_ASIL_AdcCtrl_Init_Runnable CoreHW_ASIL_AdcCtrl_Init_Runnable
#  define RTE_RUNNABLE_CoreHW_ASIL_VbatProcess_10ms_runnable CoreHW_ASIL_VbatProcess_10ms_runnable
#  define RTE_RUNNABLE_EcuHwState_P_GetEcuVoltages_CS EcuHwState_P_GetEcuVoltages_CS
#  define RTE_RUNNABLE_VbatInterface_P_GetVbatVoltage_CS VbatInterface_P_GetVbatVoltage_CS
#  define RTE_RUNNABLE_Watchdog_ASIL_10ms_runnable Watchdog_ASIL_10ms_runnable
# endif

FUNC(void, IoHwAb_ASIL_Core_CODE) CoreHW_ASIL_AdcCtrl_10ms_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, IoHwAb_ASIL_Core_CODE) CoreHW_ASIL_AdcCtrl_Init_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, IoHwAb_ASIL_Core_CODE) CoreHW_ASIL_VbatProcess_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, IoHwAb_ASIL_Core_CODE) EcuHwState_P_GetEcuVoltages_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) EcuVoltageValues); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, IoHwAb_ASIL_Core_CODE) EcuHwState_P_GetEcuVoltages_CS(P2VAR(EcuHwVoltageValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) EcuVoltageValues); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, IoHwAb_ASIL_Core_CODE) VbatInterface_P_GetVbatVoltage_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_ASIL_Core_CODE) Watchdog_ASIL_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define IoHwAb_ASIL_Core_STOP_SEC_CODE
# include "IoHwAb_ASIL_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_EcuHwState_I_AdcInFailure (1U)

#  define RTE_E_VbatInterface_I_AdcInFailure (2U)

#  define RTE_E_VbatInterface_I_IoHwAbApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_IOHWAB_ASIL_CORE_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
