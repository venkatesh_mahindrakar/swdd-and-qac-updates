/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_AuthenticationDevice_UICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <AuthenticationDevice_UICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_AUTHENTICATIONDEVICE_UICTRL_H
# define _RTE_AUTHENTICATIONDEVICE_UICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_AuthenticationDevice_UICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceAuthentication_rqst_T, RTE_VAR_NOINIT) Rte_AuthenticationDevice_UICtrl_DeviceAuthentication_rqst_DeviceAuthentication_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DriverAuthDeviceMatching_T, RTE_VAR_NOINIT) Rte_AuthenticationDevice_UICtrl_DriverAuthDeviceMatching_DriverAuthDeviceMatching; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorsAjar_stat_T, RTE_VAR_NOINIT) Rte_DoorsAjar_stat_ISig_4_oCIOM_BB2_06P_oBackbone2_8810fc26_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ButtonAuth_rqst_ButtonAuth_rqst (3U)
#  define Rte_InitValue_DeviceAuthentication_rqst_DeviceAuthentication_rqst (7U)
#  define Rte_InitValue_DoorsAjar_stat_DoorsAjar_stat (7U)
#  define Rte_InitValue_DriverAuthDeviceMatching_DriverAuthDeviceMatching (3U)
#  define Rte_InitValue_KeyPosition_KeyPosition (7U)
#  define Rte_InitValue_SwcActivation_Security_SwcActivation_Security (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuthenticationDevice_UICtrl_ButtonAuth_rqst_ButtonAuth_rqst(P2VAR(ButtonAuth_rqst_T, AUTOMATIC, RTE_AUTHENTICATIONDEVICE_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuthenticationDevice_UICtrl_KeyPosition_KeyPosition(P2VAR(KeyPosition_T, AUTOMATIC, RTE_AUTHENTICATIONDEVICE_UICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ButtonAuth_rqst_ButtonAuth_rqst Rte_Read_AuthenticationDevice_UICtrl_ButtonAuth_rqst_ButtonAuth_rqst
#  define Rte_Read_DoorsAjar_stat_DoorsAjar_stat Rte_Read_AuthenticationDevice_UICtrl_DoorsAjar_stat_DoorsAjar_stat
#  define Rte_Read_AuthenticationDevice_UICtrl_DoorsAjar_stat_DoorsAjar_stat(data) (*(data) = Rte_DoorsAjar_stat_ISig_4_oCIOM_BB2_06P_oBackbone2_8810fc26_Tx, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyPosition_KeyPosition Rte_Read_AuthenticationDevice_UICtrl_KeyPosition_KeyPosition
#  define Rte_Read_SwcActivation_Security_SwcActivation_Security Rte_Read_AuthenticationDevice_UICtrl_SwcActivation_Security_SwcActivation_Security
#  define Rte_Read_AuthenticationDevice_UICtrl_SwcActivation_Security_SwcActivation_Security(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DeviceAuthentication_rqst_DeviceAuthentication_rqst Rte_Write_AuthenticationDevice_UICtrl_DeviceAuthentication_rqst_DeviceAuthentication_rqst
#  define Rte_Write_AuthenticationDevice_UICtrl_DeviceAuthentication_rqst_DeviceAuthentication_rqst(data) (Rte_AuthenticationDevice_UICtrl_DeviceAuthentication_rqst_DeviceAuthentication_rqst = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_DriverAuthDeviceMatching_DriverAuthDeviceMatching Rte_Write_AuthenticationDevice_UICtrl_DriverAuthDeviceMatching_DriverAuthDeviceMatching
#  define Rte_Write_AuthenticationDevice_UICtrl_DriverAuthDeviceMatching_DriverAuthDeviceMatching(data) (Rte_AuthenticationDevice_UICtrl_DriverAuthDeviceMatching_DriverAuthDeviceMatching = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1T3W_VehSSButtonInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1T3W_VehSSButtonInstalled_v() (Rte_AddrPar_0x2B_P1T3W_VehSSButtonInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define AuthenticationDevice_UICtrl_START_SEC_CODE
# include "AuthenticationDevice_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_AuthenticationDevice_UICtrl_20ms_runnable AuthenticationDevice_UICtrl_20ms_runnable
# endif

FUNC(void, AuthenticationDevice_UICtrl_CODE) AuthenticationDevice_UICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define AuthenticationDevice_UICtrl_STOP_SEC_CODE
# include "AuthenticationDevice_UICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_AUTHENTICATIONDEVICE_UICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
