/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CentralDoorsLatch_Hdlr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <CentralDoorsLatch_Hdlr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_CENTRALDOORSLATCH_HDLR_H
# define _RTE_CENTRALDOORSLATCH_HDLR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_CentralDoorsLatch_Hdlr_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DoorLatch_stat_T, RTE_VAR_NOINIT) Rte_CentralDoorsLatch_Hdlr_DriverDoorLatchInternal_stat_DoorLatch_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorLatch_stat_T, RTE_VAR_NOINIT) Rte_CentralDoorsLatch_Hdlr_PsngDoorLatchInternal_stat_DoorLatch_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorLatch_rqst_decrypt_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorLatch_rqst_decrypt_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_DriverDoorLatchInternal_stat_DoorLatch_stat (7U)
#  define Rte_InitValue_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt (7U)
#  define Rte_InitValue_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt (7U)
#  define Rte_InitValue_PsngDoorLatchInternal_stat_DoorLatch_stat (7U)
#  define Rte_InitValue_SwcActivation_Security_SwcActivation_Security (1U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt Rte_Read_CentralDoorsLatch_Hdlr_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt
#  define Rte_Read_CentralDoorsLatch_Hdlr_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(data) (*(data) = Rte_VehicleAccess_Ctrl_DriverDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt Rte_Read_CentralDoorsLatch_Hdlr_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt
#  define Rte_Read_CentralDoorsLatch_Hdlr_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt(data) (*(data) = Rte_VehicleAccess_Ctrl_PassengrDoorLatch_rqst_decrypt_DoorLatch_rqst_decrypt, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Security_SwcActivation_Security Rte_Read_CentralDoorsLatch_Hdlr_SwcActivation_Security_SwcActivation_Security
#  define Rte_Read_CentralDoorsLatch_Hdlr_SwcActivation_Security_SwcActivation_Security(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DriverDoorLatchInternal_stat_DoorLatch_stat Rte_Write_CentralDoorsLatch_Hdlr_DriverDoorLatchInternal_stat_DoorLatch_stat
#  define Rte_Write_CentralDoorsLatch_Hdlr_DriverDoorLatchInternal_stat_DoorLatch_stat(data) (Rte_CentralDoorsLatch_Hdlr_DriverDoorLatchInternal_stat_DoorLatch_stat = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PsngDoorLatchInternal_stat_DoorLatch_stat Rte_Write_CentralDoorsLatch_Hdlr_PsngDoorLatchInternal_stat_DoorLatch_stat
#  define Rte_Write_CentralDoorsLatch_Hdlr_PsngDoorLatchInternal_stat_DoorLatch_stat(data) (Rte_CentralDoorsLatch_Hdlr_PsngDoorLatchInternal_stat_DoorLatch_stat = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) AdiPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_AdiInterface_P_GetAdiPinState_CS AdiInterface_P_GetAdiPinState_CS


# endif /* !defined(RTE_CORE) */


# define CentralDoorsLatch_Hdlr_START_SEC_CODE
# include "CentralDoorsLatch_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CentralDoorsLatch_Hdlr_20ms_runnable CentralDoorsLatch_Hdlr_20ms_runnable
# endif

FUNC(void, CentralDoorsLatch_Hdlr_CODE) CentralDoorsLatch_Hdlr_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define CentralDoorsLatch_Hdlr_STOP_SEC_CODE
# include "CentralDoorsLatch_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_AdiInterface_I_IoHwAbApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_CENTRALDOORSLATCH_HDLR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
