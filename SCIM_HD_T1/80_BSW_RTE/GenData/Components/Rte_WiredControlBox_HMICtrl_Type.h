/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_WiredControlBox_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <WiredControlBox_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_WIREDCONTROLBOX_HMICTRL_TYPE_H
# define _RTE_WIREDCONTROLBOX_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef ECSStandByReq_Idle
#   define ECSStandByReq_Idle (0U)
#  endif

#  ifndef ECSStandByReq_StandbyRequested
#   define ECSStandByReq_StandbyRequested (1U)
#  endif

#  ifndef ECSStandByReq_StopStandby
#   define ECSStandByReq_StopStandby (2U)
#  endif

#  ifndef ECSStandByReq_Reserved
#   define ECSStandByReq_Reserved (3U)
#  endif

#  ifndef ECSStandByReq_Reserved_01
#   define ECSStandByReq_Reserved_01 (4U)
#  endif

#  ifndef ECSStandByReq_Reserved_02
#   define ECSStandByReq_Reserved_02 (5U)
#  endif

#  ifndef ECSStandByReq_Error
#   define ECSStandByReq_Error (6U)
#  endif

#  ifndef ECSStandByReq_NotAvailable
#   define ECSStandByReq_NotAvailable (7U)
#  endif

#  ifndef EvalButtonRequest_Neutral
#   define EvalButtonRequest_Neutral (0U)
#  endif

#  ifndef EvalButtonRequest_EvaluatingPush
#   define EvalButtonRequest_EvaluatingPush (1U)
#  endif

#  ifndef EvalButtonRequest_ContinuouslyPushed
#   define EvalButtonRequest_ContinuouslyPushed (2U)
#  endif

#  ifndef EvalButtonRequest_ShortPush
#   define EvalButtonRequest_ShortPush (3U)
#  endif

#  ifndef EvalButtonRequest_Spare1
#   define EvalButtonRequest_Spare1 (4U)
#  endif

#  ifndef EvalButtonRequest_Spare2
#   define EvalButtonRequest_Spare2 (5U)
#  endif

#  ifndef EvalButtonRequest_Error
#   define EvalButtonRequest_Error (6U)
#  endif

#  ifndef EvalButtonRequest_NotAvailable
#   define EvalButtonRequest_NotAvailable (7U)
#  endif

#  ifndef FalseTrue_False
#   define FalseTrue_False (0U)
#  endif

#  ifndef FalseTrue_True
#   define FalseTrue_True (1U)
#  endif

#  ifndef FalseTrue_Error
#   define FalseTrue_Error (2U)
#  endif

#  ifndef FalseTrue_NotAvaiable
#   define FalseTrue_NotAvaiable (3U)
#  endif

#  ifndef LevelAdjustmentAction_Idle
#   define LevelAdjustmentAction_Idle (0U)
#  endif

#  ifndef LevelAdjustmentAction_UpBasic
#   define LevelAdjustmentAction_UpBasic (1U)
#  endif

#  ifndef LevelAdjustmentAction_DownBasic
#   define LevelAdjustmentAction_DownBasic (2U)
#  endif

#  ifndef LevelAdjustmentAction_UpShortMovement
#   define LevelAdjustmentAction_UpShortMovement (3U)
#  endif

#  ifndef LevelAdjustmentAction_DownShortMovement
#   define LevelAdjustmentAction_DownShortMovement (4U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved
#   define LevelAdjustmentAction_Reserved (5U)
#  endif

#  ifndef LevelAdjustmentAction_GotoDriveLevel
#   define LevelAdjustmentAction_GotoDriveLevel (6U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_01
#   define LevelAdjustmentAction_Reserved_01 (7U)
#  endif

#  ifndef LevelAdjustmentAction_Ferry
#   define LevelAdjustmentAction_Ferry (8U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_02
#   define LevelAdjustmentAction_Reserved_02 (9U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_03
#   define LevelAdjustmentAction_Reserved_03 (10U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_04
#   define LevelAdjustmentAction_Reserved_04 (11U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_05
#   define LevelAdjustmentAction_Reserved_05 (12U)
#  endif

#  ifndef LevelAdjustmentAction_Reserved_06
#   define LevelAdjustmentAction_Reserved_06 (13U)
#  endif

#  ifndef LevelAdjustmentAction_Error
#   define LevelAdjustmentAction_Error (14U)
#  endif

#  ifndef LevelAdjustmentAction_NotAvailable
#   define LevelAdjustmentAction_NotAvailable (15U)
#  endif

#  ifndef LevelAdjustmentAxles_Rear
#   define LevelAdjustmentAxles_Rear (0U)
#  endif

#  ifndef LevelAdjustmentAxles_Front
#   define LevelAdjustmentAxles_Front (1U)
#  endif

#  ifndef LevelAdjustmentAxles_Parallel
#   define LevelAdjustmentAxles_Parallel (2U)
#  endif

#  ifndef LevelAdjustmentAxles_Reserved
#   define LevelAdjustmentAxles_Reserved (3U)
#  endif

#  ifndef LevelAdjustmentAxles_Reserved_01
#   define LevelAdjustmentAxles_Reserved_01 (4U)
#  endif

#  ifndef LevelAdjustmentAxles_Reserved_02
#   define LevelAdjustmentAxles_Reserved_02 (5U)
#  endif

#  ifndef LevelAdjustmentAxles_Error
#   define LevelAdjustmentAxles_Error (6U)
#  endif

#  ifndef LevelAdjustmentAxles_NotAvailable
#   define LevelAdjustmentAxles_NotAvailable (7U)
#  endif

#  ifndef LevelAdjustmentStroke_DriveStroke
#   define LevelAdjustmentStroke_DriveStroke (0U)
#  endif

#  ifndef LevelAdjustmentStroke_DockingStroke
#   define LevelAdjustmentStroke_DockingStroke (1U)
#  endif

#  ifndef LevelAdjustmentStroke_Reserved
#   define LevelAdjustmentStroke_Reserved (2U)
#  endif

#  ifndef LevelAdjustmentStroke_Reserved_01
#   define LevelAdjustmentStroke_Reserved_01 (3U)
#  endif

#  ifndef LevelAdjustmentStroke_Reserved_02
#   define LevelAdjustmentStroke_Reserved_02 (4U)
#  endif

#  ifndef LevelAdjustmentStroke_Reserved_03
#   define LevelAdjustmentStroke_Reserved_03 (5U)
#  endif

#  ifndef LevelAdjustmentStroke_Error
#   define LevelAdjustmentStroke_Error (6U)
#  endif

#  ifndef LevelAdjustmentStroke_NotAvailable
#   define LevelAdjustmentStroke_NotAvailable (7U)
#  endif

#  ifndef LevelControlInformation_Prohibit
#   define LevelControlInformation_Prohibit (0U)
#  endif

#  ifndef LevelControlInformation_HeightAdjustment
#   define LevelControlInformation_HeightAdjustment (1U)
#  endif

#  ifndef LevelControlInformation_DockingHeight
#   define LevelControlInformation_DockingHeight (2U)
#  endif

#  ifndef LevelControlInformation_DriveHeight
#   define LevelControlInformation_DriveHeight (3U)
#  endif

#  ifndef LevelControlInformation_KneelingHeight
#   define LevelControlInformation_KneelingHeight (4U)
#  endif

#  ifndef LevelControlInformation_FerryFunction
#   define LevelControlInformation_FerryFunction (5U)
#  endif

#  ifndef LevelControlInformation_LimpHome
#   define LevelControlInformation_LimpHome (6U)
#  endif

#  ifndef LevelControlInformation_Reserved
#   define LevelControlInformation_Reserved (7U)
#  endif

#  ifndef LevelControlInformation_Reserved_01
#   define LevelControlInformation_Reserved_01 (8U)
#  endif

#  ifndef LevelControlInformation_Reserved_02
#   define LevelControlInformation_Reserved_02 (9U)
#  endif

#  ifndef LevelControlInformation_Reserved_03
#   define LevelControlInformation_Reserved_03 (10U)
#  endif

#  ifndef LevelControlInformation_Reserved_04
#   define LevelControlInformation_Reserved_04 (11U)
#  endif

#  ifndef LevelControlInformation_Reserved_05
#   define LevelControlInformation_Reserved_05 (12U)
#  endif

#  ifndef LevelControlInformation_Reserved_06
#   define LevelControlInformation_Reserved_06 (13U)
#  endif

#  ifndef LevelControlInformation_Error
#   define LevelControlInformation_Error (14U)
#  endif

#  ifndef LevelControlInformation_NotAvailable
#   define LevelControlInformation_NotAvailable (15U)
#  endif

#  ifndef LevelUserMemoryAction_Inactive
#   define LevelUserMemoryAction_Inactive (0U)
#  endif

#  ifndef LevelUserMemoryAction_Recall
#   define LevelUserMemoryAction_Recall (1U)
#  endif

#  ifndef LevelUserMemoryAction_Store
#   define LevelUserMemoryAction_Store (2U)
#  endif

#  ifndef LevelUserMemoryAction_Default
#   define LevelUserMemoryAction_Default (3U)
#  endif

#  ifndef LevelUserMemoryAction_Reserved
#   define LevelUserMemoryAction_Reserved (4U)
#  endif

#  ifndef LevelUserMemoryAction_Reserved_01
#   define LevelUserMemoryAction_Reserved_01 (5U)
#  endif

#  ifndef LevelUserMemoryAction_Error
#   define LevelUserMemoryAction_Error (6U)
#  endif

#  ifndef LevelUserMemoryAction_NotAvailable
#   define LevelUserMemoryAction_NotAvailable (7U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef RampLevelRequest_TakeNoAction
#   define RampLevelRequest_TakeNoAction (0U)
#  endif

#  ifndef RampLevelRequest_RampLevelM1
#   define RampLevelRequest_RampLevelM1 (1U)
#  endif

#  ifndef RampLevelRequest_RampLevelM2
#   define RampLevelRequest_RampLevelM2 (2U)
#  endif

#  ifndef RampLevelRequest_RampLevelM3
#   define RampLevelRequest_RampLevelM3 (3U)
#  endif

#  ifndef RampLevelRequest_RampLevelM4
#   define RampLevelRequest_RampLevelM4 (4U)
#  endif

#  ifndef RampLevelRequest_RampLevelM5
#   define RampLevelRequest_RampLevelM5 (5U)
#  endif

#  ifndef RampLevelRequest_RampLevelM6
#   define RampLevelRequest_RampLevelM6 (6U)
#  endif

#  ifndef RampLevelRequest_RampLevelM7
#   define RampLevelRequest_RampLevelM7 (7U)
#  endif

#  ifndef RampLevelRequest_NotDefined_04
#   define RampLevelRequest_NotDefined_04 (8U)
#  endif

#  ifndef RampLevelRequest_NotDefined_05
#   define RampLevelRequest_NotDefined_05 (9U)
#  endif

#  ifndef RampLevelRequest_NotDefined_06
#   define RampLevelRequest_NotDefined_06 (10U)
#  endif

#  ifndef RampLevelRequest_NotDefined_07
#   define RampLevelRequest_NotDefined_07 (11U)
#  endif

#  ifndef RampLevelRequest_NotDefined_08
#   define RampLevelRequest_NotDefined_08 (12U)
#  endif

#  ifndef RampLevelRequest_NotDefined_09
#   define RampLevelRequest_NotDefined_09 (13U)
#  endif

#  ifndef RampLevelRequest_ErrorIndicator
#   define RampLevelRequest_ErrorIndicator (14U)
#  endif

#  ifndef RampLevelRequest_NotAvailable
#   define RampLevelRequest_NotAvailable (15U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

#  ifndef VehicleMode_Hibernate
#   define VehicleMode_Hibernate (0U)
#  endif

#  ifndef VehicleMode_Parked
#   define VehicleMode_Parked (1U)
#  endif

#  ifndef VehicleMode_Living
#   define VehicleMode_Living (2U)
#  endif

#  ifndef VehicleMode_Accessory
#   define VehicleMode_Accessory (3U)
#  endif

#  ifndef VehicleMode_PreRunning
#   define VehicleMode_PreRunning (4U)
#  endif

#  ifndef VehicleMode_Cranking
#   define VehicleMode_Cranking (5U)
#  endif

#  ifndef VehicleMode_Running
#   define VehicleMode_Running (6U)
#  endif

#  ifndef VehicleMode_Spare_1
#   define VehicleMode_Spare_1 (7U)
#  endif

#  ifndef VehicleMode_Spare_2
#   define VehicleMode_Spare_2 (8U)
#  endif

#  ifndef VehicleMode_Spare_3
#   define VehicleMode_Spare_3 (9U)
#  endif

#  ifndef VehicleMode_Spare_4
#   define VehicleMode_Spare_4 (10U)
#  endif

#  ifndef VehicleMode_Spare_5
#   define VehicleMode_Spare_5 (11U)
#  endif

#  ifndef VehicleMode_Spare_6
#   define VehicleMode_Spare_6 (12U)
#  endif

#  ifndef VehicleMode_Spare_7
#   define VehicleMode_Spare_7 (13U)
#  endif

#  ifndef VehicleMode_Error
#   define VehicleMode_Error (14U)
#  endif

#  ifndef VehicleMode_NotAvailable
#   define VehicleMode_NotAvailable (15U)
#  endif

#  ifndef WiredLevelUserMemory_MemOff
#   define WiredLevelUserMemory_MemOff (0U)
#  endif

#  ifndef WiredLevelUserMemory_M1
#   define WiredLevelUserMemory_M1 (1U)
#  endif

#  ifndef WiredLevelUserMemory_M2
#   define WiredLevelUserMemory_M2 (2U)
#  endif

#  ifndef WiredLevelUserMemory_M3
#   define WiredLevelUserMemory_M3 (3U)
#  endif

#  ifndef WiredLevelUserMemory_M4
#   define WiredLevelUserMemory_M4 (4U)
#  endif

#  ifndef WiredLevelUserMemory_M5
#   define WiredLevelUserMemory_M5 (5U)
#  endif

#  ifndef WiredLevelUserMemory_M6
#   define WiredLevelUserMemory_M6 (6U)
#  endif

#  ifndef WiredLevelUserMemory_M7
#   define WiredLevelUserMemory_M7 (7U)
#  endif

#  ifndef WiredLevelUserMemory_Spare
#   define WiredLevelUserMemory_Spare (8U)
#  endif

#  ifndef WiredLevelUserMemory_Spare01
#   define WiredLevelUserMemory_Spare01 (9U)
#  endif

#  ifndef WiredLevelUserMemory_Spare02
#   define WiredLevelUserMemory_Spare02 (10U)
#  endif

#  ifndef WiredLevelUserMemory_Spare03
#   define WiredLevelUserMemory_Spare03 (11U)
#  endif

#  ifndef WiredLevelUserMemory_Spare04
#   define WiredLevelUserMemory_Spare04 (12U)
#  endif

#  ifndef WiredLevelUserMemory_Spare05
#   define WiredLevelUserMemory_Spare05 (13U)
#  endif

#  ifndef WiredLevelUserMemory_Error
#   define WiredLevelUserMemory_Error (14U)
#  endif

#  ifndef WiredLevelUserMemory_NotAvailable
#   define WiredLevelUserMemory_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_WIREDCONTROLBOX_HMICTRL_TYPE_H */
