/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Immo_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <Immo>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_IMMO_TYPE_H
# define _RTE_IMMO_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef ImmoDriver_ProcessingStatus_Idle
#   define ImmoDriver_ProcessingStatus_Idle (0U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_Ongoing
#   define ImmoDriver_ProcessingStatus_Ongoing (1U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_LfSent
#   define ImmoDriver_ProcessingStatus_LfSent (2U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_DecryptedInList
#   define ImmoDriver_ProcessingStatus_DecryptedInList (3U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_DecryptedNotInList
#   define ImmoDriver_ProcessingStatus_DecryptedNotInList (4U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_NotDecrypted
#   define ImmoDriver_ProcessingStatus_NotDecrypted (5U)
#  endif

#  ifndef ImmoDriver_ProcessingStatus_HwError
#   define ImmoDriver_ProcessingStatus_HwError (6U)
#  endif

#  ifndef ImmoType_ATA5702
#   define ImmoType_ATA5702 (0U)
#  endif

#  ifndef ImmoType_ATA5577
#   define ImmoType_ATA5577 (1U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_IMMO_TYPE_H */
