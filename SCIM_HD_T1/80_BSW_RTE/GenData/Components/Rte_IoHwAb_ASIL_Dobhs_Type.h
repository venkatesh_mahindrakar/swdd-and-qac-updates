/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_IoHwAb_ASIL_Dobhs_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <IoHwAb_ASIL_Dobhs>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_IOHWAB_ASIL_DOBHS_TYPE_H
# define _RTE_IOHWAB_ASIL_DOBHS_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef Diag_Active_FALSE
#   define Diag_Active_FALSE (0U)
#  endif

#  ifndef Diag_Active_TRUE
#   define Diag_Active_TRUE (1U)
#  endif

#  ifndef FSC_ShutdownReady
#   define FSC_ShutdownReady (0U)
#  endif

#  ifndef FSC_Reduced_12vDcDcLimit
#   define FSC_Reduced_12vDcDcLimit (1U)
#  endif

#  ifndef FSC_Reduced
#   define FSC_Reduced (2U)
#  endif

#  ifndef FSC_Operating
#   define FSC_Operating (3U)
#  endif

#  ifndef FSC_Protecting
#   define FSC_Protecting (4U)
#  endif

#  ifndef FSC_Withstand
#   define FSC_Withstand (5U)
#  endif

#  ifndef FSC_NotAvailable
#   define FSC_NotAvailable (6U)
#  endif

#  ifndef IOCtrl_AppRequest
#   define IOCtrl_AppRequest (0U)
#  endif

#  ifndef IOCtrl_DiagReturnCtrlToApp
#   define IOCtrl_DiagReturnCtrlToApp (1U)
#  endif

#  ifndef IOCtrl_DiagShortTermAdjust
#   define IOCtrl_DiagShortTermAdjust (2U)
#  endif

#  ifndef STD_LOW
#   define STD_LOW (0U)
#  endif

#  ifndef STD_HIGH
#   define STD_HIGH (1U)
#  endif

#  ifndef Inactive
#   define Inactive (255U)
#  endif

#  ifndef TestNotRun
#   define TestNotRun (0U)
#  endif

#  ifndef OffState_NoFaultDetected
#   define OffState_NoFaultDetected (16U)
#  endif

#  ifndef OffState_FaultDetected_STG
#   define OffState_FaultDetected_STG (17U)
#  endif

#  ifndef OffState_FaultDetected_STB
#   define OffState_FaultDetected_STB (18U)
#  endif

#  ifndef OffState_FaultDetected_OC
#   define OffState_FaultDetected_OC (19U)
#  endif

#  ifndef OffState_FaultDetected_VBT
#   define OffState_FaultDetected_VBT (22U)
#  endif

#  ifndef OffState_FaultDetected_VAT
#   define OffState_FaultDetected_VAT (23U)
#  endif

#  ifndef OnState_NoFaultDetected
#   define OnState_NoFaultDetected (32U)
#  endif

#  ifndef OnState_FaultDetected_STG
#   define OnState_FaultDetected_STG (33U)
#  endif

#  ifndef OnState_FaultDetected_STB
#   define OnState_FaultDetected_STB (34U)
#  endif

#  ifndef OnState_FaultDetected_OC
#   define OnState_FaultDetected_OC (35U)
#  endif

#  ifndef OnState_FaultDetected_VBT
#   define OnState_FaultDetected_VBT (38U)
#  endif

#  ifndef OnState_FaultDetected_VAT
#   define OnState_FaultDetected_VAT (39U)
#  endif

#  ifndef OnState_FaultDetected_VOR
#   define OnState_FaultDetected_VOR (41U)
#  endif

#  ifndef OnState_FaultDetected_CAT
#   define OnState_FaultDetected_CAT (44U)
#  endif

#  ifndef SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated
#   define SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated (0U)
#  endif

#  ifndef SEWS_PcbConfig_DOBHS_X1CXX_T_Populated
#   define SEWS_PcbConfig_DOBHS_X1CXX_T_Populated (1U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_IOHWAB_ASIL_DOBHS_TYPE_H */
