/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_WiredControlBox_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <WiredControlBox_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_WIREDCONTROLBOX_HMICTRL_H
# define _RTE_WIREDCONTROLBOX_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_WiredControlBox_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_Adjust_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_Down_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ECSStandByReq_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_ECSStandByReqRCECS_ECSStandByReqRCECS; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_M1_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_M2_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_M3_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ShortPulseMaxLength_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_ShortPulseMaxLength_ShortPulseMaxLength; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_Up_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FalseTrue_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredAirSuspensionStopRequest_AirSuspensionStopRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(LevelAdjustmentAction_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAction_LevelAdjustmentAction; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(LevelAdjustmentAxles_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAxles_LevelAdjustmentAxles; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(LevelAdjustmentStroke_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentStroke_LevelAdjustmentStroke; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(WiredLevelUserMemory_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelUserMemory_WiredLevelUserMemory; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(LevelUserMemoryAction_T, RTE_VAR_NOINIT) Rte_WiredControlBox_HMICtrl_WiredLevelUserMemoryAction_LevelUserMemoryAction; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_AdjustButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_BackButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FalseTrue_T, RTE_VAR_NOINIT) Rte_LevelControl_HMICtrl_BlinkECSWiredLEDs_BlinkECSWiredLEDs; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_MemButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(RampLevelRequest_T, RTE_VAR_NOINIT) Rte_RampLevelRequest_ISig_4_oCIOM_BB2_03P_oBackbone2_26c4f794_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_SelectButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_StopButtonStatus_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(EvalButtonRequest_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_WRDownButtonStatus_EvalButtonRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(EvalButtonRequest_T, RTE_VAR_NOINIT) Rte_ECSWiredRemote_LINMasterCtrl_WRUpButtonStatus_EvalButtonRequest; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AdjustButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_Adjust_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_BackButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_BlinkECSWiredLEDs_BlinkECSWiredLEDs (3U)
#  define Rte_InitValue_Down_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_ECSStandByReqRCECS_ECSStandByReqRCECS (7U)
#  define Rte_InitValue_HeightAdjustmentAllowed_HeightAdjustmentAllowed (3U)
#  define Rte_InitValue_LevelControlInformation_LevelControlInformation (15U)
#  define Rte_InitValue_M1_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_M2_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_M3_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_MemButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_RampLevelRequest_RampLevelRequest (15U)
#  define Rte_InitValue_SelectButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ShortPulseMaxLength_ShortPulseMaxLength (15U)
#  define Rte_InitValue_StopButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
#  define Rte_InitValue_Up_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
#  define Rte_InitValue_WRDownButtonStatus_EvalButtonRequest (7U)
#  define Rte_InitValue_WRUpButtonStatus_EvalButtonRequest (7U)
#  define Rte_InitValue_WiredAirSuspensionStopRequest_AirSuspensionStopRequest (3U)
#  define Rte_InitValue_WiredLevelAdjustmentAction_LevelAdjustmentAction (15U)
#  define Rte_InitValue_WiredLevelAdjustmentAxles_LevelAdjustmentAxles (7U)
#  define Rte_InitValue_WiredLevelAdjustmentStroke_LevelAdjustmentStroke (7U)
#  define Rte_InitValue_WiredLevelUserMemory_WiredLevelUserMemory (15U)
#  define Rte_InitValue_WiredLevelUserMemoryAction_LevelUserMemoryAction (7U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_WiredControlBox_HMICtrl_HeightAdjustmentAllowed_HeightAdjustmentAllowed(P2VAR(FalseTrue_T, AUTOMATIC, RTE_WIREDCONTROLBOX_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_WiredControlBox_HMICtrl_LevelControlInformation_LevelControlInformation(P2VAR(LevelControlInformation_T, AUTOMATIC, RTE_WIREDCONTROLBOX_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AdjustButtonStatus_PushButtonStatus Rte_Read_WiredControlBox_HMICtrl_AdjustButtonStatus_PushButtonStatus
#  define Rte_Read_WiredControlBox_HMICtrl_AdjustButtonStatus_PushButtonStatus(data) (*(data) = Rte_ECSWiredRemote_LINMasterCtrl_AdjustButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BackButtonStatus_PushButtonStatus Rte_Read_WiredControlBox_HMICtrl_BackButtonStatus_PushButtonStatus
#  define Rte_Read_WiredControlBox_HMICtrl_BackButtonStatus_PushButtonStatus(data) (*(data) = Rte_ECSWiredRemote_LINMasterCtrl_BackButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BlinkECSWiredLEDs_BlinkECSWiredLEDs Rte_Read_WiredControlBox_HMICtrl_BlinkECSWiredLEDs_BlinkECSWiredLEDs
#  define Rte_Read_WiredControlBox_HMICtrl_BlinkECSWiredLEDs_BlinkECSWiredLEDs(data) (*(data) = Rte_LevelControl_HMICtrl_BlinkECSWiredLEDs_BlinkECSWiredLEDs, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_HeightAdjustmentAllowed_HeightAdjustmentAllowed Rte_Read_WiredControlBox_HMICtrl_HeightAdjustmentAllowed_HeightAdjustmentAllowed
#  define Rte_Read_LevelControlInformation_LevelControlInformation Rte_Read_WiredControlBox_HMICtrl_LevelControlInformation_LevelControlInformation
#  define Rte_Read_MemButtonStatus_PushButtonStatus Rte_Read_WiredControlBox_HMICtrl_MemButtonStatus_PushButtonStatus
#  define Rte_Read_WiredControlBox_HMICtrl_MemButtonStatus_PushButtonStatus(data) (*(data) = Rte_ECSWiredRemote_LINMasterCtrl_MemButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RampLevelRequest_RampLevelRequest Rte_Read_WiredControlBox_HMICtrl_RampLevelRequest_RampLevelRequest
#  define Rte_Read_WiredControlBox_HMICtrl_RampLevelRequest_RampLevelRequest(data) (*(data) = Rte_RampLevelRequest_ISig_4_oCIOM_BB2_03P_oBackbone2_26c4f794_Tx, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SelectButtonStatus_PushButtonStatus Rte_Read_WiredControlBox_HMICtrl_SelectButtonStatus_PushButtonStatus
#  define Rte_Read_WiredControlBox_HMICtrl_SelectButtonStatus_PushButtonStatus(data) (*(data) = Rte_ECSWiredRemote_LINMasterCtrl_SelectButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_StopButtonStatus_PushButtonStatus Rte_Read_WiredControlBox_HMICtrl_StopButtonStatus_PushButtonStatus
#  define Rte_Read_WiredControlBox_HMICtrl_StopButtonStatus_PushButtonStatus(data) (*(data) = Rte_ECSWiredRemote_LINMasterCtrl_StopButtonStatus_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_WiredControlBox_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_WiredControlBox_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_WiredControlBox_HMICtrl_VehicleModeInternal_VehicleMode
#  define Rte_Read_WiredControlBox_HMICtrl_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WRDownButtonStatus_EvalButtonRequest Rte_Read_WiredControlBox_HMICtrl_WRDownButtonStatus_EvalButtonRequest
#  define Rte_Read_WiredControlBox_HMICtrl_WRDownButtonStatus_EvalButtonRequest(data) (*(data) = Rte_ECSWiredRemote_LINMasterCtrl_WRDownButtonStatus_EvalButtonRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WRUpButtonStatus_EvalButtonRequest Rte_Read_WiredControlBox_HMICtrl_WRUpButtonStatus_EvalButtonRequest
#  define Rte_Read_WiredControlBox_HMICtrl_WRUpButtonStatus_EvalButtonRequest(data) (*(data) = Rte_ECSWiredRemote_LINMasterCtrl_WRUpButtonStatus_EvalButtonRequest, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_Adjust_DeviceIndication_DeviceIndication Rte_Write_WiredControlBox_HMICtrl_Adjust_DeviceIndication_DeviceIndication
#  define Rte_Write_WiredControlBox_HMICtrl_Adjust_DeviceIndication_DeviceIndication(data) (Rte_WiredControlBox_HMICtrl_Adjust_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Down_DeviceIndication_DeviceIndication Rte_Write_WiredControlBox_HMICtrl_Down_DeviceIndication_DeviceIndication
#  define Rte_Write_WiredControlBox_HMICtrl_Down_DeviceIndication_DeviceIndication(data) (Rte_WiredControlBox_HMICtrl_Down_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ECSStandByReqRCECS_ECSStandByReqRCECS Rte_Write_WiredControlBox_HMICtrl_ECSStandByReqRCECS_ECSStandByReqRCECS
#  define Rte_Write_WiredControlBox_HMICtrl_ECSStandByReqRCECS_ECSStandByReqRCECS(data) (Rte_WiredControlBox_HMICtrl_ECSStandByReqRCECS_ECSStandByReqRCECS = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_M1_DeviceIndication_DeviceIndication Rte_Write_WiredControlBox_HMICtrl_M1_DeviceIndication_DeviceIndication
#  define Rte_Write_WiredControlBox_HMICtrl_M1_DeviceIndication_DeviceIndication(data) (Rte_WiredControlBox_HMICtrl_M1_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_M2_DeviceIndication_DeviceIndication Rte_Write_WiredControlBox_HMICtrl_M2_DeviceIndication_DeviceIndication
#  define Rte_Write_WiredControlBox_HMICtrl_M2_DeviceIndication_DeviceIndication(data) (Rte_WiredControlBox_HMICtrl_M2_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_M3_DeviceIndication_DeviceIndication Rte_Write_WiredControlBox_HMICtrl_M3_DeviceIndication_DeviceIndication
#  define Rte_Write_WiredControlBox_HMICtrl_M3_DeviceIndication_DeviceIndication(data) (Rte_WiredControlBox_HMICtrl_M3_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_ShortPulseMaxLength_ShortPulseMaxLength Rte_Write_WiredControlBox_HMICtrl_ShortPulseMaxLength_ShortPulseMaxLength
#  define Rte_Write_WiredControlBox_HMICtrl_ShortPulseMaxLength_ShortPulseMaxLength(data) (Rte_WiredControlBox_HMICtrl_ShortPulseMaxLength_ShortPulseMaxLength = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Up_DeviceIndication_DeviceIndication Rte_Write_WiredControlBox_HMICtrl_Up_DeviceIndication_DeviceIndication
#  define Rte_Write_WiredControlBox_HMICtrl_Up_DeviceIndication_DeviceIndication(data) (Rte_WiredControlBox_HMICtrl_Up_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WiredAirSuspensionStopRequest_AirSuspensionStopRequest Rte_Write_WiredControlBox_HMICtrl_WiredAirSuspensionStopRequest_AirSuspensionStopRequest
#  define Rte_Write_WiredControlBox_HMICtrl_WiredAirSuspensionStopRequest_AirSuspensionStopRequest(data) (Rte_WiredControlBox_HMICtrl_WiredAirSuspensionStopRequest_AirSuspensionStopRequest = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WiredLevelAdjustmentAction_LevelAdjustmentAction Rte_Write_WiredControlBox_HMICtrl_WiredLevelAdjustmentAction_LevelAdjustmentAction
#  define Rte_Write_WiredControlBox_HMICtrl_WiredLevelAdjustmentAction_LevelAdjustmentAction(data) (Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAction_LevelAdjustmentAction = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WiredLevelAdjustmentAxles_LevelAdjustmentAxles Rte_Write_WiredControlBox_HMICtrl_WiredLevelAdjustmentAxles_LevelAdjustmentAxles
#  define Rte_Write_WiredControlBox_HMICtrl_WiredLevelAdjustmentAxles_LevelAdjustmentAxles(data) (Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentAxles_LevelAdjustmentAxles = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WiredLevelAdjustmentStroke_LevelAdjustmentStroke Rte_Write_WiredControlBox_HMICtrl_WiredLevelAdjustmentStroke_LevelAdjustmentStroke
#  define Rte_Write_WiredControlBox_HMICtrl_WiredLevelAdjustmentStroke_LevelAdjustmentStroke(data) (Rte_WiredControlBox_HMICtrl_WiredLevelAdjustmentStroke_LevelAdjustmentStroke = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WiredLevelUserMemory_WiredLevelUserMemory Rte_Write_WiredControlBox_HMICtrl_WiredLevelUserMemory_WiredLevelUserMemory
#  define Rte_Write_WiredControlBox_HMICtrl_WiredLevelUserMemory_WiredLevelUserMemory(data) (Rte_WiredControlBox_HMICtrl_WiredLevelUserMemory_WiredLevelUserMemory = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_WiredLevelUserMemoryAction_LevelUserMemoryAction Rte_Write_WiredControlBox_HMICtrl_WiredLevelUserMemoryAction_LevelUserMemoryAction
#  define Rte_Write_WiredControlBox_HMICtrl_WiredLevelUserMemoryAction_LevelUserMemoryAction(data) (Rte_WiredControlBox_HMICtrl_WiredLevelUserMemoryAction_LevelUserMemoryAction = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BOI_63_RCECS_ButtonStuck_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)249, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_ECSStopButtonHoldTimeout_P1DWI_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWI_ECSStopButtonHoldTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RCECSButtonStucked_P1DWJ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWJ_RCECSButtonStucked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RCECSUpDownStucked_P1DWK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DWK_RCECSUpDownStucked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_RCECS_HoldCircuitTimer_P1IUS_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1IUS_RCECS_HoldCircuitTimer_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ECS_MemSwTimings_P1BWF_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BWF_ECS_MemSwTimings_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ForcedPositionStatus_Front_P1JSY_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1JSY_ForcedPositionStatus_Front_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ForcedPositionStatus_Rear_P1JSZ_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1JSZ_ForcedPositionStatus_Rear_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1DWI_ECSStopButtonHoldTimeout_v() (Rte_AddrPar_0x2B_P1DWI_ECSStopButtonHoldTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DWJ_RCECSButtonStucked_v() (Rte_AddrPar_0x2B_P1DWJ_RCECSButtonStucked_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DWK_RCECSUpDownStucked_v() (Rte_AddrPar_0x2B_P1DWK_RCECSUpDownStucked_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1IUS_RCECS_HoldCircuitTimer_v() (Rte_AddrPar_0x2B_P1IUS_RCECS_HoldCircuitTimer_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BWF_ECS_MemSwTimings_v() (&Rte_AddrPar_0x2B_P1BWF_ECS_MemSwTimings_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1JSY_ForcedPositionStatus_Front_v() (&Rte_AddrPar_0x2B_P1JSY_ForcedPositionStatus_Front_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1JSZ_ForcedPositionStatus_Rear_v() (&Rte_AddrPar_0x2B_P1JSZ_ForcedPositionStatus_Rear_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALT_ECS_PartialAirSystem_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1ALU_ECS_FullAirSystem_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1ALT_ECS_PartialAirSystem_v() (Rte_AddrPar_0x2B_and_0x37_P1ALT_ECS_PartialAirSystem_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1ALU_ECS_FullAirSystem_v() (Rte_AddrPar_0x2B_and_0x37_P1ALU_ECS_FullAirSystem_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define WiredControlBox_HMICtrl_START_SEC_CODE
# include "WiredControlBox_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_WiredControlBox_HMICtrl_20ms_runnable WiredControlBox_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_WiredControlBox_HMICtrl_Init WiredControlBox_HMICtrl_Init
# endif

FUNC(void, WiredControlBox_HMICtrl_CODE) WiredControlBox_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, WiredControlBox_HMICtrl_CODE) WiredControlBox_HMICtrl_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define WiredControlBox_HMICtrl_STOP_SEC_CODE
# include "WiredControlBox_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_WIREDCONTROLBOX_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
