/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_InteriorLights_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <InteriorLights_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_INTERIORLIGHTS_HMICTRL_H
# define _RTE_INTERIORLIGHTS_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_InteriorLights_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_DoorAutoFuncInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(IntLghtLvlIndScaled_cmd_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLghtOffModeInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLightMaxModeInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLightNightModeInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_InteriorLights_HMICtrl_IntLightRestingModeInd_cmd_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceBasic_LINMaCtrl_BunkBIntLightActvnBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightActvnBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_DoorAutoFuncBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtActvnBtn_stat_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtMaxModeFlxSw2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(FreeWheel_Status_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtNightModeBtn2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeBtn_stat_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeFlxSw2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtRestModeBtnPnl2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BunkBIntLightActvnBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH1IntLghtActvnBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2IntLightActvnBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_DoorAutoFuncBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_DoorAutoFuncInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_DoorAutoFunction_rqst_DoorAutoFunction_rqst (3U)
#  define Rte_InitValue_IntLghtActvnBtn_stat_A2PosSwitchStatus (3U)
#  define Rte_InitValue_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status (15U)
#  define Rte_InitValue_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd (15U)
#  define Rte_InitValue_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtMaxModeFlxSw2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status (15U)
#  define Rte_InitValue_IntLghtNightModeBtn2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtNightModeBtn_stat_A2PosSwitchStatus (3U)
#  define Rte_InitValue_IntLghtNightModeFlxSw2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtOffModeInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_IntLghtRestModeBtnPnl2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLightMaxModeInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_IntLightNightModeInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_IntLightRestingModeInd_cmd_DeviceIndication (3U)
#  define Rte_InitValue_InteriorLightDimming_rqst_InteriorLightDimming_rqst (31U)
#  define Rte_InitValue_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd (255U)
#  define Rte_InitValue_KeyfobLockButton_Status_PushButtonStatus (3U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLights_HMICtrl_BnkH1IntLghtMMenu_stat_InteriorLightMode(P2VAR(InteriorLightMode_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLights_HMICtrl_BunkH1IntLghtActvnBtn_stat_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLights_HMICtrl_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLights_HMICtrl_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus(P2VAR(PushButtonStatus_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLights_HMICtrl_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(P2VAR(FreeWheel_Status_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLights_HMICtrl_IntLightMode_CoreRqst_InteriorLightMode_rqst(P2VAR(InteriorLightMode_rqst_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLights_HMICtrl_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd(P2VAR(Percent8bitNoOffset_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLights_HMICtrl_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(P2VAR(uint8, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLights_HMICtrl_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(P2VAR(StandardNVM_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLights_HMICtrl_DoorAutoFunction_rqst_DoorAutoFunction_rqst(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLights_HMICtrl_IntLghtModeInd_cmd_InteriorLightMode(P2CONST(InteriorLightMode_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLights_HMICtrl_InteriorLightDimming_rqst_InteriorLightDimming_rqst(InteriorLightDimming_rqst_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLights_HMICtrl_InteriorLightMode_rqst_InteriorLightMode_rqst(P2CONST(InteriorLightMode_rqst_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLights_HMICtrl_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(P2CONST(uint8, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLights_HMICtrl_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I(P2CONST(StandardNVM_T, AUTOMATIC, RTE_INTERIORLIGHTS_HMICTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_BnkH1IntLghtMMenu_stat_InteriorLightMode Rte_Read_InteriorLights_HMICtrl_BnkH1IntLghtMMenu_stat_InteriorLightMode
#  define Rte_Read_BunkBIntLightActvnBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_BunkBIntLightActvnBtn_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_BunkBIntLightActvnBtn_stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceBasic_LINMaCtrl_BunkBIntLightActvnBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BunkH1IntLghtActvnBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_BunkH1IntLghtActvnBtn_stat_PushButtonStatus
#  define Rte_Read_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_BunkH1IntLghtDirAccsDnBtn_stat_PushButtonStatus
#  define Rte_Read_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_BunkH1IntLghtDirAccsUpBtn_stat_PushButtonStatus
#  define Rte_Read_BunkH2IntLightActvnBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_BunkH2IntLightActvnBtn_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_BunkH2IntLightActvnBtn_stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightActvnBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightDecreaseBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2IntLightIncreaseBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DoorAutoFuncBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_DoorAutoFuncBtn_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_DoorAutoFuncBtn_stat_PushButtonStatus(data) (*(data) = Rte_InteriorLightPanel_1_LINMastCtrl_DoorAutoFuncBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtActvnBtn_stat_A2PosSwitchStatus Rte_Read_InteriorLights_HMICtrl_IntLghtActvnBtn_stat_A2PosSwitchStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtActvnBtn_stat_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_IntLghtActvnBtn_stat_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(data) (*(data) = Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus(data) (*(data) = Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlDecBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status Rte_Read_InteriorLights_HMICtrl_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status
#  define Rte_Read_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus(data) (*(data) = Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtDimmingLvlIncBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus Rte_Read_InteriorLights_HMICtrl_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_IntLghtDoorAutoMaxModeBtn_stat_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(data) (*(data) = Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtMaxModeFlxSw2_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_IntLghtMaxModeFlxSw2_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtMaxModeFlxSw2_stat_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_IntLghtMaxModeFlxSw2_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status Rte_Read_InteriorLights_HMICtrl_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status(data) (*(data) = Rte_InteriorLightPanel_1_LINMastCtrl_IntLghtModeSelrFreeWheel_stat_FreeWheel_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtNightModeBtn2_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_IntLghtNightModeBtn2_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtNightModeBtn2_stat_PushButtonStatus(data) (*(data) = Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtNightModeBtn2_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtNightModeBtn_stat_A2PosSwitchStatus Rte_Read_InteriorLights_HMICtrl_IntLghtNightModeBtn_stat_A2PosSwitchStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtNightModeBtn_stat_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeBtn_stat_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtNightModeFlxSw2_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_IntLghtNightModeFlxSw2_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtNightModeFlxSw2_stat_PushButtonStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_IntLghtNightModeFlxSw2_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLghtRestModeBtnPnl2_stat_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_IntLghtRestModeBtnPnl2_stat_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(data) (*(data) = Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtRestModeBtnPnl2_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_IntLightMode_CoreRqst_InteriorLightMode_rqst Rte_Read_InteriorLights_HMICtrl_IntLightMode_CoreRqst_InteriorLightMode_rqst
#  define Rte_Read_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd Rte_Read_InteriorLights_HMICtrl_InteriorLightLevelInd_cmd_InteriorLightLevelInd_cmd
#  define Rte_Read_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I Rte_Read_InteriorLights_HMICtrl_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I
#  define Rte_Read_KeyfobLockButton_Status_PushButtonStatus Rte_Read_InteriorLights_HMICtrl_KeyfobLockButton_Status_PushButtonStatus
#  define Rte_Read_InteriorLights_HMICtrl_KeyfobLockButton_Status_PushButtonStatus(data) (*(data) = Rte_Keyfob_UICtrl_KeyfobLockButton_Status_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_InteriorLights_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_InteriorLights_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_IsUpdated_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_IsUpdated_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status Rte_IsUpdated_InteriorLights_HMICtrl_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status
#  define Rte_IsUpdated_InteriorLights_HMICtrl_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status() ((Rte_RxUpdateFlags.Rte_RxUpdate_InteriorLights_HMICtrl_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DoorAutoFuncInd_cmd_DeviceIndication Rte_Write_InteriorLights_HMICtrl_DoorAutoFuncInd_cmd_DeviceIndication
#  define Rte_Write_InteriorLights_HMICtrl_DoorAutoFuncInd_cmd_DeviceIndication(data) (Rte_InteriorLights_HMICtrl_DoorAutoFuncInd_cmd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_DoorAutoFunction_rqst_DoorAutoFunction_rqst Rte_Write_InteriorLights_HMICtrl_DoorAutoFunction_rqst_DoorAutoFunction_rqst
#  define Rte_Write_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd Rte_Write_InteriorLights_HMICtrl_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd
#  define Rte_Write_InteriorLights_HMICtrl_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd(data) (Rte_InteriorLights_HMICtrl_IntLghtLvlIndScaled_cmd_IntLghtLvlIndScaled_cmd = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtModeInd_cmd_InteriorLightMode Rte_Write_InteriorLights_HMICtrl_IntLghtModeInd_cmd_InteriorLightMode
#  define Rte_Write_IntLghtOffModeInd_cmd_DeviceIndication Rte_Write_InteriorLights_HMICtrl_IntLghtOffModeInd_cmd_DeviceIndication
#  define Rte_Write_InteriorLights_HMICtrl_IntLghtOffModeInd_cmd_DeviceIndication(data) (Rte_InteriorLights_HMICtrl_IntLghtOffModeInd_cmd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLightMaxModeInd_cmd_DeviceIndication Rte_Write_InteriorLights_HMICtrl_IntLightMaxModeInd_cmd_DeviceIndication
#  define Rte_Write_InteriorLights_HMICtrl_IntLightMaxModeInd_cmd_DeviceIndication(data) (Rte_InteriorLights_HMICtrl_IntLightMaxModeInd_cmd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLightNightModeInd_cmd_DeviceIndication Rte_Write_InteriorLights_HMICtrl_IntLightNightModeInd_cmd_DeviceIndication
#  define Rte_Write_InteriorLights_HMICtrl_IntLightNightModeInd_cmd_DeviceIndication(data) (Rte_InteriorLights_HMICtrl_IntLightNightModeInd_cmd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLightRestingModeInd_cmd_DeviceIndication Rte_Write_InteriorLights_HMICtrl_IntLightRestingModeInd_cmd_DeviceIndication
#  define Rte_Write_InteriorLights_HMICtrl_IntLightRestingModeInd_cmd_DeviceIndication(data) (Rte_InteriorLights_HMICtrl_IntLightRestingModeInd_cmd_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_InteriorLightDimming_rqst_InteriorLightDimming_rqst Rte_Write_InteriorLights_HMICtrl_InteriorLightDimming_rqst_InteriorLightDimming_rqst
#  define Rte_Write_InteriorLightMode_rqst_InteriorLightMode_rqst Rte_Write_InteriorLights_HMICtrl_InteriorLightMode_rqst_InteriorLightMode_rqst
#  define Rte_Write_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I Rte_Write_InteriorLights_HMICtrl_InteriorLights_HMICtrl_NVM_I_InteriorLights_HMICtrl_NVM_I


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss() (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss() (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_DimmingAdjustment2_GetIssState(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss() (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss() (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_OtherInteriorLights3_GetIssState(arg1) (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_IL_LockingCmdDelayOff_P1K7E_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1K7E_IL_LockingCmdDelayOff_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_IL_ShortLongPushThresholds_P1DKF_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DKF_IL_ShortLongPushThresholds_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1K7E_IL_LockingCmdDelayOff_v() (Rte_AddrPar_0x2B_P1K7E_IL_LockingCmdDelayOff_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DKF_IL_ShortLongPushThresholds_v() (&Rte_AddrPar_0x2B_P1DKF_IL_ShortLongPushThresholds_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_IL_CtrlDeviceTypeFront_P1DKH_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DKH_IL_CtrlDeviceTypeFront_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_IL_CtrlDeviceTypeBunk_P1DKI_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DKI_IL_CtrlDeviceTypeBunk_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1DKH_IL_CtrlDeviceTypeFront_v() (Rte_AddrPar_0x2B_and_0x37_P1DKH_IL_CtrlDeviceTypeFront_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DKI_IL_CtrlDeviceTypeBunk_v() (Rte_AddrPar_0x2B_and_0x37_P1DKI_IL_CtrlDeviceTypeBunk_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define InteriorLights_HMICtrl_START_SEC_CODE
# include "InteriorLights_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_InteriorLights_HMICtrl_20ms_runnable InteriorLights_HMICtrl_20ms_runnable
# endif

FUNC(void, InteriorLights_HMICtrl_CODE) InteriorLights_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define InteriorLights_HMICtrl_STOP_SEC_CODE
# include "InteriorLights_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_INTERIORLIGHTS_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
