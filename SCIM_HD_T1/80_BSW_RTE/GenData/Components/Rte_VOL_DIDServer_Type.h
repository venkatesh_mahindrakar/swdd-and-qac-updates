/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VOL_DIDServer_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <VOL_DIDServer>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VOL_DIDSERVER_TYPE_H
# define _RTE_VOL_DIDSERVER_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DCM_INITIAL
#   define DCM_INITIAL (0U)
#  endif

#  ifndef DCM_PENDING
#   define DCM_PENDING (1U)
#  endif

#  ifndef DCM_CANCEL
#   define DCM_CANCEL (2U)
#  endif

#  ifndef DCM_FORCE_RCRRP_OK
#   define DCM_FORCE_RCRRP_OK (3U)
#  endif

#  ifndef DCM_FORCE_RCRRP_NOT_OK
#   define DCM_FORCE_RCRRP_NOT_OK (64U)
#  endif

#  ifndef None
#   define None (0U)
#  endif

#  ifndef LinDiag_BUS1
#   define LinDiag_BUS1 (1U)
#  endif

#  ifndef LinDiag_BUS2
#   define LinDiag_BUS2 (2U)
#  endif

#  ifndef LinDiag_BUS3
#   define LinDiag_BUS3 (3U)
#  endif

#  ifndef LinDiag_BUS4
#   define LinDiag_BUS4 (4U)
#  endif

#  ifndef LinDiag_BUS5
#   define LinDiag_BUS5 (5U)
#  endif

#  ifndef LinDiag_SpareBUS1
#   define LinDiag_SpareBUS1 (6U)
#  endif

#  ifndef LinDiag_SpareBUS2
#   define LinDiag_SpareBUS2 (7U)
#  endif

#  ifndef LinDiag_ALL_BUSSES
#   define LinDiag_ALL_BUSSES (8U)
#  endif

#  ifndef LinDiagService_None
#   define LinDiagService_None (0U)
#  endif

#  ifndef LinDiagService_Pending
#   define LinDiagService_Pending (1U)
#  endif

#  ifndef LinDiagService_Completed
#   define LinDiagService_Completed (2U)
#  endif

#  ifndef LinDiagService_Error
#   define LinDiagService_Error (3U)
#  endif

#  ifndef VehicleMode_Hibernate
#   define VehicleMode_Hibernate (0U)
#  endif

#  ifndef VehicleMode_Parked
#   define VehicleMode_Parked (1U)
#  endif

#  ifndef VehicleMode_Living
#   define VehicleMode_Living (2U)
#  endif

#  ifndef VehicleMode_Accessory
#   define VehicleMode_Accessory (3U)
#  endif

#  ifndef VehicleMode_PreRunning
#   define VehicleMode_PreRunning (4U)
#  endif

#  ifndef VehicleMode_Cranking
#   define VehicleMode_Cranking (5U)
#  endif

#  ifndef VehicleMode_Running
#   define VehicleMode_Running (6U)
#  endif

#  ifndef VehicleMode_Spare_1
#   define VehicleMode_Spare_1 (7U)
#  endif

#  ifndef VehicleMode_Spare_2
#   define VehicleMode_Spare_2 (8U)
#  endif

#  ifndef VehicleMode_Spare_3
#   define VehicleMode_Spare_3 (9U)
#  endif

#  ifndef VehicleMode_Spare_4
#   define VehicleMode_Spare_4 (10U)
#  endif

#  ifndef VehicleMode_Spare_5
#   define VehicleMode_Spare_5 (11U)
#  endif

#  ifndef VehicleMode_Spare_6
#   define VehicleMode_Spare_6 (12U)
#  endif

#  ifndef VehicleMode_Spare_7
#   define VehicleMode_Spare_7 (13U)
#  endif

#  ifndef VehicleMode_Error
#   define VehicleMode_Error (14U)
#  endif

#  ifndef VehicleMode_NotAvailable
#   define VehicleMode_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */


/**********************************************************************************************************************
 * Definitions for Mode Management
 *********************************************************************************************************************/
# ifndef RTE_MODETYPE_DcmDiagnosticSessionControl
#  define RTE_MODETYPE_DcmDiagnosticSessionControl
typedef uint8 Rte_ModeType_DcmDiagnosticSessionControl;
# endif

# define RTE_MODE_VOL_DIDServer_DcmDiagnosticSessionControl_DEFAULT_SESSION (1U)
# ifndef RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION
#  define RTE_MODE_DcmDiagnosticSessionControl_DEFAULT_SESSION (1U)
# endif
# define RTE_MODE_VOL_DIDServer_DcmDiagnosticSessionControl_PROGRAMMING_SESSION (2U)
# ifndef RTE_MODE_DcmDiagnosticSessionControl_PROGRAMMING_SESSION
#  define RTE_MODE_DcmDiagnosticSessionControl_PROGRAMMING_SESSION (2U)
# endif
# define RTE_MODE_VOL_DIDServer_DcmDiagnosticSessionControl_EXTENDED_SESSION (3U)
# ifndef RTE_MODE_DcmDiagnosticSessionControl_EXTENDED_SESSION
#  define RTE_MODE_DcmDiagnosticSessionControl_EXTENDED_SESSION (3U)
# endif
# define RTE_TRANSITION_VOL_DIDServer_DcmDiagnosticSessionControl (255U)
# ifndef RTE_TRANSITION_DcmDiagnosticSessionControl
#  define RTE_TRANSITION_DcmDiagnosticSessionControl (255U)
# endif

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VOL_DIDSERVER_TYPE_H */
