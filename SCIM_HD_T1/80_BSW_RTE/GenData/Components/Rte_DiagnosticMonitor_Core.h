/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DiagnosticMonitor_Core.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DiagnosticMonitor_Core>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DIAGNOSTICMONITOR_CORE_H
# define _RTE_DIAGNOSTICMONITOR_CORE_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DiagnosticMonitor_Core_Type.h"
# include "Rte_DataHandleType.h"


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DiagnosticMonitor_Core_EngTraceHW_NvM_I_EngTraceHW_NvM(P2VAR(EngTraceHWData_T, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DiagnosticMonitor_Core_EngTraceHW_NvM_I_EngTraceHW_NvM(P2VAR(EngTraceHWArray, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DiagnosticMonitor_Core_EngTraceHW_NvM_I_EngTraceHW_NvM(P2CONST(EngTraceHWData_T, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DiagnosticMonitor_Core_EngTraceHW_NvM_I_EngTraceHW_NvM(P2CONST(EngTraceHWArray, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_EngTraceHW_NvM_I_EngTraceHW_NvM Rte_Read_DiagnosticMonitor_Core_EngTraceHW_NvM_I_EngTraceHW_NvM


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_EngTraceHW_NvM_I_EngTraceHW_NvM Rte_Write_DiagnosticMonitor_Core_EngTraceHW_NvM_I_EngTraceHW_NvM


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_GetEventFailed(Dem_EventIdType parg0, P2VAR(boolean, AUTOMATIC, RTE_DEMSATELLITE_0_APPL_VAR) EventFailed); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)290, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_GetEventUdsStatus(Dem_EventIdType parg0, P2VAR(Dem_UdsStatusByteType, AUTOMATIC, RTE_DEMSATELLITE_0_APPL_VAR) UDSStatusByte); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)290, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1AD0_41_Core_GeneralChecksumFailure_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)290, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)291, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_44_Core_RamFailure_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)291, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_44_Core_RamFailure_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)291, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)292, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)292, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_45_Core_ProgramMemoryFailure_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)292, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)293, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_46_Core_NvramFailure_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)293, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_46_Core_NvramFailure_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)293, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)294, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)294, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_47_Core_WatchdogFailure_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)294, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)295, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)295, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_48_Core_SupervisionFailure_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)295, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)296, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)296, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_49_Core_InternalElectronicFailure_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)296, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventFailed(arg1) (Dem_GetEventFailed((Dem_EventIdType)297, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_GetEventStatus(arg1) (Dem_GetEventUdsStatus((Dem_EventIdType)297, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1AD0_94_Core_UnexpectedOperation_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)297, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define DiagnosticMonitor_Core_START_SEC_CODE
# include "DiagnosticMonitor_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData
#  define RTE_RUNNABLE_DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData
#  define RTE_RUNNABLE_DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData
#  define RTE_RUNNABLE_DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData
#  define RTE_RUNNABLE_DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData
#  define RTE_RUNNABLE_DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData
#  define RTE_RUNNABLE_DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData
#  define RTE_RUNNABLE_DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData
#  define RTE_RUNNABLE_DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData
#  define RTE_RUNNABLE_DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData
#  define RTE_RUNNABLE_DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_ReadData DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_ReadData
#  define RTE_RUNNABLE_DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_WriteData DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_WriteData
#  define RTE_RUNNABLE_DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_ReadData DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_ReadData
#  define RTE_RUNNABLE_DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_WriteData DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_WriteData
#  define RTE_RUNNABLE_DiagnosticMonitor_Core_10ms_Runnable DiagnosticMonitor_Core_10ms_Runnable
#  define RTE_RUNNABLE_DiagnosticMonitor_Core_Init_Runnable DiagnosticMonitor_Core_Init_Runnable
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_P1QXM_Data_P1QXM_ECUMemoryFailure_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_P1QXP_Data_P1QXP_ECUSupervisionData_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_P1QXR_Data_P1QXR_ECUInternalFailureExtendedData_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_ReadData(P2VAR(Dcm_Data80ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C12_Data_X1C12_CoreDiag_ResetLog_Monitoring_WriteData(P2CONST(Dcm_Data80ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_ReadData(P2VAR(Dcm_Data130ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C13_Data_X1C13_CoreDiag_ResetLog_WriteData(P2CONST(Dcm_Data130ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_ReadData(P2VAR(Dcm_Data126ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData(P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C1U_Data_X1C1U_CoreDiag_Stack_WriteData(P2CONST(Dcm_Data126ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticMonitor_Core_CODE) DataServices_X1C1Z_Data_X1C1Z_CoreDiag_ResetLog_BSW_ReadData(P2VAR(Dcm_Data60ByteType, AUTOMATIC, RTE_DIAGNOSTICMONITOR_CORE_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, DiagnosticMonitor_Core_CODE) DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_ReadData(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, DiagnosticMonitor_Core_CODE) DataServices_X1CYG_Data_X1CYG_SwExecProfile_FirstFaults_WriteData(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, DiagnosticMonitor_Core_CODE) DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_ReadData(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, DiagnosticMonitor_Core_CODE) DataServices_X1CYH_Data_X1CYH_SwExecProfile_LastFaults_WriteData(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, DiagnosticMonitor_Core_CODE) DiagnosticMonitor_Core_10ms_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, DiagnosticMonitor_Core_CODE) DiagnosticMonitor_Core_Init_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define DiagnosticMonitor_Core_STOP_SEC_CODE
# include "DiagnosticMonitor_Core_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1QXM_Data_P1QXM_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1QXP_Data_P1QXP_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1QXR_Data_P1QXR_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1C12_Data_X1C12_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1C13_Data_X1C13_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1C1U_Data_X1C1U_E_NOT_OK (1U)

#  define RTE_E_DataServices_X1C1Z_Data_X1C1Z_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DIAGNOSTICMONITOR_CORE_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
