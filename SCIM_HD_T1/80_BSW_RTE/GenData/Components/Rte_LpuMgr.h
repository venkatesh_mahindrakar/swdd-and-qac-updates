/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_LpuMgr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <LpuMgr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_LPUMGR_H
# define _RTE_LPUMGR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_LpuMgr_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint32, RTE_VAR_NOINIT) Rte_DiagnosticComponent_LpModeRunTime_LpModeRunTime; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Fsc_OperationalMode_T, RTE_VAR_NOINIT) Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_FSC_OperationalMode_P_Fsc_OperationalMode (6U)
#  define Rte_InitValue_LpModeRunTime_P_LpModeRunTime (0U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_FSC_OperationalMode_P_Fsc_OperationalMode Rte_Read_LpuMgr_FSC_OperationalMode_P_Fsc_OperationalMode
#  define Rte_Read_LpuMgr_FSC_OperationalMode_P_Fsc_OperationalMode(data) (*(data) = Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_LpuMgr_VehicleModeInternal_VehicleMode
#  define Rte_Read_LpuMgr_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_LpModeRunTime_P_LpModeRunTime Rte_Write_LpuMgr_LpModeRunTime_P_LpModeRunTime
#  define Rte_Write_LpuMgr_LpModeRunTime_P_LpModeRunTime(data) (Rte_DiagnosticComponent_LpModeRunTime_LpModeRunTime = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_HwToleranceThreshold_X1C04_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DoorAccessIf_X1CX3_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX3_PcbConfig_DoorAccessIf_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX2_PcbConfig_CanInterfaces_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_Adi_X1CXW_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOBHS_X1CXX_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOWHS_X1CXY_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOWLS_X1CXZ_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX4_PcbConfig_PassiveAntenna_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_AdiPullUp_X1CX5_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX5_PcbConfig_AdiPullUp_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1C04_HwToleranceThreshold_v() (Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v() (Rte_AddrPar_0x29_X1CX3_PcbConfig_DoorAccessIf_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v() (&(Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v() (&Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v() (&(Rte_AddrPar_0x29_X1CX2_PcbConfig_CanInterfaces_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v() (&Rte_AddrPar_0x29_X1CX2_PcbConfig_CanInterfaces_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXW_PcbConfig_Adi_v() (&(Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXW_PcbConfig_Adi_v() (&Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXX_PcbConfig_DOBHS_v() (&(Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXX_PcbConfig_DOBHS_v() (&Rte_AddrPar_0x29_X1CXX_PcbConfig_DOBHS_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXY_PcbConfig_DOWHS_v() (&(Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXY_PcbConfig_DOWHS_v() (&Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXZ_PcbConfig_DOWLS_v() (&(Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXZ_PcbConfig_DOWLS_v() (&Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  define Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v() (&Rte_AddrPar_0x29_X1CX4_PcbConfig_PassiveAntenna_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v() (&Rte_AddrPar_0x29_X1CX5_PcbConfig_AdiPullUp_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v() (&Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WME_LowPowerPullUpAct_Parked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMF_LowPowerPullUpAct_Living_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMN_LowPower12VOutputAct_Living_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMO_LowPower12VOutputAct_Parked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AdiWakeUpConfig_P1WMD_a_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMD_AdiWakeUpConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DAI_Installed_P1WMP_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMP_DAI_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v() (Rte_AddrPar_0x2B_P1WME_LowPowerPullUpAct_Parked_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v() (Rte_AddrPar_0x2B_P1WMF_LowPowerPullUpAct_Living_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v() (Rte_AddrPar_0x2B_P1WMN_LowPower12VOutputAct_Living_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v() (Rte_AddrPar_0x2B_P1WMO_LowPower12VOutputAct_Parked_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_P1WMD_AdiWakeUpConfig_v() (&(Rte_AddrPar_0x2B_P1WMD_AdiWakeUpConfig_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_P1WMD_AdiWakeUpConfig_v() (&Rte_AddrPar_0x2B_P1WMD_AdiWakeUpConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  define Rte_Prm_P1WMP_DAI_Installed_v() (&Rte_AddrPar_0x2B_P1WMP_DAI_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WPP_isSecurityLinActive_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1WPP_isSecurityLinActive_v() (Rte_AddrPar_0x2F_P1WPP_isSecurityLinActive_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define LpuMgr_START_SEC_CODE
# include "LpuMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_LpToVapEntryRunnable LpToVapEntryRunnable
#  define RTE_RUNNABLE_VapToLpEntryRunnable VapToLpEntryRunnable
# endif

FUNC(void, LpuMgr_CODE) LpToVapEntryRunnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, LpuMgr_CODE) VapToLpEntryRunnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define LpuMgr_STOP_SEC_CODE
# include "LpuMgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_LPUMGR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
