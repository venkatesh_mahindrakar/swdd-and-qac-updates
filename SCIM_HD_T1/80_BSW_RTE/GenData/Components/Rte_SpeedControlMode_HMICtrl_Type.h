/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SpeedControlMode_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <SpeedControlMode_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SPEEDCONTROLMODE_HMICTRL_TYPE_H
# define _RTE_SPEEDCONTROLMODE_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef CCIM_ACC_ACCInactive
#   define CCIM_ACC_ACCInactive (0U)
#  endif

#  ifndef CCIM_ACC_ACC1Active
#   define CCIM_ACC_ACC1Active (1U)
#  endif

#  ifndef CCIM_ACC_ACC2Active
#   define CCIM_ACC_ACC2Active (2U)
#  endif

#  ifndef CCIM_ACC_ACC3Active
#   define CCIM_ACC_ACC3Active (3U)
#  endif

#  ifndef CCIM_ACC_Reserved
#   define CCIM_ACC_Reserved (4U)
#  endif

#  ifndef CCIM_ACC_Reserved_01
#   define CCIM_ACC_Reserved_01 (5U)
#  endif

#  ifndef CCIM_ACC_Error
#   define CCIM_ACC_Error (6U)
#  endif

#  ifndef CCIM_ACC_NotAvailable
#   define CCIM_ACC_NotAvailable (7U)
#  endif

#  ifndef CCStates_OffDisabled
#   define CCStates_OffDisabled (0U)
#  endif

#  ifndef CCStates_Hold
#   define CCStates_Hold (1U)
#  endif

#  ifndef CCStates_Accelerate
#   define CCStates_Accelerate (2U)
#  endif

#  ifndef CCStates_Decelerate
#   define CCStates_Decelerate (3U)
#  endif

#  ifndef CCStates_Resume
#   define CCStates_Resume (4U)
#  endif

#  ifndef CCStates_Set
#   define CCStates_Set (5U)
#  endif

#  ifndef CCStates_Driver_Override
#   define CCStates_Driver_Override (6U)
#  endif

#  ifndef CCStates_Spare_1
#   define CCStates_Spare_1 (7U)
#  endif

#  ifndef CCStates_Spare_2
#   define CCStates_Spare_2 (8U)
#  endif

#  ifndef CCStates_Spare_3
#   define CCStates_Spare_3 (9U)
#  endif

#  ifndef CCStates_Spare_4
#   define CCStates_Spare_4 (10U)
#  endif

#  ifndef CCStates_Spare_5
#   define CCStates_Spare_5 (11U)
#  endif

#  ifndef CCStates_Spare_6
#   define CCStates_Spare_6 (12U)
#  endif

#  ifndef CCStates_Spare_7
#   define CCStates_Spare_7 (13U)
#  endif

#  ifndef CCStates_Error
#   define CCStates_Error (14U)
#  endif

#  ifndef CCStates_NotAvailable
#   define CCStates_NotAvailable (15U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef DriverMemory_rqst_UseDefaultDriverMemory
#   define DriverMemory_rqst_UseDefaultDriverMemory (0U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory1
#   define DriverMemory_rqst_UseDriverMemory1 (1U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory2
#   define DriverMemory_rqst_UseDriverMemory2 (2U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory3
#   define DriverMemory_rqst_UseDriverMemory3 (3U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory4
#   define DriverMemory_rqst_UseDriverMemory4 (4U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory5
#   define DriverMemory_rqst_UseDriverMemory5 (5U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory6
#   define DriverMemory_rqst_UseDriverMemory6 (6U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory7
#   define DriverMemory_rqst_UseDriverMemory7 (7U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory8
#   define DriverMemory_rqst_UseDriverMemory8 (8U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory9
#   define DriverMemory_rqst_UseDriverMemory9 (9U)
#  endif

#  ifndef DriverMemory_rqst_UseDriverMemory10
#   define DriverMemory_rqst_UseDriverMemory10 (10U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory1
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory1 (11U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory2
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory2 (12U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory3
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory3 (13U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory4
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory4 (14U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory5
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory5 (15U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory6
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory6 (16U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory7
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory7 (17U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory8
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory8 (18U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory9
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory9 (19U)
#  endif

#  ifndef DriverMemory_rqst_ResetAndThenUseDriverMemory10
#   define DriverMemory_rqst_ResetAndThenUseDriverMemory10 (20U)
#  endif

#  ifndef DriverMemory_rqst_ResetAllMemThenUseDefDriverMem
#   define DriverMemory_rqst_ResetAllMemThenUseDefDriverMem (21U)
#  endif

#  ifndef DriverMemory_rqst_Spare
#   define DriverMemory_rqst_Spare (22U)
#  endif

#  ifndef DriverMemory_rqst_Spare_01
#   define DriverMemory_rqst_Spare_01 (23U)
#  endif

#  ifndef DriverMemory_rqst_Spare_02
#   define DriverMemory_rqst_Spare_02 (24U)
#  endif

#  ifndef DriverMemory_rqst_Spare_03
#   define DriverMemory_rqst_Spare_03 (25U)
#  endif

#  ifndef DriverMemory_rqst_Spare_04
#   define DriverMemory_rqst_Spare_04 (26U)
#  endif

#  ifndef DriverMemory_rqst_Spare_05
#   define DriverMemory_rqst_Spare_05 (27U)
#  endif

#  ifndef DriverMemory_rqst_Spare_06
#   define DriverMemory_rqst_Spare_06 (28U)
#  endif

#  ifndef DriverMemory_rqst_Spare_07
#   define DriverMemory_rqst_Spare_07 (29U)
#  endif

#  ifndef DriverMemory_rqst_NotAvailable
#   define DriverMemory_rqst_NotAvailable (30U)
#  endif

#  ifndef DriverMemory_rqst_Error
#   define DriverMemory_rqst_Error (31U)
#  endif

#  ifndef FWSelectedSpeedControlMode_Off
#   define FWSelectedSpeedControlMode_Off (0U)
#  endif

#  ifndef FWSelectedSpeedControlMode_CruiseControl
#   define FWSelectedSpeedControlMode_CruiseControl (1U)
#  endif

#  ifndef FWSelectedSpeedControlMode_AdjustableSpeedLimiter
#   define FWSelectedSpeedControlMode_AdjustableSpeedLimiter (2U)
#  endif

#  ifndef FWSelectedSpeedControlMode_AdaptiveCruiseControl
#   define FWSelectedSpeedControlMode_AdaptiveCruiseControl (3U)
#  endif

#  ifndef FWSelectedSpeedControlMode_Spare_01
#   define FWSelectedSpeedControlMode_Spare_01 (4U)
#  endif

#  ifndef FWSelectedSpeedControlMode_Spare_02
#   define FWSelectedSpeedControlMode_Spare_02 (5U)
#  endif

#  ifndef FWSelectedSpeedControlMode_Error
#   define FWSelectedSpeedControlMode_Error (6U)
#  endif

#  ifndef FWSelectedSpeedControlMode_NotAvailable
#   define FWSelectedSpeedControlMode_NotAvailable (7U)
#  endif

#  ifndef FreeWheel_Status_NoMovement
#   define FreeWheel_Status_NoMovement (0U)
#  endif

#  ifndef FreeWheel_Status_1StepClockwise
#   define FreeWheel_Status_1StepClockwise (1U)
#  endif

#  ifndef FreeWheel_Status_2StepsClockwise
#   define FreeWheel_Status_2StepsClockwise (2U)
#  endif

#  ifndef FreeWheel_Status_3StepsClockwise
#   define FreeWheel_Status_3StepsClockwise (3U)
#  endif

#  ifndef FreeWheel_Status_4StepsClockwise
#   define FreeWheel_Status_4StepsClockwise (4U)
#  endif

#  ifndef FreeWheel_Status_5StepsClockwise
#   define FreeWheel_Status_5StepsClockwise (5U)
#  endif

#  ifndef FreeWheel_Status_6StepsClockwise
#   define FreeWheel_Status_6StepsClockwise (6U)
#  endif

#  ifndef FreeWheel_Status_1StepCounterClockwise
#   define FreeWheel_Status_1StepCounterClockwise (7U)
#  endif

#  ifndef FreeWheel_Status_2StepsCounterClockwise
#   define FreeWheel_Status_2StepsCounterClockwise (8U)
#  endif

#  ifndef FreeWheel_Status_3StepsCounterClockwise
#   define FreeWheel_Status_3StepsCounterClockwise (9U)
#  endif

#  ifndef FreeWheel_Status_4StepsCounterClockwise
#   define FreeWheel_Status_4StepsCounterClockwise (10U)
#  endif

#  ifndef FreeWheel_Status_5StepsCounterClockwise
#   define FreeWheel_Status_5StepsCounterClockwise (11U)
#  endif

#  ifndef FreeWheel_Status_6StepsCounterClockwise
#   define FreeWheel_Status_6StepsCounterClockwise (12U)
#  endif

#  ifndef FreeWheel_Status_Spare
#   define FreeWheel_Status_Spare (13U)
#  endif

#  ifndef FreeWheel_Status_Error
#   define FreeWheel_Status_Error (14U)
#  endif

#  ifndef FreeWheel_Status_NotAvailable
#   define FreeWheel_Status_NotAvailable (15U)
#  endif

#  ifndef OffOn_Off
#   define OffOn_Off (0U)
#  endif

#  ifndef OffOn_On
#   define OffOn_On (1U)
#  endif

#  ifndef OffOn_Error
#   define OffOn_Error (2U)
#  endif

#  ifndef OffOn_NotAvailable
#   define OffOn_NotAvailable (3U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

#  ifndef XRSLStates_Off_disabled
#   define XRSLStates_Off_disabled (0U)
#  endif

#  ifndef XRSLStates_Hold
#   define XRSLStates_Hold (1U)
#  endif

#  ifndef XRSLStates_Accelerate
#   define XRSLStates_Accelerate (2U)
#  endif

#  ifndef XRSLStates_Decelerate
#   define XRSLStates_Decelerate (3U)
#  endif

#  ifndef XRSLStates_Resume
#   define XRSLStates_Resume (4U)
#  endif

#  ifndef XRSLStates_Set
#   define XRSLStates_Set (5U)
#  endif

#  ifndef XRSLStates_Driver_override
#   define XRSLStates_Driver_override (6U)
#  endif

#  ifndef XRSLStates_Error
#   define XRSLStates_Error (14U)
#  endif

#  ifndef XRSLStates_NotAvailable
#   define XRSLStates_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SPEEDCONTROLMODE_HMICTRL_TYPE_H */
