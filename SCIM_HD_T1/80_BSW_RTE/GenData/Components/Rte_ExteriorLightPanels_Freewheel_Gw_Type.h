/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ExteriorLightPanels_Freewheel_Gw_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <ExteriorLightPanels_Freewheel_Gw>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_EXTERIORLIGHTPANELS_FREEWHEEL_GW_TYPE_H
# define _RTE_EXTERIORLIGHTPANELS_FREEWHEEL_GW_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef FreeWheel_Status_NoMovement
#   define FreeWheel_Status_NoMovement (0U)
#  endif

#  ifndef FreeWheel_Status_1StepClockwise
#   define FreeWheel_Status_1StepClockwise (1U)
#  endif

#  ifndef FreeWheel_Status_2StepsClockwise
#   define FreeWheel_Status_2StepsClockwise (2U)
#  endif

#  ifndef FreeWheel_Status_3StepsClockwise
#   define FreeWheel_Status_3StepsClockwise (3U)
#  endif

#  ifndef FreeWheel_Status_4StepsClockwise
#   define FreeWheel_Status_4StepsClockwise (4U)
#  endif

#  ifndef FreeWheel_Status_5StepsClockwise
#   define FreeWheel_Status_5StepsClockwise (5U)
#  endif

#  ifndef FreeWheel_Status_6StepsClockwise
#   define FreeWheel_Status_6StepsClockwise (6U)
#  endif

#  ifndef FreeWheel_Status_1StepCounterClockwise
#   define FreeWheel_Status_1StepCounterClockwise (7U)
#  endif

#  ifndef FreeWheel_Status_2StepsCounterClockwise
#   define FreeWheel_Status_2StepsCounterClockwise (8U)
#  endif

#  ifndef FreeWheel_Status_3StepsCounterClockwise
#   define FreeWheel_Status_3StepsCounterClockwise (9U)
#  endif

#  ifndef FreeWheel_Status_4StepsCounterClockwise
#   define FreeWheel_Status_4StepsCounterClockwise (10U)
#  endif

#  ifndef FreeWheel_Status_5StepsCounterClockwise
#   define FreeWheel_Status_5StepsCounterClockwise (11U)
#  endif

#  ifndef FreeWheel_Status_6StepsCounterClockwise
#   define FreeWheel_Status_6StepsCounterClockwise (12U)
#  endif

#  ifndef FreeWheel_Status_Spare
#   define FreeWheel_Status_Spare (13U)
#  endif

#  ifndef FreeWheel_Status_Error
#   define FreeWheel_Status_Error (14U)
#  endif

#  ifndef FreeWheel_Status_NotAvailable
#   define FreeWheel_Status_NotAvailable (15U)
#  endif

#  ifndef Freewheel_Status_Ctr_NoMovement
#   define Freewheel_Status_Ctr_NoMovement (0U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position1
#   define Freewheel_Status_Ctr_Position1 (1U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position2
#   define Freewheel_Status_Ctr_Position2 (2U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position3
#   define Freewheel_Status_Ctr_Position3 (3U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position4
#   define Freewheel_Status_Ctr_Position4 (4U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position5
#   define Freewheel_Status_Ctr_Position5 (5U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position6
#   define Freewheel_Status_Ctr_Position6 (6U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position7
#   define Freewheel_Status_Ctr_Position7 (7U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position8
#   define Freewheel_Status_Ctr_Position8 (8U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position9
#   define Freewheel_Status_Ctr_Position9 (9U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position10
#   define Freewheel_Status_Ctr_Position10 (10U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position11
#   define Freewheel_Status_Ctr_Position11 (11U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position12
#   define Freewheel_Status_Ctr_Position12 (12U)
#  endif

#  ifndef Freewheel_Status_Ctr_Position13
#   define Freewheel_Status_Ctr_Position13 (13U)
#  endif

#  ifndef Freewheel_Status_Ctr_Error
#   define Freewheel_Status_Ctr_Error (14U)
#  endif

#  ifndef Freewheel_Status_Ctr_NotAvailable
#   define Freewheel_Status_Ctr_NotAvailable (15U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_EXTERIORLIGHTPANELS_FREEWHEEL_GW_TYPE_H */
