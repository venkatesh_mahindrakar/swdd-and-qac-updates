/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VehicleModeDistribution.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <VehicleModeDistribution>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VEHICLEMODEDISTRIBUTION_H
# define _RTE_VEHICLEMODEDISTRIBUTION_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_VehicleModeDistribution_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_LIN_SwcActivation_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Parked_Parked; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Fsc_OperationalMode_T, RTE_VAR_NOINIT) Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Living12VPowerStability, RTE_VAR_NOINIT) Rte_SCIM_PowerSupply12V_Hdlr_Living12VPowerStability_Living12VPowerStability; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_Fsc_OperationalMode_P_Fsc_OperationalMode (6U)
#  define Rte_InitValue_Living12VPowerStability_Living12VPowerStability (0U)
#  define Rte_InitValue_SwcActivation_Accessory_Accessory (1U)
#  define Rte_InitValue_SwcActivation_EngineRun_EngineRun (1U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
#  define Rte_InitValue_SwcActivation_LIN_SwcActivation_LIN (1U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
#  define Rte_InitValue_SwcActivation_Parked_Parked (1U)
#  define Rte_InitValue_SwcActivation_Security_SwcActivation_Security (1U)
#  define Rte_InitValue_VehicleMode_VehicleMode (15U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleModeDistribution_VehicleMode_VehicleMode(P2VAR(VehicleMode_T, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleModeDistribution_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_VehicleModeDistribution_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(P2VAR(StandardNVM_T, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleModeDistribution_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(P2CONST(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_VehicleModeDistribution_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I(P2CONST(StandardNVM_T, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode Rte_Read_VehicleModeDistribution_Fsc_OperationalMode_P_Fsc_OperationalMode
#  define Rte_Read_VehicleModeDistribution_Fsc_OperationalMode_P_Fsc_OperationalMode(data) (*(data) = Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Living12VPowerStability_Living12VPowerStability Rte_Read_VehicleModeDistribution_Living12VPowerStability_Living12VPowerStability
#  define Rte_Read_VehicleModeDistribution_Living12VPowerStability_Living12VPowerStability(data) (*(data) = Rte_SCIM_PowerSupply12V_Hdlr_Living12VPowerStability_Living12VPowerStability, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleMode_VehicleMode Rte_Read_VehicleModeDistribution_VehicleMode_VehicleMode
#  define Rte_Read_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I Rte_Read_VehicleModeDistribution_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_SwcActivation_Accessory_Accessory Rte_Write_VehicleModeDistribution_SwcActivation_Accessory_Accessory
#  define Rte_Write_VehicleModeDistribution_SwcActivation_Accessory_Accessory(data) (RTE_E_OK) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SwcActivation_EngineRun_EngineRun Rte_Write_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun
#  define Rte_Write_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun(data) (Rte_VehicleModeDistribution_SwcActivation_EngineRun_EngineRun = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SwcActivation_IgnitionOn_IgnitionOn Rte_Write_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Write_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn(data) (Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SwcActivation_LIN_SwcActivation_LIN Rte_Write_VehicleModeDistribution_SwcActivation_LIN_SwcActivation_LIN
#  define Rte_Write_VehicleModeDistribution_SwcActivation_LIN_SwcActivation_LIN(data) (Rte_VehicleModeDistribution_SwcActivation_LIN_SwcActivation_LIN = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SwcActivation_Living_Living Rte_Write_VehicleModeDistribution_SwcActivation_Living_Living
#  define Rte_Write_VehicleModeDistribution_SwcActivation_Living_Living(data) (Rte_VehicleModeDistribution_SwcActivation_Living_Living = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SwcActivation_Parked_Parked Rte_Write_VehicleModeDistribution_SwcActivation_Parked_Parked
#  define Rte_Write_VehicleModeDistribution_SwcActivation_Parked_Parked(data) (Rte_VehicleModeDistribution_SwcActivation_Parked_Parked = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_SwcActivation_Security_SwcActivation_Security Rte_Write_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security
#  define Rte_Write_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security(data) (Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I Rte_Write_VehicleModeDistribution_VehicleModeDistribution_NVM_I_VehicleModeDistribution_NVM_I
#  define Rte_Write_VehicleModeInternal_VehicleMode Rte_Write_VehicleModeDistribution_VehicleModeInternal_VehicleMode
#  define Rte_Write_VehicleModeDistribution_VehicleModeInternal_VehicleMode(data) (Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_CIOMOperStateRedundancy_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)9)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_CIOMOperStateRedundancy_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)9)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define VehicleModeDistribution_START_SEC_CODE
# include "VehicleModeDistribution_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData
#  define RTE_RUNNABLE_DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment
#  define RTE_RUNNABLE_DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData
#  define RTE_RUNNABLE_DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData
#  define RTE_RUNNABLE_VehicleModeDistribution_20ms_runnable VehicleModeDistribution_20ms_runnable
# endif

FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1EIJ_Data_P1EIJ_VehicleModeIO_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1QXJ_Data_P1QXJ_EcuRunningTime_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VehicleModeDistribution_CODE) DataServices_P1RG1_Data_P1RG1_VehicleModeTransitionData_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_VEHICLEMODEDISTRIBUTION_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, VehicleModeDistribution_CODE) VehicleModeDistribution_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define VehicleModeDistribution_STOP_SEC_CODE
# include "VehicleModeDistribution_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1EIJ_Data_P1EIJ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1QXJ_Data_P1QXJ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1RG1_Data_P1RG1_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VEHICLEMODEDISTRIBUTION_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
