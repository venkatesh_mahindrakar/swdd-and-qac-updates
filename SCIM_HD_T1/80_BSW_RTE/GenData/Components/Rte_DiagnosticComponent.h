/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DiagnosticComponent.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DiagnosticComponent>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DIAGNOSTICCOMPONENT_H
# define _RTE_DIAGNOSTICCOMPONENT_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DiagnosticComponent_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint32, RTE_VAR_NOINIT) Rte_DiagnosticComponent_LpModeRunTime_LpModeRunTime; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_DayUTC_DayUTC (255U)
#  define Rte_InitValue_DiagActiveState_isDiagActive (0U)
#  define Rte_InitValue_HoursUTC_HoursUTC (255U)
#  define Rte_InitValue_LpModeRunTime_LpModeRunTime (0U)
#  define Rte_InitValue_MinutesUTC_MinutesUTC (255U)
#  define Rte_InitValue_MonthUTC_MonthUTC (255U)
#  define Rte_InitValue_SecondsUTC_SecondsUTC (255U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
#  define Rte_InitValue_YearUTC_YearUTC (255U)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Days8bit_Fact025_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvDayUTC; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Hours8bit_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvHoursUTC; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Minutes8bit_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvMinutesUTC; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Months8bit_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvMonthUTC; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Seconds8bitFact025_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvSecondsUTC; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Years8bit_T, RTE_VAR_NOINIT) Rte_Irv_DiagnosticComponent_IrvYearUTC; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DiagnosticComponent_MinutesUTC_MinutesUTC(P2VAR(Minutes8bit_T, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_DiagnosticComponent_SecondsUTC_SecondsUTC(P2VAR(Seconds8bitFact025_T, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_DayUTC_DayUTC Rte_Read_DiagnosticComponent_DayUTC_DayUTC
#  define Rte_Read_DiagnosticComponent_DayUTC_DayUTC(data) (Com_ReceiveSignal(ComConf_ComSignal_DayUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_ce1b0a51_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_HoursUTC_HoursUTC Rte_Read_DiagnosticComponent_HoursUTC_HoursUTC
#  define Rte_Read_DiagnosticComponent_HoursUTC_HoursUTC(data) (Com_ReceiveSignal(ComConf_ComSignal_HoursUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_0ccfb540_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LpModeRunTime_LpModeRunTime Rte_Read_DiagnosticComponent_LpModeRunTime_LpModeRunTime
#  define Rte_Read_DiagnosticComponent_LpModeRunTime_LpModeRunTime(data) (*(data) = Rte_DiagnosticComponent_LpModeRunTime_LpModeRunTime, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_MinutesUTC_MinutesUTC Rte_Read_DiagnosticComponent_MinutesUTC_MinutesUTC
#  define Rte_Read_MonthUTC_MonthUTC Rte_Read_DiagnosticComponent_MonthUTC_MonthUTC
#  define Rte_Read_DiagnosticComponent_MonthUTC_MonthUTC(data) (Com_ReceiveSignal(ComConf_ComSignal_MonthUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_151be18c_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SecondsUTC_SecondsUTC Rte_Read_DiagnosticComponent_SecondsUTC_SecondsUTC
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_DiagnosticComponent_VehicleModeInternal_VehicleMode
#  define Rte_Read_DiagnosticComponent_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_YearUTC_YearUTC Rte_Read_DiagnosticComponent_YearUTC_YearUTC
#  define Rte_Read_DiagnosticComponent_YearUTC_YearUTC(data) (Com_ReceiveSignal(ComConf_ComSignal_YearUTC_ISig_3_oTD_X_HMIIOM_oBackbone1J1939_e9880687_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_IsUpdated_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_IsUpdated_SecondsUTC_SecondsUTC Rte_IsUpdated_DiagnosticComponent_SecondsUTC_SecondsUTC
#  define Rte_IsUpdated_DiagnosticComponent_SecondsUTC_SecondsUTC() ((Rte_RxUpdateFlags.Rte_RxUpdate_DiagnosticComponent_SecondsUTC_SecondsUTC == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DiagActiveState_isDiagActive Rte_Write_DiagnosticComponent_DiagActiveState_isDiagActive
#  define Rte_Write_DiagnosticComponent_DiagActiveState_isDiagActive(data) (Rte_DiagnosticComponent_DiagActiveState_isDiagActive = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LpModeRunTime_LpModeRunTime Rte_Write_DiagnosticComponent_LpModeRunTime_LpModeRunTime
#  define Rte_Write_DiagnosticComponent_LpModeRunTime_LpModeRunTime(data) (Rte_DiagnosticComponent_LpModeRunTime_LpModeRunTime = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMMASTER_0_APPL_CODE) Dem_SetOperationCycleState(Dem_OperationCycleIdType parg0, Dem_OperationCycleStateType CycleState); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMMASTER_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_OpCycle_PowerCycle_SetOperationCycleState(arg1) (Dem_SetOperationCycleState((Dem_OperationCycleIdType)0, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_Dcm_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)13)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_Dcm_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)13)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_First_Day_ReadData_IrvDayUTC() \
  Rte_Irv_DiagnosticComponent_IrvDayUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_First_Hour_ReadData_IrvHoursUTC() \
  Rte_Irv_DiagnosticComponent_IrvHoursUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_First_Minutes_ReadData_IrvMinutesUTC() \
  Rte_Irv_DiagnosticComponent_IrvMinutesUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_First_Month_ReadData_IrvMonthUTC() \
  Rte_Irv_DiagnosticComponent_IrvMonthUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_First_Seconds_ReadData_IrvSecondsUTC() \
  Rte_Irv_DiagnosticComponent_IrvSecondsUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_First_Year_ReadData_IrvYearUTC() \
  Rte_Irv_DiagnosticComponent_IrvYearUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Day_ReadData_IrvDayUTC() \
  Rte_Irv_DiagnosticComponent_IrvDayUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Hour_ReadData_IrvHoursUTC() \
  Rte_Irv_DiagnosticComponent_IrvHoursUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData_IrvMinutesUTC() \
  Rte_Irv_DiagnosticComponent_IrvMinutesUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Month_ReadData_IrvMonthUTC() \
  Rte_Irv_DiagnosticComponent_IrvMonthUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData_IrvSecondsUTC() \
  Rte_Irv_DiagnosticComponent_IrvSecondsUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_CBReadData_UTCTimeStamp_Latest_Year_ReadData_IrvYearUTC() \
  Rte_Irv_DiagnosticComponent_IrvYearUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvDayUTC() \
  Rte_Irv_DiagnosticComponent_IrvDayUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC() \
  Rte_Irv_DiagnosticComponent_IrvHoursUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC() \
  Rte_Irv_DiagnosticComponent_IrvMinutesUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC() \
  Rte_Irv_DiagnosticComponent_IrvMonthUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC() \
  Rte_Irv_DiagnosticComponent_IrvSecondsUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_LocalTimeDistribution_200ms_Runnable_IrvYearUTC() \
  Rte_Irv_DiagnosticComponent_IrvYearUTC
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvDayUTC(data) \
  (Rte_Irv_DiagnosticComponent_IrvDayUTC = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvHoursUTC(data) \
  (Rte_Irv_DiagnosticComponent_IrvHoursUTC = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMinutesUTC(data) \
  (Rte_Irv_DiagnosticComponent_IrvMinutesUTC = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvMonthUTC(data) \
  (Rte_Irv_DiagnosticComponent_IrvMonthUTC = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvSecondsUTC(data) \
  (Rte_Irv_DiagnosticComponent_IrvSecondsUTC = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_LocalTimeDistribution_200ms_Runnable_IrvYearUTC(data) \
  (Rte_Irv_DiagnosticComponent_IrvYearUTC = (data))
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define DiagnosticComponent_START_SEC_CODE
# include "DiagnosticComponent_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_First_Day_ReadData CBReadData_UTCTimeStamp_First_Day_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_First_Hour_ReadData CBReadData_UTCTimeStamp_First_Hour_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_First_Minutes_ReadData CBReadData_UTCTimeStamp_First_Minutes_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_First_Month_ReadData CBReadData_UTCTimeStamp_First_Month_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_First_Seconds_ReadData CBReadData_UTCTimeStamp_First_Seconds_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_First_Year_ReadData CBReadData_UTCTimeStamp_First_Year_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_Latest_Day_ReadData CBReadData_UTCTimeStamp_Latest_Day_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_Latest_Hour_ReadData CBReadData_UTCTimeStamp_Latest_Hour_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_Latest_Minutes_ReadData CBReadData_UTCTimeStamp_Latest_Minutes_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_Latest_Month_ReadData CBReadData_UTCTimeStamp_Latest_Month_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_Latest_Seconds_ReadData CBReadData_UTCTimeStamp_Latest_Seconds_ReadData
#  define RTE_RUNNABLE_CBReadData_UTCTimeStamp_Latest_Year_ReadData CBReadData_UTCTimeStamp_Latest_Year_ReadData
#  define RTE_RUNNABLE_DcmRequestManufacturerNotification_Confirmation DcmRequestManufacturerNotification_Confirmation
#  define RTE_RUNNABLE_DcmRequestManufacturerNotification_Indication DcmRequestManufacturerNotification_Indication
#  define RTE_RUNNABLE_DiagnosticComponent_Dcm_ActivateIss DiagnosticComponent_Dcm_ActivateIss
#  define RTE_RUNNABLE_DiagnosticComponent_Dcm_DeactivateIss DiagnosticComponent_Dcm_DeactivateIss
#  define RTE_RUNNABLE_DiagnosticCycleCtrl_100ms_Runnable DiagnosticCycleCtrl_100ms_Runnable
#  define RTE_RUNNABLE_LocalTimeDistribution_200ms_Runnable LocalTimeDistribution_200ms_Runnable
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Day_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Day_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Hour_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Hour_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Minutes_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Minutes_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Month_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Month_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Seconds_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Seconds_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Year_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_First_Year_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Day_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Day_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Hour_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Hour_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Minutes_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Minutes_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Month_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Month_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Seconds_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Seconds_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Year_ReadData(P2VAR(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) CBReadData_UTCTimeStamp_Latest_Year_ReadData(P2VAR(DataArrayType_uint8_1, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, DiagnosticComponent_CODE) DcmRequestManufacturerNotification_Confirmation(uint8 SID, uint8 ReqType, uint16 SourceAddress, Dcm_ConfirmationStatusType ConfirmationStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, DiagnosticComponent_CODE) DcmRequestManufacturerNotification_Indication(uint8 SID, P2CONST(uint8, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_DATA) RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, DiagnosticComponent_CODE) DcmRequestManufacturerNotification_Indication(uint8 SID, P2CONST(Dcm_Data2000ByteType, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_DATA) RequestData, uint16 DataSize, uint8 ReqType, uint16 SourceAddress, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_DIAGNOSTICCOMPONENT_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, DiagnosticComponent_CODE) DiagnosticComponent_Dcm_ActivateIss(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, DiagnosticComponent_CODE) DiagnosticComponent_Dcm_DeactivateIss(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, DiagnosticComponent_CODE) DiagnosticCycleCtrl_100ms_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, DiagnosticComponent_CODE) LocalTimeDistribution_200ms_Runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define DiagnosticComponent_STOP_SEC_CODE
# include "DiagnosticComponent_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_CSDataServices_UTCTimeStamp_First_Day_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_First_Hour_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_First_Minutes_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_First_Month_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_First_Seconds_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_First_Year_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_Latest_Day_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_Latest_Hour_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_Latest_Minutes_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_Latest_Month_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_Latest_Seconds_E_NOT_OK (1U)

#  define RTE_E_CSDataServices_UTCTimeStamp_Latest_Year_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)

#  define RTE_E_OperationCycle_E_NOT_OK (1U)

#  define RTE_E_OperationCycle_E_OK (0U)

#  define RTE_E_ServiceRequestNotification_E_NOT_OK (1U)

#  define RTE_E_ServiceRequestNotification_E_REQUEST_NOT_ACCEPTED (8U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DIAGNOSTICCOMPONENT_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
