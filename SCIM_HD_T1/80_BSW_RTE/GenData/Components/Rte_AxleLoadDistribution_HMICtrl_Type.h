/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_AxleLoadDistribution_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <AxleLoadDistribution_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_AXLELOADDISTRIBUTION_HMICTRL_TYPE_H
# define _RTE_AXLELOADDISTRIBUTION_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A2PosSwitchStatus_Off
#   define A2PosSwitchStatus_Off (0U)
#  endif

#  ifndef A2PosSwitchStatus_On
#   define A2PosSwitchStatus_On (1U)
#  endif

#  ifndef A2PosSwitchStatus_Error
#   define A2PosSwitchStatus_Error (2U)
#  endif

#  ifndef A2PosSwitchStatus_NotAvailable
#   define A2PosSwitchStatus_NotAvailable (3U)
#  endif

#  ifndef A3PosSwitchStatus_Middle
#   define A3PosSwitchStatus_Middle (0U)
#  endif

#  ifndef A3PosSwitchStatus_Lower
#   define A3PosSwitchStatus_Lower (1U)
#  endif

#  ifndef A3PosSwitchStatus_Upper
#   define A3PosSwitchStatus_Upper (2U)
#  endif

#  ifndef A3PosSwitchStatus_Spare
#   define A3PosSwitchStatus_Spare (3U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_01
#   define A3PosSwitchStatus_Spare_01 (4U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_02
#   define A3PosSwitchStatus_Spare_02 (5U)
#  endif

#  ifndef A3PosSwitchStatus_Error
#   define A3PosSwitchStatus_Error (6U)
#  endif

#  ifndef A3PosSwitchStatus_NotAvailable
#   define A3PosSwitchStatus_NotAvailable (7U)
#  endif

#  ifndef AltLoadDistribution_rqst_Idle
#   define AltLoadDistribution_rqst_Idle (0U)
#  endif

#  ifndef AltLoadDistribution_rqst_ALDOn
#   define AltLoadDistribution_rqst_ALDOn (1U)
#  endif

#  ifndef AltLoadDistribution_rqst_ALDOff
#   define AltLoadDistribution_rqst_ALDOff (2U)
#  endif

#  ifndef AltLoadDistribution_rqst_Reserved_1
#   define AltLoadDistribution_rqst_Reserved_1 (3U)
#  endif

#  ifndef AltLoadDistribution_rqst_Reserved_2
#   define AltLoadDistribution_rqst_Reserved_2 (4U)
#  endif

#  ifndef AltLoadDistribution_rqst_Reserved_3
#   define AltLoadDistribution_rqst_Reserved_3 (5U)
#  endif

#  ifndef AltLoadDistribution_rqst_Error
#   define AltLoadDistribution_rqst_Error (6U)
#  endif

#  ifndef AltLoadDistribution_rqst_NotAvailable
#   define AltLoadDistribution_rqst_NotAvailable (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerOff
#   define DualDeviceIndication_UpperOffLowerOff (0U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerOff
#   define DualDeviceIndication_UpperOnLowerOff (1U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerOff
#   define DualDeviceIndication_UpperBlinkLowerOff (2U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerOff
#   define DualDeviceIndication_UpperDontCareLowerOff (3U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerOn
#   define DualDeviceIndication_UpperOffLowerOn (4U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerOn
#   define DualDeviceIndication_UpperOnLowerOn (5U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerOn
#   define DualDeviceIndication_UpperBlinkLowerOn (6U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerOn
#   define DualDeviceIndication_UpperDontCareLowerOn (7U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerBlink
#   define DualDeviceIndication_UpperOffLowerBlink (8U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerBlink
#   define DualDeviceIndication_UpperOnLowerBlink (9U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerBlink
#   define DualDeviceIndication_UpperBlinkLowerBlink (10U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerBlink
#   define DualDeviceIndication_UpperDontCareLowerBlink (11U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerDontCare
#   define DualDeviceIndication_UpperOffLowerDontCare (12U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerDontCare
#   define DualDeviceIndication_UpperOnLowerDontCare (13U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerDontCare
#   define DualDeviceIndication_UpperBlinkLowerDontCare (14U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerDontCare
#   define DualDeviceIndication_UpperDontCareLowerDontCare (15U)
#  endif

#  ifndef ECSStandByRequest_NoRequest
#   define ECSStandByRequest_NoRequest (0U)
#  endif

#  ifndef ECSStandByRequest_Initiate
#   define ECSStandByRequest_Initiate (1U)
#  endif

#  ifndef ECSStandByRequest_StandbyRequestedRCECS
#   define ECSStandByRequest_StandbyRequestedRCECS (2U)
#  endif

#  ifndef ECSStandByRequest_StandbyRequestedWRC
#   define ECSStandByRequest_StandbyRequestedWRC (3U)
#  endif

#  ifndef ECSStandByRequest_Reserved
#   define ECSStandByRequest_Reserved (4U)
#  endif

#  ifndef ECSStandByRequest_Reserved_01
#   define ECSStandByRequest_Reserved_01 (5U)
#  endif

#  ifndef ECSStandByRequest_Error
#   define ECSStandByRequest_Error (6U)
#  endif

#  ifndef ECSStandByRequest_NotAvailable
#   define ECSStandByRequest_NotAvailable (7U)
#  endif

#  ifndef InactiveActive_Inactive
#   define InactiveActive_Inactive (0U)
#  endif

#  ifndef InactiveActive_Active
#   define InactiveActive_Active (1U)
#  endif

#  ifndef InactiveActive_Error
#   define InactiveActive_Error (2U)
#  endif

#  ifndef InactiveActive_NotAvailable
#   define InactiveActive_NotAvailable (3U)
#  endif

#  ifndef LiftAxleLiftPositionRequest_Idle
#   define LiftAxleLiftPositionRequest_Idle (0U)
#  endif

#  ifndef LiftAxleLiftPositionRequest_Down
#   define LiftAxleLiftPositionRequest_Down (1U)
#  endif

#  ifndef LiftAxleLiftPositionRequest_Up
#   define LiftAxleLiftPositionRequest_Up (2U)
#  endif

#  ifndef LiftAxleLiftPositionRequest_Reserved
#   define LiftAxleLiftPositionRequest_Reserved (3U)
#  endif

#  ifndef LiftAxleLiftPositionRequest_Reserved_01
#   define LiftAxleLiftPositionRequest_Reserved_01 (4U)
#  endif

#  ifndef LiftAxleLiftPositionRequest_Reserved_02
#   define LiftAxleLiftPositionRequest_Reserved_02 (5U)
#  endif

#  ifndef LiftAxleLiftPositionRequest_Error
#   define LiftAxleLiftPositionRequest_Error (6U)
#  endif

#  ifndef LiftAxleLiftPositionRequest_NotAvailable
#   define LiftAxleLiftPositionRequest_NotAvailable (7U)
#  endif

#  ifndef LiftAxlePositionStatus_Lowered
#   define LiftAxlePositionStatus_Lowered (0U)
#  endif

#  ifndef LiftAxlePositionStatus_Lifted
#   define LiftAxlePositionStatus_Lifted (1U)
#  endif

#  ifndef LiftAxlePositionStatus_Lowering
#   define LiftAxlePositionStatus_Lowering (2U)
#  endif

#  ifndef LiftAxlePositionStatus_Lifting
#   define LiftAxlePositionStatus_Lifting (3U)
#  endif

#  ifndef LiftAxlePositionStatus_Reserved
#   define LiftAxlePositionStatus_Reserved (4U)
#  endif

#  ifndef LiftAxlePositionStatus_Reserved_01
#   define LiftAxlePositionStatus_Reserved_01 (5U)
#  endif

#  ifndef LiftAxlePositionStatus_Error
#   define LiftAxlePositionStatus_Error (6U)
#  endif

#  ifndef LiftAxlePositionStatus_NotAvailable
#   define LiftAxlePositionStatus_NotAvailable (7U)
#  endif

#  ifndef LiftAxleUpRequestACK_NoAction
#   define LiftAxleUpRequestACK_NoAction (0U)
#  endif

#  ifndef LiftAxleUpRequestACK_ChangeAcknowledged
#   define LiftAxleUpRequestACK_ChangeAcknowledged (1U)
#  endif

#  ifndef LiftAxleUpRequestACK_LiftDeniedFrontOverload
#   define LiftAxleUpRequestACK_LiftDeniedFrontOverload (2U)
#  endif

#  ifndef LiftAxleUpRequestACK_LiftDeniedRearOverload
#   define LiftAxleUpRequestACK_LiftDeniedRearOverload (3U)
#  endif

#  ifndef LiftAxleUpRequestACK_LiftDeniedPBrakeActive
#   define LiftAxleUpRequestACK_LiftDeniedPBrakeActive (4U)
#  endif

#  ifndef LiftAxleUpRequestACK_SystemDeniedVersatile
#   define LiftAxleUpRequestACK_SystemDeniedVersatile (5U)
#  endif

#  ifndef LiftAxleUpRequestACK_Error
#   define LiftAxleUpRequestACK_Error (6U)
#  endif

#  ifndef LiftAxleUpRequestACK_NotAvailable
#   define LiftAxleUpRequestACK_NotAvailable (7U)
#  endif

#  ifndef LoadDistributionALDChangeACK_NoAction
#   define LoadDistributionALDChangeACK_NoAction (0U)
#  endif

#  ifndef LoadDistributionALDChangeACK_ChangeAcknowledged
#   define LoadDistributionALDChangeACK_ChangeAcknowledged (1U)
#  endif

#  ifndef LoadDistributionALDChangeACK_LoadShiftDeniedOverspeed
#   define LoadDistributionALDChangeACK_LoadShiftDeniedOverspeed (2U)
#  endif

#  ifndef LoadDistributionALDChangeACK_Spare
#   define LoadDistributionALDChangeACK_Spare (3U)
#  endif

#  ifndef LoadDistributionALDChangeACK_Spare_01
#   define LoadDistributionALDChangeACK_Spare_01 (4U)
#  endif

#  ifndef LoadDistributionALDChangeACK_SystemDeniedVersatile
#   define LoadDistributionALDChangeACK_SystemDeniedVersatile (5U)
#  endif

#  ifndef LoadDistributionALDChangeACK_Error
#   define LoadDistributionALDChangeACK_Error (6U)
#  endif

#  ifndef LoadDistributionALDChangeACK_NotAvailable
#   define LoadDistributionALDChangeACK_NotAvailable (7U)
#  endif

#  ifndef LoadDistributionChangeACK_NoAction
#   define LoadDistributionChangeACK_NoAction (0U)
#  endif

#  ifndef LoadDistributionChangeACK_ChangeAcknowledged
#   define LoadDistributionChangeACK_ChangeAcknowledged (1U)
#  endif

#  ifndef LoadDistributionChangeACK_LoadShiftDeniedFALIMOverload
#   define LoadDistributionChangeACK_LoadShiftDeniedFALIMOverload (2U)
#  endif

#  ifndef LoadDistributionChangeACK_Reserved_1
#   define LoadDistributionChangeACK_Reserved_1 (3U)
#  endif

#  ifndef LoadDistributionChangeACK_Reserved_2
#   define LoadDistributionChangeACK_Reserved_2 (4U)
#  endif

#  ifndef LoadDistributionChangeACK_SystemDeniedVersatile
#   define LoadDistributionChangeACK_SystemDeniedVersatile (5U)
#  endif

#  ifndef LoadDistributionChangeACK_Error
#   define LoadDistributionChangeACK_Error (6U)
#  endif

#  ifndef LoadDistributionChangeACK_NotAvailable
#   define LoadDistributionChangeACK_NotAvailable (7U)
#  endif

#  ifndef LoadDistributionChangeRequest_Idle
#   define LoadDistributionChangeRequest_Idle (0U)
#  endif

#  ifndef LoadDistributionChangeRequest_ChangeLoadDistribution
#   define LoadDistributionChangeRequest_ChangeLoadDistribution (1U)
#  endif

#  ifndef LoadDistributionChangeRequest_Error
#   define LoadDistributionChangeRequest_Error (2U)
#  endif

#  ifndef LoadDistributionChangeRequest_NotAvailable
#   define LoadDistributionChangeRequest_NotAvailable (3U)
#  endif

#  ifndef LoadDistributionFuncSelected_NormalLoad
#   define LoadDistributionFuncSelected_NormalLoad (0U)
#  endif

#  ifndef LoadDistributionFuncSelected_OptimisedTraction
#   define LoadDistributionFuncSelected_OptimisedTraction (1U)
#  endif

#  ifndef LoadDistributionFuncSelected_MaximumTractionTractionHelp
#   define LoadDistributionFuncSelected_MaximumTractionTractionHelp (2U)
#  endif

#  ifndef LoadDistributionFuncSelected_AlternativeLoad
#   define LoadDistributionFuncSelected_AlternativeLoad (3U)
#  endif

#  ifndef LoadDistributionFuncSelected_NoLoadDistribution
#   define LoadDistributionFuncSelected_NoLoadDistribution (4U)
#  endif

#  ifndef LoadDistributionFuncSelected_VariableAxleLoad
#   define LoadDistributionFuncSelected_VariableAxleLoad (5U)
#  endif

#  ifndef LoadDistributionFuncSelected_MaximumTractionStartingHelp
#   define LoadDistributionFuncSelected_MaximumTractionStartingHelp (6U)
#  endif

#  ifndef LoadDistributionFuncSelected_SpareValue_02
#   define LoadDistributionFuncSelected_SpareValue_02 (7U)
#  endif

#  ifndef LoadDistributionFuncSelected_SpareValue_03
#   define LoadDistributionFuncSelected_SpareValue_03 (8U)
#  endif

#  ifndef LoadDistributionFuncSelected_SpareValue_04
#   define LoadDistributionFuncSelected_SpareValue_04 (9U)
#  endif

#  ifndef LoadDistributionFuncSelected_SpareValue_05
#   define LoadDistributionFuncSelected_SpareValue_05 (10U)
#  endif

#  ifndef LoadDistributionFuncSelected_SpareValue_06
#   define LoadDistributionFuncSelected_SpareValue_06 (11U)
#  endif

#  ifndef LoadDistributionFuncSelected_SpareValue_07
#   define LoadDistributionFuncSelected_SpareValue_07 (12U)
#  endif

#  ifndef LoadDistributionFuncSelected_SpareValue_08
#   define LoadDistributionFuncSelected_SpareValue_08 (13U)
#  endif

#  ifndef LoadDistributionFuncSelected_ErrorIndicator
#   define LoadDistributionFuncSelected_ErrorIndicator (14U)
#  endif

#  ifndef LoadDistributionFuncSelected_NotAvaiable
#   define LoadDistributionFuncSelected_NotAvaiable (15U)
#  endif

#  ifndef LoadDistributionRequestedACK_NoAction
#   define LoadDistributionRequestedACK_NoAction (0U)
#  endif

#  ifndef LoadDistributionRequestedACK_ChangeAcknowledged
#   define LoadDistributionRequestedACK_ChangeAcknowledged (1U)
#  endif

#  ifndef LoadDistributionRequestedACK_LoadShiftDeniedFALIMOverload
#   define LoadDistributionRequestedACK_LoadShiftDeniedFALIMOverload (2U)
#  endif

#  ifndef LoadDistributionRequestedACK_Reserved1
#   define LoadDistributionRequestedACK_Reserved1 (3U)
#  endif

#  ifndef LoadDistributionRequestedACK_Reserved2
#   define LoadDistributionRequestedACK_Reserved2 (4U)
#  endif

#  ifndef LoadDistributionRequestedACK_SystemDeniedVersatile
#   define LoadDistributionRequestedACK_SystemDeniedVersatile (5U)
#  endif

#  ifndef LoadDistributionRequestedACK_Error
#   define LoadDistributionRequestedACK_Error (6U)
#  endif

#  ifndef LoadDistributionRequestedACK_NotAvailable
#   define LoadDistributionRequestedACK_NotAvailable (7U)
#  endif

#  ifndef LoadDistributionRequested_NoAction
#   define LoadDistributionRequested_NoAction (0U)
#  endif

#  ifndef LoadDistributionRequested_DefaultLoadDistribution
#   define LoadDistributionRequested_DefaultLoadDistribution (1U)
#  endif

#  ifndef LoadDistributionRequested_NormalLoadDistribution
#   define LoadDistributionRequested_NormalLoadDistribution (2U)
#  endif

#  ifndef LoadDistributionRequested_OptimizedTraction
#   define LoadDistributionRequested_OptimizedTraction (3U)
#  endif

#  ifndef LoadDistributionRequested_MaximumTraction1
#   define LoadDistributionRequested_MaximumTraction1 (4U)
#  endif

#  ifndef LoadDistributionRequested_MaximumTraction2
#   define LoadDistributionRequested_MaximumTraction2 (5U)
#  endif

#  ifndef LoadDistributionRequested_AlternativeLoadDistribution
#   define LoadDistributionRequested_AlternativeLoadDistribution (6U)
#  endif

#  ifndef LoadDistributionRequested_VariableLoadDistribution
#   define LoadDistributionRequested_VariableLoadDistribution (7U)
#  endif

#  ifndef LoadDistributionRequested_DelpressLoadDistribution
#   define LoadDistributionRequested_DelpressLoadDistribution (8U)
#  endif

#  ifndef LoadDistributionRequested_Reserved1
#   define LoadDistributionRequested_Reserved1 (9U)
#  endif

#  ifndef LoadDistributionRequested_Reserved2
#   define LoadDistributionRequested_Reserved2 (10U)
#  endif

#  ifndef LoadDistributionRequested_Reserved3
#   define LoadDistributionRequested_Reserved3 (11U)
#  endif

#  ifndef LoadDistributionRequested_Reserved4
#   define LoadDistributionRequested_Reserved4 (12U)
#  endif

#  ifndef LoadDistributionRequested_Reserved5
#   define LoadDistributionRequested_Reserved5 (13U)
#  endif

#  ifndef LoadDistributionRequested_Error
#   define LoadDistributionRequested_Error (14U)
#  endif

#  ifndef LoadDistributionRequested_NotAvailable
#   define LoadDistributionRequested_NotAvailable (15U)
#  endif

#  ifndef LoadDistributionSelected_NormalLoad
#   define LoadDistributionSelected_NormalLoad (0U)
#  endif

#  ifndef LoadDistributionSelected_OptimisedTraction
#   define LoadDistributionSelected_OptimisedTraction (1U)
#  endif

#  ifndef LoadDistributionSelected_StartingHelp
#   define LoadDistributionSelected_StartingHelp (2U)
#  endif

#  ifndef LoadDistributionSelected_TractionHelp
#   define LoadDistributionSelected_TractionHelp (3U)
#  endif

#  ifndef LoadDistributionSelected_AlternativeLoad
#   define LoadDistributionSelected_AlternativeLoad (4U)
#  endif

#  ifndef LoadDistributionSelected_StartingHelpInterDiff
#   define LoadDistributionSelected_StartingHelpInterDiff (5U)
#  endif

#  ifndef LoadDistributionSelected_TractionHelpInterDiff
#   define LoadDistributionSelected_TractionHelpInterDiff (6U)
#  endif

#  ifndef LoadDistributionSelected_NoLoadDistribution
#   define LoadDistributionSelected_NoLoadDistribution (7U)
#  endif

#  ifndef LoadDistributionSelected_VariableAxleLoad
#   define LoadDistributionSelected_VariableAxleLoad (8U)
#  endif

#  ifndef LoadDistributionSelected_StartingHelp2
#   define LoadDistributionSelected_StartingHelp2 (9U)
#  endif

#  ifndef LoadDistributionSelected_TractionHelp2
#   define LoadDistributionSelected_TractionHelp2 (10U)
#  endif

#  ifndef LoadDistributionSelected_Reserved_03
#   define LoadDistributionSelected_Reserved_03 (11U)
#  endif

#  ifndef LoadDistributionSelected_Reserved_04
#   define LoadDistributionSelected_Reserved_04 (12U)
#  endif

#  ifndef LoadDistributionSelected_Reserved_05
#   define LoadDistributionSelected_Reserved_05 (13U)
#  endif

#  ifndef LoadDistributionSelected_Error
#   define LoadDistributionSelected_Error (14U)
#  endif

#  ifndef LoadDistributionSelected_NotAvailable
#   define LoadDistributionSelected_NotAvailable (15U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_AXLELOADDISTRIBUTION_HMICTRL_TYPE_H */
