/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DriverAuthentication2_Ctrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <DriverAuthentication2_Ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DRIVERAUTHENTICATION2_CTRL_TYPE_H
# define _RTE_DRIVERAUTHENTICATION2_CTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DeviceAuthentication_rqst_Idle
#   define DeviceAuthentication_rqst_Idle (0U)
#  endif

#  ifndef DeviceAuthentication_rqst_DeviceAuthenticationRequest
#   define DeviceAuthentication_rqst_DeviceAuthenticationRequest (1U)
#  endif

#  ifndef DeviceAuthentication_rqst_DeviceDeauthenticationRequest
#   define DeviceAuthentication_rqst_DeviceDeauthenticationRequest (2U)
#  endif

#  ifndef DeviceAuthentication_rqst_DeviceMatching
#   define DeviceAuthentication_rqst_DeviceMatching (3U)
#  endif

#  ifndef DeviceAuthentication_rqst_Spare1
#   define DeviceAuthentication_rqst_Spare1 (4U)
#  endif

#  ifndef DeviceAuthentication_rqst_Spare2
#   define DeviceAuthentication_rqst_Spare2 (5U)
#  endif

#  ifndef DeviceAuthentication_rqst_Error
#   define DeviceAuthentication_rqst_Error (6U)
#  endif

#  ifndef DeviceAuthentication_rqst_NotAvailable
#   define DeviceAuthentication_rqst_NotAvailable (7U)
#  endif

#  ifndef DeviceInCab_stat_Idle
#   define DeviceInCab_stat_Idle (0U)
#  endif

#  ifndef DeviceInCab_stat_DeviceNotAuthenticated
#   define DeviceInCab_stat_DeviceNotAuthenticated (1U)
#  endif

#  ifndef DeviceInCab_stat_DeviceAuthenticated
#   define DeviceInCab_stat_DeviceAuthenticated (2U)
#  endif

#  ifndef DeviceInCab_stat_NotAvailable
#   define DeviceInCab_stat_NotAvailable (3U)
#  endif

#  ifndef DoorsAjar_stat_Idle
#   define DoorsAjar_stat_Idle (0U)
#  endif

#  ifndef DoorsAjar_stat_BothDoorsAreClosed
#   define DoorsAjar_stat_BothDoorsAreClosed (1U)
#  endif

#  ifndef DoorsAjar_stat_DriverDoorIsOpen
#   define DoorsAjar_stat_DriverDoorIsOpen (2U)
#  endif

#  ifndef DoorsAjar_stat_PassengerDoorIsOpen
#   define DoorsAjar_stat_PassengerDoorIsOpen (3U)
#  endif

#  ifndef DoorsAjar_stat_BothDoorsAreOpen
#   define DoorsAjar_stat_BothDoorsAreOpen (4U)
#  endif

#  ifndef DoorsAjar_stat_Spare
#   define DoorsAjar_stat_Spare (5U)
#  endif

#  ifndef DoorsAjar_stat_Error
#   define DoorsAjar_stat_Error (6U)
#  endif

#  ifndef DoorsAjar_stat_NotAvailable
#   define DoorsAjar_stat_NotAvailable (7U)
#  endif

#  ifndef EngineStartAuth_rqst_StartingOfTheTruckNotRequested
#   define EngineStartAuth_rqst_StartingOfTheTruckNotRequested (0U)
#  endif

#  ifndef EngineStartAuth_rqst_StartingOfTheTruckRequested
#   define EngineStartAuth_rqst_StartingOfTheTruckRequested (1U)
#  endif

#  ifndef EngineStartAuth_rqst_Spare
#   define EngineStartAuth_rqst_Spare (2U)
#  endif

#  ifndef EngineStartAuth_rqst_Spare_01
#   define EngineStartAuth_rqst_Spare_01 (3U)
#  endif

#  ifndef EngineStartAuth_rqst_Spare_02
#   define EngineStartAuth_rqst_Spare_02 (4U)
#  endif

#  ifndef EngineStartAuth_rqst_Spare_03
#   define EngineStartAuth_rqst_Spare_03 (5U)
#  endif

#  ifndef EngineStartAuth_rqst_Error
#   define EngineStartAuth_rqst_Error (6U)
#  endif

#  ifndef EngineStartAuth_rqst_NotAvailable
#   define EngineStartAuth_rqst_NotAvailable (7U)
#  endif

#  ifndef EngineStartAuth_stat_decrypt_Idle
#   define EngineStartAuth_stat_decrypt_Idle (0U)
#  endif

#  ifndef EngineStartAuth_stat_decrypt_CrankingIsAuthorized
#   define EngineStartAuth_stat_decrypt_CrankingIsAuthorized (1U)
#  endif

#  ifndef EngineStartAuth_stat_decrypt_CrankingIsProhibited
#   define EngineStartAuth_stat_decrypt_CrankingIsProhibited (2U)
#  endif

#  ifndef EngineStartAuth_stat_decrypt_Spare1
#   define EngineStartAuth_stat_decrypt_Spare1 (3U)
#  endif

#  ifndef EngineStartAuth_stat_decrypt_Spare2
#   define EngineStartAuth_stat_decrypt_Spare2 (4U)
#  endif

#  ifndef EngineStartAuth_stat_decrypt_Spare3
#   define EngineStartAuth_stat_decrypt_Spare3 (5U)
#  endif

#  ifndef EngineStartAuth_stat_decrypt_Error
#   define EngineStartAuth_stat_decrypt_Error (6U)
#  endif

#  ifndef EngineStartAuth_stat_decrypt_NotAvailable
#   define EngineStartAuth_stat_decrypt_NotAvailable (7U)
#  endif

#  ifndef GearBoxUnlockAuth_rqst_GearEngagementNotRequested
#   define GearBoxUnlockAuth_rqst_GearEngagementNotRequested (0U)
#  endif

#  ifndef GearBoxUnlockAuth_rqst_GearEngagementRequested
#   define GearBoxUnlockAuth_rqst_GearEngagementRequested (1U)
#  endif

#  ifndef GearBoxUnlockAuth_rqst_Error
#   define GearBoxUnlockAuth_rqst_Error (2U)
#  endif

#  ifndef GearBoxUnlockAuth_rqst_NotAvaiable
#   define GearBoxUnlockAuth_rqst_NotAvaiable (3U)
#  endif

#  ifndef GearboxUnlockAuth_stat_decrypt_Idle
#   define GearboxUnlockAuth_stat_decrypt_Idle (0U)
#  endif

#  ifndef GearboxUnlockAuth_stat_decrypt_GearEngagementAllowed
#   define GearboxUnlockAuth_stat_decrypt_GearEngagementAllowed (1U)
#  endif

#  ifndef GearboxUnlockAuth_stat_decrypt_GearEngagementRefused
#   define GearboxUnlockAuth_stat_decrypt_GearEngagementRefused (2U)
#  endif

#  ifndef GearboxUnlockAuth_stat_decrypt_Spare1
#   define GearboxUnlockAuth_stat_decrypt_Spare1 (3U)
#  endif

#  ifndef GearboxUnlockAuth_stat_decrypt_Spare2
#   define GearboxUnlockAuth_stat_decrypt_Spare2 (4U)
#  endif

#  ifndef GearboxUnlockAuth_stat_decrypt_Spare3
#   define GearboxUnlockAuth_stat_decrypt_Spare3 (5U)
#  endif

#  ifndef GearboxUnlockAuth_stat_decrypt_Error
#   define GearboxUnlockAuth_stat_decrypt_Error (6U)
#  endif

#  ifndef GearboxUnlockAuth_stat_decrypt_NotAvailable
#   define GearboxUnlockAuth_stat_decrypt_NotAvailable (7U)
#  endif

#  ifndef KeyAuthentication_rqst_KeyNotPresent
#   define KeyAuthentication_rqst_KeyNotPresent (0U)
#  endif

#  ifndef KeyAuthentication_rqst_KeyIsInserted
#   define KeyAuthentication_rqst_KeyIsInserted (1U)
#  endif

#  ifndef KeyAuthentication_rqst_RequestAuthentication
#   define KeyAuthentication_rqst_RequestAuthentication (2U)
#  endif

#  ifndef KeyAuthentication_rqst_Spare
#   define KeyAuthentication_rqst_Spare (3U)
#  endif

#  ifndef KeyAuthentication_rqst_Spare01
#   define KeyAuthentication_rqst_Spare01 (4U)
#  endif

#  ifndef KeyAuthentication_rqst_Spare02
#   define KeyAuthentication_rqst_Spare02 (5U)
#  endif

#  ifndef KeyAuthentication_rqst_Error
#   define KeyAuthentication_rqst_Error (6U)
#  endif

#  ifndef KeyAuthentication_rqst_NotAvailable
#   define KeyAuthentication_rqst_NotAvailable (7U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_KeyNotAuthenticated
#   define KeyAuthentication_stat_decrypt_KeyNotAuthenticated (0U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_KeyAuthenticated
#   define KeyAuthentication_stat_decrypt_KeyAuthenticated (1U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Spare1
#   define KeyAuthentication_stat_decrypt_Spare1 (2U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Spare2
#   define KeyAuthentication_stat_decrypt_Spare2 (3U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Spare3
#   define KeyAuthentication_stat_decrypt_Spare3 (4U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Spare4
#   define KeyAuthentication_stat_decrypt_Spare4 (5U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_Error
#   define KeyAuthentication_stat_decrypt_Error (6U)
#  endif

#  ifndef KeyAuthentication_stat_decrypt_NotAvailable
#   define KeyAuthentication_stat_decrypt_NotAvailable (7U)
#  endif

#  ifndef KeyNotValid_Idle
#   define KeyNotValid_Idle (0U)
#  endif

#  ifndef KeyNotValid_KeyNotValid
#   define KeyNotValid_KeyNotValid (1U)
#  endif

#  ifndef KeyNotValid_Error
#   define KeyNotValid_Error (6U)
#  endif

#  ifndef KeyNotValid_NotAvailable
#   define KeyNotValid_NotAvailable (7U)
#  endif

#  ifndef KeyfobAuth_rqst_Idle
#   define KeyfobAuth_rqst_Idle (0U)
#  endif

#  ifndef KeyfobAuth_rqst_RequestByPassiveMechanism
#   define KeyfobAuth_rqst_RequestByPassiveMechanism (1U)
#  endif

#  ifndef KeyfobAuth_rqst_RequestByImmobilizerMechanism
#   define KeyfobAuth_rqst_RequestByImmobilizerMechanism (2U)
#  endif

#  ifndef KeyfobAuth_rqst_Spare1
#   define KeyfobAuth_rqst_Spare1 (3U)
#  endif

#  ifndef KeyfobAuth_rqst_Spare2
#   define KeyfobAuth_rqst_Spare2 (4U)
#  endif

#  ifndef KeyfobAuth_rqst_Spare3
#   define KeyfobAuth_rqst_Spare3 (5U)
#  endif

#  ifndef KeyfobAuth_rqst_Error
#   define KeyfobAuth_rqst_Error (6U)
#  endif

#  ifndef KeyfobAuth_rqst_NotAavailable
#   define KeyfobAuth_rqst_NotAavailable (7U)
#  endif

#  ifndef KeyfobAuth_stat_Idle
#   define KeyfobAuth_stat_Idle (0U)
#  endif

#  ifndef KeyfobAuth_stat_NokeyfobAuthenticated
#   define KeyfobAuth_stat_NokeyfobAuthenticated (1U)
#  endif

#  ifndef KeyfobAuth_stat_KeyfobAuthenticated
#   define KeyfobAuth_stat_KeyfobAuthenticated (2U)
#  endif

#  ifndef KeyfobAuth_stat_Spare1
#   define KeyfobAuth_stat_Spare1 (3U)
#  endif

#  ifndef KeyfobAuth_stat_Spare2
#   define KeyfobAuth_stat_Spare2 (4U)
#  endif

#  ifndef KeyfobAuth_stat_Spare3
#   define KeyfobAuth_stat_Spare3 (5U)
#  endif

#  ifndef KeyfobAuth_stat_Error
#   define KeyfobAuth_stat_Error (6U)
#  endif

#  ifndef KeyfobAuth_stat_NotAvailable
#   define KeyfobAuth_stat_NotAvailable (7U)
#  endif

#  ifndef PinCode_rqst_Idle
#   define PinCode_rqst_Idle (0U)
#  endif

#  ifndef PinCode_rqst_PINCodeNotNeeded
#   define PinCode_rqst_PINCodeNotNeeded (1U)
#  endif

#  ifndef PinCode_rqst_StatusOfThePinCodeRequested
#   define PinCode_rqst_StatusOfThePinCodeRequested (2U)
#  endif

#  ifndef PinCode_rqst_PinCodeNeeded
#   define PinCode_rqst_PinCodeNeeded (3U)
#  endif

#  ifndef PinCode_rqst_ResetPinCodeStatus
#   define PinCode_rqst_ResetPinCodeStatus (4U)
#  endif

#  ifndef PinCode_rqst_Spare
#   define PinCode_rqst_Spare (5U)
#  endif

#  ifndef PinCode_rqst_Error
#   define PinCode_rqst_Error (6U)
#  endif

#  ifndef PinCode_rqst_NotAvailable
#   define PinCode_rqst_NotAvailable (7U)
#  endif

#  ifndef PinCode_stat_Idle
#   define PinCode_stat_Idle (0U)
#  endif

#  ifndef PinCode_stat_NoPinCodeEntered
#   define PinCode_stat_NoPinCodeEntered (1U)
#  endif

#  ifndef PinCode_stat_WrongPinCode
#   define PinCode_stat_WrongPinCode (2U)
#  endif

#  ifndef PinCode_stat_GoodPinCode
#   define PinCode_stat_GoodPinCode (3U)
#  endif

#  ifndef PinCode_stat_Error
#   define PinCode_stat_Error (6U)
#  endif

#  ifndef PinCode_stat_NotAvailable
#   define PinCode_stat_NotAvailable (7U)
#  endif

#  ifndef SEWS_P1VKG_APM_Check_Active_T_No
#   define SEWS_P1VKG_APM_Check_Active_T_No (0U)
#  endif

#  ifndef SEWS_P1VKG_APM_Check_Active_T_Yes
#   define SEWS_P1VKG_APM_Check_Active_T_Yes (1U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

#  ifndef VehicleMode_Hibernate
#   define VehicleMode_Hibernate (0U)
#  endif

#  ifndef VehicleMode_Parked
#   define VehicleMode_Parked (1U)
#  endif

#  ifndef VehicleMode_Living
#   define VehicleMode_Living (2U)
#  endif

#  ifndef VehicleMode_Accessory
#   define VehicleMode_Accessory (3U)
#  endif

#  ifndef VehicleMode_PreRunning
#   define VehicleMode_PreRunning (4U)
#  endif

#  ifndef VehicleMode_Cranking
#   define VehicleMode_Cranking (5U)
#  endif

#  ifndef VehicleMode_Running
#   define VehicleMode_Running (6U)
#  endif

#  ifndef VehicleMode_Spare_1
#   define VehicleMode_Spare_1 (7U)
#  endif

#  ifndef VehicleMode_Spare_2
#   define VehicleMode_Spare_2 (8U)
#  endif

#  ifndef VehicleMode_Spare_3
#   define VehicleMode_Spare_3 (9U)
#  endif

#  ifndef VehicleMode_Spare_4
#   define VehicleMode_Spare_4 (10U)
#  endif

#  ifndef VehicleMode_Spare_5
#   define VehicleMode_Spare_5 (11U)
#  endif

#  ifndef VehicleMode_Spare_6
#   define VehicleMode_Spare_6 (12U)
#  endif

#  ifndef VehicleMode_Spare_7
#   define VehicleMode_Spare_7 (13U)
#  endif

#  ifndef VehicleMode_Error
#   define VehicleMode_Error (14U)
#  endif

#  ifndef VehicleMode_NotAvailable
#   define VehicleMode_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DRIVERAUTHENTICATION2_CTRL_TYPE_H */
