/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VehicleStabilityControl_UICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <VehicleStabilityControl_UICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VEHICLESTABILITYCONTROL_UICTRL_TYPE_H
# define _RTE_VEHICLESTABILITYCONTROL_UICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DeactivateActivate_Deactivate
#   define DeactivateActivate_Deactivate (0U)
#  endif

#  ifndef DeactivateActivate_Activate
#   define DeactivateActivate_Activate (1U)
#  endif

#  ifndef DeactivateActivate_Error
#   define DeactivateActivate_Error (2U)
#  endif

#  ifndef DeactivateActivate_NotAvailable
#   define DeactivateActivate_NotAvailable (3U)
#  endif

#  ifndef ESCDriverReq_NoRequest
#   define ESCDriverReq_NoRequest (0U)
#  endif

#  ifndef ESCDriverReq_ESCOff
#   define ESCDriverReq_ESCOff (1U)
#  endif

#  ifndef ESCDriverReq_ESCReduced
#   define ESCDriverReq_ESCReduced (2U)
#  endif

#  ifndef ESCDriverReq_ESCOn
#   define ESCDriverReq_ESCOn (3U)
#  endif

#  ifndef ESCDriverReq_Spare01
#   define ESCDriverReq_Spare01 (4U)
#  endif

#  ifndef ESCDriverReq_Spare02
#   define ESCDriverReq_Spare02 (5U)
#  endif

#  ifndef ESCDriverReq_Error
#   define ESCDriverReq_Error (6U)
#  endif

#  ifndef ESCDriverReq_NotAvailable
#   define ESCDriverReq_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VEHICLESTABILITYCONTROL_UICTRL_TYPE_H */
