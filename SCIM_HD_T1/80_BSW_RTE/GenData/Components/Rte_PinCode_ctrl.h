/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_PinCode_ctrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <PinCode_ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_PINCODE_CTRL_H
# define _RTE_PINCODE_CTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_PinCode_ctrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PinCode_stat_T, RTE_VAR_NOINIT) Rte_PinCode_ctrl_PinCode_stat_PinCode_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PinCode_rqst_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_PinCode_rqst_PinCode_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_DynamicCode_rqst_DynamicCode_rqst (7U)
#  define Rte_InitValue_DynamicCode_value_DynamicCode_value (4294967295U)
#  define Rte_InitValue_PinCodeEntered_value_PinCodeEntered_value (0U)
#  define Rte_InitValue_PinCode_rqst_PinCode_rqst (7U)
#  define Rte_InitValue_PinCode_stat_PinCode_stat (7U)
#  define Rte_InitValue_PinCode_validity_time_PinCode_validity_time (255U)
#  define Rte_InitValue_SwcActivation_Security_SwcActivation_Security (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PinCode_ctrl_DynamicCode_rqst_DynamicCode_rqst(P2VAR(DynamicCode_rqst_T, AUTOMATIC, RTE_PINCODE_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PinCode_ctrl_PinCodeEntered_value_PinCodeEntered_value(P2VAR(Code32bit_T, AUTOMATIC, RTE_PINCODE_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PinCode_ctrl_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I(P2VAR(uint8, AUTOMATIC, RTE_PINCODE_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_PinCode_ctrl_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I(P2VAR(StandardNVM_T, AUTOMATIC, RTE_PINCODE_CTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_PinCode_ctrl_DynamicCode_value_DynamicCode_value(Code32bit_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_PinCode_ctrl_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I(P2CONST(uint8, AUTOMATIC, RTE_PINCODE_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_PinCode_ctrl_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I(P2CONST(StandardNVM_T, AUTOMATIC, RTE_PINCODE_CTRL_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_PinCode_ctrl_PinCode_validity_time_PinCode_validity_time(PinCode_validity_time_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_DynamicCode_rqst_DynamicCode_rqst Rte_Read_PinCode_ctrl_DynamicCode_rqst_DynamicCode_rqst
#  define Rte_Read_PinCodeEntered_value_PinCodeEntered_value Rte_Read_PinCode_ctrl_PinCodeEntered_value_PinCodeEntered_value
#  define Rte_Read_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I Rte_Read_PinCode_ctrl_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I
#  define Rte_Read_PinCode_rqst_PinCode_rqst Rte_Read_PinCode_ctrl_PinCode_rqst_PinCode_rqst
#  define Rte_Read_PinCode_ctrl_PinCode_rqst_PinCode_rqst(data) (*(data) = Rte_DriverAuthentication2_Ctrl_PinCode_rqst_PinCode_rqst, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Security_SwcActivation_Security Rte_Read_PinCode_ctrl_SwcActivation_Security_SwcActivation_Security
#  define Rte_Read_PinCode_ctrl_SwcActivation_Security_SwcActivation_Security(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DynamicCode_value_DynamicCode_value Rte_Write_PinCode_ctrl_DynamicCode_value_DynamicCode_value
#  define Rte_Write_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I Rte_Write_PinCode_ctrl_PinCode_ctrl_NVM_I_PinCode_ctrl_NVM_I
#  define Rte_Write_PinCode_stat_PinCode_stat Rte_Write_PinCode_ctrl_PinCode_stat_PinCode_stat
#  define Rte_Write_PinCode_ctrl_PinCode_stat_PinCode_stat(data) (Rte_PinCode_ctrl_PinCode_stat_PinCode_stat = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PinCode_validity_time_PinCode_validity_time Rte_Write_PinCode_ctrl_PinCode_validity_time_PinCode_validity_time


# endif /* !defined(RTE_CORE) */


# define PinCode_ctrl_START_SEC_CODE
# include "PinCode_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_PinCode_ctrl_20ms_runnable PinCode_ctrl_20ms_runnable
# endif

FUNC(void, PinCode_ctrl_CODE) PinCode_ctrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define PinCode_ctrl_STOP_SEC_CODE
# include "PinCode_ctrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_PINCODE_CTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
