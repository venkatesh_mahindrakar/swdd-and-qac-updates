/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_PassengersSeatBelt_hdlr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <PassengersSeatBelt_hdlr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_PASSENGERSSEATBELT_HDLR_H
# define _RTE_PASSENGERSSEATBELT_HDLR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_PassengersSeatBelt_hdlr_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_PassengersSeatBelt_PassengersSeatBelt (7U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_PassengersSeatBelt_hdlr_PassengersSeatBelt_PassengersSeatBelt(PassengersSeatBelt_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_PassengersSeatBelt_hdlr_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_PassengersSeatBelt_hdlr_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_PassengersSeatBelt_PassengersSeatBelt Rte_Write_PassengersSeatBelt_hdlr_PassengersSeatBelt_PassengersSeatBelt


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) AdiPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_AdiInterface_P_GetAdiPinState_CS AdiInterface_P_GetAdiPinState_CS
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1F0O_1E_ResistOutOfRange_SecondPsgrSeat_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)411, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1FZ9_1E_ResistOutOfRange_FirstPsgrSeat_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)412, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PassengersSeatBeltVoltageLevels_X1CY2_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY2_PassengersSeatBeltVoltageLevels_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v() (&Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CY2_PassengersSeatBeltVoltageLevels_v() (&Rte_AddrPar_0x29_X1CY2_PassengersSeatBeltVoltageLevels_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_PassengersSeatBeltInstalled_P1VQB_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VQB_PassengersSeatBeltInstalled_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PassengersSeatBeltSensorType_P1VYK_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VYK_PassengersSeatBeltSensorType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1VQB_PassengersSeatBeltInstalled_v() (Rte_AddrPar_0x2B_P1VQB_PassengersSeatBeltInstalled_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1VYK_PassengersSeatBeltSensorType_v() (Rte_AddrPar_0x2B_P1VYK_PassengersSeatBeltSensorType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define PassengersSeatBelt_hdlr_START_SEC_CODE
# include "PassengersSeatBelt_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_PassengersSeatBelt_hdlr_20ms_runnable PassengersSeatBelt_hdlr_20ms_runnable
# endif

FUNC(void, PassengersSeatBelt_hdlr_CODE) PassengersSeatBelt_hdlr_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define PassengersSeatBelt_hdlr_STOP_SEC_CODE
# include "PassengersSeatBelt_hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_AdiInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_PASSENGERSSEATBELT_HDLR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
