/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_EconomyPower_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <EconomyPower_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_ECONOMYPOWER_HMICTRL_TYPE_H
# define _RTE_ECONOMYPOWER_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef OffOn_Off
#   define OffOn_Off (0U)
#  endif

#  ifndef OffOn_On
#   define OffOn_On (1U)
#  endif

#  ifndef OffOn_Error
#   define OffOn_Error (2U)
#  endif

#  ifndef OffOn_NotAvailable
#   define OffOn_NotAvailable (3U)
#  endif

#  define InvalidValue_PushButtonStatus_T (3U)

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef TransmissionDrivingMode_EconomyExtra
#   define TransmissionDrivingMode_EconomyExtra (0U)
#  endif

#  ifndef TransmissionDrivingMode_Balanced
#   define TransmissionDrivingMode_Balanced (1U)
#  endif

#  ifndef TransmissionDrivingMode_Performance
#   define TransmissionDrivingMode_Performance (2U)
#  endif

#  ifndef TransmissionDrivingMode_OffRoad
#   define TransmissionDrivingMode_OffRoad (3U)
#  endif

#  ifndef TransmissionDrivingMode_HeavyDuty
#   define TransmissionDrivingMode_HeavyDuty (4U)
#  endif

#  ifndef TransmissionDrivingMode_Dynamometer
#   define TransmissionDrivingMode_Dynamometer (5U)
#  endif

#  ifndef TransmissionDrivingMode_KickDown
#   define TransmissionDrivingMode_KickDown (6U)
#  endif

#  ifndef TransmissionDrivingMode_NotAvailable
#   define TransmissionDrivingMode_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_ECONOMYPOWER_HMICTRL_TYPE_H */
