/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Cdd_LinTpHandling.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <Cdd_LinTpHandling>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_CDD_LINTPHANDLING_H
# define _RTE_CDD_LINTPHANDLING_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Cdd_LinTpHandling_Type.h"
# include "Rte_DataHandleType.h"


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(uint8, RTE_CODE) Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(uint8, RTE_CODE) Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Mode_<p>_<m>
 *********************************************************************************************************************/
#  define Rte_Mode_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule
#  define Rte_Mode_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule
#  define Rte_Mode_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule
#  define Rte_Mode_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule
#  define Rte_Mode_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule Rte_Mode_Cdd_LinTpHandling_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_CDD_LINDIAGNOSTICS_APPL_CODE) CddLinRxHandling_ReceiveIndication(uint8 TxId, uint8 SubServiceId, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) RxData, uint8 Length, CddLinTp_Status Status); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CddLinRxHandling_ReceiveIndication(arg1, arg2, arg3, arg4, arg5) (CddLinRxHandling_ReceiveIndication(arg1, arg2, arg3, arg4, arg5), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define Cdd_LinTpHandling_START_SEC_CODE
# include "Cdd_LinTpHandling_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_Cdd_LinTpClearRequest Cdd_LinTpClearRequest
#  define RTE_RUNNABLE_Cdd_LinTpLin1EndofNotification Cdd_LinTpLin1EndofNotification
#  define RTE_RUNNABLE_Cdd_LinTpLin2EndofNotification Cdd_LinTpLin2EndofNotification
#  define RTE_RUNNABLE_Cdd_LinTpLin3EndofNotification Cdd_LinTpLin3EndofNotification
#  define RTE_RUNNABLE_Cdd_LinTpLin4EndofNotification Cdd_LinTpLin4EndofNotification
#  define RTE_RUNNABLE_Cdd_LinTpLin5EndofNotification Cdd_LinTpLin5EndofNotification
#  define RTE_RUNNABLE_Cdd_LinTpMainfunction Cdd_LinTpMainfunction
#  define RTE_RUNNABLE_Cdd_LinTpTransmit Cdd_LinTpTransmit
# endif

FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpClearRequest(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin1EndofNotification(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin2EndofNotification(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin3EndofNotification(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin4EndofNotification(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpLin5EndofNotification(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpMainfunction(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, Cdd_LinTpHandling_CODE) Cdd_LinTpTransmit(uint8 TxId, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINTPHANDLING_APPL_VAR) TxData, uint8 Length); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define Cdd_LinTpHandling_STOP_SEC_CODE
# include "Cdd_LinTpHandling_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_CDD_LINTPHANDLING_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
