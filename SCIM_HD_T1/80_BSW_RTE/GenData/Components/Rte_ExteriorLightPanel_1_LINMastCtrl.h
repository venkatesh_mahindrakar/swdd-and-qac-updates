/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_ExteriorLightPanel_1_LINMastCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <ExteriorLightPanel_1_LINMastCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_H
# define _RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_ExteriorLightPanel_1_LINMastCtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BackLightDimming_Status_Thumbwheel_stat (31U)
#  define Rte_InitValue_BlackPanelMode_ButtonStatus_PushButtonStatus (3U)
#  define Rte_InitValue_ComMode_LIN4_ComMode_LIN (7U)
#  define Rte_InitValue_DaytimeRunningLight_Indication_DeviceIndication (3U)
#  define Rte_InitValue_DiagActiveState_isDiagActive (0U)
#  define Rte_InitValue_DiagInfoELCP1_DiagInfo (0U)
#  define Rte_InitValue_DrivingLightPlus_Indication_DeviceIndication (3U)
#  define Rte_InitValue_DrivingLight_Indication_DeviceIndication (3U)
#  define Rte_InitValue_FogLightFront_ButtonStatus_1_PushButtonStatus (3U)
#  define Rte_InitValue_FogLightRear_ButtonStatus_1_PushButtonStatus (3U)
#  define Rte_InitValue_FrontFog_Indication_DeviceIndication (3U)
#  define Rte_InitValue_LIN_BackLightDimming_Status_Thumbwheel_stat (31U)
#  define Rte_InitValue_LIN_BlackPanelMode_ButtonStat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_DaytimeRunningLight_Indica_DeviceIndication (0U)
#  define Rte_InitValue_LIN_DrivingLightPlus_Indicatio_DeviceIndication (0U)
#  define Rte_InitValue_LIN_DrivingLight_Indication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_FogLightFront_ButtonStat_1_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_FogLightRear_ButtonStat_1_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_FrontFog_Indication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_LevelingThumbwheel_stat_Thumbwheel_stat (31U)
#  define Rte_InitValue_LIN_LightMode_Status_1_FreeWheel_Status (15U)
#  define Rte_InitValue_LIN_ParkingLight_Indication_DeviceIndication (0U)
#  define Rte_InitValue_LIN_RearFog_Indication_DeviceIndication (0U)
#  define Rte_InitValue_LevelingThumbwheel_stat_Thumbwheel_stat (31U)
#  define Rte_InitValue_LightMode_Status_1_FreeWheel_Status (15U)
#  define Rte_InitValue_ParkingLight_Indication_DeviceIndication (3U)
#  define Rte_InitValue_RearFog_Indication_DeviceIndication (3U)
#  define Rte_InitValue_ResponseErrorELCP1_ResponseErrorELCP1 (FALSE)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_ExteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ELCP1LinCtrl; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DaytimeRunningLight_Indication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DrivingLightPlus_Indication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DrivingLight_Indication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanel_1_LINMastCtrl_FrontFog_Indication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_LightMode_Status_1_FreeWheel_Status(P2VAR(FreeWheel_Status_T, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanel_1_LINMastCtrl_ParkingLight_Indication_DeviceIndication(P2VAR(DeviceIndication_T, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_ExteriorLightPanel_1_LINMastCtrl_ResponseErrorELCP1_ResponseErrorELCP1(P2VAR(ResponseErrorELCP1_T, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_BackLightDimming_Status_Thumbwheel_stat(Thumbwheel_stat_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_BlackPanelMode_ButtonStatus_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_FogLightFront_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_FogLightRear_ButtonStatus_1_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_DaytimeRunningLight_Indica_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_DrivingLightPlus_Indicatio_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_DrivingLight_Indication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_FrontFog_Indication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_ParkingLight_Indication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_RearFog_Indication_DeviceIndication(DeviceIndication_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LevelingThumbwheel_stat_Thumbwheel_stat(Thumbwheel_stat_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LightMode_Status_1_FreeWheel_Status(FreeWheel_Status_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ComMode_LIN4_ComMode_LIN Rte_Read_ExteriorLightPanel_1_LINMastCtrl_ComMode_LIN4_ComMode_LIN
#  define Rte_Read_ExteriorLightPanel_1_LINMastCtrl_ComMode_LIN4_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN4_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DaytimeRunningLight_Indication_DeviceIndication Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DaytimeRunningLight_Indication_DeviceIndication
#  define Rte_Read_DiagActiveState_isDiagActive Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DiagActiveState_isDiagActive
#  define Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DiagActiveState_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagInfoELCP1_DiagInfo Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DiagInfoELCP1_DiagInfo
#  define Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DiagInfoELCP1_DiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_DiagInfoELCP1_oELCP1toCIOM_L4_oLIN03_e88000c8_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DrivingLightPlus_Indication_DeviceIndication Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DrivingLightPlus_Indication_DeviceIndication
#  define Rte_Read_DrivingLight_Indication_DeviceIndication Rte_Read_ExteriorLightPanel_1_LINMastCtrl_DrivingLight_Indication_DeviceIndication
#  define Rte_Read_FrontFog_Indication_DeviceIndication Rte_Read_ExteriorLightPanel_1_LINMastCtrl_FrontFog_Indication_DeviceIndication
#  define Rte_Read_LIN_BackLightDimming_Status_Thumbwheel_stat Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_BackLightDimming_Status_Thumbwheel_stat
#  define Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_BackLightDimming_Status_Thumbwheel_stat(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BackLightDimming_Status_oELCP1toCIOM_L4_oLIN03_ff25f41e_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_BlackPanelMode_ButtonStat_PushButtonStatus Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_BlackPanelMode_ButtonStat_PushButtonStatus
#  define Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_BlackPanelMode_ButtonStat_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_BlackPanelMode_ButtonStat_oELCP1toCIOM_L4_oLIN03_dece2221_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_FogLightFront_ButtonStat_1_PushButtonStatus Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_FogLightFront_ButtonStat_1_PushButtonStatus
#  define Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_FogLightFront_ButtonStat_1_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_FogLightFront_ButtonStat_1_oELCP1toCIOM_L4_oLIN03_e1a5bfc2_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_FogLightRear_ButtonStat_1_PushButtonStatus Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_FogLightRear_ButtonStat_1_PushButtonStatus
#  define Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_FogLightRear_ButtonStat_1_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_FogLightRear_ButtonStat_1_oELCP1toCIOM_L4_oLIN03_14baadea_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_LevelingThumbwheel_stat_Thumbwheel_stat Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_LevelingThumbwheel_stat_Thumbwheel_stat
#  define Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_LevelingThumbwheel_stat_Thumbwheel_stat(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_LevelingThumbwheel_stat_oELCP1toCIOM_L4_oLIN03_cb03d712_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_LightMode_Status_1_FreeWheel_Status Rte_Read_ExteriorLightPanel_1_LINMastCtrl_LIN_LightMode_Status_1_FreeWheel_Status
#  define Rte_Read_ParkingLight_Indication_DeviceIndication Rte_Read_ExteriorLightPanel_1_LINMastCtrl_ParkingLight_Indication_DeviceIndication
#  define Rte_Read_RearFog_Indication_DeviceIndication Rte_Read_ExteriorLightPanel_1_LINMastCtrl_RearFog_Indication_DeviceIndication
#  define Rte_Read_ExteriorLightPanel_1_LINMastCtrl_RearFog_Indication_DeviceIndication(data) (Com_ReceiveSignal(ComConf_ComSignal_RearFog_Indication_oVMCU_BB2_03P_oBackbone2_fa31dca4_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ResponseErrorELCP1_ResponseErrorELCP1 Rte_Read_ExteriorLightPanel_1_LINMastCtrl_ResponseErrorELCP1_ResponseErrorELCP1


/**********************************************************************************************************************
 * Rte_IsUpdated_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_IsUpdated_LIN_LightMode_Status_1_FreeWheel_Status Rte_IsUpdated_ExteriorLightPanel_1_LINMastCtrl_LIN_LightMode_Status_1_FreeWheel_Status
#  define Rte_IsUpdated_ExteriorLightPanel_1_LINMastCtrl_LIN_LightMode_Status_1_FreeWheel_Status() ((Rte_RxUpdateFlags.Rte_RxUpdate_ExteriorLightPanel_1_LINMastCtrl_LIN_LightMode_Status_1_FreeWheel_Status == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_BackLightDimming_Status_Thumbwheel_stat Rte_Write_ExteriorLightPanel_1_LINMastCtrl_BackLightDimming_Status_Thumbwheel_stat
#  define Rte_Write_BlackPanelMode_ButtonStatus_PushButtonStatus Rte_Write_ExteriorLightPanel_1_LINMastCtrl_BlackPanelMode_ButtonStatus_PushButtonStatus
#  define Rte_Write_FogLightFront_ButtonStatus_1_PushButtonStatus Rte_Write_ExteriorLightPanel_1_LINMastCtrl_FogLightFront_ButtonStatus_1_PushButtonStatus
#  define Rte_Write_FogLightRear_ButtonStatus_1_PushButtonStatus Rte_Write_ExteriorLightPanel_1_LINMastCtrl_FogLightRear_ButtonStatus_1_PushButtonStatus
#  define Rte_Write_LIN_DaytimeRunningLight_Indica_DeviceIndication Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_DaytimeRunningLight_Indica_DeviceIndication
#  define Rte_Write_LIN_DrivingLightPlus_Indicatio_DeviceIndication Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_DrivingLightPlus_Indicatio_DeviceIndication
#  define Rte_Write_LIN_DrivingLight_Indication_DeviceIndication Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_DrivingLight_Indication_DeviceIndication
#  define Rte_Write_LIN_FrontFog_Indication_DeviceIndication Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_FrontFog_Indication_DeviceIndication
#  define Rte_Write_LIN_ParkingLight_Indication_DeviceIndication Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_ParkingLight_Indication_DeviceIndication
#  define Rte_Write_LIN_RearFog_Indication_DeviceIndication Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LIN_RearFog_Indication_DeviceIndication
#  define Rte_Write_LevelingThumbwheel_stat_Thumbwheel_stat Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LevelingThumbwheel_stat_Thumbwheel_stat
#  define Rte_Write_LightMode_Status_1_FreeWheel_Status Rte_Write_ExteriorLightPanel_1_LINMastCtrl_LightMode_Status_1_FreeWheel_Status


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BKC_87_ELCPLink_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)204, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN9_16_ELCP__VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)228, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN9_17_ELCP_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)229, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN9_44_ELCP_RAM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)230, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN9_45_ELCP_FLASH_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)231, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN9_46_ELCP_EEPROM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)232, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN9_49_ELCP_HWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)233, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN9_94_ELCP_SWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)234, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_DimmingAdjustment1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)14)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_DimmingAdjustment1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)14)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_ExteriorLightsRequest1_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)21)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_ExteriorLightsRequest1_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)21)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData_Irv_IOCTL_ELCP1LinCtrl() \
  Rte_Irv_ExteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ELCP1LinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU_Irv_IOCTL_ELCP1LinCtrl(data) \
  (Rte_Irv_ExteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ELCP1LinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment_Irv_IOCTL_ELCP1LinCtrl(data) \
  (Rte_Irv_ExteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ELCP1LinCtrl = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl() \
  Rte_Irv_ExteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ELCP1LinCtrl
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable_Irv_IOCTL_ELCP1LinCtrl(data) \
  (Rte_Irv_ExteriorLightPanel_1_LINMastCtrl_Irv_IOCTL_ELCP1LinCtrl = (data))
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR3_ELCP1_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1VR3_ELCP1_Installed_v() (Rte_AddrPar_0x2B_P1VR3_ELCP1_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define ExteriorLightPanel_1_LINMastCtrl_START_SEC_CODE
# include "ExteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState
#  define RTE_RUNNABLE_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData
#  define RTE_RUNNABLE_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU
#  define RTE_RUNNABLE_DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment
#  define RTE_RUNNABLE_ExteriorLightPanel_1_LINMastCtrl_20ms_runnable ExteriorLightPanel_1_LINMastCtrl_20ms_runnable
# endif

FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_FreezeCurrentState(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData(P2VAR(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ReturnControlToECU(P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(uint8, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, ExteriorLightPanel_1_LINMastCtrl_CODE) DataServices_P1EOS_Data_P1EOS_ELCP1_LIN_CTRLSignals_ShortTermAdjustment(P2CONST(Dcm_Data1ByteType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, ExteriorLightPanel_1_LINMastCtrl_CODE) ExteriorLightPanel_1_LINMastCtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define ExteriorLightPanel_1_LINMastCtrl_STOP_SEC_CODE
# include "ExteriorLightPanel_1_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1EOS_Data_P1EOS_E_NOT_OK (1U)

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_EXTERIORLIGHTPANEL_1_LINMASTCTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
