/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_MirrorHeating_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <MirrorHeating_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_MIRRORHEATING_HMICTRL_TYPE_H
# define _RTE_MIRRORHEATING_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A2PosSwitchStatus_Off
#   define A2PosSwitchStatus_Off (0U)
#  endif

#  ifndef A2PosSwitchStatus_On
#   define A2PosSwitchStatus_On (1U)
#  endif

#  ifndef A2PosSwitchStatus_Error
#   define A2PosSwitchStatus_Error (2U)
#  endif

#  ifndef A2PosSwitchStatus_NotAvailable
#   define A2PosSwitchStatus_NotAvailable (3U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef MirrorHeat_OFF
#   define MirrorHeat_OFF (0U)
#  endif

#  ifndef MirrorHeat_ONNormalMode
#   define MirrorHeat_ONNormalMode (1U)
#  endif

#  ifndef MirrorHeat_ONExtendedMode
#   define MirrorHeat_ONExtendedMode (2U)
#  endif

#  ifndef MirrorHeat_SpareValue
#   define MirrorHeat_SpareValue (3U)
#  endif

#  ifndef MirrorHeat_SpareValue_01
#   define MirrorHeat_SpareValue_01 (4U)
#  endif

#  ifndef MirrorHeat_SpareValue_02
#   define MirrorHeat_SpareValue_02 (5U)
#  endif

#  ifndef MirrorHeat_ErrorIndicator
#   define MirrorHeat_ErrorIndicator (6U)
#  endif

#  ifndef MirrorHeat_NotAvailable
#   define MirrorHeat_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_MIRRORHEATING_HMICTRL_TYPE_H */
