/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_InteriorLights_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <InteriorLights_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_INTERIORLIGHTS_HMICTRL_TYPE_H
# define _RTE_INTERIORLIGHTS_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A2PosSwitchStatus_Off
#   define A2PosSwitchStatus_Off (0U)
#  endif

#  ifndef A2PosSwitchStatus_On
#   define A2PosSwitchStatus_On (1U)
#  endif

#  ifndef A2PosSwitchStatus_Error
#   define A2PosSwitchStatus_Error (2U)
#  endif

#  ifndef A2PosSwitchStatus_NotAvailable
#   define A2PosSwitchStatus_NotAvailable (3U)
#  endif

#  ifndef A3PosSwitchStatus_Middle
#   define A3PosSwitchStatus_Middle (0U)
#  endif

#  ifndef A3PosSwitchStatus_Lower
#   define A3PosSwitchStatus_Lower (1U)
#  endif

#  ifndef A3PosSwitchStatus_Upper
#   define A3PosSwitchStatus_Upper (2U)
#  endif

#  ifndef A3PosSwitchStatus_Spare
#   define A3PosSwitchStatus_Spare (3U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_01
#   define A3PosSwitchStatus_Spare_01 (4U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_02
#   define A3PosSwitchStatus_Spare_02 (5U)
#  endif

#  ifndef A3PosSwitchStatus_Error
#   define A3PosSwitchStatus_Error (6U)
#  endif

#  ifndef A3PosSwitchStatus_NotAvailable
#   define A3PosSwitchStatus_NotAvailable (7U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef FreeWheel_Status_NoMovement
#   define FreeWheel_Status_NoMovement (0U)
#  endif

#  ifndef FreeWheel_Status_1StepClockwise
#   define FreeWheel_Status_1StepClockwise (1U)
#  endif

#  ifndef FreeWheel_Status_2StepsClockwise
#   define FreeWheel_Status_2StepsClockwise (2U)
#  endif

#  ifndef FreeWheel_Status_3StepsClockwise
#   define FreeWheel_Status_3StepsClockwise (3U)
#  endif

#  ifndef FreeWheel_Status_4StepsClockwise
#   define FreeWheel_Status_4StepsClockwise (4U)
#  endif

#  ifndef FreeWheel_Status_5StepsClockwise
#   define FreeWheel_Status_5StepsClockwise (5U)
#  endif

#  ifndef FreeWheel_Status_6StepsClockwise
#   define FreeWheel_Status_6StepsClockwise (6U)
#  endif

#  ifndef FreeWheel_Status_1StepCounterClockwise
#   define FreeWheel_Status_1StepCounterClockwise (7U)
#  endif

#  ifndef FreeWheel_Status_2StepsCounterClockwise
#   define FreeWheel_Status_2StepsCounterClockwise (8U)
#  endif

#  ifndef FreeWheel_Status_3StepsCounterClockwise
#   define FreeWheel_Status_3StepsCounterClockwise (9U)
#  endif

#  ifndef FreeWheel_Status_4StepsCounterClockwise
#   define FreeWheel_Status_4StepsCounterClockwise (10U)
#  endif

#  ifndef FreeWheel_Status_5StepsCounterClockwise
#   define FreeWheel_Status_5StepsCounterClockwise (11U)
#  endif

#  ifndef FreeWheel_Status_6StepsCounterClockwise
#   define FreeWheel_Status_6StepsCounterClockwise (12U)
#  endif

#  ifndef FreeWheel_Status_Spare
#   define FreeWheel_Status_Spare (13U)
#  endif

#  ifndef FreeWheel_Status_Error
#   define FreeWheel_Status_Error (14U)
#  endif

#  ifndef FreeWheel_Status_NotAvailable
#   define FreeWheel_Status_NotAvailable (15U)
#  endif

#  ifndef IL_ModeReq_OFF
#   define IL_ModeReq_OFF (0U)
#  endif

#  ifndef IL_ModeReq_NightDriving
#   define IL_ModeReq_NightDriving (1U)
#  endif

#  ifndef IL_ModeReq_Resting
#   define IL_ModeReq_Resting (2U)
#  endif

#  ifndef IL_ModeReq_Max
#   define IL_ModeReq_Max (3U)
#  endif

#  ifndef IL_ModeReq_NonHMIModeResting
#   define IL_ModeReq_NonHMIModeResting (4U)
#  endif

#  ifndef IL_ModeReq_NonHMIModeMax
#   define IL_ModeReq_NonHMIModeMax (5U)
#  endif

#  ifndef IL_ModeReq_ErrorIndicator
#   define IL_ModeReq_ErrorIndicator (6U)
#  endif

#  ifndef IL_ModeReq_NotAvailable
#   define IL_ModeReq_NotAvailable (7U)
#  endif

#  ifndef IL_Mode_OFF
#   define IL_Mode_OFF (0U)
#  endif

#  ifndef IL_Mode_NightDriving
#   define IL_Mode_NightDriving (1U)
#  endif

#  ifndef IL_Mode_Resting
#   define IL_Mode_Resting (2U)
#  endif

#  ifndef IL_Mode_Max
#   define IL_Mode_Max (3U)
#  endif

#  ifndef IL_Mode_spare_1
#   define IL_Mode_spare_1 (4U)
#  endif

#  ifndef IL_Mode_spare_2
#   define IL_Mode_spare_2 (5U)
#  endif

#  ifndef IL_Mode_ErrorIndicator
#   define IL_Mode_ErrorIndicator (6U)
#  endif

#  ifndef IL_Mode_NotAvailable
#   define IL_Mode_NotAvailable (7U)
#  endif

#  ifndef InteriorLightDimming_rqst_NoMovement
#   define InteriorLightDimming_rqst_NoMovement (0U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position1
#   define InteriorLightDimming_rqst_Position1 (1U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position2
#   define InteriorLightDimming_rqst_Position2 (2U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position3
#   define InteriorLightDimming_rqst_Position3 (3U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position4
#   define InteriorLightDimming_rqst_Position4 (4U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position5
#   define InteriorLightDimming_rqst_Position5 (5U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position6
#   define InteriorLightDimming_rqst_Position6 (6U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position7
#   define InteriorLightDimming_rqst_Position7 (7U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position8
#   define InteriorLightDimming_rqst_Position8 (8U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position9
#   define InteriorLightDimming_rqst_Position9 (9U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position10
#   define InteriorLightDimming_rqst_Position10 (10U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position11
#   define InteriorLightDimming_rqst_Position11 (11U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position12
#   define InteriorLightDimming_rqst_Position12 (12U)
#  endif

#  ifndef InteriorLightDimming_rqst_Position13
#   define InteriorLightDimming_rqst_Position13 (13U)
#  endif

#  ifndef InteriorLightDimming_rqst_DimUpContinously
#   define InteriorLightDimming_rqst_DimUpContinously (14U)
#  endif

#  ifndef InteriorLightDimming_rqst_DimDownContinously
#   define InteriorLightDimming_rqst_DimDownContinously (15U)
#  endif

#  ifndef InteriorLightDimming_rqst_Error
#   define InteriorLightDimming_rqst_Error (30U)
#  endif

#  ifndef InteriorLightDimming_rqst_NotAvailable
#   define InteriorLightDimming_rqst_NotAvailable (31U)
#  endif

#  ifndef ISSM_STATE_INACTIVE
#   define ISSM_STATE_INACTIVE (0U)
#  endif

#  ifndef ISSM_STATE_PENDING
#   define ISSM_STATE_PENDING (1U)
#  endif

#  ifndef ISSM_STATE_ACTIVE
#   define ISSM_STATE_ACTIVE (2U)
#  endif

#  ifndef OffOn_Off
#   define OffOn_Off (0U)
#  endif

#  ifndef OffOn_On
#   define OffOn_On (1U)
#  endif

#  ifndef OffOn_Error
#   define OffOn_Error (2U)
#  endif

#  ifndef OffOn_NotAvailable
#   define OffOn_NotAvailable (3U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef EventFlag_Low
#   define EventFlag_Low (0U)
#  endif

#  ifndef EventFlag_High
#   define EventFlag_High (1U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_INTERIORLIGHTS_HMICTRL_TYPE_H */
