/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_FlexibleSwitchesRouter_Ctrl_Router_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <FlexibleSwitchesRouter_Ctrl_Router>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_TYPE_H
# define _RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A2PosSwitchStatus_Off
#   define A2PosSwitchStatus_Off (0U)
#  endif

#  ifndef A2PosSwitchStatus_On
#   define A2PosSwitchStatus_On (1U)
#  endif

#  ifndef A2PosSwitchStatus_Error
#   define A2PosSwitchStatus_Error (2U)
#  endif

#  ifndef A2PosSwitchStatus_NotAvailable
#   define A2PosSwitchStatus_NotAvailable (3U)
#  endif

#  ifndef A3PosSwitchStatus_Middle
#   define A3PosSwitchStatus_Middle (0U)
#  endif

#  ifndef A3PosSwitchStatus_Lower
#   define A3PosSwitchStatus_Lower (1U)
#  endif

#  ifndef A3PosSwitchStatus_Upper
#   define A3PosSwitchStatus_Upper (2U)
#  endif

#  ifndef A3PosSwitchStatus_Spare
#   define A3PosSwitchStatus_Spare (3U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_01
#   define A3PosSwitchStatus_Spare_01 (4U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_02
#   define A3PosSwitchStatus_Spare_02 (5U)
#  endif

#  ifndef A3PosSwitchStatus_Error
#   define A3PosSwitchStatus_Error (6U)
#  endif

#  ifndef A3PosSwitchStatus_NotAvailable
#   define A3PosSwitchStatus_NotAvailable (7U)
#  endif

#  ifndef Inactive
#   define Inactive (0U)
#  endif

#  ifndef Diagnostic
#   define Diagnostic (1U)
#  endif

#  ifndef SwitchDetection
#   define SwitchDetection (2U)
#  endif

#  ifndef ApplicationMonitoring
#   define ApplicationMonitoring (3U)
#  endif

#  ifndef Calibration
#   define Calibration (4U)
#  endif

#  ifndef Spare1
#   define Spare1 (5U)
#  endif

#  ifndef Error
#   define Error (6U)
#  endif

#  ifndef NotAvailable
#   define NotAvailable (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

#  ifndef DEM_INIT_MONITOR_CLEAR
#   define DEM_INIT_MONITOR_CLEAR (1U)
#  endif

#  ifndef DEM_INIT_MONITOR_RESTART
#   define DEM_INIT_MONITOR_RESTART (2U)
#  endif

#  ifndef DEM_INIT_MONITOR_REENABLED
#   define DEM_INIT_MONITOR_REENABLED (3U)
#  endif

#  ifndef DEM_INIT_MONITOR_STORAGE_REENABLED
#   define DEM_INIT_MONITOR_STORAGE_REENABLED (4U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerOff
#   define DualDeviceIndication_UpperOffLowerOff (0U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerOff
#   define DualDeviceIndication_UpperOnLowerOff (1U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerOff
#   define DualDeviceIndication_UpperBlinkLowerOff (2U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerOff
#   define DualDeviceIndication_UpperDontCareLowerOff (3U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerOn
#   define DualDeviceIndication_UpperOffLowerOn (4U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerOn
#   define DualDeviceIndication_UpperOnLowerOn (5U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerOn
#   define DualDeviceIndication_UpperBlinkLowerOn (6U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerOn
#   define DualDeviceIndication_UpperDontCareLowerOn (7U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerBlink
#   define DualDeviceIndication_UpperOffLowerBlink (8U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerBlink
#   define DualDeviceIndication_UpperOnLowerBlink (9U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerBlink
#   define DualDeviceIndication_UpperBlinkLowerBlink (10U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerBlink
#   define DualDeviceIndication_UpperDontCareLowerBlink (11U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerDontCare
#   define DualDeviceIndication_UpperOffLowerDontCare (12U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerDontCare
#   define DualDeviceIndication_UpperOnLowerDontCare (13U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerDontCare
#   define DualDeviceIndication_UpperBlinkLowerDontCare (14U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerDontCare
#   define DualDeviceIndication_UpperDontCareLowerDontCare (15U)
#  endif

#  ifndef IndicationCmd_OFFLEDNotActivated
#   define IndicationCmd_OFFLEDNotActivated (0U)
#  endif

#  ifndef IndicationCmd_ONLEDActivated
#   define IndicationCmd_ONLEDActivated (1U)
#  endif

#  ifndef NeutralPushed_Neutral
#   define NeutralPushed_Neutral (0U)
#  endif

#  ifndef NeutralPushed_Pushed
#   define NeutralPushed_Pushed (1U)
#  endif

#  ifndef NeutralPushed_Error
#   define NeutralPushed_Error (2U)
#  endif

#  ifndef NeutralPushed_NotAvailable
#   define NeutralPushed_NotAvailable (3U)
#  endif

#  ifndef OffOn_Off
#   define OffOn_Off (0U)
#  endif

#  ifndef OffOn_On
#   define OffOn_On (1U)
#  endif

#  ifndef OffOn_Error
#   define OffOn_Error (2U)
#  endif

#  ifndef OffOn_NotAvailable
#   define OffOn_NotAvailable (3U)
#  endif

#  define InvalidValue_PushButtonStatus_T (3U)

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_FLEXIBLESWITCHESROUTER_CTRL_ROUTER_TYPE_H */
