/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_AxleLoadDistribution_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <AxleLoadDistribution_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_AXLELOADDISTRIBUTION_HMICTRL_H
# define _RTE_AXLELOADDISTRIBUTION_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_AxleLoadDistribution_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_ALD_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_BogieSwitch_DeviceIndication_DualDeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_MaxTract_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio1_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio2_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio4_DeviceIndication_DualDeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio5_DeviceIndication_DualDeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_Ratio6_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DualDeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_RatioALD_DualDeviceIndication_DualDeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AxleLoadDistribution_HMICtrl_TridemALD_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_ALDSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ECSStandByRequest_T, RTE_VAR_NOINIT) Rte_ECSStandByRequest_ISig_4_oCIOM_BB2_03P_oBackbone2_9b85740c_Tx; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1Switch2_Status_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio1SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio2SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio3SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio4SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio5SwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_Ratio6SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RatioALDSwitchStatus_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_TridemALDSwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ALDSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_ALD_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AltLoadDistribution_rqst_AltLoadDistribution_rqst (7U)
#  define Rte_InitValue_BogieSwitch_DeviceIndication_DualDeviceIndication (15U)
#  define Rte_InitValue_ECSStandByRequest_ECSStandByRequest (7U)
#  define Rte_InitValue_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest (3U)
#  define Rte_InitValue_LiftAxle1DirectControl_LiftAxle1DirectControl (7U)
#  define Rte_InitValue_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest (7U)
#  define Rte_InitValue_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle1PositionStatus_LiftAxle1PositionStatus (7U)
#  define Rte_InitValue_LiftAxle1Switch2_Status_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle1SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK (7U)
#  define Rte_InitValue_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest (3U)
#  define Rte_InitValue_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest (7U)
#  define Rte_InitValue_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle2SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK (7U)
#  define Rte_InitValue_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK (7U)
#  define Rte_InitValue_LoadDistributionChangeACK_LoadDistributionChangeACK (7U)
#  define Rte_InitValue_LoadDistributionChangeRequest_LoadDistributionChangeRequest (3U)
#  define Rte_InitValue_LoadDistributionFuncSelected_LoadDistributionFuncSelected (15U)
#  define Rte_InitValue_LoadDistributionRequested_LoadDistributionRequested (15U)
#  define Rte_InitValue_LoadDistributionRequestedACK_LoadDistributionRequestedACK (7U)
#  define Rte_InitValue_LoadDistributionSelected_LoadDistributionSelected (15U)
#  define Rte_InitValue_MaxTract_DeviceIndication_DeviceIndication (0U)
#  define Rte_InitValue_Ratio1SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Ratio1_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_Ratio2SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Ratio2_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_Ratio3SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Ratio4SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_Ratio4_DeviceIndication_DualDeviceIndication (15U)
#  define Rte_InitValue_Ratio5SwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_Ratio5_DeviceIndication_DualDeviceIndication (15U)
#  define Rte_InitValue_Ratio6SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_Ratio6_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_RatioALDSwitchStatus_A3PosSwitchStatus (7U)
#  define Rte_InitValue_RatioALD_DualDeviceIndication_DualDeviceIndication (15U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
#  define Rte_InitValue_TridemALDSwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_TridemALD_DeviceIndication_DeviceIndication (3U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1PositionStatus_LiftAxle1PositionStatus(P2VAR(LiftAxlePositionStatus_T, AUTOMATIC, RTE_AXLELOADDISTRIBUTION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK(P2VAR(LiftAxleUpRequestACK_T, AUTOMATIC, RTE_AXLELOADDISTRIBUTION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK(P2VAR(LiftAxleUpRequestACK_T, AUTOMATIC, RTE_AXLELOADDISTRIBUTION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK(P2VAR(LoadDistributionALDChangeACK_T, AUTOMATIC, RTE_AXLELOADDISTRIBUTION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionChangeACK_LoadDistributionChangeACK(P2VAR(LoadDistributionChangeACK_T, AUTOMATIC, RTE_AXLELOADDISTRIBUTION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionFuncSelected_LoadDistributionFuncSelected(P2VAR(LoadDistributionFuncSelected_T, AUTOMATIC, RTE_AXLELOADDISTRIBUTION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionRequestedACK_LoadDistributionRequestedACK(P2VAR(LoadDistributionRequestedACK_T, AUTOMATIC, RTE_AXLELOADDISTRIBUTION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionSelected_LoadDistributionSelected(P2VAR(LoadDistributionSelected_T, AUTOMATIC, RTE_AXLELOADDISTRIBUTION_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AxleLoadDistribution_HMICtrl_AltLoadDistribution_rqst_AltLoadDistribution_rqst(AltLoadDistribution_rqst_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest(InactiveActive_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle1DirectControl_LiftAxle1DirectControl(LiftAxleLiftPositionRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest(LiftAxleLiftPositionRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest(InactiveActive_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest(LiftAxleLiftPositionRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AxleLoadDistribution_HMICtrl_LoadDistributionChangeRequest_LoadDistributionChangeRequest(LoadDistributionChangeRequest_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AxleLoadDistribution_HMICtrl_LoadDistributionRequested_LoadDistributionRequested(LoadDistributionRequested_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ALDSwitchStatus_A2PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_ALDSwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_ALDSwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_ALDSwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ECSStandByRequest_ECSStandByRequest Rte_Read_AxleLoadDistribution_HMICtrl_ECSStandByRequest_ECSStandByRequest
#  define Rte_Read_AxleLoadDistribution_HMICtrl_ECSStandByRequest_ECSStandByRequest(data) (*(data) = Rte_ECSStandByRequest_ISig_4_oCIOM_BB2_03P_oBackbone2_9b85740c_Tx, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1MaxTractSwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LiftAxle1PositionStatus_LiftAxle1PositionStatus Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1PositionStatus_LiftAxle1PositionStatus
#  define Rte_Read_LiftAxle1Switch2_Status_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1Switch2_Status_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1Switch2_Status_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1Switch2_Status_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LiftAxle1SwitchStatus_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1SwitchStatus_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1SwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1SwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle1TRIDEMSwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle1UpRequestACK_LiftAxle1UpRequestACK
#  define Rte_Read_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2MaxTractSwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LiftAxle2SwitchStatus_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle2SwitchStatus_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle2SwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2SwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_LiftAxle2TRIDEMSwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK Rte_Read_AxleLoadDistribution_HMICtrl_LiftAxle2UpRequestACK_LiftAxle2UpRequestACK
#  define Rte_Read_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionALDChangeACK_LoadDistributionALDChangeACK
#  define Rte_Read_LoadDistributionChangeACK_LoadDistributionChangeACK Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionChangeACK_LoadDistributionChangeACK
#  define Rte_Read_LoadDistributionFuncSelected_LoadDistributionFuncSelected Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionFuncSelected_LoadDistributionFuncSelected
#  define Rte_Read_LoadDistributionRequestedACK_LoadDistributionRequestedACK Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionRequestedACK_LoadDistributionRequestedACK
#  define Rte_Read_LoadDistributionSelected_LoadDistributionSelected Rte_Read_AxleLoadDistribution_HMICtrl_LoadDistributionSelected_LoadDistributionSelected
#  define Rte_Read_Ratio1SwitchStatus_A2PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_Ratio1SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_Ratio1SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Ratio1SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio2SwitchStatus_A2PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_Ratio2SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_Ratio2SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Ratio2SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio3SwitchStatus_A2PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_Ratio3SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_Ratio3SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Ratio3SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio4SwitchStatus_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_Ratio4SwitchStatus_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_Ratio4SwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Ratio4SwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio5SwitchStatus_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_Ratio5SwitchStatus_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_Ratio5SwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Ratio5SwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Ratio6SwitchStatus_A2PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_Ratio6SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_Ratio6SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_Ratio6SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RatioALDSwitchStatus_A3PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_RatioALDSwitchStatus_A3PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_RatioALDSwitchStatus_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_RatioALDSwitchStatus_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_AxleLoadDistribution_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_AxleLoadDistribution_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TridemALDSwitchStatus_A2PosSwitchStatus Rte_Read_AxleLoadDistribution_HMICtrl_TridemALDSwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AxleLoadDistribution_HMICtrl_TridemALDSwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_TridemALDSwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_ALD_DeviceIndication_DeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_ALD_DeviceIndication_DeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_ALD_DeviceIndication_DeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_ALD_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AltLoadDistribution_rqst_AltLoadDistribution_rqst Rte_Write_AxleLoadDistribution_HMICtrl_AltLoadDistribution_rqst_AltLoadDistribution_rqst
#  define Rte_Write_BogieSwitch_DeviceIndication_DualDeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_BogieSwitch_DeviceIndication_DualDeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_BogieSwitch_DeviceIndication_DualDeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_BogieSwitch_DeviceIndication_DualDeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle1AutoLiftRequest_LiftAxle1AutoLiftRequest
#  define Rte_Write_LiftAxle1DirectControl_LiftAxle1DirectControl Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle1DirectControl_LiftAxle1DirectControl
#  define Rte_Write_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle1LiftPositionRequest_LiftAxle1LiftPositionRequest
#  define Rte_Write_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle2AutoLiftRequest_LiftAxle2AutoLiftRequest
#  define Rte_Write_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest Rte_Write_AxleLoadDistribution_HMICtrl_LiftAxle2LiftPositionRequest_LiftAxle2LiftPositionRequest
#  define Rte_Write_LoadDistributionChangeRequest_LoadDistributionChangeRequest Rte_Write_AxleLoadDistribution_HMICtrl_LoadDistributionChangeRequest_LoadDistributionChangeRequest
#  define Rte_Write_LoadDistributionRequested_LoadDistributionRequested Rte_Write_AxleLoadDistribution_HMICtrl_LoadDistributionRequested_LoadDistributionRequested
#  define Rte_Write_MaxTract_DeviceIndication_DeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_MaxTract_DeviceIndication_DeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_MaxTract_DeviceIndication_DeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_MaxTract_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio1_DeviceIndication_DeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_Ratio1_DeviceIndication_DeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_Ratio1_DeviceIndication_DeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_Ratio1_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio2_DeviceIndication_DeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_Ratio2_DeviceIndication_DeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_Ratio2_DeviceIndication_DeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_Ratio2_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio4_DeviceIndication_DualDeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_Ratio4_DeviceIndication_DualDeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_Ratio4_DeviceIndication_DualDeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_Ratio4_DeviceIndication_DualDeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio5_DeviceIndication_DualDeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_Ratio5_DeviceIndication_DualDeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_Ratio5_DeviceIndication_DualDeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_Ratio5_DeviceIndication_DualDeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_Ratio6_DeviceIndication_DeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_Ratio6_DeviceIndication_DeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_Ratio6_DeviceIndication_DeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_Ratio6_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_RatioALD_DualDeviceIndication_DualDeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_RatioALD_DualDeviceIndication_DualDeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_RatioALD_DualDeviceIndication_DualDeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_RatioALD_DualDeviceIndication_DualDeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_TridemALD_DeviceIndication_DeviceIndication Rte_Write_AxleLoadDistribution_HMICtrl_TridemALD_DeviceIndication_DeviceIndication
#  define Rte_Write_AxleLoadDistribution_HMICtrl_TridemALD_DeviceIndication_DeviceIndication(data) (Rte_AxleLoadDistribution_HMICtrl_TridemALD_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BOV_56_ALD_InvalidConfiguration_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)417, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BOV_63_ALD_ButtonStuck_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)251, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_AxleLoad_CRideLEDIndicationType_P1KN5_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1KN5_AxleLoad_CRideLEDIndicationType_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOS_AxleLoad_AccessoryBoggieALD_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOV_AxleLoad_RatioALD_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOW_AxleLoad_ArideLiftAxle_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1BOX_AxleLoad_TridemFirstAxleLift_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ0_AxleLoad_MaxTractionTag_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ2_AxleLoad_RatioPusherRocker_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZ3_AxleLoad_RatioTagRocker_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZW_AxleLoad_OneLiftPusher_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZY_AxleLoad_OneLiftTag_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1CZZ_AxleLoad_MaxTractionPusher_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1J6B_AxleLoad_AccessoryTridemALD_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1J6C_AxleLoad_BoggieDualRatio_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1J6D_AxleLoad_TridemDualRatio_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1KN2_AxleLoad_CRideLiftAxle_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1KN4_AxleLoad_CRideLEDlowerEnd_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M5B_AxleLoad_RatioRoadGripPusher_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1KN5_AxleLoad_CRideLEDIndicationType_v() (Rte_AddrPar_0x2B_P1KN5_AxleLoad_CRideLEDIndicationType_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BOS_AxleLoad_AccessoryBoggieALD_v() (Rte_AddrPar_0x2B_P1BOS_AxleLoad_AccessoryBoggieALD_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BOV_AxleLoad_RatioALD_v() (Rte_AddrPar_0x2B_P1BOV_AxleLoad_RatioALD_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BOW_AxleLoad_ArideLiftAxle_v() (Rte_AddrPar_0x2B_P1BOW_AxleLoad_ArideLiftAxle_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1BOX_AxleLoad_TridemFirstAxleLift_v() (Rte_AddrPar_0x2B_P1BOX_AxleLoad_TridemFirstAxleLift_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CZ0_AxleLoad_MaxTractionTag_v() (Rte_AddrPar_0x2B_P1CZ0_AxleLoad_MaxTractionTag_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v() (Rte_AddrPar_0x2B_P1CZ1_AxleLoad_RatioTagOrLoadDistrib_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CZ2_AxleLoad_RatioPusherRocker_v() (Rte_AddrPar_0x2B_P1CZ2_AxleLoad_RatioPusherRocker_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CZ3_AxleLoad_RatioTagRocker_v() (Rte_AddrPar_0x2B_P1CZ3_AxleLoad_RatioTagRocker_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CZW_AxleLoad_OneLiftPusher_v() (Rte_AddrPar_0x2B_P1CZW_AxleLoad_OneLiftPusher_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v() (Rte_AddrPar_0x2B_P1CZX_AxleLoad_OneLiftAxleMaxTraction_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CZY_AxleLoad_OneLiftTag_v() (Rte_AddrPar_0x2B_P1CZY_AxleLoad_OneLiftTag_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1CZZ_AxleLoad_MaxTractionPusher_v() (Rte_AddrPar_0x2B_P1CZZ_AxleLoad_MaxTractionPusher_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1J6B_AxleLoad_AccessoryTridemALD_v() (Rte_AddrPar_0x2B_P1J6B_AxleLoad_AccessoryTridemALD_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1J6C_AxleLoad_BoggieDualRatio_v() (Rte_AddrPar_0x2B_P1J6C_AxleLoad_BoggieDualRatio_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1J6D_AxleLoad_TridemDualRatio_v() (Rte_AddrPar_0x2B_P1J6D_AxleLoad_TridemDualRatio_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1KN2_AxleLoad_CRideLiftAxle_v() (Rte_AddrPar_0x2B_P1KN2_AxleLoad_CRideLiftAxle_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1KN4_AxleLoad_CRideLEDlowerEnd_v() (Rte_AddrPar_0x2B_P1KN4_AxleLoad_CRideLEDlowerEnd_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1M5B_AxleLoad_RatioRoadGripPusher_v() (Rte_AddrPar_0x2B_P1M5B_AxleLoad_RatioRoadGripPusher_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1BOY_AxleLoad_TridemSecondAxleLift_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1BOY_AxleLoad_TridemSecondAxleLift_v() (Rte_AddrPar_0x2B_and_0x37_P1BOY_AxleLoad_TridemSecondAxleLift_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define AxleLoadDistribution_HMICtrl_START_SEC_CODE
# include "AxleLoadDistribution_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_AxleLoadDistribution_HMICtrl_20ms_runnable AxleLoadDistribution_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_AxleLoadDistribution_HMICtrl_Init AxleLoadDistribution_HMICtrl_Init
# endif

FUNC(void, AxleLoadDistribution_HMICtrl_CODE) AxleLoadDistribution_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, AxleLoadDistribution_HMICtrl_CODE) AxleLoadDistribution_HMICtrl_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define AxleLoadDistribution_HMICtrl_STOP_SEC_CODE
# include "AxleLoadDistribution_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_AXLELOADDISTRIBUTION_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
