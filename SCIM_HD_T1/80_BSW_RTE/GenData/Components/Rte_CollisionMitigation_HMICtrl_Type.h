/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_CollisionMitigation_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <CollisionMitigation_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_COLLISIONMITIGATION_HMICTRL_TYPE_H
# define _RTE_COLLISIONMITIGATION_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A2PosSwitchStatus_Off
#   define A2PosSwitchStatus_Off (0U)
#  endif

#  ifndef A2PosSwitchStatus_On
#   define A2PosSwitchStatus_On (1U)
#  endif

#  ifndef A2PosSwitchStatus_Error
#   define A2PosSwitchStatus_Error (2U)
#  endif

#  ifndef A2PosSwitchStatus_NotAvailable
#   define A2PosSwitchStatus_NotAvailable (3U)
#  endif

#  ifndef CM_Status_CM_Disabled
#   define CM_Status_CM_Disabled (0U)
#  endif

#  ifndef CM_Status_CM_Enabled
#   define CM_Status_CM_Enabled (1U)
#  endif

#  ifndef CM_Status_CM_PreMitigationBraking
#   define CM_Status_CM_PreMitigationBraking (2U)
#  endif

#  ifndef CM_Status_CM_MitigationBraking
#   define CM_Status_CM_MitigationBraking (3U)
#  endif

#  ifndef CM_Status_CM_MitigationBrakingFinished
#   define CM_Status_CM_MitigationBrakingFinished (4U)
#  endif

#  ifndef CM_Status_CM_DisabledBySystem
#   define CM_Status_CM_DisabledBySystem (5U)
#  endif

#  ifndef CM_Status_ErrorIndicator
#   define CM_Status_ErrorIndicator (6U)
#  endif

#  ifndef CM_Status_NotAvailable
#   define CM_Status_NotAvailable (7U)
#  endif

#  ifndef CollSituationHMICtrlRequestVM_NoIndication
#   define CollSituationHMICtrlRequestVM_NoIndication (0U)
#  endif

#  ifndef CollSituationHMICtrlRequestVM_CM_FCWdisabledOnAftermarket
#   define CollSituationHMICtrlRequestVM_CM_FCWdisabledOnAftermarket (1U)
#  endif

#  ifndef CollSituationHMICtrlRequestVM_FCWdisabledOnAftermarket
#   define CollSituationHMICtrlRequestVM_FCWdisabledOnAftermarket (2U)
#  endif

#  ifndef CollSituationHMICtrlRequestVM_Spare1
#   define CollSituationHMICtrlRequestVM_Spare1 (3U)
#  endif

#  ifndef CollSituationHMICtrlRequestVM_Spare2
#   define CollSituationHMICtrlRequestVM_Spare2 (4U)
#  endif

#  ifndef CollSituationHMICtrlRequestVM_Spare3
#   define CollSituationHMICtrlRequestVM_Spare3 (5U)
#  endif

#  ifndef CollSituationHMICtrlRequestVM_Spare4
#   define CollSituationHMICtrlRequestVM_Spare4 (6U)
#  endif

#  ifndef CollSituationHMICtrlRequestVM_NotAvailable
#   define CollSituationHMICtrlRequestVM_NotAvailable (7U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef DisableEnable_Disable
#   define DisableEnable_Disable (0U)
#  endif

#  ifndef DisableEnable_Enable
#   define DisableEnable_Enable (1U)
#  endif

#  ifndef DisableEnable_Error
#   define DisableEnable_Error (2U)
#  endif

#  ifndef DisableEnable_NotAvailable
#   define DisableEnable_NotAvailable (3U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef SetCMOperation_NoRequest
#   define SetCMOperation_NoRequest (0U)
#  endif

#  ifndef SetCMOperation_SetEmergencyBrakeON
#   define SetCMOperation_SetEmergencyBrakeON (1U)
#  endif

#  ifndef SetCMOperation_SetEmergencyBrakeOFF
#   define SetCMOperation_SetEmergencyBrakeOFF (2U)
#  endif

#  ifndef SetCMOperation_Spare1
#   define SetCMOperation_Spare1 (3U)
#  endif

#  ifndef SetCMOperation_Spare2
#   define SetCMOperation_Spare2 (4U)
#  endif

#  ifndef SetCMOperation_Spare3
#   define SetCMOperation_Spare3 (5U)
#  endif

#  ifndef SetCMOperation_Error
#   define SetCMOperation_Error (6U)
#  endif

#  ifndef SetCMOperation_NotAvailable
#   define SetCMOperation_NotAvailable (7U)
#  endif

#  ifndef SetFCWOperation_NoRequest
#   define SetFCWOperation_NoRequest (0U)
#  endif

#  ifndef SetFCWOperation_SetCollisionWarningON
#   define SetFCWOperation_SetCollisionWarningON (1U)
#  endif

#  ifndef SetFCWOperation_SetCollisionWarningOFF
#   define SetFCWOperation_SetCollisionWarningOFF (2U)
#  endif

#  ifndef SetFCWOperation_Spare1
#   define SetFCWOperation_Spare1 (3U)
#  endif

#  ifndef SetFCWOperation_Spare2
#   define SetFCWOperation_Spare2 (4U)
#  endif

#  ifndef SetFCWOperation_Spare3
#   define SetFCWOperation_Spare3 (5U)
#  endif

#  ifndef SetFCWOperation_Error
#   define SetFCWOperation_Error (6U)
#  endif

#  ifndef SetFCWOperation_NotAvailable
#   define SetFCWOperation_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_COLLISIONMITIGATION_HMICTRL_TYPE_H */
