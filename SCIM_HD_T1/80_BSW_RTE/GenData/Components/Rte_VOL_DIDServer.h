/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VOL_DIDServer.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <VOL_DIDServer>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VOL_DIDSERVER_H
# define _RTE_VOL_DIDSERVER_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_VOL_DIDServer_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AmbientAirTemperature_AmbientAirTemperature (65535U)
#  define Rte_InitValue_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes (4294967295U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(uint8, RTE_CODE) Rte_Mode_VOL_DIDServer_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AmbientAirTemperature_AmbientAirTemperature Rte_Read_VOL_DIDServer_AmbientAirTemperature_AmbientAirTemperature
#  define Rte_Read_VOL_DIDServer_AmbientAirTemperature_AmbientAirTemperature(data) (Com_ReceiveSignal(ComConf_ComSignal_AmbientAirTemperature_ISig_3_oAMB_X_VMCU_oBackbone1J1939_a102c408_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes Rte_Read_VOL_DIDServer_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes
#  define Rte_Read_VOL_DIDServer_TotalVehicleDistanceHighRes_TotalVehicleDistanceHighRes(data) (Com_ReceiveSignal(ComConf_ComSignal_TotalVehicleDistanceHighRes_ISig_3_oVDHR_X_VMCU_oBackbone1J1939_6b802705_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_VOL_DIDServer_VehicleModeInternal_VehicleMode
#  define Rte_Read_VOL_DIDServer_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Mode_<p>_<m>
 *********************************************************************************************************************/
#  define Rte_Mode_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl Rte_Mode_VOL_DIDServer_DcmDiagnosticSessionControl_DcmDiagnosticSessionControl


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_CDD_LINDIAGNOSTICS_APPL_CODE) CddLinDiagServices_SlaveNodePnSnReq(LinDiagBusInfo LinBusInfo); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CddLinDiagServices_SlaveNodePnSnReq(arg1) (CddLinDiagServices_SlaveNodePnSnReq(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_CDD_LINDIAGNOSTICS_APPL_CODE) CddLinDiagServices_SlaveNodePnSnResp(P2VAR(LinDiagServiceStatus, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) DiagServiceStatus, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) NoOfLinSlaves, P2VAR(uint8, AUTOMATIC, RTE_CDD_LINDIAGNOSTICS_APPL_VAR) LinDiagRespPNSN); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CDD_LINDIAGNOSTICS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CddLinDiagServices_SlaveNodePnSnResp(arg1, arg2, arg3) (CddLinDiagServices_SlaveNodePnSnResp(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CJT_EnableCustomDemCfgCrc_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1CJT_EnableCustomDemCfgCrc_v() (Rte_AddrPar_0x29_X1CJT_EnableCustomDemCfgCrc_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_ChassisId_CHANO_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_CHANO_ChassisId_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_VIN_VINNO_T, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_VINNO_VIN_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1C54_FactoryModeActive_v() (Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_CHANO_ChassisId_v() (&(Rte_AddrPar_0x2F_CHANO_ChassisId_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_CHANO_ChassisId_v() (&Rte_AddrPar_0x2F_CHANO_ChassisId_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_VINNO_VIN_v() (&(Rte_AddrPar_0x2F_VINNO_VIN_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_VINNO_VIN_v() (&Rte_AddrPar_0x2F_VINNO_VIN_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

# endif /* !defined(RTE_CORE) */


# define VOL_DIDServer_START_SEC_CODE
# include "VOL_DIDServer_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_CHANO_Data_CHANO_ChassisId_ReadData DataServices_CHANO_Data_CHANO_ChassisId_ReadData
#  define RTE_RUNNABLE_DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData
#  define RTE_RUNNABLE_DataServices_P1AFS_Data_P1AFS_Odometer_ReadData DataServices_P1AFS_Data_P1AFS_Odometer_ReadData
#  define RTE_RUNNABLE_DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData
#  define RTE_RUNNABLE_DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData
#  define RTE_RUNNABLE_DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength
#  define RTE_RUNNABLE_DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData
#  define RTE_RUNNABLE_DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData
#  define RTE_RUNNABLE_DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength
#  define RTE_RUNNABLE_DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData
#  define RTE_RUNNABLE_DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength
#  define RTE_RUNNABLE_DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData
#  define RTE_RUNNABLE_DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData
#  define RTE_RUNNABLE_DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData
#  define RTE_RUNNABLE_DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData
#  define RTE_RUNNABLE_DataServices_VINNO_Data_VINNO_VIN_ReadData DataServices_VINNO_Data_VINNO_VIN_ReadData
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_CHANO_Data_CHANO_ChassisId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_CHANO_Data_CHANO_ChassisId_ReadData(P2VAR(Dcm_Data16ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFR_Data_P1AFR_OutdoorTemperature_ReadData(P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFS_Data_P1AFS_Odometer_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFS_Data_P1AFS_Odometer_ReadData(P2VAR(Dcm_Data4ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1AFT_Data_P1AFT_VehicleMode_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadData(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data406ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALA_Data_P1ALA_ECUHardwareNumber_ReadDataLength(Dcm_OpStatusType OpStatus, P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALB_Data_P1ALB_SystemNameOrEngineType_ReadData(P2VAR(Dcm_Data8ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadData(P2VAR(Dcm_Data241ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALP_Data_P1ALP_ApplicationDataId_ReadDataLength(P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadData(P2VAR(Dcm_Data241ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1ALQ_Data_P1ALQ_ApplicationSoftwareId_ReadDataLength(P2VAR(uint16, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) DataLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1B1O_Data_P1B1O_BootSWIdentifier_ReadData(P2VAR(Dcm_Data221ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1DIH_Data_P1DIH_activeDiagnosticSessionDataId_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1OLT_Data_P1OLT_BuildVersionInfo_ReadData(P2VAR(Dcm_Data64ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_P1Q82_Data_P1Q82_DescriptionFileSha256_ReadData(P2VAR(Dcm_Data64ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_VINNO_Data_VINNO_VIN_ReadData(P2VAR(uint8, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VOL_DIDServer_CODE) DataServices_VINNO_Data_VINNO_VIN_ReadData(P2VAR(Dcm_Data17ByteType, AUTOMATIC, RTE_VOL_DIDSERVER_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define VOL_DIDServer_STOP_SEC_CODE
# include "VOL_DIDServer_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_CHANO_Data_CHANO_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1AFR_Data_P1AFR_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1AFS_Data_P1AFS_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1AFT_Data_P1AFT_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ALA_Data_P1ALA_DCM_E_PENDING (10U)

#  define RTE_E_DataServices_P1ALA_Data_P1ALA_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ALB_Data_P1ALB_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ALP_Data_P1ALP_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1ALQ_Data_P1ALQ_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1B1O_Data_P1B1O_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DIH_Data_P1DIH_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1OLT_Data_P1OLT_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1Q82_Data_P1Q82_E_NOT_OK (1U)

#  define RTE_E_DataServices_VINNO_Data_VINNO_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VOL_DIDSERVER_H */
