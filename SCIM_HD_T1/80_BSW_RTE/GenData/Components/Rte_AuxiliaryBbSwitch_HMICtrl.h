/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_AuxiliaryBbSwitch_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <AuxiliaryBbSwitch_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_AUXILIARYBBSWITCH_HMICTRL_H
# define _RTE_AUXILIARYBBSWITCH_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_AuxiliaryBbSwitch_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch1_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch2_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch3_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch4_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch5_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch6_DeviceIndication_DeviceIndication; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch1SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch2SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch3SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch4SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch5SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A2PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch6SwitchStatus_A2PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_AuxBbSwitch1_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch2_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch3_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch4_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch5_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxBbSwitch6_DeviceIndication_DeviceIndication (3U)
#  define Rte_InitValue_AuxSwitch1SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch2SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch3SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch4SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch5SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitch6SwitchStatus_A2PosSwitchStatus (3U)
#  define Rte_InitValue_AuxSwitchBbLoad1_Request_AuxSwitchBbLoad1_Request (3U)
#  define Rte_InitValue_AuxSwitchBbLoad1_Status_AuxSwitchBbLoad1_Status (3U)
#  define Rte_InitValue_AuxSwitchBbLoad2_Request_AuxSwitchBbLoad2_Request (3U)
#  define Rte_InitValue_AuxSwitchBbLoad2_Status_AuxSwitchBbLoad2_Status (3U)
#  define Rte_InitValue_AuxSwitchBbLoad3_Request_AuxSwitchBbLoad3_Request (3U)
#  define Rte_InitValue_AuxSwitchBbLoad3_Status_AuxSwitchBbLoad3_Status (3U)
#  define Rte_InitValue_AuxSwitchBbLoad4_Request_AuxSwitchBbLoad4_Request (3U)
#  define Rte_InitValue_AuxSwitchBbLoad4_Status_AuxSwitchBbLoad4_Status (3U)
#  define Rte_InitValue_AuxSwitchBbLoad5_Request_AuxSwitchBbLoad5_Request (3U)
#  define Rte_InitValue_AuxSwitchBbLoad5_Status_AuxSwitchBbLoad5_Status (3U)
#  define Rte_InitValue_AuxSwitchBbLoad6_Request_AuxSwitchBbLoad6_Request (3U)
#  define Rte_InitValue_AuxSwitchBbLoad6_Status_AuxSwitchBbLoad6_Status (3U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
#  define Rte_InitValue_WRCAux1Request_WRCAuxRequest (3U)
#  define Rte_InitValue_WRCAux2Request_WRCAuxRequest (3U)
#  define Rte_InitValue_WRCAux3Request_WRCAuxRequest (3U)
#  define Rte_InitValue_WRCAux4Request_WRCAuxRequest (3U)
#  define Rte_InitValue_WRCAux5Request_WRCAuxRequest (3U)
#  define Rte_InitValue_WRCAux6Request_WRCAuxRequest (3U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad1_Status_AuxSwitchBbLoad1_Status(P2VAR(InactiveActive_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad2_Status_AuxSwitchBbLoad2_Status(P2VAR(InactiveActive_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad3_Status_AuxSwitchBbLoad3_Status(P2VAR(InactiveActive_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad4_Status_AuxSwitchBbLoad4_Status(P2VAR(InactiveActive_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad5_Status_AuxSwitchBbLoad5_Status(P2VAR(InactiveActive_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad6_Status_AuxSwitchBbLoad6_Status(P2VAR(InactiveActive_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux1Request_WRCAuxRequest(P2VAR(OffOn_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux2Request_WRCAuxRequest(P2VAR(OffOn_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux3Request_WRCAuxRequest(P2VAR(OffOn_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux4Request_WRCAuxRequest(P2VAR(OffOn_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux5Request_WRCAuxRequest(P2VAR(OffOn_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux6Request_WRCAuxRequest(P2VAR(OffOn_T, AUTOMATIC, RTE_AUXILIARYBBSWITCH_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad1_Request_AuxSwitchBbLoad1_Request(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad2_Request_AuxSwitchBbLoad2_Request(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad3_Request_AuxSwitchBbLoad3_Request(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad4_Request_AuxSwitchBbLoad4_Request(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad5_Request_AuxSwitchBbLoad5_Request(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad6_Request_AuxSwitchBbLoad6_Request(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AuxSwitch1SwitchStatus_A2PosSwitchStatus Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch1SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch1SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch1SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxSwitch2SwitchStatus_A2PosSwitchStatus Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch2SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch2SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch2SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxSwitch3SwitchStatus_A2PosSwitchStatus Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch3SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch3SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch3SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxSwitch4SwitchStatus_A2PosSwitchStatus Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch4SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch4SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch4SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxSwitch5SwitchStatus_A2PosSwitchStatus Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch5SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch5SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch5SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxSwitch6SwitchStatus_A2PosSwitchStatus Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch6SwitchStatus_A2PosSwitchStatus
#  define Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitch6SwitchStatus_A2PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_AuxSwitch6SwitchStatus_A2PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_AuxSwitchBbLoad1_Status_AuxSwitchBbLoad1_Status Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad1_Status_AuxSwitchBbLoad1_Status
#  define Rte_Read_AuxSwitchBbLoad2_Status_AuxSwitchBbLoad2_Status Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad2_Status_AuxSwitchBbLoad2_Status
#  define Rte_Read_AuxSwitchBbLoad3_Status_AuxSwitchBbLoad3_Status Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad3_Status_AuxSwitchBbLoad3_Status
#  define Rte_Read_AuxSwitchBbLoad4_Status_AuxSwitchBbLoad4_Status Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad4_Status_AuxSwitchBbLoad4_Status
#  define Rte_Read_AuxSwitchBbLoad5_Status_AuxSwitchBbLoad5_Status Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad5_Status_AuxSwitchBbLoad5_Status
#  define Rte_Read_AuxSwitchBbLoad6_Status_AuxSwitchBbLoad6_Status Rte_Read_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad6_Status_AuxSwitchBbLoad6_Status
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_AuxiliaryBbSwitch_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_AuxiliaryBbSwitch_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_WRCAux1Request_WRCAuxRequest Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux1Request_WRCAuxRequest
#  define Rte_Read_WRCAux2Request_WRCAuxRequest Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux2Request_WRCAuxRequest
#  define Rte_Read_WRCAux3Request_WRCAuxRequest Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux3Request_WRCAuxRequest
#  define Rte_Read_WRCAux4Request_WRCAuxRequest Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux4Request_WRCAuxRequest
#  define Rte_Read_WRCAux5Request_WRCAuxRequest Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux5Request_WRCAuxRequest
#  define Rte_Read_WRCAux6Request_WRCAuxRequest Rte_Read_AuxiliaryBbSwitch_HMICtrl_WRCAux6Request_WRCAuxRequest


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_AuxBbSwitch1_DeviceIndication_DeviceIndication Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch1_DeviceIndication_DeviceIndication
#  define Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch1_DeviceIndication_DeviceIndication(data) (Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch1_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxBbSwitch2_DeviceIndication_DeviceIndication Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch2_DeviceIndication_DeviceIndication
#  define Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch2_DeviceIndication_DeviceIndication(data) (Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch2_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxBbSwitch3_DeviceIndication_DeviceIndication Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch3_DeviceIndication_DeviceIndication
#  define Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch3_DeviceIndication_DeviceIndication(data) (Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch3_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxBbSwitch4_DeviceIndication_DeviceIndication Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch4_DeviceIndication_DeviceIndication
#  define Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch4_DeviceIndication_DeviceIndication(data) (Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch4_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxBbSwitch5_DeviceIndication_DeviceIndication Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch5_DeviceIndication_DeviceIndication
#  define Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch5_DeviceIndication_DeviceIndication(data) (Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch5_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxBbSwitch6_DeviceIndication_DeviceIndication Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch6_DeviceIndication_DeviceIndication
#  define Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch6_DeviceIndication_DeviceIndication(data) (Rte_AuxiliaryBbSwitch_HMICtrl_AuxBbSwitch6_DeviceIndication_DeviceIndication = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_AuxSwitchBbLoad1_Request_AuxSwitchBbLoad1_Request Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad1_Request_AuxSwitchBbLoad1_Request
#  define Rte_Write_AuxSwitchBbLoad2_Request_AuxSwitchBbLoad2_Request Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad2_Request_AuxSwitchBbLoad2_Request
#  define Rte_Write_AuxSwitchBbLoad3_Request_AuxSwitchBbLoad3_Request Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad3_Request_AuxSwitchBbLoad3_Request
#  define Rte_Write_AuxSwitchBbLoad4_Request_AuxSwitchBbLoad4_Request Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad4_Request_AuxSwitchBbLoad4_Request
#  define Rte_Write_AuxSwitchBbLoad5_Request_AuxSwitchBbLoad5_Request Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad5_Request_AuxSwitchBbLoad5_Request
#  define Rte_Write_AuxSwitchBbLoad6_Request_AuxSwitchBbLoad6_Request Rte_Write_AuxiliaryBbSwitch_HMICtrl_AuxSwitchBbLoad6_Request_AuxSwitchBbLoad6_Request


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_AuxBBSw_TimeoutForReq_P1DV1_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DV1_AuxBBSw_TimeoutForReq_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBBLoadStat_MaxInitTime_P1M93_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1M93_AuxBBLoadStat_MaxInitTime_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DI0_AuxBBSw5_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DI1_AuxBbSw6_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIW_AuxBbSw1_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIX_AuxBbSw2_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIY_AuxBbSw3_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DIZ_AuxBbSw4_Act_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1DV1_AuxBBSw_TimeoutForReq_v() (Rte_AddrPar_0x2B_P1DV1_AuxBBSw_TimeoutForReq_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1M93_AuxBBLoadStat_MaxInitTime_v() (Rte_AddrPar_0x2B_P1M93_AuxBBLoadStat_MaxInitTime_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DI0_AuxBBSw5_Act_v() (Rte_AddrPar_0x2B_P1DI0_AuxBBSw5_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DI1_AuxBbSw6_Act_v() (Rte_AddrPar_0x2B_P1DI1_AuxBbSw6_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DIW_AuxBbSw1_Act_v() (Rte_AddrPar_0x2B_P1DIW_AuxBbSw1_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DIX_AuxBbSw2_Act_v() (Rte_AddrPar_0x2B_P1DIX_AuxBbSw2_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DIY_AuxBbSw3_Act_v() (Rte_AddrPar_0x2B_P1DIY_AuxBbSw3_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DIZ_AuxBbSw4_Act_v() (Rte_AddrPar_0x2B_P1DIZ_AuxBbSw4_Act_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_AuxBbSw1_Logic_P1DI2_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI2_AuxBbSw1_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw2_Logic_P1DI3_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI3_AuxBbSw2_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw3_Logic_P1DI4_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI4_AuxBbSw3_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw4_Logic_P1DI5_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI5_AuxBbSw4_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw5_Logic_P1DI6_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI6_AuxBbSw5_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AuxBbSw6_Logic_P1DI7_T, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1DI7_AuxBbSw6_Logic_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B_and_0x37) Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1DI2_AuxBbSw1_Logic_v() (Rte_AddrPar_0x2B_and_0x37_P1DI2_AuxBbSw1_Logic_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DI3_AuxBbSw2_Logic_v() (Rte_AddrPar_0x2B_and_0x37_P1DI3_AuxBbSw2_Logic_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DI4_AuxBbSw3_Logic_v() (Rte_AddrPar_0x2B_and_0x37_P1DI4_AuxBbSw3_Logic_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DI5_AuxBbSw4_Logic_v() (Rte_AddrPar_0x2B_and_0x37_P1DI5_AuxBbSw4_Logic_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DI6_AuxBbSw5_Logic_v() (Rte_AddrPar_0x2B_and_0x37_P1DI6_AuxBbSw5_Logic_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1DI7_AuxBbSw6_Logic_v() (Rte_AddrPar_0x2B_and_0x37_P1DI7_AuxBbSw6_Logic_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B9X_WirelessRC_Enable_v() (Rte_AddrPar_0x2B_and_0x37_P1B9X_WirelessRC_Enable_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define AuxiliaryBbSwitch_HMICtrl_START_SEC_CODE
# include "AuxiliaryBbSwitch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_AuxiliaryBbSwitch_HMICtrl_20ms_runnable AuxiliaryBbSwitch_HMICtrl_20ms_runnable
#  define RTE_RUNNABLE_AuxiliaryBbSwitch_HMICtrl_init AuxiliaryBbSwitch_HMICtrl_init
# endif

FUNC(void, AuxiliaryBbSwitch_HMICtrl_CODE) AuxiliaryBbSwitch_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, AuxiliaryBbSwitch_HMICtrl_CODE) AuxiliaryBbSwitch_HMICtrl_init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define AuxiliaryBbSwitch_HMICtrl_STOP_SEC_CODE
# include "AuxiliaryBbSwitch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_AUXILIARYBBSWITCH_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
