/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_LpuMgr_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <LpuMgr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_LPUMGR_TYPE_H
# define _RTE_LPUMGR_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef FSC_ShutdownReady
#   define FSC_ShutdownReady (0U)
#  endif

#  ifndef FSC_Reduced_12vDcDcLimit
#   define FSC_Reduced_12vDcDcLimit (1U)
#  endif

#  ifndef FSC_Reduced
#   define FSC_Reduced (2U)
#  endif

#  ifndef FSC_Operating
#   define FSC_Operating (3U)
#  endif

#  ifndef FSC_Protecting
#   define FSC_Protecting (4U)
#  endif

#  ifndef FSC_Withstand
#   define FSC_Withstand (5U)
#  endif

#  ifndef FSC_NotAvailable
#   define FSC_NotAvailable (6U)
#  endif

#  ifndef SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated
#   define SEWS_PcbConfig_Adi_X1CXW_T_NotPopulated (0U)
#  endif

#  ifndef SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration
#   define SEWS_PcbConfig_Adi_X1CXW_T_PullDownConfiguration (1U)
#  endif

#  ifndef SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration
#   define SEWS_PcbConfig_Adi_X1CXW_T_PullUpLivingConfiguration (2U)
#  endif

#  ifndef SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration
#   define SEWS_PcbConfig_Adi_X1CXW_T_PullUpParkedConfiguration (3U)
#  endif

#  ifndef SEWS_PcbConfig_CanInterfaces_X1CX2_T_NotPopulated
#   define SEWS_PcbConfig_CanInterfaces_X1CX2_T_NotPopulated (0U)
#  endif

#  ifndef SEWS_PcbConfig_CanInterfaces_X1CX2_T_Populated
#   define SEWS_PcbConfig_CanInterfaces_X1CX2_T_Populated (1U)
#  endif

#  ifndef SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated
#   define SEWS_PcbConfig_DOBHS_X1CXX_T_NotPopulated (0U)
#  endif

#  ifndef SEWS_PcbConfig_DOBHS_X1CXX_T_Populated
#   define SEWS_PcbConfig_DOBHS_X1CXX_T_Populated (1U)
#  endif

#  ifndef SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated
#   define SEWS_PcbConfig_DOWHS_X1CXY_T_NotPopulated (0U)
#  endif

#  ifndef SEWS_PcbConfig_DOWHS_X1CXY_T_Populated
#   define SEWS_PcbConfig_DOWHS_X1CXY_T_Populated (1U)
#  endif

#  ifndef SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated
#   define SEWS_PcbConfig_DOWLS_X1CXZ_T_NotPopulated (0U)
#  endif

#  ifndef SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated
#   define SEWS_PcbConfig_DOWLS_X1CXZ_T_Populated (1U)
#  endif

#  ifndef SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated
#   define SEWS_PcbConfig_LinInterfaces_X1CX0_T_NotPopulated (0U)
#  endif

#  ifndef SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated
#   define SEWS_PcbConfig_LinInterfaces_X1CX0_T_Populated (1U)
#  endif

#  ifndef SEWS_X1CX4_PiInterface_T_NotPopulated
#   define SEWS_X1CX4_PiInterface_T_NotPopulated (0U)
#  endif

#  ifndef SEWS_X1CX4_PiInterface_T_Populated
#   define SEWS_X1CX4_PiInterface_T_Populated (1U)
#  endif

#  ifndef VehicleMode_Hibernate
#   define VehicleMode_Hibernate (0U)
#  endif

#  ifndef VehicleMode_Parked
#   define VehicleMode_Parked (1U)
#  endif

#  ifndef VehicleMode_Living
#   define VehicleMode_Living (2U)
#  endif

#  ifndef VehicleMode_Accessory
#   define VehicleMode_Accessory (3U)
#  endif

#  ifndef VehicleMode_PreRunning
#   define VehicleMode_PreRunning (4U)
#  endif

#  ifndef VehicleMode_Cranking
#   define VehicleMode_Cranking (5U)
#  endif

#  ifndef VehicleMode_Running
#   define VehicleMode_Running (6U)
#  endif

#  ifndef VehicleMode_Spare_1
#   define VehicleMode_Spare_1 (7U)
#  endif

#  ifndef VehicleMode_Spare_2
#   define VehicleMode_Spare_2 (8U)
#  endif

#  ifndef VehicleMode_Spare_3
#   define VehicleMode_Spare_3 (9U)
#  endif

#  ifndef VehicleMode_Spare_4
#   define VehicleMode_Spare_4 (10U)
#  endif

#  ifndef VehicleMode_Spare_5
#   define VehicleMode_Spare_5 (11U)
#  endif

#  ifndef VehicleMode_Spare_6
#   define VehicleMode_Spare_6 (12U)
#  endif

#  ifndef VehicleMode_Spare_7
#   define VehicleMode_Spare_7 (13U)
#  endif

#  ifndef VehicleMode_Error
#   define VehicleMode_Error (14U)
#  endif

#  ifndef VehicleMode_NotAvailable
#   define VehicleMode_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_LPUMGR_TYPE_H */
