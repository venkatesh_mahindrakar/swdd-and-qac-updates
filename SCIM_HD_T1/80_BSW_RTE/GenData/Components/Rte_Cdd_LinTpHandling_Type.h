/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Cdd_LinTpHandling_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <Cdd_LinTpHandling>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_CDD_LINTPHANDLING_TYPE_H
# define _RTE_CDD_LINTPHANDLING_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef CDD_LIN_NOT_OK
#   define CDD_LIN_NOT_OK (11U)
#  endif

#  ifndef CDD_LIN_TX_OK
#   define CDD_LIN_TX_OK (0U)
#  endif

#  ifndef CDD_LIN_TX_BUSY
#   define CDD_LIN_TX_BUSY (1U)
#  endif

#  ifndef CDD_LIN_TX_HEADER_ERROR
#   define CDD_LIN_TX_HEADER_ERROR (2U)
#  endif

#  ifndef CDD_LIN_TX_ERROR
#   define CDD_LIN_TX_ERROR (3U)
#  endif

#  ifndef CDD_LIN_RX_OK
#   define CDD_LIN_RX_OK (4U)
#  endif

#  ifndef CDD_LIN_RX_BUSY
#   define CDD_LIN_RX_BUSY (5U)
#  endif

#  ifndef CDD_LIN_RX_ERROR
#   define CDD_LIN_RX_ERROR (6U)
#  endif

#  ifndef CDD_LIN_RX_NO_RESPONSE
#   define CDD_LIN_RX_NO_RESPONSE (7U)
#  endif

#  ifndef CDD_LIN_NONE
#   define CDD_LIN_NONE (9U)
#  endif

# endif /* RTE_CORE */


/**********************************************************************************************************************
 * Definitions for Mode Management
 *********************************************************************************************************************/
# ifndef RTE_MODETYPE_BswMRteMDG_LIN1Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN1Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN1Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN2Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN2Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN2Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN3Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN3Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN3Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN4Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN4Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN4Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LIN5Schedule
#  define RTE_MODETYPE_BswMRteMDG_LIN5Schedule
typedef uint8 Rte_ModeType_BswMRteMDG_LIN5Schedule;
# endif
# ifndef RTE_MODETYPE_BswMRteMDG_LINSchTableState
#  define RTE_MODETYPE_BswMRteMDG_LINSchTableState
typedef uint8 Rte_ModeType_BswMRteMDG_LINSchTableState;
# endif

# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN1Schedule_LIN1_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_NULL (3U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN1Schedule_LIN1_Table1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1 (4U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN1Schedule_LIN1_Table2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2 (5U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN1Schedule_LIN1_Table_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E
#  define RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E (6U)
# endif
# define RTE_TRANSITION_Cdd_LinTpHandling_BswMRteMDG_LIN1Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN1Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN1Schedule (7U)
# endif

# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0 (1U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN2Schedule_LIN2_NULL (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_NULL (2U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN2Schedule_LIN2_TABLE0 (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0 (3U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E (4U)
# endif
# define RTE_TRANSITION_Cdd_LinTpHandling_BswMRteMDG_LIN2Schedule (5U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN2Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN2Schedule (5U)
# endif

# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN3Schedule_LIN3_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_NULL (3U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN3Schedule_LIN3_TABLE1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1 (4U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN3Schedule_LIN3_TABLE2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2 (5U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E (6U)
# endif
# define RTE_TRANSITION_Cdd_LinTpHandling_BswMRteMDG_LIN3Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN3Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN3Schedule (7U)
# endif

# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN4Schedule_LIN4_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_NULL (3U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN4Schedule_LIN4_TABLE1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1 (4U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN4Schedule_LIN4_TABLE2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2 (5U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E (6U)
# endif
# define RTE_TRANSITION_Cdd_LinTpHandling_BswMRteMDG_LIN4Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN4Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN4Schedule (7U)
# endif

# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp (0U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp (0U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1 (1U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1 (1U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2 (2U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2 (2U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN5Schedule_LIN5_NULL (3U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_NULL (3U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN5Schedule_LIN5_TABLE1 (4U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1 (4U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN5Schedule_LIN5_TABLE2 (5U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2 (5U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E (6U)
# ifndef RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E
#  define RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E (6U)
# endif
# define RTE_TRANSITION_Cdd_LinTpHandling_BswMRteMDG_LIN5Schedule (7U)
# ifndef RTE_TRANSITION_BswMRteMDG_LIN5Schedule
#  define RTE_TRANSITION_BswMRteMDG_LIN5Schedule (7U)
# endif

# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification (0U)
# ifndef RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification
#  define RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification (0U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT (1U)
# ifndef RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT
#  define RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_INIT (1U)
# endif
# define RTE_MODE_Cdd_LinTpHandling_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication (2U)
# ifndef RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication
#  define RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication (2U)
# endif
# define RTE_TRANSITION_Cdd_LinTpHandling_BswMRteMDG_LINSchTableState (3U)
# ifndef RTE_TRANSITION_BswMRteMDG_LINSchTableState
#  define RTE_TRANSITION_BswMRteMDG_LINSchTableState (3U)
# endif

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_CDD_LINTPHANDLING_TYPE_H */
