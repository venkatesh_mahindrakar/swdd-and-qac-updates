/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Immo.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <Immo>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_IMMO_H
# define _RTE_IMMO_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Immo_Type.h"
# include "Rte_DataHandleType.h"


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(Boolean, RTE_VAR_NOINIT) Rte_Irv_Immo_stopByUser; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_Immo_AddrParP1DS4_stat_dataP1DS4(P2VAR(SEWS_KeyfobEncryptCode_P1DS4_T, AUTOMATIC, RTE_IMMO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_Immo_AddrParP1DS4_stat_dataP1DS4(P2VAR(SEWS_KeyfobEncryptCode_P1DS4_a_T, AUTOMATIC, RTE_IMMO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_Immo_KeyFobNV_PR_KeyFobNV(P2VAR(uint8, AUTOMATIC, RTE_IMMO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_Immo_KeyFobNV_PR_KeyFobNV(P2VAR(KeyFobNVM_T, AUTOMATIC, RTE_IMMO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Immo_KeyFobNV_PR_KeyFobNV(P2CONST(uint8, AUTOMATIC, RTE_IMMO_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Immo_KeyFobNV_PR_KeyFobNV(P2CONST(KeyFobNVM_T, AUTOMATIC, RTE_IMMO_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_AddrParP1DS4_stat_dataP1DS4 Rte_Read_Immo_AddrParP1DS4_stat_dataP1DS4
#  define Rte_Read_KeyFobNV_PR_KeyFobNV Rte_Read_Immo_KeyFobNV_PR_KeyFobNV


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_KeyFobNV_PR_KeyFobNV Rte_Write_Immo_KeyFobNV_PR_KeyFobNV


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) RE_SetupDstTelegram(uint8 Dst_Order); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_SetupDstTelegram_SetDstTelegram(arg1) (RE_SetupDstTelegram(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_LFIC_APPL_CODE) RE_TimeoutTxTelegram(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_LFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_TimeoutTxTelegram_CS() (RE_TimeoutTxTelegram(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_ImmoProcessingRqst_ImmoCircuitProcessing_stopByUser(data) \
  (Rte_Irv_Immo_stopByUser = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_Immo_10ms_runnable_stopByUser() \
  Rte_Irv_Immo_stopByUser
/* PRQA L:L1 */


# endif /* !defined(RTE_CORE) */


# define Immo_START_SEC_CODE
# include "Immo_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_ImmoProcessingRqst_GetImmoCircuitProcessingResult GetImmoCircuitProcessingResult
#  define RTE_RUNNABLE_ImmoProcessingRqst_ImmoCircuitProcessing ImmoCircuitProcessing
#  define RTE_RUNNABLE_Immo_10ms_runnable Immo_10ms_runnable
#  define RTE_RUNNABLE_KeyfobMatchingOperations_GetMatchingStatus GetMatchingStatus
#  define RTE_RUNNABLE_KeyfobMatchingOperations_MatchKeyfobByLF MatchKeyfobByLF
#  define RTE_RUNNABLE_KeyfobMatchingOperations_ReinitializeMatchingList ReinitializeMatchingList
# endif

FUNC(void, Immo_CODE) GetImmoCircuitProcessingResult(P2VAR(SCIM_ImmoDriver_ProcessingStatus_T, AUTOMATIC, RTE_IMMO_APPL_VAR) ImmoProcessingStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Immo_CODE) ImmoCircuitProcessing(SCIM_ImmoType_T ImmoType, Boolean AuthRequestedbyImmo); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Immo_CODE) Immo_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, Immo_CODE) GetMatchingStatus(P2VAR(uint8, AUTOMATIC, RTE_IMMO_APPL_VAR) matchingStatus, P2VAR(uint8, AUTOMATIC, RTE_IMMO_APPL_VAR) matchedKeyfobCount); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Immo_CODE) MatchKeyfobByLF(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, Immo_CODE) ReinitializeMatchingList(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define Immo_STOP_SEC_CODE
# include "Immo_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_IMMO_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
