/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SideReverseLight_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <SideReverseLight_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SIDEREVERSELIGHT_HMICTRL_TYPE_H
# define _RTE_SIDEREVERSELIGHT_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A3PosSwitchStatus_Middle
#   define A3PosSwitchStatus_Middle (0U)
#  endif

#  ifndef A3PosSwitchStatus_Lower
#   define A3PosSwitchStatus_Lower (1U)
#  endif

#  ifndef A3PosSwitchStatus_Upper
#   define A3PosSwitchStatus_Upper (2U)
#  endif

#  ifndef A3PosSwitchStatus_Spare
#   define A3PosSwitchStatus_Spare (3U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_01
#   define A3PosSwitchStatus_Spare_01 (4U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_02
#   define A3PosSwitchStatus_Spare_02 (5U)
#  endif

#  ifndef A3PosSwitchStatus_Error
#   define A3PosSwitchStatus_Error (6U)
#  endif

#  ifndef A3PosSwitchStatus_NotAvailable
#   define A3PosSwitchStatus_NotAvailable (7U)
#  endif

#  ifndef BBNetworkExtraSideWrknLights_KeepCurrentLightStatus
#   define BBNetworkExtraSideWrknLights_KeepCurrentLightStatus (0U)
#  endif

#  ifndef BBNetworkExtraSideWrknLights_TurnOnLightWorkingMode
#   define BBNetworkExtraSideWrknLights_TurnOnLightWorkingMode (1U)
#  endif

#  ifndef BBNetworkExtraSideWrknLights_TurnOffLightWorkingMode
#   define BBNetworkExtraSideWrknLights_TurnOffLightWorkingMode (2U)
#  endif

#  ifndef BBNetworkExtraSideWrknLights_Reserved
#   define BBNetworkExtraSideWrknLights_Reserved (3U)
#  endif

#  ifndef BBNetworkExtraSideWrknLights_Reserved_01
#   define BBNetworkExtraSideWrknLights_Reserved_01 (4U)
#  endif

#  ifndef BBNetworkExtraSideWrknLights_Reserved_02
#   define BBNetworkExtraSideWrknLights_Reserved_02 (5U)
#  endif

#  ifndef BBNetworkExtraSideWrknLights_Error
#   define BBNetworkExtraSideWrknLights_Error (6U)
#  endif

#  ifndef BBNetworkExtraSideWrknLights_NotAvailable
#   define BBNetworkExtraSideWrknLights_NotAvailable (7U)
#  endif

#  ifndef CabExtraSideWorkingLight_rqst_ExtraSideWorkingLightNotRqstdCab
#   define CabExtraSideWorkingLight_rqst_ExtraSideWorkingLightNotRqstdCab (0U)
#  endif

#  ifndef CabExtraSideWorkingLight_rqst_WorkingModeRequestedCab
#   define CabExtraSideWorkingLight_rqst_WorkingModeRequestedCab (1U)
#  endif

#  ifndef CabExtraSideWorkingLight_rqst_ReverseModeRequestedCab
#   define CabExtraSideWorkingLight_rqst_ReverseModeRequestedCab (2U)
#  endif

#  ifndef CabExtraSideWorkingLight_rqst_SpareValue
#   define CabExtraSideWorkingLight_rqst_SpareValue (3U)
#  endif

#  ifndef CabExtraSideWorkingLight_rqst_SpareValue_01
#   define CabExtraSideWorkingLight_rqst_SpareValue_01 (4U)
#  endif

#  ifndef CabExtraSideWorkingLight_rqst_SpareValue_02
#   define CabExtraSideWorkingLight_rqst_SpareValue_02 (5U)
#  endif

#  ifndef CabExtraSideWorkingLight_rqst_Error
#   define CabExtraSideWorkingLight_rqst_Error (6U)
#  endif

#  ifndef CabExtraSideWorkingLight_rqst_NotAvailable
#   define CabExtraSideWorkingLight_rqst_NotAvailable (7U)
#  endif

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerOff
#   define DualDeviceIndication_UpperOffLowerOff (0U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerOff
#   define DualDeviceIndication_UpperOnLowerOff (1U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerOff
#   define DualDeviceIndication_UpperBlinkLowerOff (2U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerOff
#   define DualDeviceIndication_UpperDontCareLowerOff (3U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerOn
#   define DualDeviceIndication_UpperOffLowerOn (4U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerOn
#   define DualDeviceIndication_UpperOnLowerOn (5U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerOn
#   define DualDeviceIndication_UpperBlinkLowerOn (6U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerOn
#   define DualDeviceIndication_UpperDontCareLowerOn (7U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerBlink
#   define DualDeviceIndication_UpperOffLowerBlink (8U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerBlink
#   define DualDeviceIndication_UpperOnLowerBlink (9U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerBlink
#   define DualDeviceIndication_UpperBlinkLowerBlink (10U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerBlink
#   define DualDeviceIndication_UpperDontCareLowerBlink (11U)
#  endif

#  ifndef DualDeviceIndication_UpperOffLowerDontCare
#   define DualDeviceIndication_UpperOffLowerDontCare (12U)
#  endif

#  ifndef DualDeviceIndication_UpperOnLowerDontCare
#   define DualDeviceIndication_UpperOnLowerDontCare (13U)
#  endif

#  ifndef DualDeviceIndication_UpperBlinkLowerDontCare
#   define DualDeviceIndication_UpperBlinkLowerDontCare (14U)
#  endif

#  ifndef DualDeviceIndication_UpperDontCareLowerDontCare
#   define DualDeviceIndication_UpperDontCareLowerDontCare (15U)
#  endif

#  ifndef OffOn_Off
#   define OffOn_Off (0U)
#  endif

#  ifndef OffOn_On
#   define OffOn_On (1U)
#  endif

#  ifndef OffOn_Error
#   define OffOn_Error (2U)
#  endif

#  ifndef OffOn_NotAvailable
#   define OffOn_NotAvailable (3U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SIDEREVERSELIGHT_HMICTRL_TYPE_H */
