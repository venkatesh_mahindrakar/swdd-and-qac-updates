/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DriveSidePwrWindowsSelector.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DriveSidePwrWindowsSelector>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DRIVESIDEPWRWINDOWSSELECTOR_H
# define _RTE_DRIVESIDEPWRWINDOWSSELECTOR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DriveSidePwrWindowsSelector_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2PowerWinCloseDSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2PowerWinClosePSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2PowerWinOpenDSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_BunkH2PowerWinOpenPSBtn_stat_PushButtonStatus (3U)
#  define Rte_InitValue_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus (7U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriveSidePwrWindowsSelector_BunkH2PowerWinCloseDSBtn_stat_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriveSidePwrWindowsSelector_BunkH2PowerWinClosePSBtn_stat_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriveSidePwrWindowsSelector_BunkH2PowerWinOpenDSBtn_stat_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_DriveSidePwrWindowsSelector_BunkH2PowerWinOpenPSBtn_stat_PushButtonStatus(PushButtonStatus_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus Rte_Read_DriveSidePwrWindowsSelector_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus
#  define Rte_Read_DriveSidePwrWindowsSelector_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinCloseDSBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus Rte_Read_DriveSidePwrWindowsSelector_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus
#  define Rte_Read_DriveSidePwrWindowsSelector_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinClosePSBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus Rte_Read_DriveSidePwrWindowsSelector_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus
#  define Rte_Read_DriveSidePwrWindowsSelector_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenDSBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus Rte_Read_DriveSidePwrWindowsSelector_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus
#  define Rte_Read_DriveSidePwrWindowsSelector_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus(data) (*(data) = Rte_BunkUserInterfaceHigh2_LINMaCtrl_BunkH2OutPwrWinOpenPSBtn_stat_PushButtonStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus Rte_Read_DriveSidePwrWindowsSelector_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus
#  define Rte_Read_DriveSidePwrWindowsSelector_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinLeftSide_stat_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus Rte_Read_DriveSidePwrWindowsSelector_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus
#  define Rte_Read_DriveSidePwrWindowsSelector_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_FlexSwitchPwrWinRightSide_stat_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_DriveSidePwrWindowsSelector_SwcActivation_Living_Living
#  define Rte_Read_DriveSidePwrWindowsSelector_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_BunkH2PowerWinCloseDSBtn_stat_PushButtonStatus Rte_Write_DriveSidePwrWindowsSelector_BunkH2PowerWinCloseDSBtn_stat_PushButtonStatus
#  define Rte_Write_BunkH2PowerWinClosePSBtn_stat_PushButtonStatus Rte_Write_DriveSidePwrWindowsSelector_BunkH2PowerWinClosePSBtn_stat_PushButtonStatus
#  define Rte_Write_BunkH2PowerWinOpenDSBtn_stat_PushButtonStatus Rte_Write_DriveSidePwrWindowsSelector_BunkH2PowerWinOpenDSBtn_stat_PushButtonStatus
#  define Rte_Write_BunkH2PowerWinOpenPSBtn_stat_PushButtonStatus Rte_Write_DriveSidePwrWindowsSelector_BunkH2PowerWinOpenPSBtn_stat_PushButtonStatus


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_DriverPosition_LHD_RHD_P1ALJ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1ALJ_DriverPosition_LHD_RHD_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2G_LECMH_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1ALJ_DriverPosition_LHD_RHD_v() (Rte_AddrPar_0x2B_P1ALJ_DriverPosition_LHD_RHD_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1B2G_LECMH_Installed_v() (Rte_AddrPar_0x2B_P1B2G_LECMH_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define DriveSidePwrWindowsSelector_START_SEC_CODE
# include "DriveSidePwrWindowsSelector_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DriveSidePwrWindowsSelector_20ms_runnable DriveSidePwrWindowsSelector_20ms_runnable
# endif

FUNC(void, DriveSidePwrWindowsSelector_CODE) DriveSidePwrWindowsSelector_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define DriveSidePwrWindowsSelector_STOP_SEC_CODE
# include "DriveSidePwrWindowsSelector_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DRIVESIDEPWRWINDOWSSELECTOR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
