/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Keyfob_Mgr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <Keyfob_Mgr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_KEYFOB_MGR_H
# define _RTE_KEYFOB_MGR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_Keyfob_Mgr_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(KeyfobAuth_stat_T, RTE_VAR_NOINIT) Rte_Keyfob_Mgr_KeyfobAuth_stat_KeyfobAuth_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyfobOutsideLocation_stat_T, RTE_VAR_NOINIT) Rte_Keyfob_Mgr_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DriverAuthDeviceMatching_T, RTE_VAR_NOINIT) Rte_AuthenticationDevice_UICtrl_DriverAuthDeviceMatching_DriverAuthDeviceMatching; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyfobAuth_rqst_T, RTE_VAR_NOINIT) Rte_DriverAuthentication2_Ctrl_KeyfobAuth_rqst_KeyfobAuth_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(KeyfobLocation_rqst_T, RTE_VAR_NOINIT) Rte_VehicleAccess_Ctrl_KeyfobLocation_rqst_KeyfobLocation_rqst; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_DiagActiveState_isDiagActive (0U)
#  define Rte_InitValue_DriverAuthDeviceMatching_DriverAuthDeviceMatching (3U)
#  define Rte_InitValue_KeyfobAuth_rqst_KeyfobAuth_rqst (7U)
#  define Rte_InitValue_KeyfobAuth_stat_KeyfobAuth_stat (7U)
#  define Rte_InitValue_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat (0U)
#  define Rte_InitValue_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst (3U)
#  define Rte_InitValue_KeyfobLocation_rqst_KeyfobLocation_rqst (3U)
#  define Rte_InitValue_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle (7U)
#  define Rte_InitValue_SwcActivation_Security_SwcActivation_Security (1U)
# endif


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Buffers for inter-runnable variables
 *********************************************************************************************************************/

#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_Keyfob_Mgr_P1B0T_MatchedKeyfobCount; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_Irv_Keyfob_Mgr_P1B0T_MatchingStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
# endif /* !defined(RTE_CORE) */


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_Keyfob_Mgr_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst(P2VAR(KeyfobInCabPresencePS_T, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Keyfob_Mgr_AddrParP1DS4_stat_dataP1DS4(P2CONST(SEWS_KeyfobEncryptCode_P1DS4_T, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Keyfob_Mgr_AddrParP1DS4_stat_dataP1DS4(P2CONST(SEWS_KeyfobEncryptCode_P1DS4_a_T, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_Keyfob_Mgr_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat(KeyfobInCabLocation_stat_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_DiagActiveState_isDiagActive Rte_Read_Keyfob_Mgr_DiagActiveState_isDiagActive
#  define Rte_Read_Keyfob_Mgr_DiagActiveState_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DriverAuthDeviceMatching_DriverAuthDeviceMatching Rte_Read_Keyfob_Mgr_DriverAuthDeviceMatching_DriverAuthDeviceMatching
#  define Rte_Read_Keyfob_Mgr_DriverAuthDeviceMatching_DriverAuthDeviceMatching(data) (*(data) = Rte_AuthenticationDevice_UICtrl_DriverAuthDeviceMatching_DriverAuthDeviceMatching, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyfobAuth_rqst_KeyfobAuth_rqst Rte_Read_Keyfob_Mgr_KeyfobAuth_rqst_KeyfobAuth_rqst
#  define Rte_Read_Keyfob_Mgr_KeyfobAuth_rqst_KeyfobAuth_rqst(data) (*(data) = Rte_DriverAuthentication2_Ctrl_KeyfobAuth_rqst_KeyfobAuth_rqst, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst Rte_Read_Keyfob_Mgr_KeyfobInCabPresencePS_rqst_KeyfobInCabPresencePS_rqst
#  define Rte_Read_KeyfobLocation_rqst_KeyfobLocation_rqst Rte_Read_Keyfob_Mgr_KeyfobLocation_rqst_KeyfobLocation_rqst
#  define Rte_Read_Keyfob_Mgr_KeyfobLocation_rqst_KeyfobLocation_rqst(data) (*(data) = Rte_VehicleAccess_Ctrl_KeyfobLocation_rqst_KeyfobLocation_rqst, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Security_SwcActivation_Security Rte_Read_Keyfob_Mgr_SwcActivation_Security_SwcActivation_Security
#  define Rte_Read_Keyfob_Mgr_SwcActivation_Security_SwcActivation_Security(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Security_SwcActivation_Security, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_AddrParP1DS4_stat_dataP1DS4 Rte_Write_Keyfob_Mgr_AddrParP1DS4_stat_dataP1DS4
#  define Rte_Write_KeyfobAuth_stat_KeyfobAuth_stat Rte_Write_Keyfob_Mgr_KeyfobAuth_stat_KeyfobAuth_stat
#  define Rte_Write_Keyfob_Mgr_KeyfobAuth_stat_KeyfobAuth_stat(data) (Rte_Keyfob_Mgr_KeyfobAuth_stat_KeyfobAuth_stat = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat Rte_Write_Keyfob_Mgr_KeyfobInCabLocation_stat_KeyfobInCabLocation_stat
#  define Rte_Write_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle Rte_Write_Keyfob_Mgr_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle
#  define Rte_Write_Keyfob_Mgr_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle(data) (Rte_Keyfob_Mgr_KeyfobOutsideLocation_stat_KeyfobOutsideLocation_stat_Idle = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IMMO_APPL_CODE) GetImmoCircuitProcessingResult(P2VAR(SCIM_ImmoDriver_ProcessingStatus_T, AUTOMATIC, RTE_IMMO_APPL_VAR) ImmoProcessingStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_ImmoProcessingRqst_GetImmoCircuitProcessingResult(arg1) (GetImmoCircuitProcessingResult(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IMMO_APPL_CODE) ImmoCircuitProcessing(SCIM_ImmoType_T ImmoType, Boolean AuthRequestedbyImmo); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_ImmoProcessingRqst_ImmoCircuitProcessing(arg1, arg2) (ImmoCircuitProcessing(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IMMO_APPL_CODE) GetMatchingStatus(P2VAR(uint8, AUTOMATIC, RTE_IMMO_APPL_VAR) matchingStatus, P2VAR(uint8, AUTOMATIC, RTE_IMMO_APPL_VAR) matchedKeyfobCount); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_KeyfobMatchingOperations_GetMatchingStatus(arg1, arg2) (GetMatchingStatus(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IMMO_APPL_CODE) MatchKeyfobByLF(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_KeyfobMatchingOperations_MatchKeyfobByLF() (MatchKeyfobByLF(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IMMO_APPL_CODE) ReinitializeMatchingList(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IMMO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_KeyfobMatchingOperations_ReinitializeMatchingList() (ReinitializeMatchingList(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_PEPS_APPL_CODE) GetKeyfobPassiveSearchResult(P2VAR(SCIM_PassiveDriver_ProcessingStatus_T, AUTOMATIC, RTE_PEPS_APPL_VAR) ProcessingStatus, P2VAR(KeyfobInCabLocation_stat_T, AUTOMATIC, RTE_PEPS_APPL_VAR) KeyfobLocationbyPassive_Incab, P2VAR(KeyfobOutsideLocation_stat_T, AUTOMATIC, RTE_PEPS_APPL_VAR) KeyfobLocationbyPassive_Outcab); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_KeyfobPassiveSearchRqst_GetKeyfobPassiveSearchResult(arg1, arg2, arg3) (GetKeyfobPassiveSearchResult(arg1, arg2, arg3), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_PEPS_APPL_CODE) KeyfobPassiveSearch(SCIM_PassiveSearchCoverage_T PassiveSearchCoverage, boolean SearchRequestedbyPassive); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_KeyfobPassiveSearchRqst_KeyfobPassiveSearch(arg1, arg2) (KeyfobPassiveSearch(arg1, arg2), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchedKeyfobCount() \
  Rte_Irv_Keyfob_Mgr_P1B0T_MatchedKeyfobCount
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData_P1B0T_MatchingStatus() \
  Rte_Irv_Keyfob_Mgr_P1B0T_MatchingStatus
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF() \
  Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount() \
  Rte_Irv_Keyfob_Mgr_P1B0T_MatchedKeyfobCount
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus() \
  Rte_Irv_Keyfob_Mgr_P1B0T_MatchingStatus
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_FlagKeyfobMatchingByLF(data) \
  (Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchedKeyfobCount(data) \
  (Rte_Irv_Keyfob_Mgr_P1B0T_MatchedKeyfobCount = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_Keyfob_Mgr_20ms_runnable_P1B0T_MatchingStatus(data) \
  (Rte_Irv_Keyfob_Mgr_P1B0T_MatchingStatus = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF() \
  Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults_FlagKeyfobMatchingByLF(data) \
  (Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF() \
  Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start_FlagKeyfobMatchingByLF(data) \
  (Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF() \
  Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop_FlagKeyfobMatchingByLF(data) \
  (Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF = (data))
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_RoutineServices_Y1ABD_ClearECU_Start_FlagKeyfobMatchingByLF() \
  Rte_Irv_Keyfob_Mgr_FlagKeyfobMatchingByLF
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_KeyMatchingIndicationTimeout_X1CV3_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CV3_KeyMatchingIndicationTimeout_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CV4_KeyMatchingReinforcedAuth_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXE_isKeyfobSecurityFuseBlowAct_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1CV3_KeyMatchingIndicationTimeout_v() (Rte_AddrPar_0x29_X1CV3_KeyMatchingIndicationTimeout_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CV4_KeyMatchingReinforcedAuth_v() (Rte_AddrPar_0x29_X1CV4_KeyMatchingReinforcedAuth_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CXE_isKeyfobSecurityFuseBlowAct_v() (Rte_AddrPar_0x29_X1CXE_isKeyfobSecurityFuseBlowAct_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1B2U_KeyfobPresent_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_KeyfobEncryptCode_P1DS4_a_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1DS4_KeyfobEncryptCode_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1B2U_KeyfobPresent_v() (Rte_AddrPar_0x2B_P1B2U_KeyfobPresent_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_P1DS4_KeyfobEncryptCode_v() (&(Rte_AddrPar_0x2B_P1DS4_KeyfobEncryptCode_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_P1DS4_KeyfobEncryptCode_v() (&Rte_AddrPar_0x2B_P1DS4_KeyfobEncryptCode_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_CrankingLockActivation_P1DS3_T, RTE_CONST_SA_lvl_0x2D_and_0x37) Rte_AddrPar_0x2D_and_0x37_P1DS3_CrankingLockActivation_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2D_and_0x37_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1DS3_CrankingLockActivation_v() (Rte_AddrPar_0x2D_and_0x37_P1DS3_CrankingLockActivation_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1C54_FactoryModeActive_v() (Rte_AddrPar_0x2F_P1C54_FactoryModeActive_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define Keyfob_Mgr_START_SEC_CODE
# include "Keyfob_Mgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData
#  define RTE_RUNNABLE_DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData
#  define RTE_RUNNABLE_DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData
#  define RTE_RUNNABLE_DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData
#  define RTE_RUNNABLE_DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData
#  define RTE_RUNNABLE_Keyfob_Mgr_20ms_runnable Keyfob_Mgr_20ms_runnable
#  define RTE_RUNNABLE_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults
#  define RTE_RUNNABLE_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start
#  define RTE_RUNNABLE_RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop
#  define RTE_RUNNABLE_RoutineServices_Y1ABD_ClearECU_Start RoutineServices_Y1ABD_ClearECU_Start
# endif

# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1B0T_Data_P1B0T_KeyFobMatchingStatus_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1DS3_Data_P1DS3_CrankingLockActivation_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FDL_Data_P1FDL_KeyfobMatchingStatus_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1FM6_Data_P1FM6_KeyAuthentificationStatus_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_ReadData(P2VAR(Dcm_Data256ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1KAO_Data_P1KAO_NVRAMAreaTransfertData_WriteData(P2CONST(Dcm_Data256ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) Data, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) DataServices_P1VKK_Data_P1VKK_PassiveKeyfobAuthStatus_ReadData(P2VAR(Dcm_Data1ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(void, Keyfob_Mgr_CODE) Keyfob_Mgr_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_RequestResults(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Start(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data2ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_R1AAA_KeyfobMatchingRoutineByLF_Stop(Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_Y1ABD_ClearECU_Start(P2CONST(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(uint8, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, Keyfob_Mgr_CODE) RoutineServices_Y1ABD_ClearECU_Start(P2CONST(Dcm_Data4ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_DATA) In_Common_Diagnostics_DataRecord, Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data3ByteType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) Out_Common_Diagnostics_DataRecord, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_KEYFOB_MGR_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define Keyfob_Mgr_STOP_SEC_CODE
# include "Keyfob_Mgr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DataServices_P1B0T_Data_P1B0T_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1DS3_Data_P1DS3_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1FDL_Data_P1FDL_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1FM6_Data_P1FM6_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1KAO_Data_P1KAO_E_NOT_OK (1U)

#  define RTE_E_DataServices_P1VKK_Data_P1VKK_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_R1AAA_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_R1AAA_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_R1AAA_E_NOT_OK (1U)

#  define RTE_E_RoutineServices_Y1ABD_DCM_E_FORCE_RCRRP (12U)

#  define RTE_E_RoutineServices_Y1ABD_DCM_E_PENDING (10U)

#  define RTE_E_RoutineServices_Y1ABD_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_KEYFOB_MGR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
