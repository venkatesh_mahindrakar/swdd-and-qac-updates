/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_SCIM_Manager.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <SCIM_Manager>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_SCIM_MANAGER_H
# define _RTE_SCIM_MANAGER_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_SCIM_Manager_Type.h"
# include "Rte_DataHandleType.h"


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_ProvideEntropy(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) seedPtr, UInt32 seedLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(Std_ReturnType, RTE_VEC_SECURITYACCESS_APPL_CODE) VEC_SecurityAccess_ProvideEntropy(P2CONST(RandomSeedDataBuffer, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) seedPtr, UInt32 seedLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_VEC_SECURITYACCESS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_VEC_SecurityAccess_ProvideEntropy_ProvideEntropy VEC_SecurityAccess_ProvideEntropy


# endif /* !defined(RTE_CORE) */


# define SCIM_Manager_START_SEC_CODE
# include "SCIM_Manager_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_SCIM_Manager_10ms_runnable SCIM_Manager_10ms_runnable
#  define RTE_RUNNABLE_SCIM_Manager_EnterRun SCIM_Manager_EnterRun
# endif

FUNC(void, SCIM_Manager_CODE) SCIM_Manager_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, SCIM_Manager_CODE) SCIM_Manager_EnterRun(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define SCIM_Manager_STOP_SEC_CODE
# include "SCIM_Manager_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_VEC_SecurityAccess_ProvideEntropy_E_BUSY (2U)

#  define RTE_E_VEC_SecurityAccess_ProvideEntropy_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_SCIM_MANAGER_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
