/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_InteriorLightPanel_2_LINMastCtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <InteriorLightPanel_2_LINMastCtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_INTERIORLIGHTPANEL_2_LINMASTCTRL_H
# define _RTE_INTERIORLIGHTPANEL_2_LINMASTCTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_InteriorLightPanel_2_LINMastCtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtNightModeBtn2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtRestModeBtnPnl2_stat_PushButtonStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(ComMode_LIN_Type, RTE_VAR_NOINIT) Rte_LINMgr_ComMode_LIN4_ComMode_LIN; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_ComMode_LIN4_ComMode_LIN (7U)
#  define Rte_InitValue_DiagInfoILCP2_DiagInfo (0U)
#  define Rte_InitValue_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status (15U)
#  define Rte_InitValue_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtNightModeBtn2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_IntLghtRestModeBtnPnl2_stat_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_IntLghtCenterBtnFreeWhl_s_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status (15U)
#  define Rte_InitValue_LIN_IntLghtMaxModeBtnPnl2_s_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_IntLghtNightModeBt2_s_PushButtonStatus (3U)
#  define Rte_InitValue_LIN_IntLghtRestModeBtnPnl2_s_PushButtonStatus (3U)
#  define Rte_InitValue_ResponseErrorILCP2_ResponseErrorILCP2 (FALSE)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status(P2VAR(FreeWheel_Status_T, AUTOMATIC, RTE_INTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_InteriorLightPanel_2_LINMastCtrl_ResponseErrorILCP2_ResponseErrorILCP2(P2VAR(ResponseErrorILCP2_T, AUTOMATIC, RTE_INTERIORLIGHTPANEL_2_LINMASTCTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status(FreeWheel_Status_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_ComMode_LIN4_ComMode_LIN Rte_Read_InteriorLightPanel_2_LINMastCtrl_ComMode_LIN4_ComMode_LIN
#  define Rte_Read_InteriorLightPanel_2_LINMastCtrl_ComMode_LIN4_ComMode_LIN(data) (*(data) = Rte_LINMgr_ComMode_LIN4_ComMode_LIN, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_DiagInfoILCP2_DiagInfo Rte_Read_InteriorLightPanel_2_LINMastCtrl_DiagInfoILCP2_DiagInfo
#  define Rte_Read_InteriorLightPanel_2_LINMastCtrl_DiagInfoILCP2_DiagInfo(data) (Com_ReceiveSignal(ComConf_ComSignal_DiagInfoILCP2_oILCP2toCIOM_L4_oLIN03_df77e335_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_IntLghtCenterBtnFreeWhl_s_PushButtonStatus Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtCenterBtnFreeWhl_s_PushButtonStatus
#  define Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtCenterBtnFreeWhl_s_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_IntLghtCenterBtnFreeWhl_s_oILCP2toCIOM_L4_oLIN03_27a7dc09_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status
#  define Rte_Read_LIN_IntLghtMaxModeBtnPnl2_s_PushButtonStatus Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtMaxModeBtnPnl2_s_PushButtonStatus
#  define Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtMaxModeBtnPnl2_s_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_IntLghtMaxModeBtnPnl2_s_oILCP2toCIOM_L4_oLIN03_eeb895fc_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_IntLghtNightModeBt2_s_PushButtonStatus Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtNightModeBt2_s_PushButtonStatus
#  define Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtNightModeBt2_s_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_IntLghtNightModeBt2_s_oILCP2toCIOM_L4_oLIN03_c1a85f21_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_LIN_IntLghtRestModeBtnPnl2_s_PushButtonStatus Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtRestModeBtnPnl2_s_PushButtonStatus
#  define Rte_Read_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtRestModeBtnPnl2_s_PushButtonStatus(data) (Com_ReceiveSignal(ComConf_ComSignal_LIN_IntLghtRestModeBtnPnl2_s_oILCP2toCIOM_L4_oLIN03_46e1f77a_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ResponseErrorILCP2_ResponseErrorILCP2 Rte_Read_InteriorLightPanel_2_LINMastCtrl_ResponseErrorILCP2_ResponseErrorILCP2


/**********************************************************************************************************************
 * Rte_IsUpdated_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_IsUpdated_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status Rte_IsUpdated_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status
#  define Rte_IsUpdated_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status() ((Rte_RxUpdateFlags.Rte_RxUpdate_InteriorLightPanel_2_LINMastCtrl_LIN_IntLghtDimmingLvlFreeWhl_s_FreeWheel_Status == 1U) ? TRUE : FALSE) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus
#  define Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus(data) (Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtCenterBtnFreeWhl_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtDimmingLvlFreeWhl_stat_FreeWheel_Status
#  define Rte_Write_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus
#  define Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus(data) (Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtMaxModeBtnPnl2_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtNightModeBtn2_stat_PushButtonStatus Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtNightModeBtn2_stat_PushButtonStatus
#  define Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtNightModeBtn2_stat_PushButtonStatus(data) (Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtNightModeBtn2_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_IntLghtRestModeBtnPnl2_stat_PushButtonStatus Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtRestModeBtnPnl2_stat_PushButtonStatus
#  define Rte_Write_InteriorLightPanel_2_LINMastCtrl_IntLghtRestModeBtnPnl2_stat_PushButtonStatus(data) (Rte_InteriorLightPanel_2_LINMastCtrl_IntLghtRestModeBtnPnl2_stat_PushButtonStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_DEMSATELLITE_0_APPL_CODE) Dem_SetEventStatus(Dem_EventIdType parg0, Dem_EventStatusType EventStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_DEMSATELLITE_0_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_Event_D1BN4_16_ILCP_VBT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)221, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN4_17_ILCP_VAT_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)222, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN4_44_ILCP_RAM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)223, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN4_45_ILCP_FLASH_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)224, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN4_46_ILCP_EEPROM_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)225, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN4_49_ILCP_HWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)226, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1BN4_94_ILCP_SWFAIL_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)227, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_Event_D1F1F_87_ILCP2Link_NoResp_SetEventStatus(arg1) (Dem_SetEventStatus((Dem_EventIdType)392, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_ActivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_DimmingAdjustment2_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)15)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_ISSM_APPL_CODE) Issm_DeactivateIss(Issm_UserHandleType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_ISSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_UR_ANW_DimmingAdjustment2_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)15)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_OtherInteriorLights3_ActivateIss() (Issm_ActivateIss((Issm_UserHandleType)34)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Call_UR_ANW_OtherInteriorLights3_DeactivateIss() (Issm_DeactivateIss((Issm_UserHandleType)34)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VR2_ILCP2_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1VR2_ILCP2_Installed_v() (Rte_AddrPar_0x2B_P1VR2_ILCP2_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define InteriorLightPanel_2_LINMastCtrl_START_SEC_CODE
# include "InteriorLightPanel_2_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_InteriorLightPanel_2_LINMastCtrl_20ms_runnable InteriorLightPanel_2_LINMastCtrl_20ms_runnable
# endif

FUNC(void, InteriorLightPanel_2_LINMastCtrl_CODE) InteriorLightPanel_2_LINMastCtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define InteriorLightPanel_2_LINMastCtrl_STOP_SEC_CODE
# include "InteriorLightPanel_2_LINMastCtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DiagnosticMonitor_E_NOT_OK (1U)

#  define RTE_E_Issm_IssRequest_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_INTERIORLIGHTPANEL_2_LINMASTCTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
