/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_InternalDoorsAjar_Hdlr.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <InternalDoorsAjar_Hdlr>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_INTERNALDOORSAJAR_HDLR_H
# define _RTE_INTERNALDOORSAJAR_HDLR_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_InternalDoorsAjar_Hdlr_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DoorAjar_stat_T, RTE_VAR_NOINIT) Rte_InternalDoorsAjar_Hdlr_DriverDoorAjarInternal_stat_DoorAjar_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(DoorAjar_stat_T, RTE_VAR_NOINIT) Rte_InternalDoorsAjar_Hdlr_PsngrDoorAjarInternal_stat_DoorAjar_stat; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Parked_Parked; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_DriverDoorAjarInternal_stat_DoorAjar_stat (7U)
#  define Rte_InitValue_PsngrDoorAjarInternal_stat_DoorAjar_stat (7U)
#  define Rte_InitValue_SwcActivation_Parked_Parked (1U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_SwcActivation_Parked_Parked Rte_Read_InternalDoorsAjar_Hdlr_SwcActivation_Parked_Parked
#  define Rte_Read_InternalDoorsAjar_Hdlr_SwcActivation_Parked_Parked(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Parked_Parked, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_DriverDoorAjarInternal_stat_DoorAjar_stat Rte_Write_InternalDoorsAjar_Hdlr_DriverDoorAjarInternal_stat_DoorAjar_stat
#  define Rte_Write_InternalDoorsAjar_Hdlr_DriverDoorAjarInternal_stat_DoorAjar_stat(data) (Rte_InternalDoorsAjar_Hdlr_DriverDoorAjarInternal_stat_DoorAjar_stat = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Write_PsngrDoorAjarInternal_stat_DoorAjar_stat Rte_Write_InternalDoorsAjar_Hdlr_PsngrDoorAjarInternal_stat_DoorAjar_stat
#  define Rte_Write_InternalDoorsAjar_Hdlr_PsngrDoorAjarInternal_stat_DoorAjar_stat(data) (Rte_InternalDoorsAjar_Hdlr_PsngrDoorAjarInternal_stat_DoorAjar_stat = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_QM_IO_APPL_CODE) AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) AdiPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_QM_IO_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_AdiInterface_P_GetAdiPinState_CS AdiInterface_P_GetAdiPinState_CS


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v() (&Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_LockFunctionHardwareInterface_P1MXZ_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1MXZ_LockFunctionHardwareInterface_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1MXZ_LockFunctionHardwareInterface_v() (Rte_AddrPar_0x2B_P1MXZ_LockFunctionHardwareInterface_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define InternalDoorsAjar_Hdlr_START_SEC_CODE
# include "InternalDoorsAjar_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_InternalDoorsAjar_Hdlr_20ms_runnable InternalDoorsAjar_Hdlr_20ms_runnable
# endif

FUNC(void, InternalDoorsAjar_Hdlr_CODE) InternalDoorsAjar_Hdlr_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define InternalDoorsAjar_Hdlr_STOP_SEC_CODE
# include "InternalDoorsAjar_Hdlr_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_AdiInterface_I_IoHwAbApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_INTERNALDOORSAJAR_HDLR_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
