/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_EconomyPower_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <EconomyPower_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_ECONOMYPOWER_HMICTRL_H
# define _RTE_ECONOMYPOWER_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_EconomyPower_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

#  include "Com.h"


/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DeviceIndication_T, RTE_VAR_NOINIT) Rte_EconomyPower_HMICtrl_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(PushButtonStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_EconomyPowerSwitch_status_EconomyPowerSwitch_status; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_EcoBalancedSwitch_stat_EcoBalancedSwitch_stat (3U)
#  define Rte_InitValue_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd (3U)
#  define Rte_InitValue_EconomyPowerSwitch_status_EconomyPowerSwitch_status (3U)
#  define Rte_InitValue_SwcActivation_IgnitionOn_IgnitionOn (1U)
#  define Rte_InitValue_TransmissionDrivingMode_TransmissionDrivingMode (7U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EconomyPower_HMICtrl_DrivingMode_DrivingMode(P2VAR(uint8, AUTOMATIC, RTE_ECONOMYPOWER_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, RTE_CODE) Rte_Read_EconomyPower_HMICtrl_DrivingMode_DrivingMode(P2VAR(DrivingMode_T, AUTOMATIC, RTE_ECONOMYPOWER_HMICTRL_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
FUNC(Std_ReturnType, RTE_CODE) Rte_Write_EconomyPower_HMICtrl_EcoBalancedSwitch_stat_EcoBalancedSwitch_stat(OffOn_T data); /* PRQA S 0850 */ /* MD_MSR_19.8 */

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_DrivingMode_DrivingMode Rte_Read_EconomyPower_HMICtrl_DrivingMode_DrivingMode
#  define Rte_Read_EconomyPowerSwitch_status_EconomyPowerSwitch_status Rte_Read_EconomyPower_HMICtrl_EconomyPowerSwitch_status_EconomyPowerSwitch_status
#  define Rte_Read_EconomyPower_HMICtrl_EconomyPowerSwitch_status_EconomyPowerSwitch_status(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_EconomyPowerSwitch_status_EconomyPowerSwitch_status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_IgnitionOn_IgnitionOn Rte_Read_EconomyPower_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn
#  define Rte_Read_EconomyPower_HMICtrl_SwcActivation_IgnitionOn_IgnitionOn(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_IgnitionOn_IgnitionOn, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_TransmissionDrivingMode_TransmissionDrivingMode Rte_Read_EconomyPower_HMICtrl_TransmissionDrivingMode_TransmissionDrivingMode
#  define Rte_Read_EconomyPower_HMICtrl_TransmissionDrivingMode_TransmissionDrivingMode(data) (Com_ReceiveSignal(ComConf_ComSignal_TransmissionDrivingMode_ISig_4_oTECU_BB2_02P_oBackbone2_e990eb27_Rx, (data))) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_EcoBalancedSwitch_stat_EcoBalancedSwitch_stat Rte_Write_EconomyPower_HMICtrl_EcoBalancedSwitch_stat_EcoBalancedSwitch_stat
#  define Rte_Write_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd Rte_Write_EconomyPower_HMICtrl_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd
#  define Rte_Write_EconomyPower_HMICtrl_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd(data) (Rte_EconomyPower_HMICtrl_EconomyPowerSwitch_DeviceInd_EconomyPowerSwitch_DeviceInd = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1VQ0_FuelEcoOffButton_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1VQ0_FuelEcoOffButton_v() (Rte_AddrPar_0x2B_P1VQ0_FuelEcoOffButton_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define EconomyPower_HMICtrl_START_SEC_CODE
# include "EconomyPower_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_EconomyPower_HMICtrl_20ms_runnable EconomyPower_HMICtrl_20ms_runnable
# endif

FUNC(void, EconomyPower_HMICtrl_CODE) EconomyPower_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define EconomyPower_HMICtrl_STOP_SEC_CODE
# include "EconomyPower_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_ECONOMYPOWER_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
