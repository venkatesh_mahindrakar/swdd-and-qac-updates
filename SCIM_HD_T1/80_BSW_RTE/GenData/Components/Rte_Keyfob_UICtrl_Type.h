/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_Keyfob_UICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <Keyfob_UICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_KEYFOB_UICTRL_TYPE_H
# define _RTE_KEYFOB_UICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DeactivateActivate_Deactivate
#   define DeactivateActivate_Deactivate (0U)
#  endif

#  ifndef DeactivateActivate_Activate
#   define DeactivateActivate_Activate (1U)
#  endif

#  ifndef DeactivateActivate_Error
#   define DeactivateActivate_Error (2U)
#  endif

#  ifndef DeactivateActivate_NotAvailable
#   define DeactivateActivate_NotAvailable (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

#  ifndef ISSM_STATE_INACTIVE
#   define ISSM_STATE_INACTIVE (0U)
#  endif

#  ifndef ISSM_STATE_PENDING
#   define ISSM_STATE_PENDING (1U)
#  endif

#  ifndef ISSM_STATE_ACTIVE
#   define ISSM_STATE_ACTIVE (2U)
#  endif

#  ifndef KeyfobPanicButton_Status_Neutral
#   define KeyfobPanicButton_Status_Neutral (0U)
#  endif

#  ifndef KeyfobPanicButton_Status_DoublePress
#   define KeyfobPanicButton_Status_DoublePress (1U)
#  endif

#  ifndef KeyfobPanicButton_Status_LongPress
#   define KeyfobPanicButton_Status_LongPress (2U)
#  endif

#  ifndef KeyfobPanicButton_Status_Spare_01
#   define KeyfobPanicButton_Status_Spare_01 (3U)
#  endif

#  ifndef KeyfobPanicButton_Status_Spare_02
#   define KeyfobPanicButton_Status_Spare_02 (4U)
#  endif

#  ifndef KeyfobPanicButton_Status_Spare_03
#   define KeyfobPanicButton_Status_Spare_03 (5U)
#  endif

#  ifndef KeyfobPanicButton_Status_Error
#   define KeyfobPanicButton_Status_Error (6U)
#  endif

#  ifndef KeyfobPanicButton_Status_NotAvailable
#   define KeyfobPanicButton_Status_NotAvailable (7U)
#  endif

#  ifndef NotDetected_NotDetected
#   define NotDetected_NotDetected (0U)
#  endif

#  ifndef NotDetected_Detected
#   define NotDetected_Detected (1U)
#  endif

#  ifndef NotDetected_Error
#   define NotDetected_Error (2U)
#  endif

#  ifndef NotDetected_NotAvaliable
#   define NotDetected_NotAvaliable (3U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

#  ifndef VehicleMode_Hibernate
#   define VehicleMode_Hibernate (0U)
#  endif

#  ifndef VehicleMode_Parked
#   define VehicleMode_Parked (1U)
#  endif

#  ifndef VehicleMode_Living
#   define VehicleMode_Living (2U)
#  endif

#  ifndef VehicleMode_Accessory
#   define VehicleMode_Accessory (3U)
#  endif

#  ifndef VehicleMode_PreRunning
#   define VehicleMode_PreRunning (4U)
#  endif

#  ifndef VehicleMode_Cranking
#   define VehicleMode_Cranking (5U)
#  endif

#  ifndef VehicleMode_Running
#   define VehicleMode_Running (6U)
#  endif

#  ifndef VehicleMode_Spare_1
#   define VehicleMode_Spare_1 (7U)
#  endif

#  ifndef VehicleMode_Spare_2
#   define VehicleMode_Spare_2 (8U)
#  endif

#  ifndef VehicleMode_Spare_3
#   define VehicleMode_Spare_3 (9U)
#  endif

#  ifndef VehicleMode_Spare_4
#   define VehicleMode_Spare_4 (10U)
#  endif

#  ifndef VehicleMode_Spare_5
#   define VehicleMode_Spare_5 (11U)
#  endif

#  ifndef VehicleMode_Spare_6
#   define VehicleMode_Spare_6 (12U)
#  endif

#  ifndef VehicleMode_Spare_7
#   define VehicleMode_Spare_7 (13U)
#  endif

#  ifndef VehicleMode_Error
#   define VehicleMode_Error (14U)
#  endif

#  ifndef VehicleMode_NotAvailable
#   define VehicleMode_NotAvailable (15U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_KEYFOB_UICTRL_TYPE_H */
