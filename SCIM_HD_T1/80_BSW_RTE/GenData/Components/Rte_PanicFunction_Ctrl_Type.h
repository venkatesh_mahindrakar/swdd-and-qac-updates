/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_PanicFunction_Ctrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <PanicFunction_Ctrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_PANICFUNCTION_CTRL_TYPE_H
# define _RTE_PANICFUNCTION_CTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef KeyfobPanicButton_Status_Neutral
#   define KeyfobPanicButton_Status_Neutral (0U)
#  endif

#  ifndef KeyfobPanicButton_Status_DoublePress
#   define KeyfobPanicButton_Status_DoublePress (1U)
#  endif

#  ifndef KeyfobPanicButton_Status_LongPress
#   define KeyfobPanicButton_Status_LongPress (2U)
#  endif

#  ifndef KeyfobPanicButton_Status_Spare_01
#   define KeyfobPanicButton_Status_Spare_01 (3U)
#  endif

#  ifndef KeyfobPanicButton_Status_Spare_02
#   define KeyfobPanicButton_Status_Spare_02 (4U)
#  endif

#  ifndef KeyfobPanicButton_Status_Spare_03
#   define KeyfobPanicButton_Status_Spare_03 (5U)
#  endif

#  ifndef KeyfobPanicButton_Status_Error
#   define KeyfobPanicButton_Status_Error (6U)
#  endif

#  ifndef KeyfobPanicButton_Status_NotAvailable
#   define KeyfobPanicButton_Status_NotAvailable (7U)
#  endif

#  ifndef Request_NotRequested
#   define Request_NotRequested (0U)
#  endif

#  ifndef Request_RequestActive
#   define Request_RequestActive (1U)
#  endif

#  ifndef Request_Error
#   define Request_Error (2U)
#  endif

#  ifndef Request_NotAvailable
#   define Request_NotAvailable (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_PANICFUNCTION_CTRL_TYPE_H */
