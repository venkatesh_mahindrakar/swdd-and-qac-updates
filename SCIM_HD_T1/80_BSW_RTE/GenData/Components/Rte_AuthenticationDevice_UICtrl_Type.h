/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_AuthenticationDevice_UICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <AuthenticationDevice_UICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_AUTHENTICATIONDEVICE_UICTRL_TYPE_H
# define _RTE_AUTHENTICATIONDEVICE_UICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef ButtonAuth_rqst_Idle
#   define ButtonAuth_rqst_Idle (0U)
#  endif

#  ifndef ButtonAuth_rqst_RequestDeviceAuthentication
#   define ButtonAuth_rqst_RequestDeviceAuthentication (1U)
#  endif

#  ifndef ButtonAuth_rqst_Error
#   define ButtonAuth_rqst_Error (2U)
#  endif

#  ifndef ButtonAuth_rqst_NotAvailable
#   define ButtonAuth_rqst_NotAvailable (3U)
#  endif

#  ifndef DeviceAuthentication_rqst_Idle
#   define DeviceAuthentication_rqst_Idle (0U)
#  endif

#  ifndef DeviceAuthentication_rqst_DeviceAuthenticationRequest
#   define DeviceAuthentication_rqst_DeviceAuthenticationRequest (1U)
#  endif

#  ifndef DeviceAuthentication_rqst_DeviceDeauthenticationRequest
#   define DeviceAuthentication_rqst_DeviceDeauthenticationRequest (2U)
#  endif

#  ifndef DeviceAuthentication_rqst_DeviceMatching
#   define DeviceAuthentication_rqst_DeviceMatching (3U)
#  endif

#  ifndef DeviceAuthentication_rqst_Spare1
#   define DeviceAuthentication_rqst_Spare1 (4U)
#  endif

#  ifndef DeviceAuthentication_rqst_Spare2
#   define DeviceAuthentication_rqst_Spare2 (5U)
#  endif

#  ifndef DeviceAuthentication_rqst_Error
#   define DeviceAuthentication_rqst_Error (6U)
#  endif

#  ifndef DeviceAuthentication_rqst_NotAvailable
#   define DeviceAuthentication_rqst_NotAvailable (7U)
#  endif

#  ifndef DoorsAjar_stat_Idle
#   define DoorsAjar_stat_Idle (0U)
#  endif

#  ifndef DoorsAjar_stat_BothDoorsAreClosed
#   define DoorsAjar_stat_BothDoorsAreClosed (1U)
#  endif

#  ifndef DoorsAjar_stat_DriverDoorIsOpen
#   define DoorsAjar_stat_DriverDoorIsOpen (2U)
#  endif

#  ifndef DoorsAjar_stat_PassengerDoorIsOpen
#   define DoorsAjar_stat_PassengerDoorIsOpen (3U)
#  endif

#  ifndef DoorsAjar_stat_BothDoorsAreOpen
#   define DoorsAjar_stat_BothDoorsAreOpen (4U)
#  endif

#  ifndef DoorsAjar_stat_Spare
#   define DoorsAjar_stat_Spare (5U)
#  endif

#  ifndef DoorsAjar_stat_Error
#   define DoorsAjar_stat_Error (6U)
#  endif

#  ifndef DoorsAjar_stat_NotAvailable
#   define DoorsAjar_stat_NotAvailable (7U)
#  endif

#  ifndef DriverAuthDeviceMatching_Idle
#   define DriverAuthDeviceMatching_Idle (0U)
#  endif

#  ifndef DriverAuthDeviceMatching_DeviceToMatchIsPresent
#   define DriverAuthDeviceMatching_DeviceToMatchIsPresent (1U)
#  endif

#  ifndef DriverAuthDeviceMatching_Error
#   define DriverAuthDeviceMatching_Error (2U)
#  endif

#  ifndef DriverAuthDeviceMatching_NotAvailable
#   define DriverAuthDeviceMatching_NotAvailable (3U)
#  endif

#  ifndef KeyPosition_KeyOut
#   define KeyPosition_KeyOut (0U)
#  endif

#  ifndef KeyPosition_IgnitionKeyInOffPosition
#   define KeyPosition_IgnitionKeyInOffPosition (1U)
#  endif

#  ifndef KeyPosition_IgnitionKeyInAccessoryPosition
#   define KeyPosition_IgnitionKeyInAccessoryPosition (2U)
#  endif

#  ifndef KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition
#   define KeyPosition_IgnitionKeyIn15PositionNormalDrivingPosition (3U)
#  endif

#  ifndef KeyPosition_IgnitionKeyInPreheatPosition
#   define KeyPosition_IgnitionKeyInPreheatPosition (4U)
#  endif

#  ifndef KeyPosition_IgnitionKeyInCrankPosition
#   define KeyPosition_IgnitionKeyInCrankPosition (5U)
#  endif

#  ifndef KeyPosition_ErrorIndicator
#   define KeyPosition_ErrorIndicator (6U)
#  endif

#  ifndef KeyPosition_NotAvailable
#   define KeyPosition_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_AUTHENTICATIONDEVICE_UICTRL_TYPE_H */
