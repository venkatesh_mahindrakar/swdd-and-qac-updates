/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DoubleSwitch_HMICtrl.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <DoubleSwitch_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DOUBLESWITCH_HMICTRL_H
# define _RTE_DOUBLESWITCH_HMICTRL_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_DoubleSwitch_HMICtrl_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_DoubleSwitch_HMICtrl_SwitchStatus_combined_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_1_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(A3PosSwitchStatus_T, RTE_VAR_NOINIT) Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_2_A3PosSwitchStatus; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleModeDistribution_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_SwcActivation_Living_Living; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_RoofHatch_SwitchStatus_1_A3PosSwitchStatus (7U)
#  define Rte_InitValue_RoofHatch_SwitchStatus_2_A3PosSwitchStatus (7U)
#  define Rte_InitValue_SwcActivation_Living_Living (1U)
#  define Rte_InitValue_SwitchStatus_combined_A3PosSwitchStatus (7U)
# endif


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_RoofHatch_SwitchStatus_1_A3PosSwitchStatus Rte_Read_DoubleSwitch_HMICtrl_RoofHatch_SwitchStatus_1_A3PosSwitchStatus
#  define Rte_Read_DoubleSwitch_HMICtrl_RoofHatch_SwitchStatus_1_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_1_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_RoofHatch_SwitchStatus_2_A3PosSwitchStatus Rte_Read_DoubleSwitch_HMICtrl_RoofHatch_SwitchStatus_2_A3PosSwitchStatus
#  define Rte_Read_DoubleSwitch_HMICtrl_RoofHatch_SwitchStatus_2_A3PosSwitchStatus(data) (*(data) = Rte_FlexibleSwitchesRouter_Ctrl_RoofHatch_SwitchStatus_2_A3PosSwitchStatus, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_SwcActivation_Living_Living Rte_Read_DoubleSwitch_HMICtrl_SwcActivation_Living_Living
#  define Rte_Read_DoubleSwitch_HMICtrl_SwcActivation_Living_Living(data) (*(data) = Rte_VehicleModeDistribution_SwcActivation_Living_Living, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Write_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Write_SwitchStatus_combined_A3PosSwitchStatus Rte_Write_DoubleSwitch_HMICtrl_SwitchStatus_combined_A3PosSwitchStatus
#  define Rte_Write_DoubleSwitch_HMICtrl_SwitchStatus_combined_A3PosSwitchStatus(data) (Rte_DoubleSwitch_HMICtrl_SwitchStatus_combined_A3PosSwitchStatus = (data), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define DoubleSwitch_HMICtrl_START_SEC_CODE
# include "DoubleSwitch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_DoubleSwitch_HMICtrl_20ms_runnable DoubleSwitch_HMICtrl_20ms_runnable
# endif

FUNC(void, DoubleSwitch_HMICtrl_CODE) DoubleSwitch_HMICtrl_20ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define DoubleSwitch_HMICtrl_STOP_SEC_CODE
# include "DoubleSwitch_HMICtrl_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DOUBLESWITCH_HMICTRL_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
