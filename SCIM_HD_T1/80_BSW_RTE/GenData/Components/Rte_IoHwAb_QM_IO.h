/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_IoHwAb_QM_IO.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <IoHwAb_QM_IO>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_IOHWAB_QM_IO_H
# define _RTE_IOHWAB_QM_IO_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_IoHwAb_QM_IO_Type.h"
# include "Rte_DataHandleType.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * extern declaration of RTE buffers for optimized macro implementation
 *********************************************************************************************************************/
#  define RTE_START_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern VAR(DiagActiveState_T, RTE_VAR_NOINIT) Rte_DiagnosticComponent_DiagActiveState_isDiagActive; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(Fsc_OperationalMode_T, RTE_VAR_NOINIT) Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(uint8, RTE_VAR_NOINIT) Rte_SCIM_PVTPT_IO_ScimPvtControl_P_Status; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern VAR(VehicleMode_T, RTE_VAR_NOINIT) Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#  define RTE_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

# endif /* !defined(RTE_CORE) */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Init Values for unqueued S/R communication (primitive types only)
 *********************************************************************************************************************/

#  define Rte_InitValue_DiagActiveState_P_isDiagActive (0U)
#  define Rte_InitValue_Fsc_OperationalMode_P_Fsc_OperationalMode (6U)
#  define Rte_InitValue_ScimPvtControl_P_Status (0U)
#  define Rte_InitValue_VehicleModeInternal_VehicleMode (15U)
# endif


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(P2VAR(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(P2VAR(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(P2VAR(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvRead_IoHwAb_QM_IO_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(P2VAR(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_QM_IO_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(P2CONST(Rte_DT_EcuHwDioCtrlArray_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_QM_IO_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(P2CONST(EcuHwDioCtrlArray_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_QM_IO_IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus(P2CONST(Rte_DT_EcuHwFaultValues_T_0, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(void, RTE_CODE) Rte_IrvWrite_IoHwAb_QM_IO_IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus(P2CONST(EcuHwFaultValues_T, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_DATA) data); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Read_<p>_<d> (explicit S/R communication with isQueued = false)
 *********************************************************************************************************************/
#  define Rte_Read_DiagActiveState_P_isDiagActive Rte_Read_IoHwAb_QM_IO_DiagActiveState_P_isDiagActive
#  define Rte_Read_IoHwAb_QM_IO_DiagActiveState_P_isDiagActive(data) (*(data) = Rte_DiagnosticComponent_DiagActiveState_isDiagActive, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_Fsc_OperationalMode_P_Fsc_OperationalMode Rte_Read_IoHwAb_QM_IO_Fsc_OperationalMode_P_Fsc_OperationalMode
#  define Rte_Read_IoHwAb_QM_IO_Fsc_OperationalMode_P_Fsc_OperationalMode(data) (*(data) = Rte_IoHwAb_ASIL_Core_Fsc_OperationalMode_P_Fsc_OperationalMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_ScimPvtControl_P_Status Rte_Read_IoHwAb_QM_IO_ScimPvtControl_P_Status
#  define Rte_Read_IoHwAb_QM_IO_ScimPvtControl_P_Status(data) (*(data) = Rte_SCIM_PVTPT_IO_ScimPvtControl_P_Status, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define Rte_Read_VehicleModeInternal_VehicleMode Rte_Read_IoHwAb_QM_IO_VehicleModeInternal_VehicleMode
#  define Rte_Read_IoHwAb_QM_IO_VehicleModeInternal_VehicleMode(data) (*(data) = Rte_VehicleModeDistribution_VehicleModeInternal_VehicleMode, ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_CORE_APPL_CODE) EcuHwState_P_GetEcuVoltages_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) EcuVoltageValues); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_CORE_APPL_CODE) EcuHwState_P_GetEcuVoltages_CS(P2VAR(EcuHwVoltageValues_T, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) EcuVoltageValues); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_EcuHwState_P_GetEcuVoltages_CS EcuHwState_P_GetEcuVoltages_CS
#  define RTE_START_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_IOHWAB_ASIL_CORE_APPL_CODE) VbatInterface_P_GetVbatVoltage_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_ASIL_CORE_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_ASIL_CORE_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_VbatInterface_P_GetVbatVoltage_CS VbatInterface_P_GetVbatVoltage_CS


/**********************************************************************************************************************
 * Inter-runnable variables
 *********************************************************************************************************************/

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetAdiPinState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus(data) \
  Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetAdiPinState_CS_IrvEcuIoQmFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetPullUpState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus(data) \
  Rte_IrvRead_IoHwAb_QM_IO_AdiInterface_P_GetPullUpState_CS_IrvEcuIoQmFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus(data) \
  Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDcdc12VState_CS_IrvEcuIoQmFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(data) \
  Rte_IrvRead_IoHwAb_QM_IO_Do12VInterface_P_GetDo12VPinsState_CS_IrvEcuIoQmFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_QM_IO_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus(data) \
  Rte_IrvRead_IoHwAb_QM_IO_DoblsCtrlInterface_P_GetDoblsPinState_CS_IrvEcuIoQmFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_QM_IO_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(data) \
  Rte_IrvRead_IoHwAb_QM_IO_DowhsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_QM_IO_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(data) \
  Rte_IrvRead_IoHwAb_QM_IO_DowlsInterface_P_GetDoPinStateOne_CS_IrvEcuIoQmFaultStatus(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvRead_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvRead_IoHwAb_QM_IO_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(data) \
  Rte_IrvWrite_IoHwAb_QM_IO_IoHwAb_QM_IO_10ms_runnable_IrvEcuHwDioCtrlArray(data)
/* PRQA L:L1 */

/* PRQA S 3453 L1 */ /* MD_MSR_19.7 */
#  define Rte_IrvWrite_IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus(data) \
  Rte_IrvWrite_IoHwAb_QM_IO_IoHwAb_QM_IO_10ms_runnable_IrvEcuIoQmFaultStatus(data)
/* PRQA L:L1 */


/**********************************************************************************************************************
 * Rte_Prm (Calibration component calibration parameters)
 *********************************************************************************************************************/

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_HwToleranceThreshold_X1C04_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DoorAccessIf_X1CX3_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX3_PcbConfig_DoorAccessIf_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_LinInterfaces_X1CX0_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_CanInterfaces_X1CX2_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX2_PcbConfig_CanInterfaces_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_Adi_X1CXW_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOWHS_X1CXY_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_DOWLS_X1CXZ_a_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_PassiveAntenna_X1CX4_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX4_PcbConfig_PassiveAntenna_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_PcbConfig_AdiPullUp_X1CX5_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CX5_PcbConfig_AdiPullUp_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DigitalBiLevelVoltageConfig_X1CY1_s_T, RTE_CONST_SA_lvl_0x29) Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x29_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_X1C04_HwToleranceThreshold_v() (Rte_AddrPar_0x29_X1C04_HwToleranceThreshold_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CX3_PcbConfig_DoorAccessIf_v() (Rte_AddrPar_0x29_X1CX3_PcbConfig_DoorAccessIf_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v() (&(Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CX0_PcbConfig_LinInterfaces_v() (&Rte_AddrPar_0x29_X1CX0_PcbConfig_LinInterfaces_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v() (&(Rte_AddrPar_0x29_X1CX2_PcbConfig_CanInterfaces_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CX2_PcbConfig_CanInterfaces_v() (&Rte_AddrPar_0x29_X1CX2_PcbConfig_CanInterfaces_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXW_PcbConfig_Adi_v() (&(Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXW_PcbConfig_Adi_v() (&Rte_AddrPar_0x29_X1CXW_PcbConfig_Adi_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXY_PcbConfig_DOWHS_v() (&(Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXY_PcbConfig_DOWHS_v() (&Rte_AddrPar_0x29_X1CXY_PcbConfig_DOWHS_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_X1CXZ_PcbConfig_DOWLS_v() (&(Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_X1CXZ_PcbConfig_DOWLS_v() (&Rte_AddrPar_0x29_X1CXZ_PcbConfig_DOWLS_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  define Rte_Prm_X1CX4_PcbConfig_PassiveAntenna_v() (&Rte_AddrPar_0x29_X1CX4_PcbConfig_PassiveAntenna_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CX5_PcbConfig_AdiPullUp_v() (&Rte_AddrPar_0x29_X1CX5_PcbConfig_AdiPullUp_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_X1CY1_DigitalBiLevelVoltageConfig_v() (&Rte_AddrPar_0x29_X1CY1_DigitalBiLevelVoltageConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(SEWS_Diag_Act_DOWHS01_P1V6O_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6O_Diag_Act_DOWHS01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWHS02_P1V6P_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6P_Diag_Act_DOWHS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWLS02_P1V7E_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7E_Diag_Act_DOWLS02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Diag_Act_DOWLS03_P1V7F_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V7F_Diag_Act_DOWLS03_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WME_LowPowerPullUpAct_Parked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMF_LowPowerPullUpAct_Living_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMN_LowPower12VOutputAct_Living_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(boolean, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMO_LowPower12VOutputAct_Parked_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_AdiWakeUpConfig_P1WMD_a_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMD_AdiWakeUpConfig_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI07_P1V60_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V60_Fault_Config_ADI07_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI08_P1V61_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V61_Fault_Config_ADI08_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI09_P1V62_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V62_Fault_Config_ADI09_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI10_P1V63_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V63_Fault_Config_ADI10_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI11_P1V64_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V64_Fault_Config_ADI11_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI12_P1V65_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V65_Fault_Config_ADI12_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI13_P1V66_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V66_Fault_Config_ADI13_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI14_P1V67_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V67_Fault_Config_ADI14_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI15_P1V68_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V68_Fault_Config_ADI15_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI16_P1V69_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V69_Fault_Config_ADI16_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI01_P1V6U_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6U_Fault_Config_ADI01_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI02_P1V6V_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6V_Fault_Config_ADI02_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI03_P1V6W_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6W_Fault_Config_ADI03_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI04_P1V6X_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6X_Fault_Config_ADI04_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI05_P1V6Y_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6Y_Fault_Config_ADI05_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Config_ADI06_P1V6Z_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V6Z_Fault_Config_ADI06_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_Fault_Cfg_DcDc12v_P1V8F_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1V8F_Fault_Cfg_DcDc12v_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */
extern CONST(SEWS_DAI_Installed_P1WMP_s_T, RTE_CONST_SA_lvl_0x2B) Rte_AddrPar_0x2B_P1WMP_DAI_Installed_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2B_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1V6O_Diag_Act_DOWHS01_v() (Rte_AddrPar_0x2B_P1V6O_Diag_Act_DOWHS01_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6P_Diag_Act_DOWHS02_v() (Rte_AddrPar_0x2B_P1V6P_Diag_Act_DOWHS02_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V7E_Diag_Act_DOWLS02_v() (Rte_AddrPar_0x2B_P1V7E_Diag_Act_DOWLS02_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V7F_Diag_Act_DOWLS03_v() (Rte_AddrPar_0x2B_P1V7F_Diag_Act_DOWLS03_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WME_LowPowerPullUpAct_Parked_v() (Rte_AddrPar_0x2B_P1WME_LowPowerPullUpAct_Parked_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WMF_LowPowerPullUpAct_Living_v() (Rte_AddrPar_0x2B_P1WMF_LowPowerPullUpAct_Living_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WMN_LowPower12VOutputAct_Living_v() (Rte_AddrPar_0x2B_P1WMN_LowPower12VOutputAct_Living_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WMO_LowPower12VOutputAct_Parked_v() (Rte_AddrPar_0x2B_P1WMO_LowPower12VOutputAct_Parked_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
#   define Rte_Prm_P1WMD_AdiWakeUpConfig_v() (&(Rte_AddrPar_0x2B_P1WMD_AdiWakeUpConfig_v[0])) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  else
#   define Rte_Prm_P1WMD_AdiWakeUpConfig_v() (&Rte_AddrPar_0x2B_P1WMD_AdiWakeUpConfig_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  endif

#  define Rte_Prm_P1V60_Fault_Config_ADI07_v() (&Rte_AddrPar_0x2B_P1V60_Fault_Config_ADI07_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V61_Fault_Config_ADI08_v() (&Rte_AddrPar_0x2B_P1V61_Fault_Config_ADI08_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V62_Fault_Config_ADI09_v() (&Rte_AddrPar_0x2B_P1V62_Fault_Config_ADI09_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V63_Fault_Config_ADI10_v() (&Rte_AddrPar_0x2B_P1V63_Fault_Config_ADI10_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V64_Fault_Config_ADI11_v() (&Rte_AddrPar_0x2B_P1V64_Fault_Config_ADI11_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V65_Fault_Config_ADI12_v() (&Rte_AddrPar_0x2B_P1V65_Fault_Config_ADI12_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V66_Fault_Config_ADI13_v() (&Rte_AddrPar_0x2B_P1V66_Fault_Config_ADI13_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V67_Fault_Config_ADI14_v() (&Rte_AddrPar_0x2B_P1V67_Fault_Config_ADI14_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V68_Fault_Config_ADI15_v() (&Rte_AddrPar_0x2B_P1V68_Fault_Config_ADI15_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V69_Fault_Config_ADI16_v() (&Rte_AddrPar_0x2B_P1V69_Fault_Config_ADI16_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6U_Fault_Config_ADI01_v() (&Rte_AddrPar_0x2B_P1V6U_Fault_Config_ADI01_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6V_Fault_Config_ADI02_v() (&Rte_AddrPar_0x2B_P1V6V_Fault_Config_ADI02_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6W_Fault_Config_ADI03_v() (&Rte_AddrPar_0x2B_P1V6W_Fault_Config_ADI03_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6X_Fault_Config_ADI04_v() (&Rte_AddrPar_0x2B_P1V6X_Fault_Config_ADI04_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6Y_Fault_Config_ADI05_v() (&Rte_AddrPar_0x2B_P1V6Y_Fault_Config_ADI05_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V6Z_Fault_Config_ADI06_v() (&Rte_AddrPar_0x2B_P1V6Z_Fault_Config_ADI06_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1V8F_Fault_Cfg_DcDc12v_v() (&Rte_AddrPar_0x2B_P1V8F_Fault_Cfg_DcDc12v_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  define Rte_Prm_P1WMP_DAI_Installed_v() (&Rte_AddrPar_0x2B_P1WMP_DAI_Installed_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

#  ifndef RTE_MICROSAR_PIM_EXPORT
#   define RTE_START_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

extern CONST(boolean, RTE_CONST_SA_lvl_0x2F) Rte_AddrPar_0x2F_P1WPP_isSecurityLinActive_v; /* PRQA S 0850 */ /* MD_MSR_19.8 */

#   define RTE_STOP_SEC_CONST_SA_lvl_0x2F_UNSPECIFIED
#   include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  endif

#  define Rte_Prm_P1WPP_isSecurityLinActive_v() (Rte_AddrPar_0x2F_P1WPP_isSecurityLinActive_v) /* PRQA S 3453 */ /* MD_MSR_19.7 */

# endif /* !defined(RTE_CORE) */


# define IoHwAb_QM_IO_START_SEC_CODE
# include "IoHwAb_QM_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_AdiInterface_P_GetAdiPinState_CS AdiInterface_P_GetAdiPinState_CS
#  define RTE_RUNNABLE_AdiInterface_P_GetPullUpState_CS AdiInterface_P_GetPullUpState_CS
#  define RTE_RUNNABLE_AdiInterface_P_SetPullUp_CS AdiInterface_P_SetPullUp_CS
#  define RTE_RUNNABLE_Do12VInterface_P_GetDcdc12VState_CS Do12VInterface_P_GetDcdc12VState_CS
#  define RTE_RUNNABLE_Do12VInterface_P_GetDo12VPinsState_CS Do12VInterface_P_GetDo12VPinsState_CS
#  define RTE_RUNNABLE_Do12VInterface_P_SetDcdc12VActive_CS Do12VInterface_P_SetDcdc12VActive_CS
#  define RTE_RUNNABLE_Do12VInterface_P_SetDo12VLivingActive_CS Do12VInterface_P_SetDo12VLivingActive_CS
#  define RTE_RUNNABLE_Do12VInterface_P_SetDo12VParkedActive_CS Do12VInterface_P_SetDo12VParkedActive_CS
#  define RTE_RUNNABLE_DoblsCtrlInterface_P_GetDoblsPinState_CS DoblsCtrlInterface_P_GetDoblsPinState_CS
#  define RTE_RUNNABLE_DoblsCtrlInterface_P_SetDoblsActive_CS DoblsCtrlInterface_P_SetDoblsActive_CS
#  define RTE_RUNNABLE_DowhsInterface_P_GetDoPinStateOne_CS DowhsInterface_P_GetDoPinStateOne_CS
#  define RTE_RUNNABLE_DowhsInterface_P_SetDowActive_CS DowhsInterface_P_SetDowActive_CS
#  define RTE_RUNNABLE_DowlsInterface_P_GetDoPinStateOne_CS DowlsInterface_P_GetDoPinStateOne_CS
#  define RTE_RUNNABLE_DowlsInterface_P_SetDowActive_CS DowlsInterface_P_SetDowActive_CS
#  define RTE_RUNNABLE_IoHwAb_QM_IO_10ms_runnable IoHwAb_QM_IO_10ms_runnable
# endif

FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) AdiInterface_P_GetAdiPinState_CS(IOHWAB_UINT8 AdiPinRef, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) AdiPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) AdiInterface_P_GetPullUpState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Strong, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_Weak, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) isPullUpActive_DAI); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) AdiInterface_P_SetPullUp_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL ActivateStrongPullUp, IOHWAB_BOOL ActivateWeakPullUp, IOHWAB_BOOL ActivateDAIPullUp); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_GetDcdc12VState_CS(P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DcDc12vRefVoltage, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDcDc12vActivated, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_GetDo12VPinsState_CS(IOHWAB_UINT8 SelectParkedOrLivingPin, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDo12VActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Do12VPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_SetDcdc12VActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_SetDo12VLivingActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) Do12VInterface_P_SetDo12VParkedActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DoblsCtrlInterface_P_GetDoblsPinState_CS(P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) FaultStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DoblsCtrlInterface_P_SetDoblsActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowhsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowhsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowlsInterface_P_GetDoPinStateOne_CS(IOHWAB_UINT8 DoPinRef, P2VAR(IOHWAB_BOOL, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) IsDoActivated, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DoPinVoltage, P2VAR(VGTT_EcuPinVoltage_0V2, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) BatteryVoltage, P2VAR(VGTT_EcuPwmDutycycle, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DutyCycle, P2VAR(VGTT_EcuPwmPeriod, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) Period, P2VAR(VGTT_EcuPinFaultStatus, AUTOMATIC, RTE_IOHWAB_QM_IO_APPL_VAR) DiagStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, IoHwAb_QM_IO_CODE) DowlsInterface_P_SetDowActive_CS(IOCtrlReq_T IOCtrlReqType, IOHWAB_UINT8 OutputId, VGTT_EcuPwmPeriod Period, VGTT_EcuPwmDutycycle DutyCycle, IOHWAB_BOOL Activation); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, IoHwAb_QM_IO_CODE) IoHwAb_QM_IO_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define IoHwAb_QM_IO_STOP_SEC_CODE
# include "IoHwAb_QM_IO_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_AdiInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_Do12VInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DoblsCtrlInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_DowxsInterface_I_IoHwAbApplicationError (1U)

#  define RTE_E_EcuHwState_I_AdcInFailure (1U)

#  define RTE_E_VbatInterface_I_AdcInFailure (2U)

#  define RTE_E_VbatInterface_I_IoHwAbApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_IOHWAB_QM_IO_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
