/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_VEC_SecurityAccess.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <VEC_SecurityAccess>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_VEC_SECURITYACCESS_H
# define _RTE_VEC_SECURITYACCESS_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_VEC_SecurityAccess_Type.h"
# include "Rte_DataHandleType.h"

# include "Rte_Hook.h"


# define RTE_START_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * API prototypes
 *********************************************************************************************************************/


# define RTE_STOP_SEC_CODE
# include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_AsymDecryptFinish(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_AsymDecryptFinish(Csm_ConfigIdType parg0, P2VAR(AsymDecryptResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmAsymDecrypt_AsymDecryptFinish(arg1, arg2) (Csm_AsymDecryptFinish((Csm_ConfigIdType)0, arg1, arg2)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_AsymDecryptStart(Csm_ConfigIdType parg0, P2CONST(AsymPrivateKeyType, AUTOMATIC, RTE_CSM_APPL_DATA) key); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmAsymDecrypt_AsymDecryptStart(arg1) (Csm_AsymDecryptStart((Csm_ConfigIdType)0, arg1)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_AsymDecryptUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) cipherTextBuffer, uint32 cipherTextLength, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_AsymDecryptUpdate(Csm_ConfigIdType parg0, P2CONST(AsymDecryptDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) cipherTextBuffer, uint32 cipherTextLength, P2VAR(AsymDecryptResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextBuffer, P2VAR(AsymDecryptLengthBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) plainTextLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmAsymDecrypt_AsymDecryptUpdate(arg1, arg2, arg3, arg4) (Csm_AsymDecryptUpdate((Csm_ConfigIdType)0, arg1, arg2, arg3, arg4)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_RandomGenerate(Csm_ConfigIdType parg0, P2VAR(uint8, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer, uint32 resultLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_RandomGenerate(Csm_ConfigIdType parg0, P2VAR(RandomGenerateResultBuffer, AUTOMATIC, RTE_CSM_APPL_VAR) resultBuffer, uint32 resultLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmRandomGenerate_RandomGenerate(arg1, arg2) (Csm_RandomGenerate((Csm_ConfigIdType)1, arg1, arg2)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_RandomSeedFinish(Csm_ConfigIdType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmRandomSeed_RandomSeedFinish() (Csm_RandomSeedFinish((Csm_ConfigIdType)2)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_RandomSeedStart(Csm_ConfigIdType parg0); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmRandomSeed_RandomSeedStart() (Csm_RandomSeedStart((Csm_ConfigIdType)2)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
#  ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_RandomSeedUpdate(Csm_ConfigIdType parg0, P2CONST(uint8, AUTOMATIC, RTE_CSM_APPL_DATA) seedBuffer, uint32 seedLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  else
FUNC(Std_ReturnType, RTE_CSM_APPL_CODE) Csm_RandomSeedUpdate(Csm_ConfigIdType parg0, P2CONST(RandomSeedDataBuffer, AUTOMATIC, RTE_CSM_APPL_DATA) seedBuffer, uint32 seedLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  endif
#  define RTE_STOP_SEC_CSM_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_CsmRandomSeed_RandomSeedUpdate(arg1, arg2) (Csm_RandomSeedUpdate((Csm_ConfigIdType)2, arg1, arg2)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


/**********************************************************************************************************************
 * Exclusive Areas
 *********************************************************************************************************************/

#  define Rte_Enter_ExclusiveArea() SuspendAllInterrupts()  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */

#  define Rte_Exit_ExclusiveArea() ResumeAllInterrupts()  /* RteAnalyzer(ExclusiveArea, ALL_INTERRUPT_BLOCKING) */


# endif /* !defined(RTE_CORE) */


# define VEC_SecurityAccess_START_SEC_CODE
# include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_VEC_SecurityAccess_CallbackNotificationAsymDecrypt VEC_SecurityAccess_CallbackNotificationAsymDecrypt
#  define RTE_RUNNABLE_VEC_SecurityAccess_CallbackNotificationRandomGenerate VEC_SecurityAccess_CallbackNotificationRandomGenerate
#  define RTE_RUNNABLE_VEC_SecurityAccess_CallbackNotificationRandomSeed VEC_SecurityAccess_CallbackNotificationRandomSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_Init VEC_SecurityAccess_Init
#  define RTE_RUNNABLE_VEC_SecurityAccess_MainFunction VEC_SecurityAccess_MainFunction
#  define RTE_RUNNABLE_VEC_SecurityAccess_ProvideEntropy VEC_SecurityAccess_ProvideEntropy
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_01_CompareKey VEC_SecurityAccess_SA_Seed_01_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_01_GetSeed VEC_SecurityAccess_SA_Seed_01_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_03_CompareKey VEC_SecurityAccess_SA_Seed_03_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_03_GetSeed VEC_SecurityAccess_SA_Seed_03_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_05_CompareKey VEC_SecurityAccess_SA_Seed_05_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_05_GetSeed VEC_SecurityAccess_SA_Seed_05_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_07_CompareKey VEC_SecurityAccess_SA_Seed_07_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_07_GetSeed VEC_SecurityAccess_SA_Seed_07_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_09_CompareKey VEC_SecurityAccess_SA_Seed_09_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_09_GetSeed VEC_SecurityAccess_SA_Seed_09_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_0B_CompareKey VEC_SecurityAccess_SA_Seed_0B_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_0B_GetSeed VEC_SecurityAccess_SA_Seed_0B_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_0D_CompareKey VEC_SecurityAccess_SA_Seed_0D_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_0D_GetSeed VEC_SecurityAccess_SA_Seed_0D_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_0F_CompareKey VEC_SecurityAccess_SA_Seed_0F_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_0F_GetSeed VEC_SecurityAccess_SA_Seed_0F_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_11_CompareKey VEC_SecurityAccess_SA_Seed_11_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_11_GetSeed VEC_SecurityAccess_SA_Seed_11_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_13_CompareKey VEC_SecurityAccess_SA_Seed_13_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_13_GetSeed VEC_SecurityAccess_SA_Seed_13_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_15_CompareKey VEC_SecurityAccess_SA_Seed_15_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_15_GetSeed VEC_SecurityAccess_SA_Seed_15_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_17_CompareKey VEC_SecurityAccess_SA_Seed_17_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_17_GetSeed VEC_SecurityAccess_SA_Seed_17_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_19_CompareKey VEC_SecurityAccess_SA_Seed_19_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_19_GetSeed VEC_SecurityAccess_SA_Seed_19_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_1B_CompareKey VEC_SecurityAccess_SA_Seed_1B_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_1B_GetSeed VEC_SecurityAccess_SA_Seed_1B_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_1D_CompareKey VEC_SecurityAccess_SA_Seed_1D_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_1D_GetSeed VEC_SecurityAccess_SA_Seed_1D_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_1F_CompareKey VEC_SecurityAccess_SA_Seed_1F_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_1F_GetSeed VEC_SecurityAccess_SA_Seed_1F_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_21_CompareKey VEC_SecurityAccess_SA_Seed_21_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_21_GetSeed VEC_SecurityAccess_SA_Seed_21_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_23_CompareKey VEC_SecurityAccess_SA_Seed_23_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_23_GetSeed VEC_SecurityAccess_SA_Seed_23_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_25_CompareKey VEC_SecurityAccess_SA_Seed_25_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_25_GetSeed VEC_SecurityAccess_SA_Seed_25_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_27_CompareKey VEC_SecurityAccess_SA_Seed_27_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_27_GetSeed VEC_SecurityAccess_SA_Seed_27_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_29_CompareKey VEC_SecurityAccess_SA_Seed_29_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_29_GetSeed VEC_SecurityAccess_SA_Seed_29_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_2B_CompareKey VEC_SecurityAccess_SA_Seed_2B_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_2B_GetSeed VEC_SecurityAccess_SA_Seed_2B_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_2D_CompareKey VEC_SecurityAccess_SA_Seed_2D_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_2D_GetSeed VEC_SecurityAccess_SA_Seed_2D_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_2F_CompareKey VEC_SecurityAccess_SA_Seed_2F_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_2F_GetSeed VEC_SecurityAccess_SA_Seed_2F_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_31_CompareKey VEC_SecurityAccess_SA_Seed_31_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_31_GetSeed VEC_SecurityAccess_SA_Seed_31_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_33_CompareKey VEC_SecurityAccess_SA_Seed_33_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_33_GetSeed VEC_SecurityAccess_SA_Seed_33_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_35_CompareKey VEC_SecurityAccess_SA_Seed_35_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_35_GetSeed VEC_SecurityAccess_SA_Seed_35_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_37_CompareKey VEC_SecurityAccess_SA_Seed_37_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_37_GetSeed VEC_SecurityAccess_SA_Seed_37_GetSeed
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_39_CompareKey VEC_SecurityAccess_SA_Seed_39_CompareKey
#  define RTE_RUNNABLE_VEC_SecurityAccess_SA_Seed_39_GetSeed VEC_SecurityAccess_SA_Seed_39_GetSeed
# endif

FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CallbackNotificationAsymDecrypt(Csm_ReturnType retVal); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CallbackNotificationRandomGenerate(Csm_ReturnType retVal); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_CallbackNotificationRandomSeed(Csm_ReturnType retVal); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, VEC_SecurityAccess_CODE) VEC_SecurityAccess_Init(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, VEC_SecurityAccess_CODE) VEC_SecurityAccess_MainFunction(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_ProvideEntropy(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) seedPtr, UInt32 seedLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_ProvideEntropy(P2CONST(RandomSeedDataBuffer, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) seedPtr, UInt32 seedLength); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_01_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_01_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_01_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_01_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_03_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_03_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_03_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_03_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_05_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_05_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_05_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_05_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_07_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_07_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_07_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_07_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_09_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_09_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_09_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_09_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0B_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0D_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0F_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_0F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_11_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_11_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_11_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_11_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_13_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_13_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_13_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_13_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_15_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_15_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_15_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_15_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_17_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_17_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_17_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_17_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_19_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_19_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_19_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_19_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1B_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1D_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1F_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_1F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_21_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_21_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_21_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_21_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_23_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_23_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_23_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_23_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_25_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_25_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_25_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_25_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_27_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_27_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_27_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_27_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_29_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_29_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_29_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_29_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2B_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2B_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2B_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2D_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2D_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2D_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2F_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2F_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_2F_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_31_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_31_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_31_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_31_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_33_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_33_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_33_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_33_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_35_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_35_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_35_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_35_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_37_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_37_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_37_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_37_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_39_CompareKey(P2CONST(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_39_CompareKey(P2CONST(Dcm_Data128ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_DATA) Key, Dcm_OpStatusType OpStatus, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif
# ifdef RTE_PTR2ARRAYBASETYPE_PASSING
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_39_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(UInt8, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# else
FUNC(Std_ReturnType, VEC_SecurityAccess_CODE) VEC_SecurityAccess_SA_Seed_39_GetSeed(Dcm_OpStatusType OpStatus, P2VAR(Dcm_Data116ByteType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) Seed, P2VAR(Dcm_NegativeResponseCodeType, AUTOMATIC, RTE_VEC_SECURITYACCESS_APPL_VAR) ErrorCode); /* PRQA S 0850 */ /* MD_MSR_19.8 */
# endif

# define VEC_SecurityAccess_STOP_SEC_CODE
# include "VEC_SecurityAccess_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_CsmAsymDecrypt_CSM_E_BUSY (2U)

#  define RTE_E_CsmAsymDecrypt_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmAsymDecrypt_CSM_E_SMALL_BUFFER (3U)

#  define RTE_E_CsmCallback_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmRandomGenerate_CSM_E_BUSY (2U)

#  define RTE_E_CsmRandomGenerate_CSM_E_ENTROPY_EXHAUSTION (4U)

#  define RTE_E_CsmRandomGenerate_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmRandomSeed_CSM_E_BUSY (2U)

#  define RTE_E_CsmRandomSeed_CSM_E_NOT_OK (1U)

#  define RTE_E_CsmRandomSeed_CSM_E_SMALL_BUFFER (3U)

#  define RTE_E_SecurityAccess_DCM_E_COMPARE_KEY_FAILED (11U)

#  define RTE_E_SecurityAccess_DCM_E_PENDING (10U)

#  define RTE_E_SecurityAccess_E_NOT_OK (1U)

#  define RTE_E_VEC_SecurityAccess_ProvideEntropy_E_BUSY (2U)

#  define RTE_E_VEC_SecurityAccess_ProvideEntropy_E_NOT_OK (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_VEC_SECURITYACCESS_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
