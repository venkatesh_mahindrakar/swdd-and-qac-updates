/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_TransferCase_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <TransferCase_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_TRANSFERCASE_HMICTRL_TYPE_H
# define _RTE_TRANSFERCASE_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DeviceIndication_Off
#   define DeviceIndication_Off (0U)
#  endif

#  ifndef DeviceIndication_On
#   define DeviceIndication_On (1U)
#  endif

#  ifndef DeviceIndication_Blink
#   define DeviceIndication_Blink (2U)
#  endif

#  ifndef DeviceIndication_SpareValue
#   define DeviceIndication_SpareValue (3U)
#  endif

#  ifndef NotEngagedEngaged_NotEngaged
#   define NotEngagedEngaged_NotEngaged (0U)
#  endif

#  ifndef NotEngagedEngaged_Engaged
#   define NotEngagedEngaged_Engaged (1U)
#  endif

#  ifndef NotEngagedEngaged_Error
#   define NotEngagedEngaged_Error (2U)
#  endif

#  ifndef NotEngagedEngaged_NotAvailable
#   define NotEngagedEngaged_NotAvailable (3U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef TransferCaseNeutral_Req2_NoRequest
#   define TransferCaseNeutral_Req2_NoRequest (0U)
#  endif

#  ifndef TransferCaseNeutral_Req2_PutTransferCaseInDrive
#   define TransferCaseNeutral_Req2_PutTransferCaseInDrive (1U)
#  endif

#  ifndef TransferCaseNeutral_Req2_PutTransferCaseInNeutral
#   define TransferCaseNeutral_Req2_PutTransferCaseInNeutral (2U)
#  endif

#  ifndef TransferCaseNeutral_Req2_Spare1
#   define TransferCaseNeutral_Req2_Spare1 (3U)
#  endif

#  ifndef TransferCaseNeutral_Req2_Spare2
#   define TransferCaseNeutral_Req2_Spare2 (4U)
#  endif

#  ifndef TransferCaseNeutral_Req2_Spare3
#   define TransferCaseNeutral_Req2_Spare3 (5U)
#  endif

#  ifndef TransferCaseNeutral_Req2_Error
#   define TransferCaseNeutral_Req2_Error (6U)
#  endif

#  ifndef TransferCaseNeutral_Req2_NotAvailable
#   define TransferCaseNeutral_Req2_NotAvailable (7U)
#  endif

#  ifndef TransferCaseNeutral_NoAction
#   define TransferCaseNeutral_NoAction (0U)
#  endif

#  ifndef TransferCaseNeutral_RequestConfirmed
#   define TransferCaseNeutral_RequestConfirmed (1U)
#  endif

#  ifndef TransferCaseNeutral_Error
#   define TransferCaseNeutral_Error (2U)
#  endif

#  ifndef TransferCaseNeutral_NotAvailable
#   define TransferCaseNeutral_NotAvailable (3U)
#  endif

#  ifndef TransferCaseNeutral_status_TransferCaseInDrive
#   define TransferCaseNeutral_status_TransferCaseInDrive (0U)
#  endif

#  ifndef TransferCaseNeutral_status_TransferCaseInNeutral
#   define TransferCaseNeutral_status_TransferCaseInNeutral (1U)
#  endif

#  ifndef TransferCaseNeutral_status_Error
#   define TransferCaseNeutral_status_Error (2U)
#  endif

#  ifndef TransferCaseNeutral_status_NotAvailable
#   define TransferCaseNeutral_status_NotAvailable (3U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_TRANSFERCASE_HMICTRL_TYPE_H */
