/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_RKE.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application header file for SW-C <RKE>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_RKE_H
# define _RTE_RKE_H

# ifndef RTE_CORE
#  ifdef RTE_APPLICATION_HEADER_FILE
#   error Multiple application header files included.
#  endif
#  define RTE_APPLICATION_HEADER_FILE
#  ifndef RTE_PTR2ARRAYBASETYPE_PASSING
#   define RTE_PTR2ARRAYBASETYPE_PASSING
#  endif
# endif

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

/* include files */

# include "Rte_RKE_Type.h"
# include "Rte_DataHandleType.h"


# ifndef RTE_CORE

/**********************************************************************************************************************
 * Rte_Call_<p>_<o> (unmapped) for synchronous C/S communication
 *********************************************************************************************************************/
#  define Rte_Call_DiagForceDriveAntenastatus_CS() (RTE_E_UNCONNECTED) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_PEPS_APPL_CODE) RE_Generate_Telegram_EncryptionKey(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_PEPS_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_GenerateTelegramEncryptionKey_CS() (RE_Generate_Telegram_EncryptionKey(), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */
#  define RTE_START_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */
FUNC(void, RTE_IOHWAB_RFIC_APPL_CODE) SetRcvRkeData(P2VAR(st_RKEdata, AUTOMATIC, RTE_IOHWAB_RFIC_APPL_VAR) RcvRKEDataonRFIC); /* PRQA S 0850 */ /* MD_MSR_19.8 */
#  define RTE_STOP_SEC_IOHWAB_RFIC_APPL_CODE
#  include "MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

#  define Rte_Call_RkeData_SetRcvRkeData(arg1) (SetRcvRkeData(arg1), ((Std_ReturnType)RTE_E_OK)) /* PRQA S 3453 */ /* MD_MSR_19.7 */


# endif /* !defined(RTE_CORE) */


# define RKE_START_SEC_CODE
# include "RKE_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */

/**********************************************************************************************************************
 * Runnable entities
 *********************************************************************************************************************/

# ifndef RTE_CORE
#  define RTE_RUNNABLE_RE_Confirm_RKE_Event RE_Confirm_RKE_Event
#  define RTE_RUNNABLE_RE_GetFobRkeState RE_GetFobRkeState
#  define RTE_RUNNABLE_RE_GetKeyfobButtonStatus RE_GetKeyfobButtonStatus
#  define RTE_RUNNABLE_RE_RKE_rcv_Manager RE_RKE_rcv_Manager
#  define RTE_RUNNABLE_RKE_10ms_runnable RKE_10ms_runnable
# endif

FUNC(void, RKE_CODE) RE_Confirm_RKE_Event(void); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(Std_ReturnType, RKE_CODE) RE_GetFobRkeState(P2VAR(ButtonStatus, AUTOMATIC, RTE_RKE_APPL_VAR) RkeButton, P2VAR(uint16, AUTOMATIC, RTE_RKE_APPL_VAR) FobID, P2VAR(uint32, AUTOMATIC, RTE_RKE_APPL_VAR) FobSN, P2VAR(uint16, AUTOMATIC, RTE_RKE_APPL_VAR) FobRollingCounter, P2VAR(uint16, AUTOMATIC, RTE_RKE_APPL_VAR) ScimRollingCounter, P2VAR(uint8, AUTOMATIC, RTE_RKE_APPL_VAR) FobBattery); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, RKE_CODE) RE_GetKeyfobButtonStatus(P2VAR(uint8, AUTOMATIC, RTE_RKE_APPL_VAR) BatteryStatus, P2VAR(ButtonStatus, AUTOMATIC, RTE_RKE_APPL_VAR) ButtonStatus); /* PRQA S 0850 */ /* MD_MSR_19.8 */
FUNC(void, RKE_CODE) RE_RKE_rcv_Manager(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */
FUNC(void, RKE_CODE) RKE_10ms_runnable(void); /* PRQA S 0850, 3451 */ /* MD_MSR_19.8, MD_Rte_3451 */

# define RKE_STOP_SEC_CODE
# include "RKE_MemMap.h" /* PRQA S 5087 */ /* MD_MSR_19.1 */


# ifndef RTE_CORE
/**********************************************************************************************************************
 * Application errors
 *********************************************************************************************************************/

#  define RTE_E_DiagForceDriveAntenastatus_I_return (1U)

#  define RTE_E_RkeInterface_I_RadioApplicationError (1U)
# endif /* !defined(RTE_CORE) */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_RKE_H */

/**********************************************************************************************************************
 MISRA 2004 violations and justifications
 *********************************************************************************************************************/

/* module specific MISRA deviations:
   MD_Rte_3451:  MISRA rule: 8.8
     Reason:     Schedulable entities are declared by the RTE and also by the BSW modules.
     Risk:       No functional risk.
     Prevention: Not required.

*/
