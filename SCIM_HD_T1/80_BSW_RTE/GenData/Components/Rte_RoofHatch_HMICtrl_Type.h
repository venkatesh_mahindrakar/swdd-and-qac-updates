/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_RoofHatch_HMICtrl_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <RoofHatch_HMICtrl>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_ROOFHATCH_HMICTRL_TYPE_H
# define _RTE_ROOFHATCH_HMICTRL_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef A3PosSwitchStatus_Middle
#   define A3PosSwitchStatus_Middle (0U)
#  endif

#  ifndef A3PosSwitchStatus_Lower
#   define A3PosSwitchStatus_Lower (1U)
#  endif

#  ifndef A3PosSwitchStatus_Upper
#   define A3PosSwitchStatus_Upper (2U)
#  endif

#  ifndef A3PosSwitchStatus_Spare
#   define A3PosSwitchStatus_Spare (3U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_01
#   define A3PosSwitchStatus_Spare_01 (4U)
#  endif

#  ifndef A3PosSwitchStatus_Spare_02
#   define A3PosSwitchStatus_Spare_02 (5U)
#  endif

#  ifndef A3PosSwitchStatus_Error
#   define A3PosSwitchStatus_Error (6U)
#  endif

#  ifndef A3PosSwitchStatus_NotAvailable
#   define A3PosSwitchStatus_NotAvailable (7U)
#  endif

#  ifndef PushButtonStatus_Neutral
#   define PushButtonStatus_Neutral (0U)
#  endif

#  ifndef PushButtonStatus_Pushed
#   define PushButtonStatus_Pushed (1U)
#  endif

#  ifndef PushButtonStatus_Error
#   define PushButtonStatus_Error (2U)
#  endif

#  ifndef PushButtonStatus_NotAvailable
#   define PushButtonStatus_NotAvailable (3U)
#  endif

#  ifndef RoofHatch_HMI_rqst_NoAction
#   define RoofHatch_HMI_rqst_NoAction (0U)
#  endif

#  ifndef RoofHatch_HMI_rqst_Close
#   define RoofHatch_HMI_rqst_Close (1U)
#  endif

#  ifndef RoofHatch_HMI_rqst_Open
#   define RoofHatch_HMI_rqst_Open (2U)
#  endif

#  ifndef RoofHatch_HMI_rqst_SpareValue
#   define RoofHatch_HMI_rqst_SpareValue (3U)
#  endif

#  ifndef RoofHatch_HMI_rqst_SpareValue_01
#   define RoofHatch_HMI_rqst_SpareValue_01 (4U)
#  endif

#  ifndef RoofHatch_HMI_rqst_SpareValue_02
#   define RoofHatch_HMI_rqst_SpareValue_02 (5U)
#  endif

#  ifndef RoofHatch_HMI_rqst_ErrorIndicator
#   define RoofHatch_HMI_rqst_ErrorIndicator (6U)
#  endif

#  ifndef RoofHatch_HMI_rqst_NotAvailable
#   define RoofHatch_HMI_rqst_NotAvailable (7U)
#  endif

#  ifndef Operational
#   define Operational (0U)
#  endif

#  ifndef NonOperational
#   define NonOperational (1U)
#  endif

#  ifndef OperationalEntry
#   define OperationalEntry (2U)
#  endif

#  ifndef OperationalExit
#   define OperationalExit (3U)
#  endif

# endif /* RTE_CORE */

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_ROOFHATCH_HMICTRL_TYPE_H */
