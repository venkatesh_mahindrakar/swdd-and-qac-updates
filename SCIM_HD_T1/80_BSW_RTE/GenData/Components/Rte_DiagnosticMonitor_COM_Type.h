/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *             File:  Rte_DiagnosticMonitor_COM_Type.h
 *           Config:  SCIM_HD_T1.dpa
 *      ECU-Project:  SCIM_HD_T1
 *
 *        Generator:  MICROSAR RTE Generator Version 4.20.0
 *                    RTE Core Version 1.20.0
 *          License:  CBD1800194
 *
 *      Description:  Application types header file for SW-C <DiagnosticMonitor_COM>
 *********************************************************************************************************************/

/* double include prevention */
#ifndef _RTE_DIAGNOSTICMONITOR_COM_TYPE_H
# define _RTE_DIAGNOSTICMONITOR_COM_TYPE_H

# ifdef __cplusplus
extern "C"
{
# endif /* __cplusplus */

# include "Rte_Type.h"

# ifndef RTE_CORE

/**********************************************************************************************************************
 * Range, Invalidation, Enumeration and Bit Field Definitions
 *********************************************************************************************************************/

#  ifndef DEM_EVENT_STATUS_PASSED
#   define DEM_EVENT_STATUS_PASSED (0U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED
#   define DEM_EVENT_STATUS_FAILED (1U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED
#   define DEM_EVENT_STATUS_PREPASSED (2U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED
#   define DEM_EVENT_STATUS_PREFAILED (3U)
#  endif

#  ifndef DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED
#   define DEM_EVENT_STATUS_FDC_THRESHOLD_REACHED (4U)
#  endif

#  ifndef DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PASSED_CONDITIONS_NOT_FULFILLED (5U)
#  endif

#  ifndef DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_FAILED_CONDITIONS_NOT_FULFILLED (6U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREPASSED_CONDITIONS_NOT_FULFILLED (7U)
#  endif

#  ifndef DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED
#   define DEM_EVENT_STATUS_PREFAILED_CONDITIONS_NOT_FULFILLED (8U)
#  endif

# endif /* RTE_CORE */


/**********************************************************************************************************************
 * Definitions for Mode Management
 *********************************************************************************************************************/
# ifndef RTE_MODETYPE_BswMRteMDG_CanBusOff
#  define RTE_MODETYPE_BswMRteMDG_CanBusOff
typedef uint8 Rte_ModeType_BswMRteMDG_CanBusOff;
# endif

# define RTE_MODE_DiagnosticMonitor_COM_BswMRteMDG_CanBusOff_CAN_BusOff (0U)
# ifndef RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff
#  define RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff (0U)
# endif
# define RTE_MODE_DiagnosticMonitor_COM_BswMRteMDG_CanBusOff_CAN_NormalCom (1U)
# ifndef RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom
#  define RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom (1U)
# endif
# define RTE_TRANSITION_DiagnosticMonitor_COM_BswMRteMDG_CanBusOff (2U)
# ifndef RTE_TRANSITION_BswMRteMDG_CanBusOff
#  define RTE_TRANSITION_BswMRteMDG_CanBusOff (2U)
# endif

# ifdef __cplusplus
} /* extern "C" */
# endif /* __cplusplus */

#endif /* _RTE_DIAGNOSTICMONITOR_COM_TYPE_H */
