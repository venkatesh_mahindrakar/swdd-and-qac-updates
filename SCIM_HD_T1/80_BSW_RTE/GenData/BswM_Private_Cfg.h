/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: BswM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: BswM_Private_Cfg.h
 *   Generation Time: 2020-11-11 14:25:30
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/


#if !defined(BSWM_PRIVATE_CFG_H)
#define BSWM_PRIVATE_CFG_H


/* -----------------------------------------------------------------------------
    &&&~ INCLUDE
 ----------------------------------------------------------------------------- */
#include "BswM_Cfg.h"
#include "Com.h" 
#include "Dem.h" 
#include "Rte_BswM.h" 
#include "J1939Rm.h" 
#include "WrapNv.h" 
#include "SchM_BswM.h" 
#include "EcuM.h" 
#include "Can.h" 
#include "Lin.h" 
#include "CanIf.h" 
#include "LinIf.h" 
#include "PduR.h" 
#include "CanSM_EcuM.h" 
#include "LinSM.h" 
#include "CanNm.h" 
#include "Nm.h" 
#include "CanTp.h" 
#include "J1939Tp.h" 
#include "Issm.h" 
#include "ComM.h" 
#include "Dcm.h" 
#include "J1939Nm.h" 
#include "Issm_Cbk.h" 
#include "Fee.h" 
#include "NvM.h" 
#include "LinTrcv_30_Generic.h" 
#include "CanTrcv_30_GenericCan.h" 
#include "Cry_30_LibCv.h" 
#include "Csm.h" 
#include "CanXcp.h" 
#include "Xcp.h" 
#include "Rtm.h" 
#include "RamTst.h" 
#include "MemIf.h" 
#include "Rte_Main.h" 



/**********************************************************************************************************************
 *  GLOBAL CONSTANT MACROS
**********************************************************************************************************************/


# define BSWM_FUNCTION_BASED STD_ON

#if (defined (BSWM_ACTIONLISTSTRUEIDXOFRULES))
# if ((BSWM_ACTIONLISTSTRUEIDXOFRULES == STD_ON) && defined (BSWM_NO_ACTIONLISTSTRUEIDXOFRULES))
#  define BSWM_NO_ACTIONLIST(partition) BSWM_NO_ACTIONLISTSTRUEIDXOFRULES
# endif
#endif
#if (defined(BSWM_NO_ACTIONLIST))
#else
# if(defined(BSWM_ACTIONLISTSFALSEIDXOFRULES))
#  if ((BSWM_ACTIONLISTSFALSEIDXOFRULES == STD_ON) && defined (BSWM_NO_ACTIONLISTSFALSEIDXOFRULES))
#   define BSWM_NO_ACTIONLIST(partition) BSWM_NO_ACTIONLISTSFALSEIDXOFRULES
#  endif
# endif
#endif
#if (defined(BSWM_NO_ACTIONLIST))
#else
# if(defined(BSWM_SIZEOFACTIONLISTS))
#  if (BSWM_SIZEOFACTIONLISTS == STD_ON)
#   define BSWM_NO_ACTIONLIST(partition) BswM_GetSizeOfActionLists(partition) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#  endif
# endif
#endif
#if (defined(BSWM_NO_ACTIONLIST))
#else
# define BSWM_NO_ACTIONLIST(partition) 255
#endif

#define BSWM_GROUPCONTROL_IDLE   (uint8)0x00u
#define BSWM_GROUPCONTROL_NORMAL (uint8)0x01u
#define BSWM_GROUPCONTROL_REINIT (uint8)0x02u
#define BSWM_GROUPCONTROL_DM     (uint8)0x04u

#if BSWM_NVMJOBSTATE == STD_ON
#define NVM_SERVICE_ID_READALL   (uint8)0x0Cu
#define NVM_SERVICE_ID_WRITEALL  (uint8)0x0Du
#endif



#define BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table1                0u 
#define BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable0              1u 
#define BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable               2u 
#define BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable               3u 
#define BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable               4u 
#define BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table2                5u 
#define BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_TableE                6u 
#define BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable1              7u 
#define BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable2              8u 
#define BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_Table0                9u 
#define BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_TableE                10u 
#define BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable               11u 
#define BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable               12u 
#define BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable1              13u 
#define BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable2              14u 
#define BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table1                15u 
#define BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table2                16u 
#define BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_TableE                17u 
#define BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable1              18u 
#define BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable2              19u 
#define BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table1                20u 
#define BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table2                21u 
#define BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_TableE                22u 
#define BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable2              23u 
#define BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable1              24u 
#define BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table1                25u 
#define BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table2                26u 
#define BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_TableE                27u 
#define BSWM_ID_RULE_CC_Rule_DcmEcuReset_Execute                         28u 
#define BSWM_ID_RULE_CC_Rule_DcmEcuReset_Trigger                         29u 
#define BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_Table1                     30u 
#define BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_NULL                       31u 
#define BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_Table2                     32u 
#define BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_Table_E                    33u 
#define BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp        34u 
#define BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1 35u 
#define BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2 36u 
#define BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp        37u 
#define BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1 38u 
#define BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2 39u 
#define BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_NULL                       40u 
#define BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_Table1                     41u 
#define BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_Table2                     42u 
#define BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_Table_E                    43u 
#define BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp        44u 
#define BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0 45u 
#define BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_NULL                       46u 
#define BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_Table0                     47u 
#define BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_Table_E                    48u 
#define BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp        49u 
#define BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1 50u 
#define BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2 51u 
#define BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_NULL                       52u 
#define BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_Table1                     53u 
#define BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_Table2                     54u 
#define BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_Table_E                    55u 
#define BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp        56u 
#define BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1 57u 
#define BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2 58u 
#define BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_NULL                       59u 
#define BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_Table1                     60u 
#define BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_Table2                     61u 
#define BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_Table_E                    62u 
#define BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX_DM                      63u 
#define BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289         64u 
#define BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX_DM                      65u 
#define BSWM_ID_RULE_CC_CN_LIN02_c2d7c6f3                                66u 
#define BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX                    67u 
#define BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX_DM                 68u 
#define BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_TX                            69u 
#define BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX                         70u 
#define BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289                 71u 
#define BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX                         72u 
#define BSWM_ID_RULE_CC_CN_LIN03_b5d0f665                                73u 
#define BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX                            74u 
#define BSWM_ID_RULE_CC_CN_LIN01_5bde9749                                75u 
#define BSWM_ID_RULE_CC_CN_LIN04_2bb463c6                                76u 
#define BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX                    77u 
#define BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX_DM                         78u 
#define BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX_DM                 79u 
#define BSWM_ID_RULE_CC_CN_LIN00_2cd9a7df                                80u 
#define BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX              81u 
#define BSWM_ID_RULE_ESH_RunToPostRun                                    82u 
#define BSWM_ID_RULE_ESH_RunToPostRunNested                              83u 
#define BSWM_ID_RULE_ESH_WaitToShutdown                                  84u 
#define BSWM_ID_RULE_ESH_WakeupToPrep                                    85u 
#define BSWM_ID_RULE_ESH_WaitToWakeup                                    86u 
#define BSWM_ID_RULE_ESH_WakeupToRun                                     87u 
#define BSWM_ID_RULE_ESH_InitToWakeup                                    88u 
#define BSWM_ID_RULE_ESH_PostRunNested                                   89u 
#define BSWM_ID_RULE_ESH_PostRun                                         90u 
#define BSWM_ID_RULE_ESH_PrepToWait                                      91u 
#define BSWM_ID_RULE_ESH_DemInit                                         92u 
#define BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_TX                    93u 
#define BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_TX                         94u 
#define BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_TX                         95u 
#define BSWM_ID_RULE_CC_Rule_LIN1_ScheduleTableEndNotification           96u 
#define BSWM_ID_RULE_CC_Rule_LIN2_ScheduleTableEndNotification           97u 
#define BSWM_ID_RULE_CC_Rule_LIN3_ScheduleTableEndNotification           98u 
#define BSWM_ID_RULE_CC_Rule_LIN4_ScheduleTableEndNotification           99u 
#define BSWM_ID_RULE_CC_Rule_LIN5_ScheduleTableEndNotification           100u 
#define BSWM_ID_RULE_CC_CN_LIN06_c5ba02ea                                101u 
#define BSWM_ID_RULE_CC_CN_LIN05_5cb35350                                102u 
#define BSWM_ID_RULE_CC_CN_LIN07_b2bd327c                                103u 
#define BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX                              104u 
#define BSWM_ID_RULE_CC_CN_CAN6_b040c073_TX                              105u 
#define BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX_DM                           106u 
#define BSWM_ID_RULE_CC_Rule_LIN6_ScheduleTableEndNotification           107u 
#define BSWM_ID_RULE_CC_Rule_LIN7_ScheduleTableEndNotification           108u 
#define BSWM_ID_RULE_CC_Rule_LIN8_ScheduleTableEndNotification           109u 
#define BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable0              110u 
#define BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable               111u 
#define BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_Table0                112u 
#define BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable               113u 
#define BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable               114u 
#define BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable0              115u 
#define BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable0              116u 
#define BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_Table0                117u 
#define BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_Table0                118u 
#define BSWM_ID_RULE_CC_Rule_LIN6_Schedule_To_Table0                     119u 
#define BSWM_ID_RULE_CC_Rule_LIN7_Schedule_To_Table0                     120u 
#define BSWM_ID_RULE_CC_Rule_LIN8_Schedule_To_Table0                     121u 
#define BSWM_ID_RULE_CC_Rule_LIN6_Schedule_To_MSTable                    122u 
#define BSWM_ID_RULE_CC_Rule_LIN7_Schedule_To_MSTable                    123u 
#define BSWM_ID_RULE_CC_Rule_LIN8_Schedule_To_MSTable                    124u 
#define BSWM_ID_RULE_CC_Rule_LIN6_Schedule_To_MSTable0                   125u 
#define BSWM_ID_RULE_CC_Rule_LIN7_Schedule_To_MSTable0                   126u 
#define BSWM_ID_RULE_CC_Rule_LIN8_Schedule_To_MSTable0                   127u 
#define BSWM_ID_RULE_CC_DCMResetProcess_Started                          128u 
#define BSWM_ID_RULE_CC_DCMResetProcess_InProgress                       129u 
#define BSWM_ID_RULE_CC_Rule_DcmEcuReset_JumpToBTL                       130u 
#define BSWM_ID_RULE_CC_Rule_NvmWriteAll_Request                         131u 
#define BSWM_ID_RULE_CC_Rule_BB1_BusOff_Indication                       132u 
#define BSWM_ID_RULE_CC_Rule_BB2_BusOff_Indication                       133u 
#define BSWM_ID_RULE_CC_Rule_CAN6_BusOff_Indication                      134u 
#define BSWM_ID_RULE_CC_Rule_CabSubnet_BusOff_Indication                 135u 
#define BSWM_ID_RULE_CC_Rule_FMSNet_BusOff_Indication                    136u 
#define BSWM_ID_RULE_CC_Rule_SecuritySubnet_BusOff_Indication            137u 
#define BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX      138u 
#define BSWM_ID_RULE_CC_Rule_PvtReportCtrl                               139u 

#define BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_MSTable1                           0u 
#define BSWM_ID_AL_CC_AL_LIN2_SchTableStartInd_MSTable0                           1u 
#define BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_MSTable                            2u 
#define BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_MSTable1                           3u 
#define BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_MSTable2                           4u 
#define BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_MSTable2                           5u 
#define BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_Table1                             6u 
#define BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_Table2                             7u 
#define BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_TableE                             8u 
#define BSWM_ID_AL_CC_AL_LIN2_SchTableStartInd_Table0                             9u 
#define BSWM_ID_AL_CC_AL_LIN2_SchTableStartInd_TableE                             10u 
#define BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_MSTable                            11u 
#define BSWM_ID_AL_CC_AL_LIN2_SchTableStartInd_MSTable                            12u 
#define BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_MSTable1                           13u 
#define BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_MSTable2                           14u 
#define BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_Table1                             15u 
#define BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_Table2                             16u 
#define BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_TableE                             17u 
#define BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_MSTable2                           18u 
#define BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_MSTable                            19u 
#define BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_Table1                             20u 
#define BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_Table2                             21u 
#define BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_TableE                             22u 
#define BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_MSTable1                           23u 
#define BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_MSTable                            24u 
#define BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_Table1                             25u 
#define BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_Table2                             26u 
#define BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_TableE                             27u 
#define BSWM_ID_AL_CC_AL_DcmEcuReset_Trigger                                      28u 
#define BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_Table2                             29u 
#define BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp                30u 
#define BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp                31u 
#define BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_Table_E                            32u 
#define BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1        33u 
#define BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_Table1                             34u 
#define BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_NULL                               35u 
#define BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_Table2                             36u 
#define BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_Table_E                            37u 
#define BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp                38u 
#define BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1        39u 
#define BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2        40u 
#define BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp                41u 
#define BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp                42u 
#define BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_Table2                             43u 
#define BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_Table_E                            44u 
#define BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_Table_E                            45u 
#define BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_Table_E                            46u 
#define BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_Table2                             47u 
#define BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_Table1                             48u 
#define BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_Table1                             49u 
#define BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_Table0                             50u 
#define BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_Table1                             51u 
#define BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_NULL                               52u 
#define BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_NULL                               53u 
#define BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_NULL                               54u 
#define BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_NULL                               55u 
#define BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2        56u 
#define BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2        57u 
#define BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2        58u 
#define BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0        59u 
#define BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1        60u 
#define BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1        61u 
#define BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM                         62u 
#define BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM                          63u 
#define BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline         64u 
#define BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online          65u 
#define BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_Disable_DM                         66u 
#define BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_Enable_DM                          67u 
#define BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable                    68u 
#define BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit               69u 
#define BSWM_ID_AL_CC_AL_CN_LIN02_c2d7c6f3_Disable                                70u 
#define BSWM_ID_AL_CC_AL_CN_LIN02_c2d7c6f3_Enable                                 71u 
#define BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable                    72u 
#define BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit               73u 
#define BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM                    74u 
#define BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM                     75u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_TX_Disable                            76u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit                       77u 
#define BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_RX_Disable                         78u 
#define BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit                    79u 
#define BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable                         80u 
#define BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit                    81u 
#define BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_TX_Disable                         82u 
#define BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit                    83u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline                 84u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online                  85u 
#define BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable                         86u 
#define BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit                    87u 
#define BSWM_ID_AL_CC_AL_CN_LIN03_b5d0f665_Disable                                88u 
#define BSWM_ID_AL_CC_AL_CN_LIN03_b5d0f665_Enable                                 89u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_RX_Disable                            90u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit                       91u 
#define BSWM_ID_AL_CC_AL_CN_LIN01_5bde9749_Disable                                92u 
#define BSWM_ID_AL_CC_AL_CN_LIN01_5bde9749_Enable                                 93u 
#define BSWM_ID_AL_CC_AL_CN_LIN04_2bb463c6_Disable                                94u 
#define BSWM_ID_AL_CC_AL_CN_LIN04_2bb463c6_Enable                                 95u 
#define BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable                    96u 
#define BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit               97u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_Disable_DM                            98u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_Enable_DM                             99u 
#define BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM                    100u 
#define BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM                     101u 
#define BSWM_ID_AL_CC_AL_CN_LIN00_2cd9a7df_Disable                                102u 
#define BSWM_ID_AL_CC_AL_CN_LIN00_2cd9a7df_Enable                                 103u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable              104u 
#define BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit         105u 
#define BSWM_ID_AL_ESH_AL_ExitRun                                                 106u 
#define BSWM_ID_AL_ESH_AL_RunToPostRun                                            107u 
#define BSWM_ID_AL_ESH_AL_WaitForNvMToShutdown                                    108u 
#define BSWM_ID_AL_ESH_AL_WakeupToPrep                                            109u 
#define BSWM_ID_AL_ESH_AL_WaitForNvMWakeup                                        110u 
#define BSWM_ID_AL_ESH_AL_WakeupToRun                                             111u 
#define BSWM_ID_AL_ESH_AL_InitToWakeup                                            112u 
#define BSWM_ID_AL_ESH_AL_PostRunToPrepShutdown                                   113u 
#define BSWM_ID_AL_ESH_AL_PostRunToRun                                            114u 
#define BSWM_ID_AL_ESH_AL_ExitPostRun                                             115u 
#define BSWM_ID_AL_ESH_AL_PrepShutdownToWaitForNvM                                116u 
#define BSWM_ID_AL_INIT_AL_Initialize                                             117u 
#define BSWM_ID_AL_ESH_AL_DemInit                                                 118u 
#define BSWM_ID_AL_CC_AL_LIN1_ScheduleTableEndNotification                        119u 
#define BSWM_ID_AL_CC_AL_LIN2_ScheduleTableEndNotification                        120u 
#define BSWM_ID_AL_CC_AL_LIN3_ScheduleTableEndNotification                        121u 
#define BSWM_ID_AL_CC_AL_LIN4_ScheduleTableEndNotification                        122u 
#define BSWM_ID_AL_CC_AL_LIN5_ScheduleTableEndNotification                        123u 
#define BSWM_ID_AL_CC_AL_CN_LIN06_c5ba02ea_Disable                                124u 
#define BSWM_ID_AL_CC_AL_CN_LIN06_c5ba02ea_Enable                                 125u 
#define BSWM_ID_AL_CC_AL_CN_LIN05_5cb35350_Disable                                126u 
#define BSWM_ID_AL_CC_AL_CN_LIN05_5cb35350_Enable                                 127u 
#define BSWM_ID_AL_CC_AL_CN_LIN07_b2bd327c_Disable                                128u 
#define BSWM_ID_AL_CC_AL_CN_LIN07_b2bd327c_Enable                                 129u 
#define BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_RX_Disable                              130u 
#define BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit                         131u 
#define BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_TX_Disable                              132u 
#define BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit                         133u 
#define BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_Disable_DM                              134u 
#define BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_Enable_DM                               135u 
#define BSWM_ID_AL_CC_AL_LIN6_ScheduleTableEndNotification                        136u 
#define BSWM_ID_AL_CC_AL_LIN7_ScheduleTableEndNotification                        137u 
#define BSWM_ID_AL_CC_AL_LIN8_ScheduleTableEndNotification                        138u 
#define BSWM_ID_AL_CC_AL_LIN6_SchTableStartInd_Table0                             139u 
#define BSWM_ID_AL_CC_AL_LIN7_SchTableStartInd_Table0                             140u 
#define BSWM_ID_AL_CC_AL_LIN8_SchTableStartInd_Table0                             141u 
#define BSWM_ID_AL_CC_AL_LIN6_SchTableStartInd_MSTable                            142u 
#define BSWM_ID_AL_CC_AL_LIN7_SchTableStartInd_MSTable                            143u 
#define BSWM_ID_AL_CC_AL_LIN8_SchTableStartInd_MSTable                            144u 
#define BSWM_ID_AL_CC_AL_LIN6_SchTableStartInd_MSTable0                           145u 
#define BSWM_ID_AL_CC_AL_LIN7_SchTableStartInd_MSTable0                           146u 
#define BSWM_ID_AL_CC_AL_LIN8_SchTableStartInd_MSTable0                           147u 
#define BSWM_ID_AL_CC_AL_LIN6_ScheduleTable_to_Table0                             148u 
#define BSWM_ID_AL_CC_AL_LIN7_ScheduleTable_to_Table0                             149u 
#define BSWM_ID_AL_CC_AL_LIN8_ScheduleTable_to_Table0                             150u 
#define BSWM_ID_AL_CC_AL_LIN6_ScheduleTable_to_MSTable0                           151u 
#define BSWM_ID_AL_CC_AL_LIN6_ScheduleTable_to_MSTable                            152u 
#define BSWM_ID_AL_CC_AL_LIN7_ScheduleTable_to_MSTable0                           153u 
#define BSWM_ID_AL_CC_AL_LIN7_ScheduleTable_to_MSTable                            154u 
#define BSWM_ID_AL_CC_AL_LIN8_ScheduleTable_to_MSTable0                           155u 
#define BSWM_ID_AL_CC_AL_LIN8_ScheduleTable_to_MSTable                            156u 
#define BSWM_ID_AL_CC_AL_DCMResetProcess_Started                                  157u 
#define BSWM_ID_AL_CC_AL_DCMResetProcess_InProgress                               158u 
#define BSWM_ID_AL_CC_AL_RunToECUReset                                            159u 
#define BSWM_ID_AL_CC_AL_DcmEcuReset_Execute                                      160u 
#define BSWM_ID_AL_CC_AL_DcmEcuReset_JumpToBTL                                    161u 
#define BSWM_ID_AL_CC_AL_DCMResetPostRun                                          162u 
#define BSWM_ID_AL_CC_AL_PrepShutdown_NvmWriteAll                                 163u 
#define BSWM_ID_AL_CC_True_AL_BB1_BusOff_Indication                               164u 
#define BSWM_ID_AL_CC_False_AL_BB1_BusOff_Indication                              165u 
#define BSWM_ID_AL_CC_True_AL_BB2_BusOff_Indication                               166u 
#define BSWM_ID_AL_CC_False_AL_CAN6_BusOff_Indication                             167u 
#define BSWM_ID_AL_CC_True_AL_CAN6_BusOff_Indication                              168u 
#define BSWM_ID_AL_CC_False_AL_BB2_BusOff_Indication                              169u 
#define BSWM_ID_AL_CC_False_AL_CabSubnet_BusOff_Indication                        170u 
#define BSWM_ID_AL_CC_False_AL_FMSNet_BusOff_Indication                           171u 
#define BSWM_ID_AL_CC_True_AL_CabSubnet_BusOff_Indication                         172u 
#define BSWM_ID_AL_CC_True_AL_FMSNet_BusOff_Indication                            173u 
#define BSWM_ID_AL_CC_False_AL_SecuritySubnet_BusOff_Indication                   174u 
#define BSWM_ID_AL_CC_True_AL_SecuritySubnet_BusOff_Indication                    175u 
#define BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable      176u 
#define BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit 177u 
#define BSWM_ID_AL_AL_PvtReport_Enabled                                           178u 
#define BSWM_ID_AL_AL_PvtReport_Disabled                                          179u 


/**********************************************************************************************************************
 *  GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
/** 
  \defgroup  BswMPCGetConstantDuplicatedRootDataMacros  BswM Get Constant Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated by constance root data elements.
  \{
*/ 
#define BswM_GetPartitionIdentifiersOfPCConfig()                                                    BswM_PartitionIdentifiers  /**< the pointer to BswM_PartitionIdentifiers */
#define BswM_GetSizeOfPartitionIdentifiersOfPCConfig()                                              1u  /**< the number of accomplishable value elements in BswM_PartitionIdentifiers */
#define BswM_GetActionListQueueOfPCPartitionConfig(partitionIndex)                                  BswM_ActionListQueue.raw  /**< the pointer to BswM_ActionListQueue */
#define BswM_GetActionListsOfPCPartitionConfig(partitionIndex)                                      BswM_ActionLists  /**< the pointer to BswM_ActionLists */
#define BswM_GetCanSMChannelMappingOfPCPartitionConfig(partitionIndex)                              BswM_CanSMChannelMapping  /**< the pointer to BswM_CanSMChannelMapping */
#define BswM_GetCanSMChannelStateOfPCPartitionConfig(partitionIndex)                                BswM_CanSMChannelState  /**< the pointer to BswM_CanSMChannelState */
#define BswM_GetComMChannelMappingOfPCPartitionConfig(partitionIndex)                               BswM_ComMChannelMapping  /**< the pointer to BswM_ComMChannelMapping */
#define BswM_GetComMChannelStateOfPCPartitionConfig(partitionIndex)                                 BswM_ComMChannelState  /**< the pointer to BswM_ComMChannelState */
#define BswM_GetDcmComMappingOfPCPartitionConfig(partitionIndex)                                    BswM_DcmComMapping  /**< the pointer to BswM_DcmComMapping */
#define BswM_GetDcmComStateOfPCPartitionConfig(partitionIndex)                                      BswM_DcmComState  /**< the pointer to BswM_DcmComState */
#define BswM_GetDeferredRulesOfPCPartitionConfig(partitionIndex)                                    BswM_DeferredRules  /**< the pointer to BswM_DeferredRules */
#define BswM_GetForcedActionListPriorityOfPCPartitionConfig(partitionIndex)                         (&(BswM_ForcedActionListPriority))  /**< the pointer to BswM_ForcedActionListPriority */
#define BswM_GetGenericMappingOfPCPartitionConfig(partitionIndex)                                   BswM_GenericMapping  /**< the pointer to BswM_GenericMapping */
#define BswM_GetGenericStateOfPCPartitionConfig(partitionIndex)                                     BswM_GenericState  /**< the pointer to BswM_GenericState */
#define BswM_GetImmediateUserOfPCPartitionConfig(partitionIndex)                                    BswM_ImmediateUser  /**< the pointer to BswM_ImmediateUser */
#define BswM_GetInitGenVarAndInitALOfPCPartitionConfig(partitionIndex)                              BswM_InitGenVarAndInitAL  /**< the pointer to BswM_InitGenVarAndInitAL */
#define BswM_GetInitializedOfPCPartitionConfig(partitionIndex)                                      (&(BswM_Initialized))  /**< the pointer to BswM_Initialized */
#define BswM_GetJ1939NmMappingOfPCPartitionConfig(partitionIndex)                                   BswM_J1939NmMapping  /**< the pointer to BswM_J1939NmMapping */
#define BswM_GetJ1939NmStateOfPCPartitionConfig(partitionIndex)                                     BswM_J1939NmState  /**< the pointer to BswM_J1939NmState */
#define BswM_GetLinSMMappingOfPCPartitionConfig(partitionIndex)                                     BswM_LinSMMapping  /**< the pointer to BswM_LinSMMapping */
#define BswM_GetLinSMStateOfPCPartitionConfig(partitionIndex)                                       BswM_LinSMState  /**< the pointer to BswM_LinSMState */
#define BswM_GetLinScheduleEndMappingOfPCPartitionConfig(partitionIndex)                            BswM_LinScheduleEndMapping  /**< the pointer to BswM_LinScheduleEndMapping */
#define BswM_GetLinScheduleEndStateOfPCPartitionConfig(partitionIndex)                              BswM_LinScheduleEndState  /**< the pointer to BswM_LinScheduleEndState */
#define BswM_GetLinScheduleMappingOfPCPartitionConfig(partitionIndex)                               BswM_LinScheduleMapping  /**< the pointer to BswM_LinScheduleMapping */
#define BswM_GetLinScheduleStateOfPCPartitionConfig(partitionIndex)                                 BswM_LinScheduleState  /**< the pointer to BswM_LinScheduleState */
#define BswM_GetModeNotificationFctOfPCPartitionConfig(partitionIndex)                              BswM_ModeNotificationFct  /**< the pointer to BswM_ModeNotificationFct */
#define BswM_GetModeRequestMappingOfPCPartitionConfig(partitionIndex)                               BswM_ModeRequestMapping  /**< the pointer to BswM_ModeRequestMapping */
#define BswM_GetModeRequestQueueOfPCPartitionConfig(partitionIndex)                                 BswM_ModeRequestQueue  /**< the pointer to BswM_ModeRequestQueue */
#define BswM_GetNvMJobMappingOfPCPartitionConfig(partitionIndex)                                    BswM_NvMJobMapping  /**< the pointer to BswM_NvMJobMapping */
#define BswM_GetNvMJobStateOfPCPartitionConfig(partitionIndex)                                      BswM_NvMJobState  /**< the pointer to BswM_NvMJobState */
#define BswM_GetQueueSemaphoreOfPCPartitionConfig(partitionIndex)                                   (&(BswM_QueueSemaphore))  /**< the pointer to BswM_QueueSemaphore */
#define BswM_GetQueueWrittenOfPCPartitionConfig(partitionIndex)                                     (&(BswM_QueueWritten))  /**< the pointer to BswM_QueueWritten */
#define BswM_GetRuleStatesOfPCPartitionConfig(partitionIndex)                                       BswM_RuleStates.raw  /**< the pointer to BswM_RuleStates */
#define BswM_GetRulesIndOfPCPartitionConfig(partitionIndex)                                         BswM_RulesInd  /**< the pointer to BswM_RulesInd */
#define BswM_GetRulesOfPCPartitionConfig(partitionIndex)                                            BswM_Rules  /**< the pointer to BswM_Rules */
#define BswM_GetSizeOfActionListsOfPCPartitionConfig(partitionIndex)                                180u  /**< the number of accomplishable value elements in BswM_ActionLists */
#define BswM_GetSizeOfCanSMChannelMappingOfPCPartitionConfig(partitionIndex)                        6u  /**< the number of accomplishable value elements in BswM_CanSMChannelMapping */
#define BswM_GetSizeOfCanSMChannelStateOfPCPartitionConfig(partitionIndex)                          6u  /**< the number of accomplishable value elements in BswM_CanSMChannelState */
#define BswM_GetSizeOfComMChannelMappingOfPCPartitionConfig(partitionIndex)                         14u  /**< the number of accomplishable value elements in BswM_ComMChannelMapping */
#define BswM_GetSizeOfComMChannelStateOfPCPartitionConfig(partitionIndex)                           14u  /**< the number of accomplishable value elements in BswM_ComMChannelState */
#define BswM_GetSizeOfDcmComMappingOfPCPartitionConfig(partitionIndex)                              6u  /**< the number of accomplishable value elements in BswM_DcmComMapping */
#define BswM_GetSizeOfDcmComStateOfPCPartitionConfig(partitionIndex)                                6u  /**< the number of accomplishable value elements in BswM_DcmComState */
#define BswM_GetSizeOfDeferredRulesOfPCPartitionConfig(partitionIndex)                              20u  /**< the number of accomplishable value elements in BswM_DeferredRules */
#define BswM_GetSizeOfGenericMappingOfPCPartitionConfig(partitionIndex)                             5u  /**< the number of accomplishable value elements in BswM_GenericMapping */
#define BswM_GetSizeOfGenericStateOfPCPartitionConfig(partitionIndex)                               5u  /**< the number of accomplishable value elements in BswM_GenericState */
#define BswM_GetSizeOfImmediateUserOfPCPartitionConfig(partitionIndex)                              49u  /**< the number of accomplishable value elements in BswM_ImmediateUser */
#define BswM_GetSizeOfInitGenVarAndInitALOfPCPartitionConfig(partitionIndex)                        1u  /**< the number of accomplishable value elements in BswM_InitGenVarAndInitAL */
#define BswM_GetSizeOfJ1939NmMappingOfPCPartitionConfig(partitionIndex)                             2u  /**< the number of accomplishable value elements in BswM_J1939NmMapping */
#define BswM_GetSizeOfJ1939NmStateOfPCPartitionConfig(partitionIndex)                               2u  /**< the number of accomplishable value elements in BswM_J1939NmState */
#define BswM_GetSizeOfLinSMMappingOfPCPartitionConfig(partitionIndex)                               8u  /**< the number of accomplishable value elements in BswM_LinSMMapping */
#define BswM_GetSizeOfLinSMStateOfPCPartitionConfig(partitionIndex)                                 8u  /**< the number of accomplishable value elements in BswM_LinSMState */
#define BswM_GetSizeOfLinScheduleEndMappingOfPCPartitionConfig(partitionIndex)                      8u  /**< the number of accomplishable value elements in BswM_LinScheduleEndMapping */
#define BswM_GetSizeOfLinScheduleEndStateOfPCPartitionConfig(partitionIndex)                        8u  /**< the number of accomplishable value elements in BswM_LinScheduleEndState */
#define BswM_GetSizeOfLinScheduleMappingOfPCPartitionConfig(partitionIndex)                         8u  /**< the number of accomplishable value elements in BswM_LinScheduleMapping */
#define BswM_GetSizeOfLinScheduleStateOfPCPartitionConfig(partitionIndex)                           8u  /**< the number of accomplishable value elements in BswM_LinScheduleState */
#define BswM_GetSizeOfModeNotificationFctOfPCPartitionConfig(partitionIndex)                        1u  /**< the number of accomplishable value elements in BswM_ModeNotificationFct */
#define BswM_GetSizeOfModeRequestMappingOfPCPartitionConfig(partitionIndex)                         10u  /**< the number of accomplishable value elements in BswM_ModeRequestMapping */
#define BswM_GetSizeOfNvMJobMappingOfPCPartitionConfig(partitionIndex)                              1u  /**< the number of accomplishable value elements in BswM_NvMJobMapping */
#define BswM_GetSizeOfNvMJobStateOfPCPartitionConfig(partitionIndex)                                1u  /**< the number of accomplishable value elements in BswM_NvMJobState */
#define BswM_GetSizeOfRuleStatesOfPCPartitionConfig(partitionIndex)                                 140u  /**< the number of accomplishable value elements in BswM_RuleStates */
#define BswM_GetSizeOfRulesIndOfPCPartitionConfig(partitionIndex)                                   151u  /**< the number of accomplishable value elements in BswM_RulesInd */
#define BswM_GetSizeOfRulesOfPCPartitionConfig(partitionIndex)                                      140u  /**< the number of accomplishable value elements in BswM_Rules */
#define BswM_GetSizeOfSwcModeRequestUpdateFctOfPCPartitionConfig(partitionIndex)                    1u  /**< the number of accomplishable value elements in BswM_SwcModeRequestUpdateFct */
#define BswM_GetSizeOfTimerStateOfPCPartitionConfig(partitionIndex)                                 3u  /**< the number of accomplishable value elements in BswM_TimerState */
#define BswM_GetSizeOfTimerValueOfPCPartitionConfig(partitionIndex)                                 3u  /**< the number of accomplishable value elements in BswM_TimerValue */
#define BswM_GetSwcModeRequestUpdateFctOfPCPartitionConfig(partitionIndex)                          BswM_SwcModeRequestUpdateFct  /**< the pointer to BswM_SwcModeRequestUpdateFct */
#define BswM_GetTimerStateOfPCPartitionConfig(partitionIndex)                                       BswM_TimerState.raw  /**< the pointer to BswM_TimerState */
#define BswM_GetTimerValueOfPCPartitionConfig(partitionIndex)                                       BswM_TimerValue.raw  /**< the pointer to BswM_TimerValue */
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCGetDuplicatedRootDataMacros  BswM Get Duplicated Root Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated root data elements.
  \{
*/ 
#define BswM_GetSizeOfActionListQueueOfPCPartitionConfig(partitionIndex)                            BswM_GetSizeOfActionListsOfPCPartitionConfig(partitionIndex)  /**< the number of accomplishable value elements in BswM_ActionListQueue */
#define BswM_GetSizeOfModeRequestQueueOfPCPartitionConfig(partitionIndex)                           BswM_GetSizeOfImmediateUserOfPCPartitionConfig(partitionIndex)  /**< the number of accomplishable value elements in BswM_ModeRequestQueue */
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCGetDataMacros  BswM Get Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read CONST and VAR data.
  \{
*/ 
#define BswM_GetActionListQueue(Index, partitionIndex)                                              (BswM_GetActionListQueueOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetFctPtrOfActionLists(Index, partitionIndex)                                          (BswM_GetActionListsOfPCPartitionConfig(partitionIndex)[(Index)].FctPtrOfActionLists)
#define BswM_GetExternalIdOfCanSMChannelMapping(Index, partitionIndex)                              (BswM_GetCanSMChannelMappingOfPCPartitionConfig(partitionIndex)[(Index)].ExternalIdOfCanSMChannelMapping)
#define BswM_GetImmediateUserEndIdxOfCanSMChannelMapping(Index, partitionIndex)                     (BswM_GetCanSMChannelMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserEndIdxOfCanSMChannelMapping)
#define BswM_GetImmediateUserStartIdxOfCanSMChannelMapping(Index, partitionIndex)                   (BswM_GetCanSMChannelMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserStartIdxOfCanSMChannelMapping)
#define BswM_GetCanSMChannelState(Index, partitionIndex)                                            (BswM_GetCanSMChannelStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetExternalIdOfComMChannelMapping(Index, partitionIndex)                               (BswM_GetComMChannelMappingOfPCPartitionConfig(partitionIndex)[(Index)].ExternalIdOfComMChannelMapping)
#define BswM_GetComMChannelState(Index, partitionIndex)                                             (BswM_GetComMChannelStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetExternalIdOfDcmComMapping(Index, partitionIndex)                                    (BswM_GetDcmComMappingOfPCPartitionConfig(partitionIndex)[(Index)].ExternalIdOfDcmComMapping)
#define BswM_GetImmediateUserEndIdxOfDcmComMapping(Index, partitionIndex)                           (BswM_GetDcmComMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserEndIdxOfDcmComMapping)
#define BswM_GetImmediateUserStartIdxOfDcmComMapping(Index, partitionIndex)                         (BswM_GetDcmComMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserStartIdxOfDcmComMapping)
#define BswM_GetDcmComState(Index, partitionIndex)                                                  (BswM_GetDcmComStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetRulesIdxOfDeferredRules(Index, partitionIndex)                                      (BswM_GetDeferredRulesOfPCPartitionConfig(partitionIndex)[(Index)].RulesIdxOfDeferredRules)
#define BswM_GetForcedActionListPriority(partitionIndex)                                            ((*(BswM_GetForcedActionListPriorityOfPCPartitionConfig(partitionIndex))))
#define BswM_GetExternalIdOfGenericMapping(Index, partitionIndex)                                   (BswM_GetGenericMappingOfPCPartitionConfig(partitionIndex)[(Index)].ExternalIdOfGenericMapping)
#define BswM_GetImmediateUserEndIdxOfGenericMapping(Index, partitionIndex)                          (BswM_GetGenericMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserEndIdxOfGenericMapping)
#define BswM_GetImmediateUserStartIdxOfGenericMapping(Index, partitionIndex)                        (BswM_GetGenericMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserStartIdxOfGenericMapping)
#define BswM_GetInitValueOfGenericMapping(Index, partitionIndex)                                    (BswM_GetGenericMappingOfPCPartitionConfig(partitionIndex)[(Index)].InitValueOfGenericMapping)
#define BswM_GetGenericState(Index, partitionIndex)                                                 (BswM_GetGenericStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetMaskedBitsOfImmediateUser(Index, partitionIndex)                                    (BswM_GetImmediateUserOfPCPartitionConfig(partitionIndex)[(Index)].MaskedBitsOfImmediateUser)
#define BswM_GetRulesIndEndIdxOfImmediateUser(Index, partitionIndex)                                (BswM_GetImmediateUserOfPCPartitionConfig(partitionIndex)[(Index)].RulesIndEndIdxOfImmediateUser)
#define BswM_GetRulesIndStartIdxOfImmediateUser(Index, partitionIndex)                              (BswM_GetImmediateUserOfPCPartitionConfig(partitionIndex)[(Index)].RulesIndStartIdxOfImmediateUser)
#define BswM_GetInitGenVarAndInitAL(Index, partitionIndex)                                          (BswM_GetInitGenVarAndInitALOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_IsInitialized(partitionIndex)                                                          (((*(BswM_GetInitializedOfPCPartitionConfig(partitionIndex)))) != FALSE)
#define BswM_GetExternalIdOfJ1939NmMapping(Index, partitionIndex)                                   (BswM_GetJ1939NmMappingOfPCPartitionConfig(partitionIndex)[(Index)].ExternalIdOfJ1939NmMapping)
#define BswM_GetImmediateUserEndIdxOfJ1939NmMapping(Index, partitionIndex)                          (BswM_GetJ1939NmMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserEndIdxOfJ1939NmMapping)
#define BswM_GetImmediateUserStartIdxOfJ1939NmMapping(Index, partitionIndex)                        (BswM_GetJ1939NmMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserStartIdxOfJ1939NmMapping)
#define BswM_GetJ1939NmState(Index, partitionIndex)                                                 (BswM_GetJ1939NmStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetExternalIdOfLinSMMapping(Index, partitionIndex)                                     (BswM_GetLinSMMappingOfPCPartitionConfig(partitionIndex)[(Index)].ExternalIdOfLinSMMapping)
#define BswM_GetImmediateUserEndIdxOfLinSMMapping(Index, partitionIndex)                            (BswM_GetLinSMMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserEndIdxOfLinSMMapping)
#define BswM_GetImmediateUserStartIdxOfLinSMMapping(Index, partitionIndex)                          (BswM_GetLinSMMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserStartIdxOfLinSMMapping)
#define BswM_GetLinSMState(Index, partitionIndex)                                                   (BswM_GetLinSMStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetExternalIdOfLinScheduleEndMapping(Index, partitionIndex)                            (BswM_GetLinScheduleEndMappingOfPCPartitionConfig(partitionIndex)[(Index)].ExternalIdOfLinScheduleEndMapping)
#define BswM_GetImmediateUserEndIdxOfLinScheduleEndMapping(Index, partitionIndex)                   (BswM_GetLinScheduleEndMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserEndIdxOfLinScheduleEndMapping)
#define BswM_GetImmediateUserStartIdxOfLinScheduleEndMapping(Index, partitionIndex)                 (BswM_GetLinScheduleEndMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserStartIdxOfLinScheduleEndMapping)
#define BswM_GetLinScheduleEndState(Index, partitionIndex)                                          (BswM_GetLinScheduleEndStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetExternalIdOfLinScheduleMapping(Index, partitionIndex)                               (BswM_GetLinScheduleMappingOfPCPartitionConfig(partitionIndex)[(Index)].ExternalIdOfLinScheduleMapping)
#define BswM_GetImmediateUserEndIdxOfLinScheduleMapping(Index, partitionIndex)                      (BswM_GetLinScheduleMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserEndIdxOfLinScheduleMapping)
#define BswM_GetImmediateUserStartIdxOfLinScheduleMapping(Index, partitionIndex)                    (BswM_GetLinScheduleMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserStartIdxOfLinScheduleMapping)
#define BswM_GetLinScheduleState(Index, partitionIndex)                                             (BswM_GetLinScheduleStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetModeNotificationFct(Index, partitionIndex)                                          (BswM_GetModeNotificationFctOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetImmediateUserEndIdxOfModeRequestMapping(Index, partitionIndex)                      (BswM_GetModeRequestMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserEndIdxOfModeRequestMapping)
#define BswM_GetImmediateUserStartIdxOfModeRequestMapping(Index, partitionIndex)                    (BswM_GetModeRequestMappingOfPCPartitionConfig(partitionIndex)[(Index)].ImmediateUserStartIdxOfModeRequestMapping)
#define BswM_GetModeRequestQueue(Index, partitionIndex)                                             (BswM_GetModeRequestQueueOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetNvMJobState(Index, partitionIndex)                                                  (BswM_GetNvMJobStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetPCPartitionConfigIdxOfPartitionIdentifiers(Index)                                   (BswM_GetPartitionIdentifiersOfPCConfig()[(Index)].PCPartitionConfigIdxOfPartitionIdentifiers)
#define BswM_GetPartitionSNVOfPartitionIdentifiers(Index)                                           (BswM_GetPartitionIdentifiersOfPCConfig()[(Index)].PartitionSNVOfPartitionIdentifiers)
#define BswM_GetQueueSemaphore(partitionIndex)                                                      ((*(BswM_GetQueueSemaphoreOfPCPartitionConfig(partitionIndex))))
#define BswM_IsQueueWritten(partitionIndex)                                                         (((*(BswM_GetQueueWrittenOfPCPartitionConfig(partitionIndex)))) != FALSE)
#define BswM_GetRuleStates(Index, partitionIndex)                                                   (BswM_GetRuleStatesOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetFctPtrOfRules(Index, partitionIndex)                                                (BswM_GetRulesOfPCPartitionConfig(partitionIndex)[(Index)].FctPtrOfRules)
#define BswM_GetIdOfRules(Index, partitionIndex)                                                    (BswM_GetRulesOfPCPartitionConfig(partitionIndex)[(Index)].IdOfRules)
#define BswM_GetRuleStatesIdxOfRules(Index, partitionIndex)                                         (BswM_GetRulesOfPCPartitionConfig(partitionIndex)[(Index)].RuleStatesIdxOfRules)
#define BswM_GetRulesInd(Index, partitionIndex)                                                     (BswM_GetRulesIndOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetSwcModeRequestUpdateFct(Index, partitionIndex)                                      (BswM_GetSwcModeRequestUpdateFctOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetTimerState(Index, partitionIndex)                                                   (BswM_GetTimerStateOfPCPartitionConfig(partitionIndex)[(Index)])
#define BswM_GetTimerValue(Index, partitionIndex)                                                   (BswM_GetTimerValueOfPCPartitionConfig(partitionIndex)[(Index)])
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCGetBitDataMacros  BswM Get Bit Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read bitcoded data elements.
  \{
*/ 
#define BswM_IsOnInitOfImmediateUser(Index, partitionIndex)                                         (BSWM_ONINITOFIMMEDIATEUSER_MASK == (BswM_GetMaskedBitsOfImmediateUser(((Index)), (partitionIndex)) & BSWM_ONINITOFIMMEDIATEUSER_MASK))  /**< Arbitrate depending rules on initialization. */
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCGetDeduplicatedDataMacros  BswM Get Deduplicated Data Macros (PRE_COMPILE)
  \brief  These macros can be used to read deduplicated data elements.
  \{
*/ 
#define BswM_IsImmediateUserUsedOfCanSMChannelMapping(Index, partitionIndex)                        (((TRUE)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to BswM_ImmediateUser */
#define BswM_GetInitValueOfCanSMChannelMapping(Index, partitionIndex)                               CANSM_BSWM_NO_COMMUNICATION  /**< Initialization value of port. */
#define BswM_GetInitValueOfComMChannelMapping(Index, partitionIndex)                                COMM_NO_COMMUNICATION  /**< Initialization value of port. */
#define BswM_IsImmediateUserUsedOfDcmComMapping(Index, partitionIndex)                              (((TRUE)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to BswM_ImmediateUser */
#define BswM_GetInitValueOfDcmComMapping(Index, partitionIndex)                                     DCM_ENABLE_RX_TX_NORM  /**< Initialization value of port. */
#define BswM_IsImmediateUserUsedOfGenericMapping(Index, partitionIndex)                             (((boolean)(BswM_GetImmediateUserStartIdxOfGenericMapping(((Index)), (partitionIndex)) != BSWM_NO_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to BswM_ImmediateUser */
#define BswM_IsRulesIndUsedOfImmediateUser(Index, partitionIndex)                                   (((boolean)(BswM_GetRulesIndStartIdxOfImmediateUser(((Index)), (partitionIndex)) != BSWM_NO_RULESINDSTARTIDXOFIMMEDIATEUSER)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to BswM_RulesInd */
#define BswM_IsImmediateUserUsedOfJ1939NmMapping(Index, partitionIndex)                             (((TRUE)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to BswM_ImmediateUser */
#define BswM_GetInitValueOfJ1939NmMapping(Index, partitionIndex)                                    NM_STATE_BUS_SLEEP  /**< Initialization value of port. */
#define BswM_IsImmediateUserUsedOfLinSMMapping(Index, partitionIndex)                               (((TRUE)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to BswM_ImmediateUser */
#define BswM_GetInitValueOfLinSMMapping(Index, partitionIndex)                                      LINSM_BSWM_NO_COM  /**< Initialization value of port. */
#define BswM_IsImmediateUserUsedOfLinScheduleEndMapping(Index, partitionIndex)                      (((TRUE)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to BswM_ImmediateUser */
#define BswM_GetInitValueOfLinScheduleEndMapping(Index, partitionIndex)                             0  /**< Initialization value of port. */
#define BswM_IsImmediateUserUsedOfLinScheduleMapping(Index, partitionIndex)                         (((TRUE)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to BswM_ImmediateUser */
#define BswM_GetInitValueOfLinScheduleMapping(Index, partitionIndex)                                0  /**< Initialization value of port. */
#define BswM_IsImmediateUserUsedOfModeRequestMapping(Index, partitionIndex)                         (((boolean)(BswM_GetImmediateUserStartIdxOfModeRequestMapping(((Index)), (partitionIndex)) != BSWM_NO_IMMEDIATEUSERSTARTIDXOFMODEREQUESTMAPPING)) != FALSE)  /**< TRUE, if the 0:n relation has 1 relation pointing to BswM_ImmediateUser */
#define BswM_GetExternalIdOfNvMJobMapping(Index, partitionIndex)                                    NVM_SERVICE_ID_WRITEALL  /**< External id of BswMNvMJobModeIndication. */
#define BswM_GetInitValueOfNvMJobMapping(Index, partitionIndex)                                     NVM_REQ_OK  /**< Initialization value of port. */
#define BswM_GetInitOfRules(Index, partitionIndex)                                                  BSWM_FALSE  /**< Initialization value of rule state (TRUE, FALSE, UNDEFINED or DEACTIVATED). */
#define BswM_GetSizeOfActionListQueue(partitionIndex)                                               BswM_GetSizeOfActionListQueueOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfActionLists(partitionIndex)                                                   BswM_GetSizeOfActionListsOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfCanSMChannelMapping(partitionIndex)                                           BswM_GetSizeOfCanSMChannelMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfCanSMChannelState(partitionIndex)                                             BswM_GetSizeOfCanSMChannelStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfComMChannelMapping(partitionIndex)                                            BswM_GetSizeOfComMChannelMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfComMChannelState(partitionIndex)                                              BswM_GetSizeOfComMChannelStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfDcmComMapping(partitionIndex)                                                 BswM_GetSizeOfDcmComMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfDcmComState(partitionIndex)                                                   BswM_GetSizeOfDcmComStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfDeferredRules(partitionIndex)                                                 BswM_GetSizeOfDeferredRulesOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfGenericMapping(partitionIndex)                                                BswM_GetSizeOfGenericMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfGenericState(partitionIndex)                                                  BswM_GetSizeOfGenericStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfImmediateUser(partitionIndex)                                                 BswM_GetSizeOfImmediateUserOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfInitGenVarAndInitAL(partitionIndex)                                           BswM_GetSizeOfInitGenVarAndInitALOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfJ1939NmMapping(partitionIndex)                                                BswM_GetSizeOfJ1939NmMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfJ1939NmState(partitionIndex)                                                  BswM_GetSizeOfJ1939NmStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfLinSMMapping(partitionIndex)                                                  BswM_GetSizeOfLinSMMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfLinSMState(partitionIndex)                                                    BswM_GetSizeOfLinSMStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfLinScheduleEndMapping(partitionIndex)                                         BswM_GetSizeOfLinScheduleEndMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfLinScheduleEndState(partitionIndex)                                           BswM_GetSizeOfLinScheduleEndStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfLinScheduleMapping(partitionIndex)                                            BswM_GetSizeOfLinScheduleMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfLinScheduleState(partitionIndex)                                              BswM_GetSizeOfLinScheduleStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfModeNotificationFct(partitionIndex)                                           BswM_GetSizeOfModeNotificationFctOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfModeRequestMapping(partitionIndex)                                            BswM_GetSizeOfModeRequestMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfModeRequestQueue(partitionIndex)                                              BswM_GetSizeOfModeRequestQueueOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfNvMJobMapping(partitionIndex)                                                 BswM_GetSizeOfNvMJobMappingOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfNvMJobState(partitionIndex)                                                   BswM_GetSizeOfNvMJobStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfPartitionIdentifiers()                                                        BswM_GetSizeOfPartitionIdentifiersOfPCConfig()
#define BswM_GetSizeOfRuleStates(partitionIndex)                                                    BswM_GetSizeOfRuleStatesOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfRules(partitionIndex)                                                         BswM_GetSizeOfRulesOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfRulesInd(partitionIndex)                                                      BswM_GetSizeOfRulesIndOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfSwcModeRequestUpdateFct(partitionIndex)                                       BswM_GetSizeOfSwcModeRequestUpdateFctOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfTimerState(partitionIndex)                                                    BswM_GetSizeOfTimerStateOfPCPartitionConfig(partitionIndex)
#define BswM_GetSizeOfTimerValue(partitionIndex)                                                    BswM_GetSizeOfTimerValueOfPCPartitionConfig(partitionIndex)
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCSetDataMacros  BswM Set Data Macros (PRE_COMPILE)
  \brief  These macros can be used to write data.
  \{
*/ 
#define BswM_SetActionListQueue(Index, Value, partitionIndex)                                       BswM_GetActionListQueueOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetCanSMChannelState(Index, Value, partitionIndex)                                     BswM_GetCanSMChannelStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetComMChannelState(Index, Value, partitionIndex)                                      BswM_GetComMChannelStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetDcmComState(Index, Value, partitionIndex)                                           BswM_GetDcmComStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetForcedActionListPriority(Value, partitionIndex)                                     (*(BswM_GetForcedActionListPriorityOfPCPartitionConfig(partitionIndex))) = (Value)
#define BswM_SetGenericState(Index, Value, partitionIndex)                                          BswM_GetGenericStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetInitialized(Value, partitionIndex)                                                  (*(BswM_GetInitializedOfPCPartitionConfig(partitionIndex))) = (Value)
#define BswM_SetJ1939NmState(Index, Value, partitionIndex)                                          BswM_GetJ1939NmStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetLinSMState(Index, Value, partitionIndex)                                            BswM_GetLinSMStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetLinScheduleEndState(Index, Value, partitionIndex)                                   BswM_GetLinScheduleEndStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetLinScheduleState(Index, Value, partitionIndex)                                      BswM_GetLinScheduleStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetModeRequestQueue(Index, Value, partitionIndex)                                      BswM_GetModeRequestQueueOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetNvMJobState(Index, Value, partitionIndex)                                           BswM_GetNvMJobStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetQueueSemaphore(Value, partitionIndex)                                               (*(BswM_GetQueueSemaphoreOfPCPartitionConfig(partitionIndex))) = (Value)
#define BswM_SetQueueWritten(Value, partitionIndex)                                                 (*(BswM_GetQueueWrittenOfPCPartitionConfig(partitionIndex))) = (Value)
#define BswM_SetRuleStates(Index, Value, partitionIndex)                                            BswM_GetRuleStatesOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetTimerState(Index, Value, partitionIndex)                                            BswM_GetTimerStateOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
#define BswM_SetTimerValue(Index, Value, partitionIndex)                                            BswM_GetTimerValueOfPCPartitionConfig(partitionIndex)[(Index)] = (Value)
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCHasMacros  BswM Has Macros (PRE_COMPILE)
  \brief  These macros can be used to detect at runtime a deactivated piece of information. TRUE in the CONFIGURATION_VARIANT PRE-COMPILE, TRUE or FALSE in the CONFIGURATION_VARIANT POST-BUILD.
  \{
*/ 
#define BswM_HasActionListQueue(partitionIndex)                                                     (TRUE != FALSE)
#define BswM_HasActionLists(partitionIndex)                                                         (TRUE != FALSE)
#define BswM_HasFctPtrOfActionLists(partitionIndex)                                                 (TRUE != FALSE)
#define BswM_HasCanSMChannelMapping(partitionIndex)                                                 (TRUE != FALSE)
#define BswM_HasExternalIdOfCanSMChannelMapping(partitionIndex)                                     (TRUE != FALSE)
#define BswM_HasImmediateUserEndIdxOfCanSMChannelMapping(partitionIndex)                            (TRUE != FALSE)
#define BswM_HasImmediateUserStartIdxOfCanSMChannelMapping(partitionIndex)                          (TRUE != FALSE)
#define BswM_HasImmediateUserUsedOfCanSMChannelMapping(partitionIndex)                              (TRUE != FALSE)
#define BswM_HasInitValueOfCanSMChannelMapping(partitionIndex)                                      (TRUE != FALSE)
#define BswM_HasCanSMChannelState(partitionIndex)                                                   (TRUE != FALSE)
#define BswM_HasComMChannelMapping(partitionIndex)                                                  (TRUE != FALSE)
#define BswM_HasExternalIdOfComMChannelMapping(partitionIndex)                                      (TRUE != FALSE)
#define BswM_HasInitValueOfComMChannelMapping(partitionIndex)                                       (TRUE != FALSE)
#define BswM_HasComMChannelState(partitionIndex)                                                    (TRUE != FALSE)
#define BswM_HasDcmComMapping(partitionIndex)                                                       (TRUE != FALSE)
#define BswM_HasExternalIdOfDcmComMapping(partitionIndex)                                           (TRUE != FALSE)
#define BswM_HasImmediateUserEndIdxOfDcmComMapping(partitionIndex)                                  (TRUE != FALSE)
#define BswM_HasImmediateUserStartIdxOfDcmComMapping(partitionIndex)                                (TRUE != FALSE)
#define BswM_HasImmediateUserUsedOfDcmComMapping(partitionIndex)                                    (TRUE != FALSE)
#define BswM_HasInitValueOfDcmComMapping(partitionIndex)                                            (TRUE != FALSE)
#define BswM_HasDcmComState(partitionIndex)                                                         (TRUE != FALSE)
#define BswM_HasDeferredRules(partitionIndex)                                                       (TRUE != FALSE)
#define BswM_HasRulesIdxOfDeferredRules(partitionIndex)                                             (TRUE != FALSE)
#define BswM_HasForcedActionListPriority(partitionIndex)                                            (TRUE != FALSE)
#define BswM_HasGenericMapping(partitionIndex)                                                      (TRUE != FALSE)
#define BswM_HasExternalIdOfGenericMapping(partitionIndex)                                          (TRUE != FALSE)
#define BswM_HasImmediateUserEndIdxOfGenericMapping(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasImmediateUserStartIdxOfGenericMapping(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasImmediateUserUsedOfGenericMapping(partitionIndex)                                   (TRUE != FALSE)
#define BswM_HasInitValueOfGenericMapping(partitionIndex)                                           (TRUE != FALSE)
#define BswM_HasGenericState(partitionIndex)                                                        (TRUE != FALSE)
#define BswM_HasImmediateUser(partitionIndex)                                                       (TRUE != FALSE)
#define BswM_HasMaskedBitsOfImmediateUser(partitionIndex)                                           (TRUE != FALSE)
#define BswM_HasOnInitOfImmediateUser(partitionIndex)                                               (TRUE != FALSE)
#define BswM_HasRulesIndEndIdxOfImmediateUser(partitionIndex)                                       (TRUE != FALSE)
#define BswM_HasRulesIndStartIdxOfImmediateUser(partitionIndex)                                     (TRUE != FALSE)
#define BswM_HasRulesIndUsedOfImmediateUser(partitionIndex)                                         (TRUE != FALSE)
#define BswM_HasInitGenVarAndInitAL(partitionIndex)                                                 (TRUE != FALSE)
#define BswM_HasInitialized(partitionIndex)                                                         (TRUE != FALSE)
#define BswM_HasJ1939NmMapping(partitionIndex)                                                      (TRUE != FALSE)
#define BswM_HasExternalIdOfJ1939NmMapping(partitionIndex)                                          (TRUE != FALSE)
#define BswM_HasImmediateUserEndIdxOfJ1939NmMapping(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasImmediateUserStartIdxOfJ1939NmMapping(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasImmediateUserUsedOfJ1939NmMapping(partitionIndex)                                   (TRUE != FALSE)
#define BswM_HasInitValueOfJ1939NmMapping(partitionIndex)                                           (TRUE != FALSE)
#define BswM_HasJ1939NmState(partitionIndex)                                                        (TRUE != FALSE)
#define BswM_HasLinSMMapping(partitionIndex)                                                        (TRUE != FALSE)
#define BswM_HasExternalIdOfLinSMMapping(partitionIndex)                                            (TRUE != FALSE)
#define BswM_HasImmediateUserEndIdxOfLinSMMapping(partitionIndex)                                   (TRUE != FALSE)
#define BswM_HasImmediateUserStartIdxOfLinSMMapping(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasImmediateUserUsedOfLinSMMapping(partitionIndex)                                     (TRUE != FALSE)
#define BswM_HasInitValueOfLinSMMapping(partitionIndex)                                             (TRUE != FALSE)
#define BswM_HasLinSMState(partitionIndex)                                                          (TRUE != FALSE)
#define BswM_HasLinScheduleEndMapping(partitionIndex)                                               (TRUE != FALSE)
#define BswM_HasExternalIdOfLinScheduleEndMapping(partitionIndex)                                   (TRUE != FALSE)
#define BswM_HasImmediateUserEndIdxOfLinScheduleEndMapping(partitionIndex)                          (TRUE != FALSE)
#define BswM_HasImmediateUserStartIdxOfLinScheduleEndMapping(partitionIndex)                        (TRUE != FALSE)
#define BswM_HasImmediateUserUsedOfLinScheduleEndMapping(partitionIndex)                            (TRUE != FALSE)
#define BswM_HasInitValueOfLinScheduleEndMapping(partitionIndex)                                    (TRUE != FALSE)
#define BswM_HasLinScheduleEndState(partitionIndex)                                                 (TRUE != FALSE)
#define BswM_HasLinScheduleMapping(partitionIndex)                                                  (TRUE != FALSE)
#define BswM_HasExternalIdOfLinScheduleMapping(partitionIndex)                                      (TRUE != FALSE)
#define BswM_HasImmediateUserEndIdxOfLinScheduleMapping(partitionIndex)                             (TRUE != FALSE)
#define BswM_HasImmediateUserStartIdxOfLinScheduleMapping(partitionIndex)                           (TRUE != FALSE)
#define BswM_HasImmediateUserUsedOfLinScheduleMapping(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasInitValueOfLinScheduleMapping(partitionIndex)                                       (TRUE != FALSE)
#define BswM_HasLinScheduleState(partitionIndex)                                                    (TRUE != FALSE)
#define BswM_HasModeNotificationFct(partitionIndex)                                                 (TRUE != FALSE)
#define BswM_HasModeRequestMapping(partitionIndex)                                                  (TRUE != FALSE)
#define BswM_HasImmediateUserEndIdxOfModeRequestMapping(partitionIndex)                             (TRUE != FALSE)
#define BswM_HasImmediateUserStartIdxOfModeRequestMapping(partitionIndex)                           (TRUE != FALSE)
#define BswM_HasImmediateUserUsedOfModeRequestMapping(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasModeRequestQueue(partitionIndex)                                                    (TRUE != FALSE)
#define BswM_HasNvMJobMapping(partitionIndex)                                                       (TRUE != FALSE)
#define BswM_HasExternalIdOfNvMJobMapping(partitionIndex)                                           (TRUE != FALSE)
#define BswM_HasInitValueOfNvMJobMapping(partitionIndex)                                            (TRUE != FALSE)
#define BswM_HasNvMJobState(partitionIndex)                                                         (TRUE != FALSE)
#define BswM_HasPartitionIdentifiers()                                                              (TRUE != FALSE)
#define BswM_HasPCPartitionConfigIdxOfPartitionIdentifiers()                                        (TRUE != FALSE)
#define BswM_HasPartitionSNVOfPartitionIdentifiers()                                                (TRUE != FALSE)
#define BswM_HasQueueSemaphore(partitionIndex)                                                      (TRUE != FALSE)
#define BswM_HasQueueWritten(partitionIndex)                                                        (TRUE != FALSE)
#define BswM_HasRuleStates(partitionIndex)                                                          (TRUE != FALSE)
#define BswM_HasRules(partitionIndex)                                                               (TRUE != FALSE)
#define BswM_HasFctPtrOfRules(partitionIndex)                                                       (TRUE != FALSE)
#define BswM_HasIdOfRules(partitionIndex)                                                           (TRUE != FALSE)
#define BswM_HasInitOfRules(partitionIndex)                                                         (TRUE != FALSE)
#define BswM_HasRuleStatesIdxOfRules(partitionIndex)                                                (TRUE != FALSE)
#define BswM_HasRulesInd(partitionIndex)                                                            (TRUE != FALSE)
#define BswM_HasSizeOfActionListQueue(partitionIndex)                                               (TRUE != FALSE)
#define BswM_HasSizeOfActionLists(partitionIndex)                                                   (TRUE != FALSE)
#define BswM_HasSizeOfCanSMChannelMapping(partitionIndex)                                           (TRUE != FALSE)
#define BswM_HasSizeOfCanSMChannelState(partitionIndex)                                             (TRUE != FALSE)
#define BswM_HasSizeOfComMChannelMapping(partitionIndex)                                            (TRUE != FALSE)
#define BswM_HasSizeOfComMChannelState(partitionIndex)                                              (TRUE != FALSE)
#define BswM_HasSizeOfDcmComMapping(partitionIndex)                                                 (TRUE != FALSE)
#define BswM_HasSizeOfDcmComState(partitionIndex)                                                   (TRUE != FALSE)
#define BswM_HasSizeOfDeferredRules(partitionIndex)                                                 (TRUE != FALSE)
#define BswM_HasSizeOfGenericMapping(partitionIndex)                                                (TRUE != FALSE)
#define BswM_HasSizeOfGenericState(partitionIndex)                                                  (TRUE != FALSE)
#define BswM_HasSizeOfImmediateUser(partitionIndex)                                                 (TRUE != FALSE)
#define BswM_HasSizeOfInitGenVarAndInitAL(partitionIndex)                                           (TRUE != FALSE)
#define BswM_HasSizeOfJ1939NmMapping(partitionIndex)                                                (TRUE != FALSE)
#define BswM_HasSizeOfJ1939NmState(partitionIndex)                                                  (TRUE != FALSE)
#define BswM_HasSizeOfLinSMMapping(partitionIndex)                                                  (TRUE != FALSE)
#define BswM_HasSizeOfLinSMState(partitionIndex)                                                    (TRUE != FALSE)
#define BswM_HasSizeOfLinScheduleEndMapping(partitionIndex)                                         (TRUE != FALSE)
#define BswM_HasSizeOfLinScheduleEndState(partitionIndex)                                           (TRUE != FALSE)
#define BswM_HasSizeOfLinScheduleMapping(partitionIndex)                                            (TRUE != FALSE)
#define BswM_HasSizeOfLinScheduleState(partitionIndex)                                              (TRUE != FALSE)
#define BswM_HasSizeOfModeNotificationFct(partitionIndex)                                           (TRUE != FALSE)
#define BswM_HasSizeOfModeRequestMapping(partitionIndex)                                            (TRUE != FALSE)
#define BswM_HasSizeOfModeRequestQueue(partitionIndex)                                              (TRUE != FALSE)
#define BswM_HasSizeOfNvMJobMapping(partitionIndex)                                                 (TRUE != FALSE)
#define BswM_HasSizeOfNvMJobState(partitionIndex)                                                   (TRUE != FALSE)
#define BswM_HasSizeOfPartitionIdentifiers()                                                        (TRUE != FALSE)
#define BswM_HasSizeOfRuleStates(partitionIndex)                                                    (TRUE != FALSE)
#define BswM_HasSizeOfRules(partitionIndex)                                                         (TRUE != FALSE)
#define BswM_HasSizeOfRulesInd(partitionIndex)                                                      (TRUE != FALSE)
#define BswM_HasSizeOfSwcModeRequestUpdateFct(partitionIndex)                                       (TRUE != FALSE)
#define BswM_HasSizeOfTimerState(partitionIndex)                                                    (TRUE != FALSE)
#define BswM_HasSizeOfTimerValue(partitionIndex)                                                    (TRUE != FALSE)
#define BswM_HasSwcModeRequestUpdateFct(partitionIndex)                                             (TRUE != FALSE)
#define BswM_HasTimerState(partitionIndex)                                                          (TRUE != FALSE)
#define BswM_HasTimerValue(partitionIndex)                                                          (TRUE != FALSE)
#define BswM_HasPCConfig()                                                                          (TRUE != FALSE)
#define BswM_HasPCPartitionConfigOfPCConfig()                                                       (TRUE != FALSE)
#define BswM_HasPartitionIdentifiersOfPCConfig()                                                    (TRUE != FALSE)
#define BswM_HasSizeOfPartitionIdentifiersOfPCConfig()                                              (TRUE != FALSE)
#define BswM_HasPCPartitionConfig()                                                                 (TRUE != FALSE)
#define BswM_HasActionListQueueOfPCPartitionConfig(partitionIndex)                                  (TRUE != FALSE)
#define BswM_HasActionListsOfPCPartitionConfig(partitionIndex)                                      (TRUE != FALSE)
#define BswM_HasCanSMChannelMappingOfPCPartitionConfig(partitionIndex)                              (TRUE != FALSE)
#define BswM_HasCanSMChannelStateOfPCPartitionConfig(partitionIndex)                                (TRUE != FALSE)
#define BswM_HasComMChannelMappingOfPCPartitionConfig(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasComMChannelStateOfPCPartitionConfig(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasDcmComMappingOfPCPartitionConfig(partitionIndex)                                    (TRUE != FALSE)
#define BswM_HasDcmComStateOfPCPartitionConfig(partitionIndex)                                      (TRUE != FALSE)
#define BswM_HasDeferredRulesOfPCPartitionConfig(partitionIndex)                                    (TRUE != FALSE)
#define BswM_HasForcedActionListPriorityOfPCPartitionConfig(partitionIndex)                         (TRUE != FALSE)
#define BswM_HasGenericMappingOfPCPartitionConfig(partitionIndex)                                   (TRUE != FALSE)
#define BswM_HasGenericStateOfPCPartitionConfig(partitionIndex)                                     (TRUE != FALSE)
#define BswM_HasImmediateUserOfPCPartitionConfig(partitionIndex)                                    (TRUE != FALSE)
#define BswM_HasInitGenVarAndInitALOfPCPartitionConfig(partitionIndex)                              (TRUE != FALSE)
#define BswM_HasInitializedOfPCPartitionConfig(partitionIndex)                                      (TRUE != FALSE)
#define BswM_HasJ1939NmMappingOfPCPartitionConfig(partitionIndex)                                   (TRUE != FALSE)
#define BswM_HasJ1939NmStateOfPCPartitionConfig(partitionIndex)                                     (TRUE != FALSE)
#define BswM_HasLinSMMappingOfPCPartitionConfig(partitionIndex)                                     (TRUE != FALSE)
#define BswM_HasLinSMStateOfPCPartitionConfig(partitionIndex)                                       (TRUE != FALSE)
#define BswM_HasLinScheduleEndMappingOfPCPartitionConfig(partitionIndex)                            (TRUE != FALSE)
#define BswM_HasLinScheduleEndStateOfPCPartitionConfig(partitionIndex)                              (TRUE != FALSE)
#define BswM_HasLinScheduleMappingOfPCPartitionConfig(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasLinScheduleStateOfPCPartitionConfig(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasModeNotificationFctOfPCPartitionConfig(partitionIndex)                              (TRUE != FALSE)
#define BswM_HasModeRequestMappingOfPCPartitionConfig(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasModeRequestQueueOfPCPartitionConfig(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasNvMJobMappingOfPCPartitionConfig(partitionIndex)                                    (TRUE != FALSE)
#define BswM_HasNvMJobStateOfPCPartitionConfig(partitionIndex)                                      (TRUE != FALSE)
#define BswM_HasQueueSemaphoreOfPCPartitionConfig(partitionIndex)                                   (TRUE != FALSE)
#define BswM_HasQueueWrittenOfPCPartitionConfig(partitionIndex)                                     (TRUE != FALSE)
#define BswM_HasRuleStatesOfPCPartitionConfig(partitionIndex)                                       (TRUE != FALSE)
#define BswM_HasRulesIndOfPCPartitionConfig(partitionIndex)                                         (TRUE != FALSE)
#define BswM_HasRulesOfPCPartitionConfig(partitionIndex)                                            (TRUE != FALSE)
#define BswM_HasSizeOfActionListQueueOfPCPartitionConfig(partitionIndex)                            (TRUE != FALSE)
#define BswM_HasSizeOfActionListsOfPCPartitionConfig(partitionIndex)                                (TRUE != FALSE)
#define BswM_HasSizeOfCanSMChannelMappingOfPCPartitionConfig(partitionIndex)                        (TRUE != FALSE)
#define BswM_HasSizeOfCanSMChannelStateOfPCPartitionConfig(partitionIndex)                          (TRUE != FALSE)
#define BswM_HasSizeOfComMChannelMappingOfPCPartitionConfig(partitionIndex)                         (TRUE != FALSE)
#define BswM_HasSizeOfComMChannelStateOfPCPartitionConfig(partitionIndex)                           (TRUE != FALSE)
#define BswM_HasSizeOfDcmComMappingOfPCPartitionConfig(partitionIndex)                              (TRUE != FALSE)
#define BswM_HasSizeOfDcmComStateOfPCPartitionConfig(partitionIndex)                                (TRUE != FALSE)
#define BswM_HasSizeOfDeferredRulesOfPCPartitionConfig(partitionIndex)                              (TRUE != FALSE)
#define BswM_HasSizeOfGenericMappingOfPCPartitionConfig(partitionIndex)                             (TRUE != FALSE)
#define BswM_HasSizeOfGenericStateOfPCPartitionConfig(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasSizeOfImmediateUserOfPCPartitionConfig(partitionIndex)                              (TRUE != FALSE)
#define BswM_HasSizeOfInitGenVarAndInitALOfPCPartitionConfig(partitionIndex)                        (TRUE != FALSE)
#define BswM_HasSizeOfJ1939NmMappingOfPCPartitionConfig(partitionIndex)                             (TRUE != FALSE)
#define BswM_HasSizeOfJ1939NmStateOfPCPartitionConfig(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasSizeOfLinSMMappingOfPCPartitionConfig(partitionIndex)                               (TRUE != FALSE)
#define BswM_HasSizeOfLinSMStateOfPCPartitionConfig(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasSizeOfLinScheduleEndMappingOfPCPartitionConfig(partitionIndex)                      (TRUE != FALSE)
#define BswM_HasSizeOfLinScheduleEndStateOfPCPartitionConfig(partitionIndex)                        (TRUE != FALSE)
#define BswM_HasSizeOfLinScheduleMappingOfPCPartitionConfig(partitionIndex)                         (TRUE != FALSE)
#define BswM_HasSizeOfLinScheduleStateOfPCPartitionConfig(partitionIndex)                           (TRUE != FALSE)
#define BswM_HasSizeOfModeNotificationFctOfPCPartitionConfig(partitionIndex)                        (TRUE != FALSE)
#define BswM_HasSizeOfModeRequestMappingOfPCPartitionConfig(partitionIndex)                         (TRUE != FALSE)
#define BswM_HasSizeOfModeRequestQueueOfPCPartitionConfig(partitionIndex)                           (TRUE != FALSE)
#define BswM_HasSizeOfNvMJobMappingOfPCPartitionConfig(partitionIndex)                              (TRUE != FALSE)
#define BswM_HasSizeOfNvMJobStateOfPCPartitionConfig(partitionIndex)                                (TRUE != FALSE)
#define BswM_HasSizeOfRuleStatesOfPCPartitionConfig(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasSizeOfRulesIndOfPCPartitionConfig(partitionIndex)                                   (TRUE != FALSE)
#define BswM_HasSizeOfRulesOfPCPartitionConfig(partitionIndex)                                      (TRUE != FALSE)
#define BswM_HasSizeOfSwcModeRequestUpdateFctOfPCPartitionConfig(partitionIndex)                    (TRUE != FALSE)
#define BswM_HasSizeOfTimerStateOfPCPartitionConfig(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasSizeOfTimerValueOfPCPartitionConfig(partitionIndex)                                 (TRUE != FALSE)
#define BswM_HasSwcModeRequestUpdateFctOfPCPartitionConfig(partitionIndex)                          (TRUE != FALSE)
#define BswM_HasTimerStateOfPCPartitionConfig(partitionIndex)                                       (TRUE != FALSE)
#define BswM_HasTimerValueOfPCPartitionConfig(partitionIndex)                                       (TRUE != FALSE)
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCIncrementDataMacros  BswM Increment Data Macros (PRE_COMPILE)
  \brief  These macros can be used to increment VAR data with numerical nature.
  \{
*/ 
#define BswM_IncActionListQueue(Index, partitionIndex)                                              BswM_GetActionListQueue(((Index)), (partitionIndex))++
#define BswM_IncCanSMChannelState(Index, partitionIndex)                                            BswM_GetCanSMChannelState(((Index)), (partitionIndex))++
#define BswM_IncComMChannelState(Index, partitionIndex)                                             BswM_GetComMChannelState(((Index)), (partitionIndex))++
#define BswM_IncDcmComState(Index, partitionIndex)                                                  BswM_GetDcmComState(((Index)), (partitionIndex))++
#define BswM_IncForcedActionListPriority(partitionIndex)                                            BswM_GetForcedActionListPriority(partitionIndex)++
#define BswM_IncGenericState(Index, partitionIndex)                                                 BswM_GetGenericState(((Index)), (partitionIndex))++
#define BswM_IncJ1939NmState(Index, partitionIndex)                                                 BswM_GetJ1939NmState(((Index)), (partitionIndex))++
#define BswM_IncLinSMState(Index, partitionIndex)                                                   BswM_GetLinSMState(((Index)), (partitionIndex))++
#define BswM_IncLinScheduleEndState(Index, partitionIndex)                                          BswM_GetLinScheduleEndState(((Index)), (partitionIndex))++
#define BswM_IncLinScheduleState(Index, partitionIndex)                                             BswM_GetLinScheduleState(((Index)), (partitionIndex))++
#define BswM_IncModeRequestQueue(Index, partitionIndex)                                             BswM_GetModeRequestQueue(((Index)), (partitionIndex))++
#define BswM_IncNvMJobState(Index, partitionIndex)                                                  BswM_GetNvMJobState(((Index)), (partitionIndex))++
#define BswM_IncQueueSemaphore(partitionIndex)                                                      BswM_GetQueueSemaphore(partitionIndex)++
#define BswM_IncRuleStates(Index, partitionIndex)                                                   BswM_GetRuleStates(((Index)), (partitionIndex))++
#define BswM_IncTimerState(Index, partitionIndex)                                                   BswM_GetTimerState(((Index)), (partitionIndex))++
#define BswM_IncTimerValue(Index, partitionIndex)                                                   BswM_GetTimerValue(((Index)), (partitionIndex))++
/** 
  \}
*/ 

/** 
  \defgroup  BswMPCDecrementDataMacros  BswM Decrement Data Macros (PRE_COMPILE)
  \brief  These macros can be used to decrement VAR data with numerical nature.
  \{
*/ 
#define BswM_DecActionListQueue(Index, partitionIndex)                                              BswM_GetActionListQueue(((Index)), (partitionIndex))--
#define BswM_DecCanSMChannelState(Index, partitionIndex)                                            BswM_GetCanSMChannelState(((Index)), (partitionIndex))--
#define BswM_DecComMChannelState(Index, partitionIndex)                                             BswM_GetComMChannelState(((Index)), (partitionIndex))--
#define BswM_DecDcmComState(Index, partitionIndex)                                                  BswM_GetDcmComState(((Index)), (partitionIndex))--
#define BswM_DecForcedActionListPriority(partitionIndex)                                            BswM_GetForcedActionListPriority(partitionIndex)--
#define BswM_DecGenericState(Index, partitionIndex)                                                 BswM_GetGenericState(((Index)), (partitionIndex))--
#define BswM_DecJ1939NmState(Index, partitionIndex)                                                 BswM_GetJ1939NmState(((Index)), (partitionIndex))--
#define BswM_DecLinSMState(Index, partitionIndex)                                                   BswM_GetLinSMState(((Index)), (partitionIndex))--
#define BswM_DecLinScheduleEndState(Index, partitionIndex)                                          BswM_GetLinScheduleEndState(((Index)), (partitionIndex))--
#define BswM_DecLinScheduleState(Index, partitionIndex)                                             BswM_GetLinScheduleState(((Index)), (partitionIndex))--
#define BswM_DecModeRequestQueue(Index, partitionIndex)                                             BswM_GetModeRequestQueue(((Index)), (partitionIndex))--
#define BswM_DecNvMJobState(Index, partitionIndex)                                                  BswM_GetNvMJobState(((Index)), (partitionIndex))--
#define BswM_DecQueueSemaphore(partitionIndex)                                                      BswM_GetQueueSemaphore(partitionIndex)--
#define BswM_DecRuleStates(Index, partitionIndex)                                                   BswM_GetRuleStates(((Index)), (partitionIndex))--
#define BswM_DecTimerState(Index, partitionIndex)                                                   BswM_GetTimerState(((Index)), (partitionIndex))--
#define BswM_DecTimerValue(Index, partitionIndex)                                                   BswM_GetTimerValue(((Index)), (partitionIndex))--
/** 
  \}
*/ 

  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


/** 
  \defgroup  DataAccessMacros  Data Access Macros
  \brief  generated data access macros to abstract the generated data from the code to read and write CONST or VAR data.
  \{
*/ 
  /* PRQA S 3453 Macros_3453 */  /* MD_MSR_FctLikeMacro */
/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION MACROS
**********************************************************************************************************************/
  /* PRQA L:Macros_3453 */
/** 
  \}
*/ 

/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL ACCESS FUNCTION MACROS
**********************************************************************************************************************/


#define BswM_GetPartitionContext() 0u

/* PRQA S 3453 1 */ /* MD_BswM_3453 */
#define BswM_SetIpduGroup(pduId, bitVal) Com_SetIpduGroup(BswM_ComIPduGroupState, (pduId), (bitVal))
/* PRQA S 3453 1 */ /* MD_BswM_3453 */
#define BswM_SetIpduReinitGroup(pduId, bitVal) Com_SetIpduGroup(BswM_ComIPduGroupReinitState, (pduId), (bitVal))
/* PRQA S 3453 1 */ /* MD_BswM_3453 */
#define BswM_SetIpduDMGroup(pduId, bitVal) Com_SetIpduGroup(BswM_ComRxIPduGroupDMState, (pduId), (bitVal))

/* PRQA S 3453 1 */ /* MD_BswM_3453 */
#define BswM_MarkPduGroupControlInvocation(control) BswM_PduGroupControlInvocation |= (control)
#define BswM_MarkDmControlInvocation() BswM_PduGroupControlInvocation |= BSWM_GROUPCONTROL_DM


/**********************************************************************************************************************
 *  GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/* PRQA S 3449, 3451 EXTERNDECLARATIONS */ /* MD_BSWM_3449, MD_BSWM_3451 */ 
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  BswM_ActionLists
**********************************************************************************************************************/
/** 
  \var    BswM_ActionLists
  \details
  Element    Description
  FctPtr     Pointer to the array list function.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_ActionListsType, BSWM_CONST) BswM_ActionLists[180];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_CanSMChannelMapping
**********************************************************************************************************************/
/** 
  \var    BswM_CanSMChannelMapping
  \brief  Maps the external id of BswMCanSMIndication to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMCanSMIndication.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_CanSMChannelMappingType, BSWM_CONST) BswM_CanSMChannelMapping[6];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ComMChannelMapping
**********************************************************************************************************************/
/** 
  \var    BswM_ComMChannelMapping
  \brief  Maps the external id of BswMComMIndication to an internal id and references immediate request ports.
  \details
  Element       Description
  ExternalId    External id of BswMComMIndication.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_ComMChannelMappingType, BSWM_CONST) BswM_ComMChannelMapping[14];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_DcmComMapping
**********************************************************************************************************************/
/** 
  \var    BswM_DcmComMapping
  \brief  Maps the external id of BswMDcmComModeRequest to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMDcmComModeRequest.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_DcmComMappingType, BSWM_CONST) BswM_DcmComMapping[6];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_DeferredRules
**********************************************************************************************************************/
/** 
  \var    BswM_DeferredRules
  \details
  Element     Description
  RulesIdx    the index of the 1:1 relation pointing to BswM_Rules
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_DeferredRulesType, BSWM_CONST) BswM_DeferredRules[20];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_GenericMapping
**********************************************************************************************************************/
/** 
  \var    BswM_GenericMapping
  \brief  Maps the external id of BswMGenericRequest to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMGenericRequest.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
  InitValue                Initialization value of port.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_GenericMappingType, BSWM_CONST) BswM_GenericMapping[5];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ImmediateUser
**********************************************************************************************************************/
/** 
  \var    BswM_ImmediateUser
  \brief  Contains all immediate request ports.
  \details
  Element             Description
  MaskedBits          contains bitcoded the boolean data of BswM_OnInitOfImmediateUser, BswM_RulesIndUsedOfImmediateUser
  RulesIndEndIdx      the end index of the 0:n relation pointing to BswM_RulesInd
  RulesIndStartIdx    the start index of the 0:n relation pointing to BswM_RulesInd
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_ImmediateUserType, BSWM_CONST) BswM_ImmediateUser[49];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_InitGenVarAndInitAL
**********************************************************************************************************************/
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_InitGenVarAndInitALType, BSWM_CONST) BswM_InitGenVarAndInitAL[1];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_J1939NmMapping
**********************************************************************************************************************/
/** 
  \var    BswM_J1939NmMapping
  \brief  Maps the external id of BswMJ1939NmIndication to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMJ1939NmIndication.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_J1939NmMappingType, BSWM_CONST) BswM_J1939NmMapping[2];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinSMMapping
**********************************************************************************************************************/
/** 
  \var    BswM_LinSMMapping
  \brief  Maps the external id of BswMLinSMIndication to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMLinSMIndication.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_LinSMMappingType, BSWM_CONST) BswM_LinSMMapping[8];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinScheduleEndMapping
**********************************************************************************************************************/
/** 
  \var    BswM_LinScheduleEndMapping
  \brief  Maps the external id of BswMLinScheduleEndNotification to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMLinScheduleEndNotification.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_LinScheduleEndMappingType, BSWM_CONST) BswM_LinScheduleEndMapping[8];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinScheduleMapping
**********************************************************************************************************************/
/** 
  \var    BswM_LinScheduleMapping
  \brief  Maps the external id of BswMLinScheduleIndication to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMLinScheduleIndication.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_LinScheduleMappingType, BSWM_CONST) BswM_LinScheduleMapping[8];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ModeNotificationFct
**********************************************************************************************************************/
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_PartitionFunctionType, BSWM_CONST) BswM_ModeNotificationFct[1];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ModeRequestMapping
**********************************************************************************************************************/
/** 
  \var    BswM_ModeRequestMapping
  \brief  Maps the external id of BswMSwcModeRequest to an internal id and references immediate request ports.
  \details
  Element                  Description
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_ModeRequestMappingType, BSWM_CONST) BswM_ModeRequestMapping[10];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_PartitionIdentifiers
**********************************************************************************************************************/
/** 
  \var    BswM_PartitionIdentifiers
  \brief  the partition contex in Config
  \details
  Element                 Description
  PartitionSNV        
  PCPartitionConfigIdx    the index of the 1:1 relation pointing to BswM_PCPartitionConfig
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_PartitionIdentifiersType, BSWM_CONST) BswM_PartitionIdentifiers[1];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_Rules
**********************************************************************************************************************/
/** 
  \var    BswM_Rules
  \details
  Element          Description
  Id               External id of rule.
  RuleStatesIdx    the index of the 1:1 relation pointing to BswM_RuleStates
  FctPtr           Pointer to the rule function which does the arbitration.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_RulesType, BSWM_CONST) BswM_Rules[140];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_RulesInd
**********************************************************************************************************************/
/** 
  \var    BswM_RulesInd
  \brief  the indexes of the 1:1 sorted relation pointing to BswM_Rules
*/ 
#define BSWM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_RulesIndType, BSWM_CONST) BswM_RulesInd[151];
#define BSWM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_SwcModeRequestUpdateFct
**********************************************************************************************************************/
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern CONST(BswM_PartitionFunctionType, BSWM_CONST) BswM_SwcModeRequestUpdateFct[1];
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ActionListQueue
**********************************************************************************************************************/
/** 
  \var    BswM_ActionListQueue
  \brief  Variable to store action lists which shall be executed.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_ActionListQueueUType, BSWM_VAR_NOINIT) BswM_ActionListQueue;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_CanSMChannelState
**********************************************************************************************************************/
/** 
  \var    BswM_CanSMChannelState
  \brief  Variable to store current mode of BswMCanSMIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(CanSM_BswMCurrentStateType, BSWM_VAR_NOINIT) BswM_CanSMChannelState[6];
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ComMChannelState
**********************************************************************************************************************/
/** 
  \var    BswM_ComMChannelState
  \brief  Variable to store current mode of BswMComMIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(ComM_ModeType, BSWM_VAR_NOINIT) BswM_ComMChannelState[14];
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_DcmComState
**********************************************************************************************************************/
/** 
  \var    BswM_DcmComState
  \brief  Variable to store current mode of BswMDcmComModeRequest mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(Dcm_CommunicationModeType, BSWM_VAR_NOINIT) BswM_DcmComState[6];
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ForcedActionListPriority
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_ForcedActionListPriorityType, BSWM_VAR_NOINIT) BswM_ForcedActionListPriority;
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_GenericState
**********************************************************************************************************************/
/** 
  \var    BswM_GenericState
  \brief  Variable to store current mode of BswMGenericRequest mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_ModeType, BSWM_VAR_NOINIT) BswM_GenericState[5];
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_Initialized
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_InitializedType, BSWM_VAR_NOINIT) BswM_Initialized;
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_J1939NmState
**********************************************************************************************************************/
/** 
  \var    BswM_J1939NmState
  \brief  Variable to store current mode of BswMJ1939NmIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(Nm_StateType, BSWM_VAR_NOINIT) BswM_J1939NmState[2];
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinSMState
**********************************************************************************************************************/
/** 
  \var    BswM_LinSMState
  \brief  Variable to store current mode of BswMLinSMIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(LinSM_ModeType, BSWM_VAR_NOINIT) BswM_LinSMState[8];
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinScheduleEndState
**********************************************************************************************************************/
/** 
  \var    BswM_LinScheduleEndState
  \brief  Variable to store current mode of BswMLinScheduleEndNotification mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(LinIf_SchHandleType, BSWM_VAR_NOINIT) BswM_LinScheduleEndState[8];
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinScheduleState
**********************************************************************************************************************/
/** 
  \var    BswM_LinScheduleState
  \brief  Variable to store current mode of BswMLinScheduleIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(LinIf_SchHandleType, BSWM_VAR_NOINIT) BswM_LinScheduleState[8];
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ModeRequestQueue
**********************************************************************************************************************/
/** 
  \var    BswM_ModeRequestQueue
  \brief  Variable to store an immediate mode request which must be queued because of a current active arbitration.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_ModeRequestQueueType, BSWM_VAR_NOINIT) BswM_ModeRequestQueue[49];
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_NvMJobState
**********************************************************************************************************************/
/** 
  \var    BswM_NvMJobState
  \brief  Variable to store current mode of BswMNvMJobModeIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(NvM_RequestResultType, BSWM_VAR_NOINIT) BswM_NvMJobState[1];
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_QueueSemaphore
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_QueueSemaphoreType, BSWM_VAR_NOINIT) BswM_QueueSemaphore;
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_QueueWritten
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_QueueWrittenType, BSWM_VAR_NOINIT) BswM_QueueWritten;
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_RuleStates
**********************************************************************************************************************/
/** 
  \var    BswM_RuleStates
  \brief  Stores the last execution state of the rule.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_RuleStatesUType, BSWM_VAR_NOINIT) BswM_RuleStates;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_TimerState
**********************************************************************************************************************/
/** 
  \var    BswM_TimerState
  \brief  Variable to store current state of BswMTimer (STARTED, STOPPER OR EXPIRED).
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_TimerStateUType, BSWM_VAR_NOINIT) BswM_TimerState;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_TimerValue
**********************************************************************************************************************/
/** 
  \var    BswM_TimerValue
  \brief  Variable to store current timer values.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
extern VAR(BswM_TimerValueUType, BSWM_VAR_NOINIT) BswM_TimerValue;  /* PRQA S 0759 */  /* MD_CSL_Union */
#define BSWM_STOP_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */



/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL DATA PROTOTYPES
**********************************************************************************************************************/

/* PRQA L:EXTERNDECLARATIONS */


#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "BswM_vMemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#if (BSWM_USE_INIT_POINTER == STD_ON)
extern  P2CONST(BswM_ConfigType, AUTOMATIC, BSWM_PBCFG) BswM_ConfigPtr;
#endif

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "BswM_vMemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define BSWM_START_SEC_VAR_NOINIT_8BIT
#include "BswM_vMemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

extern VAR(uint8, BSWM_VAR_NOINIT) BswM_PduGroupControlInvocation;

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
#include "BswM_vMemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */


#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
#include "BswM_vMemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/* PRQA S 3218 3 */ /* MD_BswM_3218 */
extern VAR(Com_IpduGroupVector, BSWM_VAR_NOINIT) BswM_ComIPduGroupState;
extern VAR(Com_IpduGroupVector, BSWM_VAR_NOINIT) BswM_ComIPduGroupReinitState;
extern VAR(Com_IpduGroupVector, BSWM_VAR_NOINIT) BswM_ComRxIPduGroupDMState;

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
#include "BswM_vMemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#define BswM_IsPreInitialized()                     (BswM_PreInitialized) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */
#define BswM_SetPreInitialized(Value)               (BswM_IsPreInitialized()) = (Value) /* PRQA S 3453 */ /* MD_MSR_FctLikeMacro */

/**********************************************************************************************************************
 *  GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/


/**********************************************************************************************************************
  CONFIGURATION CLASS: POST_BUILD
  SECTION: GLOBAL FUNCTION PROTOTYPES
**********************************************************************************************************************/


/* -----------------------------------------------------------------------------
    &&&~ EXTERNAL DECLARATIONS
 ----------------------------------------------------------------------------- */

#define BSWM_START_SEC_CODE
#include "BswM_vMemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

/*! \addtogroup    BswMGeneratedFunctions BswM Generated Functions
 * \{
 */
/* PRQA S 0779 FUNCTIONDECLARATIONS */ /* MD_MSR_Rule5.2_0779 */

/*!
 * \{
 * \brief       Function called by Rte to indicate a new mode of a SwcModeRequestPort.
 * \details     Mode is read from Rte, stored and depending immediate rules are arbitrated.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \note        Function is called by RTE.
 */
/*! \fn BswM_Read_LIN3_ScheduleTableRequestMode */
/*! \fn BswM_Read_LIN1_ScheduleTableRequestMode */
/*! \fn BswM_Read_LIN2_ScheduleTableRequestMode */
/*! \fn BswM_Read_LIN4_ScheduleTableRequestMode */
/*! \fn BswM_Read_LIN5_ScheduleTableRequestMode */
/*! \fn BswM_Read_LIN6_ScheduleTableRequestMode */
/*! \fn BswM_Read_LIN7_ScheduleTableRequestMode */
/*! \fn BswM_Read_LIN8_ScheduleTableRequestMode */
/*! \} */ /* End of sharing description for BswMRteRequestFunctions */
/* PRQA L:FUNCTIONDECLARATIONS */
/*! \} */ /* End of group BswMGeneratedFunctions */





#if (BSWM_FUNCTION_BASED == STD_OFF)
/**********************************************************************************************************************
 *  BswM_Action_ActionListHandler()
 **********************************************************************************************************************/
/*!
 * \brief       Executes an action list.
 * \details     Executes all actions of an action list.
 * \param[in]   handleId  Id of the action list to execute.
 * \param[in]   partitionIdx Index of current partition Context
 * \return      E_OK      Action list was completely executed.
 * \return      E_NOT_OK  Action list was aborted because an action failed.
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
extern FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_ActionListHandler(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
#endif

#if ((BSWM_RULES == STD_ON)  && (BSWM_FUNCTION_BASED == STD_OFF))
/**********************************************************************************************************************
 *  BswM_ArbitrateRule()
 **********************************************************************************************************************/
/*!
 * \brief       Arbitrates a rule.
 * \details     Evaluates the logical expression of the rule and determines the action list to execute.
 * \param[in]   ruleId  Id of the rule to arbitrate
 * \param[in]   partitionIdx  Index of current partition Context
 * \return      ID of action list to execute (BSWM_NO_ACTIONLIST if no action list shall be executed)
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
extern FUNC(BswM_SizeOfActionListsType, BSWM_CODE) BswM_ArbitrateRule(BswM_HandleType ruleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
#endif

/**********************************************************************************************************************
 *  BswM_ExecuteIpduGroupControl()
 **********************************************************************************************************************/
/*!
 * \brief       Enabes and disables PDU Groups and DeadlineMonitoring in Com.
 * \details     Forwards the changes to the local Com_IpduGroupVector caused by executed actions to the corresponding 
 *              Com APIS.
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
extern FUNC(void, BSWM_CODE) BswM_ExecuteIpduGroupControl(void);

#if(BSWM_IMMEDIATEUSER == STD_ON)
# if (BSWM_DEV_ERROR_REPORT == STD_ON)
/**********************************************************************************************************************
 *  BswM_ImmediateModeRequest()
 **********************************************************************************************************************/
/*!
 * \brief       Processes an immediate mode request.
 * \details     Queues mode request and starts arbitration of depending rules if no other request is currently active.
 * \param[in]   start   Handle of first mode request.
 * \param[in]   end     Handle of last mode request.
 * \param[in]   sid     Service Id of calling API. Only available if BSWM_DEV_ERROR_REPORT is STD_ON.
 * \param[in]   partitionIdx Index of current partition Context
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
extern FUNC(void, BSWM_CODE) BswM_ImmediateModeRequest(BswM_SizeOfImmediateUserType start, BswM_SizeOfImmediateUserType end, uint8 sid, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
# else
extern FUNC(void, BSWM_CODE) BswM_ImmediateModeRequest(BswM_SizeOfImmediateUserType start, BswM_SizeOfImmediateUserType end, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
# endif
#endif

#define BSWM_STOP_SEC_CODE
#include "BswM_vMemMap.h" /* PRQA S 5087 */ /* MD_MSR_MemMap */

#endif /* BSWM_PRIVATE_CFG_H */


