/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: BswM
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: BswM_Lcfg.c
 *   Generation Time: 2020-11-11 14:25:30
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#define BSWM_LCFG_SOURCE

/* -----------------------------------------------------------------------------
    &&&~ MISRA JUSTIFICATION
 ----------------------------------------------------------------------------- */
/* PRQA S 0785, 0786 EOF */ /* MD_CSL_DistinctIdentifiers */

/* -----------------------------------------------------------------------------
    &&&~ INCLUDE
 ----------------------------------------------------------------------------- */
/* PRQA S 0857 EOF */ /* MD_MSR_1.1_857 */
#include "BswM.h"
#include "BswM_Private_Cfg.h"
#include "SchM_BswM.h"

#if !defined (BSWM_LOCAL)
# define BSWM_LOCAL static
#endif

#if !defined (BSWM_LOCAL_INLINE) /* COV_BSWM_LOCAL_INLINE */
# define BSWM_LOCAL_INLINE LOCAL_INLINE
#endif



#define BSWM_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* -----------------------------------------------------------------------------
    &&&~ LOCAL FUNCTION DECLARATIONS
 ----------------------------------------------------------------------------- */
/**********************************************************************************************************************
 *  BswM_Action_RuleHandler()
 **********************************************************************************************************************/
/*!
 * \brief       Executes a rule.
 * \details     Arbitrates a rule and executes corresponding action list.
 * \param[in]   handleId       Id of the rule to execute.
 * \param[in]   partitionIdx   Current partition context.
 * \return      E_OK      No action list was executed or corresponding action list was completely executed.
 * \return      E_NOT_OK  Action list was aborted because an action failed.
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_RuleHandler(BswM_HandleType handleId,
                                                                   BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);

/**********************************************************************************************************************
 *  BswM_UpdateRuleStates()
 **********************************************************************************************************************/
/*!
 * \brief       Updates the state of a rule.
 * \details     Set rule state of passed ruleId to passed state.
 * \param[in]   ruleId         Id of the rule to update.
 * \param[in]   state          New state of the rule.
 * \param[in]   partitionIdx   Current partition context.
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
BSWM_LOCAL_INLINE FUNC(void, BSWM_CODE) BswM_UpdateRuleStates(BswM_SizeOfRuleStatesType ruleId, 
                                                              BswM_RuleStatesType state,
                                                              BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);

/**********************************************************************************************************************
 *  BswM_UpdateTimer()
 **********************************************************************************************************************/
/*!
 * \brief       Updates a timer.
 * \details     Set timer value of passed timerId to passed value and calculates the new state.
 * \param[in]   timerId        Id of the timer to update.
 * \param[in]   value          New value of the timer.
 * \param[in]   partitionIdx   Current partition context. 
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
BSWM_LOCAL_INLINE FUNC(void, BSWM_CODE) BswM_UpdateTimer(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx,
                                                         BswM_SizeOfTimerValueType timerId,
                                                         BswM_TimerValueType value);

/*! \addtogroup    BswMGeneratedFunctions BswM Generated Functions
 * \{
 */
/* PRQA S 0779 FUNCTIONDECLARATIONS */ /* MD_MSR_Rule5.2_0779 */

/**********************************************************************************************************************
 *  Init
 *********************************************************************************************************************/
/*! \defgroup    Init
 * \{
 */
/**********************************************************************************************************************
 *  BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
/*!
 * \brief       Initializes BswM.
 * \details     Part of the BswM_Init. Initializes all generated variables and executes action lists for initialization.
 * \pre         -
 * \reentrant   FALSE
 * \synchronous TRUE
 * \note        May only be called by BswM_Init.
 */
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
/*! \} */ /* End of group Init */

/**********************************************************************************************************************
 *  ImmediateSwcRequest
 *********************************************************************************************************************/
/*! \defgroup    ImmediateSwcRequest
 * \{
 */
/**********************************************************************************************************************
 *  BswM_ImmediateSwcRequest
 *********************************************************************************************************************/
/*!
 * \brief       Arbitrates depending immediate rules of a Swc Mode Request port.
 * \details     Checks if port is valid and arbitrates depending immediate rules.
 * \param[IN]   requestId     Index of a swc mode request port.
 * \param[IN]   partitionIdx  Current partition index
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \note        Function called by immediate swc request functions (BswMRteRequestFunctions).
 */
BSWM_LOCAL_INLINE FUNC(void, BSWM_CODE) BswM_ImmediateSwcRequest(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx, BswM_SizeOfModeRequestMappingType requestId);
/*! \} */ /* End of group ImmediateSwcRequest */

/**********************************************************************************************************************
 *  Common
 *********************************************************************************************************************/
/*! \defgroup    Common
 * \{
 */
/**********************************************************************************************************************
 *  BswM_ModeNotificationFct_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
/*!
 * \brief       Switch Modes of RTE and writes RTE values.
 * \details     Forwards a BswM Switch Action to the RTE and writes value of RteRequestPorts to RTE.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \note        May only be called by BswM_MainFunction.
 */
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_ModeNotificationFct_BSWM_SINGLEPARTITION(void);
/*! \} */ /* End of group Common */

/**********************************************************************************************************************
 *  SwcModeRequestUpdate
 *********************************************************************************************************************/
/*! \defgroup    SwcModeRequestUpdate
 * \{
 */
/**********************************************************************************************************************
 *  BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
/*!
 * \brief       Reads port values from RTE.
 * \details     Gets the current value of SwcModeRequest Ports and SwcNotification Ports from RTE.
 * \pre         -
 * \context     TASK
 * \reentrant   TRUE
 * \synchronous TRUE
 * \note        May only be called by BswM_MainFunction.
 */
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION(void);
/*! \} */ /* End of group SwcModeRequestUpdate */

/**********************************************************************************************************************
 *  BswMActionListFunctions
 *********************************************************************************************************************/
/*! \defgroup    BswMActionListFunctions
 * \{
 */

/*!
 * \{
 * \brief       Execute actions of action list.
 * \details     Generated function which depends on the configuration. Executes the actions of the action list in the
 *              configured order.
 * \return      E_OK      Action list was completely executed.
 * \return      E_NOT_OK  Action list was aborted because an action failed.
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DcmEcuReset_Trigger(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_ExitRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_RunToPostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_WaitForNvMToShutdown(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_WakeupToPrep(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_WaitForNvMWakeup(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_WakeupToRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_InitToWakeup(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_PostRunToPrepShutdown(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_PostRunToRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_ExitPostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_PrepShutdownToWaitForNvM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_INIT_AL_Initialize(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_DemInit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DCMResetProcess_Started(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DCMResetProcess_InProgress(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_RunToECUReset(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DcmEcuReset_JumpToBTL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DCMResetPostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_PrepShutdown_NvmWriteAll(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_BB1_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_BB1_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_BB2_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_CAN6_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_CAN6_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_BB2_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_CabSubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_FMSNet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_CabSubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_FMSNet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_SecuritySubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_SecuritySubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_AL_PvtReport_Enabled(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_AL_PvtReport_Disabled(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
/*! \} */ /* End of sharing description for BswMActionListFunctions */
#define BswM_ActionList_CC_AL_DcmEcuReset_Execute BswM_ActionList_ESH_AL_WaitForNvMToShutdown
/*! \} */ /* End of group BswMActionListFunctions */

/**********************************************************************************************************************
 *  BswMRuleFunctions
 *********************************************************************************************************************/
/*! \defgroup    BswMRuleFunctions
 * \{
 */

/*!
 * \{
 * \brief       Arbitrates the configured rule.
 * \details     Generated function which depends on the configuration. Arbitrates the rule and returns the action list
 *              which shall be executed.
 * \return      ID of action list to execute (BSWM_NO_ACTIONLIST if no ActionList shall be executed)
 * \pre         -
 * \context     ANY
 * \reentrant   TRUE
 * \synchronous TRUE
 */
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_DcmEcuReset_Execute(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_DcmEcuReset_Trigger(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CabSubnet_9ea693f1_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone2_78967e2c_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN02_c2d7c6f3(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone2_78967e2c_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CabSubnet_9ea693f1_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN03_b5d0f665(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN01_5bde9749(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN04_2bb463c6(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN00_2cd9a7df(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_RunToPostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_RunToPostRunNested(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_WaitToShutdown(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_WakeupToPrep(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_WaitToWakeup(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_WakeupToRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_InitToWakeup(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_PostRunNested(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_PostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_PrepToWait(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_DemInit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CabSubnet_9ea693f1_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone2_78967e2c_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN06_c5ba02ea(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN05_5cb35350(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN07_b2bd327c(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CAN6_b040c073_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CAN6_b040c073_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CAN6_b040c073_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_Schedule_To_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_Schedule_To_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_Schedule_To_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_Schedule_To_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_Schedule_To_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_Schedule_To_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_Schedule_To_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_Schedule_To_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_Schedule_To_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_DCMResetProcess_Started(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_DCMResetProcess_InProgress(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_DcmEcuReset_JumpToBTL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_NvmWriteAll_Request(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_BB1_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_BB2_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_CAN6_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_CabSubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_FMSNet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_SecuritySubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_PvtReportCtrl(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx);
/*! \} */ /* End of sharing description for BswMRuleFunctions */
/*! \} */ /* End of group BswMRuleFunctions */
/* PRQA L:FUNCTIONDECLARATIONS */
/*! \} */ /* End of group BswMGeneratedFunctions */

#define BSWM_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"


/* -----------------------------------------------------------------------------
    &&&~ LOCAL VARIABLE DECLARATIONS
 ----------------------------------------------------------------------------- */

/* PRQA S 0779 VARIABLEDECLARATIONS */ /* MD_MSR_Rule5.2_0779 */ 


#define BSWM_START_SEC_VAR_NOINIT_8BIT
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

VAR(uint8, BSWM_VAR_NOINIT) BswM_PduGroupControlInvocation;

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"


#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* PRQA S 3218 3 */ /* MD_BswM_3218 */
VAR(Com_IpduGroupVector, BSWM_VAR_NOINIT) BswM_ComIPduGroupState;
VAR(Com_IpduGroupVector, BSWM_VAR_NOINIT) BswM_ComIPduGroupReinitState;
VAR(Com_IpduGroupVector, BSWM_VAR_NOINIT) BswM_ComRxIPduGroupDMState;

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* PRQA L:VARIABLEDECLARATIONS */

/**********************************************************************************************************************
 *  LOCAL DATA PROTOTYPES
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA PROTOTYPES
**********************************************************************************************************************/



/**********************************************************************************************************************
 *  LOCAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: LOCAL DATA
**********************************************************************************************************************/


/**********************************************************************************************************************
 *  GLOBAL DATA
**********************************************************************************************************************/


/* PRQA S 0310 GLOBALDATADECLARATIONS */ /* MD_BSWM_0310 */ 
/**********************************************************************************************************************
  CONFIGURATION CLASS: PRE_COMPILE
  SECTION: GLOBAL DATA
**********************************************************************************************************************/
/**********************************************************************************************************************
  BswM_ActionLists
**********************************************************************************************************************/
/** 
  \var    BswM_ActionLists
  \details
  Element    Description
  FctPtr     Pointer to the array list function.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ActionListsType, BSWM_CONST) BswM_ActionLists[180] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    FctPtr                                                                                Comment                   Referable Keys */
  { /*     0 */ BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable1                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_SchTableStartInd_MSTable1] */
  { /*     1 */ BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable0                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_SchTableStartInd_MSTable0] */
  { /*     2 */ BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_SchTableStartInd_MSTable] */
  { /*     3 */ BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable1                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_SchTableStartInd_MSTable1] */
  { /*     4 */ BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable2                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_SchTableStartInd_MSTable2] */
  { /*     5 */ BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable2                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_SchTableStartInd_MSTable2] */
  { /*     6 */ BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table1                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_SchTableStartInd_Table1] */
  { /*     7 */ BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table2                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_SchTableStartInd_Table2] */
  { /*     8 */ BswM_ActionList_CC_AL_LIN1_SchTableStartInd_TableE                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_SchTableStartInd_TableE] */
  { /*     9 */ BswM_ActionList_CC_AL_LIN2_SchTableStartInd_Table0                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_SchTableStartInd_Table0] */
  { /*    10 */ BswM_ActionList_CC_AL_LIN2_SchTableStartInd_TableE                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_SchTableStartInd_TableE] */
  { /*    11 */ BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_SchTableStartInd_MSTable] */
  { /*    12 */ BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_SchTableStartInd_MSTable] */
  { /*    13 */ BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable1                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_SchTableStartInd_MSTable1] */
  { /*    14 */ BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable2                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_SchTableStartInd_MSTable2] */
  { /*    15 */ BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table1                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_SchTableStartInd_Table1] */
  { /*    16 */ BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table2                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_SchTableStartInd_Table2] */
  { /*    17 */ BswM_ActionList_CC_AL_LIN3_SchTableStartInd_TableE                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_SchTableStartInd_TableE] */
  { /*    18 */ BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable2                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_SchTableStartInd_MSTable2] */
  { /*    19 */ BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_SchTableStartInd_MSTable] */
  { /*    20 */ BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table1                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_SchTableStartInd_Table1] */
  { /*    21 */ BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table2                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_SchTableStartInd_Table2] */
  { /*    22 */ BswM_ActionList_CC_AL_LIN4_SchTableStartInd_TableE                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_SchTableStartInd_TableE] */
  { /*    23 */ BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable1                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_SchTableStartInd_MSTable1] */
  { /*    24 */ BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_SchTableStartInd_MSTable] */
  { /*    25 */ BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table1                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_SchTableStartInd_Table1] */
  { /*    26 */ BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table2                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_SchTableStartInd_Table2] */
  { /*    27 */ BswM_ActionList_CC_AL_LIN5_SchTableStartInd_TableE                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_SchTableStartInd_TableE] */
  { /*    28 */ BswM_ActionList_CC_AL_DcmEcuReset_Trigger                                      },  /* [Priority: 0] */  /* [AL_CC_AL_DcmEcuReset_Trigger] */
  { /*    29 */ BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table2                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_ScheduleTable_to_Table2] */
  { /*    30 */ BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp                },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp] */
  { /*    31 */ BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp                },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp] */
  { /*    32 */ BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table_E                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_ScheduleTable_to_Table_E] */
  { /*    33 */ BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1] */
  { /*    34 */ BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table1                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_ScheduleTable_to_Table1] */
  { /*    35 */ BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_NULL                               },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_ScheduleTable_to_NULL] */
  { /*    36 */ BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table2                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_ScheduleTable_to_Table2] */
  { /*    37 */ BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table_E                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_ScheduleTable_to_Table_E] */
  { /*    38 */ BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp                },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp] */
  { /*    39 */ BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1] */
  { /*    40 */ BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2] */
  { /*    41 */ BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp                },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp] */
  { /*    42 */ BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp                },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp] */
  { /*    43 */ BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table2                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_ScheduleTable_to_Table2] */
  { /*    44 */ BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table_E                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_ScheduleTable_to_Table_E] */
  { /*    45 */ BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table_E                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_ScheduleTable_to_Table_E] */
  { /*    46 */ BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table_E                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_ScheduleTable_to_Table_E] */
  { /*    47 */ BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table2                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_ScheduleTable_to_Table2] */
  { /*    48 */ BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table1                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_ScheduleTable_to_Table1] */
  { /*    49 */ BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table1                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_ScheduleTable_to_Table1] */
    /* Index    FctPtr                                                                                Comment                   Referable Keys */
  { /*    50 */ BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table0                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_ScheduleTable_to_Table0] */
  { /*    51 */ BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table1                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_ScheduleTable_to_Table1] */
  { /*    52 */ BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_NULL                               },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_ScheduleTable_to_NULL] */
  { /*    53 */ BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_NULL                               },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_ScheduleTable_to_NULL] */
  { /*    54 */ BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_NULL                               },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_ScheduleTable_to_NULL] */
  { /*    55 */ BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_NULL                               },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_ScheduleTable_to_NULL] */
  { /*    56 */ BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2] */
  { /*    57 */ BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2] */
  { /*    58 */ BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2] */
  { /*    59 */ BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0] */
  { /*    60 */ BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1] */
  { /*    61 */ BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1] */
  { /*    62 */ BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM                         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM] */
  { /*    63 */ BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM                          },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM] */
  { /*    64 */ BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline] */
  { /*    65 */ BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online          },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online] */
  { /*    66 */ BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Disable_DM                         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone2_78967e2c_Disable_DM] */
  { /*    67 */ BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Enable_DM                          },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone2_78967e2c_Enable_DM] */
  { /*    68 */ BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable                    },  /* [Priority: 0] */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable] */
  { /*    69 */ BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit               },  /* [Priority: 0] */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit] */
  { /*    70 */ BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Disable                                },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN02_c2d7c6f3_Disable] */
  { /*    71 */ BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Enable                                 },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN02_c2d7c6f3_Enable] */
  { /*    72 */ BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable                    },  /* [Priority: 0] */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable] */
  { /*    73 */ BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit               },  /* [Priority: 0] */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit] */
  { /*    74 */ BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM                    },  /* [Priority: 0] */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM] */
  { /*    75 */ BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM                     },  /* [Priority: 0] */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM] */
  { /*    76 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_Disable                            },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_TX_Disable] */
  { /*    77 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit                       },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit] */
  { /*    78 */ BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_Disable                         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone2_78967e2c_RX_Disable] */
  { /*    79 */ BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit                    },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit] */
  { /*    80 */ BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable                         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable] */
  { /*    81 */ BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit                    },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit] */
  { /*    82 */ BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_Disable                         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone2_78967e2c_TX_Disable] */
  { /*    83 */ BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit                    },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit] */
  { /*    84 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline                 },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline] */
  { /*    85 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online                  },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online] */
  { /*    86 */ BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable                         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable] */
  { /*    87 */ BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit                    },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit] */
  { /*    88 */ BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Disable                                },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN03_b5d0f665_Disable] */
  { /*    89 */ BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Enable                                 },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN03_b5d0f665_Enable] */
  { /*    90 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_Disable                            },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_RX_Disable] */
  { /*    91 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit                       },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit] */
  { /*    92 */ BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Disable                                },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN01_5bde9749_Disable] */
  { /*    93 */ BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Enable                                 },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN01_5bde9749_Enable] */
  { /*    94 */ BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Disable                                },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN04_2bb463c6_Disable] */
  { /*    95 */ BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Enable                                 },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN04_2bb463c6_Enable] */
  { /*    96 */ BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable                    },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable] */
  { /*    97 */ BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit               },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit] */
  { /*    98 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Disable_DM                            },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_Disable_DM] */
  { /*    99 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Enable_DM                             },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_Enable_DM] */
    /* Index    FctPtr                                                                                Comment                   Referable Keys */
  { /*   100 */ BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM                    },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM] */
  { /*   101 */ BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM                     },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM] */
  { /*   102 */ BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Disable                                },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN00_2cd9a7df_Disable] */
  { /*   103 */ BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Enable                                 },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN00_2cd9a7df_Enable] */
  { /*   104 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable              },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable] */
  { /*   105 */ BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit] */
  { /*   106 */ BswM_ActionList_ESH_AL_ExitRun                                                 },  /* [Priority: 0] */  /* [AL_ESH_AL_ExitRun] */
  { /*   107 */ BswM_ActionList_ESH_AL_RunToPostRun                                            },  /* [Priority: 0] */  /* [AL_ESH_AL_RunToPostRun] */
  { /*   108 */ BswM_ActionList_ESH_AL_WaitForNvMToShutdown                                    },  /* [Priority: 0] */  /* [AL_ESH_AL_WaitForNvMToShutdown] */
  { /*   109 */ BswM_ActionList_ESH_AL_WakeupToPrep                                            },  /* [Priority: 0] */  /* [AL_ESH_AL_WakeupToPrep] */
  { /*   110 */ BswM_ActionList_ESH_AL_WaitForNvMWakeup                                        },  /* [Priority: 0] */  /* [AL_ESH_AL_WaitForNvMWakeup] */
  { /*   111 */ BswM_ActionList_ESH_AL_WakeupToRun                                             },  /* [Priority: 0] */  /* [AL_ESH_AL_WakeupToRun] */
  { /*   112 */ BswM_ActionList_ESH_AL_InitToWakeup                                            },  /* [Priority: 0] */  /* [AL_ESH_AL_InitToWakeup] */
  { /*   113 */ BswM_ActionList_ESH_AL_PostRunToPrepShutdown                                   },  /* [Priority: 0] */  /* [AL_ESH_AL_PostRunToPrepShutdown] */
  { /*   114 */ BswM_ActionList_ESH_AL_PostRunToRun                                            },  /* [Priority: 0] */  /* [AL_ESH_AL_PostRunToRun] */
  { /*   115 */ BswM_ActionList_ESH_AL_ExitPostRun                                             },  /* [Priority: 0] */  /* [AL_ESH_AL_ExitPostRun] */
  { /*   116 */ BswM_ActionList_ESH_AL_PrepShutdownToWaitForNvM                                },  /* [Priority: 0] */  /* [AL_ESH_AL_PrepShutdownToWaitForNvM] */
  { /*   117 */ BswM_ActionList_INIT_AL_Initialize                                             },  /* [Priority: 0] */  /* [AL_INIT_AL_Initialize] */
  { /*   118 */ BswM_ActionList_ESH_AL_DemInit                                                 },  /* [Priority: 0] */  /* [AL_ESH_AL_DemInit] */
  { /*   119 */ BswM_ActionList_CC_AL_LIN1_ScheduleTableEndNotification                        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN1_ScheduleTableEndNotification] */
  { /*   120 */ BswM_ActionList_CC_AL_LIN2_ScheduleTableEndNotification                        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN2_ScheduleTableEndNotification] */
  { /*   121 */ BswM_ActionList_CC_AL_LIN3_ScheduleTableEndNotification                        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN3_ScheduleTableEndNotification] */
  { /*   122 */ BswM_ActionList_CC_AL_LIN4_ScheduleTableEndNotification                        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN4_ScheduleTableEndNotification] */
  { /*   123 */ BswM_ActionList_CC_AL_LIN5_ScheduleTableEndNotification                        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN5_ScheduleTableEndNotification] */
  { /*   124 */ BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Disable                                },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN06_c5ba02ea_Disable] */
  { /*   125 */ BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Enable                                 },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN06_c5ba02ea_Enable] */
  { /*   126 */ BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Disable                                },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN05_5cb35350_Disable] */
  { /*   127 */ BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Enable                                 },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN05_5cb35350_Enable] */
  { /*   128 */ BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Disable                                },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN07_b2bd327c_Disable] */
  { /*   129 */ BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Enable                                 },  /* [Priority: 0] */  /* [AL_CC_AL_CN_LIN07_b2bd327c_Enable] */
  { /*   130 */ BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_Disable                              },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CAN6_b040c073_RX_Disable] */
  { /*   131 */ BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit                         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit] */
  { /*   132 */ BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_Disable                              },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CAN6_b040c073_TX_Disable] */
  { /*   133 */ BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit                         },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit] */
  { /*   134 */ BswM_ActionList_CC_AL_CN_CAN6_b040c073_Disable_DM                              },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CAN6_b040c073_Disable_DM] */
  { /*   135 */ BswM_ActionList_CC_AL_CN_CAN6_b040c073_Enable_DM                               },  /* [Priority: 0] */  /* [AL_CC_AL_CN_CAN6_b040c073_Enable_DM] */
  { /*   136 */ BswM_ActionList_CC_AL_LIN6_ScheduleTableEndNotification                        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN6_ScheduleTableEndNotification] */
  { /*   137 */ BswM_ActionList_CC_AL_LIN7_ScheduleTableEndNotification                        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN7_ScheduleTableEndNotification] */
  { /*   138 */ BswM_ActionList_CC_AL_LIN8_ScheduleTableEndNotification                        },  /* [Priority: 0] */  /* [AL_CC_AL_LIN8_ScheduleTableEndNotification] */
  { /*   139 */ BswM_ActionList_CC_AL_LIN6_SchTableStartInd_Table0                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN6_SchTableStartInd_Table0] */
  { /*   140 */ BswM_ActionList_CC_AL_LIN7_SchTableStartInd_Table0                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN7_SchTableStartInd_Table0] */
  { /*   141 */ BswM_ActionList_CC_AL_LIN8_SchTableStartInd_Table0                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN8_SchTableStartInd_Table0] */
  { /*   142 */ BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN6_SchTableStartInd_MSTable] */
  { /*   143 */ BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN7_SchTableStartInd_MSTable] */
  { /*   144 */ BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN8_SchTableStartInd_MSTable] */
  { /*   145 */ BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable0                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN6_SchTableStartInd_MSTable0] */
  { /*   146 */ BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable0                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN7_SchTableStartInd_MSTable0] */
  { /*   147 */ BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable0                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN8_SchTableStartInd_MSTable0] */
  { /*   148 */ BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_Table0                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN6_ScheduleTable_to_Table0] */
  { /*   149 */ BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_Table0                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN7_ScheduleTable_to_Table0] */
    /* Index    FctPtr                                                                                Comment                   Referable Keys */
  { /*   150 */ BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_Table0                             },  /* [Priority: 0] */  /* [AL_CC_AL_LIN8_ScheduleTable_to_Table0] */
  { /*   151 */ BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable0                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN6_ScheduleTable_to_MSTable0] */
  { /*   152 */ BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN6_ScheduleTable_to_MSTable] */
  { /*   153 */ BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable0                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN7_ScheduleTable_to_MSTable0] */
  { /*   154 */ BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN7_ScheduleTable_to_MSTable] */
  { /*   155 */ BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable0                           },  /* [Priority: 0] */  /* [AL_CC_AL_LIN8_ScheduleTable_to_MSTable0] */
  { /*   156 */ BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable                            },  /* [Priority: 0] */  /* [AL_CC_AL_LIN8_ScheduleTable_to_MSTable] */
  { /*   157 */ BswM_ActionList_CC_AL_DCMResetProcess_Started                                  },  /* [Priority: 0] */  /* [AL_CC_AL_DCMResetProcess_Started] */
  { /*   158 */ BswM_ActionList_CC_AL_DCMResetProcess_InProgress                               },  /* [Priority: 0] */  /* [AL_CC_AL_DCMResetProcess_InProgress] */
  { /*   159 */ BswM_ActionList_CC_AL_RunToECUReset                                            },  /* [Priority: 0] */  /* [AL_CC_AL_RunToECUReset] */
  { /*   160 */ BswM_ActionList_CC_AL_DcmEcuReset_Execute                                      },  /* [Priority: 0] */  /* [AL_CC_AL_DcmEcuReset_Execute] */
  { /*   161 */ BswM_ActionList_CC_AL_DcmEcuReset_JumpToBTL                                    },  /* [Priority: 0] */  /* [AL_CC_AL_DcmEcuReset_JumpToBTL] */
  { /*   162 */ BswM_ActionList_CC_AL_DCMResetPostRun                                          },  /* [Priority: 0] */  /* [AL_CC_AL_DCMResetPostRun] */
  { /*   163 */ BswM_ActionList_CC_AL_PrepShutdown_NvmWriteAll                                 },  /* [Priority: 0] */  /* [AL_CC_AL_PrepShutdown_NvmWriteAll] */
  { /*   164 */ BswM_ActionList_CC_True_AL_BB1_BusOff_Indication                               },  /* [Priority: 0] */  /* [AL_CC_True_AL_BB1_BusOff_Indication] */
  { /*   165 */ BswM_ActionList_CC_False_AL_BB1_BusOff_Indication                              },  /* [Priority: 0] */  /* [AL_CC_False_AL_BB1_BusOff_Indication] */
  { /*   166 */ BswM_ActionList_CC_True_AL_BB2_BusOff_Indication                               },  /* [Priority: 0] */  /* [AL_CC_True_AL_BB2_BusOff_Indication] */
  { /*   167 */ BswM_ActionList_CC_False_AL_CAN6_BusOff_Indication                             },  /* [Priority: 0] */  /* [AL_CC_False_AL_CAN6_BusOff_Indication] */
  { /*   168 */ BswM_ActionList_CC_True_AL_CAN6_BusOff_Indication                              },  /* [Priority: 0] */  /* [AL_CC_True_AL_CAN6_BusOff_Indication] */
  { /*   169 */ BswM_ActionList_CC_False_AL_BB2_BusOff_Indication                              },  /* [Priority: 0] */  /* [AL_CC_False_AL_BB2_BusOff_Indication] */
  { /*   170 */ BswM_ActionList_CC_False_AL_CabSubnet_BusOff_Indication                        },  /* [Priority: 0] */  /* [AL_CC_False_AL_CabSubnet_BusOff_Indication] */
  { /*   171 */ BswM_ActionList_CC_False_AL_FMSNet_BusOff_Indication                           },  /* [Priority: 0] */  /* [AL_CC_False_AL_FMSNet_BusOff_Indication] */
  { /*   172 */ BswM_ActionList_CC_True_AL_CabSubnet_BusOff_Indication                         },  /* [Priority: 0] */  /* [AL_CC_True_AL_CabSubnet_BusOff_Indication] */
  { /*   173 */ BswM_ActionList_CC_True_AL_FMSNet_BusOff_Indication                            },  /* [Priority: 0] */  /* [AL_CC_True_AL_FMSNet_BusOff_Indication] */
  { /*   174 */ BswM_ActionList_CC_False_AL_SecuritySubnet_BusOff_Indication                   },  /* [Priority: 0] */  /* [AL_CC_False_AL_SecuritySubnet_BusOff_Indication] */
  { /*   175 */ BswM_ActionList_CC_True_AL_SecuritySubnet_BusOff_Indication                    },  /* [Priority: 0] */  /* [AL_CC_True_AL_SecuritySubnet_BusOff_Indication] */
  { /*   176 */ BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable      },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable] */
  { /*   177 */ BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit },  /* [Priority: 0] */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit] */
  { /*   178 */ BswM_ActionList_AL_PvtReport_Enabled                                           },  /* [Priority: 0] */  /* [AL_AL_PvtReport_Enabled] */
  { /*   179 */ BswM_ActionList_AL_PvtReport_Disabled                                          }   /* [Priority: 0] */  /* [AL_AL_PvtReport_Disabled] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_CanSMChannelMapping
**********************************************************************************************************************/
/** 
  \var    BswM_CanSMChannelMapping
  \brief  Maps the external id of BswMCanSMIndication to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMCanSMIndication.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_CanSMChannelMappingType, BSWM_CONST) BswM_CanSMChannelMapping[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                                       ImmediateUserEndIdx  ImmediateUserStartIdx        Referable Keys */
  { /*     0 */ ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae,                  1u,                    0u },  /* [CANSM_CHANNEL_0, MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae, MRP_CanSMIndication_BB1] */
  { /*     1 */ ComMConf_ComMChannel_CN_Backbone2_78967e2c     ,                  2u,                    1u },  /* [CANSM_CHANNEL_1, MRP_CC_CanSMIndication_CN_Backbone2_78967e2c, MRP_CanSMIndication_BB2] */
  { /*     2 */ ComMConf_ComMChannel_CN_CAN6_b040c073          ,                  3u,                    2u },  /* [CANSM_CHANNEL_2, MRP_CC_CanSMIndication_CN_CAN6_b040c073, MRP_CanSMIndication_CAN6] */
  { /*     3 */ ComMConf_ComMChannel_CN_CabSubnet_9ea693f1     ,                  4u,                    3u },  /* [CANSM_CHANNEL_3, MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1, MRP_CanSMIndication_CabSubnet] */
  { /*     4 */ ComMConf_ComMChannel_CN_FMSNet_fce1aae5        ,                  5u,                    4u },  /* [CANSM_CHANNEL_4, MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5, MRP_CanSMIndication_FMSNet] */
  { /*     5 */ ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54,                  6u,                    5u }   /* [CANSM_CHANNEL_5, MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54, MRP_CanSMIndication_SecuritySubnet] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ComMChannelMapping
**********************************************************************************************************************/
/** 
  \var    BswM_ComMChannelMapping
  \brief  Maps the external id of BswMComMIndication to an internal id and references immediate request ports.
  \details
  Element       Description
  ExternalId    External id of BswMComMIndication.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ComMChannelMappingType, BSWM_CONST) BswM_ComMChannelMapping[14] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                                             Referable Keys */
  { /*     0 */ ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae },  /* [COMM_CHANNEL_0, MRP_ESH_ComMIndication_CN_Backbone1J1939_0b1f4bae] */
  { /*     1 */ ComMConf_ComMChannel_CN_Backbone2_78967e2c      },  /* [COMM_CHANNEL_1, MRP_ESH_ComMIndication_CN_Backbone2_78967e2c] */
  { /*     2 */ ComMConf_ComMChannel_CN_CAN6_b040c073           },  /* [COMM_CHANNEL_2, MRP_ESH_ComMIndication_CN_CAN6_b040c073] */
  { /*     3 */ ComMConf_ComMChannel_CN_CabSubnet_9ea693f1      },  /* [COMM_CHANNEL_3, MRP_ESH_ComMIndication_CN_CabSubnet_9ea693f1] */
  { /*     4 */ ComMConf_ComMChannel_CN_FMSNet_fce1aae5         },  /* [COMM_CHANNEL_4, MRP_ESH_ComMIndication_CN_FMSNet_fce1aae5] */
  { /*     5 */ ComMConf_ComMChannel_CN_LIN00_2cd9a7df          },  /* [COMM_CHANNEL_5, MRP_ESH_ComMIndication_CN_LIN00_2cd9a7df] */
  { /*     6 */ ComMConf_ComMChannel_CN_LIN01_5bde9749          },  /* [COMM_CHANNEL_6, MRP_ESH_ComMIndication_CN_LIN01_5bde9749] */
  { /*     7 */ ComMConf_ComMChannel_CN_LIN02_c2d7c6f3          },  /* [COMM_CHANNEL_7, MRP_ESH_ComMIndication_CN_LIN02_c2d7c6f3] */
  { /*     8 */ ComMConf_ComMChannel_CN_LIN03_b5d0f665          },  /* [COMM_CHANNEL_8, MRP_ESH_ComMIndication_CN_LIN03_b5d0f665] */
  { /*     9 */ ComMConf_ComMChannel_CN_LIN04_2bb463c6          },  /* [COMM_CHANNEL_9, MRP_ESH_ComMIndication_CN_LIN04_2bb463c6] */
  { /*    10 */ ComMConf_ComMChannel_CN_LIN05_5cb35350          },  /* [COMM_CHANNEL_10, MRP_ESH_ComMIndication_CN_LIN05_5cb35350] */
  { /*    11 */ ComMConf_ComMChannel_CN_LIN06_c5ba02ea          },  /* [COMM_CHANNEL_11, MRP_ESH_ComMIndication_CN_LIN06_c5ba02ea] */
  { /*    12 */ ComMConf_ComMChannel_CN_LIN07_b2bd327c          },  /* [COMM_CHANNEL_12, MRP_ESH_ComMIndication_CN_LIN07_b2bd327c] */
  { /*    13 */ ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54 }   /* [COMM_CHANNEL_13, MRP_ESH_ComMIndication_CN_SecuritySubnet_e7a0ee54] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_DcmComMapping
**********************************************************************************************************************/
/** 
  \var    BswM_DcmComMapping
  \brief  Maps the external id of BswMDcmComModeRequest to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMDcmComModeRequest.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_DcmComMappingType, BSWM_CONST) BswM_DcmComMapping[6] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                                       ImmediateUserEndIdx  ImmediateUserStartIdx        Referable Keys */
  { /*     0 */ ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae,                  7u,                    6u },  /* [DCM_COM_0, MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae] */
  { /*     1 */ ComMConf_ComMChannel_CN_Backbone2_78967e2c     ,                  8u,                    7u },  /* [DCM_COM_1, MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
  { /*     2 */ ComMConf_ComMChannel_CN_CAN6_b040c073          ,                  9u,                    8u },  /* [DCM_COM_2, MRP_CC_DcmComIndication_CN_CAN6_b040c073] */
  { /*     3 */ ComMConf_ComMChannel_CN_CabSubnet_9ea693f1     ,                 10u,                    9u },  /* [DCM_COM_3, MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1] */
  { /*     4 */ ComMConf_ComMChannel_CN_FMSNet_fce1aae5        ,                 11u,                   10u },  /* [DCM_COM_4, MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  { /*     5 */ ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54,                 12u,                   11u }   /* [DCM_COM_5, MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_DeferredRules
**********************************************************************************************************************/
/** 
  \var    BswM_DeferredRules
  \details
  Element     Description
  RulesIdx    the index of the 1:1 relation pointing to BswM_Rules
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_DeferredRulesType, BSWM_CONST) BswM_DeferredRules[20] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    RulesIdx        Referable Keys */
  { /*     0 */      28u },  /* [R_CC_Rule_DcmEcuReset_Execute] */
  { /*     1 */      29u },  /* [R_CC_Rule_DcmEcuReset_Trigger] */
  { /*     2 */      82u },  /* [R_ESH_RunToPostRun] */
  { /*     3 */      84u },  /* [R_ESH_WaitToShutdown] */
  { /*     4 */      85u },  /* [R_ESH_WakeupToPrep] */
  { /*     5 */      86u },  /* [R_ESH_WaitToWakeup] */
  { /*     6 */      87u },  /* [R_ESH_WakeupToRun] */
  { /*     7 */      90u },  /* [R_ESH_PostRun] */
  { /*     8 */      91u },  /* [R_ESH_PrepToWait] */
  { /*     9 */     128u },  /* [R_CC_DCMResetProcess_Started] */
  { /*    10 */     129u },  /* [R_CC_DCMResetProcess_InProgress] */
  { /*    11 */     130u },  /* [R_CC_Rule_DcmEcuReset_JumpToBTL] */
  { /*    12 */     131u },  /* [R_CC_Rule_NvmWriteAll_Request] */
  { /*    13 */     132u },  /* [R_CC_Rule_BB1_BusOff_Indication] */
  { /*    14 */     133u },  /* [R_CC_Rule_BB2_BusOff_Indication] */
  { /*    15 */     134u },  /* [R_CC_Rule_CAN6_BusOff_Indication] */
  { /*    16 */     135u },  /* [R_CC_Rule_CabSubnet_BusOff_Indication] */
  { /*    17 */     136u },  /* [R_CC_Rule_FMSNet_BusOff_Indication] */
  { /*    18 */     137u },  /* [R_CC_Rule_SecuritySubnet_BusOff_Indication] */
  { /*    19 */     139u }   /* [R_CC_Rule_PvtReportCtrl] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_GenericMapping
**********************************************************************************************************************/
/** 
  \var    BswM_GenericMapping
  \brief  Maps the external id of BswMGenericRequest to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMGenericRequest.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
  InitValue                Initialization value of port.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_GenericMappingType, BSWM_CONST) BswM_GenericMapping[5] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                            ImmediateUserEndIdx                          ImmediateUserStartIdx                          InitValue                                                            Referable Keys */
  { /*     0 */ BSWM_GENERIC_DCMResetProcess        , BSWM_NO_IMMEDIATEUSERENDIDXOFGENERICMAPPING, BSWM_NO_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING, BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Idle           },  /* [GENERIC_0, MRP_DCMResetProcess] */
  { /*     1 */ BSWM_GENERIC_ESH_ComMPendingRequests, BSWM_NO_IMMEDIATEUSERENDIDXOFGENERICMAPPING, BSWM_NO_IMMEDIATEUSERSTARTIDXOFGENERICMAPPING, BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_NO_REQUEST },  /* [GENERIC_1, MRP_ESH_ComMPendingRequests] */
  { /*     2 */ BSWM_GENERIC_ESH_DemInitStatus      ,                                         13u,                                           12u, BSWM_GENERICVALUE_ESH_DemInitStatus_DEM_INITIALIZED           },  /* [GENERIC_2, MRP_ESH_DemInitStatus] */
  { /*     3 */ BSWM_GENERIC_ESH_State              ,                                         14u,                                           13u, BSWM_GENERICVALUE_ESH_State_ESH_INIT                          },  /* [GENERIC_3, MRP_ESH_State] */
  { /*     4 */ BSWM_GENERIC_ISSM_RunRequest        ,                                         15u,                                           14u, BSWM_GENERICVALUE_ISSM_RunRequest_ISSM_NO_RUN_REQUEST         }   /* [GENERIC_4, MRP_ISSM_RunRequest] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ImmediateUser
**********************************************************************************************************************/
/** 
  \var    BswM_ImmediateUser
  \brief  Contains all immediate request ports.
  \details
  Element             Description
  MaskedBits          contains bitcoded the boolean data of BswM_OnInitOfImmediateUser, BswM_RulesIndUsedOfImmediateUser
  RulesIndEndIdx      the end index of the 0:n relation pointing to BswM_RulesInd
  RulesIndStartIdx    the start index of the 0:n relation pointing to BswM_RulesInd
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ImmediateUserType, BSWM_CONST) BswM_ImmediateUser[49] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    MaskedBits  RulesIndEndIdx                         RulesIndStartIdx                               Comment                                                                           Referable Keys */
  { /*     0 */      0x01u,                                    3u,                                      0u },  /* [Name: CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae]                 */  /* [MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae, CANSM_CHANNEL_0] */
  { /*     1 */      0x01u,                                    7u,                                      3u },  /* [Name: CC_CanSMIndication_CN_Backbone2_78967e2c]                      */  /* [MRP_CC_CanSMIndication_CN_Backbone2_78967e2c, CANSM_CHANNEL_1] */
  { /*     2 */      0x01u,                                   10u,                                      7u },  /* [Name: CC_CanSMIndication_CN_CAN6_b040c073]                           */  /* [MRP_CC_CanSMIndication_CN_CAN6_b040c073, CANSM_CHANNEL_2] */
  { /*     3 */      0x01u,                                   13u,                                     10u },  /* [Name: CC_CanSMIndication_CN_CabSubnet_9ea693f1]                      */  /* [MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1, CANSM_CHANNEL_3] */
  { /*     4 */      0x01u,                                   17u,                                     13u },  /* [Name: CC_CanSMIndication_CN_FMSNet_fce1aae5]                         */  /* [MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5, CANSM_CHANNEL_4] */
  { /*     5 */      0x01u,                                   20u,                                     17u },  /* [Name: CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54]                 */  /* [MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54, CANSM_CHANNEL_5] */
  { /*     6 */      0x01u,                                   23u,                                     20u },  /* [Name: CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae]                */  /* [MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae, DCM_COM_0] */
  { /*     7 */      0x01u,                                   27u,                                     23u },  /* [Name: CC_DcmComIndication_CN_Backbone2_78967e2c]                     */  /* [MRP_CC_DcmComIndication_CN_Backbone2_78967e2c, DCM_COM_1] */
  { /*     8 */      0x01u,                                   30u,                                     27u },  /* [Name: CC_DcmComIndication_CN_CAN6_b040c073]                          */  /* [MRP_CC_DcmComIndication_CN_CAN6_b040c073, DCM_COM_2] */
  { /*     9 */      0x01u,                                   33u,                                     30u },  /* [Name: CC_DcmComIndication_CN_CabSubnet_9ea693f1]                     */  /* [MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1, DCM_COM_3] */
  { /*    10 */      0x01u,                                   37u,                                     33u },  /* [Name: CC_DcmComIndication_CN_FMSNet_fce1aae5]                        */  /* [MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5, DCM_COM_4] */
  { /*    11 */      0x01u,                                   40u,                                     37u },  /* [Name: CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54]                */  /* [MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54, DCM_COM_5] */
  { /*    12 */      0x00u, BSWM_NO_RULESINDENDIDXOFIMMEDIATEUSER, BSWM_NO_RULESINDSTARTIDXOFIMMEDIATEUSER },  /* [Name: ESH_DemInitStatus]                                             */  /* [MRP_ESH_DemInitStatus, GENERIC_2] */
  { /*    13 */      0x03u,                                   51u,                                     40u },  /* [Name: ESH_State]                                                     */  /* [MRP_ESH_State, GENERIC_3] */
  { /*    14 */      0x01u,                                   52u,                                     51u },  /* [Name: ISSM_RunRequest]                                               */  /* [MRP_ISSM_RunRequest, GENERIC_4] */
  { /*    15 */      0x01u,                                   54u,                                     52u },  /* [Name: CC_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289] */  /* [MRP_CC_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289, J1939_NM_0] */
  { /*    16 */      0x01u,                                   56u,                                     54u },  /* [Name: CC_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289]         */  /* [MRP_CC_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289, J1939_NM_1] */
  { /*    17 */      0x01u,                                   62u,                                     56u },  /* [Name: Lin1SchInd_Table2]                                             */  /* [COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1], LIN_SCHEDULE_0] */
  { /*    18 */      0x01u,                                   66u,                                     62u },  /* [Name: Lin2SchInd_MasterReq_SlaveResp]                                */  /* [COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0], LIN_SCHEDULE_1] */
  { /*    19 */      0x01u,                                   72u,                                     66u },  /* [Name: Lin3SchInd_MasterReq_SlaveResp_Table2]                         */  /* [COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E], LIN_SCHEDULE_2] */
  { /*    20 */      0x01u,                                   78u,                                     72u },  /* [Name: Lin4SchInd_Table2]                                             */  /* [COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E], LIN_SCHEDULE_3] */
  { /*    21 */      0x01u,                                   84u,                                     78u },  /* [Name: Lin5SchInd_MasterReq_SlaveResp_Table1]                         */  /* [COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E], LIN_SCHEDULE_4] */
  { /*    22 */      0x01u,                                   87u,                                     84u },  /* [Name: Lin6SchInd_MasterReq_SlaveResp_Table0]                         */  /* [COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0], LIN_SCHEDULE_5] */
  { /*    23 */      0x01u,                                   90u,                                     87u },  /* [Name: Lin7SchInd_Table0]                                             */  /* [COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0], LIN_SCHEDULE_6] */
  { /*    24 */      0x01u,                                   93u,                                     90u },  /* [Name: Lin8SchInd_Table0]                                             */  /* [COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp], LIN_SCHEDULE_7] */
  { /*    25 */      0x01u,                                   94u,                                     93u },  /* [Name: LinScheduleEndNotification_LIN1]                               */  /* [MRP_LinScheduleEndNotification_LIN1, LIN_SCHEDULEEND_0] */
  { /*    26 */      0x01u,                                   95u,                                     94u },  /* [Name: LinScheduleEndNotification_LIN2]                               */  /* [MRP_LinScheduleEndNotification_LIN2, LIN_SCHEDULEEND_1] */
  { /*    27 */      0x01u,                                   96u,                                     95u },  /* [Name: LinScheduleEndNotification_LIN3]                               */  /* [MRP_LinScheduleEndNotification_LIN3, LIN_SCHEDULEEND_2] */
  { /*    28 */      0x01u,                                   97u,                                     96u },  /* [Name: LinScheduleEndNotification_LIN4]                               */  /* [MRP_LinScheduleEndNotification_LIN4, LIN_SCHEDULEEND_3] */
  { /*    29 */      0x01u,                                   98u,                                     97u },  /* [Name: LinScheduleEndNotification_LIN5]                               */  /* [MRP_LinScheduleEndNotification_LIN5, LIN_SCHEDULEEND_4] */
  { /*    30 */      0x01u,                                   99u,                                     98u },  /* [Name: LinScheduleEndNotification_LIN6]                               */  /* [MRP_LinScheduleEndNotification_LIN6, LIN_SCHEDULEEND_5] */
  { /*    31 */      0x01u,                                  100u,                                     99u },  /* [Name: LinScheduleEndNotification_LIN7]                               */  /* [MRP_LinScheduleEndNotification_LIN7, LIN_SCHEDULEEND_6] */
  { /*    32 */      0x01u,                                  101u,                                    100u },  /* [Name: LinScheduleEndNotification_LIN8]                               */  /* [MRP_LinScheduleEndNotification_LIN8, LIN_SCHEDULEEND_7] */
  { /*    33 */      0x01u,                                  102u,                                    101u },  /* [Name: CC_LinSMIndication_CN_LIN00_2cd9a7df]                          */  /* [MRP_CC_LinSMIndication_CN_LIN00_2cd9a7df, LINSM_CHANNEL_0] */
  { /*    34 */      0x01u,                                  103u,                                    102u },  /* [Name: CC_LinSMIndication_CN_LIN01_5bde9749]                          */  /* [MRP_CC_LinSMIndication_CN_LIN01_5bde9749, LINSM_CHANNEL_1] */
  { /*    35 */      0x01u,                                  104u,                                    103u },  /* [Name: CC_LinSMIndication_CN_LIN02_c2d7c6f3]                          */  /* [MRP_CC_LinSMIndication_CN_LIN02_c2d7c6f3, LINSM_CHANNEL_2] */
  { /*    36 */      0x01u,                                  105u,                                    104u },  /* [Name: CC_LinSMIndication_CN_LIN03_b5d0f665]                          */  /* [MRP_CC_LinSMIndication_CN_LIN03_b5d0f665, LINSM_CHANNEL_3] */
  { /*    37 */      0x01u,                                  106u,                                    105u },  /* [Name: CC_LinSMIndication_CN_LIN04_2bb463c6]                          */  /* [MRP_CC_LinSMIndication_CN_LIN04_2bb463c6, LINSM_CHANNEL_4] */
  { /*    38 */      0x01u,                                  107u,                                    106u },  /* [Name: CC_LinSMIndication_CN_LIN05_5cb35350]                          */  /* [MRP_CC_LinSMIndication_CN_LIN05_5cb35350, LINSM_CHANNEL_5] */
  { /*    39 */      0x01u,                                  108u,                                    107u },  /* [Name: CC_LinSMIndication_CN_LIN06_c5ba02ea]                          */  /* [MRP_CC_LinSMIndication_CN_LIN06_c5ba02ea, LINSM_CHANNEL_6] */
  { /*    40 */      0x01u,                                  109u,                                    108u },  /* [Name: CC_LinSMIndication_CN_LIN07_b2bd327c]                          */  /* [MRP_CC_LinSMIndication_CN_LIN07_b2bd327c, LINSM_CHANNEL_7] */
  { /*    41 */      0x01u,                                  116u,                                    109u },  /* [Name: LIN1_ScheduleTableRequestMode]                                 */  /* [MRP_LIN1_ScheduleTableRequestMode, SWC_REQUEST_0] */
  { /*    42 */      0x01u,                                  121u,                                    116u },  /* [Name: LIN2_ScheduleTableRequestMode]                                 */  /* [MRP_LIN2_ScheduleTableRequestMode, SWC_REQUEST_1] */
  { /*    43 */      0x01u,                                  128u,                                    121u },  /* [Name: LIN3_ScheduleTableRequestMode]                                 */  /* [MRP_LIN3_ScheduleTableRequestMode, SWC_REQUEST_2] */
  { /*    44 */      0x01u,                                  135u,                                    128u },  /* [Name: LIN4_ScheduleTableRequestMode]                                 */  /* [MRP_LIN4_ScheduleTableRequestMode, SWC_REQUEST_3] */
  { /*    45 */      0x01u,                                  142u,                                    135u },  /* [Name: LIN5_ScheduleTableRequestMode]                                 */  /* [MRP_LIN5_ScheduleTableRequestMode, SWC_REQUEST_4] */
  { /*    46 */      0x01u,                                  145u,                                    142u },  /* [Name: LIN6_ScheduleTableRequestMode]                                 */  /* [MRP_LIN6_ScheduleTableRequestMode, SWC_REQUEST_5] */
  { /*    47 */      0x01u,                                  148u,                                    145u },  /* [Name: LIN7_ScheduleTableRequestMode]                                 */  /* [MRP_LIN7_ScheduleTableRequestMode, SWC_REQUEST_6] */
  { /*    48 */      0x01u,                                  151u,                                    148u }   /* [Name: LIN8_ScheduleTableRequestMode]                                 */  /* [MRP_LIN8_ScheduleTableRequestMode, SWC_REQUEST_7] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_InitGenVarAndInitAL
**********************************************************************************************************************/
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_InitGenVarAndInitALType, BSWM_CONST) BswM_InitGenVarAndInitAL[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     InitGenVarAndInitAL                            */
  /*     0 */ BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION 
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_J1939NmMapping
**********************************************************************************************************************/
/** 
  \var    BswM_J1939NmMapping
  \brief  Maps the external id of BswMJ1939NmIndication to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMJ1939NmIndication.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_J1939NmMappingType, BSWM_CONST) BswM_J1939NmMapping[2] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                                                                                         ImmediateUserEndIdx  ImmediateUserStartIdx        Referable Keys */
  { /*     0 */ ((J1939NmConf_J1939NmNode_CIOM_4d5cd289 << 8u) | ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae),                 16u,                   15u },  /* [J1939_NM_0, MRP_CC_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289] */
  { /*     1 */ ((J1939NmConf_J1939NmNode_CIOM_4d5cd289 << 8u) | ComMConf_ComMChannel_CN_FMSNet_fce1aae5)        ,                 17u,                   16u }   /* [J1939_NM_1, MRP_CC_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinSMMapping
**********************************************************************************************************************/
/** 
  \var    BswM_LinSMMapping
  \brief  Maps the external id of BswMLinSMIndication to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMLinSMIndication.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_LinSMMappingType, BSWM_CONST) BswM_LinSMMapping[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                              ImmediateUserEndIdx  ImmediateUserStartIdx        Referable Keys */
  { /*     0 */ ComMConf_ComMChannel_CN_LIN00_2cd9a7df,                 34u,                   33u },  /* [LINSM_CHANNEL_0, MRP_CC_LinSMIndication_CN_LIN00_2cd9a7df] */
  { /*     1 */ ComMConf_ComMChannel_CN_LIN01_5bde9749,                 35u,                   34u },  /* [LINSM_CHANNEL_1, MRP_CC_LinSMIndication_CN_LIN01_5bde9749] */
  { /*     2 */ ComMConf_ComMChannel_CN_LIN02_c2d7c6f3,                 36u,                   35u },  /* [LINSM_CHANNEL_2, MRP_CC_LinSMIndication_CN_LIN02_c2d7c6f3] */
  { /*     3 */ ComMConf_ComMChannel_CN_LIN03_b5d0f665,                 37u,                   36u },  /* [LINSM_CHANNEL_3, MRP_CC_LinSMIndication_CN_LIN03_b5d0f665] */
  { /*     4 */ ComMConf_ComMChannel_CN_LIN04_2bb463c6,                 38u,                   37u },  /* [LINSM_CHANNEL_4, MRP_CC_LinSMIndication_CN_LIN04_2bb463c6] */
  { /*     5 */ ComMConf_ComMChannel_CN_LIN05_5cb35350,                 39u,                   38u },  /* [LINSM_CHANNEL_5, MRP_CC_LinSMIndication_CN_LIN05_5cb35350] */
  { /*     6 */ ComMConf_ComMChannel_CN_LIN06_c5ba02ea,                 40u,                   39u },  /* [LINSM_CHANNEL_6, MRP_CC_LinSMIndication_CN_LIN06_c5ba02ea] */
  { /*     7 */ ComMConf_ComMChannel_CN_LIN07_b2bd327c,                 41u,                   40u }   /* [LINSM_CHANNEL_7, MRP_CC_LinSMIndication_CN_LIN07_b2bd327c] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinScheduleEndMapping
**********************************************************************************************************************/
/** 
  \var    BswM_LinScheduleEndMapping
  \brief  Maps the external id of BswMLinScheduleEndNotification to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMLinScheduleEndNotification.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_LinScheduleEndMappingType, BSWM_CONST) BswM_LinScheduleEndMapping[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                              ImmediateUserEndIdx  ImmediateUserStartIdx        Referable Keys */
  { /*     0 */ ComMConf_ComMChannel_CN_LIN00_2cd9a7df,                 26u,                   25u },  /* [LIN_SCHEDULEEND_0, MRP_LinScheduleEndNotification_LIN1] */
  { /*     1 */ ComMConf_ComMChannel_CN_LIN01_5bde9749,                 27u,                   26u },  /* [LIN_SCHEDULEEND_1, MRP_LinScheduleEndNotification_LIN2] */
  { /*     2 */ ComMConf_ComMChannel_CN_LIN02_c2d7c6f3,                 28u,                   27u },  /* [LIN_SCHEDULEEND_2, MRP_LinScheduleEndNotification_LIN3] */
  { /*     3 */ ComMConf_ComMChannel_CN_LIN03_b5d0f665,                 29u,                   28u },  /* [LIN_SCHEDULEEND_3, MRP_LinScheduleEndNotification_LIN4] */
  { /*     4 */ ComMConf_ComMChannel_CN_LIN04_2bb463c6,                 30u,                   29u },  /* [LIN_SCHEDULEEND_4, MRP_LinScheduleEndNotification_LIN5] */
  { /*     5 */ ComMConf_ComMChannel_CN_LIN05_5cb35350,                 31u,                   30u },  /* [LIN_SCHEDULEEND_5, MRP_LinScheduleEndNotification_LIN6] */
  { /*     6 */ ComMConf_ComMChannel_CN_LIN06_c5ba02ea,                 32u,                   31u },  /* [LIN_SCHEDULEEND_6, MRP_LinScheduleEndNotification_LIN7] */
  { /*     7 */ ComMConf_ComMChannel_CN_LIN07_b2bd327c,                 33u,                   32u }   /* [LIN_SCHEDULEEND_7, MRP_LinScheduleEndNotification_LIN8] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinScheduleMapping
**********************************************************************************************************************/
/** 
  \var    BswM_LinScheduleMapping
  \brief  Maps the external id of BswMLinScheduleIndication to an internal id and references immediate request ports.
  \details
  Element                  Description
  ExternalId               External id of BswMLinScheduleIndication.
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_LinScheduleMappingType, BSWM_CONST) BswM_LinScheduleMapping[8] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ExternalId                              ImmediateUserEndIdx  ImmediateUserStartIdx        Referable Keys */
  { /*     0 */ ComMConf_ComMChannel_CN_LIN00_2cd9a7df,                 18u,                   17u },  /* [LIN_SCHEDULE_0, COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  { /*     1 */ ComMConf_ComMChannel_CN_LIN01_5bde9749,                 19u,                   18u },  /* [LIN_SCHEDULE_1, COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  { /*     2 */ ComMConf_ComMChannel_CN_LIN02_c2d7c6f3,                 20u,                   19u },  /* [LIN_SCHEDULE_2, COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  { /*     3 */ ComMConf_ComMChannel_CN_LIN03_b5d0f665,                 21u,                   20u },  /* [LIN_SCHEDULE_3, COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  { /*     4 */ ComMConf_ComMChannel_CN_LIN04_2bb463c6,                 22u,                   21u },  /* [LIN_SCHEDULE_4, COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  { /*     5 */ ComMConf_ComMChannel_CN_LIN05_5cb35350,                 23u,                   22u },  /* [LIN_SCHEDULE_5, COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0]] */
  { /*     6 */ ComMConf_ComMChannel_CN_LIN06_c5ba02ea,                 24u,                   23u },  /* [LIN_SCHEDULE_6, COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0]] */
  { /*     7 */ ComMConf_ComMChannel_CN_LIN07_b2bd327c,                 25u,                   24u }   /* [LIN_SCHEDULE_7, COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp]] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ModeNotificationFct
**********************************************************************************************************************/
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_PartitionFunctionType, BSWM_CONST) BswM_ModeNotificationFct[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     ModeNotificationFct                            */
  /*     0 */ BswM_ModeNotificationFct_BSWM_SINGLEPARTITION 
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ModeRequestMapping
**********************************************************************************************************************/
/** 
  \var    BswM_ModeRequestMapping
  \brief  Maps the external id of BswMSwcModeRequest to an internal id and references immediate request ports.
  \details
  Element                  Description
  ImmediateUserEndIdx      the end index of the 0:n relation pointing to BswM_ImmediateUser
  ImmediateUserStartIdx    the start index of the 0:n relation pointing to BswM_ImmediateUser
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_ModeRequestMappingType, BSWM_CONST) BswM_ModeRequestMapping[10] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    ImmediateUserEndIdx                              ImmediateUserStartIdx                                    Referable Keys */
  { /*     0 */                                             42u,                                               41u },  /* [SWC_REQUEST_0, MRP_LIN1_ScheduleTableRequestMode] */
  { /*     1 */                                             43u,                                               42u },  /* [SWC_REQUEST_1, MRP_LIN2_ScheduleTableRequestMode] */
  { /*     2 */                                             44u,                                               43u },  /* [SWC_REQUEST_2, MRP_LIN3_ScheduleTableRequestMode] */
  { /*     3 */                                             45u,                                               44u },  /* [SWC_REQUEST_3, MRP_LIN4_ScheduleTableRequestMode] */
  { /*     4 */                                             46u,                                               45u },  /* [SWC_REQUEST_4, MRP_LIN5_ScheduleTableRequestMode] */
  { /*     5 */                                             47u,                                               46u },  /* [SWC_REQUEST_5, MRP_LIN6_ScheduleTableRequestMode] */
  { /*     6 */                                             48u,                                               47u },  /* [SWC_REQUEST_6, MRP_LIN7_ScheduleTableRequestMode] */
  { /*     7 */                                             49u,                                               48u },  /* [SWC_REQUEST_7, MRP_LIN8_ScheduleTableRequestMode] */
  { /*     8 */ BSWM_NO_IMMEDIATEUSERENDIDXOFMODEREQUESTMAPPING, BSWM_NO_IMMEDIATEUSERSTARTIDXOFMODEREQUESTMAPPING },  /* [SWC_REQUEST_8, MRP_SwcModeRequest_NvmWriteAllRequest] */
  { /*     9 */ BSWM_NO_IMMEDIATEUSERENDIDXOFMODEREQUESTMAPPING, BSWM_NO_IMMEDIATEUSERSTARTIDXOFMODEREQUESTMAPPING }   /* [SWC_REQUEST_9, MRP_SwcModeRequest_PvtReportCtrl] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_PartitionIdentifiers
**********************************************************************************************************************/
/** 
  \var    BswM_PartitionIdentifiers
  \brief  the partition contex in Config
  \details
  Element                 Description
  PartitionSNV        
  PCPartitionConfigIdx    the index of the 1:1 relation pointing to BswM_PCPartitionConfig
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_PartitionIdentifiersType, BSWM_CONST) BswM_PartitionIdentifiers[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    PartitionSNV          PCPartitionConfigIdx */
  { /*     0 */ BSWM_SINGLEPARTITION,                   0u }
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_Rules
**********************************************************************************************************************/
/** 
  \var    BswM_Rules
  \details
  Element          Description
  Id               External id of rule.
  RuleStatesIdx    the index of the 1:1 relation pointing to BswM_RuleStates
  FctPtr           Pointer to the rule function which does the arbitration.
*/ 
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_RulesType, BSWM_CONST) BswM_Rules[140] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
    /* Index    Id    RuleStatesIdx  FctPtr                                                               Referable Keys */
  { /*     0 */  43u,            0u, BswM_Rule_CC_Rule_LIN1_SchTableStartInd_Table1                },  /* [R_CC_Rule_LIN1_SchTableStartInd_Table1, COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  { /*     1 */  55u,            1u, BswM_Rule_CC_Rule_LIN2_SchTableStartInd_MSTable0              },  /* [R_CC_Rule_LIN2_SchTableStartInd_MSTable0, COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  { /*     2 */  64u,            2u, BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable               },  /* [R_CC_Rule_LIN3_SchTableStartInd_MSTable, COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  { /*     3 */  78u,            3u, BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable               },  /* [R_CC_Rule_LIN4_SchTableStartInd_MSTable, COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  { /*     4 */  92u,            4u, BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable               },  /* [R_CC_Rule_LIN5_SchTableStartInd_MSTable, COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  { /*     5 */  44u,            5u, BswM_Rule_CC_Rule_LIN1_SchTableStartInd_Table2                },  /* [R_CC_Rule_LIN1_SchTableStartInd_Table2, COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  { /*     6 */  45u,            6u, BswM_Rule_CC_Rule_LIN1_SchTableStartInd_TableE                },  /* [R_CC_Rule_LIN1_SchTableStartInd_TableE, COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  { /*     7 */  41u,            7u, BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable1              },  /* [R_CC_Rule_LIN1_SchTableStartInd_MSTable1, COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  { /*     8 */  42u,            8u, BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable2              },  /* [R_CC_Rule_LIN1_SchTableStartInd_MSTable2, COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  { /*     9 */  56u,            9u, BswM_Rule_CC_Rule_LIN2_SchTableStartInd_Table0                },  /* [R_CC_Rule_LIN2_SchTableStartInd_Table0, COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  { /*    10 */  57u,           10u, BswM_Rule_CC_Rule_LIN2_SchTableStartInd_TableE                },  /* [R_CC_Rule_LIN2_SchTableStartInd_TableE, COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  { /*    11 */  40u,           11u, BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable               },  /* [R_CC_Rule_LIN1_SchTableStartInd_MSTable, COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  { /*    12 */  54u,           12u, BswM_Rule_CC_Rule_LIN2_SchTableStartInd_MSTable               },  /* [R_CC_Rule_LIN2_SchTableStartInd_MSTable, COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  { /*    13 */  65u,           13u, BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable1              },  /* [R_CC_Rule_LIN3_SchTableStartInd_MSTable1, COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  { /*    14 */  66u,           14u, BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable2              },  /* [R_CC_Rule_LIN3_SchTableStartInd_MSTable2, COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  { /*    15 */  67u,           15u, BswM_Rule_CC_Rule_LIN3_SchTableStartInd_Table1                },  /* [R_CC_Rule_LIN3_SchTableStartInd_Table1, COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  { /*    16 */  68u,           16u, BswM_Rule_CC_Rule_LIN3_SchTableStartInd_Table2                },  /* [R_CC_Rule_LIN3_SchTableStartInd_Table2, COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  { /*    17 */  69u,           17u, BswM_Rule_CC_Rule_LIN3_SchTableStartInd_TableE                },  /* [R_CC_Rule_LIN3_SchTableStartInd_TableE, COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  { /*    18 */  79u,           18u, BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable1              },  /* [R_CC_Rule_LIN4_SchTableStartInd_MSTable1, COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  { /*    19 */  80u,           19u, BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable2              },  /* [R_CC_Rule_LIN4_SchTableStartInd_MSTable2, COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  { /*    20 */  81u,           20u, BswM_Rule_CC_Rule_LIN4_SchTableStartInd_Table1                },  /* [R_CC_Rule_LIN4_SchTableStartInd_Table1, COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  { /*    21 */  82u,           21u, BswM_Rule_CC_Rule_LIN4_SchTableStartInd_Table2                },  /* [R_CC_Rule_LIN4_SchTableStartInd_Table2, COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  { /*    22 */  83u,           22u, BswM_Rule_CC_Rule_LIN4_SchTableStartInd_TableE                },  /* [R_CC_Rule_LIN4_SchTableStartInd_TableE, COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  { /*    23 */  94u,           23u, BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable2              },  /* [R_CC_Rule_LIN5_SchTableStartInd_MSTable2, COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  { /*    24 */  93u,           24u, BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable1              },  /* [R_CC_Rule_LIN5_SchTableStartInd_MSTable1, COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  { /*    25 */  95u,           25u, BswM_Rule_CC_Rule_LIN5_SchTableStartInd_Table1                },  /* [R_CC_Rule_LIN5_SchTableStartInd_Table1, COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  { /*    26 */  96u,           26u, BswM_Rule_CC_Rule_LIN5_SchTableStartInd_Table2                },  /* [R_CC_Rule_LIN5_SchTableStartInd_Table2, COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  { /*    27 */  97u,           27u, BswM_Rule_CC_Rule_LIN5_SchTableStartInd_TableE                },  /* [R_CC_Rule_LIN5_SchTableStartInd_TableE, COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  { /*    28 */  36u,           28u, BswM_Rule_CC_Rule_DcmEcuReset_Execute                         },  /* [R_CC_Rule_DcmEcuReset_Execute, MRP_SwcModeNotification_DcmEcuReset, MRP_DCMResetProcess] */
  { /*    29 */  38u,           29u, BswM_Rule_CC_Rule_DcmEcuReset_Trigger                         },  /* [R_CC_Rule_DcmEcuReset_Trigger, MRP_ESH_State, MRP_ESH_ModeNotification, MRP_SwcModeNotification_DcmEcuReset] */
  { /*    30 */  75u,           30u, BswM_Rule_CC_Rule_LIN3_Schedule_To_Table1                     },  /* [R_CC_Rule_LIN3_Schedule_To_Table1, MRP_LIN3_ScheduleTableRequestMode] */
  { /*    31 */  74u,           31u, BswM_Rule_CC_Rule_LIN3_Schedule_To_NULL                       },  /* [R_CC_Rule_LIN3_Schedule_To_NULL, MRP_LIN3_ScheduleTableRequestMode] */
  { /*    32 */  76u,           32u, BswM_Rule_CC_Rule_LIN3_Schedule_To_Table2                     },  /* [R_CC_Rule_LIN3_Schedule_To_Table2, MRP_LIN3_ScheduleTableRequestMode] */
  { /*    33 */  77u,           33u, BswM_Rule_CC_Rule_LIN3_Schedule_To_Table_E                    },  /* [R_CC_Rule_LIN3_Schedule_To_Table_E, MRP_LIN3_ScheduleTableRequestMode] */
  { /*    34 */  71u,           34u, BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp        },  /* [R_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp, MRP_LIN3_ScheduleTableRequestMode] */
  { /*    35 */  72u,           35u, BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1 },  /* [R_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1, MRP_LIN3_ScheduleTableRequestMode] */
  { /*    36 */  73u,           36u, BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2 },  /* [R_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2, MRP_LIN3_ScheduleTableRequestMode] */
  { /*    37 */  47u,           37u, BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp        },  /* [R_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp, MRP_LIN1_ScheduleTableRequestMode] */
  { /*    38 */  48u,           38u, BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1 },  /* [R_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1, MRP_LIN1_ScheduleTableRequestMode] */
  { /*    39 */  49u,           39u, BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2 },  /* [R_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2, MRP_LIN1_ScheduleTableRequestMode] */
  { /*    40 */  50u,           40u, BswM_Rule_CC_Rule_LIN1_Schedule_To_NULL                       },  /* [R_CC_Rule_LIN1_Schedule_To_NULL, MRP_LIN1_ScheduleTableRequestMode] */
  { /*    41 */  51u,           41u, BswM_Rule_CC_Rule_LIN1_Schedule_To_Table1                     },  /* [R_CC_Rule_LIN1_Schedule_To_Table1, MRP_LIN1_ScheduleTableRequestMode] */
  { /*    42 */  52u,           42u, BswM_Rule_CC_Rule_LIN1_Schedule_To_Table2                     },  /* [R_CC_Rule_LIN1_Schedule_To_Table2, MRP_LIN1_ScheduleTableRequestMode] */
  { /*    43 */  53u,           43u, BswM_Rule_CC_Rule_LIN1_Schedule_To_Table_E                    },  /* [R_CC_Rule_LIN1_Schedule_To_Table_E, MRP_LIN1_ScheduleTableRequestMode] */
  { /*    44 */  59u,           44u, BswM_Rule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp        },  /* [R_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp, MRP_LIN2_ScheduleTableRequestMode] */
  { /*    45 */  60u,           45u, BswM_Rule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0 },  /* [R_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0, MRP_LIN2_ScheduleTableRequestMode] */
  { /*    46 */  61u,           46u, BswM_Rule_CC_Rule_LIN2_Schedule_To_NULL                       },  /* [R_CC_Rule_LIN2_Schedule_To_NULL, MRP_LIN2_ScheduleTableRequestMode] */
  { /*    47 */  62u,           47u, BswM_Rule_CC_Rule_LIN2_Schedule_To_Table0                     },  /* [R_CC_Rule_LIN2_Schedule_To_Table0, MRP_LIN2_ScheduleTableRequestMode] */
  { /*    48 */  63u,           48u, BswM_Rule_CC_Rule_LIN2_Schedule_To_Table_E                    },  /* [R_CC_Rule_LIN2_Schedule_To_Table_E, MRP_LIN2_ScheduleTableRequestMode] */
  { /*    49 */  85u,           49u, BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp        },  /* [R_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp, MRP_LIN4_ScheduleTableRequestMode] */
    /* Index    Id    RuleStatesIdx  FctPtr                                                               Referable Keys */
  { /*    50 */  86u,           50u, BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1 },  /* [R_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1, MRP_LIN4_ScheduleTableRequestMode] */
  { /*    51 */  87u,           51u, BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2 },  /* [R_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2, MRP_LIN4_ScheduleTableRequestMode] */
  { /*    52 */  88u,           52u, BswM_Rule_CC_Rule_LIN4_Schedule_To_NULL                       },  /* [R_CC_Rule_LIN4_Schedule_To_NULL, MRP_LIN4_ScheduleTableRequestMode] */
  { /*    53 */  89u,           53u, BswM_Rule_CC_Rule_LIN4_Schedule_To_Table1                     },  /* [R_CC_Rule_LIN4_Schedule_To_Table1, MRP_LIN4_ScheduleTableRequestMode] */
  { /*    54 */  90u,           54u, BswM_Rule_CC_Rule_LIN4_Schedule_To_Table2                     },  /* [R_CC_Rule_LIN4_Schedule_To_Table2, MRP_LIN4_ScheduleTableRequestMode] */
  { /*    55 */  91u,           55u, BswM_Rule_CC_Rule_LIN4_Schedule_To_Table_E                    },  /* [R_CC_Rule_LIN4_Schedule_To_Table_E, MRP_LIN4_ScheduleTableRequestMode] */
  { /*    56 */  99u,           56u, BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp        },  /* [R_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp, MRP_LIN5_ScheduleTableRequestMode] */
  { /*    57 */ 100u,           57u, BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1 },  /* [R_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1, MRP_LIN5_ScheduleTableRequestMode] */
  { /*    58 */ 101u,           58u, BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2 },  /* [R_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2, MRP_LIN5_ScheduleTableRequestMode] */
  { /*    59 */ 102u,           59u, BswM_Rule_CC_Rule_LIN5_Schedule_To_NULL                       },  /* [R_CC_Rule_LIN5_Schedule_To_NULL, MRP_LIN5_ScheduleTableRequestMode] */
  { /*    60 */ 103u,           60u, BswM_Rule_CC_Rule_LIN5_Schedule_To_Table1                     },  /* [R_CC_Rule_LIN5_Schedule_To_Table1, MRP_LIN5_ScheduleTableRequestMode] */
  { /*    61 */ 104u,           61u, BswM_Rule_CC_Rule_LIN5_Schedule_To_Table2                     },  /* [R_CC_Rule_LIN5_Schedule_To_Table2, MRP_LIN5_ScheduleTableRequestMode] */
  { /*    62 */ 105u,           62u, BswM_Rule_CC_Rule_LIN5_Schedule_To_Table_E                    },  /* [R_CC_Rule_LIN5_Schedule_To_Table_E, MRP_LIN5_ScheduleTableRequestMode] */
  { /*    63 */  12u,           63u, BswM_Rule_CC_CN_CabSubnet_9ea693f1_RX_DM                      },  /* [R_CC_CN_CabSubnet_9ea693f1_RX_DM, MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1, MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1] */
  { /*    64 */   1u,           64u, BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289         },  /* [R_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289, MRP_CC_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289] */
  { /*    65 */   6u,           65u, BswM_Rule_CC_CN_Backbone2_78967e2c_RX_DM                      },  /* [R_CC_CN_Backbone2_78967e2c_RX_DM, MRP_CC_CanSMIndication_CN_Backbone2_78967e2c, MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
  { /*    66 */  21u,           66u, BswM_Rule_CC_CN_LIN02_c2d7c6f3                                },  /* [R_CC_CN_LIN02_c2d7c6f3, MRP_CC_LinSMIndication_CN_LIN02_c2d7c6f3] */
  { /*    67 */  27u,           67u, BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_RX                    },  /* [R_CC_CN_SecuritySubnet_e7a0ee54_RX, MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54, MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54] */
  { /*    68 */  28u,           68u, BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_RX_DM                 },  /* [R_CC_CN_SecuritySubnet_e7a0ee54_RX_DM, MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54, MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54] */
  { /*    69 */  18u,           69u, BswM_Rule_CC_CN_FMSNet_fce1aae5_TX                            },  /* [R_CC_CN_FMSNet_fce1aae5_TX, MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5, MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  { /*    70 */   5u,           70u, BswM_Rule_CC_CN_Backbone2_78967e2c_RX                         },  /* [R_CC_CN_Backbone2_78967e2c_RX, MRP_CC_CanSMIndication_CN_Backbone2_78967e2c, MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
  { /*    71 */  14u,           71u, BswM_Rule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289                 },  /* [R_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289, MRP_CC_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289] */
  { /*    72 */  11u,           72u, BswM_Rule_CC_CN_CabSubnet_9ea693f1_RX                         },  /* [R_CC_CN_CabSubnet_9ea693f1_RX, MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1, MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1] */
  { /*    73 */  22u,           73u, BswM_Rule_CC_CN_LIN03_b5d0f665                                },  /* [R_CC_CN_LIN03_b5d0f665, MRP_CC_LinSMIndication_CN_LIN03_b5d0f665] */
  { /*    74 */  16u,           74u, BswM_Rule_CC_CN_FMSNet_fce1aae5_RX                            },  /* [R_CC_CN_FMSNet_fce1aae5_RX, MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5, MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  { /*    75 */  20u,           75u, BswM_Rule_CC_CN_LIN01_5bde9749                                },  /* [R_CC_CN_LIN01_5bde9749, MRP_CC_LinSMIndication_CN_LIN01_5bde9749] */
  { /*    76 */  23u,           76u, BswM_Rule_CC_CN_LIN04_2bb463c6                                },  /* [R_CC_CN_LIN04_2bb463c6, MRP_CC_LinSMIndication_CN_LIN04_2bb463c6] */
  { /*    77 */   3u,           77u, BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_RX                    },  /* [R_CC_CN_Backbone1J1939_0b1f4bae_RX, MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae, MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae] */
  { /*    78 */  17u,           78u, BswM_Rule_CC_CN_FMSNet_fce1aae5_RX_DM                         },  /* [R_CC_CN_FMSNet_fce1aae5_RX_DM, MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5, MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  { /*    79 */   4u,           79u, BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_RX_DM                 },  /* [R_CC_CN_Backbone1J1939_0b1f4bae_RX_DM, MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae, MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae] */
  { /*    80 */  19u,           80u, BswM_Rule_CC_CN_LIN00_2cd9a7df                                },  /* [R_CC_CN_LIN00_2cd9a7df, MRP_CC_LinSMIndication_CN_LIN00_2cd9a7df] */
  { /*    81 */  15u,           81u, BswM_Rule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX              },  /* [R_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX, MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5, MRP_CC_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289, MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  { /*    82 */ 134u,           82u, BswM_Rule_ESH_RunToPostRun                                    },  /* [R_ESH_RunToPostRun, MRP_ISSM_RunRequest, MRP_ESH_State, MRP_ESH_ModeNotification, MRP_ESH_ComMIndication_CN_Backbone1J1939_0b1f4bae, MRP_ESH_ComMIndication_CN_LIN02_c2d7c6f3, MRP_ESH_ComMIndication_CN_LIN04_2bb463c6, MRP_ESH_ComMIndication_CN_CabSubnet_9ea693f1, MRP_ESH_ComMIndication_CN_LIN03_b5d0f665, MRP_ESH_ComMIndication_CN_LIN00_2cd9a7df, MRP_ESH_ComMIndication_CN_Backbone2_78967e2c, MRP_ESH_ComMIndication_CN_LIN01_5bde9749, MRP_ESH_ComMIndication_CN_FMSNet_fce1aae5, MRP_ESH_ComMIndication_CN_SecuritySubnet_e7a0ee54, MRP_ESH_ComMIndication_CN_LIN06_c5ba02ea, MRP_ESH_ComMIndication_CN_LIN05_5cb35350, MRP_ESH_ComMIndication_CN_LIN07_b2bd327c, MRP_ESH_ComMIndication_CN_CAN6_b040c073, MRP_ESH_SelfRunRequestTimer] */
  { /*    83 */ 135u,           83u, BswM_Rule_ESH_RunToPostRunNested                              },  /* [R_ESH_RunToPostRunNested] */
  { /*    84 */ 136u,           84u, BswM_Rule_ESH_WaitToShutdown                                  },  /* [R_ESH_WaitToShutdown, MRP_ESH_State, MRP_ESH_NvMIndication, MRP_MemIfStatus, MRP_ESH_NvM_WriteAllTimer, MRP_ESH_EcuM_GetValidatedWakeupEvents, MRP_ESH_ComMPendingRequests] */
  { /*    85 */ 138u,           85u, BswM_Rule_ESH_WakeupToPrep                                    },  /* [R_ESH_WakeupToPrep, MRP_ESH_State, MRP_ESH_NvMIndication, MRP_ESH_NvM_CancelWriteAllTimer, MRP_ESH_EcuM_GetPendingWakeupEvents, MRP_ESH_EcuM_GetValidatedWakeupEvents, MRP_ESH_ComMPendingRequests, MRP_ESH_ModeNotification] */
  { /*    86 */ 137u,           86u, BswM_Rule_ESH_WaitToWakeup                                    },  /* [R_ESH_WaitToWakeup, MRP_SwcModeNotification_DcmEcuReset, MRP_ESH_State, MRP_ESH_EcuM_GetValidatedWakeupEvents, MRP_ESH_ComMPendingRequests] */
  { /*    87 */ 139u,           87u, BswM_Rule_ESH_WakeupToRun                                     },  /* [R_ESH_WakeupToRun, MRP_ESH_State, MRP_ESH_EcuM_GetValidatedWakeupEvents, MRP_ESH_ComMPendingRequests, MRP_ESH_NvMIndication, MRP_ESH_NvM_CancelWriteAllTimer, MRP_ESH_ModeNotification] */
  { /*    88 */ 130u,           88u, BswM_Rule_ESH_InitToWakeup                                    },  /* [R_ESH_InitToWakeup, MRP_ESH_State] */
  { /*    89 */ 132u,           89u, BswM_Rule_ESH_PostRunNested                                   },  /* [R_ESH_PostRunNested] */
  { /*    90 */ 131u,           90u, BswM_Rule_ESH_PostRun                                         },  /* [R_ESH_PostRun, MRP_ESH_State, MRP_ESH_ModeNotification] */
  { /*    91 */ 133u,           91u, BswM_Rule_ESH_PrepToWait                                      },  /* [R_ESH_PrepToWait, MRP_ESH_State, MRP_ESH_ModeNotification] */
  { /*    92 */ 129u,           92u, BswM_Rule_ESH_DemInit                                         },  /* [R_ESH_DemInit] */
  { /*    93 */  29u,           93u, BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_TX                    },  /* [R_CC_CN_SecuritySubnet_e7a0ee54_TX, MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54, MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54] */
  { /*    94 */  13u,           94u, BswM_Rule_CC_CN_CabSubnet_9ea693f1_TX                         },  /* [R_CC_CN_CabSubnet_9ea693f1_TX, MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1, MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1] */
  { /*    95 */   7u,           95u, BswM_Rule_CC_CN_Backbone2_78967e2c_TX                         },  /* [R_CC_CN_Backbone2_78967e2c_TX, MRP_CC_CanSMIndication_CN_Backbone2_78967e2c, MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
  { /*    96 */  46u,           96u, BswM_Rule_CC_Rule_LIN1_ScheduleTableEndNotification           },  /* [R_CC_Rule_LIN1_ScheduleTableEndNotification, MRP_LinScheduleEndNotification_LIN1] */
  { /*    97 */  58u,           97u, BswM_Rule_CC_Rule_LIN2_ScheduleTableEndNotification           },  /* [R_CC_Rule_LIN2_ScheduleTableEndNotification, MRP_LinScheduleEndNotification_LIN2] */
  { /*    98 */  70u,           98u, BswM_Rule_CC_Rule_LIN3_ScheduleTableEndNotification           },  /* [R_CC_Rule_LIN3_ScheduleTableEndNotification, MRP_LinScheduleEndNotification_LIN3] */
  { /*    99 */  84u,           99u, BswM_Rule_CC_Rule_LIN4_ScheduleTableEndNotification           },  /* [R_CC_Rule_LIN4_ScheduleTableEndNotification, MRP_LinScheduleEndNotification_LIN4] */
    /* Index    Id    RuleStatesIdx  FctPtr                                                               Referable Keys */
  { /*   100 */  98u,          100u, BswM_Rule_CC_Rule_LIN5_ScheduleTableEndNotification           },  /* [R_CC_Rule_LIN5_ScheduleTableEndNotification, MRP_LinScheduleEndNotification_LIN5] */
  { /*   101 */  25u,          101u, BswM_Rule_CC_CN_LIN06_c5ba02ea                                },  /* [R_CC_CN_LIN06_c5ba02ea, MRP_CC_LinSMIndication_CN_LIN06_c5ba02ea] */
  { /*   102 */  24u,          102u, BswM_Rule_CC_CN_LIN05_5cb35350                                },  /* [R_CC_CN_LIN05_5cb35350, MRP_CC_LinSMIndication_CN_LIN05_5cb35350] */
  { /*   103 */  26u,          103u, BswM_Rule_CC_CN_LIN07_b2bd327c                                },  /* [R_CC_CN_LIN07_b2bd327c, MRP_CC_LinSMIndication_CN_LIN07_b2bd327c] */
  { /*   104 */   8u,          104u, BswM_Rule_CC_CN_CAN6_b040c073_RX                              },  /* [R_CC_CN_CAN6_b040c073_RX, MRP_CC_CanSMIndication_CN_CAN6_b040c073, MRP_CC_DcmComIndication_CN_CAN6_b040c073] */
  { /*   105 */  10u,          105u, BswM_Rule_CC_CN_CAN6_b040c073_TX                              },  /* [R_CC_CN_CAN6_b040c073_TX, MRP_CC_CanSMIndication_CN_CAN6_b040c073, MRP_CC_DcmComIndication_CN_CAN6_b040c073] */
  { /*   106 */   9u,          106u, BswM_Rule_CC_CN_CAN6_b040c073_RX_DM                           },  /* [R_CC_CN_CAN6_b040c073_RX_DM, MRP_CC_CanSMIndication_CN_CAN6_b040c073, MRP_CC_DcmComIndication_CN_CAN6_b040c073] */
  { /*   107 */ 109u,          107u, BswM_Rule_CC_Rule_LIN6_ScheduleTableEndNotification           },  /* [R_CC_Rule_LIN6_ScheduleTableEndNotification, MRP_LinScheduleEndNotification_LIN6] */
  { /*   108 */ 116u,          108u, BswM_Rule_CC_Rule_LIN7_ScheduleTableEndNotification           },  /* [R_CC_Rule_LIN7_ScheduleTableEndNotification, MRP_LinScheduleEndNotification_LIN7] */
  { /*   109 */ 123u,          109u, BswM_Rule_CC_Rule_LIN8_ScheduleTableEndNotification           },  /* [R_CC_Rule_LIN8_ScheduleTableEndNotification, MRP_LinScheduleEndNotification_LIN8] */
  { /*   110 */ 107u,          110u, BswM_Rule_CC_Rule_LIN6_SchTableStartInd_MSTable0              },  /* [R_CC_Rule_LIN6_SchTableStartInd_MSTable0, COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0]] */
  { /*   111 */ 106u,          111u, BswM_Rule_CC_Rule_LIN6_SchTableStartInd_MSTable               },  /* [R_CC_Rule_LIN6_SchTableStartInd_MSTable, COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0]] */
  { /*   112 */ 108u,          112u, BswM_Rule_CC_Rule_LIN6_SchTableStartInd_Table0                },  /* [R_CC_Rule_LIN6_SchTableStartInd_Table0, COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0]] */
  { /*   113 */ 113u,          113u, BswM_Rule_CC_Rule_LIN7_SchTableStartInd_MSTable               },  /* [R_CC_Rule_LIN7_SchTableStartInd_MSTable, COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0]] */
  { /*   114 */ 120u,          114u, BswM_Rule_CC_Rule_LIN8_SchTableStartInd_MSTable               },  /* [R_CC_Rule_LIN8_SchTableStartInd_MSTable, COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp]] */
  { /*   115 */ 114u,          115u, BswM_Rule_CC_Rule_LIN7_SchTableStartInd_MSTable0              },  /* [R_CC_Rule_LIN7_SchTableStartInd_MSTable0, COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0]] */
  { /*   116 */ 121u,          116u, BswM_Rule_CC_Rule_LIN8_SchTableStartInd_MSTable0              },  /* [R_CC_Rule_LIN8_SchTableStartInd_MSTable0, COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp]] */
  { /*   117 */ 115u,          117u, BswM_Rule_CC_Rule_LIN7_SchTableStartInd_Table0                },  /* [R_CC_Rule_LIN7_SchTableStartInd_Table0, COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0]] */
  { /*   118 */ 122u,          118u, BswM_Rule_CC_Rule_LIN8_SchTableStartInd_Table0                },  /* [R_CC_Rule_LIN8_SchTableStartInd_Table0, COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp]] */
  { /*   119 */ 112u,          119u, BswM_Rule_CC_Rule_LIN6_Schedule_To_Table0                     },  /* [R_CC_Rule_LIN6_Schedule_To_Table0, MRP_LIN6_ScheduleTableRequestMode] */
  { /*   120 */ 119u,          120u, BswM_Rule_CC_Rule_LIN7_Schedule_To_Table0                     },  /* [R_CC_Rule_LIN7_Schedule_To_Table0, MRP_LIN7_ScheduleTableRequestMode] */
  { /*   121 */ 126u,          121u, BswM_Rule_CC_Rule_LIN8_Schedule_To_Table0                     },  /* [R_CC_Rule_LIN8_Schedule_To_Table0, MRP_LIN8_ScheduleTableRequestMode] */
  { /*   122 */ 110u,          122u, BswM_Rule_CC_Rule_LIN6_Schedule_To_MSTable                    },  /* [R_CC_Rule_LIN6_Schedule_To_MSTable, MRP_LIN6_ScheduleTableRequestMode] */
  { /*   123 */ 117u,          123u, BswM_Rule_CC_Rule_LIN7_Schedule_To_MSTable                    },  /* [R_CC_Rule_LIN7_Schedule_To_MSTable, MRP_LIN7_ScheduleTableRequestMode] */
  { /*   124 */ 124u,          124u, BswM_Rule_CC_Rule_LIN8_Schedule_To_MSTable                    },  /* [R_CC_Rule_LIN8_Schedule_To_MSTable, MRP_LIN8_ScheduleTableRequestMode] */
  { /*   125 */ 111u,          125u, BswM_Rule_CC_Rule_LIN6_Schedule_To_MSTable0                   },  /* [R_CC_Rule_LIN6_Schedule_To_MSTable0, MRP_LIN6_ScheduleTableRequestMode] */
  { /*   126 */ 118u,          126u, BswM_Rule_CC_Rule_LIN7_Schedule_To_MSTable0                   },  /* [R_CC_Rule_LIN7_Schedule_To_MSTable0, MRP_LIN7_ScheduleTableRequestMode] */
  { /*   127 */ 125u,          127u, BswM_Rule_CC_Rule_LIN8_Schedule_To_MSTable0                   },  /* [R_CC_Rule_LIN8_Schedule_To_MSTable0, MRP_LIN8_ScheduleTableRequestMode] */
  { /*   128 */  31u,          128u, BswM_Rule_CC_DCMResetProcess_Started                          },  /* [R_CC_DCMResetProcess_Started, MRP_DCMResetProcess] */
  { /*   129 */  30u,          129u, BswM_Rule_CC_DCMResetProcess_InProgress                       },  /* [R_CC_DCMResetProcess_InProgress, MRP_DCMResetProcess, MRP_ESH_NvMIndication, MRP_MemIfStatus, MRP_ESH_NvM_WriteAllTimer] */
  { /*   130 */  37u,          130u, BswM_Rule_CC_Rule_DcmEcuReset_JumpToBTL                       },  /* [R_CC_Rule_DcmEcuReset_JumpToBTL, MRP_ESH_State, MRP_ESH_ModeNotification, MRP_SwcModeNotification_DcmEcuReset] */
  { /*   131 */ 127u,          131u, BswM_Rule_CC_Rule_NvmWriteAll_Request                         },  /* [R_CC_Rule_NvmWriteAll_Request, MRP_SwcModeRequest_NvmWriteAllRequest, MRP_ESH_State, MRP_ESH_ModeNotification, MRP_ESH_NvM_WriteAllTimer] */
  { /*   132 */  32u,          132u, BswM_Rule_CC_Rule_BB1_BusOff_Indication                       },  /* [R_CC_Rule_BB1_BusOff_Indication, MRP_CanSMIndication_BB1] */
  { /*   133 */  33u,          133u, BswM_Rule_CC_Rule_BB2_BusOff_Indication                       },  /* [R_CC_Rule_BB2_BusOff_Indication, MRP_CanSMIndication_BB2] */
  { /*   134 */  34u,          134u, BswM_Rule_CC_Rule_CAN6_BusOff_Indication                      },  /* [R_CC_Rule_CAN6_BusOff_Indication, MRP_CanSMIndication_CAN6] */
  { /*   135 */  35u,          135u, BswM_Rule_CC_Rule_CabSubnet_BusOff_Indication                 },  /* [R_CC_Rule_CabSubnet_BusOff_Indication, MRP_CanSMIndication_CabSubnet] */
  { /*   136 */  39u,          136u, BswM_Rule_CC_Rule_FMSNet_BusOff_Indication                    },  /* [R_CC_Rule_FMSNet_BusOff_Indication, MRP_CanSMIndication_FMSNet] */
  { /*   137 */ 128u,          137u, BswM_Rule_CC_Rule_SecuritySubnet_BusOff_Indication            },  /* [R_CC_Rule_SecuritySubnet_BusOff_Indication, MRP_CanSMIndication_SecuritySubnet] */
  { /*   138 */   2u,          138u, BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX      },  /* [R_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX, MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae, MRP_CC_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289, MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae] */
  { /*   139 */   0u,          139u, BswM_Rule_CC_Rule_PvtReportCtrl                               }   /* [R_CC_Rule_PvtReportCtrl, MRP_SwcModeRequest_PvtReportCtrl, MRP_CC_CanSMIndication_CN_Backbone2_78967e2c, MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_RulesInd
**********************************************************************************************************************/
/** 
  \var    BswM_RulesInd
  \brief  the indexes of the 1:1 sorted relation pointing to BswM_Rules
*/ 
#define BSWM_START_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_RulesIndType, BSWM_CONST) BswM_RulesInd[151] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     RulesInd      Referable Keys */
  /*     0 */       77u,  /* [MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae] */
  /*     1 */       79u,  /* [MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae] */
  /*     2 */      138u,  /* [MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae] */
  /*     3 */       65u,  /* [MRP_CC_CanSMIndication_CN_Backbone2_78967e2c] */
  /*     4 */       70u,  /* [MRP_CC_CanSMIndication_CN_Backbone2_78967e2c] */
  /*     5 */       95u,  /* [MRP_CC_CanSMIndication_CN_Backbone2_78967e2c] */
  /*     6 */      139u,  /* [MRP_CC_CanSMIndication_CN_Backbone2_78967e2c] */
  /*     7 */      104u,  /* [MRP_CC_CanSMIndication_CN_CAN6_b040c073] */
  /*     8 */      105u,  /* [MRP_CC_CanSMIndication_CN_CAN6_b040c073] */
  /*     9 */      106u,  /* [MRP_CC_CanSMIndication_CN_CAN6_b040c073] */
  /*    10 */       63u,  /* [MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1] */
  /*    11 */       72u,  /* [MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1] */
  /*    12 */       94u,  /* [MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1] */
  /*    13 */       69u,  /* [MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5] */
  /*    14 */       74u,  /* [MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5] */
  /*    15 */       78u,  /* [MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5] */
  /*    16 */       81u,  /* [MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5] */
  /*    17 */       67u,  /* [MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54] */
  /*    18 */       68u,  /* [MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54] */
  /*    19 */       93u,  /* [MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54] */
  /*    20 */       77u,  /* [MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae] */
  /*    21 */       79u,  /* [MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae] */
  /*    22 */      138u,  /* [MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae] */
  /*    23 */       65u,  /* [MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
  /*    24 */       70u,  /* [MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
  /*    25 */       95u,  /* [MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
  /*    26 */      139u,  /* [MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
  /*    27 */      104u,  /* [MRP_CC_DcmComIndication_CN_CAN6_b040c073] */
  /*    28 */      105u,  /* [MRP_CC_DcmComIndication_CN_CAN6_b040c073] */
  /*    29 */      106u,  /* [MRP_CC_DcmComIndication_CN_CAN6_b040c073] */
  /*    30 */       63u,  /* [MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1] */
  /*    31 */       72u,  /* [MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1] */
  /*    32 */       94u,  /* [MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1] */
  /*    33 */       69u,  /* [MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  /*    34 */       74u,  /* [MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  /*    35 */       78u,  /* [MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  /*    36 */       81u,  /* [MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  /*    37 */       67u,  /* [MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54] */
  /*    38 */       68u,  /* [MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54] */
  /*    39 */       93u,  /* [MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54] */
  /*    40 */       29u,  /* [MRP_ESH_State] */
  /*    41 */       82u,  /* [MRP_ESH_State] */
  /*    42 */       84u,  /* [MRP_ESH_State] */
  /*    43 */       85u,  /* [MRP_ESH_State] */
  /*    44 */       86u,  /* [MRP_ESH_State] */
  /*    45 */       87u,  /* [MRP_ESH_State] */
  /*    46 */       88u,  /* [MRP_ESH_State] */
  /*    47 */       90u,  /* [MRP_ESH_State] */
  /*    48 */       91u,  /* [MRP_ESH_State] */
  /*    49 */      130u,  /* [MRP_ESH_State] */
  /* Index     RulesInd      Referable Keys */
  /*    50 */      131u,  /* [MRP_ESH_State] */
  /*    51 */       82u,  /* [MRP_ISSM_RunRequest] */
  /*    52 */       64u,  /* [MRP_CC_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289] */
  /*    53 */      138u,  /* [MRP_CC_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289] */
  /*    54 */       71u,  /* [MRP_CC_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289] */
  /*    55 */       81u,  /* [MRP_CC_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289] */
  /*    56 */        0u,  /* [COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  /*    57 */        5u,  /* [COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  /*    58 */        6u,  /* [COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  /*    59 */        7u,  /* [COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  /*    60 */        8u,  /* [COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  /*    61 */       11u,  /* [COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  /*    62 */        1u,  /* [COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  /*    63 */        9u,  /* [COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  /*    64 */       10u,  /* [COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  /*    65 */       12u,  /* [COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  /*    66 */        2u,  /* [COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  /*    67 */       13u,  /* [COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  /*    68 */       14u,  /* [COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  /*    69 */       15u,  /* [COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  /*    70 */       16u,  /* [COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  /*    71 */       17u,  /* [COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  /*    72 */        3u,  /* [COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  /*    73 */       18u,  /* [COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  /*    74 */       19u,  /* [COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  /*    75 */       20u,  /* [COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  /*    76 */       21u,  /* [COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  /*    77 */       22u,  /* [COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  /*    78 */        4u,  /* [COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  /*    79 */       23u,  /* [COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  /*    80 */       24u,  /* [COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  /*    81 */       25u,  /* [COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  /*    82 */       26u,  /* [COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  /*    83 */       27u,  /* [COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  /*    84 */      110u,  /* [COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0]] */
  /*    85 */      111u,  /* [COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0]] */
  /*    86 */      112u,  /* [COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0]] */
  /*    87 */      113u,  /* [COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0]] */
  /*    88 */      115u,  /* [COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0]] */
  /*    89 */      117u,  /* [COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0]] */
  /*    90 */      114u,  /* [COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp]] */
  /*    91 */      116u,  /* [COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp]] */
  /*    92 */      118u,  /* [COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp]] */
  /*    93 */       96u,  /* [MRP_LinScheduleEndNotification_LIN1] */
  /*    94 */       97u,  /* [MRP_LinScheduleEndNotification_LIN2] */
  /*    95 */       98u,  /* [MRP_LinScheduleEndNotification_LIN3] */
  /*    96 */       99u,  /* [MRP_LinScheduleEndNotification_LIN4] */
  /*    97 */      100u,  /* [MRP_LinScheduleEndNotification_LIN5] */
  /*    98 */      107u,  /* [MRP_LinScheduleEndNotification_LIN6] */
  /*    99 */      108u,  /* [MRP_LinScheduleEndNotification_LIN7] */
  /* Index     RulesInd      Referable Keys */
  /*   100 */      109u,  /* [MRP_LinScheduleEndNotification_LIN8] */
  /*   101 */       80u,  /* [MRP_CC_LinSMIndication_CN_LIN00_2cd9a7df] */
  /*   102 */       75u,  /* [MRP_CC_LinSMIndication_CN_LIN01_5bde9749] */
  /*   103 */       66u,  /* [MRP_CC_LinSMIndication_CN_LIN02_c2d7c6f3] */
  /*   104 */       73u,  /* [MRP_CC_LinSMIndication_CN_LIN03_b5d0f665] */
  /*   105 */       76u,  /* [MRP_CC_LinSMIndication_CN_LIN04_2bb463c6] */
  /*   106 */      102u,  /* [MRP_CC_LinSMIndication_CN_LIN05_5cb35350] */
  /*   107 */      101u,  /* [MRP_CC_LinSMIndication_CN_LIN06_c5ba02ea] */
  /*   108 */      103u,  /* [MRP_CC_LinSMIndication_CN_LIN07_b2bd327c] */
  /*   109 */       37u,  /* [MRP_LIN1_ScheduleTableRequestMode] */
  /*   110 */       38u,  /* [MRP_LIN1_ScheduleTableRequestMode] */
  /*   111 */       39u,  /* [MRP_LIN1_ScheduleTableRequestMode] */
  /*   112 */       40u,  /* [MRP_LIN1_ScheduleTableRequestMode] */
  /*   113 */       41u,  /* [MRP_LIN1_ScheduleTableRequestMode] */
  /*   114 */       42u,  /* [MRP_LIN1_ScheduleTableRequestMode] */
  /*   115 */       43u,  /* [MRP_LIN1_ScheduleTableRequestMode] */
  /*   116 */       44u,  /* [MRP_LIN2_ScheduleTableRequestMode] */
  /*   117 */       45u,  /* [MRP_LIN2_ScheduleTableRequestMode] */
  /*   118 */       46u,  /* [MRP_LIN2_ScheduleTableRequestMode] */
  /*   119 */       47u,  /* [MRP_LIN2_ScheduleTableRequestMode] */
  /*   120 */       48u,  /* [MRP_LIN2_ScheduleTableRequestMode] */
  /*   121 */       30u,  /* [MRP_LIN3_ScheduleTableRequestMode] */
  /*   122 */       31u,  /* [MRP_LIN3_ScheduleTableRequestMode] */
  /*   123 */       32u,  /* [MRP_LIN3_ScheduleTableRequestMode] */
  /*   124 */       33u,  /* [MRP_LIN3_ScheduleTableRequestMode] */
  /*   125 */       34u,  /* [MRP_LIN3_ScheduleTableRequestMode] */
  /*   126 */       35u,  /* [MRP_LIN3_ScheduleTableRequestMode] */
  /*   127 */       36u,  /* [MRP_LIN3_ScheduleTableRequestMode] */
  /*   128 */       49u,  /* [MRP_LIN4_ScheduleTableRequestMode] */
  /*   129 */       50u,  /* [MRP_LIN4_ScheduleTableRequestMode] */
  /*   130 */       51u,  /* [MRP_LIN4_ScheduleTableRequestMode] */
  /*   131 */       52u,  /* [MRP_LIN4_ScheduleTableRequestMode] */
  /*   132 */       53u,  /* [MRP_LIN4_ScheduleTableRequestMode] */
  /*   133 */       54u,  /* [MRP_LIN4_ScheduleTableRequestMode] */
  /*   134 */       55u,  /* [MRP_LIN4_ScheduleTableRequestMode] */
  /*   135 */       56u,  /* [MRP_LIN5_ScheduleTableRequestMode] */
  /*   136 */       57u,  /* [MRP_LIN5_ScheduleTableRequestMode] */
  /*   137 */       58u,  /* [MRP_LIN5_ScheduleTableRequestMode] */
  /*   138 */       59u,  /* [MRP_LIN5_ScheduleTableRequestMode] */
  /*   139 */       60u,  /* [MRP_LIN5_ScheduleTableRequestMode] */
  /*   140 */       61u,  /* [MRP_LIN5_ScheduleTableRequestMode] */
  /*   141 */       62u,  /* [MRP_LIN5_ScheduleTableRequestMode] */
  /*   142 */      119u,  /* [MRP_LIN6_ScheduleTableRequestMode] */
  /*   143 */      122u,  /* [MRP_LIN6_ScheduleTableRequestMode] */
  /*   144 */      125u,  /* [MRP_LIN6_ScheduleTableRequestMode] */
  /*   145 */      120u,  /* [MRP_LIN7_ScheduleTableRequestMode] */
  /*   146 */      123u,  /* [MRP_LIN7_ScheduleTableRequestMode] */
  /*   147 */      126u,  /* [MRP_LIN7_ScheduleTableRequestMode] */
  /*   148 */      121u,  /* [MRP_LIN8_ScheduleTableRequestMode] */
  /*   149 */      124u,  /* [MRP_LIN8_ScheduleTableRequestMode] */
  /* Index     RulesInd      Referable Keys */
  /*   150 */      127u   /* [MRP_LIN8_ScheduleTableRequestMode] */
};
#define BSWM_STOP_SEC_CONST_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_SwcModeRequestUpdateFct
**********************************************************************************************************************/
#define BSWM_START_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
CONST(BswM_PartitionFunctionType, BSWM_CONST) BswM_SwcModeRequestUpdateFct[1] = {  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index     SwcModeRequestUpdateFct                            */
  /*     0 */ BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION 
};
#define BSWM_STOP_SEC_CONST_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ActionListQueue
**********************************************************************************************************************/
/** 
  \var    BswM_ActionListQueue
  \brief  Variable to store action lists which shall be executed.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_ActionListQueueUType, BSWM_VAR_NOINIT) BswM_ActionListQueue;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [AL_CC_AL_LIN1_SchTableStartInd_MSTable1] */
  /*     1 */  /* [AL_CC_AL_LIN2_SchTableStartInd_MSTable0] */
  /*     2 */  /* [AL_CC_AL_LIN3_SchTableStartInd_MSTable] */
  /*     3 */  /* [AL_CC_AL_LIN4_SchTableStartInd_MSTable1] */
  /*     4 */  /* [AL_CC_AL_LIN5_SchTableStartInd_MSTable2] */
  /*     5 */  /* [AL_CC_AL_LIN1_SchTableStartInd_MSTable2] */
  /*     6 */  /* [AL_CC_AL_LIN1_SchTableStartInd_Table1] */
  /*     7 */  /* [AL_CC_AL_LIN1_SchTableStartInd_Table2] */
  /*     8 */  /* [AL_CC_AL_LIN1_SchTableStartInd_TableE] */
  /*     9 */  /* [AL_CC_AL_LIN2_SchTableStartInd_Table0] */
  /*    10 */  /* [AL_CC_AL_LIN2_SchTableStartInd_TableE] */
  /*    11 */  /* [AL_CC_AL_LIN1_SchTableStartInd_MSTable] */
  /*    12 */  /* [AL_CC_AL_LIN2_SchTableStartInd_MSTable] */
  /*    13 */  /* [AL_CC_AL_LIN3_SchTableStartInd_MSTable1] */
  /*    14 */  /* [AL_CC_AL_LIN3_SchTableStartInd_MSTable2] */
  /*    15 */  /* [AL_CC_AL_LIN3_SchTableStartInd_Table1] */
  /*    16 */  /* [AL_CC_AL_LIN3_SchTableStartInd_Table2] */
  /*    17 */  /* [AL_CC_AL_LIN3_SchTableStartInd_TableE] */
  /*    18 */  /* [AL_CC_AL_LIN4_SchTableStartInd_MSTable2] */
  /*    19 */  /* [AL_CC_AL_LIN4_SchTableStartInd_MSTable] */
  /*    20 */  /* [AL_CC_AL_LIN4_SchTableStartInd_Table1] */
  /*    21 */  /* [AL_CC_AL_LIN4_SchTableStartInd_Table2] */
  /*    22 */  /* [AL_CC_AL_LIN4_SchTableStartInd_TableE] */
  /*    23 */  /* [AL_CC_AL_LIN5_SchTableStartInd_MSTable1] */
  /*    24 */  /* [AL_CC_AL_LIN5_SchTableStartInd_MSTable] */
  /*    25 */  /* [AL_CC_AL_LIN5_SchTableStartInd_Table1] */
  /*    26 */  /* [AL_CC_AL_LIN5_SchTableStartInd_Table2] */
  /*    27 */  /* [AL_CC_AL_LIN5_SchTableStartInd_TableE] */
  /*    28 */  /* [AL_CC_AL_DcmEcuReset_Trigger] */
  /*    29 */  /* [AL_CC_AL_LIN5_ScheduleTable_to_Table2] */
  /*    30 */  /* [AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp] */
  /*    31 */  /* [AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp] */
  /*    32 */  /* [AL_CC_AL_LIN2_ScheduleTable_to_Table_E] */
  /*    33 */  /* [AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1] */
  /*    34 */  /* [AL_CC_AL_LIN3_ScheduleTable_to_Table1] */
  /*    35 */  /* [AL_CC_AL_LIN3_ScheduleTable_to_NULL] */
  /*    36 */  /* [AL_CC_AL_LIN3_ScheduleTable_to_Table2] */
  /*    37 */  /* [AL_CC_AL_LIN3_ScheduleTable_to_Table_E] */
  /*    38 */  /* [AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp] */
  /*    39 */  /* [AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1] */
  /*    40 */  /* [AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2] */
  /*    41 */  /* [AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp] */
  /*    42 */  /* [AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp] */
  /*    43 */  /* [AL_CC_AL_LIN1_ScheduleTable_to_Table2] */
  /*    44 */  /* [AL_CC_AL_LIN5_ScheduleTable_to_Table_E] */
  /*    45 */  /* [AL_CC_AL_LIN4_ScheduleTable_to_Table_E] */
  /*    46 */  /* [AL_CC_AL_LIN1_ScheduleTable_to_Table_E] */
  /*    47 */  /* [AL_CC_AL_LIN4_ScheduleTable_to_Table2] */
  /*    48 */  /* [AL_CC_AL_LIN5_ScheduleTable_to_Table1] */
  /*    49 */  /* [AL_CC_AL_LIN4_ScheduleTable_to_Table1] */
  /* Index        Referable Keys */
  /*    50 */  /* [AL_CC_AL_LIN2_ScheduleTable_to_Table0] */
  /*    51 */  /* [AL_CC_AL_LIN1_ScheduleTable_to_Table1] */
  /*    52 */  /* [AL_CC_AL_LIN5_ScheduleTable_to_NULL] */
  /*    53 */  /* [AL_CC_AL_LIN4_ScheduleTable_to_NULL] */
  /*    54 */  /* [AL_CC_AL_LIN2_ScheduleTable_to_NULL] */
  /*    55 */  /* [AL_CC_AL_LIN1_ScheduleTable_to_NULL] */
  /*    56 */  /* [AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2] */
  /*    57 */  /* [AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2] */
  /*    58 */  /* [AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2] */
  /*    59 */  /* [AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0] */
  /*    60 */  /* [AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1] */
  /*    61 */  /* [AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1] */
  /*    62 */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM] */
  /*    63 */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM] */
  /*    64 */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline] */
  /*    65 */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online] */
  /*    66 */  /* [AL_CC_AL_CN_Backbone2_78967e2c_Disable_DM] */
  /*    67 */  /* [AL_CC_AL_CN_Backbone2_78967e2c_Enable_DM] */
  /*    68 */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable] */
  /*    69 */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit] */
  /*    70 */  /* [AL_CC_AL_CN_LIN02_c2d7c6f3_Disable] */
  /*    71 */  /* [AL_CC_AL_CN_LIN02_c2d7c6f3_Enable] */
  /*    72 */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable] */
  /*    73 */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit] */
  /*    74 */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM] */
  /*    75 */  /* [AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM] */
  /*    76 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_TX_Disable] */
  /*    77 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit] */
  /*    78 */  /* [AL_CC_AL_CN_Backbone2_78967e2c_RX_Disable] */
  /*    79 */  /* [AL_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit] */
  /*    80 */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable] */
  /*    81 */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit] */
  /*    82 */  /* [AL_CC_AL_CN_Backbone2_78967e2c_TX_Disable] */
  /*    83 */  /* [AL_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit] */
  /*    84 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline] */
  /*    85 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online] */
  /*    86 */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable] */
  /*    87 */  /* [AL_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit] */
  /*    88 */  /* [AL_CC_AL_CN_LIN03_b5d0f665_Disable] */
  /*    89 */  /* [AL_CC_AL_CN_LIN03_b5d0f665_Enable] */
  /*    90 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_RX_Disable] */
  /*    91 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit] */
  /*    92 */  /* [AL_CC_AL_CN_LIN01_5bde9749_Disable] */
  /*    93 */  /* [AL_CC_AL_CN_LIN01_5bde9749_Enable] */
  /*    94 */  /* [AL_CC_AL_CN_LIN04_2bb463c6_Disable] */
  /*    95 */  /* [AL_CC_AL_CN_LIN04_2bb463c6_Enable] */
  /*    96 */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable] */
  /*    97 */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit] */
  /*    98 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_Disable_DM] */
  /*    99 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_Enable_DM] */
  /* Index        Referable Keys */
  /*   100 */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM] */
  /*   101 */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM] */
  /*   102 */  /* [AL_CC_AL_CN_LIN00_2cd9a7df_Disable] */
  /*   103 */  /* [AL_CC_AL_CN_LIN00_2cd9a7df_Enable] */
  /*   104 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable] */
  /*   105 */  /* [AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit] */
  /*   106 */  /* [AL_ESH_AL_ExitRun] */
  /*   107 */  /* [AL_ESH_AL_RunToPostRun] */
  /*   108 */  /* [AL_ESH_AL_WaitForNvMToShutdown] */
  /*   109 */  /* [AL_ESH_AL_WakeupToPrep] */
  /*   110 */  /* [AL_ESH_AL_WaitForNvMWakeup] */
  /*   111 */  /* [AL_ESH_AL_WakeupToRun] */
  /*   112 */  /* [AL_ESH_AL_InitToWakeup] */
  /*   113 */  /* [AL_ESH_AL_PostRunToPrepShutdown] */
  /*   114 */  /* [AL_ESH_AL_PostRunToRun] */
  /*   115 */  /* [AL_ESH_AL_ExitPostRun] */
  /*   116 */  /* [AL_ESH_AL_PrepShutdownToWaitForNvM] */
  /*   117 */  /* [AL_INIT_AL_Initialize] */
  /*   118 */  /* [AL_ESH_AL_DemInit] */
  /*   119 */  /* [AL_CC_AL_LIN1_ScheduleTableEndNotification] */
  /*   120 */  /* [AL_CC_AL_LIN2_ScheduleTableEndNotification] */
  /*   121 */  /* [AL_CC_AL_LIN3_ScheduleTableEndNotification] */
  /*   122 */  /* [AL_CC_AL_LIN4_ScheduleTableEndNotification] */
  /*   123 */  /* [AL_CC_AL_LIN5_ScheduleTableEndNotification] */
  /*   124 */  /* [AL_CC_AL_CN_LIN06_c5ba02ea_Disable] */
  /*   125 */  /* [AL_CC_AL_CN_LIN06_c5ba02ea_Enable] */
  /*   126 */  /* [AL_CC_AL_CN_LIN05_5cb35350_Disable] */
  /*   127 */  /* [AL_CC_AL_CN_LIN05_5cb35350_Enable] */
  /*   128 */  /* [AL_CC_AL_CN_LIN07_b2bd327c_Disable] */
  /*   129 */  /* [AL_CC_AL_CN_LIN07_b2bd327c_Enable] */
  /*   130 */  /* [AL_CC_AL_CN_CAN6_b040c073_RX_Disable] */
  /*   131 */  /* [AL_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit] */
  /*   132 */  /* [AL_CC_AL_CN_CAN6_b040c073_TX_Disable] */
  /*   133 */  /* [AL_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit] */
  /*   134 */  /* [AL_CC_AL_CN_CAN6_b040c073_Disable_DM] */
  /*   135 */  /* [AL_CC_AL_CN_CAN6_b040c073_Enable_DM] */
  /*   136 */  /* [AL_CC_AL_LIN6_ScheduleTableEndNotification] */
  /*   137 */  /* [AL_CC_AL_LIN7_ScheduleTableEndNotification] */
  /*   138 */  /* [AL_CC_AL_LIN8_ScheduleTableEndNotification] */
  /*   139 */  /* [AL_CC_AL_LIN6_SchTableStartInd_Table0] */
  /*   140 */  /* [AL_CC_AL_LIN7_SchTableStartInd_Table0] */
  /*   141 */  /* [AL_CC_AL_LIN8_SchTableStartInd_Table0] */
  /*   142 */  /* [AL_CC_AL_LIN6_SchTableStartInd_MSTable] */
  /*   143 */  /* [AL_CC_AL_LIN7_SchTableStartInd_MSTable] */
  /*   144 */  /* [AL_CC_AL_LIN8_SchTableStartInd_MSTable] */
  /*   145 */  /* [AL_CC_AL_LIN6_SchTableStartInd_MSTable0] */
  /*   146 */  /* [AL_CC_AL_LIN7_SchTableStartInd_MSTable0] */
  /*   147 */  /* [AL_CC_AL_LIN8_SchTableStartInd_MSTable0] */
  /*   148 */  /* [AL_CC_AL_LIN6_ScheduleTable_to_Table0] */
  /*   149 */  /* [AL_CC_AL_LIN7_ScheduleTable_to_Table0] */
  /* Index        Referable Keys */
  /*   150 */  /* [AL_CC_AL_LIN8_ScheduleTable_to_Table0] */
  /*   151 */  /* [AL_CC_AL_LIN6_ScheduleTable_to_MSTable0] */
  /*   152 */  /* [AL_CC_AL_LIN6_ScheduleTable_to_MSTable] */
  /*   153 */  /* [AL_CC_AL_LIN7_ScheduleTable_to_MSTable0] */
  /*   154 */  /* [AL_CC_AL_LIN7_ScheduleTable_to_MSTable] */
  /*   155 */  /* [AL_CC_AL_LIN8_ScheduleTable_to_MSTable0] */
  /*   156 */  /* [AL_CC_AL_LIN8_ScheduleTable_to_MSTable] */
  /*   157 */  /* [AL_CC_AL_DCMResetProcess_Started] */
  /*   158 */  /* [AL_CC_AL_DCMResetProcess_InProgress] */
  /*   159 */  /* [AL_CC_AL_RunToECUReset] */
  /*   160 */  /* [AL_CC_AL_DcmEcuReset_Execute] */
  /*   161 */  /* [AL_CC_AL_DcmEcuReset_JumpToBTL] */
  /*   162 */  /* [AL_CC_AL_DCMResetPostRun] */
  /*   163 */  /* [AL_CC_AL_PrepShutdown_NvmWriteAll] */
  /*   164 */  /* [AL_CC_True_AL_BB1_BusOff_Indication] */
  /*   165 */  /* [AL_CC_False_AL_BB1_BusOff_Indication] */
  /*   166 */  /* [AL_CC_True_AL_BB2_BusOff_Indication] */
  /*   167 */  /* [AL_CC_False_AL_CAN6_BusOff_Indication] */
  /*   168 */  /* [AL_CC_True_AL_CAN6_BusOff_Indication] */
  /*   169 */  /* [AL_CC_False_AL_BB2_BusOff_Indication] */
  /*   170 */  /* [AL_CC_False_AL_CabSubnet_BusOff_Indication] */
  /*   171 */  /* [AL_CC_False_AL_FMSNet_BusOff_Indication] */
  /*   172 */  /* [AL_CC_True_AL_CabSubnet_BusOff_Indication] */
  /*   173 */  /* [AL_CC_True_AL_FMSNet_BusOff_Indication] */
  /*   174 */  /* [AL_CC_False_AL_SecuritySubnet_BusOff_Indication] */
  /*   175 */  /* [AL_CC_True_AL_SecuritySubnet_BusOff_Indication] */
  /*   176 */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable] */
  /*   177 */  /* [AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit] */
  /*   178 */  /* [AL_AL_PvtReport_Enabled] */
  /*   179 */  /* [AL_AL_PvtReport_Disabled] */

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_CanSMChannelState
**********************************************************************************************************************/
/** 
  \var    BswM_CanSMChannelState
  \brief  Variable to store current mode of BswMCanSMIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(CanSM_BswMCurrentStateType, BSWM_VAR_NOINIT) BswM_CanSMChannelState[6];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [CANSM_CHANNEL_0, MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae, MRP_CanSMIndication_BB1] */
  /*     1 */  /* [CANSM_CHANNEL_1, MRP_CC_CanSMIndication_CN_Backbone2_78967e2c, MRP_CanSMIndication_BB2] */
  /*     2 */  /* [CANSM_CHANNEL_2, MRP_CC_CanSMIndication_CN_CAN6_b040c073, MRP_CanSMIndication_CAN6] */
  /*     3 */  /* [CANSM_CHANNEL_3, MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1, MRP_CanSMIndication_CabSubnet] */
  /*     4 */  /* [CANSM_CHANNEL_4, MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5, MRP_CanSMIndication_FMSNet] */
  /*     5 */  /* [CANSM_CHANNEL_5, MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54, MRP_CanSMIndication_SecuritySubnet] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ComMChannelState
**********************************************************************************************************************/
/** 
  \var    BswM_ComMChannelState
  \brief  Variable to store current mode of BswMComMIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(ComM_ModeType, BSWM_VAR_NOINIT) BswM_ComMChannelState[14];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [COMM_CHANNEL_0, MRP_ESH_ComMIndication_CN_Backbone1J1939_0b1f4bae] */
  /*     1 */  /* [COMM_CHANNEL_1, MRP_ESH_ComMIndication_CN_Backbone2_78967e2c] */
  /*     2 */  /* [COMM_CHANNEL_2, MRP_ESH_ComMIndication_CN_CAN6_b040c073] */
  /*     3 */  /* [COMM_CHANNEL_3, MRP_ESH_ComMIndication_CN_CabSubnet_9ea693f1] */
  /*     4 */  /* [COMM_CHANNEL_4, MRP_ESH_ComMIndication_CN_FMSNet_fce1aae5] */
  /*     5 */  /* [COMM_CHANNEL_5, MRP_ESH_ComMIndication_CN_LIN00_2cd9a7df] */
  /*     6 */  /* [COMM_CHANNEL_6, MRP_ESH_ComMIndication_CN_LIN01_5bde9749] */
  /*     7 */  /* [COMM_CHANNEL_7, MRP_ESH_ComMIndication_CN_LIN02_c2d7c6f3] */
  /*     8 */  /* [COMM_CHANNEL_8, MRP_ESH_ComMIndication_CN_LIN03_b5d0f665] */
  /*     9 */  /* [COMM_CHANNEL_9, MRP_ESH_ComMIndication_CN_LIN04_2bb463c6] */
  /*    10 */  /* [COMM_CHANNEL_10, MRP_ESH_ComMIndication_CN_LIN05_5cb35350] */
  /*    11 */  /* [COMM_CHANNEL_11, MRP_ESH_ComMIndication_CN_LIN06_c5ba02ea] */
  /*    12 */  /* [COMM_CHANNEL_12, MRP_ESH_ComMIndication_CN_LIN07_b2bd327c] */
  /*    13 */  /* [COMM_CHANNEL_13, MRP_ESH_ComMIndication_CN_SecuritySubnet_e7a0ee54] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_DcmComState
**********************************************************************************************************************/
/** 
  \var    BswM_DcmComState
  \brief  Variable to store current mode of BswMDcmComModeRequest mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Dcm_CommunicationModeType, BSWM_VAR_NOINIT) BswM_DcmComState[6];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [DCM_COM_0, MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae] */
  /*     1 */  /* [DCM_COM_1, MRP_CC_DcmComIndication_CN_Backbone2_78967e2c] */
  /*     2 */  /* [DCM_COM_2, MRP_CC_DcmComIndication_CN_CAN6_b040c073] */
  /*     3 */  /* [DCM_COM_3, MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1] */
  /*     4 */  /* [DCM_COM_4, MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5] */
  /*     5 */  /* [DCM_COM_5, MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ForcedActionListPriority
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_ForcedActionListPriorityType, BSWM_VAR_NOINIT) BswM_ForcedActionListPriority;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_GenericState
**********************************************************************************************************************/
/** 
  \var    BswM_GenericState
  \brief  Variable to store current mode of BswMGenericRequest mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_ModeType, BSWM_VAR_NOINIT) BswM_GenericState[5];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [GENERIC_0, MRP_DCMResetProcess] */
  /*     1 */  /* [GENERIC_1, MRP_ESH_ComMPendingRequests] */
  /*     2 */  /* [GENERIC_2, MRP_ESH_DemInitStatus] */
  /*     3 */  /* [GENERIC_3, MRP_ESH_State] */
  /*     4 */  /* [GENERIC_4, MRP_ISSM_RunRequest] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_Initialized
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_InitializedType, BSWM_VAR_NOINIT) BswM_Initialized;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_J1939NmState
**********************************************************************************************************************/
/** 
  \var    BswM_J1939NmState
  \brief  Variable to store current mode of BswMJ1939NmIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(Nm_StateType, BSWM_VAR_NOINIT) BswM_J1939NmState[2];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [J1939_NM_0, MRP_CC_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289] */
  /*     1 */  /* [J1939_NM_1, MRP_CC_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinSMState
**********************************************************************************************************************/
/** 
  \var    BswM_LinSMState
  \brief  Variable to store current mode of BswMLinSMIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinSM_ModeType, BSWM_VAR_NOINIT) BswM_LinSMState[8];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [LINSM_CHANNEL_0, MRP_CC_LinSMIndication_CN_LIN00_2cd9a7df] */
  /*     1 */  /* [LINSM_CHANNEL_1, MRP_CC_LinSMIndication_CN_LIN01_5bde9749] */
  /*     2 */  /* [LINSM_CHANNEL_2, MRP_CC_LinSMIndication_CN_LIN02_c2d7c6f3] */
  /*     3 */  /* [LINSM_CHANNEL_3, MRP_CC_LinSMIndication_CN_LIN03_b5d0f665] */
  /*     4 */  /* [LINSM_CHANNEL_4, MRP_CC_LinSMIndication_CN_LIN04_2bb463c6] */
  /*     5 */  /* [LINSM_CHANNEL_5, MRP_CC_LinSMIndication_CN_LIN05_5cb35350] */
  /*     6 */  /* [LINSM_CHANNEL_6, MRP_CC_LinSMIndication_CN_LIN06_c5ba02ea] */
  /*     7 */  /* [LINSM_CHANNEL_7, MRP_CC_LinSMIndication_CN_LIN07_b2bd327c] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinScheduleEndState
**********************************************************************************************************************/
/** 
  \var    BswM_LinScheduleEndState
  \brief  Variable to store current mode of BswMLinScheduleEndNotification mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinIf_SchHandleType, BSWM_VAR_NOINIT) BswM_LinScheduleEndState[8];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [LIN_SCHEDULEEND_0, MRP_LinScheduleEndNotification_LIN1] */
  /*     1 */  /* [LIN_SCHEDULEEND_1, MRP_LinScheduleEndNotification_LIN2] */
  /*     2 */  /* [LIN_SCHEDULEEND_2, MRP_LinScheduleEndNotification_LIN3] */
  /*     3 */  /* [LIN_SCHEDULEEND_3, MRP_LinScheduleEndNotification_LIN4] */
  /*     4 */  /* [LIN_SCHEDULEEND_4, MRP_LinScheduleEndNotification_LIN5] */
  /*     5 */  /* [LIN_SCHEDULEEND_5, MRP_LinScheduleEndNotification_LIN6] */
  /*     6 */  /* [LIN_SCHEDULEEND_6, MRP_LinScheduleEndNotification_LIN7] */
  /*     7 */  /* [LIN_SCHEDULEEND_7, MRP_LinScheduleEndNotification_LIN8] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_LinScheduleState
**********************************************************************************************************************/
/** 
  \var    BswM_LinScheduleState
  \brief  Variable to store current mode of BswMLinScheduleIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(LinIf_SchHandleType, BSWM_VAR_NOINIT) BswM_LinScheduleState[8];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [LIN_SCHEDULE_0, COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1]] */
  /*     1 */  /* [LIN_SCHEDULE_1, COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0]] */
  /*     2 */  /* [LIN_SCHEDULE_2, COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E]] */
  /*     3 */  /* [LIN_SCHEDULE_3, COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E]] */
  /*     4 */  /* [LIN_SCHEDULE_4, COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E]] */
  /*     5 */  /* [LIN_SCHEDULE_5, COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0]] */
  /*     6 */  /* [LIN_SCHEDULE_6, COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0]] */
  /*     7 */  /* [LIN_SCHEDULE_7, COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp]] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_ModeRequestQueue
**********************************************************************************************************************/
/** 
  \var    BswM_ModeRequestQueue
  \brief  Variable to store an immediate mode request which must be queued because of a current active arbitration.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_ModeRequestQueueType, BSWM_VAR_NOINIT) BswM_ModeRequestQueue[49];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [MRP_CC_CanSMIndication_CN_Backbone1J1939_0b1f4bae, CANSM_CHANNEL_0] */
  /*     1 */  /* [MRP_CC_CanSMIndication_CN_Backbone2_78967e2c, CANSM_CHANNEL_1] */
  /*     2 */  /* [MRP_CC_CanSMIndication_CN_CAN6_b040c073, CANSM_CHANNEL_2] */
  /*     3 */  /* [MRP_CC_CanSMIndication_CN_CabSubnet_9ea693f1, CANSM_CHANNEL_3] */
  /*     4 */  /* [MRP_CC_CanSMIndication_CN_FMSNet_fce1aae5, CANSM_CHANNEL_4] */
  /*     5 */  /* [MRP_CC_CanSMIndication_CN_SecuritySubnet_e7a0ee54, CANSM_CHANNEL_5] */
  /*     6 */  /* [MRP_CC_DcmComIndication_CN_Backbone1J1939_0b1f4bae, DCM_COM_0] */
  /*     7 */  /* [MRP_CC_DcmComIndication_CN_Backbone2_78967e2c, DCM_COM_1] */
  /*     8 */  /* [MRP_CC_DcmComIndication_CN_CAN6_b040c073, DCM_COM_2] */
  /*     9 */  /* [MRP_CC_DcmComIndication_CN_CabSubnet_9ea693f1, DCM_COM_3] */
  /*    10 */  /* [MRP_CC_DcmComIndication_CN_FMSNet_fce1aae5, DCM_COM_4] */
  /*    11 */  /* [MRP_CC_DcmComIndication_CN_SecuritySubnet_e7a0ee54, DCM_COM_5] */
  /*    12 */  /* [MRP_ESH_DemInitStatus, GENERIC_2] */
  /*    13 */  /* [MRP_ESH_State, GENERIC_3] */
  /*    14 */  /* [MRP_ISSM_RunRequest, GENERIC_4] */
  /*    15 */  /* [MRP_CC_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289, J1939_NM_0] */
  /*    16 */  /* [MRP_CC_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289, J1939_NM_1] */
  /*    17 */  /* [COMBINATION[MRP_Lin1SchInd_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp, MRP_Lin1SchInd_MasterReq_SlaveResp_Table2, MRP_Lin1SchInd_MasterReq_SlaveResp_Table1, MRP_Lin1SchInd_Table_E, MRP_Lin1SchInd_Table1], LIN_SCHEDULE_0] */
  /*    18 */  /* [COMBINATION[MRP_Lin2SchInd_MasterReq_SlaveResp, MRP_Lin2SchInd_Table_E, MRP_Lin2SchInd_Table0, MRP_Lin2SchInd_MasterReq_SlaveResp_Table0], LIN_SCHEDULE_1] */
  /*    19 */  /* [COMBINATION[MRP_Lin3SchInd_MasterReq_SlaveResp_Table2, MRP_Lin3SchInd_Table1, MRP_Lin3SchInd_Table2, MRP_Lin3SchInd_MasterReq_SlaveResp, MRP_Lin3SchInd_MasterReq_SlaveResp_Table1, MRP_Lin3SchInd_Table_E], LIN_SCHEDULE_2] */
  /*    20 */  /* [COMBINATION[MRP_Lin4SchInd_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp_Table2, MRP_Lin4SchInd_MasterReq_SlaveResp, MRP_Lin4SchInd_MasterReq_SlaveResp_Table1, MRP_Lin4SchInd_Table1, MRP_Lin4SchInd_Table_E], LIN_SCHEDULE_3] */
  /*    21 */  /* [COMBINATION[MRP_Lin5SchInd_MasterReq_SlaveResp_Table1, MRP_Lin5SchInd_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp_Table2, MRP_Lin5SchInd_MasterReq_SlaveResp, MRP_Lin5SchInd_Table1, MRP_Lin5SchInd_Table_E], LIN_SCHEDULE_4] */
  /*    22 */  /* [COMBINATION[MRP_Lin6SchInd_MasterReq_SlaveResp_Table0, MRP_Lin6SchInd_MasterReq_SlaveResp, MRP_Lin6SchInd_Table0], LIN_SCHEDULE_5] */
  /*    23 */  /* [COMBINATION[MRP_Lin7SchInd_Table0, MRP_Lin7SchInd_MasterReq_SlaveResp, MRP_Lin7SchInd_MasterReq_SlaveResp_Table0], LIN_SCHEDULE_6] */
  /*    24 */  /* [COMBINATION[MRP_Lin8SchInd_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp_Table0, MRP_Lin8SchInd_MasterReq_SlaveResp], LIN_SCHEDULE_7] */
  /*    25 */  /* [MRP_LinScheduleEndNotification_LIN1, LIN_SCHEDULEEND_0] */
  /*    26 */  /* [MRP_LinScheduleEndNotification_LIN2, LIN_SCHEDULEEND_1] */
  /*    27 */  /* [MRP_LinScheduleEndNotification_LIN3, LIN_SCHEDULEEND_2] */
  /*    28 */  /* [MRP_LinScheduleEndNotification_LIN4, LIN_SCHEDULEEND_3] */
  /*    29 */  /* [MRP_LinScheduleEndNotification_LIN5, LIN_SCHEDULEEND_4] */
  /*    30 */  /* [MRP_LinScheduleEndNotification_LIN6, LIN_SCHEDULEEND_5] */
  /*    31 */  /* [MRP_LinScheduleEndNotification_LIN7, LIN_SCHEDULEEND_6] */
  /*    32 */  /* [MRP_LinScheduleEndNotification_LIN8, LIN_SCHEDULEEND_7] */
  /*    33 */  /* [MRP_CC_LinSMIndication_CN_LIN00_2cd9a7df, LINSM_CHANNEL_0] */
  /*    34 */  /* [MRP_CC_LinSMIndication_CN_LIN01_5bde9749, LINSM_CHANNEL_1] */
  /*    35 */  /* [MRP_CC_LinSMIndication_CN_LIN02_c2d7c6f3, LINSM_CHANNEL_2] */
  /*    36 */  /* [MRP_CC_LinSMIndication_CN_LIN03_b5d0f665, LINSM_CHANNEL_3] */
  /*    37 */  /* [MRP_CC_LinSMIndication_CN_LIN04_2bb463c6, LINSM_CHANNEL_4] */
  /*    38 */  /* [MRP_CC_LinSMIndication_CN_LIN05_5cb35350, LINSM_CHANNEL_5] */
  /*    39 */  /* [MRP_CC_LinSMIndication_CN_LIN06_c5ba02ea, LINSM_CHANNEL_6] */
  /*    40 */  /* [MRP_CC_LinSMIndication_CN_LIN07_b2bd327c, LINSM_CHANNEL_7] */
  /*    41 */  /* [MRP_LIN1_ScheduleTableRequestMode, SWC_REQUEST_0] */
  /*    42 */  /* [MRP_LIN2_ScheduleTableRequestMode, SWC_REQUEST_1] */
  /*    43 */  /* [MRP_LIN3_ScheduleTableRequestMode, SWC_REQUEST_2] */
  /*    44 */  /* [MRP_LIN4_ScheduleTableRequestMode, SWC_REQUEST_3] */
  /*    45 */  /* [MRP_LIN5_ScheduleTableRequestMode, SWC_REQUEST_4] */
  /*    46 */  /* [MRP_LIN6_ScheduleTableRequestMode, SWC_REQUEST_5] */
  /*    47 */  /* [MRP_LIN7_ScheduleTableRequestMode, SWC_REQUEST_6] */
  /*    48 */  /* [MRP_LIN8_ScheduleTableRequestMode, SWC_REQUEST_7] */

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_NvMJobState
**********************************************************************************************************************/
/** 
  \var    BswM_NvMJobState
  \brief  Variable to store current mode of BswMNvMJobModeIndication mode request ports.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(NvM_RequestResultType, BSWM_VAR_NOINIT) BswM_NvMJobState[1];  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [NVM_JOB_0, MRP_ESH_NvMIndication] */

#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_QueueSemaphore
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_QueueSemaphoreType, BSWM_VAR_NOINIT) BswM_QueueSemaphore;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_QueueWritten
**********************************************************************************************************************/
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_QueueWrittenType, BSWM_VAR_NOINIT) BswM_QueueWritten;  /* PRQA S 1514, 1533 */  /* MD_CSL_ObjectOnlyAccessedOnce */
#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_RuleStates
**********************************************************************************************************************/
/** 
  \var    BswM_RuleStates
  \brief  Stores the last execution state of the rule.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_RuleStatesUType, BSWM_VAR_NOINIT) BswM_RuleStates;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [R_CC_Rule_LIN1_SchTableStartInd_Table1] */
  /*     1 */  /* [R_CC_Rule_LIN2_SchTableStartInd_MSTable0] */
  /*     2 */  /* [R_CC_Rule_LIN3_SchTableStartInd_MSTable] */
  /*     3 */  /* [R_CC_Rule_LIN4_SchTableStartInd_MSTable] */
  /*     4 */  /* [R_CC_Rule_LIN5_SchTableStartInd_MSTable] */
  /*     5 */  /* [R_CC_Rule_LIN1_SchTableStartInd_Table2] */
  /*     6 */  /* [R_CC_Rule_LIN1_SchTableStartInd_TableE] */
  /*     7 */  /* [R_CC_Rule_LIN1_SchTableStartInd_MSTable1] */
  /*     8 */  /* [R_CC_Rule_LIN1_SchTableStartInd_MSTable2] */
  /*     9 */  /* [R_CC_Rule_LIN2_SchTableStartInd_Table0] */
  /*    10 */  /* [R_CC_Rule_LIN2_SchTableStartInd_TableE] */
  /*    11 */  /* [R_CC_Rule_LIN1_SchTableStartInd_MSTable] */
  /*    12 */  /* [R_CC_Rule_LIN2_SchTableStartInd_MSTable] */
  /*    13 */  /* [R_CC_Rule_LIN3_SchTableStartInd_MSTable1] */
  /*    14 */  /* [R_CC_Rule_LIN3_SchTableStartInd_MSTable2] */
  /*    15 */  /* [R_CC_Rule_LIN3_SchTableStartInd_Table1] */
  /*    16 */  /* [R_CC_Rule_LIN3_SchTableStartInd_Table2] */
  /*    17 */  /* [R_CC_Rule_LIN3_SchTableStartInd_TableE] */
  /*    18 */  /* [R_CC_Rule_LIN4_SchTableStartInd_MSTable1] */
  /*    19 */  /* [R_CC_Rule_LIN4_SchTableStartInd_MSTable2] */
  /*    20 */  /* [R_CC_Rule_LIN4_SchTableStartInd_Table1] */
  /*    21 */  /* [R_CC_Rule_LIN4_SchTableStartInd_Table2] */
  /*    22 */  /* [R_CC_Rule_LIN4_SchTableStartInd_TableE] */
  /*    23 */  /* [R_CC_Rule_LIN5_SchTableStartInd_MSTable2] */
  /*    24 */  /* [R_CC_Rule_LIN5_SchTableStartInd_MSTable1] */
  /*    25 */  /* [R_CC_Rule_LIN5_SchTableStartInd_Table1] */
  /*    26 */  /* [R_CC_Rule_LIN5_SchTableStartInd_Table2] */
  /*    27 */  /* [R_CC_Rule_LIN5_SchTableStartInd_TableE] */
  /*    28 */  /* [R_CC_Rule_DcmEcuReset_Execute] */
  /*    29 */  /* [R_CC_Rule_DcmEcuReset_Trigger] */
  /*    30 */  /* [R_CC_Rule_LIN3_Schedule_To_Table1] */
  /*    31 */  /* [R_CC_Rule_LIN3_Schedule_To_NULL] */
  /*    32 */  /* [R_CC_Rule_LIN3_Schedule_To_Table2] */
  /*    33 */  /* [R_CC_Rule_LIN3_Schedule_To_Table_E] */
  /*    34 */  /* [R_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp] */
  /*    35 */  /* [R_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1] */
  /*    36 */  /* [R_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2] */
  /*    37 */  /* [R_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp] */
  /*    38 */  /* [R_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1] */
  /*    39 */  /* [R_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2] */
  /*    40 */  /* [R_CC_Rule_LIN1_Schedule_To_NULL] */
  /*    41 */  /* [R_CC_Rule_LIN1_Schedule_To_Table1] */
  /*    42 */  /* [R_CC_Rule_LIN1_Schedule_To_Table2] */
  /*    43 */  /* [R_CC_Rule_LIN1_Schedule_To_Table_E] */
  /*    44 */  /* [R_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp] */
  /*    45 */  /* [R_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0] */
  /*    46 */  /* [R_CC_Rule_LIN2_Schedule_To_NULL] */
  /*    47 */  /* [R_CC_Rule_LIN2_Schedule_To_Table0] */
  /*    48 */  /* [R_CC_Rule_LIN2_Schedule_To_Table_E] */
  /*    49 */  /* [R_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp] */
  /* Index        Referable Keys */
  /*    50 */  /* [R_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1] */
  /*    51 */  /* [R_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2] */
  /*    52 */  /* [R_CC_Rule_LIN4_Schedule_To_NULL] */
  /*    53 */  /* [R_CC_Rule_LIN4_Schedule_To_Table1] */
  /*    54 */  /* [R_CC_Rule_LIN4_Schedule_To_Table2] */
  /*    55 */  /* [R_CC_Rule_LIN4_Schedule_To_Table_E] */
  /*    56 */  /* [R_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp] */
  /*    57 */  /* [R_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1] */
  /*    58 */  /* [R_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2] */
  /*    59 */  /* [R_CC_Rule_LIN5_Schedule_To_NULL] */
  /*    60 */  /* [R_CC_Rule_LIN5_Schedule_To_Table1] */
  /*    61 */  /* [R_CC_Rule_LIN5_Schedule_To_Table2] */
  /*    62 */  /* [R_CC_Rule_LIN5_Schedule_To_Table_E] */
  /*    63 */  /* [R_CC_CN_CabSubnet_9ea693f1_RX_DM] */
  /*    64 */  /* [R_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289] */
  /*    65 */  /* [R_CC_CN_Backbone2_78967e2c_RX_DM] */
  /*    66 */  /* [R_CC_CN_LIN02_c2d7c6f3] */
  /*    67 */  /* [R_CC_CN_SecuritySubnet_e7a0ee54_RX] */
  /*    68 */  /* [R_CC_CN_SecuritySubnet_e7a0ee54_RX_DM] */
  /*    69 */  /* [R_CC_CN_FMSNet_fce1aae5_TX] */
  /*    70 */  /* [R_CC_CN_Backbone2_78967e2c_RX] */
  /*    71 */  /* [R_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289] */
  /*    72 */  /* [R_CC_CN_CabSubnet_9ea693f1_RX] */
  /*    73 */  /* [R_CC_CN_LIN03_b5d0f665] */
  /*    74 */  /* [R_CC_CN_FMSNet_fce1aae5_RX] */
  /*    75 */  /* [R_CC_CN_LIN01_5bde9749] */
  /*    76 */  /* [R_CC_CN_LIN04_2bb463c6] */
  /*    77 */  /* [R_CC_CN_Backbone1J1939_0b1f4bae_RX] */
  /*    78 */  /* [R_CC_CN_FMSNet_fce1aae5_RX_DM] */
  /*    79 */  /* [R_CC_CN_Backbone1J1939_0b1f4bae_RX_DM] */
  /*    80 */  /* [R_CC_CN_LIN00_2cd9a7df] */
  /*    81 */  /* [R_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX] */
  /*    82 */  /* [R_ESH_RunToPostRun] */
  /*    83 */  /* [R_ESH_RunToPostRunNested] */
  /*    84 */  /* [R_ESH_WaitToShutdown] */
  /*    85 */  /* [R_ESH_WakeupToPrep] */
  /*    86 */  /* [R_ESH_WaitToWakeup] */
  /*    87 */  /* [R_ESH_WakeupToRun] */
  /*    88 */  /* [R_ESH_InitToWakeup] */
  /*    89 */  /* [R_ESH_PostRunNested] */
  /*    90 */  /* [R_ESH_PostRun] */
  /*    91 */  /* [R_ESH_PrepToWait] */
  /*    92 */  /* [R_ESH_DemInit] */
  /*    93 */  /* [R_CC_CN_SecuritySubnet_e7a0ee54_TX] */
  /*    94 */  /* [R_CC_CN_CabSubnet_9ea693f1_TX] */
  /*    95 */  /* [R_CC_CN_Backbone2_78967e2c_TX] */
  /*    96 */  /* [R_CC_Rule_LIN1_ScheduleTableEndNotification] */
  /*    97 */  /* [R_CC_Rule_LIN2_ScheduleTableEndNotification] */
  /*    98 */  /* [R_CC_Rule_LIN3_ScheduleTableEndNotification] */
  /*    99 */  /* [R_CC_Rule_LIN4_ScheduleTableEndNotification] */
  /* Index        Referable Keys */
  /*   100 */  /* [R_CC_Rule_LIN5_ScheduleTableEndNotification] */
  /*   101 */  /* [R_CC_CN_LIN06_c5ba02ea] */
  /*   102 */  /* [R_CC_CN_LIN05_5cb35350] */
  /*   103 */  /* [R_CC_CN_LIN07_b2bd327c] */
  /*   104 */  /* [R_CC_CN_CAN6_b040c073_RX] */
  /*   105 */  /* [R_CC_CN_CAN6_b040c073_TX] */
  /*   106 */  /* [R_CC_CN_CAN6_b040c073_RX_DM] */
  /*   107 */  /* [R_CC_Rule_LIN6_ScheduleTableEndNotification] */
  /*   108 */  /* [R_CC_Rule_LIN7_ScheduleTableEndNotification] */
  /*   109 */  /* [R_CC_Rule_LIN8_ScheduleTableEndNotification] */
  /*   110 */  /* [R_CC_Rule_LIN6_SchTableStartInd_MSTable0] */
  /*   111 */  /* [R_CC_Rule_LIN6_SchTableStartInd_MSTable] */
  /*   112 */  /* [R_CC_Rule_LIN6_SchTableStartInd_Table0] */
  /*   113 */  /* [R_CC_Rule_LIN7_SchTableStartInd_MSTable] */
  /*   114 */  /* [R_CC_Rule_LIN8_SchTableStartInd_MSTable] */
  /*   115 */  /* [R_CC_Rule_LIN7_SchTableStartInd_MSTable0] */
  /*   116 */  /* [R_CC_Rule_LIN8_SchTableStartInd_MSTable0] */
  /*   117 */  /* [R_CC_Rule_LIN7_SchTableStartInd_Table0] */
  /*   118 */  /* [R_CC_Rule_LIN8_SchTableStartInd_Table0] */
  /*   119 */  /* [R_CC_Rule_LIN6_Schedule_To_Table0] */
  /*   120 */  /* [R_CC_Rule_LIN7_Schedule_To_Table0] */
  /*   121 */  /* [R_CC_Rule_LIN8_Schedule_To_Table0] */
  /*   122 */  /* [R_CC_Rule_LIN6_Schedule_To_MSTable] */
  /*   123 */  /* [R_CC_Rule_LIN7_Schedule_To_MSTable] */
  /*   124 */  /* [R_CC_Rule_LIN8_Schedule_To_MSTable] */
  /*   125 */  /* [R_CC_Rule_LIN6_Schedule_To_MSTable0] */
  /*   126 */  /* [R_CC_Rule_LIN7_Schedule_To_MSTable0] */
  /*   127 */  /* [R_CC_Rule_LIN8_Schedule_To_MSTable0] */
  /*   128 */  /* [R_CC_DCMResetProcess_Started] */
  /*   129 */  /* [R_CC_DCMResetProcess_InProgress] */
  /*   130 */  /* [R_CC_Rule_DcmEcuReset_JumpToBTL] */
  /*   131 */  /* [R_CC_Rule_NvmWriteAll_Request] */
  /*   132 */  /* [R_CC_Rule_BB1_BusOff_Indication] */
  /*   133 */  /* [R_CC_Rule_BB2_BusOff_Indication] */
  /*   134 */  /* [R_CC_Rule_CAN6_BusOff_Indication] */
  /*   135 */  /* [R_CC_Rule_CabSubnet_BusOff_Indication] */
  /*   136 */  /* [R_CC_Rule_FMSNet_BusOff_Indication] */
  /*   137 */  /* [R_CC_Rule_SecuritySubnet_BusOff_Indication] */
  /*   138 */  /* [R_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX] */
  /*   139 */  /* [R_CC_Rule_PvtReportCtrl] */

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_TimerState
**********************************************************************************************************************/
/** 
  \var    BswM_TimerState
  \brief  Variable to store current state of BswMTimer (STARTED, STOPPER OR EXPIRED).
*/ 
#define BSWM_START_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_TimerStateUType, BSWM_VAR_NOINIT) BswM_TimerState;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [MRP_ESH_NvM_CancelWriteAllTimer] */
  /*     1 */  /* [MRP_ESH_NvM_WriteAllTimer] */
  /*     2 */  /* [MRP_ESH_SelfRunRequestTimer] */

#define BSWM_STOP_SEC_VAR_NOINIT_8BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */

/**********************************************************************************************************************
  BswM_TimerValue
**********************************************************************************************************************/
/** 
  \var    BswM_TimerValue
  \brief  Variable to store current timer values.
*/ 
#define BSWM_START_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */
VAR(BswM_TimerValueUType, BSWM_VAR_NOINIT) BswM_TimerValue;  /* PRQA S 0759, 1514, 1533 */  /* MD_CSL_Union, MD_CSL_ObjectOnlyAccessedOnce, MD_CSL_ObjectOnlyAccessedOnce */
  /* Index        Referable Keys */
  /*     0 */  /* [MRP_ESH_NvM_CancelWriteAllTimer] */
  /*     1 */  /* [MRP_ESH_NvM_WriteAllTimer] */
  /*     2 */  /* [MRP_ESH_SelfRunRequestTimer] */

#define BSWM_STOP_SEC_VAR_NOINIT_32BIT
/*lint -save -esym(961, 19.1) */
#include "BswM_vMemMap.h"  /* PRQA S 5087 */  /* MD_MSR_MemMap */
/*lint -restore */


/* PRQA L:GLOBALDATADECLARATIONS */

#define BSWM_START_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

VAR(Rte_ModeType_BswMRteMDG_LIN2Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule;
VAR(Rte_ModeType_BswMRteMDG_LIN3Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule;
VAR(Rte_ModeType_BswMRteMDG_LIN4Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule;
VAR(Rte_ModeType_BswMRteMDG_LIN5Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule;
VAR(Rte_ModeType_BswMRteMDG_LIN1Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule;
VAR(Rte_ModeType_ESH_Mode, BSWM_VAR_NOINIT) BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode;
VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
VAR(Rte_ModeType_BswMRteMDG_LINSchTableState, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState;
VAR(Rte_ModeType_BswMRteMDG_LIN6Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule;
VAR(Rte_ModeType_BswMRteMDG_LIN8Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule;
VAR(Rte_ModeType_BswMRteMDG_LIN7Schedule, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule;
VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;
VAR(Rte_ModeType_BswMRteMDG_CanBusOff, BSWM_VAR_NOINIT) BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff;

VAR(BswM_BswMRteMDG_LIN3Schedule, BSWM_VAR_NOINIT) Request_LIN3_ScheduleTableRequestMode_requestedMode;
VAR(BswM_BswMRteMDG_LIN1Schedule, BSWM_VAR_NOINIT) Request_LIN1_ScheduleTableRequestMode_requestedMode;
VAR(BswM_BswMRteMDG_LIN2Schedule, BSWM_VAR_NOINIT) Request_LIN2_ScheduleTableRequestMode_requestedMode;
VAR(BswM_BswMRteMDG_LIN4Schedule, BSWM_VAR_NOINIT) Request_LIN4_ScheduleTableRequestMode_requestedMode;
VAR(BswM_BswMRteMDG_LIN5Schedule, BSWM_VAR_NOINIT) Request_LIN5_ScheduleTableRequestMode_requestedMode;
VAR(BswM_BswMRteMDG_LIN6Schedule, BSWM_VAR_NOINIT) Request_LIN6_ScheduleTableRequestMode_requestedMode;
VAR(BswM_BswMRteMDG_LIN7Schedule, BSWM_VAR_NOINIT) Request_LIN7_ScheduleTableRequestMode_requestedMode;
VAR(BswM_BswMRteMDG_LIN8Schedule, BSWM_VAR_NOINIT) Request_LIN8_ScheduleTableRequestMode_requestedMode;
VAR(BswM_BswMRteMDG_NvmWriteAllRequest, BSWM_VAR_NOINIT) Request_SwcModeRequest_NvmWriteAllRequest_requestedMode;
VAR(BswM_BswMRteMDG_PvtReport_X1C14, BSWM_VAR_NOINIT) Request_SwcModeRequest_PvtReportCtrl_requestedMode;
VAR(Rte_ModeType_ESH_Mode, BSWM_VAR_NOINIT) BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode;
VAR(Rte_ModeType_DcmEcuReset, BSWM_VAR_NOINIT) BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset;


VAR(boolean, BSWM_VAR_NOINIT) BswM_PreInitialized;
#define BSWM_STOP_SEC_VAR_NOINIT_UNSPECIFIED
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"


#define BSWM_START_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

/* -----------------------------------------------------------------------------
    &&&~ FUNCTIONS
 ----------------------------------------------------------------------------- */
 
/**********************************************************************************************************************
 *  BswM_ExecuteIpduGroupControl()
 **********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_ExecuteIpduGroupControl(void)
{
  Com_IpduGroupVector ipduGroupReinitState;
  Com_IpduGroupVector ipduGroupState;
  Com_IpduGroupVector dmState;
  uint16 iCnt;
  uint8 controlInvocation = BSWM_GROUPCONTROL_IDLE;

  SchM_Enter_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  if(BswM_PduGroupControlInvocation != BSWM_GROUPCONTROL_IDLE)
  {
    if((BswM_PduGroupControlInvocation & BSWM_GROUPCONTROL_REINIT) != 0u)
    {
      iCnt = BSWM_IPDUGROUPVECTORSIZE;
      while(iCnt-- > (uint16)0x0000) /* PRQA S 3440 */ /* MD_BswM_3440 */
      {
        ipduGroupReinitState[iCnt] = BswM_ComIPduGroupReinitState[iCnt]; /* SBSW_BSWM_SETIPDUGROUPVECTOR */
      }
    }
    /* Check if also PDU control actions without reinitialization flag were performed.
     * If yes, execute them too and update BswM_ComIPduGroupReinitState */
    if((BswM_PduGroupControlInvocation & BSWM_GROUPCONTROL_NORMAL) != 0u)
    {
      iCnt = BSWM_IPDUGROUPVECTORSIZE;
      while(iCnt-- > (uint16)0x0000) /* PRQA S 3440 */ /* MD_BswM_3440 */
      {
        ipduGroupState[iCnt] = BswM_ComIPduGroupState[iCnt]; /* SBSW_BSWM_SETIPDUGROUPVECTOR */
        BswM_ComIPduGroupReinitState[iCnt] = BswM_ComIPduGroupState[iCnt]; /* SBSW_BSWM_SETIPDUGROUPVECTOR */
      }
    }
    if((BswM_PduGroupControlInvocation & BSWM_GROUPCONTROL_DM) != 0u)
    {
      iCnt = BSWM_IPDUGROUPVECTORSIZE;
      while(iCnt-- > (uint16)0x0000) /* PRQA S 3440 */ /* MD_BswM_3440 */
      {
        dmState[iCnt] = BswM_ComRxIPduGroupDMState[iCnt]; /* SBSW_BSWM_SETIPDUGROUPVECTOR */
      }
    }
    controlInvocation = BswM_PduGroupControlInvocation;
    BswM_PduGroupControlInvocation = BSWM_GROUPCONTROL_IDLE;
  }
  SchM_Exit_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */

  if(controlInvocation != BSWM_GROUPCONTROL_IDLE)
  {
    if((controlInvocation & BSWM_GROUPCONTROL_REINIT) != 0u)
    {
      Com_IpduGroupControl(ipduGroupReinitState, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
    }
    if((controlInvocation & BSWM_GROUPCONTROL_NORMAL) != 0u)
    {
      Com_IpduGroupControl(ipduGroupState, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
    }
    if((controlInvocation & BSWM_GROUPCONTROL_DM) != 0u)
    {
      Com_ReceptionDMControl(dmState); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
    }
  }
} /* PRQA S 6010, 6030 */ /* MD_MSR_STPTH, MD_MSR_STCYC */

/**********************************************************************************************************************
 *  BswM_Action_RuleHandler()
 **********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_Action_RuleHandler(BswM_HandleType handleId, BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType actionListIndex;
  Std_ReturnType retVal = E_NOT_OK; /* PRQA S 2981 */ /* MD_MSR_RetVal */
  
#if ( BSWM_DEV_ERROR_DETECT == STD_ON )
  if (handleId < BswM_GetSizeOfRules(partitionIdx))
#endif
  {
    SchM_Enter_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    actionListIndex = BswM_GetFctPtrOfRules(handleId, partitionIdx)(partitionIdx); /* SBSW_BSWM_RULEFCTPTR */
    SchM_Exit_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
    if(actionListIndex < BswM_GetSizeOfActionLists(partitionIdx))
    {
      retVal = BswM_GetFctPtrOfActionLists(actionListIndex, partitionIdx)(partitionIdx); /* SBSW_BSWM_ACTIONLISTFCTPTR */
    }
    else
    {
      retVal = E_OK;
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  
  return retVal;
} 

/**********************************************************************************************************************
 *  BswM_UpdateRuleStates()
 **********************************************************************************************************************/
BSWM_LOCAL_INLINE FUNC(void, BSWM_CODE) BswM_UpdateRuleStates(BswM_SizeOfRuleStatesType ruleId, 
                                                                   BswM_RuleStatesType state,
                                                                   BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  if (ruleId < BswM_GetSizeOfRuleStates(partitionIdx))
  {
    BswM_SetRuleStates(ruleId, state, partitionIdx); /* SBSW_BSWM_SETRULESTATE */
  }
  
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}

/**********************************************************************************************************************
 *  BswM_UpdateTimer()
 **********************************************************************************************************************/
BSWM_LOCAL_INLINE FUNC(void, BSWM_CODE) BswM_UpdateTimer(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx,
                                                              BswM_SizeOfTimerValueType timerId,
                                                              BswM_TimerValueType value)
{
  if (timerId < BswM_GetSizeOfTimerValue(partitionIdx))
  {
      SchM_Enter_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
      BswM_SetTimerValue(timerId, value, partitionIdx); /* SBSW_BSWM_SETTIMER */
      BswM_SetTimerState(timerId, (BswM_TimerStateType)((value != 0u) ? BSWM_TIMER_STARTED : BSWM_TIMER_STOPPED), partitionIdx); /* SBSW_BSWM_SETTIMER */
      SchM_Exit_BswM_BSWM_EXCLUSIVE_AREA_0(); /* PRQA S 3109 */ /* MD_MSR_14.3 */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}

/**********************************************************************************************************************
 *  BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_InitGenVarAndInitAL_BSWM_SINGLEPARTITION(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{

  BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule = 0xFFu;
  BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = 0xFFu;
  BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = 0xFFu;
  BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = 0xFFu;
  BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = 0xFFu;
  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = 0xFFu;
  BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
  BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
  BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
  BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
  BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
  BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
  BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
  BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
  BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule = 0xFFu;
  BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule = 0xFFu;
  BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule = 0xFFu;
  BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
  BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
  BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
  BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
  BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
  BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
  Request_LIN3_ScheduleTableRequestMode_requestedMode = LIN3_NULL;
  Request_LIN1_ScheduleTableRequestMode_requestedMode = LIN1_NULL;
  Request_LIN2_ScheduleTableRequestMode_requestedMode = LIN2_NULL;
  Request_LIN4_ScheduleTableRequestMode_requestedMode = LIN4_NULL;
  Request_LIN5_ScheduleTableRequestMode_requestedMode = LIN5_NULL;
  Request_LIN6_ScheduleTableRequestMode_requestedMode = LIN6_NULL;
  Request_LIN7_ScheduleTableRequestMode_requestedMode = LIN7_NULL;
  Request_LIN8_ScheduleTableRequestMode_requestedMode = LIN8_NULL;
  Request_SwcModeRequest_NvmWriteAllRequest_requestedMode = NvmWriteAll_NoRequest;
  Request_SwcModeRequest_PvtReportCtrl_requestedMode = PvtReport_Enabled;
  BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_STARTUP;
  BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset = RTE_MODE_DcmEcuReset_NONE;
  BswM_PduGroupControlInvocation = BSWM_GROUPCONTROL_IDLE;

  /* PRQA S 3109 COMCLEARIPDU */ /* MD_BswM_3109 */
  Com_ClearIpduGroupVector(BswM_ComIPduGroupState); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  Com_ClearIpduGroupVector(BswM_ComIPduGroupReinitState); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  Com_ClearIpduGroupVector(BswM_ComRxIPduGroupDMState); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMCLEARIPDU */
  (void)BswM_ActionList_INIT_AL_Initialize(partitionIdx);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}

/**********************************************************************************************************************
 *  BswMRteRequestFunctions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  BswM_Read_LIN3_ScheduleTableRequestMode
 *********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_Read_LIN3_ScheduleTableRequestMode(void)
{
  (void)Rte_Read_Request_LIN3_ScheduleTableRequestMode_requestedMode(&Request_LIN3_ScheduleTableRequestMode_requestedMode);  /* SBSW_BSWM_RTE_READ */
  BswM_ImmediateSwcRequest(BSWM_SINGLEPARTITION, 2);
}

/**********************************************************************************************************************
 *  BswM_Read_LIN1_ScheduleTableRequestMode
 *********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_Read_LIN1_ScheduleTableRequestMode(void)
{
  (void)Rte_Read_Request_LIN1_ScheduleTableRequestMode_requestedMode(&Request_LIN1_ScheduleTableRequestMode_requestedMode);  /* SBSW_BSWM_RTE_READ */
  BswM_ImmediateSwcRequest(BSWM_SINGLEPARTITION, 0);
}

/**********************************************************************************************************************
 *  BswM_Read_LIN2_ScheduleTableRequestMode
 *********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_Read_LIN2_ScheduleTableRequestMode(void)
{
  (void)Rte_Read_Request_LIN2_ScheduleTableRequestMode_requestedMode(&Request_LIN2_ScheduleTableRequestMode_requestedMode);  /* SBSW_BSWM_RTE_READ */
  BswM_ImmediateSwcRequest(BSWM_SINGLEPARTITION, 1);
}

/**********************************************************************************************************************
 *  BswM_Read_LIN4_ScheduleTableRequestMode
 *********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_Read_LIN4_ScheduleTableRequestMode(void)
{
  (void)Rte_Read_Request_LIN4_ScheduleTableRequestMode_requestedMode(&Request_LIN4_ScheduleTableRequestMode_requestedMode);  /* SBSW_BSWM_RTE_READ */
  BswM_ImmediateSwcRequest(BSWM_SINGLEPARTITION, 3);
}

/**********************************************************************************************************************
 *  BswM_Read_LIN5_ScheduleTableRequestMode
 *********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_Read_LIN5_ScheduleTableRequestMode(void)
{
  (void)Rte_Read_Request_LIN5_ScheduleTableRequestMode_requestedMode(&Request_LIN5_ScheduleTableRequestMode_requestedMode);  /* SBSW_BSWM_RTE_READ */
  BswM_ImmediateSwcRequest(BSWM_SINGLEPARTITION, 4);
}

/**********************************************************************************************************************
 *  BswM_Read_LIN6_ScheduleTableRequestMode
 *********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_Read_LIN6_ScheduleTableRequestMode(void)
{
  (void)Rte_Read_Request_LIN6_ScheduleTableRequestMode_requestedMode(&Request_LIN6_ScheduleTableRequestMode_requestedMode);  /* SBSW_BSWM_RTE_READ */
  BswM_ImmediateSwcRequest(BSWM_SINGLEPARTITION, 5);
}

/**********************************************************************************************************************
 *  BswM_Read_LIN7_ScheduleTableRequestMode
 *********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_Read_LIN7_ScheduleTableRequestMode(void)
{
  (void)Rte_Read_Request_LIN7_ScheduleTableRequestMode_requestedMode(&Request_LIN7_ScheduleTableRequestMode_requestedMode);  /* SBSW_BSWM_RTE_READ */
  BswM_ImmediateSwcRequest(BSWM_SINGLEPARTITION, 6);
}

/**********************************************************************************************************************
 *  BswM_Read_LIN8_ScheduleTableRequestMode
 *********************************************************************************************************************/
FUNC(void, BSWM_CODE) BswM_Read_LIN8_ScheduleTableRequestMode(void)
{
  (void)Rte_Read_Request_LIN8_ScheduleTableRequestMode_requestedMode(&Request_LIN8_ScheduleTableRequestMode_requestedMode);  /* SBSW_BSWM_RTE_READ */
  BswM_ImmediateSwcRequest(BSWM_SINGLEPARTITION, 7);
}

/**********************************************************************************************************************
 *  BswM_ImmediateSwcRequest
 *********************************************************************************************************************/
BSWM_LOCAL_INLINE FUNC(void, BSWM_CODE) BswM_ImmediateSwcRequest(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx, BswM_SizeOfModeRequestMappingType requestId)
{
  if(requestId < BswM_GetSizeOfModeRequestMapping(partitionIdx))
  {
    BswM_ImmediateModeRequest(BswM_GetImmediateUserStartIdxOfModeRequestMapping(requestId, partitionIdx), BswM_GetImmediateUserEndIdxOfModeRequestMapping(requestId, partitionIdx), partitionIdx);
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
}

/**********************************************************************************************************************
 *  BswM_ModeNotificationFct_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_ModeNotificationFct_BSWM_SINGLEPARTITION(void)
{
  if(BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule(BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule(BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule(BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule(BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule(BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = 0xFFu;
    }
  }
  if(BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode != 0xFFu)
  {
    if(Rte_Switch_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode(BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode) == RTE_E_OK)
    {
      BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState(BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule(BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule(BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule(BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
    }
  }
  if(BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff != 0xFFu)
  {
    if(Rte_Switch_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff(BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff) == RTE_E_OK)
    {
      BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = 0xFFu;
    }
  }
}

/**********************************************************************************************************************
 *  BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(void, BSWM_CODE) BswM_SwcModeRequestUpdateFct_BSWM_SINGLEPARTITION(void)
{
  uint32 mode;
  mode = Rte_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode();
  if (mode != RTE_TRANSITION_ESH_Mode)
  {
    BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode = (Rte_ModeType_ESH_Mode)mode;
  }
  mode = Rte_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset();
  if (mode != RTE_TRANSITION_DcmEcuReset)
  {
    BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset = (Rte_ModeType_DcmEcuReset)mode;
  }
  (void)Rte_Read_Request_SwcModeRequest_NvmWriteAllRequest_requestedMode(&Request_SwcModeRequest_NvmWriteAllRequest_requestedMode); /* SBSW_BSWM_RTE_READ */
  (void)Rte_Read_Request_SwcModeRequest_PvtReportCtrl_requestedMode(&Request_SwcModeRequest_PvtReportCtrl_requestedMode); /* SBSW_BSWM_RTE_READ */
}

/**********************************************************************************************************************
 *  BswMActionListFunctions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table1;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp_TABLE0;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table1;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table2;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp_Table2;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table1;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table2;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_Table_E;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_SchTableStartInd_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE0;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_TABLE_E;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN1CurSchTable_BswM_MDGP_BswMRteMDG_LIN1Schedule = RTE_MODE_BswMRteMDG_LIN1Schedule_LIN1_MasterReq_SlaveResp;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN2CurSchTable_BswM_MDGP_BswMRteMDG_LIN2Schedule = RTE_MODE_BswMRteMDG_LIN2Schedule_LIN2_MasterReq_SlaveResp;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table1;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_MasterReq_SlaveResp_Table2;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE1;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE2;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN3CurSchTable_BswM_MDGP_BswMRteMDG_LIN3Schedule = RTE_MODE_BswMRteMDG_LIN3Schedule_LIN3_TABLE_E;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp_Table2;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_MasterReq_SlaveResp;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE1;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE2;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN4CurSchTable_BswM_MDGP_BswMRteMDG_LIN4Schedule = RTE_MODE_BswMRteMDG_LIN4Schedule_LIN4_TABLE_E;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp_Table1;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_MasterReq_SlaveResp;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE1;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE2;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN5CurSchTable_BswM_MDGP_BswMRteMDG_LIN5Schedule = RTE_MODE_BswMRteMDG_LIN5Schedule_LIN5_TABLE_E;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_DcmEcuReset_Trigger
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DcmEcuReset_Trigger(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)BswM_ActionList_CC_AL_DCMResetPostRun(partitionIdx);
  (void)EcuM_SelectShutdownTarget(ECUM_STATE_RESET, EcuMConf_EcuMResetMode_ECUM_RESET_MCU);
  BswM_RequestMode(BSWM_GENERIC_DCMResetProcess, BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Started);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN04_2bb463c6, LinSMConf_LinSMSchedule_Table_2_aa1a2032);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN03_b5d0f665, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_f18f4341);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN04_2bb463c6, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_294c3867);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN01_5bde9749, LinSMConf_LinSMSchedule_Table_e_dbb68a08);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_2a158a89);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, LinSMConf_LinSMSchedule_Table_1_cbd13a3e);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, LinSMConf_LinSMSchedule_CHNL_08a9294c_f4be7c2c);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, LinSMConf_LinSMSchedule_Table_2_52d86b84);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, LinSMConf_LinSMSchedule_Table_e_a7d7afd3);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_1692e5d6);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_631128e5);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_fa18795f);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_03d8aeb9);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN01_5bde9749, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_e4c5082e);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, LinSMConf_LinSMSchedule_Table_2_b3b6af29);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN04_2bb463c6, LinSMConf_LinSMSchedule_Table_e_5f15e465);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN03_b5d0f665, LinSMConf_LinSMSchedule_Table_e_3ad84ea5);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, LinSMConf_LinSMSchedule_Table_e_46b96b7e);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN03_b5d0f665, LinSMConf_LinSMSchedule_Table_2_cfd78af2);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN04_2bb463c6, LinSMConf_LinSMSchedule_Table_1_33137188);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN03_b5d0f665, LinSMConf_LinSMSchedule_Table_1_56dedb48);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN01_5bde9749, LinSMConf_LinSMSchedule_Table0_df24d2a0);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, LinSMConf_LinSMSchedule_Table_1_2abffe93);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN04_2bb463c6, LinSMConf_LinSMSchedule_CHNL_def0ca51_faa54df8);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN03_b5d0f665, LinSMConf_LinSMSchedule_CHNL_c3f5fae9_fb3b3248);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN01_5bde9749, LinSMConf_LinSMSchedule_CHNL_8e3d5be2_a66eb9f7);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, LinSMConf_LinSMSchedule_CHNL_45618847_32a17f26);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN04_2bb463c6, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_21159feb);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN03_b5d0f665, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_de9a2869);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_b31cdb33);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN01_5bde9749, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_9e3481d6);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN04_2bb463c6, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_b81cce51);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN03_b5d0f665, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_479379d3);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Rx_063a5fbc, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Rx_063a5fbc, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  if(E_OK != J1939Rm_SetState(ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae, J1939NmConf_J1939NmNode_CIOM_4d5cd289, J1939RM_STATE_OFFLINE))
  {
    Dem_ReportErrorStatus(DemConf_DemEventParameter_AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFa_540060b1, DEM_EVENT_STATUS_FAILED);
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  if(E_OK != J1939Rm_SetState(ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae, J1939NmConf_J1939NmNode_CIOM_4d5cd289, J1939RM_STATE_ONLINE))
  {
    Dem_ReportErrorStatus(DemConf_DemEventParameter_AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, DEM_EVENT_STATUS_FAILED);
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Disable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Rx_4e624434, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Enable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Rx_4e624434, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_Sec_CanTp_Tx, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_Sec_CanTp_Tx, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Tx_6c66ba3a, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_Sec_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_Sec_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN02_Tx_214b70a7, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN02_Tx_214b70a7, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN02_Rx_7711d721, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN02_Rx_7711d721, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Enable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_1692e5d6);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN02_Tx_214b70a7, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN02_Tx_214b70a7, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN02_Rx_7711d721, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN02_Rx_7711d721, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oSecuritySubnet_Rx_3a3c1dbc, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x0F_BC_89b46e88, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x10_BC_fa80b878, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Rx_4e624434, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Rx_4e624434, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Rx_4e624434, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Rx_4e624434, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Tx_5060f83a, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Tx_5060f83a, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_Cab_CanTp_Tx, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_Cab_CanTp_Tx, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Tx_5060f83a, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Tx_5060f83a, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_Cab_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_Cab_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Tx_1838e3b2, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Tx_1838e3b2, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_BB2_CanTp_Tx, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_BB2_CanTp_Tx, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Tx_1838e3b2, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Tx_1838e3b2, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_BB2_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_BB2_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  if(E_OK != J1939Rm_SetState(ComMConf_ComMChannel_CN_FMSNet_fce1aae5, J1939NmConf_J1939NmNode_CIOM_4d5cd289, J1939RM_STATE_OFFLINE))
  {
    Dem_ReportErrorStatus(DemConf_DemEventParameter_AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_OFFLINE_BswMReportFailToDemRef, DEM_EVENT_STATUS_FAILED);
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  if(E_OK != J1939Rm_SetState(ComMConf_ComMChannel_CN_FMSNet_fce1aae5, J1939NmConf_J1939NmNode_CIOM_4d5cd289, J1939RM_STATE_ONLINE))
  {
    Dem_ReportErrorStatus(DemConf_DemEventParameter_AutoCreatedDemEvent_CC_J1939RmStateSwitch_CN_FMSNet_fce1aae5_CIOM_4d5cd289_J1939RM_STATE_ONLINE_BswMReportFailToDemRef, DEM_EVENT_STATUS_FAILED);
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Rx_063a5fbc, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Rx_063a5fbc, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Rx_063a5fbc, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCabSubnet_Rx_063a5fbc, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN03_Tx_99f717c2, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN03_Tx_99f717c2, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN03_Rx_cfadb044, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN03_Rx_cfadb044, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Enable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN03_b5d0f665, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_f18f4341);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN03_Tx_99f717c2, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN03_Tx_99f717c2, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN03_Rx_cfadb044, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN03_Rx_cfadb044, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Rx_BC_dd2c1510, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Rx_BC_dd2c1510, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Rx_BC_dd2c1510, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Rx_BC_dd2c1510, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN01_Tx_33fedf49, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN01_Tx_33fedf49, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN01_Rx_65a478cf, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN01_Rx_65a478cf, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Enable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN01_5bde9749, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_e4c5082e);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN01_Tx_33fedf49, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN01_Tx_33fedf49, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN01_Rx_65a478cf, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN01_Rx_65a478cf, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN04_Tx_04202f7b, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN04_Tx_04202f7b, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN04_Rx_527a88fd, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN04_Rx_527a88fd, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Enable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN04_2bb463c6, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_294c3867);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN04_Tx_04202f7b, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN04_Tx_04202f7b, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN04_Rx_527a88fd, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN04_Rx_527a88fd, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Disable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Rx_BC_dd2c1510, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Enable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Rx_BC_dd2c1510, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Rx_BC_405bc776, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN00_Tx_8b42b82c, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN00_Tx_8b42b82c, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN00_Rx_dd181faa, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN00_Rx_dd181faa, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Enable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_03d8aeb9);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN00_Tx_8b42b82c, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN00_Tx_8b42b82c, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN00_Rx_dd181faa, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN00_Rx_dd181faa, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oFMSNet_Tx_0x31_BC_38fc8c7d, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_ExitRun
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_ExitRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  SchM_Enter_BswM_BSWM_EXCLUSIVE_AREA_0();
  /*lint -restore */
  ESH_ComM_CheckPendingRequests();
  if(E_OK != BswM_Action_RuleHandler(BSWM_ID_RULE_ESH_RunToPostRunNested, partitionIdx))
  {
    Dem_ReportErrorStatus(DemConf_DemEventParameter_AutoCreatedDemEvent_ESH_RunToPostRunNested_BswMReportFailToDemRef, DEM_EVENT_STATUS_FAILED);
  }
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  SchM_Exit_BswM_BSWM_EXCLUSIVE_AREA_0();
  /*lint -restore */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_RunToPostRun
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_RunToPostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN04_2bb463c6, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_CabSubnet_9ea693f1, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN03_b5d0f665, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_Backbone2_78967e2c, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN01_5bde9749, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_FMSNet_fce1aae5, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN06_c5ba02ea, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN05_5cb35350, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN07_b2bd327c, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_CAN6_b040c073, FALSE);
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  EcuM_ClearValidatedWakeupEvent(ECUM_WKSOURCE_ALL_SOURCES);
  /*lint -restore */
  BswM_ESH_OnEnterPostRun();
  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_POSTRUN;
  BswM_RequestMode(BSWM_GENERIC_ESH_State, BSWM_GENERICVALUE_ESH_State_ESH_POST_RUN);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_WaitForNvMToShutdown
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_WaitForNvMToShutdown(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_NvM_WriteAllTimer, 0u);
  BswM_ESH_OnEnterShutdown();
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  (void)EcuM_GoToSelectedShutdownTarget();
  /*lint -restore */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_WakeupToPrep
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_WakeupToPrep(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_ESH_OnEnterPrepShutdown();
  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_SHUTDOWN;
  BswM_RequestMode(BSWM_GENERIC_ESH_State, BSWM_GENERICVALUE_ESH_State_ESH_PREP_SHUTDOWN);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_WaitForNvMWakeup
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_WaitForNvMWakeup(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_NvM_WriteAllTimer, 0u);
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_NvM_CancelWriteAllTimer, 12000uL);
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  NvM_CancelWriteAll();
  /*lint -restore */
  BswM_ESH_OnEnterWakeup();
  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_WAKEUP;
  BswM_RequestMode(BSWM_GENERIC_ESH_State, BSWM_GENERICVALUE_ESH_State_ESH_WAKEUP);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_WakeupToRun
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_WakeupToRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_NvM_CancelWriteAllTimer, 0u);
  (void)BswM_Action_RuleHandler(BSWM_ID_RULE_ESH_DemInit, partitionIdx);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN04_2bb463c6, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_CabSubnet_9ea693f1, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN03_b5d0f665, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_Backbone2_78967e2c, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN01_5bde9749, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_FMSNet_fce1aae5, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54, TRUE);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_Cab_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduReinitGroup(ComConf_ComIPduGroup_CIOM_Cab_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_Cab_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_BB2_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduReinitGroup(ComConf_ComIPduGroup_CIOM_BB2_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_BB2_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_Sec_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduReinitGroup(ComConf_ComIPduGroup_CIOM_Sec_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_Sec_CanTp_Tx, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_REINIT);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN06_c5ba02ea, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN05_5cb35350, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN07_b2bd327c, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_CAN6_b040c073, TRUE);
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_SelfRunRequestTimer, 500uL);
  BswM_ESH_OnEnterRun();
  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_RUN;
  BswM_RequestMode(BSWM_GENERIC_ESH_State, BSWM_GENERICVALUE_ESH_State_ESH_RUN);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_InitToWakeup
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_InitToWakeup(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_ESH_OnEnterWakeup();
  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_WAKEUP;
  BswM_RequestMode(BSWM_GENERIC_ESH_State, BSWM_GENERICVALUE_ESH_State_ESH_WAKEUP);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_PostRunToPrepShutdown
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_PostRunToPrepShutdown(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Dem_Shutdown();
  /*lint -restore */
  BswM_RequestMode(BSWM_GENERIC_ESH_DemInitStatus, BSWM_GENERICVALUE_ESH_DemInitStatus_DEM_NOT_INITIALIZED);
  BswM_ESH_OnEnterPrepShutdown();
  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_SHUTDOWN;
  BswM_RequestMode(BSWM_GENERIC_ESH_State, BSWM_GENERICVALUE_ESH_State_ESH_PREP_SHUTDOWN);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_PostRunToRun
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_PostRunToRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN04_2bb463c6, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_CabSubnet_9ea693f1, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN03_b5d0f665, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_Backbone2_78967e2c, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN01_5bde9749, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_FMSNet_fce1aae5, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN06_c5ba02ea, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN05_5cb35350, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN07_b2bd327c, TRUE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_CAN6_b040c073, TRUE);
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_SelfRunRequestTimer, 500uL);
  BswM_ESH_OnEnterRun();
  BswM_Switch_ESH_ModeSwitch_BswM_MDGP_ESH_Mode = RTE_MODE_ESH_Mode_RUN;
  BswM_RequestMode(BSWM_GENERIC_ESH_State, BSWM_GENERICVALUE_ESH_State_ESH_RUN);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_ExitPostRun
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_ExitPostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  SchM_Enter_BswM_BSWM_EXCLUSIVE_AREA_0();
  /*lint -restore */
  ESH_ComM_CheckPendingRequests();
  if(E_OK != BswM_Action_RuleHandler(BSWM_ID_RULE_ESH_PostRunNested, partitionIdx))
  {
    Dem_ReportErrorStatus(DemConf_DemEventParameter_AutoCreatedDemEvent_ESH_PostRunNested_BswMReportFailToDemRef, DEM_EVENT_STATUS_FAILED);
  }
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  SchM_Exit_BswM_BSWM_EXCLUSIVE_AREA_0();
  /*lint -restore */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_PrepShutdownToWaitForNvM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_PrepShutdownToWaitForNvM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)BswM_Action_RuleHandler(BSWM_ID_RULE_CC_Rule_NvmWriteAll_Request, partitionIdx);
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_NvM_WriteAllTimer, 6000uL);
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  NvM_WriteAll();
  /*lint -restore */
  ESH_ComM_CheckPendingRequests();
  BswM_ESH_OnEnterWaitForNvm();
  BswM_RequestMode(BSWM_GENERIC_ESH_State, BSWM_GENERICVALUE_ESH_State_ESH_WAIT_FOR_NVM);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_INIT_AL_Initialize
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_INIT_AL_Initialize(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_AL_SetProgrammableInterrupts();
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Fee_Init();
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  NvM_Init();
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  WrapNv_Init();
  /*lint -restore */
  BswM_INIT_NvMReadAll();
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Cry_30_LibCv_Init();
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Can_Init(Can_Config_Ptr);
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Lin_Init(Lin_Config_Ptr);
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  CanTrcv_30_GenericCan_Init(CanTrcv_30_GenericCan_Config_Ptr);
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  ComM_Init();
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  LinTrcv_30_Generic_Init(LinTrcv_30_Generic_Config_Ptr);
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Nm_Init();
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Csm_Init();
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Dem_Init(Dem_Config_Ptr);
  /*lint -restore */
  Wakeup_BSWModule_Init();
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  CanXcp_Init(NULL_PTR);
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Xcp_Init();
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Rtm_Init();
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  RamTst_Init();
  /*lint -restore */
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Rte_Start();
  /*lint -restore */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_ESH_AL_DemInit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_ESH_AL_DemInit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Dem_Init(Dem_Config_Ptr);
  /*lint -restore */
  BswM_RequestMode(BSWM_GENERIC_ESH_DemInitStatus, BSWM_GENERICVALUE_ESH_DemInitStatus_DEM_INITIALIZED);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN1_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN1_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN1SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN2_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN2_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN2SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN3_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN3_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN3SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN4_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN4_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN4SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN5_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN5_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN5SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN06_Tx_ae29e7f0, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN06_Tx_ae29e7f0, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN06_Rx_f8734076, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN06_Rx_f8734076, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Enable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN06_c5ba02ea, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_3c067308);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN06_Tx_ae29e7f0, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN06_Tx_ae29e7f0, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN06_Rx_f8734076, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN06_Rx_f8734076, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN05_Tx_bc9c481e, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN05_Tx_bc9c481e, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN05_Rx_eac6ef98, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN05_Rx_eac6ef98, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Enable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN05_5cb35350, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_ce519ef0);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN05_Tx_bc9c481e, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN05_Tx_bc9c481e, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN05_Rx_eac6ef98, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN05_Rx_eac6ef98, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN07_Tx_16958095, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN07_Tx_16958095, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN07_Rx_40cf2713, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN07_Rx_40cf2713, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Enable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Enable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN07_b2bd327c, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_db1bd59f);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN07_Tx_16958095, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN07_Tx_16958095, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oLIN07_Rx_40cf2713, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oLIN07_Rx_40cf2713, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Rx_08e785ef, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Rx_08e785ef, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Rx_08e785ef, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Rx_08e785ef, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Tx_5ebd2269, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Tx_5ebd2269, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Tx_5ebd2269, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Tx_5ebd2269, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CAN6_b040c073_Disable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_Disable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Rx_08e785ef, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_CAN6_b040c073_Enable_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_CAN6_b040c073_Enable_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oCAN6_Rx_08e785ef, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkDmControlInvocation();
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN6_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN7_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN8_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_EndOfNotification;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN6_SchTableStartInd_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_TABLE0;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN7_SchTableStartInd_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_TABLE0;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN8_SchTableStartInd_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_TABLE0;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN6SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN6CurSchTable_BswM_MDGP_BswMRteMDG_LIN6Schedule = RTE_MODE_BswMRteMDG_LIN6Schedule_LIN6_MasterReq_SlaveResp_Table0;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN7SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN7CurSchTable_BswM_MDGP_BswMRteMDG_LIN7Schedule = RTE_MODE_BswMRteMDG_LIN7Schedule_LIN7_MasterReq_SlaveResp_Table0;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_LIN8SchTableState_BswM_MDGP_BswMRteMDG_LINSchTableState = RTE_MODE_BswMRteMDG_LINSchTableState_LIN_SchTable_StartOfIndication;
  BswM_Switch_BswMSP_LIN8CurSchTable_BswM_MDGP_BswMRteMDG_LIN8Schedule = RTE_MODE_BswMRteMDG_LIN8Schedule_LIN8_MasterReq_SlaveResp_Table0;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN05_5cb35350, LinSMConf_LinSMSchedule_Table0_1f9db836);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN06_c5ba02ea, LinSMConf_LinSMSchedule_Table0_a257d4f8);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN07_b2bd327c, LinSMConf_LinSMSchedule_Table0_7fc10d7d);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN05_5cb35350, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_45fec21a);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN05_5cb35350, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_ce519ef0);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN06_c5ba02ea, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_1ee9730f);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN06_c5ba02ea, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_3c067308);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN07_b2bd327c, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_281be3fc);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)LinSM_ScheduleRequest(ComMConf_ComMChannel_CN_LIN07_b2bd327c, LinSMConf_LinSMSchedule_MasterReq_SlaveResp_db1bd59f);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_DCMResetProcess_Started
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DCMResetProcess_Started(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_NvM_WriteAllTimer, 6000uL);
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  NvM_WriteAll();
  /*lint -restore */
  (void)BswM_ActionList_CC_AL_RunToECUReset(partitionIdx);
  BswM_RequestMode(BSWM_GENERIC_DCMResetProcess, BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Inprogress);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_DCMResetProcess_InProgress
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DCMResetProcess_InProgress(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_NvM_WriteAllTimer, 0u);
  BswM_RequestMode(BSWM_GENERIC_DCMResetProcess, BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Completed);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_RunToECUReset
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_RunToECUReset(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_Backbone1J1939_0b1f4bae, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN02_c2d7c6f3, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN04_2bb463c6, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_CabSubnet_9ea693f1, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN03_b5d0f665, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN00_2cd9a7df, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_Backbone2_78967e2c, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN01_5bde9749, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_FMSNet_fce1aae5, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_SecuritySubnet_e7a0ee54, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN06_c5ba02ea, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN05_5cb35350, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_LIN07_b2bd327c, FALSE);
  ComM_CommunicationAllowed(ComMConf_ComMChannel_CN_CAN6_b040c073, FALSE);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_DcmEcuReset_JumpToBTL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DcmEcuReset_JumpToBTL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  (void)EcuM_SelectShutdownTarget(ECUM_STATE_RESET, EcuMConf_EcuMResetMode_ECUM_RESET_MCU);
  BswM_RequestMode(BSWM_GENERIC_DCMResetProcess, BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Completed);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_DCMResetPostRun
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_DCMResetPostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  Dem_Shutdown();
  /*lint -restore */
  BswM_RequestMode(BSWM_GENERIC_ESH_DemInitStatus, BSWM_GENERICVALUE_ESH_DemInitStatus_DEM_NOT_INITIALIZED);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_PrepShutdown_NvmWriteAll
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_PrepShutdown_NvmWriteAll(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_UpdateTimer(partitionIdx, BSWM_TMR_ESH_NvM_WriteAllTimer, 6000uL);
  /*lint -save -e534 *//* PRQA S 3109, 3200 1 */ /* MD_MSR_14.3, MD_BSWM_3200 */
  NvM_WriteAll();
  /*lint -restore */
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_True_AL_BB1_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_BB1_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_False_AL_BB1_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_BB1_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_BB1_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_True_AL_BB2_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_BB2_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_False_AL_CAN6_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_CAN6_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_True_AL_CAN6_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_CAN6_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_CAN6_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_False_AL_BB2_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_BB2_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_BB2_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_False_AL_CabSubnet_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_CabSubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_False_AL_FMSNet_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_FMSNet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_True_AL_CabSubnet_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_CabSubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_CabSubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_True_AL_FMSNet_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_FMSNet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_FMSnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_False_AL_SecuritySubnet_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_False_AL_SecuritySubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_NormalCom;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_True_AL_SecuritySubnet_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_True_AL_SecuritySubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_Switch_BswMSP_SecuritySubnet_CanBusOff_Ind_BswM_MDGP_BswMRteMDG_CanBusOff = RTE_MODE_BswMRteMDG_CanBusOff_CAN_BusOff;
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Tx_5c9baa61, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Tx_5c9baa61, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Tx_5c9baa61, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone1J1939_Tx_5c9baa61, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_AL_PvtReport_Enabled
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_AL_PvtReport_Enabled(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Tx_Debug, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Tx_Debug, TRUE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswM_ActionList_AL_PvtReport_Disabled
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(Std_ReturnType, BSWM_CODE) BswM_ActionList_AL_PvtReport_Disabled(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Tx_Debug, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  /* PRQA S 0277, 3109, 3201, 3325 COMSETIPDU */ /* MD_BswM_0277, MD_BswM_3109, MD_BswM_3201, MD_BswM_3325 */ /*lint -e506 -e572 */
  BswM_SetIpduDMGroup(ComConf_ComIPduGroup_CIOM_oBackbone2_Tx_Debug, FALSE); /* SBSW_BSWM_IPDUGROUPVECTORCALL */
  /* PRQA L:COMSETIPDU */ /*lint +e506 +e572 */
  BswM_MarkPduGroupControlInvocation(BSWM_GROUPCONTROL_NORMAL);
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return E_OK;
}/* PRQA S 6010, 6030, 6050 */ /* MD_MSR_STPTH, MD_MSR_STCYC, MD_MSR_STCAL */

/**********************************************************************************************************************
 *  BswMRuleFunctions
 *********************************************************************************************************************/

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_SchTableStartInd_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN1SchTableStartInd_Table1. */
    if(BswM_GetLinScheduleState(0, 0u) == LinSMConf_LinSMSchedule_Table_1_2abffe93)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table1, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table1, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table1. */
        retVal = BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_Table1;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table1, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_SchTableStartInd_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN2SchTableStartInd_MSTable0. */
    if(BswM_GetLinScheduleState(1, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_9e3481d6)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable0, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable0, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable0. */
        retVal = BSWM_ID_AL_CC_AL_LIN2_SchTableStartInd_MSTable0;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable0, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN3SchTableStartInd_MSTable. */
    if(BswM_GetLinScheduleState(2, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_1692e5d6)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable. */
        retVal = BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_MSTable;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN4SchTableStartInd_MSTable. */
    if(BswM_GetLinScheduleState(3, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_f18f4341)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable. */
        retVal = BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_MSTable;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN5SchTableStartInd_MSTable. */
    if(BswM_GetLinScheduleState(4, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_294c3867)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable. */
        retVal = BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_MSTable;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_SchTableStartInd_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN1SchTableStartInd_Table2. */
    if(BswM_GetLinScheduleState(0, 0u) == LinSMConf_LinSMSchedule_Table_2_b3b6af29)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table2, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table2, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN1_SchTableStartInd_Table2. */
        retVal = BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_Table2;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_Table2, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_TableE, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN1SchTableStartInd_TableE. */
    if(BswM_GetLinScheduleState(0, 0u) == LinSMConf_LinSMSchedule_Table_e_46b96b7e)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_TableE, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_TableE, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN1_SchTableStartInd_TableE. */
        retVal = BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_TableE;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_TableE, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN1SchTableStartInd_MSTable1. */
    if(BswM_GetLinScheduleState(0, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_2a158a89)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable1, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable1, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable1. */
        retVal = BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_MSTable1;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable1, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN1SchTableStartInd_MSTable2. */
    if(BswM_GetLinScheduleState(0, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_b31cdb33)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable2, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable2, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable2. */
        retVal = BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_MSTable2;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable2, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_SchTableStartInd_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_Table0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN2SchTableStartInd_Table0. */
    if(BswM_GetLinScheduleState(1, 0u) == LinSMConf_LinSMSchedule_Table0_df24d2a0)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_Table0, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_Table0, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN2_SchTableStartInd_Table0. */
        retVal = BSWM_ID_AL_CC_AL_LIN2_SchTableStartInd_Table0;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_Table0, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_TableE, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN2SchTableStartInd_TableE. */
    if(BswM_GetLinScheduleState(1, 0u) == LinSMConf_LinSMSchedule_Table_e_dbb68a08)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_TableE, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_TableE, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN2_SchTableStartInd_TableE. */
        retVal = BSWM_ID_AL_CC_AL_LIN2_SchTableStartInd_TableE;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_TableE, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN1SchTableStartInd_MSTable. */
    if(BswM_GetLinScheduleState(0, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_03d8aeb9)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN1_SchTableStartInd_MSTable. */
        retVal = BSWM_ID_AL_CC_AL_LIN1_SchTableStartInd_MSTable;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_SchTableStartInd_MSTable, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN2SchTableStartInd_MSTable. */
    if(BswM_GetLinScheduleState(1, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_e4c5082e)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN2_SchTableStartInd_MSTable. */
        retVal = BSWM_ID_AL_CC_AL_LIN2_SchTableStartInd_MSTable;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_SchTableStartInd_MSTable, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN3SchTableStartInd_MSTable1. */
    if(BswM_GetLinScheduleState(2, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_631128e5)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable1, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable1, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable1. */
        retVal = BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_MSTable1;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable1, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN3SchTableStartInd_MSTable2. */
    if(BswM_GetLinScheduleState(2, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_fa18795f)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable2, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable2, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN3_SchTableStartInd_MSTable2. */
        retVal = BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_MSTable2;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_MSTable2, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_SchTableStartInd_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN3SchTableStartInd_Table1. */
    if(BswM_GetLinScheduleState(2, 0u) == LinSMConf_LinSMSchedule_Table_1_cbd13a3e)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table1, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table1, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table1. */
        retVal = BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_Table1;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table1, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_SchTableStartInd_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN3SchTableStartInd_Table2. */
    if(BswM_GetLinScheduleState(2, 0u) == LinSMConf_LinSMSchedule_Table_2_52d86b84)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table2, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table2, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN3_SchTableStartInd_Table2. */
        retVal = BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_Table2;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_Table2, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_TableE, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN3SchTableStartInd_TableE. */
    if(BswM_GetLinScheduleState(2, 0u) == LinSMConf_LinSMSchedule_Table_e_a7d7afd3)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_TableE, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_TableE, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN3_SchTableStartInd_TableE. */
        retVal = BSWM_ID_AL_CC_AL_LIN3_SchTableStartInd_TableE;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_SchTableStartInd_TableE, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN4SchTableStartInd_MSTable1. */
    if(BswM_GetLinScheduleState(3, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_479379d3)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable1, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable1, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable1. */
        retVal = BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_MSTable1;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable1, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN4SchTableStartInd_MSTable2. */
    if(BswM_GetLinScheduleState(3, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_de9a2869)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable2, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable2, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN4_SchTableStartInd_MSTable2. */
        retVal = BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_MSTable2;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_MSTable2, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_SchTableStartInd_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN4SchTableStartInd_Table1. */
    if(BswM_GetLinScheduleState(3, 0u) == LinSMConf_LinSMSchedule_Table_1_56dedb48)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table1, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table1, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table1. */
        retVal = BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_Table1;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table1, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_SchTableStartInd_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN4SchTableStartInd_Table2. */
    if(BswM_GetLinScheduleState(3, 0u) == LinSMConf_LinSMSchedule_Table_2_cfd78af2)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table2, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table2, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN4_SchTableStartInd_Table2. */
        retVal = BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_Table2;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_Table2, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_TableE, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN4SchTableStartInd_TableE. */
    if(BswM_GetLinScheduleState(3, 0u) == LinSMConf_LinSMSchedule_Table_e_3ad84ea5)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_TableE, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_TableE, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN4_SchTableStartInd_TableE. */
        retVal = BSWM_ID_AL_CC_AL_LIN4_SchTableStartInd_TableE;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_SchTableStartInd_TableE, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN5SchTableStartInd_MSTable2. */
    if(BswM_GetLinScheduleState(4, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_21159feb)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable2, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable2, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable2. */
        retVal = BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_MSTable2;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable2, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_MSTable1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN5SchTableStartInd_MSTable1. */
    if(BswM_GetLinScheduleState(4, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_b81cce51)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable1, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable1, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN5_SchTableStartInd_MSTable1. */
        retVal = BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_MSTable1;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_MSTable1, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_SchTableStartInd_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN5SchTableStartInd_Table1. */
    if(BswM_GetLinScheduleState(4, 0u) == LinSMConf_LinSMSchedule_Table_1_33137188)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table1, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table1, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table1. */
        retVal = BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_Table1;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table1, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_SchTableStartInd_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN5SchTableStartInd_Table2. */
    if(BswM_GetLinScheduleState(4, 0u) == LinSMConf_LinSMSchedule_Table_2_aa1a2032)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table2, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table2, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN5_SchTableStartInd_Table2. */
        retVal = BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_Table2;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_Table2, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_SchTableStartInd_TableE
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_SchTableStartInd_TableE(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_TableE, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN5SchTableStartInd_TableE. */
    if(BswM_GetLinScheduleState(4, 0u) == LinSMConf_LinSMSchedule_Table_e_5f15e465)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_TableE, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_TableE, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN5_SchTableStartInd_TableE. */
        retVal = BSWM_ID_AL_CC_AL_LIN5_SchTableStartInd_TableE;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_SchTableStartInd_TableE, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_DcmEcuReset_Execute
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_DcmEcuReset_Execute(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_Execute, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_DcmEcuReset_Execute. */
    if((BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset == RTE_MODE_DcmEcuReset_EXECUTE) && (BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Completed))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_Execute, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_Execute, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_DcmEcuReset_Execute. */
        retVal = BSWM_ID_AL_CC_AL_DcmEcuReset_Execute;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_Execute, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_DcmEcuReset_Trigger
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_DcmEcuReset_Trigger(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_Trigger, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_DcmEcuReset_Trigger_and_EcuRunState. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if(((BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_RUN) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_RUN)) && ((BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset == RTE_MODE_DcmEcuReset_HARD) || (BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset == RTE_MODE_DcmEcuReset_KEYONOFF) || (BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset == RTE_MODE_DcmEcuReset_SOFT)))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_Trigger, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_Trigger, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_DcmEcuReset_Trigger. */
        retVal = BSWM_ID_AL_CC_AL_DcmEcuReset_Trigger;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_Trigger, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_Schedule_To_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN3_Schedule_To_Table1. */
    if(Request_LIN3_ScheduleTableRequestMode_requestedMode == LIN3_TABLE1)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table1. */
      retVal = BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_Table1;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_Schedule_To_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_NULL, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN3_Schedule_To_NULL. */
    if(Request_LIN3_ScheduleTableRequestMode_requestedMode == LIN3_NULL)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_NULL. */
      retVal = BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_NULL;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_Schedule_To_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN3_Schedule_To_Table2. */
    if(Request_LIN3_ScheduleTableRequestMode_requestedMode == LIN3_TABLE2)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table2. */
      retVal = BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_Table2;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_Schedule_To_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_Table_E, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN3_Schedule_To_Table_E. */
    if(Request_LIN3_ScheduleTableRequestMode_requestedMode == LIN3_TABLE_E)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_Table_E. */
      retVal = BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_Table_E;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN3_Schedule_To_MasterReq_SlaveResp. */
    if(Request_LIN3_ScheduleTableRequestMode_requestedMode == LIN3_MasterReq_SlaveResp)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp. */
      retVal = BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN3_Schedule_To_MasterReq_SlaveResp_Table1. */
    if(Request_LIN3_ScheduleTableRequestMode_requestedMode == LIN3_MasterReq_SlaveResp_Table1)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1. */
      retVal = BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_1;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_Schedule_To_MasterReq_SlaveResp_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN3_Schedule_To_MasterReq_SlaveResp_Table2. */
    if(Request_LIN3_ScheduleTableRequestMode_requestedMode == LIN3_MasterReq_SlaveResp_Table2)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2. */
      retVal = BSWM_ID_AL_CC_AL_LIN3_ScheduleTable_to_MasterReq_SlaveResp_Table_2;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN1_Schedule_To_MasterReq_SlaveResp. */
    if(Request_LIN1_ScheduleTableRequestMode_requestedMode == LIN1_MasterReq_SlaveResp)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp. */
      retVal = BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN1_Schedule_To_MasterReq_SlaveResp_Table1. */
    if(Request_LIN1_ScheduleTableRequestMode_requestedMode == LIN1_MasterReq_SlaveResp_Table1)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1. */
      retVal = BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_1;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_MasterReq_SlaveResp_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN1_Schedule_To_MasterReq_SlaveResp_Table2. */
    if(Request_LIN1_ScheduleTableRequestMode_requestedMode == LIN1_MasterReq_SlaveResp_Table2)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2. */
      retVal = BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_MasterReq_SlaveResp_Table_2;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_Schedule_To_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_NULL, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN1_Schedule_To_NULL. */
    if(Request_LIN1_ScheduleTableRequestMode_requestedMode == LIN1_NULL)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_NULL. */
      retVal = BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_NULL;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_Schedule_To_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN1_Schedule_To_Table1. */
    if(Request_LIN1_ScheduleTableRequestMode_requestedMode == LIN1_Table1)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table1. */
      retVal = BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_Table1;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_Schedule_To_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN1_Schedule_To_Table2. */
    if(Request_LIN1_ScheduleTableRequestMode_requestedMode == LIN1_Table2)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table2. */
      retVal = BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_Table2;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_Schedule_To_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_Schedule_To_Table_E, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN1_Schedule_To_Table_E. */
    if(Request_LIN1_ScheduleTableRequestMode_requestedMode == LIN1_Table_E)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN1_ScheduleTable_to_Table_E. */
      retVal = BSWM_ID_AL_CC_AL_LIN1_ScheduleTable_to_Table_E;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN2_Schedule_To_MasterReq_SlaveResp. */
    if(Request_LIN2_ScheduleTableRequestMode_requestedMode == LIN2_MasterReq_SlaveResp)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp. */
      retVal = BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_MasterReq_SlaveResp_Table0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN2_Schedule_To_MasterReq_SlaveResp_Table0. */
    if(Request_LIN2_ScheduleTableRequestMode_requestedMode == LIN2_MasterReq_SlaveResp_TABLE0)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0. */
      retVal = BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_MasterReq_SlaveResp_Table_0;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_Schedule_To_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_NULL, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN2_Schedule_To_NULL. */
    if(Request_LIN2_ScheduleTableRequestMode_requestedMode == LIN2_NULL)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_NULL. */
      retVal = BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_NULL;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_Schedule_To_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_Table0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN2_Schedule_To_Table0. */
    if(Request_LIN2_ScheduleTableRequestMode_requestedMode == LIN2_TABLE0)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table0. */
      retVal = BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_Table0;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_Schedule_To_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_Schedule_To_Table_E, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN2_Schedule_To_Table_E. */
    if(Request_LIN2_ScheduleTableRequestMode_requestedMode == LIN2_TABLE_E)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN2_ScheduleTable_to_Table_E. */
      retVal = BSWM_ID_AL_CC_AL_LIN2_ScheduleTable_to_Table_E;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN4_Schedule_To_MasterReq_SlaveResp. */
    if(Request_LIN4_ScheduleTableRequestMode_requestedMode == LIN4_MasterReq_SlaveResp)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp. */
      retVal = BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN4_Schedule_To_MasterReq_SlaveResp_Table1. */
    if(Request_LIN4_ScheduleTableRequestMode_requestedMode == LIN4_MasterReq_SlaveResp_Table1)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1. */
      retVal = BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_1;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_MasterReq_SlaveResp_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN4_Schedule_To_MasterReq_SlaveResp_Table2. */
    if(Request_LIN4_ScheduleTableRequestMode_requestedMode == LIN4_MasterReq_SlaveResp_Table2)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2. */
      retVal = BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_MasterReq_SlaveResp_Table_2;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_Schedule_To_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_NULL, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN4_Schedule_To_NULL. */
    if(Request_LIN4_ScheduleTableRequestMode_requestedMode == LIN4_NULL)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_NULL. */
      retVal = BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_NULL;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_Schedule_To_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN4_Schedule_To_Table1. */
    if(Request_LIN4_ScheduleTableRequestMode_requestedMode == LIN4_TABLE1)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table1. */
      retVal = BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_Table1;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_Schedule_To_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN4_Schedule_To_Table2. */
    if(Request_LIN4_ScheduleTableRequestMode_requestedMode == LIN4_TABLE2)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table2. */
      retVal = BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_Table2;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_Schedule_To_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_Schedule_To_Table_E, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN4_Schedule_To_Table_E. */
    if(Request_LIN4_ScheduleTableRequestMode_requestedMode == LIN4_TABLE_E)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN4_ScheduleTable_to_Table_E. */
      retVal = BSWM_ID_AL_CC_AL_LIN4_ScheduleTable_to_Table_E;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN5_Schedule_To_MasterReq_SlaveResp. */
    if(Request_LIN5_ScheduleTableRequestMode_requestedMode == LIN5_MasterReq_SlaveResp)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp. */
      retVal = BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN5_Schedule_To_MasterReq_SlaveResp_Table1. */
    if(Request_LIN5_ScheduleTableRequestMode_requestedMode == LIN5_MasterReq_SlaveResp_Table1)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1. */
      retVal = BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_1;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_MasterReq_SlaveResp_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN5_Schedule_To_MasterReq_SlaveResp_Table2. */
    if(Request_LIN5_ScheduleTableRequestMode_requestedMode == LIN5_MasterReq_SlaveResp_Table2)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2. */
      retVal = BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_MasterReq_SlaveResp_Table_2;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_Schedule_To_NULL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_NULL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_NULL, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN5_Schedule_To_NULL. */
    if(Request_LIN5_ScheduleTableRequestMode_requestedMode == LIN5_NULL)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_NULL. */
      retVal = BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_NULL;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_Schedule_To_Table1
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_Table1(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_Table1, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN5_Schedule_To_Table1. */
    if(Request_LIN5_ScheduleTableRequestMode_requestedMode == LIN5_TABLE1)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table1. */
      retVal = BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_Table1;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_Schedule_To_Table2
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_Table2(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_Table2, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN5_Schedule_To_Table2. */
    if(Request_LIN5_ScheduleTableRequestMode_requestedMode == LIN5_TABLE2)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table2. */
      retVal = BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_Table2;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_Schedule_To_Table_E
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_Schedule_To_Table_E(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_Schedule_To_Table_E, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_LIN5_Schedule_To_Table_E. */
    if(Request_LIN5_ScheduleTableRequestMode_requestedMode == LIN5_TABLE_E)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN5_ScheduleTable_to_Table_E. */
      retVal = BSWM_ID_AL_CC_AL_LIN5_ScheduleTable_to_Table_E;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_CabSubnet_9ea693f1_RX_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CabSubnet_9ea693f1_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX_DM, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_CabSubnet_9ea693f1_RX_DM. */
    if((BswM_GetCanSMChannelState(3, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX_DM, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX_DM, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_Enable_DM;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX_DM, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX_DM, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_Disable_DM;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_J1939NmIndication_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_EQ_NM_STATE_NORMAL_OPERATION. */
    if(BswM_GetJ1939NmState(0, 0u) == NM_STATE_NORMAL_OPERATION)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Online;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_Offline;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_Backbone2_78967e2c_RX_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone2_78967e2c_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX_DM, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_Backbone2_78967e2c_RX_DM. */
    if((BswM_GetCanSMChannelState(1, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX_DM, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX_DM, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Enable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_Enable_DM;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX_DM, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX_DM, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_Disable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_Disable_DM;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_LIN02_c2d7c6f3
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN02_c2d7c6f3(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN02_c2d7c6f3, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_LIN02_c2d7c6f3. */
    if(BswM_GetLinSMState(2, 0u) == LINSM_BSWM_FULL_COM)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN02_c2d7c6f3, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN02_c2d7c6f3, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Enable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN02_c2d7c6f3_Enable;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN02_c2d7c6f3, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN02_c2d7c6f3, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN02_c2d7c6f3_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN02_c2d7c6f3_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_RX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_SecuritySubnet_e7a0ee54_RX. */
    if((BswM_GetCanSMChannelState(5, 0u) != CANSM_BSWM_NO_COMMUNICATION) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_RX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_RX_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX_DM, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_SecuritySubnet_e7a0ee54_RX_DM. */
    if((BswM_GetCanSMChannelState(5, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX_DM, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX_DM, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Enable_DM;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX_DM, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_RX_DM, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_Disable_DM;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_FMSNet_fce1aae5_TX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_TX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_FMSNet_fce1aae5_TX. */
    if((BswM_GetCanSMChannelState(4, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(4, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(4, 0u) != DCM_ENABLE_RX_DISABLE_TX_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(4, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_TX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_TX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_TX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_TX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_TX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_TX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_TX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_Backbone2_78967e2c_RX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone2_78967e2c_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_Backbone2_78967e2c_RX. */
    if((BswM_GetCanSMChannelState(1, 0u) != CANSM_BSWM_NO_COMMUNICATION) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_RX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_RX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_RX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_RX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_J1939NmIndication_CN_FMSNet_fce1aae5_CIOM_4d5cd289_EQ_NM_STATE_NORMAL_OPERATION. */
    if(BswM_GetJ1939NmState(1, 0u) == NM_STATE_NORMAL_OPERATION)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Online;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_Offline;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_CabSubnet_9ea693f1_RX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CabSubnet_9ea693f1_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_CabSubnet_9ea693f1_RX. */
    if((BswM_GetCanSMChannelState(3, 0u) != CANSM_BSWM_NO_COMMUNICATION) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_RX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_RX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_RX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_LIN03_b5d0f665
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN03_b5d0f665(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN03_b5d0f665, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_LIN03_b5d0f665. */
    if(BswM_GetLinSMState(3, 0u) == LINSM_BSWM_FULL_COM)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN03_b5d0f665, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN03_b5d0f665, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Enable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN03_b5d0f665_Enable;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN03_b5d0f665, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN03_b5d0f665, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN03_b5d0f665_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN03_b5d0f665_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_FMSNet_fce1aae5_RX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_FMSNet_fce1aae5_RX. */
    if((BswM_GetCanSMChannelState(4, 0u) != CANSM_BSWM_NO_COMMUNICATION) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_RX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_RX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_RX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_LIN01_5bde9749
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN01_5bde9749(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN01_5bde9749, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_LIN01_5bde9749. */
    if(BswM_GetLinSMState(1, 0u) == LINSM_BSWM_FULL_COM)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN01_5bde9749, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN01_5bde9749, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Enable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN01_5bde9749_Enable;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN01_5bde9749, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN01_5bde9749, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN01_5bde9749_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN01_5bde9749_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_LIN04_2bb463c6
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN04_2bb463c6(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN04_2bb463c6, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_LIN04_2bb463c6. */
    if(BswM_GetLinSMState(4, 0u) == LINSM_BSWM_FULL_COM)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN04_2bb463c6, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN04_2bb463c6, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Enable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN04_2bb463c6_Enable;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN04_2bb463c6, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN04_2bb463c6, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN04_2bb463c6_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN04_2bb463c6_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_RX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_Backbone1J1939_0b1f4bae_RX. */
    if((BswM_GetCanSMChannelState(0, 0u) != CANSM_BSWM_NO_COMMUNICATION) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_RX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_FMSNet_fce1aae5_RX_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX_DM, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_FMSNet_fce1aae5_RX_DM. */
    if((BswM_GetCanSMChannelState(4, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX_DM, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX_DM, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Enable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_Enable_DM;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX_DM, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_RX_DM, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_Disable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_Disable_DM;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_RX_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX_DM, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_Backbone1J1939_0b1f4bae_RX_DM. */
    if((BswM_GetCanSMChannelState(0, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_ENABLE_TX_NM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM_NM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX_DM, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX_DM, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Enable_DM;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX_DM, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_RX_DM, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_Disable_DM;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_LIN00_2cd9a7df
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN00_2cd9a7df(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN00_2cd9a7df, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_LIN00_2cd9a7df. */
    if(BswM_GetLinSMState(0, 0u) == LINSM_BSWM_FULL_COM)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN00_2cd9a7df, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN00_2cd9a7df, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Enable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN00_2cd9a7df_Enable;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN00_2cd9a7df, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN00_2cd9a7df, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN00_2cd9a7df_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN00_2cd9a7df_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX. */
    if((BswM_GetCanSMChannelState(4, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetJ1939NmState(1, 0u) != NM_STATE_OFFLINE) && (BswM_GetDcmComState(4, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(4, 0u) != DCM_ENABLE_RX_DISABLE_TX_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(4, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM_NM) && (BswM_GetDcmComState(4, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_FMSNet_fce1aae5_CIOM_4d5cd289_TX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_RunToPostRun
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_RunToPostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_RunToPostRun, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_RunToPostRunTransition. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if((BswM_GetGenericState(4, 0u) != BSWM_GENERICVALUE_ISSM_RunRequest_ISSM_RUN_REQUEST) && ((BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_RUN) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_RUN)) && (((BswM_GetComMChannelState(0, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(7, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(9, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(3, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(8, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(5, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(1, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(6, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(4, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(13, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(11, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(10, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(12, 0u) == COMM_NO_COMMUNICATION) && (BswM_GetComMChannelState(2, 0u) == COMM_NO_COMMUNICATION)) && (BswM_GetTimerState(2, 0u) == BSWM_TIMER_EXPIRED)))
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_ExitRun. */
      retVal = BSWM_ID_AL_ESH_AL_ExitRun;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_RunToPostRunNested
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_RunToPostRunNested(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_RunToPostRunNested, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_ComMNoPendingRequests. */
    if(BswM_GetGenericState(1, 0u) == BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_NO_REQUEST)
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_RunToPostRun. */
      retVal = BSWM_ID_AL_ESH_AL_RunToPostRun;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_WaitToShutdown
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_WaitToShutdown(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_WaitToShutdown, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_WaitForNvMToShutdown. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if((BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_WAIT_FOR_NVM) && (((BswM_GetNvMJobState(0, 0u) != NVM_REQ_PENDING) && (MemIf_GetStatus(MEMIF_BROADCAST_ID) == MEMIF_IDLE)) || (BswM_GetTimerState(1, 0u) != BSWM_TIMER_STARTED)) && ((EcuM_GetValidatedWakeupEvents() == 0u) && (BswM_GetGenericState(1, 0u) == BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_NO_REQUEST)))
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_WaitForNvMToShutdown. */
      retVal = BSWM_ID_AL_ESH_AL_WaitForNvMToShutdown;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_WakeupToPrep
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_WakeupToPrep(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_WakeupToPrep, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_WakeupToPrepShutdown. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if((BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_WAKEUP) && ((BswM_GetNvMJobState(0, 0u) != NVM_REQ_PENDING) || (BswM_GetTimerState(0, 0u) != BSWM_TIMER_STARTED)) && (EcuM_GetPendingWakeupEvents() == 0u) && ((EcuM_GetValidatedWakeupEvents() == 0u) && (BswM_GetGenericState(1, 0u) == BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_NO_REQUEST)) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_WAKEUP))
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_WakeupToPrep. */
      retVal = BSWM_ID_AL_ESH_AL_WakeupToPrep;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_WaitToWakeup
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_WaitToWakeup(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_WaitToWakeup, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_WaitForNvMToWakeup. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if((BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset != RTE_MODE_DcmEcuReset_EXECUTE) && (BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_WAIT_FOR_NVM) && ((EcuM_GetValidatedWakeupEvents() != 0u) || (BswM_GetGenericState(1, 0u) == BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_PENDING_REQUEST)))
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_WaitForNvMWakeup. */
      retVal = BSWM_ID_AL_ESH_AL_WaitForNvMWakeup;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_WakeupToRun
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_WakeupToRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_WakeupToRun, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_WakeupToRun. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if((BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_WAKEUP) && ((EcuM_GetValidatedWakeupEvents() != 0u) || (BswM_GetGenericState(1, 0u) == BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_PENDING_REQUEST)) && ((BswM_GetNvMJobState(0, 0u) != NVM_REQ_PENDING) || (BswM_GetTimerState(0, 0u) != BSWM_TIMER_STARTED)) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_WAKEUP))
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_WakeupToRun. */
      retVal = BSWM_ID_AL_ESH_AL_WakeupToRun;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_InitToWakeup
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_InitToWakeup(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_InitToWakeup, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_InitToWakeup. */
    if(BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_INIT)
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_InitToWakeup. */
      retVal = BSWM_ID_AL_ESH_AL_InitToWakeup;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_PostRunNested
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_PostRunNested(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_PostRunNested, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_PostRunToRun. */
    if((EcuM_GetValidatedWakeupEvents() != 0u) || (BswM_GetGenericState(1, 0u) == BSWM_GENERICVALUE_ESH_ComMPendingRequests_ESH_COMM_PENDING_REQUEST))
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_PostRunToRun. */
      retVal = BSWM_ID_AL_ESH_AL_PostRunToRun;
    }
    else
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_PostRunToPrepShutdown. */
      retVal = BSWM_ID_AL_ESH_AL_PostRunToPrepShutdown;
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_PostRun
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_PostRun(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_PostRun, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_PostRun. */
    if((BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_POST_RUN) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_POSTRUN))
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_ExitPostRun. */
      retVal = BSWM_ID_AL_ESH_AL_ExitPostRun;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_PrepToWait
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_PrepToWait(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_PrepToWait, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_PrepShutdownToWaitForNvM. */
    if((BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_PREP_SHUTDOWN) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_SHUTDOWN))
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_PrepShutdownToWaitForNvM. */
      retVal = BSWM_ID_AL_ESH_AL_PrepShutdownToWaitForNvM;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_ESH_DemInit
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_ESH_DemInit(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_ESH_DemInit, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression ESH_LE_DemNotInitialized. */
    if(BswM_GetGenericState(2, 0u) == BSWM_GENERICVALUE_ESH_DemInitStatus_DEM_NOT_INITIALIZED)
    {
      /* Return conditional action list BswM_ActionList_ESH_AL_DemInit. */
      retVal = BSWM_ID_AL_ESH_AL_DemInit;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_TX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_SecuritySubnet_e7a0ee54_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_TX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_SecuritySubnet_e7a0ee54_TX. */
    if((BswM_GetCanSMChannelState(5, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(5, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(5, 0u) != DCM_ENABLE_RX_DISABLE_TX_NM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(5, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM_NM) && (BswM_GetDcmComState(5, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_TX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_TX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_TX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_SecuritySubnet_e7a0ee54_TX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_SecuritySubnet_e7a0ee54_TX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_CabSubnet_9ea693f1_TX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CabSubnet_9ea693f1_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_TX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_CabSubnet_9ea693f1_TX. */
    if((BswM_GetCanSMChannelState(3, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(3, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(3, 0u) != DCM_ENABLE_RX_DISABLE_TX_NM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(3, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM_NM) && (BswM_GetDcmComState(3, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_TX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_TX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_TX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_TX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CabSubnet_9ea693f1_TX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_CabSubnet_9ea693f1_TX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_Backbone2_78967e2c_TX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone2_78967e2c_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_TX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_Backbone2_78967e2c_TX. */
    if((BswM_GetCanSMChannelState(1, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(1, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(1, 0u) != DCM_ENABLE_RX_DISABLE_TX_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(1, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_TX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_TX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_TX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_TX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone2_78967e2c_TX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone2_78967e2c_TX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone2_78967e2c_TX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN1_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN1_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN1_ScheduleTableEndNotification, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN1SchTableEndNotif. */
    if((BswM_GetLinScheduleEndState(0, 0u) == LinSMConf_LinSMSchedule_Table_2_b3b6af29) || (BswM_GetLinScheduleEndState(0, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_03d8aeb9) || (BswM_GetLinScheduleEndState(0, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_b31cdb33) || (BswM_GetLinScheduleEndState(0, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_2a158a89) || (BswM_GetLinScheduleEndState(0, 0u) == LinSMConf_LinSMSchedule_Table_1_2abffe93) || (BswM_GetLinScheduleEndState(0, 0u) == LinSMConf_LinSMSchedule_Table_e_46b96b7e) || (BswM_GetLinScheduleEndState(0, 0u) == LinSMConf_LinSMSchedule_CHNL_45618847_32a17f26))
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN1_ScheduleTableEndNotification. */
      retVal = BSWM_ID_AL_CC_AL_LIN1_ScheduleTableEndNotification;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN2_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN2_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN2_ScheduleTableEndNotification, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN2SchTableEndNotif. */
    if((BswM_GetLinScheduleEndState(1, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_e4c5082e) || (BswM_GetLinScheduleEndState(1, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_9e3481d6) || (BswM_GetLinScheduleEndState(1, 0u) == LinSMConf_LinSMSchedule_Table0_df24d2a0) || (BswM_GetLinScheduleEndState(1, 0u) == LinSMConf_LinSMSchedule_Table_e_dbb68a08) || (BswM_GetLinScheduleEndState(1, 0u) == LinSMConf_LinSMSchedule_CHNL_8e3d5be2_a66eb9f7))
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN2_ScheduleTableEndNotification. */
      retVal = BSWM_ID_AL_CC_AL_LIN2_ScheduleTableEndNotification;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN3_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN3_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN3_ScheduleTableEndNotification, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN3SchTableEndNotif. */
    if((BswM_GetLinScheduleEndState(2, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_1692e5d6) || (BswM_GetLinScheduleEndState(2, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_631128e5) || (BswM_GetLinScheduleEndState(2, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_fa18795f) || (BswM_GetLinScheduleEndState(2, 0u) == LinSMConf_LinSMSchedule_Table_1_cbd13a3e) || (BswM_GetLinScheduleEndState(2, 0u) == LinSMConf_LinSMSchedule_Table_2_52d86b84) || (BswM_GetLinScheduleEndState(2, 0u) == LinSMConf_LinSMSchedule_Table_e_a7d7afd3) || (BswM_GetLinScheduleEndState(2, 0u) == LinSMConf_LinSMSchedule_CHNL_08a9294c_f4be7c2c))
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN3_ScheduleTableEndNotification. */
      retVal = BSWM_ID_AL_CC_AL_LIN3_ScheduleTableEndNotification;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN4_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN4_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN4_ScheduleTableEndNotification, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN4SchTableEndNotif. */
    if((BswM_GetLinScheduleEndState(3, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_f18f4341) || (BswM_GetLinScheduleEndState(3, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_479379d3) || (BswM_GetLinScheduleEndState(3, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_de9a2869) || (BswM_GetLinScheduleEndState(3, 0u) == LinSMConf_LinSMSchedule_Table_1_56dedb48) || (BswM_GetLinScheduleEndState(3, 0u) == LinSMConf_LinSMSchedule_Table_2_cfd78af2) || (BswM_GetLinScheduleEndState(3, 0u) == LinSMConf_LinSMSchedule_Table_e_3ad84ea5) || (BswM_GetLinScheduleEndState(3, 0u) == LinSMConf_LinSMSchedule_CHNL_c3f5fae9_fb3b3248))
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN4_ScheduleTableEndNotification. */
      retVal = BSWM_ID_AL_CC_AL_LIN4_ScheduleTableEndNotification;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN5_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN5_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN5_ScheduleTableEndNotification, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_LIN5SchTableEndNotif. */
    if((BswM_GetLinScheduleEndState(4, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_294c3867) || (BswM_GetLinScheduleEndState(4, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_1_b81cce51) || (BswM_GetLinScheduleEndState(4, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table_2_21159feb) || (BswM_GetLinScheduleEndState(4, 0u) == LinSMConf_LinSMSchedule_Table_1_33137188) || (BswM_GetLinScheduleEndState(4, 0u) == LinSMConf_LinSMSchedule_Table_2_aa1a2032) || (BswM_GetLinScheduleEndState(4, 0u) == LinSMConf_LinSMSchedule_Table_e_5f15e465) || (BswM_GetLinScheduleEndState(4, 0u) == LinSMConf_LinSMSchedule_CHNL_def0ca51_faa54df8))
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN5_ScheduleTableEndNotification. */
      retVal = BSWM_ID_AL_CC_AL_LIN5_ScheduleTableEndNotification;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_LIN06_c5ba02ea
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN06_c5ba02ea(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN06_c5ba02ea, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_LIN06_c5ba02ea. */
    if(BswM_GetLinSMState(6, 0u) == LINSM_BSWM_FULL_COM)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN06_c5ba02ea, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN06_c5ba02ea, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Enable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN06_c5ba02ea_Enable;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN06_c5ba02ea, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN06_c5ba02ea, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN06_c5ba02ea_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN06_c5ba02ea_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_LIN05_5cb35350
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN05_5cb35350(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN05_5cb35350, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_LIN05_5cb35350. */
    if(BswM_GetLinSMState(5, 0u) == LINSM_BSWM_FULL_COM)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN05_5cb35350, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN05_5cb35350, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Enable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN05_5cb35350_Enable;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN05_5cb35350, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN05_5cb35350, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN05_5cb35350_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN05_5cb35350_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_LIN07_b2bd327c
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_LIN07_b2bd327c(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN07_b2bd327c, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_LIN07_b2bd327c. */
    if(BswM_GetLinSMState(7, 0u) == LINSM_BSWM_FULL_COM)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN07_b2bd327c, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN07_b2bd327c, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Enable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN07_b2bd327c_Enable;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_LIN07_b2bd327c, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_LIN07_b2bd327c, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_LIN07_b2bd327c_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_LIN07_b2bd327c_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_CAN6_b040c073_RX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CAN6_b040c073_RX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_CAN6_b040c073_RX. */
    if((BswM_GetCanSMChannelState(2, 0u) != CANSM_BSWM_NO_COMMUNICATION) && (BswM_GetDcmComState(2, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(2, 0u) != DCM_DISABLE_RX_TX_NORMAL))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_RX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CAN6_b040c073_RX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_RX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_CAN6_b040c073_TX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CAN6_b040c073_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_TX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_CAN6_b040c073_TX. */
    if((BswM_GetCanSMChannelState(2, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(2, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM) && (BswM_GetDcmComState(2, 0u) != DCM_DISABLE_RX_TX_NORMAL))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_TX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_TX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_TX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_TX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_TX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CAN6_b040c073_TX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_TX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_CAN6_b040c073_RX_DM
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_CAN6_b040c073_RX_DM(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX_DM, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_CAN6_b040c073_RX_DM. */
    if((BswM_GetCanSMChannelState(2, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(2, 0u) != DCM_DISABLE_RX_ENABLE_TX_NORM) && (BswM_GetDcmComState(2, 0u) != DCM_DISABLE_RX_TX_NORMAL))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX_DM, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX_DM, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CAN6_b040c073_Enable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_Enable_DM;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX_DM, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_CAN6_b040c073_RX_DM, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_CAN6_b040c073_Disable_DM. */
        retVal = BSWM_ID_AL_CC_AL_CN_CAN6_b040c073_Disable_DM;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN6_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_ScheduleTableEndNotification, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN6_ScheduleTableEndNotification. */
    if((BswM_GetLinScheduleEndState(5, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_45fec21a) || (BswM_GetLinScheduleEndState(5, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_ce519ef0) || (BswM_GetLinScheduleEndState(5, 0u) == LinSMConf_LinSMSchedule_Table0_1f9db836))
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN6_ScheduleTableEndNotification. */
      retVal = BSWM_ID_AL_CC_AL_LIN6_ScheduleTableEndNotification;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN7_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_ScheduleTableEndNotification, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN7_ScheduleTableEndNotification. */
    if((BswM_GetLinScheduleEndState(6, 0u) == LinSMConf_LinSMSchedule_Table0_a257d4f8) || (BswM_GetLinScheduleEndState(6, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_3c067308) || (BswM_GetLinScheduleEndState(6, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_1ee9730f))
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN7_ScheduleTableEndNotification. */
      retVal = BSWM_ID_AL_CC_AL_LIN7_ScheduleTableEndNotification;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN8_ScheduleTableEndNotification
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_ScheduleTableEndNotification(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_ScheduleTableEndNotification, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN8_ScheduleTableEndNotification. */
    if((BswM_GetLinScheduleEndState(7, 0u) == LinSMConf_LinSMSchedule_Table0_7fc10d7d) || (BswM_GetLinScheduleEndState(7, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_281be3fc) || (BswM_GetLinScheduleEndState(7, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_db1bd59f))
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN8_ScheduleTableEndNotification. */
      retVal = BSWM_ID_AL_CC_AL_LIN8_ScheduleTableEndNotification;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN6_SchTableStartInd_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN6_SchTableStartInd_MSTable0. */
    if(BswM_GetLinScheduleState(5, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_45fec21a)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable0, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable0, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable0. */
        retVal = BSWM_ID_AL_CC_AL_LIN6_SchTableStartInd_MSTable0;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable0, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN6_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN6_SchTableStartInd_MSTable. */
    if(BswM_GetLinScheduleState(5, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_ce519ef0)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN6_SchTableStartInd_MSTable. */
        retVal = BSWM_ID_AL_CC_AL_LIN6_SchTableStartInd_MSTable;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_MSTable, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN6_SchTableStartInd_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_Table0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN6_SchTableStartInd_Table0. */
    if(BswM_GetLinScheduleState(5, 0u) == LinSMConf_LinSMSchedule_Table0_1f9db836)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_Table0, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_Table0, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN6_SchTableStartInd_Table0. */
        retVal = BSWM_ID_AL_CC_AL_LIN6_SchTableStartInd_Table0;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_SchTableStartInd_Table0, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN7_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN7_SchTableStartInd_MSTable. */
    if(BswM_GetLinScheduleState(6, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_3c067308)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable. */
        retVal = BSWM_ID_AL_CC_AL_LIN7_SchTableStartInd_MSTable;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN8_SchTableStartInd_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_SchTableStartInd_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN8_SchTableStartInd_MSTable. */
    if(BswM_GetLinScheduleState(7, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_db1bd59f)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable. */
        retVal = BSWM_ID_AL_CC_AL_LIN8_SchTableStartInd_MSTable;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN7_SchTableStartInd_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN7_SchTableStartInd_MSTable0. */
    if(BswM_GetLinScheduleState(6, 0u) == LinSMConf_LinSMSchedule_Table0_a257d4f8)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable0, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable0, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN7_SchTableStartInd_MSTable0. */
        retVal = BSWM_ID_AL_CC_AL_LIN7_SchTableStartInd_MSTable0;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_MSTable0, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN8_SchTableStartInd_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_SchTableStartInd_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN8_SchTableStartInd_MSTable0. */
    if(BswM_GetLinScheduleState(7, 0u) == LinSMConf_LinSMSchedule_MasterReq_SlaveResp_Table0_281be3fc)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable0, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable0, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN8_SchTableStartInd_MSTable0. */
        retVal = BSWM_ID_AL_CC_AL_LIN8_SchTableStartInd_MSTable0;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_MSTable0, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN7_SchTableStartInd_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_Table0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN7_SchTableStartInd_Table0. */
    if(BswM_GetLinScheduleState(6, 0u) == LinSMConf_LinSMSchedule_Table0_a257d4f8)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_Table0, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_Table0, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN7_SchTableStartInd_Table0. */
        retVal = BSWM_ID_AL_CC_AL_LIN7_SchTableStartInd_Table0;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_SchTableStartInd_Table0, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN8_SchTableStartInd_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_SchTableStartInd_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_Table0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN8_SchTableStartInd_Table0. */
    if(BswM_GetLinScheduleState(7, 0u) == LinSMConf_LinSMSchedule_Table0_7fc10d7d)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_Table0, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_Table0, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_LIN8_SchTableStartInd_Table0. */
        retVal = BSWM_ID_AL_CC_AL_LIN8_SchTableStartInd_Table0;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_SchTableStartInd_Table0, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN6_Schedule_To_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_Schedule_To_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_Schedule_To_Table0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN6_Schedule_To_Table0. */
    if(Request_LIN6_ScheduleTableRequestMode_requestedMode == LIN6_TABLE0)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_Table0. */
      retVal = BSWM_ID_AL_CC_AL_LIN6_ScheduleTable_to_Table0;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN7_Schedule_To_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_Schedule_To_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_Schedule_To_Table0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN7_Schedule_To_Table0. */
    if(Request_LIN7_ScheduleTableRequestMode_requestedMode == LIN7_TABLE0)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_Table0. */
      retVal = BSWM_ID_AL_CC_AL_LIN7_ScheduleTable_to_Table0;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN8_Schedule_To_Table0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_Schedule_To_Table0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_Schedule_To_Table0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN8_Schedule_To_Table0. */
    if(Request_LIN8_ScheduleTableRequestMode_requestedMode == LIN8_TABLE0)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_Table0. */
      retVal = BSWM_ID_AL_CC_AL_LIN8_ScheduleTable_to_Table0;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN6_Schedule_To_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_Schedule_To_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_Schedule_To_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN6_Schedule_To_MSTable. */
    if(Request_LIN6_ScheduleTableRequestMode_requestedMode == LIN6_MasterReq_SlaveResp)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable. */
      retVal = BSWM_ID_AL_CC_AL_LIN6_ScheduleTable_to_MSTable;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN7_Schedule_To_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_Schedule_To_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_Schedule_To_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN7_Schedule_To_MSTable. */
    if(Request_LIN7_ScheduleTableRequestMode_requestedMode == LIN7_MasterReq_SlaveResp)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable. */
      retVal = BSWM_ID_AL_CC_AL_LIN7_ScheduleTable_to_MSTable;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN8_Schedule_To_MSTable
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_Schedule_To_MSTable(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_Schedule_To_MSTable, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN8_Schedule_To_MSTable. */
    if(Request_LIN8_ScheduleTableRequestMode_requestedMode == LIN8_MasterReq_SlaveResp)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable. */
      retVal = BSWM_ID_AL_CC_AL_LIN8_ScheduleTable_to_MSTable;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN6_Schedule_To_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN6_Schedule_To_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN6_Schedule_To_MSTable0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN6_Schedule_To_MSTable0. */
    if(Request_LIN6_ScheduleTableRequestMode_requestedMode == LIN6_MasterReq_SlaveResp_Table0)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN6_ScheduleTable_to_MSTable0. */
      retVal = BSWM_ID_AL_CC_AL_LIN6_ScheduleTable_to_MSTable0;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN7_Schedule_To_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN7_Schedule_To_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN7_Schedule_To_MSTable0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN7_Schedule_To_MSTable0. */
    if(Request_LIN7_ScheduleTableRequestMode_requestedMode == LIN7_MasterReq_SlaveResp_Table0)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN7_ScheduleTable_to_MSTable0. */
      retVal = BSWM_ID_AL_CC_AL_LIN7_ScheduleTable_to_MSTable0;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_LIN8_Schedule_To_MSTable0
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_LIN8_Schedule_To_MSTable0(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_LIN8_Schedule_To_MSTable0, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_LIN8_Schedule_To_MSTable0. */
    if(Request_LIN8_ScheduleTableRequestMode_requestedMode == LIN8_MasterReq_SlaveResp_Table0)
    {
      /* Return conditional action list BswM_ActionList_CC_AL_LIN8_ScheduleTable_to_MSTable0. */
      retVal = BSWM_ID_AL_CC_AL_LIN8_ScheduleTable_to_MSTable0;
    }
    /* No false action list configured. */
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_DCMResetProcess_Started
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_DCMResetProcess_Started(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_DCMResetProcess_Started, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_DCMResetProcess_Started. */
    if(BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Started)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_DCMResetProcess_Started, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_DCMResetProcess_Started, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_DCMResetProcess_Started. */
        retVal = BSWM_ID_AL_CC_AL_DCMResetProcess_Started;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_DCMResetProcess_Started, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_DCMResetProcess_InProgress
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_DCMResetProcess_InProgress(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_DCMResetProcess_InProgress, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_DCMResetProcess_InProgress. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if((BswM_GetGenericState(0, 0u) == BSWM_GENERICVALUE_DCMResetProcess_ResetProcess_Inprogress) && (((BswM_GetNvMJobState(0, 0u) != NVM_REQ_PENDING) && (MemIf_GetStatus(MEMIF_BROADCAST_ID) == MEMIF_IDLE)) || (BswM_GetTimerState(1, 0u) != BSWM_TIMER_STARTED)))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_DCMResetProcess_InProgress, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_DCMResetProcess_InProgress, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_DCMResetProcess_InProgress. */
        retVal = BSWM_ID_AL_CC_AL_DCMResetProcess_InProgress;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_DCMResetProcess_InProgress, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_DcmEcuReset_JumpToBTL
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_DcmEcuReset_JumpToBTL(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_JumpToBTL, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_DcmEcuReset_JumpBTL_and_EcuRunState. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if(((BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_RUN) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_RUN)) && ((BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset == RTE_MODE_DcmEcuReset_JUMPTOBOOTLOADER) || (BswM_Mode_Notification_SwcModeNotification_DcmEcuReset_DcmEcuReset == RTE_MODE_DcmEcuReset_JUMPTOSYSSUPPLIERBOOTLOADER)))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_JumpToBTL, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_JumpToBTL, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_DcmEcuReset_JumpToBTL. */
        retVal = BSWM_ID_AL_CC_AL_DcmEcuReset_JumpToBTL;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_DcmEcuReset_JumpToBTL, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_NvmWriteAll_Request
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_NvmWriteAll_Request(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_NvmWriteAll_Request, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_NvmWriteAll_Requested. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if((Request_SwcModeRequest_NvmWriteAllRequest_requestedMode == NvmWriteAll_Request) && ((BswM_GetGenericState(3, 0u) == BSWM_GENERICVALUE_ESH_State_ESH_PREP_SHUTDOWN) && (BswM_Mode_Notification_ESH_ModeNotification_BswM_MDGP_ESH_Mode == RTE_MODE_ESH_Mode_SHUTDOWN)) && (BswM_GetTimerState(1, 0u) != BSWM_TIMER_STARTED))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_NvmWriteAll_Request, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_NvmWriteAll_Request, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_PrepShutdown_NvmWriteAll. */
        retVal = BSWM_ID_AL_CC_AL_PrepShutdown_NvmWriteAll;
      }
    }
    else
    {
      BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_NvmWriteAll_Request, BSWM_FALSE, partitionIdx);
      /* No false action list configured. */
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_BB1_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_BB1_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_BB1_BusOff_Indication, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_BB1_BusOff_Indication. */
    if(BswM_GetCanSMChannelState(0, 0u) == CANSM_BSWM_BUS_OFF)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_BB1_BusOff_Indication, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_BB1_BusOff_Indication, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_True_AL_BB1_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_True_AL_BB1_BusOff_Indication;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_BB1_BusOff_Indication, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_BB1_BusOff_Indication, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_False_AL_BB1_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_False_AL_BB1_BusOff_Indication;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_BB2_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_BB2_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_BB2_BusOff_Indication, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_BB2_BusOff_Indication. */
    if(BswM_GetCanSMChannelState(1, 0u) == CANSM_BSWM_BUS_OFF)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_BB2_BusOff_Indication, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_BB2_BusOff_Indication, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_True_AL_BB2_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_True_AL_BB2_BusOff_Indication;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_BB2_BusOff_Indication, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_BB2_BusOff_Indication, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_False_AL_BB2_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_False_AL_BB2_BusOff_Indication;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_CAN6_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_CAN6_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_CAN6_BusOff_Indication, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_CAN6_BusOff_Indication. */
    if(BswM_GetCanSMChannelState(2, 0u) == CANSM_BSWM_BUS_OFF)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_CAN6_BusOff_Indication, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_CAN6_BusOff_Indication, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_True_AL_CAN6_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_True_AL_CAN6_BusOff_Indication;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_CAN6_BusOff_Indication, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_CAN6_BusOff_Indication, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_False_AL_CAN6_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_False_AL_CAN6_BusOff_Indication;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_CabSubnet_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_CabSubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_CabSubnet_BusOff_Indication, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_CabSubnet_BusOff_Indication. */
    if(BswM_GetCanSMChannelState(3, 0u) == CANSM_BSWM_BUS_OFF)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_CabSubnet_BusOff_Indication, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_CabSubnet_BusOff_Indication, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_True_AL_CabSubnet_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_True_AL_CabSubnet_BusOff_Indication;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_CabSubnet_BusOff_Indication, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_CabSubnet_BusOff_Indication, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_False_AL_CabSubnet_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_False_AL_CabSubnet_BusOff_Indication;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_FMSNet_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_FMSNet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_FMSNet_BusOff_Indication, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_FMSNet_BusOff_Indication. */
    if(BswM_GetCanSMChannelState(4, 0u) == CANSM_BSWM_BUS_OFF)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_FMSNet_BusOff_Indication, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_FMSNet_BusOff_Indication, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_True_AL_FMSNet_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_True_AL_FMSNet_BusOff_Indication;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_FMSNet_BusOff_Indication, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_FMSNet_BusOff_Indication, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_False_AL_FMSNet_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_False_AL_FMSNet_BusOff_Indication;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_SecuritySubnet_BusOff_Indication
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_SecuritySubnet_BusOff_Indication(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_SecuritySubnet_BusOff_Indication, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_CC_Rule_SecuritySubnet_BusOff_Indication. */
    if(BswM_GetCanSMChannelState(5, 0u) == CANSM_BSWM_BUS_OFF)
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_SecuritySubnet_BusOff_Indication, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_SecuritySubnet_BusOff_Indication, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_True_AL_SecuritySubnet_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_True_AL_SecuritySubnet_BusOff_Indication;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_SecuritySubnet_BusOff_Indication, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_SecuritySubnet_BusOff_Indication, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_False_AL_SecuritySubnet_BusOff_Indication. */
        retVal = BSWM_ID_AL_CC_False_AL_SecuritySubnet_BusOff_Indication;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression CC_LE_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX. */
    if((BswM_GetCanSMChannelState(0, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetJ1939NmState(0, 0u) != NM_STATE_OFFLINE) && (BswM_GetDcmComState(0, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(0, 0u) != DCM_ENABLE_RX_DISABLE_TX_NM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(0, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM_NM) && (BswM_GetDcmComState(0, 0u) != DCM_DISABLE_RX_TX_NORM_NM))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_EnableNoinit;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable. */
        retVal = BSWM_ID_AL_CC_AL_CN_Backbone1J1939_0b1f4bae_CIOM_4d5cd289_TX_Disable;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}

/**********************************************************************************************************************
 *  BswM_Rule_CC_Rule_PvtReportCtrl
 *********************************************************************************************************************/
BSWM_LOCAL FUNC(BswM_HandleType, BSWM_CODE) BswM_Rule_CC_Rule_PvtReportCtrl(BswM_PCPartitionConfigIdxOfPartitionIdentifiersType partitionIdx)
{
  BswM_HandleType retVal = BSWM_NO_ACTIONLIST(partitionIdx);
  if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_PvtReportCtrl, partitionIdx) != BSWM_DEACTIVATED )
  {
    /* Evaluate logical expression LE_PvtReportCtrl_Enabled. */ /* PRQA S 3415 1 */ /* MD_BSWM_3415 */
    if((Request_SwcModeRequest_PvtReportCtrl_requestedMode == PvtReport_Enabled) && ((BswM_GetCanSMChannelState(1, 0u) == CANSM_BSWM_FULL_COMMUNICATION) && (BswM_GetDcmComState(1, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NORMAL) && (BswM_GetDcmComState(1, 0u) != DCM_ENABLE_RX_DISABLE_TX_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NM) && (BswM_GetDcmComState(1, 0u) != DCM_ENABLE_RX_DISABLE_TX_NORM_NM) && (BswM_GetDcmComState(1, 0u) != DCM_DISABLE_RX_TX_NORM_NM)))
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_PvtReportCtrl, partitionIdx) != BSWM_TRUE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_PvtReportCtrl, BSWM_TRUE, partitionIdx);
        /* Return triggered action list BswM_ActionList_AL_PvtReport_Enabled. */
        retVal = BSWM_ID_AL_AL_PvtReport_Enabled;
      }
    }
    else
    {
      if( BswM_GetRuleStates(BSWM_ID_RULE_CC_Rule_PvtReportCtrl, partitionIdx) != BSWM_FALSE ) /* COV_BSWM_TRIGGEREDRULEEXECUTION */
      {
        BswM_UpdateRuleStates(BSWM_ID_RULE_CC_Rule_PvtReportCtrl, BSWM_FALSE, partitionIdx);
        /* Return triggered action list BswM_ActionList_AL_PvtReport_Disabled. */
        retVal = BSWM_ID_AL_AL_PvtReport_Disabled;
      }
    }
  }
  BSWM_DUMMY_STATEMENT(partitionIdx); /* PRQA S 1338, 2983, 3112 */ /* MD_MSR_DummyStmt */
  return retVal;
}


#define BSWM_STOP_SEC_CODE
/* PRQA S 5087 1 */ /* MD_MSR_MemMap */
#include "BswM_vMemMap.h"

