/**********************************************************************************************************************
 *  COPYRIGHT
 *  -------------------------------------------------------------------------------------------------------------------
 *  \verbatim
 *
 *                 This software is copyright protected and proprietary to Vector Informatik GmbH.
 *                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
 *                 All other rights remain with Vector Informatik GmbH.
 *  \endverbatim
 *  -------------------------------------------------------------------------------------------------------------------
 *  LICENSE
 *  -------------------------------------------------------------------------------------------------------------------
 *            Module: Os
 *           Program: Volvo AUTOSAR Platform 2.0 (MSR_VolvoAb_SLP2)
 *          Customer: Volvo Group Trucks Technology
 *       Expiry Date: Not restricted
 *  Ordered Derivat.: Freescale MPC5746C
 *    License Scope : The usage is restricted to CBD1800194_D04
 *
 *  -------------------------------------------------------------------------------------------------------------------
 *  FILE DESCRIPTION
 *  -------------------------------------------------------------------------------------------------------------------
 *              File: Os_Compiler_Cfg.h
 *   Generation Time: 2020-11-11 14:25:32
 *           Project: SCIM_HD_T1 - Version 1.0
 *          Delivery: CBD1800194_D04
 *      Tool Version: DaVinci Configurator  5.19.50 SP3
 *
 *
 *********************************************************************************************************************/

#ifndef OS_COMPILER_CFG_H
# define OS_COMPILER_CFG_H

/**********************************************************************************************************************
 *  OS USER CALLOUT CODE SECTIONS
 *********************************************************************************************************************/

# define OS_ASW_10MS_TASK_CODE
# define OS_ASW_20MS_TASK_CODE
# define OS_ASW_ASYNC_TASK_CODE
# define OS_ASW_INIT_TASK_CODE
# define OS_BSW_10MS_TASK_CODE
# define OS_BSW_5MS_TASK_CODE
# define OS_BSW_ASYNC_TASK_CODE
# define OS_BSW_DIAG_TASK_CODE
# define OS_BSW_LIN_TASK_CODE
# define OS_CANMAILBOXISR_0_CODE
# define OS_CANMAILBOXISR_1_CODE
# define OS_CANMAILBOXISR_2_CODE
# define OS_CANMAILBOXISR_4_CODE
# define OS_CANMAILBOXISR_6_CODE
# define OS_CANMAILBOXISR_7_CODE
# define OS_CPULOADIDLETASK_CODE
# define OS_EMIOS_0_CH_12_CH_13_ISR_CODE
# define OS_EMIOS_0_CH_14_CH_15_ISR_CODE
# define OS_EMIOS_0_CH_2_CH_3_ISR_CODE
# define OS_EMIOS_0_CH_4_CH_5_ISR_CODE
# define OS_EMIOS_0_CH_8_CH_9_ISR_CODE
# define OS_GPT_PIT_0_TIMER_0_ISR_CODE
# define OS_GPT_PIT_0_TIMER_1_ISR_CODE
# define OS_GPT_PIT_0_TIMER_2_ISR_CODE
# define OS_INIT_TASK_CODE
# define OS_LINISR_0_CODE
# define OS_LINISR_1_CODE
# define OS_LINISR_10_CODE
# define OS_LINISR_4_CODE
# define OS_LINISR_6_CODE
# define OS_LINISR_7_CODE
# define OS_LINISR_8_CODE
# define OS_LINISR_9_CODE
# define OS_MCU_PLLDIG_PLL0_LOSSOFLOCKISR_CODE
# define OS_WKPU_EXT_IRQ_0_7_ISR_CODE
# define OS_WKPU_EXT_IRQ_16_23_ISR_CODE
# define OS_WKPU_EXT_IRQ_24_31_ISR_CODE
# define OS_WKPU_EXT_IRQ_8_15_ISR_CODE


#endif /* OS_COMPILER_CFG_H */

/**********************************************************************************************************************
 *  END OF FILE: Os_Compiler_Cfg.h
 *********************************************************************************************************************/
 
