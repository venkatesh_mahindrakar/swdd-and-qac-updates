AUTHOR S. Akulionak, VGTT, v01

@ECHO ON

TITLE CONVERT_COM_TO_VLC SCIM

SET generator_path=..\..\..\scim_sip\CBD1800194_D04_Mpc57xx\Generators\LegacyDb2SystemDescrConverter\
SET COM_PATH=..\00_SystemBaseline\
SET ASR_PATH=.\

%generator_path%LegacyDb2SystemDescrConverter.exe -r 42 -e %ECU_PN% -o %ASR_PATH% ^
%COM_PATH%CAN\Backbone1J1939_postfix.dbc ^
%COM_PATH%CAN\Backbone2_postfix.dbc ^
%COM_PATH%CAN\CabSubnet_postfix.dbc ^
%COM_PATH%CAN\SecuritySubnet_postfix.dbc ^
%COM_PATH%CAN\FMSNet_postfix.dbc ^
%COM_PATH%LIN\LIN1_CIOM_postfix.ldf ^
%COM_PATH%LIN\LIN2_CIOM_postfix.ldf ^
%COM_PATH%LIN\LIN3_CIOM_postfix.ldf ^
%COM_PATH%LIN\LIN4_CIOM_postfix.ldf ^
%COM_PATH%LIN\LIN5_CIOM_postfix.ldf ^
%COM_PATH%PVT\LIN6_CIOM_postfix.ldf ^
%COM_PATH%PVT\LIN7_CIOM_postfix.ldf ^
%COM_PATH%PVT\LIN8_CIOM_postfix.ldf ^
%COM_PATH%PVT\PVT_CAN6.dbc ^
CanToLinGateway.vsde

%generator_path%LegacyDb2SystemDescrConverter.exe -r 42 -e %ECU_PN% -o %ASR_PATH% ^
%COM_PATH%PVT\PVT.dbc

PAUSE