# *********************************************************************************************************************
#   COPYRIGHT
#   -------------------------------------------------------------------------------------------------------------------
# 
#                 This software is copyright protected and proprietary to Vector Informatik GmbH.
#                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
#                 All other rights remain with Vector Informatik GmbH.
#   -------------------------------------------------------------------------------------------------------------------
#   FILE DESCRIPTION
#   -------------------------------------------------------------------------------------------------------------------
#           File:  Pvt_rules.mak
#         Config:  SCIM_HD_T1.dpa
#    ECU-Project:  CIOM
# 
#      Generator:  MICROSAR RTE Generator Version 4.19.0
#                  RTE Core Version 1.19.0
#        License:  CBD1800194
# 
#    Description:  GNU MAKEFILE (rules)
# *********************************************************************************************************************


# (Production-Template Combination )

APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\AxleLoadDistribution_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\DoubleSwitch_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\ReverseGearWarning_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\RoofHatch_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\ServiceBraking_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\SpeedControlMode_HMICtrl.c

# (Production-PVT Combination )


APP_SOURCE_LST += $(PVT_SOURCE_DIR)\LevelControl_HMICtrl.c
#APP_SOURCE_LST += $(PRD_SOURCE_DIR)\LevelControl_HMICtrl_Logic.c
APP_SOURCE_LST += $(PVT_SOURCE_DIR)\LKS_HMICtrl.c
#APP_SOURCE_LST += $(PVT_SOURCE_DIR)\TractionDifflockHMICtrl.c
APP_SOURCE_LST += $(PVT_SOURCE_DIR)\WiredControlBox_HMICtrl.c
APP_SOURCE_LST += $(PVT_SOURCE_DIR)\SCIM_PVTPT_IO.c
APP_SOURCE_LST += $(PVT_SOURCE_DIR)\SCIM_PVTPT_FlexData.c
#APP_SOURCE_LST += $(PRD_SOURCE_DIR)\WiredControlBox_HMICtrl_Logic.c



# (PVT Only)
APP_SOURCE_LST += $(PVT_SOURCE_DIR)\InteriorLights_HMICtrl.c

# SeoyonPEPSLib
