# *********************************************************************************************************************
#   COPYRIGHT
#   -------------------------------------------------------------------------------------------------------------------
# 
#                 This software is copyright protected and proprietary to Vector Informatik GmbH.
#                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
#                 All other rights remain with Vector Informatik GmbH.
#   -------------------------------------------------------------------------------------------------------------------
#   FILE DESCRIPTION
#   -------------------------------------------------------------------------------------------------------------------
#           File:  Pvt_cfg.mak
#         Config:  SCIM_HD_T1.dpa
#    ECU-Project:  CIOM
# 
#      Generator:  MICROSAR RTE Generator Version 4.19.0
#                  RTE Core Version 1.19.0
#        License:  CBD1800194
# 
#    Description:  GNU MAKEFILE (configs)
# *********************************************************************************************************************
