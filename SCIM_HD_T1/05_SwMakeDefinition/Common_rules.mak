# *********************************************************************************************************************
#   COPYRIGHT
#   -------------------------------------------------------------------------------------------------------------------
# 
#                 This software is copyright protected and proprietary to Vector Informatik GmbH.
#                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
#                 All other rights remain with Vector Informatik GmbH.
#   -------------------------------------------------------------------------------------------------------------------
#   FILE DESCRIPTION
#   -------------------------------------------------------------------------------------------------------------------
#           File:  Production_rules.mak
#         Config:  SCIM_HD_T1.dpa
#    ECU-Project:  CIOM
# 
#      Generator:  MICROSAR RTE Generator Version 4.19.0
#                  RTE Core Version 1.19.0
#        License:  CBD1800194
# 
#    Description:  GNU MAKEFILE (rules)
# *********************************************************************************************************************





# SCIM Application files

ADDITIONAL_INCLUDES += $(CMN_SOURCE_DIR)\Include

# Common files used for Production and PVTPT SW builds
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\ApproachLight_HMICtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\AuthenticationDevice_UICtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\BunkUserInterfaceBasic_LINMaCtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\BunkUserInterfaceHigh2_LINMaCtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\BunkUserInterfaceHigh2_LINMaCtrl_Logic.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\CentralDoorsLatch_Hdlr.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\DiffLockPanel_LINMasterCtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\DriverAuthentication2_Ctrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\DriverSeatBeltBuckleSwitch_hdlr.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\ECSWiredRemote_LINMasterCtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\ExteriorLightPanels_Freewheel_Gw.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\ExteriorLightPanel_1_LINMastCtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\ExteriorLightPanel_2_LINMastCtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\FaultEventGateway_ctrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\FlexibleSwitchesRouter_Ctrl_Router.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\FlexibleSwitchesRouter_Ctrl_LINMstr.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\InCabLock_HMICtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\InteriorLightPanel_1_LINMastCtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\InteriorLightPanel_2_LINMastCtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\InternalDoorsAjar_Hdlr.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\Keyfob_Mgr.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\Keyfob_UICtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\LuggageCompartements2_hdlr.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\PassiveDoorButton_hdlr.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\SCIM_LINMgr.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\SCIM_PowerSupply12V_Hdlr.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\SCIM_PVTPT_FlexData.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\SCIM_PVTPT_IO.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\SpeedControlFreeWheel_LINMasCtrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\VEC_CryptoProxyReceiverSwc.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\VEC_CryptoProxySenderSwc.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\VehicleAccess_Ctrl.c
APP_SOURCE_LST += $(CMN_SOURCE_DIR)\VehicleModeDistribution.c

# SeoyonPEPSLib

