# *********************************************************************************************************************
#   COPYRIGHT
#   -------------------------------------------------------------------------------------------------------------------
# 
#                 This software is copyright protected and proprietary to Vector Informatik GmbH.
#                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
#                 All other rights remain with Vector Informatik GmbH.
#   -------------------------------------------------------------------------------------------------------------------
#   FILE DESCRIPTION
#   -------------------------------------------------------------------------------------------------------------------
#           File:  Production_rules.mak
#         Config:  SCIM_HD_T1.dpa
#    ECU-Project:  CIOM
# 
#      Generator:  MICROSAR RTE Generator Version 4.19.0
#                  RTE Core Version 1.19.0
#        License:  CBD1800194
# 
#    Description:  GNU MAKEFILE (rules)
# *********************************************************************************************************************





# SCIM Application files

#ADDITIONAL_INCLUDES += $(TEMPLATE_SOURCE_DIR)\Include

# Templates for Production SWC Volvo
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\AuxHorn_Input_Hdlr.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\AuxiliaryBbSwitch_HMICtrl.c
#APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\CabTilt_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\CityHorn_Input_Hdlr.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\CollisionMitigation_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\DAS_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\DriveSidePwrWindowsSelector.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\EconomyPower_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\EngineSpeedControl_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\ExtraBbContOrSlid_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\ExtraBbTailLiftCrane_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\ExtraLighting_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\FMSGateway.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\FrontLidOpening_hdlr.c
#to delete next file
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\FrontLidLatchActuator_hdlr.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\FrontLidLatchSensor_hdlr.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\FrontPropulsion_UICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\FrontPropulsion_UOCtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\Horn_ctrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\Horn_HMICtrl.c
#APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\IndicationPrioritizer.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\InteriorLights_HMICtrl.c
#APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\LaneChangeSupport_HMICtrl.c
#APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\MirrorFolding_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\MirrorHeating_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\MovingUnitTraction_UICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\PassengersSeatBelt_hdlr.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\PinCode_ctrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\PowerTakeOff_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\RearAxleSteering_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\SideReverseLight_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\StaticCornerLight_HMICtrl.c
#APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\TemporaryRSL_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\TheftAlarm_HMI2_ctrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\TransferCase_HMICtrl.c
APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\VehicleStabilityControl_UICtrl.c
#APP_SOURCE_LST += $(TEMPLATE_SOURCE_DIR)\WarmUp_HMICtrl.c

# SeoyonPEPSLib

