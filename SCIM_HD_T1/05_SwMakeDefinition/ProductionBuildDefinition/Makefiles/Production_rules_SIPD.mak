# *********************************************************************************************************************
#   COPYRIGHT
#   -------------------------------------------------------------------------------------------------------------------
# 
#                 This software is copyright protected and proprietary to Vector Informatik GmbH.
#                 Vector Informatik GmbH grants to you only those rights as set out in the license conditions.
#                 All other rights remain with Vector Informatik GmbH.
#   -------------------------------------------------------------------------------------------------------------------
#   FILE DESCRIPTION
#   -------------------------------------------------------------------------------------------------------------------
#           File:  Production_rules.mak
#         Config:  SCIM_HD_T1.dpa
#    ECU-Project:  CIOM
# 
#      Generator:  MICROSAR RTE Generator Version 4.19.0
#                  RTE Core Version 1.19.0
#        License:  CBD1800194
# 
#    Description:  GNU MAKEFILE (rules)
# *********************************************************************************************************************





# SCIM Application files

ADDITIONAL_INCLUDES += $(PRD_SOURCE_DIR)\Include

# Production SWC
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\AxleLoadDistribution_HMICtrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\AxleLoadDistribution_HMICtrl_Switch.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\DoubleSwitch_HMICtrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\LevelControl_HMICtrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\LevelControl_HMICtrl_Logic.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\LKS_HMICtrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\ReverseGearWarning_HMICtrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\RoofHatch_HMICtrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\ServiceBraking_HMICtrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\SlaveDiagMgr.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\SpeedControlMode_HMICtrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\SwivelSeat_ctrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\SwivelSeatSwitch_hdlr.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\WiredControlBox_HMICtrl.c
APP_SOURCE_LST += $(PRD_SOURCE_DIR)\WiredControlBox_HMICtrl_Logic.c


# SeoyonPEPSLib

