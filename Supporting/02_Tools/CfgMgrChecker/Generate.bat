rm file_status.txt
cd  ../../../SCIM_HD_T1/10_ApplicationLayer/01_CommonBuildSource
touch file_status.txt; find . -maxdepth 1 -type f -name "*.c" -print0 | while IFS= read -r -d $'\0' file; do git log -1 --pretty="format:%cs     %h" $file >> ../../../Supporting/02_Tools/CfgMgrChecker/file_status.txt; echo "    $file" >> ../../../Supporting/02_Tools/CfgMgrChecker/file_status.txt; done
cd  ../../../SCIM_HD_T1/10_ApplicationLayer/02_ProductionBuildSource
touch file_status.txt; find . -maxdepth 1 -type f -name "*.c" -print0 | while IFS= read -r -d $'\0' file; do git log -1 --pretty="format:%cs     %h" $file >> ../../../Supporting/02_Tools/CfgMgrChecker/file_status.txt; echo "    $file" >> ../../../Supporting/02_Tools/CfgMgrChecker/file_status.txt; done
cd  ../../../SCIM_HD_T1/50_BoardSupportPackage/IoHwAb
touch file_status.txt; find . -maxdepth 1 -type f -name "*.c" -print0 | while IFS= read -r -d $'\0' file; do git log -1 --pretty="format:%cs     %h" $file >> ../../../Supporting/02_Tools/CfgMgrChecker/file_status.txt; echo "    $file" >> ../../../Supporting/02_Tools/CfgMgrChecker/file_status.txt; done
cd  ../../../SCIM_HD_T1/50_BoardSupportPackage/DiagnosticComponent
touch file_status.txt; find . -maxdepth 1 -type f -name "*.c" -print0 | while IFS= read -r -d $'\0' file; do git log -1 --pretty="format:%cs     %h" $file >> ../../../Supporting/02_Tools/CfgMgrChecker/file_status.txt; echo "    $file" >> ../../../Supporting/02_Tools/CfgMgrChecker/file_status.txt; done
