#include <stdlib.h>
unsigned int variable;

//#kw_override MACRO_StdRteRead_ExtRPort (RTE_return,extInputPortVar,RteNotAvailableValue,RteComErrorValue) extInputPortVar = retValue + (Std_ReturnType)(rand() % 1)
#kw_override MACRO_StdRteRead_ExtRPort(RTE_return,extInputPortVar,RteNotAvailableValue,RteComErrorValue) extInputPortVar = (variable + (unsigned int)(retValue)) & 0xFFu
//#kw_override MACRO_StdRteRead_IntRPort(RTE_return,IntInputPortVar,RteError) IntInputPortVarretValue = retValue + (Std_ReturnType)(rand() % 1)
#kw_override MACRO_StdRteRead_IntRPort(RTE_return,IntInputPortVar,RteError) IntInputPortVarretValue = (variable + (unsigned int)(retValue)) & 0xFFu