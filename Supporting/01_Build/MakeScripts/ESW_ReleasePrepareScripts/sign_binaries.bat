@echo off

rem set HwPn=%~1
echo %param1%
set eswbinaries_dir=%~dp0..\..\MadeBuild\ESW_ReleasePrepare\Binaries
set engtoolscript_dir=%~dp0..\..\MadeBuild\ESW_ReleasePrepare\EngTool_Scripts
@echo on
%~dp0..\..\..\02_Tools\SewsApi_1.9.0.0\SewsApi.exe SIGN -i %eswbinaries_dir%\SCIM_SBL.hex -o %engtoolscript_dir%\sign_SCIM_SBL.txt -p %HwPn% -c

%~dp0..\..\..\02_Tools\SewsApi_1.9.0.0\SewsApi.exe SIGN -i %eswbinaries_dir%\SCIM_POSTBUILD.hex -o %engtoolscript_dir%\sign_SCIM_POSTBUILD.txt -p %HwPn% -c

%~dp0..\..\..\02_Tools\SewsApi_1.9.0.0\SewsApi.exe SIGN -i %eswbinaries_dir%\SCIM_DST.hex -o %engtoolscript_dir%\sign_SCIM_DST.txt -p %HwPn% -c
%~dp0..\..\..\02_Tools\SewsApi_1.9.0.0\SewsApi.exe SIGN -i %eswbinaries_dir%\SCIM_MSW.hex -o %engtoolscript_dir%\sign_SCIM_MSW.txt -p %HwPn% -c
%~dp0..\..\..\02_Tools\SewsApi_1.9.0.0\SewsApi.exe SIGN -i %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex -o %engtoolscript_dir%\sign_SCIM_MSW_DemoKeys.txt -p %HwPn% -c
@echo off