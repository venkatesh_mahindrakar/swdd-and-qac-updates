@echo off

setlocal
set sip_dir=%~dp0..\..\..\..\..\scim_sip\CBD1800195_D00_Mpc57xx
set defaulthex_dir=%~dp0..\Bin\MswPnSecKeys
set demohex_dir=%~dp0..\Bin\DemoSecKeys
set eswbinaries_dir=%~dp0..\..\MadeBuild\ESW_ReleasePrepare\Binaries

if "%1" == "" goto default_hex
set hex_file=%1
goto esw_structure_hex
:default_hex

set hex_file=SCIM_BP_PROD.hex

:esw_structure_hex

dir /T:W  /O:D %defaulthex_dir%\%hex_file%
@echo on
%sip_dir%\Misc\HexView\hexview.exe %defaulthex_dir%\%hex_file% -S -e:error.txt -ad:0x20 -al -xs -o %eswbinaries_dir%\SCIM_DST.hex
%sip_dir%\Misc\HexView\hexview.exe %defaulthex_dir%\%hex_file% -S -e:error.txt -ad:0x20 -al -xs -o %eswbinaries_dir%\SCIM_POSTBUILD.hex
%sip_dir%\Misc\HexView\hexview.exe %defaulthex_dir%\%hex_file% -S -e:error.txt -ad:0x20 -al -xs -o %eswbinaries_dir%\SCIM_MSW.hex
%sip_dir%\Misc\HexView\hexview.exe %demohex_dir%\%hex_file% -S -e:error.txt -ad:0x20 -al -xs -o %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex

%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_DST.hex       -S -e:error.txt /AR:0xF9C000-0xFBFFFF -xs -o %eswbinaries_dir%\SCIM_DST.hex
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_POSTBUILD.hex -S -e:error.txt /AR:0x01000000-0x0103FFFF -xs -o %eswbinaries_dir%\SCIM_POSTBUILD.hex
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW.hex           -S -e:error.txt /cr:0xF9C000-0xFBFFFF -xs -o %eswbinaries_dir%\SCIM_MSW.hex
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW.hex           -S -e:error.txt /cr:0x01000000-0x0103FFFF -xs -o %eswbinaries_dir%\SCIM_MSW.hex
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW.hex -S -e:error.txt /cr:0xF90000-0xF9BFFF -xs -o %eswbinaries_dir%\SCIM_MSW.hex
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW.hex -S -e:error.txt /cr:0x40000000-0x40040000 -xs -o %eswbinaries_dir%\SCIM_MSW.hex 
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW.hex -S -e:error.txt /cr:0x1200000-0x120001F -xs -o %eswbinaries_dir%\SCIM_MSW.hex 

%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex           -S -e:error.txt /cr:0xF9C000-0xFBFFFF -xs -o %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex           -S -e:error.txt /cr:0x01000000-0x0103FFFF -xs -o %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex -S -e:error.txt /cr:0xF90000-0xF9BFFF -xs -o %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex -S -e:error.txt /cr:0x40000000-0x40040000 -xs -o %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex 
%sip_dir%\Misc\HexView\hexview.exe %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex -S -e:error.txt /cr:0x1200000-0x120001F -xs -o %eswbinaries_dir%\SCIM_MSW_DemoKeys.hex 