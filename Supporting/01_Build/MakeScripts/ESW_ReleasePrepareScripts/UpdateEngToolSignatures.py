import os
import sys
import xml.etree.ElementTree as ET
ET.register_namespace("","http://esw.volvo.com/apciplus/1_7")

#script_dir_path = 'C:/GIT/scim_ecu_hd_t1/Supporting/01_Build/MakeScripts/'
#script_dir_path = os.path.abspath('') #debug folder
script_dir_path = os.path.abspath('') + '/ESW_ReleasePrepareScripts'
#print (script_dir_path)
root_repos_dir = os.path.abspath(script_dir_path + '/../../../../')
#print (root_repos_dir)
EngTool_files_dir = root_repos_dir + '/Supporting/01_Build/MadeBuild/ESW_ReleasePrepare/EngTool_Scripts/'
#print (EngTool_files_dir)
binary_dir = root_repos_dir + '/Supporting/01_Build/MakeScripts/Bin/MswPnSecKeys/'
#script_dir_path = script_dir_path.replace(os.path.sep, os.path.altsep)
#print (script_dir_path)

############### DST ################
# get signature
f = open(EngTool_files_dir + 'sign_SCIM_DST.txt')
lines = f.readlines()
print (lines[8])

# get xml signature

tree = ET.parse(EngTool_files_dir + 'SCIM_Download_Script_DefaultBuild.xml')
root = tree.getroot()

#Update signature
print(root[1][2][0].attrib)
root[1][2][0].set ('Signature', lines[8])
print(root[1][2][0].attrib)

#tree.write(EngTool_files_dir + 'SCIM_Download_Script_DefaultBuild.xml')

############### POSTBUILD ################
# get signature
f = open(EngTool_files_dir + 'sign_SCIM_POSTBUILD.txt')

lines = f.readlines()
print (lines[8])

# get xml signature
#tree = ET.parse(EngTool_files_dir + 'SCIM_Download_Script_DefaultBuild.xml')
#root = tree.getroot()

#Update signature
print(root[1][2][1].attrib)
root[1][2][1].set ('Signature', lines[8])
print(root[1][2][1].attrib)

#tree.write(EngTool_files_dir + 'SCIM_Download_Script_DefaultBuild.xml')

############### MSW ################
# get signature
f = open(EngTool_files_dir + 'sign_SCIM_MSW.txt')

MswSign_lines = f.readlines()
print (MswSign_lines[8])

# get xml signature
#tree = ET.parse(EngTool_files_dir + 'SCIM_Download_Script_DefaultBuild.xml')
#root = tree.getroot()

#Update signature
print(root[1][2][2].attrib)
root[1][2][2].set ('Signature', MswSign_lines[8])
print(root[1][2][2].attrib)

#tree.write(EngTool_files_dir + 'SCIM_Download_Script_DefaultBuild.xml')


############### SBL ################
# get signature
f = open(EngTool_files_dir + 'sign_SCIM_SBL.txt')
SblSign_lines = f.readlines()
print (SblSign_lines[8])

# get xml signature

#tree = ET.parse(EngTool_files_dir + 'SCIM_Download_Script_DefaultBuild.xml')
#root = tree.getroot()

#Update PN
#root[1].set ('HardwarePartNumber', sys.argv[1])

#Update signature
print(root[1][1][0].attrib)
root[1][1][0].set ('Signature', SblSign_lines[8])
print(root[1][1][0].attrib)

tree.write(EngTool_files_dir + 'SCIM_Download_Script_DefaultBuild.xml')

##### MSW only, production key ###################"
# get xml signature
tree = ET.parse(EngTool_files_dir + 'SCIM_Download_Script_MswProdKey.xml')
root = tree.getroot()

#Update signatures
root[1][1][0].set ('Signature', SblSign_lines[8])
root[1][2][0].set ('Signature', MswSign_lines[8])

#Update PN
#root[1].set ('HardwarePartNumber', sys.argv[1])

tree.write(EngTool_files_dir + 'SCIM_Download_Script_MswProdKey.xml')

##### MSW only, demo key ###################"
# get signature
f = open(EngTool_files_dir + 'sign_SCIM_MSW_DemoKeys.txt')

lines = f.readlines()
print (lines[8])

# get xml signature
tree = ET.parse(EngTool_files_dir + 'SCIM_Download_Script_MswDemoKey.xml')
root = tree.getroot()

#Update signature
print(root[1][2][0].attrib)
root[1][2][0].set ('Signature', lines[8])
print(root[1][2][0].attrib)

root[1][1][0].set ('Signature', SblSign_lines[8])

#Update PN
#root[1].set ('HardwarePartNumber', sys.argv[1])

tree.write(EngTool_files_dir + 'SCIM_Download_Script_MswDemoKey.xml')