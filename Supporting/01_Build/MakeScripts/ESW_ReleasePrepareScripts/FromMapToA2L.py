import os
import xml.etree.ElementTree as ET

# for script debug: script_dir_path = os.path.abspath('')
script_dir_path = os.path.abspath('') + '/ESW_ReleasePrepareScripts'
root_repos_dir = script_dir_path + '/../../../../'
desc_file_dir = root_repos_dir + 'Supporting/01_Build/MadeBuild/ESW_ReleasePrepare/DescriptionFiles/'
binary_dir = root_repos_dir + 'Supporting/01_Build/MakeScripts/Bin/MswPnSecKeys/'
print(script_dir_path)

#open().read().find('Rte_AddrPar_0x2B_and_0x37_P1DKI_IL_CtrlDeviceTypeBunk_v'):

AddrMapExtract = open(desc_file_dir + 'AddrMapExtract.map','w')
MatchMapExtract = open(desc_file_dir + 'MatchMapExtract.map','w')

MapFileLine = []
with open (binary_dir + 'SCIM_BP_PROD.map', 'r') as MapFile:
    for line in MapFile:
        if 'Rte_AddrPar_' in line:
            if not '.security_level' in line:
                MapFileLine = line
#                print (MapFileLine)
                MapFileLineAddr = MapFileLine[MapFileLine.find("00f"):MapFileLine.find("00f") + 8]
#                print (MapFileLineAddr)
                AddrMapExtract.write(line)

AddrMapExtract.close()

SCIM_AddrA2L = open(desc_file_dir + 'SCIM.a2l','w')

ParameterName = []
PatchedLine = []
ParameterAddr = []
with open (root_repos_dir + 'SCIM_HD_T1/30_Diagnostic/Sews.a2l', 'r') as A2LFile:
    for line in A2LFile:
        if 'Rte_AddrPar_' in line:
            #print (line)
            MatchMapExtract.write(line)
            ParameterName = line[line.find("Rte_AddrPar_"):line.find("_v") + 2]
            #print (ParameterName)
            # search address
            isFound = False
#            with open(binary_dir + 'SCIM_BP_PROD.map', 'r') as MapFile:
            with open(desc_file_dir + 'AddrMapExtract.map', 'r') as MapFile:
                for StringOfMapFile in MapFile:
                    if ParameterName in StringOfMapFile:
#                        if not '.security_level' in StringOfMapFile:
                            #print (StringOfMapFile)
                            ParameterAddr = StringOfMapFile[StringOfMapFile.find("00f"):StringOfMapFile.find("00f") + 8]
                            #print (ParameterAddr)
                            MatchMapExtract.write(StringOfMapFile)
                            isFound = True
            if isFound == False:
                print('Parameter not found in map file: ', ParameterName)
            CopyLine = line
        elif ('0x0 SEWS_RL' in line and isFound == True):
            #print (line)
            StringBefore = line[None:line.find("0x")]
            #print (StringBefore)
            StringAfter = line[line.find("x0")+2:None]
            #print (StringAfter)
            PatchedLine = StringBefore + "0x" + ParameterAddr + StringAfter
            #print (PatchedLine)
            CopyLine = PatchedLine
        else:
            CopyLine = line

        SCIM_AddrA2L.write(CopyLine)

print ("A2L file updated")