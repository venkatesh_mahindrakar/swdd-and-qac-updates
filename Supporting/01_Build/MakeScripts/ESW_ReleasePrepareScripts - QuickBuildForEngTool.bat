@echo off

xcopy /F /Y ESW_ReleasePrepareScripts\EngToolTemplates\*.xml ..\MadeBuild\ESW_ReleasePrepare\EngTool_Scripts\

set BuildFolder=%~dp0
set OdxFolder=%~dp0..\..\..\SCIM_HD_T1\30_Diagnostic\ODX\
set DescFolder=%~dp0..\MadeBuild\ESW_ReleasePrepare\DescriptionFiles

:selectHW
set HwPn=23953522
echo ===============================
echo Select a HW PN:
echo ===============================
rem echo 1) 1.0.0.0:            23283361
rem echo 2) 1.0.1.0:            23883421
rem set /p choice=Type option:
rem if "%choice%"=="1" set HwPn=23283361
rem if "%choice%"=="2" set HwPn=23883421
echo ===============================
echo SelectedHW:            %HwPn%
echo ===============================

rem timeout 3 > NUL

rem #############################################

rem call Build_Production.bat
echo Decompose Binaries

call %BuildFolder%\ESW_ReleasePrepareScripts\extract_binaries.bat

echo Sign Binaries
rem call %BuildFolder%\ESW_ReleasePrepareScripts\sign_binaries_23283361.bat
rem call %BuildFolder%\ESW_ReleasePrepareScripts\sign_binaries_23883421.bat
call %BuildFolder%\ESW_ReleasePrepareScripts\sign_binaries.bat %HwPn%


python %BuildFolder%\ESW_ReleasePrepareScripts\UpdateEngToolSignatures.py %HwPn%

choice /m "Generate Description File?" /c yn /t 5 /d n
IF ERRORLEVEL 2 GOTO SkipDescFile
CALL "ESW_ReleasePrepareScripts - Generate Desc File.bat"
echo ===============================

:SkipDescFile

timeout 10 > NUL
explorer.exe "%~dp0..\MadeBuild\ESW_ReleasePrepare\"