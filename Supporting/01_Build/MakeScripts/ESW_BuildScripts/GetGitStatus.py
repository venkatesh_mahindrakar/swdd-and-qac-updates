import os

script_dir_path = os.path.abspath('')
#print(script_dir_path)
repo_path = script_dir_path + '/../../../'
#print(repo_path)

cmd = f"git -C {repo_path} diff --stat"
stream = os.popen(cmd)
output = stream.read().strip()
if output != "":
  status = "dirty"
else:
  status = "clean"
cmd = f"git -C {repo_path} rev-parse --short HEAD"
stream = os.popen(cmd)
hashValue = stream.read().strip()
cmd = f"git -C {repo_path} rev-parse --abbrev-ref HEAD"
stream = os.popen(cmd)
branch = stream.read().strip()
gitSummary = branch + " #" + hashValue + " (" + status + ")"

if ("(clean)" in gitSummary):
    COLOR =  '\033[32m'
else:
    COLOR =  '\033[31m'
print("")
print(COLOR + "Git local repository status: " + gitSummary)
COLOR = '\033[m'
print(COLOR + "")


defineGitSummary = '#define BuildTag ("' + gitSummary + '")'
File_inc = open(repo_path + 'SCIM_HD_T1/50_BoardSupportPackage/DiagnosticComponent/' + 'BuildId.inc', 'w')
File_inc.write(defineGitSummary)
File_inc.close()