@echo off

set BuildFolder=%~dp0
set OdxFolder=%~dp0..\..\..\SCIM_HD_T1\30_Diagnostic\ODX\
set DescFolder=%~dp0..\MadeBuild\ESW_ReleasePrepare\DescriptionFiles

echo Generate Description File
python %BuildFolder%\ESW_ReleasePrepareScripts\FromMapToA2L.py

call %BuildFolder%\ESW_ReleasePrepareScripts\GenerateDescriptionFile.bat

rem pause

rem explorer.exe "%~dp0..\MadeBuild\ESW_ReleasePrepare\"